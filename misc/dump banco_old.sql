--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alternativarespostasvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.alternativarespostasvs (
    id bigint NOT NULL,
    ordem integer NOT NULL,
    proximapergunta integer NOT NULL,
    texto character varying(1000),
    possuicomplemento boolean,
    id_pergunta_questionario bigint
);


ALTER TABLE public.alternativarespostasvs OWNER TO dbroot;

--
-- Name: andamentooperacao; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.andamentooperacao (
    id bigint NOT NULL,
    comentario character varying(3000),
    data timestamp without time zone,
    fase integer,
    proximafase integer,
    id_mot_aprovacao bigint,
    id_operacao bigint,
    id_usuario bigint
);


ALTER TABLE public.andamentooperacao OWNER TO dbroot;

--
-- Name: ano_mod_comb_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.ano_mod_comb_santander (
    id bigint NOT NULL,
    description character varying(100),
    integrationcode character varying(100),
    id_modelo bigint
);


ALTER TABLE public.ano_mod_comb_santander OWNER TO dbroot;

--
-- Name: anunciosvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.anunciosvs (
    id bigint NOT NULL,
    anofabricacao integer,
    anomodelo integer,
    comentarios character varying(3000),
    marca character varying(100),
    modeloveiculo character varying(200),
    placa character varying(100),
    textoanuncio character varying(400),
    url character varying(400),
    valor numeric(38,0),
    id_lead bigint
);


ALTER TABLE public.anunciosvs OWNER TO dbroot;

--
-- Name: aprovacao_bancaria_motivos; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.aprovacao_bancaria_motivos (
    aprovacao_id bigint NOT NULL,
    motivo_id bigint NOT NULL
);


ALTER TABLE public.aprovacao_bancaria_motivos OWNER TO dbroot;

--
-- Name: aprovacaobancaria; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.aprovacaobancaria (
    id bigint NOT NULL,
    aprovado boolean,
    comentario character varying(2000),
    data timestamp without time zone,
    id_calculo bigint,
    id_usuario bigint
);


ALTER TABLE public.aprovacaobancaria OWNER TO dbroot;

--
-- Name: atividadeeconomicasantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.atividadeeconomicasantander (
    id bigint NOT NULL,
    description character varying(100),
    id_tipo bigint
);


ALTER TABLE public.atividadeeconomicasantander OWNER TO dbroot;

--
-- Name: atividadeleadsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.atividadeleadsvs (
    id bigint NOT NULL,
    concluida boolean,
    data timestamp without time zone,
    dataconclusao timestamp without time zone,
    datacriacao timestamp without time zone,
    descricao character varying(3000),
    nome character varying(300),
    notaconclusao character varying(3000),
    tipoatividade character varying(255),
    id_lead bigint,
    id_usuario_conclusao bigint,
    id_usuario_criacao bigint
);


ALTER TABLE public.atividadeleadsvs OWNER TO dbroot;

--
-- Name: avalista; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.avalista (
    id bigint NOT NULL,
    cidadenascimento character varying(100),
    cpf character varying(255),
    dataemissaorg timestamp without time zone,
    datanascimento timestamp without time zone,
    email character varying(100),
    escolaridade character varying(255),
    estadocivil character varying(255),
    nacionalidade character varying(100),
    nome character varying(255),
    nomemae character varying(100),
    nomepai character varying(100),
    orgaoemissordocrg character varying(255),
    orgaoemissorrg character varying(100),
    rg character varying(100),
    sexo character varying(255),
    telefonecelular character varying(100),
    telefoneresidencial character varying(100),
    tipoavalista character varying(255),
    ufemissorrg character varying(100),
    ufnascimento character varying(100),
    uforgaoemissordocrg character varying(255),
    conjuge_id bigint,
    endereco_id bigint,
    ocupacao_id bigint
);


ALTER TABLE public.avalista OWNER TO dbroot;

--
-- Name: avalista_svs_imoveis_pat; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.avalista_svs_imoveis_pat (
    avalista_id bigint NOT NULL,
    imovel_id bigint NOT NULL
);


ALTER TABLE public.avalista_svs_imoveis_pat OWNER TO dbroot;

--
-- Name: avalista_svs_outrasrendas; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.avalista_svs_outrasrendas (
    avalista_id bigint NOT NULL,
    outrarenda_id bigint NOT NULL
);


ALTER TABLE public.avalista_svs_outrasrendas OWNER TO dbroot;

--
-- Name: avalista_svs_veiculos_pat; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.avalista_svs_veiculos_pat (
    avalista_id bigint NOT NULL,
    veiculo_id bigint NOT NULL
);


ALTER TABLE public.avalista_svs_veiculos_pat OWNER TO dbroot;

--
-- Name: banco; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.banco (
    id bigint NOT NULL,
    ativo boolean,
    descricao character varying(250) NOT NULL,
    numero character varying(50),
    site character varying(250)
);


ALTER TABLE public.banco OWNER TO dbroot;

--
-- Name: bancosantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.bancosantander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.bancosantander OWNER TO dbroot;

--
-- Name: calculorentabilidade; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.calculorentabilidade (
    id bigint NOT NULL,
    aprovacaoonline boolean,
    bancointegracaoonline character varying(255),
    coeficiente double precision,
    coeficientealterado boolean,
    enviadoaobanco boolean,
    parcelas integer,
    percentrada double precision,
    percplus double precision,
    percretorno double precision,
    status character varying(255),
    valor double precision,
    valorcartorio double precision,
    valorentrada double precision,
    valorfinanciado double precision,
    valorgravame double precision,
    valoroutros double precision,
    valorplus double precision,
    valorpmt double precision,
    valorretorno double precision,
    valortac double precision,
    valorvistoria double precision,
    id_instituicao_financ bigint,
    id_mascara bigint,
    id_operacao bigint,
    id_tabelafinanceira bigint
);


ALTER TABLE public.calculorentabilidade OWNER TO dbroot;

--
-- Name: campoparsesvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.campoparsesvs (
    id bigint NOT NULL,
    campo character varying(200),
    campoparse character varying(255),
    tag character varying(200),
    id_configuracao_parse bigint
);


ALTER TABLE public.campoparsesvs OWNER TO dbroot;

--
-- Name: cidade; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.cidade (
    id bigint NOT NULL,
    cep character varying(30),
    nome character varying(100) NOT NULL,
    estado_id bigint
);


ALTER TABLE public.cidade OWNER TO dbroot;

--
-- Name: cliente; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.cliente (
    id bigint NOT NULL,
    cpfcnpj character varying(255) NOT NULL,
    email character varying(255),
    nome character varying(255),
    tipopessoa character varying(255)
);


ALTER TABLE public.cliente OWNER TO dbroot;

--
-- Name: cnpj_if_prod_fin_v2; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.cnpj_if_prod_fin_v2 (
    cnpj_inst_financeira_id bigint NOT NULL,
    produto_financeiro_id bigint NOT NULL
);


ALTER TABLE public.cnpj_if_prod_fin_v2 OWNER TO dbroot;

--
-- Name: cnpj_inst_financ; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.cnpj_inst_financ (
    id bigint NOT NULL,
    ativo boolean,
    cnpj character varying(50),
    razaosocial character varying(100),
    instituicao_financeira_id bigint
);


ALTER TABLE public.cnpj_inst_financ OWNER TO dbroot;

--
-- Name: conf_par_emails_dom; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.conf_par_emails_dom (
    configuracao_id bigint NOT NULL,
    dominio_email_id bigint NOT NULL
);


ALTER TABLE public.conf_par_emails_dom OWNER TO dbroot;

--
-- Name: configuracaoparsesvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.configuracaoparsesvs (
    id bigint NOT NULL,
    ativo boolean,
    descricao character varying(200),
    id_fonte_lead bigint
);


ALTER TABLE public.configuracaoparsesvs OWNER TO dbroot;

--
-- Name: conjuge; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.conjuge (
    id bigint NOT NULL,
    cidadenascimento character varying(100),
    cpf character varying(255),
    datanascimento timestamp without time zone,
    email character varying(100),
    escolaridade character varying(255),
    estadocivil character varying(255),
    nacionalidade character varying(100),
    nome character varying(255),
    nomemae character varying(100),
    nomepai character varying(100),
    orgaoemissordocrg character varying(255),
    orgaoemissorrg character varying(100),
    rg character varying(100),
    sexo character varying(255),
    telefonecelular character varying(100),
    telefoneresidencial character varying(100),
    ufemissorrg character varying(100),
    ufnascimento character varying(100),
    uforgaoemissordocrg character varying(255),
    ocupacao_id bigint
);


ALTER TABLE public.conjuge OWNER TO dbroot;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO dbroot;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO dbroot;

--
-- Name: dominioemailsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.dominioemailsvs (
    id bigint NOT NULL,
    ativo boolean,
    email character varying(200)
);


ALTER TABLE public.dominioemailsvs OWNER TO dbroot;

--
-- Name: emailblacklist; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.emailblacklist (
    id bigint NOT NULL,
    ativo boolean,
    email character varying(200)
);


ALTER TABLE public.emailblacklist OWNER TO dbroot;

--
-- Name: endereco; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.endereco (
    id bigint NOT NULL,
    bairro character varying(200),
    cep character varying(100),
    codigodealerworkflow bigint,
    complemento character varying(2000),
    enumtipoiptu character varying(255),
    logradouro character varying(2000),
    nacidadedesde timestamp without time zone,
    noimoveldesde timestamp without time zone,
    numero character varying(50),
    tipologradouro character varying(255),
    tiporesidencia character varying(255),
    id_municipio bigint,
    id_tipo_endereco bigint
);


ALTER TABLE public.endereco OWNER TO dbroot;

--
-- Name: entidadeteste; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.entidadeteste (
    id bigint NOT NULL,
    descricao character varying(300)
);


ALTER TABLE public.entidadeteste OWNER TO dbroot;

--
-- Name: estado; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.estado (
    id bigint NOT NULL,
    codigo character varying(255),
    nome character varying(255),
    sigla character varying(255)
);


ALTER TABLE public.estado OWNER TO dbroot;

--
-- Name: estado_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.estado_santander (
    id character varying(255) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.estado_santander OWNER TO dbroot;

--
-- Name: estadocivilsantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.estadocivilsantander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.estadocivilsantander OWNER TO dbroot;

--
-- Name: faturamentoclientesvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.faturamentoclientesvs (
    id bigint NOT NULL,
    datareferencia timestamp without time zone,
    valor double precision
);


ALTER TABLE public.faturamentoclientesvs OWNER TO dbroot;

--
-- Name: financiamentoonlineitau; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.financiamentoonlineitau (
    id bigint NOT NULL,
    id_operacao bigint,
    id_json_credit_an_itau bigint,
    id_json_sim_resp_itau bigint,
    id_json_transac_req_itau bigint
);


ALTER TABLE public.financiamentoonlineitau OWNER TO dbroot;

--
-- Name: financiamentoonlinesantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.financiamentoonlinesantander (
    id bigint NOT NULL,
    pdfcet bytea,
    status character varying(100),
    id_calculo_rentabilidade bigint,
    id_json_req_id_santander bigint,
    id_json_req_prop_santander bigint,
    id_json_simulacao_santander bigint,
    id_json_resp_prean_santander bigint,
    id_json_resp_prop_santander bigint,
    id_json_resp_sim_santander bigint
);


ALTER TABLE public.financiamentoonlinesantander OWNER TO dbroot;

--
-- Name: fonteleadsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.fonteleadsvs (
    id bigint NOT NULL,
    ativo boolean,
    descricao character varying(200)
);


ALTER TABLE public.fonteleadsvs OWNER TO dbroot;

--
-- Name: formapagamentosantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.formapagamentosantander (
    id bigint NOT NULL,
    codigoproduto character varying(100),
    description character varying(100)
);


ALTER TABLE public.formapagamentosantander OWNER TO dbroot;

--
-- Name: fornecedorclientesvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.fornecedorclientesvs (
    id bigint NOT NULL,
    nome character varying(255),
    telefone character varying(255)
);


ALTER TABLE public.fornecedorclientesvs OWNER TO dbroot;

--
-- Name: grauparentescosantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.grauparentescosantander (
    id bigint NOT NULL,
    description character varying(100),
    integrationcode character varying(100)
);


ALTER TABLE public.grauparentescosantander OWNER TO dbroot;

--
-- Name: hist_int_leads_icarros; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.hist_int_leads_icarros (
    id bigint NOT NULL,
    datahorafiltro timestamp without time zone,
    datahorainicio timestamp without time zone,
    quantidaderegistros integer,
    id_unidade_organizacional bigint
);


ALTER TABLE public.hist_int_leads_icarros OWNER TO dbroot;

--
-- Name: historicoalteracao; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.historicoalteracao (
    id bigint NOT NULL,
    classeobjeto character varying(255),
    data timestamp without time zone,
    log character varying(255),
    objetoalteradoid bigint,
    tipo character varying(255),
    id_usuario bigint
);


ALTER TABLE public.historicoalteracao OWNER TO dbroot;

--
-- Name: historicoleadsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.historicoleadsvs (
    id bigint NOT NULL,
    data timestamp without time zone,
    descricao character varying(200),
    statusanterior character varying(255),
    statusatual character varying(255),
    id_lead bigint,
    id_usuario bigint
);


ALTER TABLE public.historicoleadsvs OWNER TO dbroot;

--
-- Name: icarrostoken; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.icarrostoken (
    id bigint NOT NULL,
    accesstoken character varying(2000) NOT NULL,
    creationdate timestamp without time zone NOT NULL,
    expiresin integer NOT NULL,
    idtoken character varying(2000) NOT NULL,
    notbeforepolicy integer NOT NULL,
    refreshexpiresin integer NOT NULL,
    refreshtoken character varying(2000) NOT NULL,
    sessionstate character varying(255) NOT NULL,
    tokentype character varying(255) NOT NULL,
    icarrosusuario_id bigint NOT NULL
);


ALTER TABLE public.icarrostoken OWNER TO dbroot;

--
-- Name: icarrosusuario; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.icarrosusuario (
    id bigint NOT NULL,
    clientid character varying(255) NOT NULL,
    clientsecret character varying(255) NOT NULL,
    descricao character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE public.icarrosusuario OWNER TO dbroot;

--
-- Name: imovelpatrimonio; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.imovelpatrimonio (
    id bigint NOT NULL,
    descricao character varying(1000),
    tipoimovel character varying(255),
    valor double precision,
    endereco_id bigint
);


ALTER TABLE public.imovelpatrimonio OWNER TO dbroot;

--
-- Name: instituicaofinanceira; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.instituicaofinanceira (
    id bigint NOT NULL,
    alteradoem timestamp without time zone,
    ativa boolean,
    codigo character varying(50),
    codigodealerworkflow bigint,
    criadoem timestamp without time zone,
    nome character varying(200),
    razaosocial character varying(200),
    id_usuario_criacao_alteracao bigint
);


ALTER TABLE public.instituicaofinanceira OWNER TO dbroot;

--
-- Name: itemcbc; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.itemcbc (
    id bigint NOT NULL,
    adicionaraofinanciamento boolean,
    codigo integer,
    dataprevisao timestamp without time zone,
    valor double precision,
    id_opcao_item_cbc bigint,
    id_operacao bigint
);


ALTER TABLE public.itemcbc OWNER TO dbroot;

--
-- Name: json_credit_an_itau; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.json_credit_an_itau (
    id bigint NOT NULL,
    data timestamp without time zone,
    jsonbyte bytea
);


ALTER TABLE public.json_credit_an_itau OWNER TO dbroot;

--
-- Name: json_ident_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.json_ident_santander (
    id bigint NOT NULL,
    cpfcliente character varying(255),
    data timestamp without time zone,
    jsonbyte bytea,
    nomecliente character varying(255),
    tipoveiculo character varying(255),
    id_ano_modelo bigint,
    id_modelo bigint
);


ALTER TABLE public.json_ident_santander OWNER TO dbroot;

--
-- Name: json_preanalise_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.json_preanalise_santander (
    id bigint NOT NULL,
    cpfcliente character varying(255),
    data timestamp without time zone,
    jsonbyte bytea,
    nomecliente character varying(255)
);


ALTER TABLE public.json_preanalise_santander OWNER TO dbroot;

--
-- Name: json_req_sim_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.json_req_sim_santander (
    id bigint NOT NULL,
    data timestamp without time zone,
    jsonbyte bytea
);


ALTER TABLE public.json_req_sim_santander OWNER TO dbroot;

--
-- Name: json_sim_resp_itau; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.json_sim_resp_itau (
    id bigint NOT NULL,
    data timestamp without time zone,
    jsonbyte bytea
);


ALTER TABLE public.json_sim_resp_itau OWNER TO dbroot;

--
-- Name: json_transac_req_itau; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.json_transac_req_itau (
    id bigint NOT NULL,
    cpfcliente character varying(255),
    data timestamp without time zone,
    jsonbyte bytea,
    nomecliente character varying(255)
);


ALTER TABLE public.json_transac_req_itau OWNER TO dbroot;

--
-- Name: jsonrequisicaoproposta; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.jsonrequisicaoproposta (
    id bigint NOT NULL,
    data timestamp without time zone,
    jsonbyte bytea
);


ALTER TABLE public.jsonrequisicaoproposta OWNER TO dbroot;

--
-- Name: jsonrespostaproposta; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.jsonrespostaproposta (
    id bigint NOT NULL,
    data timestamp without time zone,
    jsonbyte bytea
);


ALTER TABLE public.jsonrespostaproposta OWNER TO dbroot;

--
-- Name: jsonrespostasimulacaosantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.jsonrespostasimulacaosantander (
    id bigint NOT NULL,
    data timestamp without time zone,
    jsonbyte bytea
);


ALTER TABLE public.jsonrespostasimulacaosantander OWNER TO dbroot;

--
-- Name: leadsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.leadsvs (
    id bigint NOT NULL,
    assuntoemail character varying(100),
    cnpj_unidade character varying(100),
    corpoemail text,
    cpfcliente character varying(255),
    cpfvendedorcl character varying(255),
    dataemail timestamp without time zone,
    datainclusao timestamp without time zone,
    departamento character varying(255),
    email character varying(200),
    enviadoconnectlead boolean,
    idleadcl character varying(255),
    meiocontato character varying(255),
    nome character varying(100),
    nomecompleto character varying(100),
    nomevendedorcl character varying(255),
    status character varying(255),
    telefone character varying(100),
    id_fonte_lead bigint,
    id_produto_interesse bigint,
    id_responsavel bigint,
    id_unidade_organizacional bigint,
    id_leadenriquecido bigint,
    id_motivo_descarte bigint,
    fromemail character varying(255)
);


ALTER TABLE public.leadsvs OWNER TO dbroot;

--
-- Name: leadsvsenriquecido; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.leadsvsenriquecido (
    id bigint NOT NULL,
    cep character varying(20),
    cidade character varying(100),
    complemento character varying(255),
    cpf character varying(100),
    cpfstatus character varying(100),
    datanascimento character varying(20),
    endereco character varying(255),
    estado character varying(100),
    genero character varying(10),
    maenome character varying(100),
    nome character varying(100),
    numero character varying(255),
    jsonretorno text
);


ALTER TABLE public.leadsvsenriquecido OWNER TO dbroot;

--
-- Name: marcasantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.marcasantander (
    id bigint NOT NULL,
    description character varying(100),
    integrationcode character varying(100),
    marcamysaga character varying(255),
    tipoveiculo character varying(255)
);


ALTER TABLE public.marcasantander OWNER TO dbroot;

--
-- Name: masc_taxa_tabela_fin; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.masc_taxa_tabela_fin (
    id bigint NOT NULL,
    alteradoem timestamp without time zone,
    ativa boolean,
    bancointegracaoonline character varying(255),
    codigoretorno character varying(200),
    criadoem timestamp without time zone,
    mascara character varying(200),
    ordem integer,
    percentualretorno double precision,
    id_usuario_criacao_alteracao bigint
);


ALTER TABLE public.masc_taxa_tabela_fin OWNER TO dbroot;

--
-- Name: mod_veic_sant_unidades; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.mod_veic_sant_unidades (
    modelo_id bigint NOT NULL,
    unidade_id bigint NOT NULL
);


ALTER TABLE public.mod_veic_sant_unidades OWNER TO dbroot;

--
-- Name: modelo_quest_departamentos; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.modelo_quest_departamentos (
    modelo_questionario_id bigint,
    departamento character varying(255)
);


ALTER TABLE public.modelo_quest_departamentos OWNER TO dbroot;

--
-- Name: modelo_quest_unidades; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.modelo_quest_unidades (
    modelo_questionario_id bigint NOT NULL,
    unidade_id bigint NOT NULL
);


ALTER TABLE public.modelo_quest_unidades OWNER TO dbroot;

--
-- Name: modelo_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.modelo_santander (
    id bigint NOT NULL,
    ativo boolean,
    description character varying(100),
    integrationcode character varying(100),
    id_marca bigint
);


ALTER TABLE public.modelo_santander OWNER TO dbroot;

--
-- Name: modeloquestionariosvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.modeloquestionariosvs (
    id bigint NOT NULL,
    uuid character varying(100),
    descricao character varying(300),
    ativo boolean
);


ALTER TABLE public.modeloquestionariosvs OWNER TO dbroot;

--
-- Name: modeloveiculo; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.modeloveiculo (
    id bigint NOT NULL,
    descricao character varying(200) NOT NULL
);


ALTER TABLE public.modeloveiculo OWNER TO dbroot;

--
-- Name: motivoaprovacaosvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.motivoaprovacaosvs (
    id bigint NOT NULL,
    alteradoem timestamp without time zone,
    ativo boolean,
    criadoem timestamp without time zone,
    titulo character varying(200),
    id_usuario_criacao_alteracao bigint
);


ALTER TABLE public.motivoaprovacaosvs OWNER TO dbroot;

--
-- Name: motivodescartesvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.motivodescartesvs (
    id bigint NOT NULL,
    alteradoem timestamp without time zone,
    ativo boolean,
    criadoem timestamp without time zone,
    titulo character varying(200),
    id_usuario_criacao_alteracao bigint
);


ALTER TABLE public.motivodescartesvs OWNER TO dbroot;

--
-- Name: municipio; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.municipio (
    id bigint NOT NULL,
    codigoibge character varying(100),
    nome character varying(100),
    estadocodigo character varying(255)
);


ALTER TABLE public.municipio OWNER TO dbroot;

--
-- Name: municipio_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.municipio_santander (
    id bigint NOT NULL,
    description character varying(100),
    id_estado character varying(255)
);


ALTER TABLE public.municipio_santander OWNER TO dbroot;

--
-- Name: nacionalidadesantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.nacionalidadesantander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.nacionalidadesantander OWNER TO dbroot;

--
-- Name: naturezajuridicasantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.naturezajuridicasantander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.naturezajuridicasantander OWNER TO dbroot;

--
-- Name: notaleadsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.notaleadsvs (
    id bigint NOT NULL,
    data timestamp without time zone,
    nota character varying(3000),
    id_lead bigint
);


ALTER TABLE public.notaleadsvs OWNER TO dbroot;

--
-- Name: ocupacao; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.ocupacao (
    id bigint NOT NULL,
    capitalsocial double precision,
    cargo character varying(255),
    cnpj character varying(200),
    contador character varying(200),
    datainicialcargo timestamp without time zone,
    nomecontador character varying(200),
    nomeempregador character varying(200),
    ocupacao character varying(200),
    percparticipacao double precision,
    renda double precision,
    sociodesde timestamp without time zone,
    telefone character varying(200),
    telefonecontador character varying(200),
    tipoocupacao character varying(255),
    atividade_id bigint,
    tipo_atividade_id bigint,
    endereco_id bigint,
    profissao_id bigint
);


ALTER TABLE public.ocupacao OWNER TO dbroot;

--
-- Name: op_finan_online_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.op_finan_online_santander (
    id bigint NOT NULL,
    financiamento_id bigint NOT NULL
);


ALTER TABLE public.op_finan_online_santander OWNER TO dbroot;

--
-- Name: opcaoitemcbc; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.opcaoitemcbc (
    id bigint NOT NULL,
    ativo boolean,
    codigodealerworkflow bigint,
    descricao character varying(500),
    id_unidade_organizacional bigint
);


ALTER TABLE public.opcaoitemcbc OWNER TO dbroot;

--
-- Name: opcionaisveiculoproposta; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.opcionaisveiculoproposta (
    id bigint NOT NULL,
    opcional_id bigint NOT NULL
);


ALTER TABLE public.opcionaisveiculoproposta OWNER TO dbroot;

--
-- Name: opcionalveiculo; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.opcionalveiculo (
    id bigint NOT NULL,
    opcionalcodigo bigint,
    opcionaldescricao character varying(2000)
);


ALTER TABLE public.opcionalveiculo OWNER TO dbroot;

--
-- Name: operacaofinanciada; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.operacaofinanciada (
    id bigint NOT NULL,
    cpfvendedor character varying(100),
    dataentrada timestamp without time zone,
    dataultimamovimentacao timestamp without time zone,
    departamento character varying(255),
    fase character varying(255),
    numeroproposta character varying(100),
    parcelas integer,
    percentrada double precision,
    tipotabelafaturamento character varying(255),
    tipoveiculo character varying(255),
    valor double precision,
    valorcbc double precision,
    valorcbcfinanciamento double precision,
    valorentrada double precision,
    valorfinanciado double precision,
    id_ano_modelo bigint,
    id_calculo_tabela_instit bigint,
    id_cliente bigint,
    id_instituicao_financ_fat bigint,
    id_marca_santander bigint,
    id_modelo bigint,
    id_unidade_organizacional bigint,
    id_vendedor bigint,
    id_calculo_faturamento bigint,
    id_proposta bigint
);


ALTER TABLE public.operacaofinanciada OWNER TO dbroot;

--
-- Name: operacaofinanciadacalcenv; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.operacaofinanciadacalcenv (
    id bigint NOT NULL,
    calculo_id bigint NOT NULL
);


ALTER TABLE public.operacaofinanciadacalcenv OWNER TO dbroot;

--
-- Name: operacaofinanciadatodasrent; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.operacaofinanciadatodasrent (
    id bigint NOT NULL,
    calculo_id bigint NOT NULL
);


ALTER TABLE public.operacaofinanciadatodasrent OWNER TO dbroot;

--
-- Name: outrarendaclientesvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.outrarendaclientesvs (
    id bigint NOT NULL,
    descricao character varying(500),
    telefone character varying(500),
    valor double precision
);


ALTER TABLE public.outrarendaclientesvs OWNER TO dbroot;

--
-- Name: parcelaentrada; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.parcelaentrada (
    id bigint NOT NULL,
    codigo integer,
    data timestamp without time zone,
    observacao character varying(1000),
    tipo character varying(255),
    valor double precision,
    id_operacao bigint,
    id_tipo_pagamento bigint,
    id_parcela_abater bigint
);


ALTER TABLE public.parcelaentrada OWNER TO dbroot;

--
-- Name: perfilsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.perfilsvs (
    id bigint NOT NULL,
    ativo boolean,
    nome character varying(100)
);


ALTER TABLE public.perfilsvs OWNER TO dbroot;

--
-- Name: perguntaquestionariosvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.perguntaquestionariosvs (
    id bigint NOT NULL,
    ordem integer NOT NULL,
    uuid character varying(100),
    texto character varying(300),
    tiporesposta character varying(100),
    ativo boolean,
    id_modelo_questionario bigint,
    respostaobrigatoria boolean
);


ALTER TABLE public.perguntaquestionariosvs OWNER TO dbroot;

--
-- Name: porteempresasantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.porteempresasantander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.porteempresasantander OWNER TO dbroot;

--
-- Name: produtofinanceiro; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.produtofinanceiro (
    id bigint NOT NULL,
    alteradoem timestamp without time zone,
    ativo boolean,
    criadoem timestamp without time zone,
    nome character varying(100),
    id_usuario_criacao_alteracao bigint
);


ALTER TABLE public.produtofinanceiro OWNER TO dbroot;

--
-- Name: produtointeresse; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.produtointeresse (
    id bigint NOT NULL,
    alteradoem timestamp without time zone,
    ativo boolean,
    criadoem timestamp without time zone,
    descricao character varying(200),
    id_usuario_criacao_alteracao bigint,
    id_familia_veiculo_dealer character varying(50)
);


ALTER TABLE public.produtointeresse OWNER TO dbroot;

--
-- Name: profissaosantander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.profissaosantander (
    id bigint NOT NULL,
    description character varying(100),
    professionequivalentezflow character varying(255)
);


ALTER TABLE public.profissaosantander OWNER TO dbroot;

--
-- Name: proposta; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.proposta (
    id bigint NOT NULL,
    aprovacaocobranca boolean,
    aprovacaoentrega boolean,
    aprovacaogerente boolean,
    atendimentocodigo bigint,
    empresacnpj character varying(255),
    entregapesquisaemail boolean,
    enviaemail boolean,
    estoquetipo character varying(50),
    liberacaoextra boolean,
    propostacodigo bigint,
    propostaobservacao character varying(1000),
    propostavalor double precision,
    id_cliente_svs bigint,
    id_unidade_organizacional bigint,
    id_veiculo bigint
);


ALTER TABLE public.proposta OWNER TO dbroot;

--
-- Name: referenciabancaria; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.referenciabancaria (
    id bigint NOT NULL,
    agencia character varying(200),
    clientedesde timestamp without time zone,
    conta character varying(200),
    contato character varying(200),
    digitoagencia character varying(200),
    digitoconta character varying(200),
    nome character varying(200),
    telefonecontato character varying(200),
    tipoconta character varying(255),
    id_banco bigint,
    id_cidade bigint,
    id_estado bigint
);


ALTER TABLE public.referenciabancaria OWNER TO dbroot;

--
-- Name: referenciapessoal; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.referenciapessoal (
    id bigint NOT NULL,
    nome character varying(200),
    relacionamento character varying(200),
    telefonecelular character varying(200),
    telefonefixo character varying(200)
);


ALTER TABLE public.referenciapessoal OWNER TO dbroot;

--
-- Name: regra_spam_departamentos; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.regra_spam_departamentos (
    regraspamsvs_id bigint,
    departamento character varying(255)
);


ALTER TABLE public.regra_spam_departamentos OWNER TO dbroot;

--
-- Name: regra_spam_fontes_lead; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.regra_spam_fontes_lead (
    regra_id bigint NOT NULL,
    fonte_lead_id bigint NOT NULL
);


ALTER TABLE public.regra_spam_fontes_lead OWNER TO dbroot;

--
-- Name: regra_spam_tipos_regra; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.regra_spam_tipos_regra (
    regraspamsvs_id bigint,
    tipo_regra character varying(255)
);


ALTER TABLE public.regra_spam_tipos_regra OWNER TO dbroot;

--
-- Name: regra_spam_unidades; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.regra_spam_unidades (
    regra_id bigint NOT NULL,
    unidade_id bigint NOT NULL
);


ALTER TABLE public.regra_spam_unidades OWNER TO dbroot;

--
-- Name: regraspamsvs; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.regraspamsvs (
    id bigint NOT NULL,
    ativo boolean,
    escopo character varying(255),
    filtro character varying(255),
    descricao character varying(500),
    id_motivo_descarte bigint
);


ALTER TABLE public.regraspamsvs OWNER TO dbroot;

--
-- Name: seq_alternativa_resposta; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_alternativa_resposta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_alternativa_resposta OWNER TO dbroot;

--
-- Name: seq_andamentooperacao; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_andamentooperacao
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_andamentooperacao OWNER TO dbroot;

--
-- Name: seq_ano_mod_comb_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_ano_mod_comb_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_ano_mod_comb_santander OWNER TO dbroot;

--
-- Name: seq_anunciosvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_anunciosvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_anunciosvs OWNER TO dbroot;

--
-- Name: seq_aprovacaobancaria; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_aprovacaobancaria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_aprovacaobancaria OWNER TO dbroot;

--
-- Name: seq_atividadeeconomicasantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_atividadeeconomicasantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_atividadeeconomicasantander OWNER TO dbroot;

--
-- Name: seq_atividadeleadsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_atividadeleadsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_atividadeleadsvs OWNER TO dbroot;

--
-- Name: seq_avalista; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_avalista
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_avalista OWNER TO dbroot;

--
-- Name: seq_banco; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_banco
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_banco OWNER TO dbroot;

--
-- Name: seq_bancosantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_bancosantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_bancosantander OWNER TO dbroot;

--
-- Name: seq_calculorentabilidade; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_calculorentabilidade
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_calculorentabilidade OWNER TO dbroot;

--
-- Name: seq_campoparsesvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_campoparsesvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_campoparsesvs OWNER TO dbroot;

--
-- Name: seq_cidade; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_cidade
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_cidade OWNER TO dbroot;

--
-- Name: seq_cliente; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_cliente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_cliente OWNER TO dbroot;

--
-- Name: seq_cnpj_inst_financ; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_cnpj_inst_financ
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_cnpj_inst_financ OWNER TO dbroot;

--
-- Name: seq_configuracaoparsesvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_configuracaoparsesvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_configuracaoparsesvs OWNER TO dbroot;

--
-- Name: seq_conjuge; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_conjuge
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_conjuge OWNER TO dbroot;

--
-- Name: seq_dominioemailsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_dominioemailsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_dominioemailsvs OWNER TO dbroot;

--
-- Name: seq_emailblacklist; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_emailblacklist
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_emailblacklist OWNER TO dbroot;

--
-- Name: seq_endereco; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_endereco
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_endereco OWNER TO dbroot;

--
-- Name: seq_entidade_teste; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_entidade_teste
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_entidade_teste OWNER TO dbroot;

--
-- Name: seq_estado; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_estado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_estado OWNER TO dbroot;

--
-- Name: seq_estado_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_estado_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_estado_santander OWNER TO dbroot;

--
-- Name: seq_estadocivilsantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_estadocivilsantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_estadocivilsantander OWNER TO dbroot;

--
-- Name: seq_faturamentoclientesvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_faturamentoclientesvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_faturamentoclientesvs OWNER TO dbroot;

--
-- Name: seq_financiamentoonlineitau; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_financiamentoonlineitau
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_financiamentoonlineitau OWNER TO dbroot;

--
-- Name: seq_financiamentoonlinesantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_financiamentoonlinesantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_financiamentoonlinesantander OWNER TO dbroot;

--
-- Name: seq_fonteleadsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_fonteleadsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_fonteleadsvs OWNER TO dbroot;

--
-- Name: seq_formapagamentosantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_formapagamentosantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_formapagamentosantander OWNER TO dbroot;

--
-- Name: seq_fornecedorclientesvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_fornecedorclientesvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_fornecedorclientesvs OWNER TO dbroot;

--
-- Name: seq_grauparentescosantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_grauparentescosantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_grauparentescosantander OWNER TO dbroot;

--
-- Name: seq_hist_int_leads_icarros; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_hist_int_leads_icarros
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_hist_int_leads_icarros OWNER TO dbroot;

--
-- Name: seq_historicoalteracao; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_historicoalteracao
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_historicoalteracao OWNER TO dbroot;

--
-- Name: seq_historicoleadsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_historicoleadsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_historicoleadsvs OWNER TO dbroot;

--
-- Name: seq_icarrostoken; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_icarrostoken
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_icarrostoken OWNER TO dbroot;

--
-- Name: seq_icarrosusuario; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_icarrosusuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_icarrosusuario OWNER TO dbroot;

--
-- Name: seq_imovelpatrimonio; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_imovelpatrimonio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_imovelpatrimonio OWNER TO dbroot;

--
-- Name: seq_instituicaofinanceira; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_instituicaofinanceira
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_instituicaofinanceira OWNER TO dbroot;

--
-- Name: seq_itemcbc; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_itemcbc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_itemcbc OWNER TO dbroot;

--
-- Name: seq_json_credit_an_itau; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_json_credit_an_itau
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_json_credit_an_itau OWNER TO dbroot;

--
-- Name: seq_json_ident_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_json_ident_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_json_ident_santander OWNER TO dbroot;

--
-- Name: seq_json_preanalise_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_json_preanalise_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_json_preanalise_santander OWNER TO dbroot;

--
-- Name: seq_json_req_sim_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_json_req_sim_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_json_req_sim_santander OWNER TO dbroot;

--
-- Name: seq_json_sim_resp_itau; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_json_sim_resp_itau
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_json_sim_resp_itau OWNER TO dbroot;

--
-- Name: seq_json_transac_req_itau; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_json_transac_req_itau
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_json_transac_req_itau OWNER TO dbroot;

--
-- Name: seq_jsonrequisicaoproposta; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_jsonrequisicaoproposta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_jsonrequisicaoproposta OWNER TO dbroot;

--
-- Name: seq_jsonrespostaproposta; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_jsonrespostaproposta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_jsonrespostaproposta OWNER TO dbroot;

--
-- Name: seq_jsonrespostasimulacaosantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_jsonrespostasimulacaosantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_jsonrespostasimulacaosantander OWNER TO dbroot;

--
-- Name: seq_leadsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_leadsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_leadsvs OWNER TO dbroot;

--
-- Name: seq_leadsvsenriquecido; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_leadsvsenriquecido
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_leadsvsenriquecido OWNER TO dbroot;

--
-- Name: seq_marcasantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_marcasantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_marcasantander OWNER TO dbroot;

--
-- Name: seq_masc_taxa_tabela_fin; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_masc_taxa_tabela_fin
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_masc_taxa_tabela_fin OWNER TO dbroot;

--
-- Name: seq_modelo_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_modelo_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_modelo_santander OWNER TO dbroot;

--
-- Name: seq_modeloquestionario; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_modeloquestionario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_modeloquestionario OWNER TO dbroot;

--
-- Name: seq_modeloveiculo; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_modeloveiculo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_modeloveiculo OWNER TO dbroot;

--
-- Name: seq_motivoaprovacaosvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_motivoaprovacaosvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_motivoaprovacaosvs OWNER TO dbroot;

--
-- Name: seq_motivodescartesvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_motivodescartesvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_motivodescartesvs OWNER TO dbroot;

--
-- Name: seq_municipio; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_municipio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_municipio OWNER TO dbroot;

--
-- Name: seq_municipio_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_municipio_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_municipio_santander OWNER TO dbroot;

--
-- Name: seq_nacionalidadesantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_nacionalidadesantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_nacionalidadesantander OWNER TO dbroot;

--
-- Name: seq_naturezajuridicasantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_naturezajuridicasantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_naturezajuridicasantander OWNER TO dbroot;

--
-- Name: seq_notaleadsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_notaleadsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_notaleadsvs OWNER TO dbroot;

--
-- Name: seq_ocupacao; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_ocupacao
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_ocupacao OWNER TO dbroot;

--
-- Name: seq_opcaoitemcbc; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_opcaoitemcbc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_opcaoitemcbc OWNER TO dbroot;

--
-- Name: seq_opcionalveiculo; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_opcionalveiculo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_opcionalveiculo OWNER TO dbroot;

--
-- Name: seq_operacaofinanciada; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_operacaofinanciada
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_operacaofinanciada OWNER TO dbroot;

--
-- Name: seq_outrarendaclientesvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_outrarendaclientesvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_outrarendaclientesvs OWNER TO dbroot;

--
-- Name: seq_parcelaentrada; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_parcelaentrada
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_parcelaentrada OWNER TO dbroot;

--
-- Name: seq_perfilsvs; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_perfilsvs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_perfilsvs OWNER TO dbroot;

--
-- Name: seq_perg_questionario; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_perg_questionario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_perg_questionario OWNER TO dbroot;

--
-- Name: seq_porteempresasantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_porteempresasantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_porteempresasantander OWNER TO dbroot;

--
-- Name: seq_produtofinanceiro; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_produtofinanceiro
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_produtofinanceiro OWNER TO dbroot;

--
-- Name: seq_produtointeresse; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_produtointeresse
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_produtointeresse OWNER TO dbroot;

--
-- Name: seq_profissaosantander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_profissaosantander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_profissaosantander OWNER TO dbroot;

--
-- Name: seq_proposta; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_proposta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_proposta OWNER TO dbroot;

--
-- Name: seq_referenciabancaria; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_referenciabancaria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_referenciabancaria OWNER TO dbroot;

--
-- Name: seq_referenciapessoal; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_referenciapessoal
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_referenciapessoal OWNER TO dbroot;

--
-- Name: seq_regraspam; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_regraspam
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_regraspam OWNER TO dbroot;

--
-- Name: seq_tabelafinanceira; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tabelafinanceira
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tabelafinanceira OWNER TO dbroot;

--
-- Name: seq_taxa_perfil_if; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_taxa_perfil_if
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_taxa_perfil_if OWNER TO dbroot;

--
-- Name: seq_taxa_tabela_financeira; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_taxa_tabela_financeira
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_taxa_tabela_financeira OWNER TO dbroot;

--
-- Name: seq_taxaperfiltabelafinanceira; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_taxaperfiltabelafinanceira
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_taxaperfiltabelafinanceira OWNER TO dbroot;

--
-- Name: seq_telefone; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_telefone
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_telefone OWNER TO dbroot;

--
-- Name: seq_tipo_ativ_ec_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipo_ativ_ec_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipo_ativ_ec_santander OWNER TO dbroot;

--
-- Name: seq_tipo_doc_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipo_doc_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipo_doc_santander OWNER TO dbroot;

--
-- Name: seq_tipo_rel_emp_santander; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipo_rel_emp_santander
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipo_rel_emp_santander OWNER TO dbroot;

--
-- Name: seq_tipoendereco; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipoendereco
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipoendereco OWNER TO dbroot;

--
-- Name: seq_tipopagamento; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipopagamento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipopagamento OWNER TO dbroot;

--
-- Name: seq_tipotabelafinanceira; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipotabelafinanceira
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipotabelafinanceira OWNER TO dbroot;

--
-- Name: seq_tipotelefone; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_tipotelefone
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_tipotelefone OWNER TO dbroot;

--
-- Name: seq_unidadeorganizacional; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_unidadeorganizacional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_unidadeorganizacional OWNER TO dbroot;

--
-- Name: seq_usuario; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_usuario OWNER TO dbroot;

--
-- Name: seq_veiculopatrimonio; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_veiculopatrimonio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_veiculopatrimonio OWNER TO dbroot;

--
-- Name: seq_veiculoproposta; Type: SEQUENCE; Schema: public; Owner: dbroot
--

CREATE SEQUENCE public.seq_veiculoproposta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_veiculoproposta OWNER TO dbroot;

--
-- Name: tabela_financ_unidades_v2; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tabela_financ_unidades_v2 (
    tabela_financeira_id bigint NOT NULL,
    unidade_id bigint NOT NULL
);


ALTER TABLE public.tabela_financ_unidades_v2 OWNER TO dbroot;

--
-- Name: tabelafinanceira; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tabelafinanceira (
    id bigint NOT NULL,
    ativa boolean,
    datafinal timestamp without time zone,
    datainicial timestamp without time zone,
    nome character varying(300),
    tipo character varying(255),
    valorbonus double precision,
    valorcartorio double precision,
    valorgravame double precision,
    valoroutros double precision,
    valorpercentualbonus double precision,
    valorpercentualplus double precision,
    valorplus double precision,
    valortaccobrada double precision,
    valortacdevolvida double precision,
    valorvistoria double precision,
    id_instituicao_financeira bigint,
    id_produto_financeiro bigint
);


ALTER TABLE public.tabelafinanceira OWNER TO dbroot;

--
-- Name: taxa_perfil_if; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.taxa_perfil_if (
    id bigint NOT NULL,
    taxafinal double precision,
    taxainicial double precision,
    instituicao_financeira_id bigint,
    perfil_svs_id bigint
);


ALTER TABLE public.taxa_perfil_if OWNER TO dbroot;

--
-- Name: taxa_tabela_financeira; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.taxa_tabela_financeira (
    id bigint NOT NULL,
    anofim integer,
    anoinicio integer,
    carencia integer,
    coeficiente double precision,
    parcelas integer,
    percentualentrada double precision,
    percentualrebate double precision,
    percentualtaxaretorno double precision,
    taxames double precision,
    valorrebate double precision,
    zerokm boolean,
    id_mascara bigint,
    tabela_financeira_id bigint
);


ALTER TABLE public.taxa_tabela_financeira OWNER TO dbroot;

--
-- Name: taxaperfiltabelafinanceira; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.taxaperfiltabelafinanceira (
    id bigint NOT NULL,
    taxafinal double precision,
    taxainicial double precision,
    perfil_svs_id bigint,
    tabela_financeira_id bigint
);


ALTER TABLE public.taxaperfiltabelafinanceira OWNER TO dbroot;

--
-- Name: telefone; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.telefone (
    id bigint NOT NULL,
    codigodealerworkflow bigint,
    ddd character varying(10),
    telefone character varying(200),
    id_tipo_endereco bigint,
    id_tipo_telefone bigint
);


ALTER TABLE public.telefone OWNER TO dbroot;

--
-- Name: tipo_ativ_ec_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipo_ativ_ec_santander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.tipo_ativ_ec_santander OWNER TO dbroot;

--
-- Name: tipo_doc_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipo_doc_santander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.tipo_doc_santander OWNER TO dbroot;

--
-- Name: tipo_rel_emp_santander; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipo_rel_emp_santander (
    id bigint NOT NULL,
    description character varying(100)
);


ALTER TABLE public.tipo_rel_emp_santander OWNER TO dbroot;

--
-- Name: tipoendereco; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipoendereco (
    id bigint NOT NULL,
    ativo boolean,
    codigodealerworkflow bigint,
    descricao character varying(255)
);


ALTER TABLE public.tipoendereco OWNER TO dbroot;

--
-- Name: tipopagamento; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipopagamento (
    id bigint NOT NULL,
    ativo boolean,
    codigodealerworkflow bigint,
    descricao character varying(500),
    tipo character varying(255)
);


ALTER TABLE public.tipopagamento OWNER TO dbroot;

--
-- Name: tipotabelafinanceira; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipotabelafinanceira (
    id bigint NOT NULL,
    ativo boolean,
    nome character varying(100)
);


ALTER TABLE public.tipotabelafinanceira OWNER TO dbroot;

--
-- Name: tipotelefone; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.tipotelefone (
    id bigint NOT NULL,
    ativo boolean,
    codigodealerworkflow bigint,
    descricao character varying(200)
);


ALTER TABLE public.tipotelefone OWNER TO dbroot;

--
-- Name: unidadeorganizacional; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.unidadeorganizacional (
    id bigint NOT NULL,
    cnpj character varying(100) NOT NULL,
    nomefantasia character varying(100) NOT NULL,
    ativo boolean,
    emailintegracao character varying(100),
    datainiciointegracao timestamp without time zone,
    integra boolean
);


ALTER TABLE public.unidadeorganizacional OWNER TO dbroot;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.usuario (
    id bigint NOT NULL,
    ativo boolean,
    cpf character varying(14) NOT NULL,
    login character varying(25) NOT NULL,
    nome character varying(100)
);


ALTER TABLE public.usuario OWNER TO dbroot;

--
-- Name: veiculopatrimonio; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.veiculopatrimonio (
    id bigint NOT NULL,
    anofabricacao integer,
    anomodelo integer,
    marca character varying(100),
    modelo character varying(100),
    placa character varying(100),
    valor double precision
);


ALTER TABLE public.veiculopatrimonio OWNER TO dbroot;

--
-- Name: veiculoproposta; Type: TABLE; Schema: public; Owner: dbroot
--

CREATE TABLE public.veiculoproposta (
    id bigint NOT NULL,
    adaptado boolean,
    anoinformadomanualmente boolean,
    chassi character varying(255),
    codigomodelofipe character varying(255),
    codigomodelomarca character varying(255),
    codigomodelomolicar character varying(255),
    cor character varying(50),
    descricaomarca character varying(50),
    descricaomodelo character varying(255),
    placa character varying(255),
    taxi boolean,
    veiculoanofabricacao integer,
    veiculoanomodelo integer,
    veiculocodigo character varying(255),
    veiculocorcodexterna character varying(255),
    veiculoestadocodplaca character varying(255),
    veiculoestadonomeplaca character varying(255),
    veiculokm integer,
    veiculomodeloveiculocod character varying(255),
    veiculomunicipionomeplacaibge character varying(255),
    veiculonrrenavam character varying(255),
    veiculostatus character varying(255)
);


ALTER TABLE public.veiculoproposta OWNER TO dbroot;

--
-- Data for Name: alternativarespostasvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.alternativarespostasvs (id, ordem, proximapergunta, texto, possuicomplemento, id_pergunta_questionario) FROM stdin;
\.


--
-- Data for Name: andamentooperacao; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.andamentooperacao (id, comentario, data, fase, proximafase, id_mot_aprovacao, id_operacao, id_usuario) FROM stdin;
\.


--
-- Data for Name: ano_mod_comb_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.ano_mod_comb_santander (id, description, integrationcode, id_modelo) FROM stdin;
\.


--
-- Data for Name: anunciosvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.anunciosvs (id, anofabricacao, anomodelo, comentarios, marca, modeloveiculo, placa, textoanuncio, url, valor, id_lead) FROM stdin;
1	\N	2018	Qro carro	Volkswagen	Trendline 1.0	\N		http://www.sagavw.com.br/volkswagen/gol.html?loja=saga-t-7-volkswagen&amp;utm_campaign=busca-paga-go-t7:model&amp;utm_source=google&amp;utm_medium=cpc	\N	27
2	\N	\N				\N		http://www.sagavw.com.br/contato/fale-conosco.html?loja=saga-t-7-volkswagen&amp;utm_campaign=busca-paga-go-t7:model	\N	28
3	\N	2018		Volkswagen	Trendline 1.0	\N		http://www.sagavw.com.br/volkswagen/gol.html?loja=saga-t-7-volkswagen&amp;utm_campaign=busca-paga-go-t7:model&amp;utm_source=google&amp;utm_medium=cpc	\N	29
4	\N	2018		Volkswagen	Trendline 1.0	\N		http://www.sagavw.com.br/volkswagen/gol.html	\N	32
5	\N	\N				\N		http://www.sagavw.com.br/lojas/saga-t-7-volkswagen.html	\N	33
6	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	36
7	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.000,00)	\N	68000	37
8	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)	\N	49999	38
9	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	39
10	\N	\N	\N	\N	\N	\N	2018/2019 Up! 1.0 12v E-Flex move up! 0km (R$ 48.300,00)	\N	48300	40
11	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	41
12	\N	\N	\N	\N	\N	\N	2018/2018 Polo 200 TSI Comfortline (Aut) (Flex) 0km (R$ 66.300,00)	\N	66300	42
13	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.399,00)	\N	51399	43
14	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	44
15	\N	\N	\N	\N	\N	\N	() 2019 Fox 1.6 MSI Connect (Flex) (R$ 49,990)	\N	49990	45
16	\N	\N	\N	\N	\N	\N	2018/2019 Up! 1.0 12v E-Flex move up! 0km (R$ 48.300,00)	\N	48300	46
17	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	47
18	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	48
19	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	49
20	\N	\N	\N	\N	\N	\N	2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)	\N	54990	50
21	\N	\N	\N	\N	\N	\N	2018/2018 Virtus 200 TSI Highline (Aut) (Flex) 0km (R$ 87.900,00)	\N	87900	51
22	\N	\N	\N	\N	\N	\N	2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)	\N	53990	52
23	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	\N	68990	53
24	\N	\N	\N	\N	\N	\N	() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)	\N	86390	54
25	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 66.900,00)	\N	66900	55
26	\N	\N	\N	\N	\N	\N	() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)	\N	86390	56
27	\N	\N	\N	\N	\N	\N	() 2019 Up! 1.0 12v E-Flex move up! (R$ 48,400)	\N	48400	57
28	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)	\N	49999	58
29	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	59
30	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	60
31	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	61
32	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	62
33	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	63
34	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)	\N	50999	64
35	\N	\N	\N	\N	\N	\N	() 2018 Polo 200 TSI Comfortline (Aut) (Flex) (R$ 66,500)	\N	66500	65
36	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	66
37	\N	\N	\N	\N	\N	\N	2018/2018 Polo 200 TSI Comfortline (Aut) (Flex) 0km (R$ 66.300,00)	\N	66300	67
38	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)	\N	50999	68
39	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	69
40	\N	\N	\N	\N	\N	\N	2018/2019 GOL 1.6 MSI (FLEX) 0KM (R$ 54.900,00)	\N	\N	70
41	\N	\N	\N	\N	\N	\N	2018/2018 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.990,00)	\N	49990	71
42	\N	\N	\N	\N	\N	\N	() 2018 Virtus 200 TSI Highline (Aut) (Flex) (R$ 86,990)	\N	86990	72
43	\N	\N	\N	\N	\N	\N	2018/2019 FOX 1.6 MSI XTREME (FLEX) 0KM (R$ 55.900,00)	\N	\N	73
44	\N	\N	\N	\N	\N	\N	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	\N	42900	74
45	\N	\N	\N	\N	\N	\N	() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)	\N	86390	75
46	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	76
47	\N	\N	\N	\N	\N	\N	2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)	\N	54990	77
48	\N	\N	\N	\N	\N	\N	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	\N	42900	78
49	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	79
50	\N	\N	\N	\N	\N	\N	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	\N	42900	80
51	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)	\N	50999	81
52	\N	\N	\N	\N	\N	\N	2018/2019 FOX 1.6 MSI XTREME (FLEX) 0KM (R$ 55.900,00)	\N	\N	82
53	\N	\N	\N	\N	\N	\N	2018/2018 POLO 200 TSI COMFORTLINE (AUT) (FLEX) 0KM (R$ 67.600,00)	\N	\N	83
54	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	\N	54900	84
55	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)	\N	45900	85
56	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	86
57	\N	\N	\N	\N	\N	\N	() 2019 Polo 1.0 (Flex) (R$ 54,999)	\N	54999	87
58	\N	\N	\N	\N	\N	\N	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	\N	42900	88
59	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 55.990,00)	\N	55990	89
60	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)	\N	45900	90
61	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	91
62	\N	\N	\N	\N	\N	\N	() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)	\N	45900	92
63	\N	\N	\N	\N	\N	\N	() 2019 Polo 1.0 (Flex) (R$ 53,990)	\N	53990	93
64	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	97
65	\N	\N	\N	\N	\N	\N	2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)	\N	54990	98
66	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.499,00)	\N	51499	99
67	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Highline (Flex) (Aut) 0km (R$ 79.999,00)	\N	79999	100
68	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	101
69	\N	\N	\N	\N	\N	\N	2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)	\N	53990	102
70	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	\N	68990	103
71	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	104
72	\N	\N	\N	\N	\N	\N	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 48.990,00)	\N	48990	105
73	\N	\N	\N	\N	\N	\N	() 2019 Up! 1.0 12v E-Flex move up! (R$ 48,300)	\N	48300	106
74	\N	\N	\N	\N	\N	\N	() 2019 Polo 200 TSI Comfortline (Aut) (R$ 68,000)	\N	68000	107
75	\N	\N	\N	\N	\N	\N	() 2018 Virtus 200 TSI Highline (Aut) (Flex) (R$ 87,900)	\N	87900	108
76	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	109
77	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	110
78	\N	\N	\N	\N	\N	\N	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	\N	56990	111
79	\N	\N	\N	\N	\N	\N	2018/2018 GOL 1.0 MPI CITY (FLEX) 0KM (R$ 40.900,00)	\N	40900	113
80	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	\N	68990	114
81	\N	\N	\N	\N	\N	\N	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	\N	68990	115
82	\N	\N	\N	\N	\N	\N	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	\N	40900	116
83	\N	\N	\N	\N	\N	\N	2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)	\N	53990	117
84	\N	\N	\N	\N	\N	\N	2018/2018 GOL 1.0 MPI CITY (FLEX) 0KM (R$ 40.900,00)	\N	40900	118
\.


--
-- Data for Name: aprovacao_bancaria_motivos; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.aprovacao_bancaria_motivos (aprovacao_id, motivo_id) FROM stdin;
\.


--
-- Data for Name: aprovacaobancaria; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.aprovacaobancaria (id, aprovado, comentario, data, id_calculo, id_usuario) FROM stdin;
\.


--
-- Data for Name: atividadeeconomicasantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.atividadeeconomicasantander (id, description, id_tipo) FROM stdin;
\.


--
-- Data for Name: atividadeleadsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.atividadeleadsvs (id, concluida, data, dataconclusao, datacriacao, descricao, nome, notaconclusao, tipoatividade, id_lead, id_usuario_conclusao, id_usuario_criacao) FROM stdin;
4	f	2018-08-08 16:00:19.291	\N	2018-08-08 13:03:45.145	testeeee	\N	\N	EMAIL	20	\N	\N
6	t	2018-08-09 19:23:58	2018-08-08 18:07:17.029	2018-08-08 16:27:05.861	ligar 	\N	adsfasdfasd	LIGACAO	24	\N	\N
3	t	2018-08-07 22:09:45.286	2018-08-08 18:12:54.288	2018-08-07 18:12:56.306	teste	\N	asdfasdfasdf	EMAIL	12	\N	\N
2	t	2018-08-08 21:00:34	2018-08-08 18:18:13.091	2018-08-07 18:04:09.488	TESTE	\N	asdasdfdas	LIGACAO	11	\N	\N
1	t	2018-08-07 21:46:19.253	2018-08-09 10:56:41.074	2018-08-07 17:49:37.433	TESTE	\N	TESTEEEE	LIGACAO	11	\N	\N
5	t	2018-08-08 22:22:45.076	2018-08-09 14:10:07.122	2018-08-08 16:26:40.118	TESTE de Novo Lead	\N	TESTE LUAN 01	EMAIL	24	\N	\N
8	t	2018-08-09 20:41:38	2018-08-13 14:28:52.13	2018-08-08 17:45:08.529	asdfasdfasdf	\N	adsfasdfadsfadsf	EMAIL	24	\N	\N
28	t	2018-08-10 20:48:58	2018-08-10 21:04:49.341	2018-08-10 20:48:37.4	passar trote	\N	concluindo	LIGACAO	16	\N	\N
9	t	2018-08-10 11:24:01	2018-08-09 11:25:54.533	2018-08-09 11:23:49.408	atividade	\N	concluindo 123	EMAIL	17	\N	\N
12	f	2018-08-09 20:13:14.565	\N	2018-08-09 17:15:46.011	Ligar novamente	\N	\N	TAREFA	25	\N	\N
15	f	2018-08-10 19:04:03	\N	2018-08-09 19:05:05.178	TESTE 03	\N	\N	LIGACAO	12	\N	\N
16	f	2018-08-10 19:05:41	\N	2018-08-09 19:05:17.464	TESTE 04	\N	\N	EMAIL	12	\N	\N
13	t	2018-08-10 19:03:15	2018-08-10 09:13:00.91	2018-08-09 19:03:00.444	TESTE 01	\N	concluido	EMAIL	11	\N	\N
17	t	2018-08-10 11:52:07.88	2018-08-10 09:13:20.251	2018-08-10 11:52:25.658	TESTE 01	\N	concluido	LIGACAO	11	\N	\N
18	f	2018-08-11 14:20:47	\N	2018-08-10 14:21:42.332	TESTANDO ATIVIDADE FUTURO	\N	\N	EMAIL	11	\N	\N
14	f	2018-08-09 19:03:42	\N	2018-08-09 19:03:26.656	TESTE 02	\N	\N	LIGACAO	11	\N	\N
11	t	2018-08-09 15:40:28.228	2018-08-10 14:29:39.892	2018-08-09 16:34:12.931	teste	\N	concluindo um atrasado.	LIGACAO	25	\N	\N
19	f	2018-08-11 14:31:06	\N	2018-08-10 14:30:50.951	TESTANDO FUTURAS	\N	\N	LIGACAO	25	\N	\N
20	f	2018-08-10 16:05:11	\N	2018-08-10 16:05:27.145	TESTE 	\N	\N	EMAIL	11	\N	\N
21	f	2018-08-11 16:06:03	\N	2018-08-10 16:05:59.367	aaaaaaaa	\N	\N	LIGACAO	11	\N	\N
7	t	2018-08-10 19:24:24	2018-08-10 17:29:39.627	2018-08-08 16:27:34.861	"Não há ninguém que ame a dor por si só, que a busque e queira tê-la, simplesmente por ser dor..."\nO que é Lorem Ipsum?\nLorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.\n\nPorque nós o usamos?\nÉ um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).\n\n	\N	asdfasdfasdfadfadfa	EMAIL	24	\N	\N
22	t	2018-08-16 18:06:03	2018-08-10 18:07:48	2018-08-10 18:05:55.552	TESTETESTETSTETSTET	\N	teste	EMAIL	24	\N	\N
23	f	2018-08-11 18:40:14	\N	2018-08-10 18:40:56.971	teste	\N	\N	EMAIL	11	\N	\N
24	f	2018-08-11 19:09:17	\N	2018-08-10 19:09:54.825	eteste	\N	\N	EMAIL	11	\N	\N
25	f	2018-08-30 20:43:45	\N	2018-08-10 20:45:11.434	ligar	\N	\N	EMAIL	19	\N	\N
26	f	2018-08-10 20:45:47	\N	2018-08-10 20:45:30.888	passar trote	\N	\N	LIGACAO	19	\N	\N
30	f	2018-08-16 20:11:53	\N	2018-08-11 20:12:20.074	teste	\N	\N	EMAIL	94	\N	\N
31	f	2018-08-21 20:17:20	\N	2018-08-11 20:12:44.324	teste	\N	\N	LIGACAO	94	\N	\N
33	f	2018-08-24 20:27:22	\N	2018-08-11 20:27:56.311	teste	\N	\N	LIGACAO	91	\N	\N
10	t	2018-08-29 14:10:06	2018-08-11 20:37:46.193	2018-08-09 14:10:00.469	TESTE LUAN 01	\N	tesste	LIGACAO	24	\N	\N
34	t	2018-08-11 20:27:56	2018-08-13 16:02:37.373	2018-08-11 20:28:06.036	teste	\N	finalizando	EMAIL	91	3091	\N
39	f	2018-08-13 17:34:30.282	\N	2018-08-13 17:34:14.663	teste234324234	\N	\N	EMAIL	16	\N	3091
27	t	2018-08-11 20:47:12	2018-08-13 17:34:14.955	2018-08-10 20:48:22.294	mandar email	\N	conc	EMAIL	16	3091	\N
38	t	2018-08-14 14:29:11	2018-08-14 18:15:37.566	2018-08-13 14:28:51.822	asdfasdfasdf	\N	teste	EMAIL	24	3428	\N
41	f	2018-08-16 11:43:24	\N	2018-08-15 11:43:12.008	TESTE 02	\N	\N	EMAIL	21	\N	3428
29	t	2018-08-12 11:18:56	2018-08-15 11:43:12.361	2018-08-11 11:18:52.248	teste	\N	TESTE 02	EMAIL	21	3428	\N
42	f	2018-08-15 12:28:39.613	\N	2018-08-15 12:28:36.779	TESTE	\N	\N	EMAIL	24	\N	3428
40	t	2018-08-15 18:15:53	2018-08-15 12:28:53.182	2018-08-14 18:15:37.245	teste	\N	TESTE	EMAIL	24	3428	3428
37	t	2018-08-14 17:23:21	2018-08-15 12:28:58.453	2018-08-13 14:23:49.295	teste	\N	teste	LIGACAO	91	3163	\N
43	f	2018-08-15 12:44:55.264	\N	2018-08-15 12:45:31.001	teste 01	\N	\N	EMAIL	24	\N	3428
44	f	2018-08-15 13:46:11	\N	2018-08-15 12:46:27.019	teste 02	\N	\N	LIGACAO	24	\N	3428
45	f	2018-08-15 12:51:03	\N	2018-08-15 12:46:48.989	teste 03	\N	\N	TAREFA	24	\N	3428
35	t	2018-08-30 20:31:16	2018-08-16 17:21:02.086	2018-08-11 20:32:20.192	teste	\N	teste	LIGACAO	90	3428	\N
32	t	2018-08-31 20:20:27	2018-08-16 17:22:49.668	2018-08-11 20:21:25.063	teste	\N	Cliente pediu retorno no próximo dia	LIGACAO	92	3428	\N
46	t	2018-08-16 17:21:15.431	2018-08-16 17:50:20.895	2018-08-16 17:21:01.758	TESTE	\N	teste	EMAIL	90	3163	3428
48	t	2018-08-16 17:50:08.731	2018-08-16 17:50:48.599	2018-08-16 17:50:20.601	teste	\N	teste 2	LIGACAO	90	3163	3163
36	t	2018-08-14 15:48:13	2018-08-17 17:10:54.998	2018-08-13 14:20:34.264	teste	\N	teste de nota de conclusão	LIGACAO	92	3163	\N
47	t	2018-08-17 17:22:32	2018-08-17 17:23:17.596	2018-08-16 17:22:49.365	Entrar em contato com o cliente.	\N	Nota de conclusão	LIGACAO	92	3163	3428
50	t	2018-08-17 17:21:22.915	2018-08-17 17:40:18.774	2018-08-17 17:23:17.281	Teste	\N	teste	LIGACAO	92	3163	3163
52	f	2018-08-18 12:06:28.661	\N	2018-08-18 12:07:27.112	teste italo	\N	\N	EMAIL	96	\N	3091
53	f	2018-08-20 21:11:07.288	\N	2018-08-20 21:10:53.028	teste	\N	\N	EMAIL	92	\N	3428
51	t	2018-08-18 17:23:42	2018-08-20 21:10:53.347	2018-08-17 17:40:18.454	teste	\N	teste	EMAIL	92	3428	3163
54	f	2018-08-23 14:11:09	\N	2018-08-21 14:10:57.013	sgfdsfg	\N	\N	TAREFA	11	\N	\N
55	t	2018-08-23 12:02:44	2018-08-22 09:02:39.525	2018-08-22 09:02:24.466	fdsf	\N	TEste	LIGACAO	92	\N	\N
56	f	2018-08-29 12:03:10	\N	2018-08-22 09:02:56.726	etstet	\N	\N	LIGACAO	90	\N	\N
49	t	2018-08-24 17:50:29	2018-08-22 09:02:57.042	2018-08-16 17:50:48.29	teste 2	\N	twatwt	LIGACAO	90	\N	3163
57	f	2018-08-23 12:06:54	\N	2018-08-22 09:06:45.805	yrtgrfd	\N	\N	LIGACAO	113	\N	\N
58	f	2018-08-24 12:08:08	\N	2018-08-22 09:07:49.4	dcg	\N	\N	LIGACAO	113	\N	\N
59	f	2018-08-23 12:11:58	\N	2018-08-22 09:11:38.469	asdfghl	\N	\N	TAREFA	116	\N	\N
60	f	2018-08-22 17:30:50.989	\N	2018-08-22 14:30:38.169	teste	\N	\N	EMAIL	113	\N	3428
61	f	2018-08-22 17:31:05	\N	2018-08-22 14:31:04.762	entrar em contato com o cliente	\N	\N	LIGACAO	113	\N	3428
62	f	2018-08-23 19:02:58.032	\N	2018-08-23 16:02:47.753	asdfasdfasfd	\N	\N	EMAIL	116	\N	3428
63	f	2018-08-24 19:03:14	\N	2018-08-23 16:02:58.133	sdasdfasdfsf	\N	\N	LIGACAO	116	\N	3428
\.


--
-- Data for Name: avalista; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.avalista (id, cidadenascimento, cpf, dataemissaorg, datanascimento, email, escolaridade, estadocivil, nacionalidade, nome, nomemae, nomepai, orgaoemissordocrg, orgaoemissorrg, rg, sexo, telefonecelular, telefoneresidencial, tipoavalista, ufemissorrg, ufnascimento, uforgaoemissordocrg, conjuge_id, endereco_id, ocupacao_id) FROM stdin;
\.


--
-- Data for Name: avalista_svs_imoveis_pat; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.avalista_svs_imoveis_pat (avalista_id, imovel_id) FROM stdin;
\.


--
-- Data for Name: avalista_svs_outrasrendas; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.avalista_svs_outrasrendas (avalista_id, outrarenda_id) FROM stdin;
\.


--
-- Data for Name: avalista_svs_veiculos_pat; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.avalista_svs_veiculos_pat (avalista_id, veiculo_id) FROM stdin;
\.


--
-- Data for Name: banco; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.banco (id, ativo, descricao, numero, site) FROM stdin;
\.


--
-- Data for Name: bancosantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.bancosantander (id, description) FROM stdin;
\.


--
-- Data for Name: calculorentabilidade; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.calculorentabilidade (id, aprovacaoonline, bancointegracaoonline, coeficiente, coeficientealterado, enviadoaobanco, parcelas, percentrada, percplus, percretorno, status, valor, valorcartorio, valorentrada, valorfinanciado, valorgravame, valoroutros, valorplus, valorpmt, valorretorno, valortac, valorvistoria, id_instituicao_financ, id_mascara, id_operacao, id_tabelafinanceira) FROM stdin;
\.


--
-- Data for Name: campoparsesvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.campoparsesvs (id, campo, campoparse, tag, id_configuracao_parse) FROM stdin;
2	<td>name</td><td>#NOME</td>	NOME	#NOME	13
3	<td>phone</td><td>#PHONE</td>	TELEFONE	#PHONE	13
4	<td>email</td><td>#EMAIL</td>	EMAIL	#EMAIL	13
5	<td>comentarios</td><td>#COMENTARIO</td>	COMENTARIO	#COMENTARIO	13
6	<td>campo_ano_form</td><td>#ANOMODELO</td>	ANOMODELO	#ANOMODELO	13
7	<td>received_from_url</td><td>#URL</td>	URL	#URL	13
8	<td>campo_veiculo_form</td><td>#MARCA</td>	MARCA	#MARCA	13
9	<td>campo_versao_form</td><td>#MODELOVEICULO</td>	MODELOVEICULO	#MODELOVEICULO	13
10	<td>carro</td><td>#ANUNCIOVEICULO</td>	ANUNCIOVEICULO	#ANUNCIOVEICULO	13
11	<td>preco</td><td>#PRECO</td>	PRECO	#PRECO	13
29	teste	TELEFONE	teste	15
30	teste	EMAIL	teste	15
54	teste	TELEFONE	teste	18
55	fds	EMAIL	#TELEFONE	18
\.


--
-- Data for Name: cidade; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.cidade (id, cep, nome, estado_id) FROM stdin;
\.


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.cliente (id, cpfcnpj, email, nome, tipopessoa) FROM stdin;
\.


--
-- Data for Name: cnpj_if_prod_fin_v2; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.cnpj_if_prod_fin_v2 (cnpj_inst_financeira_id, produto_financeiro_id) FROM stdin;
\.


--
-- Data for Name: cnpj_inst_financ; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.cnpj_inst_financ (id, ativo, cnpj, razaosocial, instituicao_financeira_id) FROM stdin;
\.


--
-- Data for Name: conf_par_emails_dom; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.conf_par_emails_dom (configuracao_id, dominio_email_id) FROM stdin;
13	5
14	7
16	4
\.


--
-- Data for Name: configuracaoparsesvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.configuracaoparsesvs (id, ativo, descricao, id_fonte_lead) FROM stdin;
13	t	SO	7
14	t	Configuração Fonte teste 123	8
15	t	Configuração testenovo	9
16	t	Configuração OLX	10
17	t	Configuração ICarros	11
18	t	Configuração asdfasdf	12
\.


--
-- Data for Name: conjuge; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.conjuge (id, cidadenascimento, cpf, datanascimento, email, escolaridade, estadocivil, nacionalidade, nome, nomemae, nomepai, orgaoemissordocrg, orgaoemissorrg, rg, sexo, telefonecelular, telefoneresidencial, ufemissorrg, ufnascimento, uforgaoemissordocrg, ocupacao_id) FROM stdin;
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
raw	includeAll	ddl/change-sets/svs_00.sql	2018-08-07 14:18:49.659775	1	EXECUTED	8:e77ce1068ffa8773cf0692a65815d5cb	sql		\N	3.6.2	\N	\N	3662366304
raw	includeAll	ddl/change-sets/svs_01.sql	2018-08-07 15:04:25.121693	2	EXECUTED	8:956a5054a7d108952dbbb93b7a4327b5	sql		\N	3.6.2	\N	\N	3665102644
raw	includeAll	ddl/change-sets/svs_02.sql	2018-08-07 17:37:15.289887	3	EXECUTED	8:d819427819c02482da89a33acf2f16b5	sql		\N	3.6.2	\N	\N	3674272787
raw	includeAll	ddl/change-sets/svs_03.sql	2018-08-08 14:53:37.837466	4	EXECUTED	8:76b670b3ce45d9143c451a3a696ff290	sql		\N	3.6.2	\N	\N	3750854661
raw	includeAll	ddl/change-sets/svs_04.sql	2018-08-08 17:04:06.388895	5	EXECUTED	8:e74929c58035ee7975f6aef5f0d6a64a	sql		\N	3.6.2	\N	\N	3758683229
raw	includeAll	ddl/change-sets/svs_05.sql	2018-08-09 14:40:31.212236	6	EXECUTED	8:9b7b7b631bae6c4c1da07deb0001e643	sql		\N	3.6.2	\N	\N	3836467447
raw	includeAll	ddl/change-sets/svs_06.sql	2018-08-13 17:25:09.774536	7	EXECUTED	8:66c03aebc1f83e213254556abb232d2e	sql		\N	3.6.2	\N	\N	4191943122
raw	includeAll	ddl/change-sets/svs_07.sql	2018-08-14 17:42:28.296941	8	EXECUTED	8:ecaed220a1da011aa744946f4ed6d232	sql		\N	3.6.2	\N	\N	4268548282
raw	includeAll	ddl/change-sets/svs_08.sql	2018-08-16 13:25:38.451684	9	EXECUTED	8:04a66ed7a64d2e75951364a8a854be06	sql		\N	3.6.2	\N	\N	4425938370
raw	includeAll	ddl/change-sets/svs_fin_00.sql	2018-08-23 11:07:04.524791	11	EXECUTED	8:8b01f833d042e74efe44a05d9b0b8660	sql		\N	3.6.2	\N	\N	5033251217
raw	includeAll	ddl/change-sets/svs_09.sql	2018-08-23 09:31:14.348143	10	EXECUTED	8:5de2ab168fc2c7ed9a13f23742da252d	sql		\N	3.6.2	\N	\N	5027500897
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: dominioemailsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.dominioemailsvs (id, ativo, email) FROM stdin;
5	t	blueprintleads@searchoptics.com
6	t	luan.caracanha@saganet.com.br
7	t	luan.caracanha@saganet.com
4	f	l@l.com.br
9	t	lead@teste.com
3	t	me@you.com
11	t	leade@gamil.co
1	f	teste@teste.com2
2	f	me@you.com.br
10	f	teste@gmail.com23
12	f	t@gmail.com
13	t	emaillead@gmail.com2
8	t	emaillead@gmail.com3
\.


--
-- Data for Name: emailblacklist; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.emailblacklist (id, ativo, email) FROM stdin;
2	t	teste@gmail.com
4	t	teste@gmail.com23
5	t	italogmme@gmail.com
1	f	italogmm@gmail.com
3	f	teste@gmail.com25
6	t	emaillead@gmail.com2
\.


--
-- Data for Name: endereco; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.endereco (id, bairro, cep, codigodealerworkflow, complemento, enumtipoiptu, logradouro, nacidadedesde, noimoveldesde, numero, tipologradouro, tiporesidencia, id_municipio, id_tipo_endereco) FROM stdin;
\.


--
-- Data for Name: entidadeteste; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.entidadeteste (id, descricao) FROM stdin;
1	TESTESS
\.


--
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.estado (id, codigo, nome, sigla) FROM stdin;
\.


--
-- Data for Name: estado_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.estado_santander (id, description) FROM stdin;
\.


--
-- Data for Name: estadocivilsantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.estadocivilsantander (id, description) FROM stdin;
\.


--
-- Data for Name: faturamentoclientesvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.faturamentoclientesvs (id, datareferencia, valor) FROM stdin;
\.


--
-- Data for Name: financiamentoonlineitau; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.financiamentoonlineitau (id, id_operacao, id_json_credit_an_itau, id_json_sim_resp_itau, id_json_transac_req_itau) FROM stdin;
\.


--
-- Data for Name: financiamentoonlinesantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.financiamentoonlinesantander (id, pdfcet, status, id_calculo_rentabilidade, id_json_req_id_santander, id_json_req_prop_santander, id_json_simulacao_santander, id_json_resp_prean_santander, id_json_resp_prop_santander, id_json_resp_sim_santander) FROM stdin;
\.


--
-- Data for Name: fonteleadsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.fonteleadsvs (id, ativo, descricao) FROM stdin;
10	t	OLX
9	f	testenovo
7	t	Search Optics
8	f	Fonte teste 123
11	t	ICarros
12	t	asdfasdf
\.


--
-- Data for Name: formapagamentosantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.formapagamentosantander (id, codigoproduto, description) FROM stdin;
\.


--
-- Data for Name: fornecedorclientesvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.fornecedorclientesvs (id, nome, telefone) FROM stdin;
\.


--
-- Data for Name: grauparentescosantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.grauparentescosantander (id, description, integrationcode) FROM stdin;
\.


--
-- Data for Name: hist_int_leads_icarros; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.hist_int_leads_icarros (id, datahorafiltro, datahorainicio, quantidaderegistros, id_unidade_organizacional) FROM stdin;
1	2018-08-11 12:59:54.695	\N	58	82
2	2018-08-17 15:12:53.798	2018-08-11 09:59:54.695	15	82
3	2018-08-21 11:00:36.216	2018-08-17 15:12:53.798	5	82
4	2018-08-21 15:49:41.827	2018-08-21 11:00:36.216	1	82
\.


--
-- Data for Name: historicoalteracao; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.historicoalteracao (id, classeobjeto, data, log, objetoalteradoid, tipo, id_usuario) FROM stdin;
1	com.svs.model.centralLeads.EmailBlackList	2018-08-07 14:41:25.402	Inclusão	1	CRIACAO	\N
2	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:47.389	Inclusão	2	CRIACAO	\N
3	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:48.633	Inclusão	3	CRIACAO	\N
4	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:49.862	Inclusão	4	CRIACAO	\N
5	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:51.097	Inclusão	5	CRIACAO	\N
6	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:52.328	Inclusão	6	CRIACAO	\N
7	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:53.566	Inclusão	7	CRIACAO	\N
8	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:54.793	Inclusão	8	CRIACAO	\N
9	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:56.024	Inclusão	9	CRIACAO	\N
10	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:57.252	Inclusão	10	CRIACAO	\N
11	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:58.485	Inclusão	11	CRIACAO	\N
12	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:15:59.711	Inclusão	12	CRIACAO	\N
13	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:00.943	Inclusão	13	CRIACAO	\N
14	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:02.17	Inclusão	14	CRIACAO	\N
15	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:03.398	Inclusão	15	CRIACAO	\N
16	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:04.629	Inclusão	16	CRIACAO	\N
17	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:05.865	Inclusão	17	CRIACAO	\N
18	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:07.095	Inclusão	18	CRIACAO	\N
19	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:08.694	Inclusão	19	CRIACAO	\N
20	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:09.92	Inclusão	20	CRIACAO	\N
21	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:11.149	Inclusão	21	CRIACAO	\N
22	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:12.379	Inclusão	22	CRIACAO	\N
23	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:13.61	Inclusão	23	CRIACAO	\N
24	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:14.932	Inclusão	24	CRIACAO	\N
25	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:16.164	Inclusão	25	CRIACAO	\N
26	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:17.394	Inclusão	26	CRIACAO	\N
27	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:18.629	Inclusão	27	CRIACAO	\N
28	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:19.824	Inclusão	28	CRIACAO	\N
29	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:21.012	Inclusão	29	CRIACAO	\N
30	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:22.2	Inclusão	30	CRIACAO	\N
31	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:23.398	Inclusão	31	CRIACAO	\N
32	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:24.589	Inclusão	32	CRIACAO	\N
33	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:25.774	Inclusão	33	CRIACAO	\N
34	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:26.961	Inclusão	34	CRIACAO	\N
35	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:28.149	Inclusão	35	CRIACAO	\N
36	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:29.358	Inclusão	36	CRIACAO	\N
37	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:30.556	Inclusão	37	CRIACAO	\N
38	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:31.752	Inclusão	38	CRIACAO	\N
39	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:32.944	Inclusão	39	CRIACAO	\N
40	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:34.145	Inclusão	40	CRIACAO	\N
41	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:35.348	Inclusão	41	CRIACAO	\N
42	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:36.536	Inclusão	42	CRIACAO	\N
43	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:37.733	Inclusão	43	CRIACAO	\N
44	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:39.306	Inclusão	44	CRIACAO	\N
45	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:40.498	Inclusão	45	CRIACAO	\N
46	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:41.694	Inclusão	46	CRIACAO	\N
47	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:42.894	Inclusão	47	CRIACAO	\N
48	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:44.086	Inclusão	48	CRIACAO	\N
49	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:45.277	Inclusão	49	CRIACAO	\N
50	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:46.472	Inclusão	50	CRIACAO	\N
51	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:47.661	Inclusão	51	CRIACAO	\N
52	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:48.849	Inclusão	52	CRIACAO	\N
53	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:50.419	Inclusão	53	CRIACAO	\N
54	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:51.61	Inclusão	54	CRIACAO	\N
55	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:52.797	Inclusão	55	CRIACAO	\N
56	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:53.999	Inclusão	56	CRIACAO	\N
57	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:55.193	Inclusão	57	CRIACAO	\N
58	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:56.384	Inclusão	58	CRIACAO	\N
59	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:57.575	Inclusão	59	CRIACAO	\N
60	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:58.771	Inclusão	60	CRIACAO	\N
61	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:16:59.971	Inclusão	61	CRIACAO	\N
62	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:01.17	Inclusão	62	CRIACAO	\N
63	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:02.385	Inclusão	63	CRIACAO	\N
64	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:03.575	Inclusão	64	CRIACAO	\N
65	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:04.769	Inclusão	65	CRIACAO	\N
66	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:05.957	Inclusão	66	CRIACAO	\N
67	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:07.145	Inclusão	67	CRIACAO	\N
68	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:08.335	Inclusão	68	CRIACAO	\N
69	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:09.523	Inclusão	69	CRIACAO	\N
70	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:10.711	Inclusão	70	CRIACAO	\N
71	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:11.911	Inclusão	71	CRIACAO	\N
72	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:13.485	Inclusão	72	CRIACAO	\N
73	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:14.672	Inclusão	73	CRIACAO	\N
74	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:15.865	Inclusão	74	CRIACAO	\N
75	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:17.052	Inclusão	75	CRIACAO	\N
76	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:18.253	Inclusão	76	CRIACAO	\N
77	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:19.441	Inclusão	77	CRIACAO	\N
78	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:20.629	Inclusão	78	CRIACAO	\N
79	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:21.818	Inclusão	79	CRIACAO	\N
80	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:23.008	Inclusão	80	CRIACAO	\N
81	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:24.229	Inclusão	81	CRIACAO	\N
82	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:25.424	Inclusão	82	CRIACAO	\N
83	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:26.62	Inclusão	83	CRIACAO	\N
84	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:27.809	Inclusão	84	CRIACAO	\N
85	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:28.997	Inclusão	85	CRIACAO	\N
86	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:30.194	Inclusão	86	CRIACAO	\N
87	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:31.382	Inclusão	87	CRIACAO	\N
88	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:32.576	Inclusão	88	CRIACAO	\N
89	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:33.765	Inclusão	89	CRIACAO	\N
90	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:35.355	Inclusão	90	CRIACAO	\N
91	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:36.551	Inclusão	91	CRIACAO	\N
92	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:37.738	Inclusão	92	CRIACAO	\N
93	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:39.296	Inclusão	93	CRIACAO	\N
94	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:40.496	Inclusão	94	CRIACAO	\N
95	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:41.69	Inclusão	95	CRIACAO	\N
96	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:42.876	Inclusão	96	CRIACAO	\N
97	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:44.071	Inclusão	97	CRIACAO	\N
98	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:45.256	Inclusão	98	CRIACAO	\N
99	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:46.444	Inclusão	99	CRIACAO	\N
100	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:47.642	Inclusão	100	CRIACAO	\N
101	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:48.828	Inclusão	101	CRIACAO	\N
102	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:50.015	Inclusão	102	CRIACAO	\N
103	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:51.201	Inclusão	103	CRIACAO	\N
104	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:52.392	Inclusão	104	CRIACAO	\N
105	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:53.59	Inclusão	105	CRIACAO	\N
106	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:54.781	Inclusão	106	CRIACAO	\N
107	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:55.968	Inclusão	107	CRIACAO	\N
108	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:57.159	Inclusão	108	CRIACAO	\N
109	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:58.359	Inclusão	109	CRIACAO	\N
110	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:17:59.553	Inclusão	110	CRIACAO	\N
111	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:00.738	Inclusão	111	CRIACAO	\N
112	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:01.924	Inclusão	112	CRIACAO	\N
113	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:03.116	Inclusão	113	CRIACAO	\N
114	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:04.308	Inclusão	114	CRIACAO	\N
115	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:05.497	Inclusão	115	CRIACAO	\N
116	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:06.686	Inclusão	116	CRIACAO	\N
117	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:07.873	Inclusão	117	CRIACAO	\N
118	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:09.087	Inclusão	118	CRIACAO	\N
119	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:10.276	Inclusão	119	CRIACAO	\N
120	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:11.465	Inclusão	120	CRIACAO	\N
121	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:12.649	Inclusão	121	CRIACAO	\N
122	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:13.847	Inclusão	122	CRIACAO	\N
123	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:15.416	Inclusão	123	CRIACAO	\N
124	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:16.605	Inclusão	124	CRIACAO	\N
125	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:17.799	Inclusão	125	CRIACAO	\N
126	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:18.985	Inclusão	126	CRIACAO	\N
127	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:20.174	Inclusão	127	CRIACAO	\N
128	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:21.361	Inclusão	128	CRIACAO	\N
129	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:22.552	Inclusão	129	CRIACAO	\N
130	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:23.744	Inclusão	130	CRIACAO	\N
131	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:24.932	Inclusão	131	CRIACAO	\N
132	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:26.119	Inclusão	132	CRIACAO	\N
133	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:27.304	Inclusão	133	CRIACAO	\N
134	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:28.496	Inclusão	134	CRIACAO	\N
135	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:29.688	Inclusão	135	CRIACAO	\N
136	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:30.877	Inclusão	136	CRIACAO	\N
137	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:32.065	Inclusão	137	CRIACAO	\N
138	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:33.263	Inclusão	138	CRIACAO	\N
139	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:34.452	Inclusão	139	CRIACAO	\N
140	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:35.641	Inclusão	140	CRIACAO	\N
141	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:36.828	Inclusão	141	CRIACAO	\N
142	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:38.017	Inclusão	142	CRIACAO	\N
143	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:39.202	Inclusão	143	CRIACAO	\N
144	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:40.393	Inclusão	144	CRIACAO	\N
145	com.svs.model.commom.UnidadeOrganizacional	2018-08-07 16:18:41.577	Inclusão	145	CRIACAO	\N
155	com.svs.model.centralLeads.LeadSvs	2018-08-07 17:38:16.979	Inclusão	10	CRIACAO	\N
156	com.svs.model.centralLeads.LeadSvs	2018-08-07 17:44:30.105	Inclusão	11	CRIACAO	\N
157	com.svs.model.centralLeads.LeadSvs	2018-08-07 17:48:47.953	Inclusão	12	CRIACAO	\N
158	com.svs.model.centralLeads.LeadSvs	2018-08-07 17:50:44.777	Inclusão	13	CRIACAO	\N
159	com.svs.model.centralLeads.LeadSvs	2018-08-07 21:05:53.618	Inclusão	14	CRIACAO	\N
160	com.svs.model.centralLeads.LeadSvs	2018-08-07 21:06:32.573	Inclusão	15	CRIACAO	\N
161	com.svs.model.centralLeads.LeadSvs	2018-08-07 21:07:01.472	Inclusão	16	CRIACAO	\N
162	com.svs.model.centralLeads.LeadSvs	2018-08-07 21:54:35.897	Inclusão	17	CRIACAO	\N
163	com.svs.model.centralLeads.LeadSvs	2018-08-07 22:05:33.693	Inclusão	18	CRIACAO	\N
164	com.svs.model.centralLeads.LeadSvs	2018-08-07 22:11:26.447	Inclusão	19	CRIACAO	\N
165	com.svs.model.centralLeads.LeadSvs	2018-08-07 22:35:25.766	Lead descartado	19	ALTERACAO	\N
166	com.svs.model.centralLeads.LeadSvs	2018-08-07 23:02:38.356	Inclusão	20	CRIACAO	\N
167	com.svs.model.centralLeads.LeadSvs	2018-08-07 23:17:24.852	Inclusão	21	CRIACAO	\N
168	com.svs.model.centralLeads.LeadSvs	2018-08-08 07:59:15.356	Inclusão	22	CRIACAO	\N
169	com.svs.model.centralLeads.LeadSvs	2018-08-08 08:25:12.45	[Responsável] de 'karolina.santos' para 'ITALO GUSTAVO MIRANDA MELO' 	20	ALTERACAO	\N
170	com.svs.model.centralLeads.LeadSvs	2018-08-08 08:25:23.738	[Responsável] de 'MATHEUS FELIPE DARLAN DE SOUSA' para 'ITALO GUSTAVO MIRANDA MELO' 	13	ALTERACAO	\N
171	com.svs.model.centralLeads.LeadSvs	2018-08-08 08:25:26.411	[Responsável] de 'MATHEUS FELIPE DARLAN DE SOUSA' para 'karolina.santos' 	13	ALTERACAO	\N
172	com.svs.model.centralLeads.LeadSvs	2018-08-08 08:25:40.004	[Responsável] de 'karolina.santos' para 'ITALO GUSTAVO MIRANDA MELO' 	13	ALTERACAO	\N
173	com.svs.model.centralLeads.LeadSvs	2018-08-08 08:28:42.22	[Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'karolina.santos' 	20	ALTERACAO	\N
174	com.svs.model.centralLeads.LeadSvs	2018-08-08 08:28:57.226	[Unidade organizacional] de 'Saga T 7' para 'Saga London' 	13	ALTERACAO	\N
175	com.svs.model.centralLeads.LeadSvs	2018-08-08 13:22:02.66	Inclusão	23	CRIACAO	\N
176	com.svs.model.centralLeads.LeadSvs	2018-08-08 13:26:47.676	Lead descartado	23	ALTERACAO	\N
177	com.svs.model.centralLeads.LeadSvs	2018-08-08 13:31:42.03	[Responsável] de 'karolina.santos' para 'LUAN CARLOS SANTANA BACO CARACANHA' 	17	ALTERACAO	\N
178	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:31:29.89	[Status] de 'PRE_LEAD' para '', [E-mail] de '' para teste@gmail.com, [Telefone] de '' para (11) 11111-1111, [Nome completo] de '' para teste, [Cpf cliente] de '' para 111.111.11-11 	11	ALTERACAO	\N
179	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:34:47.08	[Status] de 'PRE_LEAD' para '', [E-mail] de '' para teste@gmail.com, [Telefone] de '' para (11) 11111-1111, [Cpf cliente] de '' para 111.111.11-11 	12	ALTERACAO	\N
180	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:36:06.304	[Status] de 'PRE_LEAD' para '', [Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'LUAN CARLOS SANTANA BACO CARACANHA' 	13	ALTERACAO	\N
181	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:36:23.81	[Status] de 'PRE_LEAD' para '', [Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'karolina.santos' 	13	ALTERACAO	\N
182	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:36:54.434	[Status] de 'PRE_LEAD' para '', [E-mail] de '' para teste@gmail.com, [Telefone] de '' para (62) 62626-2662, [Cpf cliente] de '' para 123.456.87-90 	17	ALTERACAO	\N
183	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:43:17.63	[Unidade organizacional] de 'Saga London' para 'Seminovos GO Repasse' 	13	ALTERACAO	\N
184	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:43:32.162	[Unidade organizacional] de 'Crt Bsb' para 'Saga Motors Gyn' 	17	ALTERACAO	\N
185	com.svs.model.centralLeads.LeadSvs	2018-08-08 14:43:46.895	[Unidade organizacional] de 'Saga Motors Gyn' para 'Saga T 7' 	17	ALTERACAO	\N
186	com.svs.model.centralLeads.LeadSvs	2018-08-08 15:51:44.969	Lead descartado	22	ALTERACAO	\N
187	com.svs.model.centralLeads.LeadSvs	2018-08-08 15:54:29.41	Lead descartado	21	ALTERACAO	\N
188	com.svs.model.centralLeads.LeadSvs	2018-08-08 15:54:29.473	Lead descartado	20	ALTERACAO	\N
189	com.svs.model.centralLeads.LeadSvs	2018-08-08 16:15:37.457	[Unidade organizacional] de 'Saga T 7' para 'Seminovos GO Senador Canedo' 	12	ALTERACAO	\N
190	com.svs.model.centralLeads.LeadSvs	2018-08-08 16:15:53.712	[Unidade organizacional] de 'Saga Cidade' para 'Seminovos CBA' 	18	ALTERACAO	\N
191	com.svs.model.centralLeads.LeadSvs	2018-08-08 16:16:18.261	[Unidade organizacional] de 'Saga T 7' para 'Hyundai Sls Imports' 	17	ALTERACAO	\N
192	com.svs.model.centralLeads.LeadSvs	2018-08-08 16:25:19.359	Inclusão	24	CRIACAO	\N
193	com.svs.model.centralLeads.LeadSvs	2018-08-08 16:26:00.816	[Status] de 'PRE_LEAD' para '', [Telefone] de '(62) 62811-5352' para '(62) 66251-1626' 	24	ALTERACAO	\N
194	com.svs.model.centralLeads.LeadSvs	2018-08-08 16:26:56.131	[Status] de 'PRE_LEAD' para '' 	24	ALTERACAO	\N
195	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-08 16:34:43.589	Campo adicionado: #TELEFONE	14	ALTERACAO	\N
196	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-08 16:35:19.836	Campo adicionado: #TELEFONE	14	ALTERACAO	\N
197	com.svs.model.centralLeads.LeadSvs	2018-08-08 18:33:24.548	[Status] de 'PRE_LEAD' para '', [Telefone] de '(62) 66251-1626' para '(62) 64666-2626' 	24	ALTERACAO	\N
198	com.svs.model.centralLeads.EmailBlackList	2018-08-08 20:34:07.975	Inclusão	2	CRIACAO	\N
199	com.svs.model.centralLeads.EmailBlackList	2018-08-08 20:36:34.784	Inclusão	3	CRIACAO	\N
200	com.svs.model.centralLeads.DominioEmailSvs	2018-08-08 20:39:15.324	Inclusão	7	CRIACAO	\N
201	com.svs.model.centralLeads.DominioEmailSvs	2018-08-08 20:39:22.49	[Ativo] de 'true' para 'false' 	3	ALTERACAO	\N
202	com.svs.model.centralLeads.LeadSvs	2018-08-09 11:23:32.573	Inclusão	25	CRIACAO	\N
203	com.svs.model.centralLeads.LeadSvs	2018-08-09 11:23:58.109	[Status] de 'PRE_LEAD' para '', [Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'ITALO GUSTAVO MIRANDA MELO' 	17	ALTERACAO	\N
204	com.svs.model.centralLeads.LeadSvs	2018-08-09 12:29:51.254	[Departamento] de 'SEMINOVOS' para 'NOVOS' 	24	ALTERACAO	\N
205	com.svs.model.centralLeads.RegraSpamSvs	2018-08-09 14:24:44.574	Inclusão	1	CRIACAO	\N
206	com.svs.model.centralLeads.LeadSvs	2018-08-09 18:36:34.146	[Status] de 'PRE_LEAD' para '', [Telefone] de '' para (62) 62626-2626, [Cpf cliente] de '' para 123.456.789-09 	25	ALTERACAO	\N
207	com.svs.model.centralLeads.LeadSvs	2018-08-09 18:41:56.951	[Status] de 'PRE_LEAD' para '', [Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'karolina.santos' 	24	ALTERACAO	\N
208	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:18:07.398	Campo adicionado: teste	14	ALTERACAO	\N
209	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:18:29.946	Campo adicionado: teste	14	ALTERACAO	\N
210	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:18:42.957	Campo adicionado: teste Novo	14	ALTERACAO	\N
211	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:01.762	Campo removido: teste	14	ALTERACAO	\N
212	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:04.049	Campo removido: teste Novo	14	ALTERACAO	\N
213	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:05.658	Campo removido: #TELEFONE	14	ALTERACAO	\N
214	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:06.687	Campo removido: #TELEFONE	14	ALTERACAO	\N
215	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:07.982	Campo removido: #TELEFONE	14	ALTERACAO	\N
216	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:09.642	Campo removido: teste	14	ALTERACAO	\N
217	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:15.7	Campo removido: teste	14	ALTERACAO	\N
218	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:41:18.309	Campo removido: teste	14	ALTERACAO	\N
219	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:23.883	Campo removido: teste	14	ALTERACAO	\N
220	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:26.004	Campo removido: teste	14	ALTERACAO	\N
221	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:27.517	Campo removido: teste	14	ALTERACAO	\N
222	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:29.503	Campo removido: teste	14	ALTERACAO	\N
223	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:31.117	Campo removido: teste	14	ALTERACAO	\N
224	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:32.374	Campo removido: teste	14	ALTERACAO	\N
225	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:42:40.438	Campo adicionado: #TELEFONE	14	ALTERACAO	\N
226	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:44:36.17	Campo adicionado: #TELEFONE	14	ALTERACAO	\N
227	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:46:13.633	Campo adicionado: #TELEFONE	14	ALTERACAO	\N
228	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:46:16.283	Campo removido: #TELEFONE	14	ALTERACAO	\N
229	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:46:17.604	Campo removido: #TELEFONE	14	ALTERACAO	\N
230	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 11:46:18.683	Campo removido: #TELEFONE	14	ALTERACAO	\N
231	com.svs.model.centralLeads.FonteLeadSvs	2018-08-10 11:46:35.402	Inclusão	10	CRIACAO	\N
232	com.svs.model.centralLeads.FonteLeadSvs	2018-08-10 11:46:37.981	[Ativo] de 'true' para 'false' 	9	ALTERACAO	\N
233	com.svs.model.centralLeads.LeadSvs	2018-08-10 11:49:38.256	[Status] de 'PRE_LEAD' para '' 	24	ALTERACAO	\N
234	com.svs.model.centralLeads.LeadSvs	2018-08-10 13:23:50.23	[Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
235	com.svs.model.centralLeads.LeadSvs	2018-08-10 13:25:17.288	[Cpf cliente] de '555.150.15-66' para '' 	24	ALTERACAO	\N
236	com.svs.model.centralLeads.LeadSvs	2018-08-10 13:43:02.556	[Departamento] de 'SEMINOVOS' para 'VENDA_DIRETA', [Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
237	com.svs.model.centralLeads.LeadSvs	2018-08-10 13:46:13.411	[Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
238	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 13:54:22.019	Domínio e-mail adicionado: me@you.com.br	16	ALTERACAO	\N
239	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 13:55:31.014	Domínio e-mail adicionado: me@you.com.br	14	ALTERACAO	\N
240	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 13:59:40.397	Domínio e-mail adicionado: luan.caracanha@saganet.com	14	ALTERACAO	\N
241	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 14:03:11.386	Domínio e-mail adicionado: l@l.com.br	14	ALTERACAO	\N
242	com.svs.model.centralLeads.LeadSvs	2018-08-10 14:26:48.044	[Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'LUAN CARLOS SANTANA BACO CARACANHA', [Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
243	com.svs.model.centralLeads.LeadSvs	2018-08-10 11:34:31.203	[Status] de '' para FOX 2018, [Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'ITALO GUSTAVO MIRANDA MELO', [Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
244	com.svs.model.centralLeads.LeadSvs	2018-08-10 11:36:08.628	[Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
245	com.svs.model.centralLeads.LeadSvs	2018-08-10 11:36:34.037	[Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
246	com.svs.model.centralLeads.LeadSvs	2018-08-10 11:37:23.845	[Status] de 'FOX 2018' para 'GOL 2018', [Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
247	com.svs.model.centralLeads.LeadSvs	2018-08-10 11:38:13.223	[Cpf cliente] de '123.456.789-09' para '' 	25	ALTERACAO	\N
248	com.svs.model.centralLeads.LeadSvs	2018-08-10 12:18:11.591	[Status] de 'GOL 2018' para 'FOX 2018', 	25	ALTERACAO	\N
249	com.svs.model.centralLeads.LeadSvs	2018-08-10 12:18:44.193	[Status] de 'FOX 2018' para 'GOL 2018', 	25	ALTERACAO	\N
250	com.svs.model.centralLeads.LeadSvs	2018-08-10 12:19:07.481	[Status] de '' para GOL 2018, 	25	ALTERACAO	\N
251	com.svs.model.centralLeads.LeadSvs	2018-08-10 12:38:19.701	[Produto de interesse] de 'GOL 2018' para 'FOX 2018', 	25	ALTERACAO	\N
252	com.svs.model.centralLeads.LeadSvs	2018-08-10 12:48:16.812	[Produto de interesse] de 'FOX 2018' para 'BMW', 	25	ALTERACAO	\N
253	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 15:54:09.227	Domínio e-mail removido: me@you.com.br	14	ALTERACAO	\N
258	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 15:57:37.48	Domínio e-mail removido: me@you.com.br	16	ALTERACAO	\N
254	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 15:54:49.038	Domínio e-mail removido: l@l.com.br	14	ALTERACAO	\N
255	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 15:54:54.649	Domínio e-mail adicionado: l@l.com.br	16	ALTERACAO	\N
256	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 15:55:00.431	Domínio e-mail adicionado: luan.caracanha@saganet.com	16	ALTERACAO	\N
257	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 15:55:03.127	Domínio e-mail removido: l@l.com.br	16	ALTERACAO	\N
259	com.svs.model.centralLeads.FonteLeadSvs	2018-08-10 15:57:47.456	[Ativo] de 'true' para 'false' 	7	ALTERACAO	\N
260	com.svs.model.centralLeads.FonteLeadSvs	2018-08-10 15:57:52.151	[Ativo] de 'false' para 'true' 	7	ALTERACAO	\N
261	com.svs.model.centralLeads.FonteLeadSvs	2018-08-10 15:57:55.78	[Ativo] de 'true' para 'false' 	8	ALTERACAO	\N
262	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:37:57.381	Domínio e-mail removido: luan.caracanha@saganet.com	16	ALTERACAO	\N
263	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:38:01.187	Domínio e-mail adicionado: luan.caracanha@saganet.com	16	ALTERACAO	\N
264	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:38:09.563	Campo adicionado: #TELEFONE	16	ALTERACAO	\N
265	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:38:11.32	Campo removido: #TELEFONE	16	ALTERACAO	\N
266	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:49:25.283	Domínio e-mail adicionado: l@l.com.br	16	ALTERACAO	\N
267	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:49:26.905	Domínio e-mail removido: luan.caracanha@saganet.com	16	ALTERACAO	\N
268	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:49:33.075	Campo adicionado: teste	16	ALTERACAO	\N
269	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-10 16:49:34.657	Campo removido: teste	16	ALTERACAO	\N
270	com.svs.model.centralLeads.LeadSvs	2018-08-10 18:06:37.741	Lead descartado	24	ALTERACAO	\N
271	com.svs.model.centralLeads.LeadSvs	2018-08-10 19:30:17.546	Lead recuperado	19	ALTERACAO	\N
272	com.svs.model.centralLeads.LeadSvs	2018-08-10 19:37:55.771	Lead recuperado	21	ALTERACAO	\N
273	com.svs.model.centralLeads.LeadSvs	2018-08-10 19:39:20.498	Lead recuperado	22	ALTERACAO	\N
274	com.svs.model.centralLeads.LeadSvs	2018-08-10 19:48:11.006	[Responsável] de '' para karolina.santos 	22	ALTERACAO	\N
275	com.svs.model.centralLeads.LeadSvs	2018-08-10 19:48:27.139	[Responsável] de 'karolina.santos' para 'MARIO HAYASAKI JUNIOR', 	24	ALTERACAO	\N
276	com.svs.model.centralLeads.LeadSvs	2018-08-10 19:54:55.592	[Responsável] de 'karolina.santos' para 'LUAN CARLOS SANTANA BACO CARACANHA' 	22	ALTERACAO	\N
277	com.svs.model.centralLeads.LeadSvs	2018-08-10 20:06:15.446	[Responsável] de '' para LUAN CARLOS SANTANA BACO CARACANHA 	21	ALTERACAO	\N
278	com.svs.model.centralLeads.LeadSvs	2018-08-10 20:39:10.707	[Responsável] de 'MARIO HAYASAKI JUNIOR' para 'LUAN CARLOS SANTANA BACO CARACANHA', 	24	ALTERACAO	\N
279	com.svs.model.centralLeads.DominioEmailSvs	2018-08-10 20:41:07.011	[Ativo] de 'false' para 'true' 	3	ALTERACAO	\N
280	com.svs.model.centralLeads.DominioEmailSvs	2018-08-10 20:41:09.13	[Ativo] de 'true' para 'false' 	3	ALTERACAO	\N
281	com.svs.model.centralLeads.LeadSvs	2018-08-10 20:50:32.898	[Status] de 'PRE_LEAD' para 'LEAD', [Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'ITALO GUSTAVO MIRANDA MELO' 	21	ALTERACAO	\N
282	com.svs.model.centralLeads.LeadSvs	2018-08-10 20:50:42.854	[Status] de 'PRE_LEAD' para 'LEAD', [Unidade organizacional] de 'Saga T 7' para 'Saga Paris Gyn', 	16	ALTERACAO	\N
283	com.svs.model.centralLeads.FonteLeadSvs	2018-08-11 12:56:30.334	Inclusão	11	CRIACAO	\N
284	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:12.387	Inclusão	26	CRIACAO	\N
285	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:14.295	Inclusão	27	CRIACAO	\N
286	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:14.705	Inclusão	28	CRIACAO	\N
287	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:15.057	Inclusão	29	CRIACAO	\N
288	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:15.212	Inclusão	30	CRIACAO	\N
289	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:15.372	Inclusão	31	CRIACAO	\N
290	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:18.611	Inclusão	32	CRIACAO	\N
291	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:18.988	Inclusão	33	CRIACAO	\N
292	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:19.138	Inclusão	34	CRIACAO	\N
293	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:19.287	Inclusão	35	CRIACAO	\N
294	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:43.378	Inclusão	36	CRIACAO	\N
295	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:43.53	Inclusão	37	CRIACAO	\N
296	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:43.683	Inclusão	38	CRIACAO	\N
297	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:43.836	Inclusão	39	CRIACAO	\N
298	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:44.544	Inclusão	40	CRIACAO	\N
299	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:44.698	Inclusão	41	CRIACAO	\N
300	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:44.848	Inclusão	42	CRIACAO	\N
301	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:45.047	Inclusão	43	CRIACAO	\N
302	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:45.248	Inclusão	44	CRIACAO	\N
303	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:45.402	Inclusão	45	CRIACAO	\N
304	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:45.558	Inclusão	46	CRIACAO	\N
305	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:45.709	Inclusão	47	CRIACAO	\N
306	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:45.859	Inclusão	48	CRIACAO	\N
307	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.011	Inclusão	49	CRIACAO	\N
308	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.161	Inclusão	50	CRIACAO	\N
309	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.312	Inclusão	51	CRIACAO	\N
310	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.464	Inclusão	52	CRIACAO	\N
311	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.615	Inclusão	53	CRIACAO	\N
312	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.766	Inclusão	54	CRIACAO	\N
313	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:46.915	Inclusão	55	CRIACAO	\N
314	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.065	Inclusão	56	CRIACAO	\N
315	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.217	Inclusão	57	CRIACAO	\N
316	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.367	Inclusão	58	CRIACAO	\N
317	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.522	Inclusão	59	CRIACAO	\N
318	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.674	Inclusão	60	CRIACAO	\N
319	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.825	Inclusão	61	CRIACAO	\N
320	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:47.974	Inclusão	62	CRIACAO	\N
321	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:48.124	Inclusão	63	CRIACAO	\N
322	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:48.274	Inclusão	64	CRIACAO	\N
323	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:48.427	Inclusão	65	CRIACAO	\N
324	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:48.577	Inclusão	66	CRIACAO	\N
325	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:48.73	Inclusão	67	CRIACAO	\N
326	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:48.88	Inclusão	68	CRIACAO	\N
327	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.029	Inclusão	69	CRIACAO	\N
328	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.178	Inclusão	70	CRIACAO	\N
329	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.333	Inclusão	71	CRIACAO	\N
330	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.492	Inclusão	72	CRIACAO	\N
331	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.643	Inclusão	73	CRIACAO	\N
332	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.793	Inclusão	74	CRIACAO	\N
333	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:49.944	Inclusão	75	CRIACAO	\N
334	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.093	Inclusão	76	CRIACAO	\N
335	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.246	Inclusão	77	CRIACAO	\N
336	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.399	Inclusão	78	CRIACAO	\N
337	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.549	Inclusão	79	CRIACAO	\N
338	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.698	Inclusão	80	CRIACAO	\N
339	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.847	Inclusão	81	CRIACAO	\N
340	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:50.997	Inclusão	82	CRIACAO	\N
341	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:51.146	Inclusão	83	CRIACAO	\N
342	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:51.295	Inclusão	84	CRIACAO	\N
343	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:51.449	Inclusão	85	CRIACAO	\N
344	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:51.605	Inclusão	86	CRIACAO	\N
345	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:51.769	Inclusão	87	CRIACAO	\N
346	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:51.984	Inclusão	88	CRIACAO	\N
347	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:52.134	Inclusão	89	CRIACAO	\N
348	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:52.296	Inclusão	90	CRIACAO	\N
349	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:52.445	Inclusão	91	CRIACAO	\N
350	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:53.151	Inclusão	92	CRIACAO	\N
351	com.svs.model.centralLeads.LeadSvs	2018-08-11 12:59:53.302	Inclusão	93	CRIACAO	\N
352	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:32:43.508	[Status] de 'PRE_LEAD' para 'LEAD', [Departamento] de 'NOVOS' para 'SEMINOVOS', 	26	ALTERACAO	\N
353	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:32:56.347	[Responsável] de '' para karolina.santos, 	26	ALTERACAO	\N
354	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.113	[Status] de 'PRE_LEAD' para 'LEAD', 	93	ALTERACAO	\N
355	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.113	[Status] de 'PRE_LEAD' para 'LEAD', 	91	ALTERACAO	\N
356	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.119	[Status] de 'PRE_LEAD' para 'LEAD', 	92	ALTERACAO	\N
357	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.222	[Status] de 'PRE_LEAD' para 'LEAD', 	90	ALTERACAO	\N
358	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.243	[Status] de 'PRE_LEAD' para 'LEAD', 	88	ALTERACAO	\N
359	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.243	[Status] de 'PRE_LEAD' para 'LEAD', 	89	ALTERACAO	\N
360	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.26	[Status] de 'PRE_LEAD' para 'LEAD', 	87	ALTERACAO	\N
361	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.261	[Status] de 'PRE_LEAD' para 'LEAD', 	86	ALTERACAO	\N
362	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.266	[Status] de 'PRE_LEAD' para 'LEAD', 	85	ALTERACAO	\N
363	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.367	[Status] de 'PRE_LEAD' para 'LEAD', 	84	ALTERACAO	\N
364	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.389	[Status] de 'PRE_LEAD' para 'LEAD', 	83	ALTERACAO	\N
365	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.39	[Status] de 'PRE_LEAD' para 'LEAD', 	82	ALTERACAO	\N
366	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.406	[Status] de 'PRE_LEAD' para 'LEAD', 	81	ALTERACAO	\N
367	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.412	[Status] de 'PRE_LEAD' para 'LEAD', 	79	ALTERACAO	\N
368	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.416	[Status] de 'PRE_LEAD' para 'LEAD', 	80	ALTERACAO	\N
369	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.515	[Status] de 'PRE_LEAD' para 'LEAD', 	78	ALTERACAO	\N
370	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.535	[Status] de 'PRE_LEAD' para 'LEAD', 	77	ALTERACAO	\N
371	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.536	[Status] de 'PRE_LEAD' para 'LEAD', 	76	ALTERACAO	\N
372	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.554	[Status] de 'PRE_LEAD' para 'LEAD', 	75	ALTERACAO	\N
373	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.557	[Status] de 'PRE_LEAD' para 'LEAD', 	74	ALTERACAO	\N
374	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.56	[Status] de 'PRE_LEAD' para 'LEAD', 	73	ALTERACAO	\N
375	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.667	[Status] de 'PRE_LEAD' para 'LEAD', 	72	ALTERACAO	\N
376	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.68	[Status] de 'PRE_LEAD' para 'LEAD', 	70	ALTERACAO	\N
377	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.682	[Status] de 'PRE_LEAD' para 'LEAD', 	71	ALTERACAO	\N
378	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.703	[Status] de 'PRE_LEAD' para 'LEAD', 	68	ALTERACAO	\N
386	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.852	[Status] de 'PRE_LEAD' para 'LEAD', 	63	ALTERACAO	\N
387	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.957	[Status] de 'PRE_LEAD' para 'LEAD', 	60	ALTERACAO	\N
388	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.968	[Status] de 'PRE_LEAD' para 'LEAD', 	59	ALTERACAO	\N
392	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.998	[Status] de 'PRE_LEAD' para 'LEAD', 	57	ALTERACAO	\N
393	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.104	[Status] de 'PRE_LEAD' para 'LEAD', 	53	ALTERACAO	\N
398	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.144	[Status] de 'PRE_LEAD' para 'LEAD', 	49	ALTERACAO	\N
399	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.254	[Status] de 'PRE_LEAD' para 'LEAD', 	47	ALTERACAO	\N
404	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.289	[Status] de 'PRE_LEAD' para 'LEAD', 	44	ALTERACAO	\N
406	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.404	[Status] de 'PRE_LEAD' para 'LEAD', 	41	ALTERACAO	\N
408	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.433	[Status] de 'PRE_LEAD' para 'LEAD', 	37	ALTERACAO	\N
415	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.58	[Status] de 'PRE_LEAD' para 'LEAD', 	35	ALTERACAO	\N
416	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.693	[Status] de 'PRE_LEAD' para 'LEAD', 	29	ALTERACAO	\N
419	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.724	[Status] de 'PRE_LEAD' para 'LEAD', 	24	ALTERACAO	\N
420	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.839	[Status] de 'PRE_LEAD' para 'LEAD' 	18	ALTERACAO	\N
380	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.704	[Status] de 'PRE_LEAD' para 'LEAD', 	67	ALTERACAO	\N
383	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.826	[Status] de 'PRE_LEAD' para 'LEAD', 	64	ALTERACAO	\N
385	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.85	[Status] de 'PRE_LEAD' para 'LEAD', 	62	ALTERACAO	\N
391	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.997	[Status] de 'PRE_LEAD' para 'LEAD', 	56	ALTERACAO	\N
394	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.112	[Status] de 'PRE_LEAD' para 'LEAD', 	54	ALTERACAO	\N
396	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.142	[Status] de 'PRE_LEAD' para 'LEAD', 	50	ALTERACAO	\N
401	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.266	[Status] de 'PRE_LEAD' para 'LEAD', 	46	ALTERACAO	\N
402	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.287	[Status] de 'PRE_LEAD' para 'LEAD', 	45	ALTERACAO	\N
409	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.437	[Status] de 'PRE_LEAD' para 'LEAD', 	42	ALTERACAO	\N
412	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.551	[Status] de 'PRE_LEAD' para 'LEAD', 	31	ALTERACAO	\N
413	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.579	[Status] de 'PRE_LEAD' para 'LEAD', 	34	ALTERACAO	\N
422	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.847	[Status] de 'PRE_LEAD' para 'LEAD', 	33	ALTERACAO	\N
423	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.868	[Status] de 'PRE_LEAD' para 'LEAD' 	15	ALTERACAO	\N
379	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.703	[Status] de 'PRE_LEAD' para 'LEAD', 	69	ALTERACAO	\N
381	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.812	[Status] de 'PRE_LEAD' para 'LEAD', 	66	ALTERACAO	\N
382	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.824	[Status] de 'PRE_LEAD' para 'LEAD', 	65	ALTERACAO	\N
384	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.85	[Status] de 'PRE_LEAD' para 'LEAD', 	61	ALTERACAO	\N
389	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.969	[Status] de 'PRE_LEAD' para 'LEAD', 	58	ALTERACAO	\N
390	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:53.997	[Status] de 'PRE_LEAD' para 'LEAD', 	55	ALTERACAO	\N
395	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.112	[Status] de 'PRE_LEAD' para 'LEAD', 	51	ALTERACAO	\N
397	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.143	[Status] de 'PRE_LEAD' para 'LEAD', 	52	ALTERACAO	\N
400	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.256	[Status] de 'PRE_LEAD' para 'LEAD', 	48	ALTERACAO	\N
403	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.287	[Status] de 'PRE_LEAD' para 'LEAD', 	43	ALTERACAO	\N
405	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.4	[Status] de 'PRE_LEAD' para 'LEAD', 	39	ALTERACAO	\N
407	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.409	[Status] de 'PRE_LEAD' para 'LEAD', 	38	ALTERACAO	\N
410	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.437	[Status] de 'PRE_LEAD' para 'LEAD', 	40	ALTERACAO	\N
411	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.545	[Status] de 'PRE_LEAD' para 'LEAD', 	36	ALTERACAO	\N
414	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.579	[Status] de 'PRE_LEAD' para 'LEAD', 	30	ALTERACAO	\N
417	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.697	[Status] de 'PRE_LEAD' para 'LEAD', 	28	ALTERACAO	\N
418	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.723	[Status] de 'PRE_LEAD' para 'LEAD', 	27	ALTERACAO	\N
421	com.svs.model.centralLeads.LeadSvs	2018-08-11 14:33:54.845	[Status] de 'PRE_LEAD' para 'LEAD', 	14	ALTERACAO	\N
424	com.svs.model.centralLeads.LeadSvs	2018-08-11 19:04:41.846	Inclusão	94	CRIACAO	\N
425	com.svs.model.centralLeads.LeadSvs	2018-08-11 19:07:31.763	Lead descartado	94	ALTERACAO	\N
426	com.svs.model.centralLeads.LeadSvs	2018-08-11 19:08:52.236	Lead recuperado	94	ALTERACAO	\N
427	com.svs.model.centralLeads.LeadSvs	2018-08-11 19:24:11.422	[Status] de 'PRE_LEAD' para 'LEAD', [Responsável] de '' para karolina.santos, [E-mail] de '' para aksthose@hotmail.com, [Cpf cliente] de '' para 222.222.222-22 	94	ALTERACAO	\N
428	com.svs.model.centralLeads.LeadSvs	2018-08-11 19:30:30.833	Lead descartado	93	ALTERACAO	\N
429	com.svs.model.centralLeads.LeadSvs	2018-08-11 20:25:21.445	[Produto de interesse] de '' para FOX 2018, [Responsável] de '' para ITALO GUSTAVO MIRANDA MELO, [Telefone] de '' para (22) 22222-2222, 	92	ALTERACAO	\N
430	com.svs.model.centralLeads.LeadSvs	2018-08-11 20:31:06.844	[Produto de interesse] de '' para GOLF, [Responsável] de '' para karolina.santos, [Telefone] de '' para (22) 22222-2222, 	91	ALTERACAO	\N
432	com.svs.model.centralLeads.LeadSvs	2018-08-11 20:35:30.853	[Produto de interesse] de '' para FOX 2018, [Responsável] de '' para karolina.santos, 	90	ALTERACAO	\N
433	com.svs.model.centralLeads.LeadSvs	2018-08-13 08:58:37.041	[Status] de 'PRE_LEAD' para 'QUALIFICADO', 	11	ALTERACAO	\N
434	com.svs.model.centralLeads.LeadSvs	2018-08-13 09:06:48.987	[Status] de 'PRE_LEAD' para 'QUALIFICADO', 	11	ALTERACAO	\N
437	com.svs.model.centralLeads.LeadSvs	2018-08-13 12:59:20.715	[Produto de interesse] de '' para FOX 2018, [Responsável] de '' para ITALO GUSTAVO MIRANDA MELO, 	75	ALTERACAO	\N
438	com.svs.model.centralLeads.LeadSvs	2018-08-13 14:15:01.731	[Nome completo] de '' para Thais, 	91	ALTERACAO	\N
439	com.svs.model.centralLeads.LeadSvs	2018-08-13 14:15:30.751	[Nome completo] de 'Thais' para 'Thais Almeida', 	91	ALTERACAO	\N
440	com.svs.model.centralLeads.LeadSvs	2018-08-13 17:20:25.691	[Nome completo] de '' para Hugofernando, 	92	ALTERACAO	\N
441	com.svs.model.centralLeads.LeadSvs	2018-08-13 14:48:47.798	Lead descartado	11	ALTERACAO	3091
442	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.444	[Nome completo] de '' para Deivid Nunes Da Silva, 	90	ALTERACAO	\N
443	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.444	[Nome completo] de '' para Carlos Alberto De Santana Santos , 	88	ALTERACAO	\N
444	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.49	[Nome completo] de '' para Rosenan Bilibio, 	89	ALTERACAO	\N
445	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.491	[Nome completo] de '' para Robson Franco, 	87	ALTERACAO	\N
446	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.597	[Nome completo] de '' para Herica Da Silva Lima, 	84	ALTERACAO	\N
447	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.599	[Nome completo] de '' para Marta Paulina, 	83	ALTERACAO	\N
448	com.svs.model.centralLeads.LeadSvs	2018-08-13 18:40:39.767	[Nome completo] de '' para Geraldo Diniz, 	86	ALTERACAO	\N
449	com.svs.model.centralLeads.LeadSvs	2018-08-14 13:50:47.41	[Produto de interesse] de 'FOX 2018' para 'GOL 2018', 	92	ALTERACAO	\N
450	com.svs.model.centralLeads.LeadSvs	2018-08-14 14:31:50.216	[Produto de interesse] de 'GOL 2018' para ' Creta Pulse Aut', 	92	ALTERACAO	\N
451	com.svs.model.centralLeads.LeadSvs	2018-08-14 18:35:36.574	[Unidade organizacional] de 'Saga T 7' para 'Triumph Mototech', [Nome completo] de '' para teste 	15	ALTERACAO	\N
452	com.svs.model.centralLeads.LeadSvs	2018-08-14 18:43:59.767	[Unidade organizacional] de 'Estação Cidade' para 'Crt Bsb', [Nome completo] de '' para teste, 	21	ALTERACAO	\N
453	com.svs.model.centralLeads.LeadSvs	2018-08-14 18:59:49.195	[Unidade organizacional] de 'Saga Paris Gyn' para 'Saga Nissan Rvd', [Nome completo] de '' para teste, 	16	ALTERACAO	\N
454	com.svs.model.centralLeads.LeadSvs	2018-08-14 19:00:14.173	[Unidade organizacional] de 'Saga Nissan Rvd' para 'Saga Nissan Ana', 	16	ALTERACAO	\N
455	com.svs.model.centralLeads.LeadSvs	2018-08-14 19:06:52.852	[Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'karolina.santos', 	16	ALTERACAO	\N
456	com.svs.model.centralLeads.LeadSvs	2018-08-14 21:23:29.241	[Responsável] de 'karolina.santos' para 'LUAN CARLOS SANTANA BACO CARACANHA', [Nome completo] de '' para teste 	18	ALTERACAO	\N
457	com.svs.model.centralLeads.LeadSvs	2018-08-14 21:25:05.075	[Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'LUAN CARLOS SANTANA BACO CARACANHA', 	21	ALTERACAO	\N
458	com.svs.model.centralLeads.LeadSvs	2018-08-15 11:43:24.053	[Unidade organizacional] de 'Crt Bsb' para 'Saga T 7', 	21	ALTERACAO	\N
459	com.svs.model.centralLeads.LeadSvs	2018-08-15 11:43:42.594	[Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'ITALO GUSTAVO MIRANDA MELO', 	21	ALTERACAO	\N
460	com.svs.model.centralLeads.LeadSvs	2018-08-15 11:45:51.708	Lead descartado	21	ALTERACAO	\N
461	com.svs.model.centralLeads.LeadSvs	2018-08-15 12:30:12.786	Lead descartado	91	ALTERACAO	3163
462	com.svs.model.centralLeads.LeadSvs	2018-08-16 13:36:52.149	Inclusão	95	CRIACAO	\N
463	com.svs.model.centralLeads.LeadSvs	2018-08-16 13:37:39.133	Lead descartado	15	ALTERACAO	\N
464	com.svs.model.centralLeads.LeadSvs	2018-08-16 13:37:39.137	Lead descartado	24	ALTERACAO	\N
465	com.svs.model.centralLeads.LeadSvs	2018-08-16 13:37:39.258	Lead descartado	18	ALTERACAO	\N
466	com.svs.model.centralLeads.LeadSvs	2018-08-16 13:37:39.264	Lead descartado	95	ALTERACAO	\N
467	com.svs.model.centralLeads.LeadSvs	2018-08-16 13:38:13.961	Inclusão	96	CRIACAO	\N
468	com.svs.model.centralLeads.FonteLeadSvs	2018-08-17 10:50:04.963	Inclusão	12	CRIACAO	3428
469	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:12:37.817	Inclusão	97	CRIACAO	3428
470	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:12:52.51	Inclusão	98	CRIACAO	3428
471	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:13:09.494	Inclusão	99	CRIACAO	3428
472	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:13:20.46	Inclusão	100	CRIACAO	3428
473	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:13:31.647	Inclusão	101	CRIACAO	3428
474	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:13:43.17	Inclusão	102	CRIACAO	3428
475	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:13:53.811	Inclusão	103	CRIACAO	3428
476	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:14:06.6	Inclusão	104	CRIACAO	3428
477	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:14:20.379	Inclusão	105	CRIACAO	3428
478	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:14:33.076	Inclusão	106	CRIACAO	3428
479	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:14:50.666	Inclusão	107	CRIACAO	3428
480	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:15:08.629	Inclusão	108	CRIACAO	3428
481	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:15:20.888	Inclusão	109	CRIACAO	3428
482	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:15:40.399	Inclusão	110	CRIACAO	3428
483	com.svs.model.centralLeads.LeadSvs	2018-08-17 18:15:57.838	Inclusão	111	CRIACAO	3428
484	com.svs.model.centralLeads.LeadSvs	2018-08-17 19:10:35.594	Lead descartado	98	ALTERACAO	\N
485	com.svs.model.centralLeads.LeadSvs	2018-08-17 19:41:42.251	Lead descartado	97	ALTERACAO	\N
486	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 20:28:43.705	Campo adicionado: teste	18	ALTERACAO	\N
487	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 20:34:28.007	Campo adicionado: #TELEFONE	18	ALTERACAO	\N
488	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 20:34:38.259	Campo removido: #TELEFONE	18	ALTERACAO	\N
489	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 20:34:42.118	Campo removido: teste	18	ALTERACAO	\N
490	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 20:39:11.547	Campo adicionado: #TELEFONE	18	ALTERACAO	\N
491	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 21:03:34.867	Campo removido: #TELEFONE	18	ALTERACAO	\N
492	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-17 21:06:15.028	Campo adicionado: #TELEFONE	18	ALTERACAO	\N
493	com.svs.model.centralLeads.LeadSvs	2018-08-20 11:53:01.883	[Status] de 'PRE_LEAD' para 'LEAD', [Nome completo] de '' para Taniel Borges 	99	ALTERACAO	\N
494	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:01:06.05	Campo adicionado: teste	18	ALTERACAO	\N
495	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:01:08.64	Campo removido: teste	18	ALTERACAO	\N
496	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:10:28.14	Campo adicionado: teste	18	ALTERACAO	\N
497	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:10:30.777	Campo removido: teste	18	ALTERACAO	\N
498	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:13:38.972	Campo adicionado: #TELEFONE	18	ALTERACAO	\N
499	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:13:40.549	Campo removido: #TELEFONE	18	ALTERACAO	\N
500	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:19:21.451	Campo adicionado: teste	18	ALTERACAO	\N
501	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:20:53.222	Campo adicionado: #TELEFONE	18	ALTERACAO	\N
502	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:20:54.937	Campo removido: #TELEFONE	18	ALTERACAO	\N
503	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:22:40.276	Domínio e-mail adicionado: l@l.com.br	18	ALTERACAO	\N
504	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:32:51.315	Domínio e-mail adicionado: luan.caracanha@saganet.com	18	ALTERACAO	\N
505	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:32:53.432	Domínio e-mail removido: luan.caracanha@saganet.com	18	ALTERACAO	\N
506	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:39:14.881	Domínio e-mail adicionado: luan.caracanha@saganet.com	18	ALTERACAO	\N
507	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:39:18.646	Domínio e-mail adicionado: blueprintleads@searchoptics.com	18	ALTERACAO	\N
508	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:39:20.599	Domínio e-mail removido: l@l.com.br	18	ALTERACAO	\N
509	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:39:22.04	Domínio e-mail removido: blueprintleads@searchoptics.com	18	ALTERACAO	\N
510	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 12:39:23.413	Domínio e-mail removido: luan.caracanha@saganet.com	18	ALTERACAO	\N
511	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:11:30.942	Domínio e-mail adicionado: blueprintleads@searchoptics.com	18	ALTERACAO	\N
512	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:39.329	Domínio e-mail adicionado: luan.caracanha@saganet.com	18	ALTERACAO	\N
513	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:41.519	Domínio e-mail removido: blueprintleads@searchoptics.com	18	ALTERACAO	\N
514	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:44.086	Domínio e-mail removido: luan.caracanha@saganet.com	18	ALTERACAO	\N
515	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:48.319	Domínio e-mail adicionado: luan.caracanha@saganet.com.br	18	ALTERACAO	\N
518	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:55.032	Domínio e-mail removido: luan.caracanha@saganet.com.br	18	ALTERACAO	\N
525	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.58	Lead descartado	100	ALTERACAO	\N
516	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:52.157	Domínio e-mail adicionado: blueprintleads@searchoptics.com	18	ALTERACAO	\N
520	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:23:22.434	Campo removido: teste	18	ALTERACAO	\N
522	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:23:40.164	Domínio e-mail removido: luan.caracanha@saganet.com.br	18	ALTERACAO	\N
524	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.575	Lead descartado	101	ALTERACAO	\N
532	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.739	Lead descartado	106	ALTERACAO	\N
533	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.793	Lead descartado	107	ALTERACAO	\N
534	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.852	Lead descartado	109	ALTERACAO	\N
537	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.885	Lead descartado	111	ALTERACAO	\N
517	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:16:53.612	Domínio e-mail removido: blueprintleads@searchoptics.com	18	ALTERACAO	\N
526	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.585	Lead descartado	96	ALTERACAO	\N
529	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.704	Lead descartado	103	ALTERACAO	\N
530	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.732	Lead descartado	104	ALTERACAO	\N
538	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.889	Lead descartado	26	ALTERACAO	\N
539	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.949	Lead descartado	28	ALTERACAO	\N
540	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:07	Lead descartado	30	ALTERACAO	\N
542	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:07.035	Lead descartado	36	ALTERACAO	\N
519	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:23:21.043	Campo adicionado: teste	18	ALTERACAO	\N
521	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:23:26.567	Domínio e-mail adicionado: luan.caracanha@saganet.com.br	18	ALTERACAO	\N
523	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-20 13:23:41.7	Campo removido: #TELEFONE	18	ALTERACAO	\N
527	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.647	Lead descartado	99	ALTERACAO	\N
528	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.704	Lead descartado	102	ALTERACAO	\N
531	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.734	Lead descartado	105	ALTERACAO	\N
535	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.856	Lead descartado	108	ALTERACAO	\N
536	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:06.885	Lead descartado	110	ALTERACAO	\N
541	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:07.002	Lead descartado	27	ALTERACAO	\N
543	com.svs.model.centralLeads.LeadSvs	2018-08-20 14:16:07.037	Lead descartado	31	ALTERACAO	\N
544	com.svs.model.centralLeads.LeadSvs	2018-08-20 20:01:49.924	Lead recuperado	11	ALTERACAO	\N
545	com.svs.model.centralLeads.LeadSvs	2018-08-20 20:02:05.093	Lead descartado	11	ALTERACAO	\N
546	com.svs.model.centralLeads.LeadSvs	2018-08-20 20:02:57.891	Lead recuperado	11	ALTERACAO	\N
547	com.svs.model.centralLeads.LeadSvs	2018-08-20 20:13:35.837	[Status] de 'PRE_LEAD' para 'LEAD', 	11	ALTERACAO	\N
548	com.svs.model.centralLeads.LeadSvs	2018-08-20 20:14:24.788	[Status] de 'PRE_LEAD' para 'LEAD', 	32	ALTERACAO	\N
549	com.svs.model.centralLeads.LeadSvs	2018-08-20 20:30:28.023	[Produto de interesse] de '' para 207 SEDAN, [Responsável] de '' para karolina.santos, 	32	ALTERACAO	\N
550	com.svs.model.centralLeads.LeadSvs	2018-08-20 21:33:40.843	Inclusão	112	CRIACAO	\N
551	com.svs.model.centralLeads.LeadSvs	2018-08-20 21:43:18.997	[Status] de 'PRE_LEAD' para 'LEAD', [Produto de interesse] de '207' para '2500 LARAMIE', 	112	ALTERACAO	\N
552	com.svs.model.centralLeads.LeadSvs	2018-08-20 21:45:49.245	[Unidade organizacional] de 'Audi Cba' para 'Saga T 7' 	112	ALTERACAO	\N
553	com.svs.model.centralLeads.LeadSvs	2018-08-20 21:46:28.504	[Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'LUAN CARLOS SANTANA BACO CARACANHA', 	112	ALTERACAO	\N
554	com.svs.model.centralLeads.LeadSvs	2018-08-20 21:46:40.426	[Produto de interesse] de '2500 LARAMIE' para '3008' 	112	ALTERACAO	\N
555	com.svs.model.centralLeads.LeadSvs	2018-08-21 10:59:00.289	[Departamento] de 'NOVOS' para 'SEMINOVOS', 	112	ALTERACAO	\N
556	com.svs.model.centralLeads.LeadSvs	2018-08-21 10:59:05.977	[Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'karolina.santos', 	112	ALTERACAO	\N
557	com.svs.model.centralLeads.LeadSvs	2018-08-21 10:59:40.583	[Produto de interesse] de '3008' para '207 SEDAN', 	112	ALTERACAO	\N
558	com.svs.model.centralLeads.LeadSvs	2018-08-21 11:19:06.91	[Departamento] de 'VENDA_DIRETA' para 'SEMINOVOS', 	11	ALTERACAO	\N
559	com.svs.model.centralLeads.LeadSvs	2018-08-21 12:25:48.869	[Responsável] de '' para LUAN CARLOS SANTANA BACO CARACANHA, 	33	ALTERACAO	3163
560	com.svs.model.centralLeads.LeadSvs	2018-08-21 14:23:17.293	Lead descartado	11	ALTERACAO	\N
561	com.svs.model.centralLeads.LeadSvs	2018-08-21 14:27:13.673	Lead descartado	34	ALTERACAO	\N
562	com.svs.model.centralLeads.LeadSvs	2018-08-21 14:27:33.759	Lead descartado	35	ALTERACAO	\N
563	com.svs.model.centralLeads.LeadSvs	2018-08-21 13:11:28.634	Lead descartado	32	ALTERACAO	3091
564	com.svs.model.centralLeads.LeadSvs	2018-08-21 13:34:19.815	[Departamento] de 'SEMINOVOS' para 'VENDA_DIRETA', 	112	ALTERACAO	\N
566	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:00:50.441	[Responsável] de '' para karolina.santos, [Nome completo] de '' para Fernandes, 	37	ALTERACAO	\N
567	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:19:50.094	Lead descartado	38	ALTERACAO	\N
568	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:34:48.352	[Produto de interesse] de '' para 2500 LARAMIE, 	37	ALTERACAO	\N
569	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:45:18.504	Inclusão	113	CRIACAO	3428
570	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:45:19.208	Inclusão	114	CRIACAO	3428
571	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:45:21.682	Inclusão	115	CRIACAO	3428
572	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:45:25.545	Inclusão	116	CRIACAO	3428
573	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:45:35.696	Inclusão	117	CRIACAO	3428
574	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:49:15.888	Inclusão	118	CRIACAO	3428
576	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:54:38.007	Lead descartado	118	ALTERACAO	\N
577	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:56:45.673	[Status] de 'PRE_LEAD' para 'LEAD', [Responsável] de '' para LUAN CARLOS SANTANA BACO CARACANHA, 	113	ALTERACAO	\N
578	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:56:52.573	[Status] de 'PRE_LEAD' para 'LEAD', [Responsável] de '' para LUAN CARLOS SANTANA BACO CARACANHA, 	114	ALTERACAO	\N
579	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:57:03.94	Lead descartado	114	ALTERACAO	\N
580	com.svs.model.centralLeads.LeadSvs	2018-08-21 15:57:15.302	Lead descartado	115	ALTERACAO	\N
581	com.svs.model.centralLeads.LeadSvs	2018-08-21 16:19:52.372	Lead descartado	40	ALTERACAO	\N
582	com.svs.model.centralLeads.LeadSvs	2018-08-21 16:20:33.668	Lead descartado	33	ALTERACAO	\N
583	com.svs.model.centralLeads.LeadSvs	2018-08-21 16:20:44.487	[Status] de 'PRE_LEAD' para 'LEAD', [Unidade organizacional] de 'Saga T 7' para 'Saga Paris Bsb', 	116	ALTERACAO	\N
584	com.svs.model.centralLeads.LeadSvs	2018-08-21 16:34:37.005	[Responsável] de '' para karolina.santos, 	116	ALTERACAO	\N
585	com.svs.model.centralLeads.LeadSvs	2018-08-22 07:58:24.679	[Nome completo] de '' para João Carlos , 	48	ALTERACAO	\N
586	com.svs.model.centralLeads.LeadSvs	2018-08-22 09:42:42.599	Inclusão	119	CRIACAO	\N
587	com.svs.model.centralLeads.LeadSvs	2018-08-22 09:58:18.643	Inclusão	120	CRIACAO	\N
588	com.svs.model.centralLeads.LeadSvs	2018-08-22 10:00:11.862	Inclusão	121	CRIACAO	\N
589	com.svs.model.centralLeads.LeadSvs	2018-08-22 10:14:33.54	Lead descartado	121	ALTERACAO	3428
590	com.svs.model.centralLeads.LeadSvs	2018-08-22 10:15:16.497	Inclusão	122	CRIACAO	3428
591	com.svs.model.centralLeads.LeadSvs	2018-08-22 10:20:49.501	Lead descartado	92	ALTERACAO	3428
592	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:00:57.023	Inclusão	123	CRIACAO	\N
593	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:02:43.928	Inclusão	124	CRIACAO	\N
594	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:04:14.166	Inclusão	125	CRIACAO	\N
595	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:05:00.251	Lead descartado	120	ALTERACAO	\N
596	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:05:00.251	Lead descartado	125	ALTERACAO	\N
597	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:05:00.252	Lead descartado	123	ALTERACAO	\N
598	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:05:00.32	Lead descartado	122	ALTERACAO	\N
599	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:05:00.32	Lead descartado	124	ALTERACAO	\N
600	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:05:00.321	Lead descartado	119	ALTERACAO	\N
601	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:06:54.633	Inclusão	126	CRIACAO	\N
602	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:08:44.936	Inclusão	127	CRIACAO	\N
603	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:09:09.854	Inclusão	128	CRIACAO	\N
604	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:09:33.673	Inclusão	129	CRIACAO	\N
605	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:13:52.374	Inclusão	130	CRIACAO	\N
606	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:16:10.405	Inclusão	131	CRIACAO	\N
607	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:17:41.607	Inclusão	132	CRIACAO	\N
608	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:18:28.012	Inclusão	133	CRIACAO	\N
609	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:20:36.245	Inclusão	134	CRIACAO	\N
611	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.741	Lead descartado	129	ALTERACAO	\N
612	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.741	Lead descartado	132	ALTERACAO	\N
610	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.741	Lead descartado	134	ALTERACAO	\N
613	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.874	Lead descartado	126	ALTERACAO	\N
615	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.874	Lead descartado	133	ALTERACAO	\N
614	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.874	Lead descartado	127	ALTERACAO	\N
616	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.893	Lead descartado	131	ALTERACAO	\N
617	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.894	Lead descartado	130	ALTERACAO	\N
618	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:21:21.9	Lead descartado	128	ALTERACAO	\N
619	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:25:36.889	Inclusão	135	CRIACAO	\N
620	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:26:53.682	Inclusão	136	CRIACAO	\N
621	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:27:58.894	Inclusão	137	CRIACAO	\N
622	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:28:46.055	Inclusão	138	CRIACAO	\N
623	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:29:53.583	Inclusão	139	CRIACAO	\N
624	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:30:51.395	Inclusão	140	CRIACAO	\N
625	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:32:10.121	Inclusão	141	CRIACAO	\N
626	com.svs.model.centralLeads.LeadSvs	2018-08-22 11:33:12.45	Inclusão	142	CRIACAO	\N
627	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:19:33.594	Inclusão	143	CRIACAO	\N
628	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:34:41.348	Inclusão	144	CRIACAO	\N
629	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:38:00.971	Inclusão	145	CRIACAO	\N
630	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:38:29.334	Inclusão	146	CRIACAO	\N
631	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:39:08.396	Inclusão	147	CRIACAO	\N
632	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:39:28.307	Inclusão	148	CRIACAO	\N
633	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:51:48.786	Inclusão	149	CRIACAO	\N
634	com.svs.model.centralLeads.LeadSvs	2018-08-22 13:52:09.923	Inclusão	150	CRIACAO	\N
635	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:00:22.755	[Status] de 'PRE_LEAD' para 'LEAD', [Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'LUAN CARLOS SANTANA BACO CARACANHA', [Nome completo] de '' para dsafasdf, 	136	ALTERACAO	\N
636	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:03:54.931	Lead descartado	137	ALTERACAO	\N
637	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:05:12.037	[Status] de 'PRE_LEAD' para 'LEAD', [Responsável] de '' para MARIO HAYASAKI JUNIOR, 	117	ALTERACAO	3428
638	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:14:09.952	[Status] de 'PRE_LEAD' para 'LEAD', [Nome completo] de '' para gfdsgs 	141	ALTERACAO	\N
639	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:14:37.32	[Status] de 'PRE_LEAD' para 'LEAD', [Nome completo] de '' para fsgdf 	142	ALTERACAO	\N
640	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:15:26.249	[Responsável] de 'LUAN CARLOS SANTANA BACO CARACANHA' para 'karolina.santos', 	136	ALTERACAO	\N
641	com.svs.model.centralLeads.LeadSvs	2018-08-22 14:15:39.665	[Unidade organizacional] de 'Audi Center Udi' para 'Saga T 7' 	141	ALTERACAO	\N
642	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:08:00.757	[Status] de 'PRE_LEAD' para 'LEAD', [Nome completo] de '' para  bbbbbbbbb 	138	ALTERACAO	\N
643	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:08:03.199	[Status] de 'PRE_LEAD' para 'LEAD', [Nome completo] de '' para dfsdf 	135	ALTERACAO	\N
644	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:11:36.726	[Departamento] de 'SEMINOVOS' para 'NOVOS' 	138	ALTERACAO	\N
645	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:12:48.446	[Departamento] de 'SEMINOVOS' para 'NOVOS' 	135	ALTERACAO	\N
646	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:14:04.838	[Responsável] de 'karolina.santos' para 'LUAN CARLOS SANTANA BACO CARACANHA' 	141	ALTERACAO	\N
647	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:14:11.363	[Unidade organizacional] de 'Autominas France' para 'Crt Bsb' 	138	ALTERACAO	\N
648	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:33:57.59	Lead descartado	141	ALTERACAO	\N
649	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:34:15.543	[Departamento] de 'NOVOS' para 'SEMINOVOS' 	138	ALTERACAO	\N
650	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:34:19.477	[Responsável] de 'ITALO GUSTAVO MIRANDA MELO' para 'karolina.santos' 	135	ALTERACAO	\N
651	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:34:26.174	[Unidade organizacional] de 'Autominas France' para 'Saga T 7' 	135	ALTERACAO	\N
652	com.svs.model.centralLeads.LeadSvs	2018-08-22 15:41:17.317	Lead descartado	144	ALTERACAO	\N
653	com.svs.model.centralLeads.LeadSvs	2018-08-22 16:52:01.862	Lead descartado	146	ALTERACAO	3428
654	com.svs.model.centralLeads.LeadSvs	2018-08-23 09:30:23.783	Lead descartado	149	ALTERACAO	\N
656	com.svs.model.centralLeads.ModeloQuestionarioSvs	2018-08-23 10:11:28.427	Inclusão	2	CRIACAO	3091
657	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-23 10:47:30.394	Campo adicionado: #TELEFONE	18	ALTERACAO	\N
658	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-23 10:48:46.822	Domínio e-mail adicionado: blueprintleads@searchoptics.com	18	ALTERACAO	\N
659	com.svs.model.centralLeads.ConfiguracaoParseSvs	2018-08-23 10:48:48.34	Domínio e-mail removido: blueprintleads@searchoptics.com	18	ALTERACAO	\N
660	com.svs.model.centralLeads.LeadSvs	2018-08-23 12:46:56.684	[Status] de 'PRE_LEAD' para 'LEAD', [Unidade organizacional] de 'Audi Cba' para 'Audi Center Udi', [Nome completo] de '' para nmv 	139	ALTERACAO	\N
661	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:26:08.613	Inclusão	8	CRIACAO	\N
662	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:29:26.395	[E-mail] de 'emaillead@gmail.com' para 'emaillead@gmail.com2' 	8	ALTERACAO	\N
663	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:29:30.858	[E-mail] de 'emaillead@gmail.com2' para 'emaillead@gmail.com' 	8	ALTERACAO	\N
664	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:29:32.349	[Ativo] de 'true' para 'false' 	8	ALTERACAO	\N
665	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:31:37.28	[Ativo] de 'false' para 'true' 	8	ALTERACAO	\N
666	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:38:52.715	[E-mail] de 'teste@teste.com' para 'teste@teste.com2' 	1	ALTERACAO	\N
667	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:38:54.392	[Ativo] de 'true' para 'false' 	2	ALTERACAO	\N
668	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:38:55.608	[Ativo] de 'true' para 'false' 	4	ALTERACAO	\N
669	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:39:10.977	Inclusão	9	CRIACAO	\N
670	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:46:05.479	[Ativo] de 'false' para 'true' 	3	ALTERACAO	\N
671	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 14:46:10.34	[Ativo] de 'false' para 'true' 	2	ALTERACAO	\N
672	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:17:37.349	[E-mail] de 'teste@gmail.com1' para 'teste@gmail.com2' 	3	ALTERACAO	\N
673	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:17:38.935	[Ativo] de 'true' para 'false' 	3	ALTERACAO	\N
674	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:18:16.175	Inclusão	10	CRIACAO	\N
675	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:18:24.729	Inclusão	4	CRIACAO	\N
676	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:20:38.15	Inclusão	11	CRIACAO	\N
677	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:20:46.468	Inclusão	5	CRIACAO	\N
678	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:20:48.491	[Ativo] de 'true' para 'false' 	1	ALTERACAO	\N
679	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:24:21.101	Inclusão	12	CRIACAO	\N
680	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:24:23.463	[Ativo] de 'true' para 'false' 	1	ALTERACAO	\N
681	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:24:28.746	[E-mail] de 'teste@gmail.com2' para 'teste@gmail.com25' 	3	ALTERACAO	\N
682	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:30:07.493	[Ativo] de 'true' para 'false' 	2	ALTERACAO	\N
683	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:30:13.043	[Ativo] de 'true' para 'false' 	10	ALTERACAO	\N
684	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:30:15.878	[Ativo] de 'true' para 'false' 	12	ALTERACAO	\N
685	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:30:30.714	Inclusão	13	CRIACAO	\N
686	com.svs.model.centralLeads.DominioEmailSvs	2018-08-23 15:30:35.048	[E-mail] de 'emaillead@gmail.com' para 'emaillead@gmail.com3' 	8	ALTERACAO	\N
687	com.svs.model.centralLeads.EmailBlackList	2018-08-23 15:30:43.789	Inclusão	6	CRIACAO	\N
688	com.svs.model.centralLeads.ModeloQuestionarioSvs	2018-08-23 15:38:11.899	Pergunta adicionada: NOVA PERGUNTA	1	ALTERACAO	3091
689	com.svs.model.centralLeads.LeadSvs	2018-08-23 16:02:15.512	Lead descartado	90	ALTERACAO	3428
\.


--
-- Data for Name: historicoleadsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.historicoleadsvs (id, data, descricao, statusanterior, statusatual, id_lead, id_usuario) FROM stdin;
1	2018-08-10 19:19:52.3	Enviado para o ConnectLead	\N	\N	25	\N
2	2018-08-10 19:21:06.876	Enviado para o ConnectLead	\N	\N	17	\N
3	2018-08-10 19:22:40.133	Enviado para o ConnectLead	\N	\N	11	\N
4	2018-08-10 20:16:28.093	Enviado para o ConnectLead	\N	\N	13	\N
5	2018-08-10 20:45:19.219	Enviado para o ConnectLead	\N	\N	12	\N
6	2018-08-10 20:45:19.264	Enviado para o ConnectLead	\N	\N	19	\N
7	2018-08-11 14:34:25.648	Enviado para o ConnectLead	\N	\N	14	\N
8	2018-08-11 20:17:13.463	Enviado para o ConnectLead	\N	\N	94	\N
9	2018-08-14 21:20:31.006	Enviado para o ConnectLead	\N	\N	16	\N
10	2018-08-21 14:10:07.975	Enviado para o ConnectLead	\N	\N	112	\N
\.


--
-- Data for Name: icarrostoken; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.icarrostoken (id, accesstoken, creationdate, expiresin, idtoken, notbeforepolicy, refreshexpiresin, refreshtoken, sessionstate, tokentype, icarrosusuario_id) FROM stdin;
1	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJmY2EzZGY4ZC05ZjhlLTQwYjgtYjFlYS1iNDM5OTVlNDljZTkiLCJleHAiOjE1MzM5OTU5NTMsIm5iZiI6MCwiaWF0IjoxNTMzOTkyMzUzLCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJncnVwb3NhZ2EiLCJzZXNzaW9uX3N0YXRlIjoiMzlkZTJhZDItY2Y4YS00MTc1LTk1OGQtNmYxY2Q2N2MzYjk1IiwiY2xpZW50X3Nlc3Npb24iOiIwMjJhZjFkYi1hMmNiLTRlYjYtODQwMi0yMTFkNDRlYmMzNDAiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiaWNhcnJvcy13ZWJhcHAiOnsicm9sZXMiOlsidXN1YXJpb3NpdGUiLCJhbnVuY2lhbnRlcGoiXX19LCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.ZGTFubPGmxcaeHh1oNCH9oRODPXX-CYy0DBu552ZrLNvrCaOPEOeSWtkIY_ttKifbYNMqI0JBxmGCF4pRCzny3neSCOWe9S6J95A7OzP8L01wECEJtmO0wSIdGj21GdLT9LSWXJpJ_mMWLL13JFXB2K4fJMJzVqokzil_U5ED8H3FwUGfXs43O-8Ph0ex8eJwKEVaw0e8GOE32fDw2XqH-2l9AohC0jCxMKacG-0xfUJyHc5nWzkDooI6tF3DCPHpbIC8RBldIxBn15hfFC9wvFSbxmFyI49wSer_EDZGXgJGDQw5PfHDsGg2clubpOMc6-EFnMViLJoAXgTC5erZg	2018-08-11 12:59:48.846	3600	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJhZGJjYmZkNy0zYzFhLTQwM2EtYjJmOC05OWE5YjUzNTA5OTUiLCJleHAiOjE1MzM5OTU5NTMsIm5iZiI6MCwiaWF0IjoxNTMzOTkyMzUzLCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJJRCIsImF6cCI6ImdydXBvc2FnYSIsInNlc3Npb25fc3RhdGUiOiIzOWRlMmFkMi1jZjhhLTQxNzUtOTU4ZC02ZjFjZDY3YzNiOTUiLCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.IybaStdU0uYrFwyTG-oFPuNl7xk9AhrzbXDcWBOcRaoGBvIdFcS2MKTNPK8dlx6yXF6wLIhVcQR43IU3-DCvJjxqXdC4LTdmNpIZwUI7v-lq2FSFXs0siyElEtY-XOvzYqBeXgOPEH4cUwpAF2EgdKGKcdsHKGddk8kYUIu066rapkVYQUyZk-D1JX10IDGk4TYllLJYc5ebGJ7CkOBd5Gaxlv_DjCYOXcsisaQ_MMRm355H3p_oEokK5VV6WC0hQHYVb5_LjCWve8O0jUeOVAH-ZEmDGXDJoxev-Lt8AHwcBnB9L_3w8EV-K4Dp_509APRc2JPhfVXLnr7F2D1xZA	1412634388	5184000	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI2OWZkZmM5ZC05ZjcwLTRkMTgtYWFiNC1iNjAxYjBhNWZhOTEiLCJleHAiOjE1MzkxNzYzNTMsIm5iZiI6MCwiaWF0IjoxNTMzOTkyMzUzLCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiZ3J1cG9zYWdhIiwic2Vzc2lvbl9zdGF0ZSI6IjM5ZGUyYWQyLWNmOGEtNDE3NS05NThkLTZmMWNkNjdjM2I5NSIsImNsaWVudF9zZXNzaW9uIjoiMDIyYWYxZGItYTJjYi00ZWI2LTg0MDItMjExZDQ0ZWJjMzQwIiwicmVzb3VyY2VfYWNjZXNzIjp7ImljYXJyb3Mtd2ViYXBwIjp7InJvbGVzIjpbInVzdWFyaW9zaXRlIiwiYW51bmNpYW50ZXBqIl19fX0.ea3yg4ly1Qi2i9Q7nT98buwd3uyBYmkmTt4qX3lujVtijRU6Oyq1cFHeh4_rR7LuQfVT-5x4Naulkfxxv22g6_STcYs5gw_k-I0aUWcWXqBMvVtkeCNWTLJczaRG2Z86tqa6umHTRdrTQtbEa5HTgNDvHkONdhfm9lD6_1IZqLwzXIyXIM0ySUHRfkbueFlJK_Zl-Ec0R8aO_Dex22WUCfweSPzfc0RNrmknbIPO-Z8QRT_7lTDl8_VeKPp4wPT9eHk7UEQOtfYW78oLZF_y64-ojxqHbXPlHJfUExbUApVxLWvemZLjsS8doYN6NLPX_KOW1I4ifKLEHPJx7ZHQWw	39de2ad2-cf8a-4175-958d-6f1cd67c3b95	bearer	1
2	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1M2VjNzFlZC1hYzIwLTQwNDQtYmVlOS0xNGNkNDQzNGMxN2MiLCJleHAiOjE1MzQxODQ1NDgsIm5iZiI6MCwiaWF0IjoxNTM0MTgwOTQ4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJncnVwb3NhZ2EiLCJzZXNzaW9uX3N0YXRlIjoiN2U0MjBmZGUtZjYxZi00MTU1LWFkM2MtM2Y2Y2NkMWNhZWFiIiwiY2xpZW50X3Nlc3Npb24iOiIwZThiYWY2Yy0wZjEyLTQ1ZTYtOGVjMC1iMTNmNTUzNGUxODkiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiaWNhcnJvcy13ZWJhcHAiOnsicm9sZXMiOlsidXN1YXJpb3NpdGUiLCJhbnVuY2lhbnRlcGoiXX19LCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.Xf9BrWBZUZEu2-eEVV8snAOR0VkcCLnDm_mf7NdIMY2KwZi2nHFhb07UeNG9NK6Olru2A3RPTru87ypY8micQsP8X-6uucMmShIsT4B7n6xwoDiMTsrqHSV3vts8P0nh2kuJEWX3_TEcJt-aNg45EsJX3z3n9JHj8IosUivz4qPXB8l5pTYw2ef6m-eyCeqaugq5HG3pBbO_g3JraL7BHFEQSAZ5artH62-bjzYSorqAFIwPp0kx2hVx9LRuNxjnXq8FdKr4J4NhWkSIaag9hEFoVjz5jW_LvKqZ2xwbYPXeyDuLdpOTkjOO0F0jJNmjHgY5oIJ_l3eNcAaxc2gqJw	2018-08-13 17:23:02.861	3600	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJjNjY1MjA3NS0wM2U3LTQwMzUtOGQ3NS1jZGNhZmNmZDg1YTMiLCJleHAiOjE1MzQxODQ1NDgsIm5iZiI6MCwiaWF0IjoxNTM0MTgwOTQ4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJJRCIsImF6cCI6ImdydXBvc2FnYSIsInNlc3Npb25fc3RhdGUiOiI3ZTQyMGZkZS1mNjFmLTQxNTUtYWQzYy0zZjZjY2QxY2FlYWIiLCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.idhftWQh8svDfOZOO_rWRcFtv2ztDsgGBT0vn6TAj_BXtAKdGhhRLu4TAiFBtqC7SOjiyQZf6q6sOVJfAbgqiTHy922bvrX6H7fDLDqMGJyIlxUtM-K1-Vv8u8-ovN0zGtxY7eaT3M5OK2mBY5j2JMVW1XfrpCviK2bTKr1oHi4W6Q3hzxbEUfOXyn3kFWI9QLCIArMMXFS8sFUqbJfzin8qi2OFWFgfU3QhdkRVv6gq6X2QKZvM8ytiYGztaKC-DTfuCopmCJa9M_8dXkAoOHK8BJbFFpdyXitY8qO5seggyrObWKyGo8xN1iFW5RSPiHyFaC09yrir9lQbEOYhlg	1412634388	5184000	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI2OWYxYjE2Mi1jN2RkLTQ2NGMtOWJlMy01MjQ0NDMxODQ3OTgiLCJleHAiOjE1MzkzNjQ5NDgsIm5iZiI6MCwiaWF0IjoxNTM0MTgwOTQ4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiZ3J1cG9zYWdhIiwic2Vzc2lvbl9zdGF0ZSI6IjdlNDIwZmRlLWY2MWYtNDE1NS1hZDNjLTNmNmNjZDFjYWVhYiIsImNsaWVudF9zZXNzaW9uIjoiMGU4YmFmNmMtMGYxMi00NWU2LThlYzAtYjEzZjU1MzRlMTg5IiwicmVzb3VyY2VfYWNjZXNzIjp7ImljYXJyb3Mtd2ViYXBwIjp7InJvbGVzIjpbInVzdWFyaW9zaXRlIiwiYW51bmNpYW50ZXBqIl19fX0.LVb74ZTX5qe6eqKbMLWr_zZ46Q-BoZFbfjaqxA882psc7TM5YleSh2rL20nnJTfrzXGOuZzIoUglqC4wyQN_N9jitPxeCXUK1ArHxDu0XBmoT68Dk7VPKJ0Z5Yhw4I_oe8vp2vEw1jJJ7JyG3KRou5na5lbniahQQTdLfthMztT5yZWaux55B9eIl2kqjQElhjjzkTk8RRiSRVSJ0elTU-dGzEX0jrURi_Ipu5HC8CixoxG9SThVUiajBYSF0yKuUyQDMieOFETyLXI9kcSl9plDsM2mHFFwu2iMBRha5NjWYicbplNM4HnVgHgsMw_JQbYoa3yw7Dnyk7xptrELbA	7e420fde-f61f-4155-ad3c-3f6ccd1caeab	bearer	1
3	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJkMDVhYzA1NS0xMDA5LTRiYzEtOTg1Yi1kN2VkMWQ1OTY3YzEiLCJleHAiOjE1MzQ1MzI4MjEsIm5iZiI6MCwiaWF0IjoxNTM0NTI5MjIxLCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJncnVwb3NhZ2EiLCJzZXNzaW9uX3N0YXRlIjoiZDNkNjc3N2EtZWFlOC00YzdkLTkyYmQtNmVjYWRjZTQ0NTI3IiwiY2xpZW50X3Nlc3Npb24iOiJkNzVhMmZkNC0zM2QyLTRhMDgtYTQyNC04ZTY5YmU3ODExZmUiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiaWNhcnJvcy13ZWJhcHAiOnsicm9sZXMiOlsidXN1YXJpb3NpdGUiLCJhbnVuY2lhbnRlcGoiXX19LCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.TgeKRyVL5vAM0Vm9ZAZ2gQhFRW490kEGOEzMtvAVa4cKLkJrJEM2D_o2CQZORjMsCA_fSDqg4XB9gLfkwmptwVi0X25V3b7tLCzH4z_eobgtDJOkWTBetESnSF83nxiRXtkZS0i94gbiH9ILnh5MAXb6gj3JxJd77yCftyz_3ViPfvJgLPLXehPsrFvi6wQ22-86I5xIYcH-r1G5DKTXL5qjqQe-ZluGn_KY-LEfq9EqJtJ_XkM0xhNYyoZg4E8PWt1QDMb306x5nnaSnqfDBZCm_bN2yqkumjx_iq-Yy-YXDUeXv6Vd8NwhMn2zGtMRdruBsrzLc_k5v4yIjx7DxQ	2018-08-17 18:07:32.893	3600	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1NjZjNTAwMS1mMjA2LTRhZmEtOThmNC01ZTUwMzc0ZWI0NzkiLCJleHAiOjE1MzQ1MzI4MjEsIm5iZiI6MCwiaWF0IjoxNTM0NTI5MjIxLCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJJRCIsImF6cCI6ImdydXBvc2FnYSIsInNlc3Npb25fc3RhdGUiOiJkM2Q2Nzc3YS1lYWU4LTRjN2QtOTJiZC02ZWNhZGNlNDQ1MjciLCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.UwaNSFhT3txKdvaswVrZ-BLeNafQ06H_7O17pLKG58KEE02d51xCKhSsNILSCt-quSPkx_npkqtc3Sd50K_tEkTZ_iYhYP0ZIxxeFR2IJP5aWo3e2fldZGHUMXuc6ihpUjTKWBoWWy49vjI8UOA3W3AUuhSQ2juaLiHOF7k_O9d3-VnCmqnuaEbcgDP5arGm4NtgA-hDQoU77q1qd38jL2hRaO3aUpctJSlM5FFg9_mFW7FWgWd9EQ9HtZA1CPqUVE346lvQgQdA-PqtZ755LzRESOuLuF7YSEQqBi6-IHEfaatoQPNHh9rwuwobadZcvPSmFQ9CmHossYborarA5w	1412634388	5184000	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1ODc5ZGQ0MS05NzI0LTRjOTgtYWNiZi1jYTc3NmQ3MWU3NzUiLCJleHAiOjE1Mzk3MTMyMjEsIm5iZiI6MCwiaWF0IjoxNTM0NTI5MjIxLCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiZ3J1cG9zYWdhIiwic2Vzc2lvbl9zdGF0ZSI6ImQzZDY3NzdhLWVhZTgtNGM3ZC05MmJkLTZlY2FkY2U0NDUyNyIsImNsaWVudF9zZXNzaW9uIjoiZDc1YTJmZDQtMzNkMi00YTA4LWE0MjQtOGU2OWJlNzgxMWZlIiwicmVzb3VyY2VfYWNjZXNzIjp7ImljYXJyb3Mtd2ViYXBwIjp7InJvbGVzIjpbInVzdWFyaW9zaXRlIiwiYW51bmNpYW50ZXBqIl19fX0.TmD9JMpGmHTq3jyu6siYr7U-jWWTNG8ocCAqNQOr6zeDub69U5CyDzovBrVDvKrPtrHpBk7hN2X-rLSAKm_8bmwrefpLCuQLJNVplp0VvFWsPihoxnvbkLpAy6fsYD0QxumL57s3rAo09RQvuVRxGv_SGUCF3e2GIbZNTw57O7w-mAI22_bB5gvUckGVAih1q73scE1BoIxFa4SD3Uy22GQ1Zc0rgKl0y8X9cfkSvr9Y3S9J9ybfV2MB1tOcGOBnA7N1pkOWdNgkVGIms2P-x-klNybsRkOhMlX3Fd5Ylno394kC_WwiDml1_iBAqeC3COmPzF6K-yaxNSoPv8b_jg	d3d6777a-eae8-4c7d-92bd-6ecadce44527	bearer	1
4	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIyN2I1OWViYS0zNGQ3LTQzYmYtOWYxYy0zNWMzMGRiOGQxMjYiLCJleHAiOjE1MzQ4ODA3MDgsIm5iZiI6MCwiaWF0IjoxNTM0ODc3MTA4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJncnVwb3NhZ2EiLCJzZXNzaW9uX3N0YXRlIjoiMTE4NDIxNWMtMmMyMi00MTM4LTkwZGItODBkOTE5ZWJkNGUxIiwiY2xpZW50X3Nlc3Npb24iOiI2NmFiYTEwMi1hNGU2LTQ4ZjctOTQ4Yi00ZjVlODg4YzViYzMiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiaWNhcnJvcy13ZWJhcHAiOnsicm9sZXMiOlsidXN1YXJpb3NpdGUiLCJhbnVuY2lhbnRlcGoiXX19LCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.BwJH2gleCksLbPtXBNwzC_-842pp1x0XuGi6tpwVPz6dpIxZvPx7JS_1jueaQaRSahtgTC4fLRLIaOXq-3PcItzVgZmeUWQihnqix1KI6VbT3oS9ctVotuBSwp_kQ55oHKKkdyloVpmPxoUY0TsQpxKMpZfhW_5gPwdzl67QR7xZ88xmp-TV99rUZA8Fatit2jnbVUI_wLi0i1MWZ2ihU8M_XQwYw8XpfZHr0i_C6VvwKKftEnca-hEhyc5G7HDyMXgEAwYljhXA2AG_s_RCdj0mamaEaNe4nP4ZX9D3BuDuAD4qhOWLNxU5Tw6RmLrjMXWIKZkB4-neEZsQVIyMAg	2018-08-21 15:45:37.045	3600	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJhZDE3ZTEwNy1jNTkxLTRlYTMtOTY0My1jNmI0YzlmODhmMDMiLCJleHAiOjE1MzQ4ODA3MDgsIm5iZiI6MCwiaWF0IjoxNTM0ODc3MTA4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJJRCIsImF6cCI6ImdydXBvc2FnYSIsInNlc3Npb25fc3RhdGUiOiIxMTg0MjE1Yy0yYzIyLTQxMzgtOTBkYi04MGQ5MTllYmQ0ZTEiLCJuYW1lIjoiUGF1bG8gT3JzaWRhIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIiLCJnaXZlbl9uYW1lIjoiUGF1bG8iLCJmYW1pbHlfbmFtZSI6Ik9yc2lkYSIsImVtYWlsIjoicGF1bG8ub3JzaWRhQGdydXBvc2FnYS5jb20uYnIifQ.Gs-cfhEt2pWjaejCZWZA8vQoVeGRC5L5o1wZu-fnOeh-NXucTCjhV0kujsa3cKWJYCJrNqe3RgLmZxY9YgUXqYhfs-9V_595wl_1dm93z1OLbKLvakyMnzXXQVryETzZ43tuxQTiyScKFNHXN6gn0w72QcnrbLAQBPi8ntvlLWnEWbjTDlxddZ9dyIT2HCFrg12r0U23eHbqWDqmT6Wo3Jb_2jJq3xipDoNngV3K_o2SgrvOLD0Wah2rOpL6qwfRHV4ACnM-Ds3aVygwiW5manJek6AtAGFhXxFBCBvSqcNQdDderhffbIlj6kHHBSETlldMbwf4CvTI4p972QHAsw	1412634388	5184000	eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJhZjlhYjA1My01MmZlLTQ5ZTgtOTRhZi0wZGM0M2QzMGYwYWUiLCJleHAiOjE1NDAwNjExMDgsIm5iZiI6MCwiaWF0IjoxNTM0ODc3MTA4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmljYXJyb3MuY29tL2F1dGgvcmVhbG1zL2ljYXJyb3MiLCJhdWQiOiJncnVwb3NhZ2EiLCJzdWIiOiJlYWNjOGQ3Ni1kNTg3LTQ4ZmYtODJmNC0zZDQ5MDg3OTRjMzYiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiZ3J1cG9zYWdhIiwic2Vzc2lvbl9zdGF0ZSI6IjExODQyMTVjLTJjMjItNDEzOC05MGRiLTgwZDkxOWViZDRlMSIsImNsaWVudF9zZXNzaW9uIjoiNjZhYmExMDItYTRlNi00OGY3LTk0OGItNGY1ZTg4OGM1YmMzIiwicmVzb3VyY2VfYWNjZXNzIjp7ImljYXJyb3Mtd2ViYXBwIjp7InJvbGVzIjpbInVzdWFyaW9zaXRlIiwiYW51bmNpYW50ZXBqIl19fX0.IsLyEYWWOfRr__Drf64Ramf4eXSZnjP9klmO_MXhMu7CkwctYJ7fQxq5Nd14ks0McQUqEagkng-JrhNIXuoEDEZekgRHYkVKcjn83fKhH6yveTMR-2cldf2V2m3FaXLxSVWD9oihSA6zj5mIRPIk3JtP96yPwMtwjGO7Zp8HbqkgJ36Pb5ULys0ogdlMD2q7IcxY0OqoctjkPTyeq2SGQ9BW9t_b-GJrpM9Isc6EbFTbRVFnmRCKAeCjjuijcxvfhrBq3GcfWwzqA5y6s7qI0CSxsvC6TsPQAzFeMcyS8-aG7QyPiii-rIqhfg9krbK-KLOJTwjZf3YENk5h9urXeg	1184215c-2c22-4138-90db-80d919ebd4e1	bearer	1
\.


--
-- Data for Name: icarrosusuario; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.icarrosusuario (id, clientid, clientsecret, descricao, password, username) FROM stdin;
1	gruposaga	016fa09a-7a6b-4d2d-8b9c-c51f05cf0966	Usuário Saga	S@aga101010	paulo.orsida@gruposaga.com.br
\.


--
-- Data for Name: imovelpatrimonio; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.imovelpatrimonio (id, descricao, tipoimovel, valor, endereco_id) FROM stdin;
\.


--
-- Data for Name: instituicaofinanceira; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.instituicaofinanceira (id, alteradoem, ativa, codigo, codigodealerworkflow, criadoem, nome, razaosocial, id_usuario_criacao_alteracao) FROM stdin;
\.


--
-- Data for Name: itemcbc; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.itemcbc (id, adicionaraofinanciamento, codigo, dataprevisao, valor, id_opcao_item_cbc, id_operacao) FROM stdin;
\.


--
-- Data for Name: json_credit_an_itau; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.json_credit_an_itau (id, data, jsonbyte) FROM stdin;
\.


--
-- Data for Name: json_ident_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.json_ident_santander (id, cpfcliente, data, jsonbyte, nomecliente, tipoveiculo, id_ano_modelo, id_modelo) FROM stdin;
\.


--
-- Data for Name: json_preanalise_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.json_preanalise_santander (id, cpfcliente, data, jsonbyte, nomecliente) FROM stdin;
\.


--
-- Data for Name: json_req_sim_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.json_req_sim_santander (id, data, jsonbyte) FROM stdin;
\.


--
-- Data for Name: json_sim_resp_itau; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.json_sim_resp_itau (id, data, jsonbyte) FROM stdin;
\.


--
-- Data for Name: json_transac_req_itau; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.json_transac_req_itau (id, cpfcliente, data, jsonbyte, nomecliente) FROM stdin;
\.


--
-- Data for Name: jsonrequisicaoproposta; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.jsonrequisicaoproposta (id, data, jsonbyte) FROM stdin;
\.


--
-- Data for Name: jsonrespostaproposta; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.jsonrespostaproposta (id, data, jsonbyte) FROM stdin;
\.


--
-- Data for Name: jsonrespostasimulacaosantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.jsonrespostasimulacaosantander (id, data, jsonbyte) FROM stdin;
\.


--
-- Data for Name: leadsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.leadsvs (id, assuntoemail, cnpj_unidade, corpoemail, cpfcliente, cpfvendedorcl, dataemail, datainclusao, departamento, email, enviadoconnectlead, idleadcl, meiocontato, nome, nomecompleto, nomevendedorcl, status, telefone, id_fonte_lead, id_produto_interesse, id_responsavel, id_unidade_organizacional, id_leadenriquecido, id_motivo_descarte, fromemail) FROM stdin;
94	\N	\N	\N	222.222.222-22	48751618320	\N	2018-08-11 19:04:41.842	NOVOS	aksthose@hotmail.com	t	1167322	LIGACAO	Ana karolina	\N	WEVERSON MACHADO DE OLIVEIRA	QUALIFICADO	(22) 22222-2222	\N	3	3163	82	\N	4	\N
29	SAGA Whatsapp 5b4dd5e5e6e32	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body><h3>Form Details</h3><table border="1" width="100%"><tbody><tr><td>name</td><td>jhony</td></tr><tr><td>phone</td><td>95518149</td></tr><tr><td>estado</td><td>GO</td></tr><tr><td>Loja Preferida</td><td>c01104751000110@saganet.net.br</td></tr><tr><td>campo_veiculo_form</td><td>Volkswagen</td></tr><tr><td>campo_versao_form</td><td>Trendline 1.0</td></tr><tr><td>campo_ano_form</td><td>2018</td></tr><tr><td>campo_origem_form</td><td>mdp</td></tr><tr><td>utm_campaign</td><td>Busca-Paga-go-t7:model</td></tr><tr><td>utm_source</td><td>google</td></tr><tr><td>utm_medium</td><td>cpc</td></tr><tr><td>utm_uptracs</td><td>Busca-Paga-go-t7:model</td></tr><tr><td>received_from_url</td><td>http://www.sagavw.com.br/volkswagen/gol.html?loja=saga-t-7-volkswagen&amp;utm_campaign=busca-paga-go-t7:model&amp;utm_source=google&amp;utm_medium=cpc</td></tr></tbody></table></body></html>	\N	\N	2018-08-11 11:16:43	2018-08-11 12:59:15.055	NOVOS		\N	\N	INTERNET	jhony	\N	\N	LEAD	95518148	\N	\N	\N	\N	\N	\N	\N
11	\N	\N	\N	021.619.981-61	84398183191	\N	2018-08-07 20:44:29.735	SEMINOVOS	teste@gmail.com	t	1167312	SHOWROOM	teste	teste	EDSON DAVI DE SOUSA	DESCARTADO	(11) 11111-1111	11	1	3163	82	\N	4	\N
112	\N	\N	\N	123.459.987-99	00876207158	\N	2018-08-21 00:33:40.841	VENDA_DIRETA	teste2@gmail.com	t	1204929	LIGACAO	Teste Novo	Teste Novo2 	NADIA BELLONI	QUALIFICADO	(23) 23232-3232	\N	1642	3163	82	\N	\N	\N
22	\N	\N	\N	\N	\N	\N	2018-08-08 07:59:14.997	SEMINOVOS	\N	\N	\N	LIGACAO	teste	\N	\N	PRE_LEAD	(62) 96653-2622	\N	\N	\N	\N	\N	1	\N
89	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 55.990,00)	01104751000110	{"lastInteraction":1531442309838,"userId":10110944,"userName":"Rosenan Bilibio","userEmail":"rosenanbilibio@yahoo.com.br","userCpf":"52904806091","userPhones":["6299279030"],"leads":[{"id":8454573,"dateReceived":1531442309000,"dealId":17376900,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":0,"financingInputValue":16797,"vehiclePrice":55990,"title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 55.990,00)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531442309000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false},{"id":8454520,"dateReceived":1531441975000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":27097,"financingInstallmentsValue":910.2999877929688,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531441991000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	52904806091	\N	2018-07-13 00:38:29	2018-08-11 12:59:52.132	NOVOS	rosenanbilibio@yahoo.com.br	\N	\N	INTERNET	Rosenan Bilibio	Rosenan Bilibio	\N	LEAD	6299279030	\N	\N	\N	82	\N	\N	\N
32	SAGA Proposta 5b4dd5e5e6d75	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body><h3>Form Details</h3><table border="1" width="100%"><tbody><tr><td>name</td><td>Aline Peixoto de Oliveira</td></tr><tr><td>phone</td><td>6492857959</td></tr><tr><td>email</td><td>anahi.lp58@gmail.com</td></tr><tr><td>estado</td><td>GO</td></tr><tr><td>dealer-store-one</td><td>c01104751000110@saganet.net.br</td></tr><tr><td>comentarios</td><td></td></tr><tr><td>campo_veiculo_form</td><td>Volkswagen</td></tr><tr><td>campo_versao_form</td><td>Trendline 1.0</td></tr><tr><td>campo_ano_form</td><td>2018</td></tr><tr><td>campo_origem_form</td><td>mdp</td></tr><tr><td>utm_uptracs</td><td>null</td></tr><tr><td>received_from_url</td><td>http://www.sagavw.com.br/volkswagen/gol.html</td></tr></tbody></table></body></html>	9101029452	\N	2018-08-11 00:26:53	2018-08-11 12:59:18.608	NOVOS	anahi.lp58@gmail.com	\N	\N	INTERNET	Aline Peixoto de Oliveira	ALINE PEIXOTO DE OLIVEIRA	\N	DESCARTADO	64992312563	7	1642	3163	82	\N	1	\N
37	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.000,00)	01104751000110	{"lastInteraction":1533925068029,"userId":33175089,"userName":"Fernandes","userEmail":"guimfernandes@hotmail.com","userCpf":"76178536100","userPhones":["62991182033"],"leads":[{"id":8760598,"dateReceived":1533925068000,"dealId":17751872,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":20400,"vehiclePrice":68000,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.000,00)","message":"","score":99,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533925068000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":true}],"calls":[]}	76178536100	\N	2018-08-10 18:17:48	2018-08-11 12:59:43.528	NOVOS	guimfernandes@hotmail.com	\N	\N	INTERNET	Fernandes	Fernandes	\N	LEAD	62991182033	11	1599	3163	82	\N	\N	\N
16	\N	\N	\N	038.756.181-14	00099025116	\N	2018-08-07 21:07:01.144	VENDA_DIRETA	teste@gmail.com	t	1195495	SHOWROOM	teste	teste	ELIAS JOSEPH EL JHOMSI	QUALIFICADO	(62) 98242-6530	11	1	3163	55	\N	\N	\N
21	\N	\N	\N	038.756.181-14	\N	\N	2018-08-07 23:17:24.504	SEMINOVOS	kakakaka@gmail.com	\N	\N	SHOWROOM	teste	teste	\N	DESCARTADO	(56) 45645-6456	11	1	3091	82	\N	4	\N
14	\N	\N	\N	038.756.181-14	00876207158	\N	2018-08-07 21:05:53.268	VENDA_DIRETA	testeandasas@asasass.com	t	1167321	SHOWROOM	teste	\N	NADIA BELLONI	QUALIFICADO	(62) 98242-6530	11	1	3091	82	\N	\N	\N
20	\N	\N	\N	\N	\N	\N	2018-08-08 05:02:38.008	SEMINOVOS	test@teste.com	\N	\N	LIGACAO	teste Gol	\N	\N	DESCARTADO	\N	11	2	3163	82	\N	2	\N
13	\N	\N	\N	123.456.78-92	16087054120	\N	2018-08-08 05:50:44.42	SEMINOVOS	teste@gmail.com	t	1167313	SHOWROOM	teste	\N	EMILIO HANUM FILHO	QUALIFICADO	(65) 65656-5656	11	2	3163	82	\N	\N	\N
12	\N	\N	\N	111.111.11-11	82477132172	\N	2018-08-07 20:48:47.596	VENDA_DIRETA	teste@gmail.com	t	1167314	SHOWROOM	teste	\N	GEISON FRANCISCO DIAS	QUALIFICADO	(11) 11111-1111	11	1	3428	82	\N	\N	\N
19	\N	\N	\N	038.756.181-14	82477132172	\N	2018-08-07 22:11:26.09	SEMINOVOS	teste@teste.com	t	1167315	SHOWROOM	teste	\N	GEISON FRANCISCO DIAS	QUALIFICADO	(62) 98242-6530	11	1	3091	82	\N	\N	\N
25	\N	\N	\N	038.615.591-70	64881113100	\N	2018-08-09 11:23:32.559	VENDA_DIRETA	mariohj94@gmail.com	t	1167310	LIGACAO	Juca	\N	JOAO MARCIO ALVES OLIVEIRA	QUALIFICADO	(62) 62626-2626	11	2	3091	82	\N	\N	\N
17	\N	\N	\N	456.454.646-54	87342286168	\N	2018-08-08 09:54:35.567	NOVOS	teste@gmail.com	t	1167311	SHOWROOM	teste	\N	ALEXANDRE DE MOURA HAMU	QUALIFICADO	(62) 62626-2662	11	1	3091	82	\N	\N	\N
31	Proposta  Recebida: Volkswagen Gol 1.0 MPI City (Flex) 0km	01.104.751/0001-10	<html style=""><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body style="background-color:#ffffff"><div><table cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color:#f4f4f4"><tbody><tr><td align="center"><table align="center" cellpadding="0" cellspacing="0" width="460" style=""><tbody><tr><td height="20"></td></tr><tr><td style="display:none!important; visibility:hidden; font-size:1px; color:#ffffff; line-height:1px; max-height:0px; max-width:0px; overflow:hidden">Proposta Recebida</td></tr><tr><td colspan="3" height="20" style="background-color:#2867c9; font-family:'Open Sans',helvetica,arial,sans-serif; font-size:20px; line-height:20px; border-radius:8px 8px 0px 0px"></td></tr><tr style="background-color:#2867c9"><td colspan="3"><table align="center" cellpadding="0" cellspacing="0"><tbody><tr><td colspan="3"><a href="http://link.icarros.com.br/wf/click?upn=Bc-2B5VTzPecQpTN7rMxhq68hBojJWmgcb36T-2B0Lz4OESr2H-2BautDmJKvGXZAyQopFfNcUKNEtqQV532E5NrUcf4S2Gc5nZvU-2BF25B1UBMFavity3pscNL9bn-2Bwg0sJf4-2FH6-2FwkpR-2F130B39h3L97FNDx41wS4-2BBbM2VbvjrgFOF0K3LB3M4VKROLg7WQHKjwz2VegxIC-2BYCyiwQyeS87sXKLI1IZa629bcdLqQxxfZP2WhjLsJmpi2t2IszrQmZJT_DcNcxCkKgtn5UbtqYw-2F35sXS49Sp7nJJpnt9jNHJ1FUzuAM9nhs-2B7NJ2NrPfwzbO9ZhE52Ubrmx-2FK5Yhtf5ZvKeqPN4mIBMCCibWwuU8KbvFzyBsDHVmjtJRVzQV8QKUIMD1J9ZbiruptVYXWydg10USWVmyilBYOUMZtp83eTXR-2B5dOAa1GI9dq-2B5GxFeUBV3QIg403PouzkRqNHPYnkz4AS00toJGsQoxHoopmUbCHryjWlYWWJYQxdkAzKOh56RBjUOY4HSJu9J9JQdmTfg-3D-3D" target="_blank"><img border="0" height="40" src="https://marketing-image-production.s3.amazonaws.com/uploads/a4702c9b2114210a62f63bd82cb8b24f243f7223ebaf78c660e59134b2be874e846c63dc4e1a7dfe48994c5a57288ee3864ba10a28343ddd3d9aa41b486b1c6b.png" width="122" style="display:block"></a></td></tr></tbody></table></td></tr><tr><td colspan="3" height="20" style="background-color:#2867c9; line-height:20px"></td></tr><tr><td colspan="3" style="background-color:#ffffff; border-radius:0px 0px 8px 8px"><table cellpadding="0" cellspacing="0"><tbody><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="left" width="400"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:16px; color:#444444; text-align:justify; font-weight:700">Olá, SAGA T7 VOLKSWAGEN.<br>Você recebeu uma nova proposta. </span></td><td width="30"></td></tr><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="justify" width="400"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:14px; color:#444444; text-align:justify; font-weight:400">Anúncio:</span></td><td width="30"></td></tr><tr><td colspan="3" height="5"></td></tr><tr><td width="30"></td><td align="justify" width="400"><a href="http://link.icarros.com.br/wf/click?upn=Bc-2B5VTzPecQpTN7rMxhq68hBojJWmgcb36T-2B0Lz4OESFM17VovKr-2F08XbTi5TlKO6jm9PEEKL1jjj9CEKDVooZaEm3zVm-2BgdQwWe-2Brx-2FtwUQlKCq0qGY2lZnexs2kkMVKny6Kxsxxo0wRaruHMbnOvj-2BaGnK9-2BrMCa2BE1IjgwfB5Wsw3STd4b8HA5DeUZpNX9E42UnMuCHCM5JPlSCcTFV-2BGRpdVx1z1eFTtPdp1ydAGo31wHQghd2vmM0F2FppPnix3LGt5G64a2Ba-2BTpyhA-3D-3D_DcNcxCkKgtn5UbtqYw-2F35sXS49Sp7nJJpnt9jNHJ1FUzuAM9nhs-2B7NJ2NrPfwzbO9ZhE52Ubrmx-2FK5Yhtf5ZvB0B53bWSU5S-2Fox-2FYxYxzsw60FfZN98kG-2BDE7SFAxauCFpGhVBxVXowqsW3hlLxuMAs-2F3as3EOyu9lGT5prLWxM9A2pjBsL8I5C7Q5G1FnT0Zj60JlN7EZhzxQai0UQu9wcvQg85rGC33OYkaIGgyA5i5iHpjkUmweiWrXBBxWFyOB8KmYs32-2FO-2FGVCL8pByeA-3D-3D" target="_blank" style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:14px; color:#2262CD; text-align:justify; font-weight:600; text-decoration:underline">Volkswagen Gol 1.0 MPI City (Flex) 0Km - R$ 40.900 </a></td><td width="30"></td></tr><tr><td colspan="3" height="15"></td></tr></tbody></table></td></tr><tr><td colspan="3" height="10" style="background-color:#f4f4f4"></td></tr><tr><td colspan="3" style="background-color:#ffffff; border-radius:8px 8px 0px 0px"><table cellpadding="0" cellspacing="0"><tbody><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="left" width="400"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:14px; color:#444444; text-align:justify; font-weight:700">Mensagem</span></td><td width="30"></td></tr><tr><td colspan="3" height="15"></td></tr><tr><td colspan="3" align="center" width="460" valign="middle"><table cellpadding="0" cellspacing="0"><tbody><tr><td width="30"></td><td align="justify" width="400"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">O cliente não enviou uma mensagem. Entre em contato para fechar negócio.</span></td><td width="30"></td></tr><tr><td width="30"></td></tr></tbody></table></td></tr><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="center" width="400" height="1" style="border-top:1px solid  #f4f4f4"></td><td width="30"></td></tr><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="left" width="400"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:14px; color:#444444; text-align:justify; font-weight:700">Informações do cliente</span></td><td width="30"></td></tr><tr><td colspan="3" height="15"></td></tr><tr><td colspan="5" align="center" width="460" valign="middle"><table cellpadding="0" cellspacing="0"><tbody><tr><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">Nome</span></td><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">Cleibio marinho de assis</span></td><td width="30"></td></tr><tr><td width="30"></td></tr></tbody></table></td></tr><tr><td colspan="3" height="10"></td></tr><tr><td colspan="5" align="center" width="460" valign="middle"><table cellpadding="0" cellspacing="0"><tbody><tr><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">E-mail</span></td><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">pollyannasantos4@gmail.com</span></td><td width="30"></td></tr><tr><td width="30"></td></tr></tbody></table></td></tr><tr><td colspan="3" height="10"></td></tr><tr><td colspan="5" align="center" width="460" valign="middle"><table cellpadding="0" cellspacing="0"><tbody><tr><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">Celular</span></td><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">(62) 8635-3779</span></td><td width="30"></td></tr><tr><td width="30"></td></tr></tbody></table></td></tr><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="center" width="400" height="1" style="border-top:1px solid  #f4f4f4"></td><td width="30"></td></tr><tr><td colspan="3" height="30"></td></tr><tr><td width="30"></td><td align="left" width="400"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:14px; color:#444444; text-align:justify; font-weight:700">Condições</span></td><td width="30"></td></tr><tr><td colspan="3" height="15"></td></tr><tr><td colspan="5" align="center" width="460" valign="middle"><table cellpadding="0" cellspacing="0"><tbody><tr><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">Financiamento</span></td><td width="30"></td><td align="left" width="185"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:12px; color:#444444; text-align:justify; font-weight:400">Entrada de R$ 12.000</span></td><td width="30"></td></tr><tr><td width="30"></td></tr></tbody></table></td></tr><tr><td colspan="3" height="30"></td></tr></tbody></table></td></tr><tr style="background-color:#2867c9"><td height="44" style="border-radius:0px 0px 8px 8px"><table align="center" cellpadding="0" cellspacing="0" height="44"><tbody><tr><td align="center" style="padding-left:9px; padding-right:9px"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:13px; color:#FFFFFF"><a href="http://link.icarros.com.br/wf/click?upn=Bc-2B5VTzPecQpTN7rMxhq68hBojJWmgcb36T-2B0Lz4OERtuSoCeCzo5XTprixWRkWegSr3gL4e7Xcfvc9RW3ZTxxyjYmGzdt6-2BbaOTm-2FKtSc-2BrHWU-2BCK-2Br6kHl9y0eilC2kr4ois6WD0xxZYf8lNG9r6Fp-2Fpzekh5SnHvOSMSDdP-2FP1smfXgsmb81NuT9IYSwjTANhxMWvLhI3FEnqenmos16mJ8W0MYKjJJkYfIkHkgZ-2B4PclMnDds-2Bb5ftHmyENy0sMXAoekx4eoOOvVVOHBfw-3D-3D_DcNcxCkKgtn5UbtqYw-2F35sXS49Sp7nJJpnt9jNHJ1FUzuAM9nhs-2B7NJ2NrPfwzbO9ZhE52Ubrmx-2FK5Yhtf5ZvHL6XcKh9OHVGVhcFglGJJHDCcVX55Tb9n42skLBrrBLjGZ3FdvHUDxDowXUhEleQUjKt-2FPrCjWY-2BIRsSHXqRlgVx4hTPQmGaFB9eB-2FmuWr9DQ9tBWwkcXQUp3E5ZiiHNLAxTBnYi2rfPntFYd2-2BsQvrkqNkCEVit-2BJ2IYzrFNMxh6IiaOBr2S5hiRc3OruxKQ-3D-3D" target="_blank" style="text-decoration:none; color:#ffffff; padding-left:1px">Portal Revenda</a></span></td></tr></tbody></table></td></tr><tr><td height="20"></td></tr><tr><td><table cellpadding="0" cellspacing="0" width="460"><tbody><tr><td align="left"><span class="sg-image"><a href="http://link.icarros.com.br/wf/click?upn=Bc-2B5VTzPecQpTN7rMxhq68hBojJWmgcb36T-2B0Lz4OESr2H-2BautDmJKvGXZAyQopFfNcUKNEtqQV532E5NrUcf4S2Gc5nZvU-2BF25B1UBMFavity3pscNL9bn-2Bwg0sJf4-2FH6-2FwkpR-2F130B39h3L97FNDx41wS4-2BBbM2VbvjrgFOF0K3LB3M4VKROLg7WQHKjwz2VegxIC-2BYCyiwQyeS87sXKLI1IZa629bcdLqQxxfZP2WhjLsJmpi2t2IszrQmZJT_DcNcxCkKgtn5UbtqYw-2F35sXS49Sp7nJJpnt9jNHJ1FUzuAM9nhs-2B7NJ2NrPfwzbO9ZhE52Ubrmx-2FK5Yhtf5ZvGolBkW1DY78Y7gf9WgZq48AJvgj-2FAwaAesLyM0oyE-2Fj3a5DrwZ-2BHh0w8hiHCaNMPFqEcpTWV1jAehhRbf8a4g8X9g-2B51w82D-2FBtJpK6fBeu4g5ybMBfcARBxqtCoXywdYgg9aXob7bi7P4rsAY21A-2Bm52ZZQbs8s6HwMDe3kZMMIsw50zUDyG0Tj-2B6SbW6nzA-3D-3D" target="_blank"><img border="0" height="31" src="https://marketing-image-production.s3.amazonaws.com/uploads/db86dba8e7509e5164c40bd3fd3cd996e06b9ea9351b03b84c710fad08ab988269b71ba12e28d2ed03f8797365da0676669b02c2e52281d8b42c8e9e67dce6cc.png" width="95" style="width:95px; height:31px"></a></span></td><td align="right" style="vertical-align:middle"><span class="sg-image"><img height="24" src="https://marketing-image-production.s3.amazonaws.com/uploads/3b1c15576b2f62b175cacb6db4e0560332e32ba732d06524f9c59a86c0ba6b004b5aeed0efa33479ae399c5519589098ea74d4e796b1bc12ae460b4ca2e3d9ad.png" width="111" style="width:111px; height:24px"></span></td></tr></tbody></table></td></tr><tr><td height="20"></td></tr><tr><td align="left" style="line-height:10px"><span style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:10px; color:#757575">Atendimento iCarros - Segunda a sexta feira, das 09h às 18h.<br><br>E-mail: <a href="mailto:atendimento@icarros.com.br?Subject=Olá iCarros" target="_blank" style="color:#757575; font-family:'Open Sans',helvetica,arial,sans-serif; font-size:10px; color:#757575">atendimento@icarros.com.br</a> <br><br>O iCarros respeita sua privacidade e não divulga seus dados. Você está recebendo esse e-mail porque marcou a opção de receber nossas ofertas e promoções.<br><br>iCarros LTDA | Av. Pres. Juscelino Kubitschek, 180 - 2º andar - Itaim Bibi - Cep: 04543-000 - São Paulo</span></td></tr><tr><td height="20" style="font-family:'Open Sans',helvetica,arial,sans-serif; font-size:24px; line-height:20px"></td></tr></tbody></table></td></tr></tbody></table></div><img src="http://link.icarros.com.br/wf/open?upn=DcNcxCkKgtn5UbtqYw-2F35sXS49Sp7nJJpnt9jNHJ1FUzuAM9nhs-2B7NJ2NrPfwzbO9ZhE52Ubrmx-2FK5Yhtf5ZvHgnqgLHXiin-2BwabVQ1Cm2YUlxZGebYRX33Vv-2BraQLIAW78wiAhVQBd9OVO3MMQL5-2FNArWXSIPptPafSl95KAPUR-2BfDbTLABJq8rLxPzjkGHIU3IvPeKOUbYGDa5XkS-2FFDrihCTLwO550VKWxug4Fok4TjpUEavjgYjyFza48y761zjIYIKEtKOTlhc4eLNbng-3D-3D" alt="" width="1" height="1" border="0" style="height:1px!important; width:1px!important; border-width:0!important; margin-top:0!important; margin-bottom:0!important; margin-right:0!important; margin-left:0!important; padding-top:0!important; padding-bottom:0!important; padding-right:0!important; padding-left:0!important"></body></html>	\N	\N	2018-08-11 02:35:10	2018-08-11 12:59:15.37	NOVOS	\N	\N	\N	INTERNET	\N	\N	\N	DESCARTADO	\N	11	\N	\N	82	\N	3	\N
88	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	01104751000110	{"lastInteraction":1531446554009,"userId":35072746,"userName":"Carlos Alberto De Santana Santos ","userEmail":"carllosalbertoo@outlook.com","userCpf":"50234170506","userPhones":["7999742925"],"leads":[{"id":8455418,"dateReceived":1531446554000,"dealId":17799998,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12870,"vehiclePrice":42900,"vehiclePlate":"","title":"() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531446554000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	50234170506	\N	2018-07-13 01:49:14	2018-08-11 12:59:51.982	NOVOS	carllosalbertoo@outlook.com	\N	\N	INTERNET	Carlos Alberto De Santana Santos 	Carlos Alberto De Santana Santos 	\N	LEAD	7999742925	11	\N	\N	82	\N	\N	\N
115	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	01104751000110	{"lastInteraction":1534680161142,"userId":33836110,"userName":"Edu","userEmail":"edu_physio@hotmail.com","userCpf":"2108523138","userPhones":[],"leads":[{"id":8867715,"dateReceived":1534680150000,"dealId":17835900,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":20697,"financingInstallmentsValue":1473.949951171875,"vehiclePrice":68990,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534680161000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	2108523138	\N	2018-08-19 12:02:30	2018-08-21 15:45:21.679	NOVOS	edu_physio@hotmail.com	\N	\N	INTERNET	Edu	EDUARDO FELIPE FERREIRA SUHETT	\N	DESCARTADO	\N	\N	\N	\N	82	18	4	ICarros
114	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	01104751000110	{"lastInteraction":1534814780106,"userId":35590656,"userName":"Samuel David Alves Rodrigues","userEmail":"diadoradavid@hotmail.com","userCpf":"70067335101","userPhones":["6285740950"],"leads":[{"id":8890700,"dateReceived":1534814760000,"dealId":17835900,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":18000,"financingInstallmentsValue":1577.719970703125,"vehiclePrice":68990,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)","score":93,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534814780000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":true}],"calls":[]}	70067335101	\N	2018-08-21 01:26:00	2018-08-21 15:45:19.206	NOVOS	diadoradavid@hotmail.com	\N	\N	INTERNET	Samuel David Alves Rodrigues	SAMUEL DAVID ALVES RODRIGUES	\N	DESCARTADO	6285740950	\N	\N	3428	82	\N	4	ICarros
117	2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)	01104751000110	{"lastInteraction":1534553347468,"userId":25756984,"userName":"Romario","userEmail":"romariosenju@gmail.com","userCpf":"70038470195","userPhones":["62984100196"],"leads":[{"id":8854460,"dateReceived":1534553336000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":16197,"financingInstallmentsValue":1147.699951171875,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534553347000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	70038470195	\N	2018-08-18 00:48:56	2018-08-21 15:45:35.693	NOVOS	romariosenju@gmail.com	\N	\N	INTERNET	Romario	ROMARIO TEIXEIRA SANTANA	\N	LEAD	6284100196	\N	\N	3092	82	\N	\N	ICarros
116	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1534557214766,"userId":35553460,"userName":"Patricia","userEmail":"patterocha@hotmail.com","userCpf":"4327031100","userPhones":[],"leads":[{"id":8855199,"dateReceived":1534557214000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534557214000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	4327031100	\N	2018-08-18 01:53:34	2018-08-21 15:45:25.541	NOVOS	patterocha@hotmail.com	\N	\N	INTERNET	Patricia	PATRICIA SOARES DA ROCHA	\N	LEAD	\N	\N	\N	3163	51	\N	\N	ICarros
46	2018/2019 Up! 1.0 12v E-Flex move up! 0km (R$ 48.300,00)	01104751000110	{"lastInteraction":1533473656940,"userId":2344940,"userName":"Hilda Teixeira Da Costa","userEmail":"tataudi@hotmail.com","userCpf":"44628862672","userPhones":["34998870391"],"leads":[{"id":8693446,"dateReceived":1533473649000,"dealId":17730606,"dealerId":1134846,"vehicleTrimId":28762,"financingInstallmentsNumber":48,"financingInputValue":14490,"financingInstallmentsValue":1030.030029296875,"vehiclePrice":48300,"vehiclePlate":"","title":"2018/2019 Up! 1.0 12v E-Flex move up! 0km (R$ 48.300,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533473656000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	44628862672	\N	2018-08-05 12:54:09	2018-08-11 12:59:45.556	NOVOS	tataudi@hotmail.com	\N	\N	INTERNET	Hilda Teixeira Da Costa	\N	\N	LEAD	34998870391	11	\N	\N	82	\N	\N	\N
45	() 2019 Fox 1.6 MSI Connect (Flex) (R$ 49,990)	01104751000110	{"lastInteraction":1533581294844,"userId":34902658,"userName":"Francisco","userEmail":"fjestival@bol.com.br","userCpf":"83008659187","userPhones":["62985607993"],"leads":[{"id":8708314,"dateReceived":1533581275000,"dealId":17802922,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":21000,"financingInstallmentsValue":852.0,"vehiclePrice":49990,"vehiclePlate":"","title":"() 2019 Fox 1.6 MSI Connect (Flex) (R$ 49,990)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533581294000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":false}],"calls":[]}	83008659187	\N	2018-08-06 18:47:55	2018-08-11 12:59:45.4	NOVOS	fjestival@bol.com.br	\N	\N	INTERNET	Francisco	\N	\N	LEAD	62985607993	11	\N	\N	82	\N	\N	\N
95	\N	\N	\N	\N	\N	\N	2018-08-16 13:36:52.133	SEMINOVOS	\N	\N	\N	LIGACAO	teste	\N	\N	DESCARTADO	(62) 96262-6262	\N	1642	3091	21	\N	1	\N
118	2018/2018 GOL 1.0 MPI CITY (FLEX) 0KM (R$ 40.900,00)	01104751000110	{"lastInteraction":1534863456798,"userId":30200528,"userName":"Rozilda","userEmail":"rozilda37@hotmail.com","userCpf":"56637454168","userPhones":["63981270706"],"leads":[{"id":17479654,"dateReceived":1534863456000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"vehicleModelYear":2018,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 GOL 1.0 MPI CITY (FLEX) 0KM (R$ 40.900,00)","message":"","score":46,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1534863456000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	56637454168	\N	2018-08-21 14:57:36	2018-08-21 15:49:15.885	NOVOS	rozilda37@hotmail.com	\N	\N	INTERNET	Rozilda	ROZILDA OLIVEIRA ABREU	\N	DESCARTADO	63981270706	\N	\N	\N	82	21	3	ICarros
113	2018/2018 GOL 1.0 MPI CITY (FLEX) 0KM (R$ 40.900,00)	01104751000110	{"lastInteraction":1534863456798,"userId":30200528,"userName":"Rozilda","userEmail":"rozilda37@hotmail.com","userCpf":"56637454168","userPhones":["63981270706"],"leads":[{"id":17479654,"dateReceived":1534863456000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"vehicleModelYear":2018,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 GOL 1.0 MPI CITY (FLEX) 0KM (R$ 40.900,00)","message":"","score":46,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1534863456000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	56637454168	\N	2018-08-21 14:57:36	2018-08-21 15:45:18.499	NOVOS	rozilda37@hotmail.com	\N	\N	INTERNET	Rozilda	ROZILDA OLIVEIRA ABREU	\N	LEAD	63981270706	\N	\N	3428	82	\N	\N	ICarros
53	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	01104751000110	{"lastInteraction":1533077450770,"userId":32936943,"userName":"Vinícius Sincariuc","userEmail":"sincariuc321@live.con","userCpf":"45480251899","userPhones":["15998370929"],"leads":[{"id":8645397,"dateReceived":1533077410000,"dealId":17835900,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":49000,"financingInstallmentsValue":601.27001953125,"vehiclePrice":68990,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533077450000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	45480251899	\N	2018-07-31 22:50:10	2018-08-11 12:59:46.613	NOVOS	sincariuc321@live.con	\N	\N	INTERNET	Vinícius Sincariuc	\N	\N	LEAD	15998370929	11	\N	\N	82	\N	\N	\N
54	() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)	01104751000110	{"lastInteraction":1532911798799,"userId":35299078,"userName":"Ivaneide ","userEmail":"henrique.estabile@icloud.com","userCpf":"59223898153","userPhones":["62999802555"],"leads":[{"id":8619016,"dateReceived":1532911785000,"dealId":17891672,"dealerId":1134846,"vehicleTrimId":28841,"financingInstallmentsNumber":48,"financingInputValue":25917,"financingInstallmentsValue":1797.25,"vehiclePrice":86390,"vehiclePlate":"","title":"() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532911798000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	59223898153	\N	2018-07-30 00:49:45	2018-08-11 12:59:46.764	NOVOS	henrique.estabile@icloud.com	\N	\N	INTERNET	Ivaneide 	\N	\N	LEAD	62999802555	11	\N	\N	82	\N	\N	\N
96	\N	\N	\N	\N	\N	\N	2018-08-16 13:38:13.956	NOVOS	\N	\N	\N	SHOWROOM	TESTE uPDATE	\N	\N	DESCARTADO	(66) 66666-6666	\N	1684	3091	144	\N	3	\N
48	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1533250426307,"userId":35361701,"userName":"João Carlos ","userEmail":"jcalvesbispojs@hotmail.com","userCpf":"3134453118","userPhones":["62985379483"],"leads":[{"id":8669514,"dateReceived":1533250426000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","message":"Entrada de 30 mil ...","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533250426000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	3134453118	\N	2018-08-02 22:53:46	2018-08-11 12:59:45.857	NOVOS	jcalvesbispojs@hotmail.com	\N	\N	INTERNET	João Carlos 	João Carlos 	\N	LEAD	62985379483	11	\N	\N	82	\N	\N	\N
121	\N	\N	\N	\N	\N	\N	2018-08-22 10:00:11.86	SEMINOVOS	teste@gmail.com	\N	\N	LIGACAO	Teste	\N	\N	DESCARTADO	\N	\N	1641	3163	144	\N	1	\N
59	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1532864157971,"userId":7934696,"userName":"Iraci","userEmail":"da-nielmarques@hotmail.com","userCpf":"4742622197","userPhones":["62999943483"],"leads":[{"id":8611154,"dateReceived":1532864094000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":60,"financingInputValue":6000,"financingInstallmentsValue":1323.02001953125,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","score":93,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532864157000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":false}],"calls":[]}	4742622197	\N	2018-07-29 11:34:54	2018-08-11 12:59:47.52	NOVOS	da-nielmarques@hotmail.com	\N	\N	INTERNET	Iraci	\N	\N	LEAD	62999943483	11	\N	\N	82	\N	\N	\N
57	() 2019 Up! 1.0 12v E-Flex move up! (R$ 48,400)	01104751000110	{"lastInteraction":1532879616877,"userId":34066895,"userName":"Carlos Josimar","userEmail":"carlos.josimar@gmail.com","userCpf":"97279994253","userPhones":["92992327607"],"leads":[{"id":8613358,"dateReceived":1532879608000,"dealId":17730647,"dealerId":1134846,"vehicleTrimId":28762,"financingInstallmentsNumber":48,"financingInputValue":14520,"financingInstallmentsValue":1011.6199951171875,"vehiclePrice":48400,"vehiclePlate":"","title":"() 2019 Up! 1.0 12v E-Flex move up! (R$ 48,400)","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532879616000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	97279994253	\N	2018-07-29 15:53:28	2018-08-11 12:59:47.215	NOVOS	carlos.josimar@gmail.com	\N	\N	INTERNET	Carlos Josimar	\N	\N	LEAD	92992327607	11	\N	\N	82	\N	\N	\N
98	2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)	01104751000110	{"lastInteraction":1534512978323,"userId":25331291,"userName":"Aldo","userEmail":"anzjr0@gmail.com","userCpf":"7168825747","userPhones":["62999182629"],"leads":[{"id":8846748,"dateReceived":1534512978000,"dealId":18129301,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":16497,"vehiclePrice":54990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)","message":"1111 22222","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534512978000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":17467714,"dateReceived":1534512927000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"vehicleModelYear":2019,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 POLO 1.0 (FLEX) 0KM (R$ 53.990,00)","message":"xxx yyyy","score":54,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1534512927000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	7168825747	\N	2018-08-17 13:36:18	2018-08-17 18:12:52.505	NOVOS	anzjr0@gmail.com	\N	\N	INTERNET	Aldo	ALDO NAVARRO ZUQUINI JUNIOR	\N	DESCARTADO	62999182629	\N	\N	\N	82	3	4	ICarros
97	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1534514747537,"userId":29304646,"userName":"Paulo Orsida","userEmail":"pauloorsida@gmail.com","userCpf":"69766088187","userPhones":["62981023535"],"leads":[{"id":8847041,"bankTransactionId":65842517,"dateReceived":1534514597000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","score":92,"hasApprovedCredit":true,"hasRepprovedCredit":false,"callFrom":-1,"typeLead":"FICHA","lastInteraction":1534514747000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":true},{"id":8820416,"bankTransactionId":65817495,"dateReceived":1534358396000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","score":92,"hasApprovedCredit":true,"hasRepprovedCredit":false,"callFrom":-1,"typeLead":"FICHA","lastInteraction":1534358528000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":true}],"calls":[]}	69766088187	\N	2018-08-17 14:03:17	2018-08-17 18:12:37.798	NOVOS	pauloorsida@gmail.com	\N	\N	INTERNET	Paulo Orsida	PAULO RENATO ORSIDA DE LIMA	\N	DESCARTADO	62981023535	\N	\N	\N	82	2	4	ICarros
120	\N	\N	\N	\N	\N	\N	2018-08-22 09:58:18.641	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	Teste	\N	\N	DESCARTADO	\N	\N	1599	3428	82	\N	3	\N
99	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.499,00)	01104751000110	{"lastInteraction":1534509463407,"userId":33366373,"userName":"Taniel Borges","userEmail":"tanielpererekborges@gmail.com","userCpf":"70060559110","userPhones":["62982285565"],"leads":[{"id":8846108,"dateReceived":1534509463000,"dealId":17891299,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":15449,"vehiclePrice":51499,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.499,00)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534509463000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	70060559110	\N	2018-08-17 12:37:43	2018-08-17 18:13:09.491	NOVOS	tanielpererekborges@gmail.com	\N	\N	INTERNET	Taniel Borges	Taniel Borges	\N	DESCARTADO	62982285565	\N	\N	\N	82	\N	3	ICarros
92	() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)	01104751000110	{"lastInteraction":1531392813289,"userId":29640818,"userName":"Hugofernando","userEmail":"hugofernando@56.com","userCpf":"2697778144","userPhones":[],"leads":[{"id":8447749,"dateReceived":1531392813000,"dealId":18153176,"dealerId":1134846,"vehicleTrimId":28834,"financingInstallmentsNumber":36,"financingInputValue":13770,"vehiclePrice":45900,"vehiclePlate":"","title":"() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531392813000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	333.333.333-33	\N	2018-07-12 10:53:33	2018-08-11 12:59:53.149	NOVOS	hugofernando@56.com	\N	\N	INTERNET	Hugofernando	Hugofernando	\N	DESCARTADO	(22) 22222-2222	\N	1707	3091	82	\N	1	\N
122	\N	\N	\N	\N	\N	\N	2018-08-22 10:15:16.495	SEMINOVOS	\N	\N	\N	LIGACAO	asdfasdfasdf	\N	\N	DESCARTADO	(55) 55555-5555	\N	1684	3163	82	\N	3	\N
66	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1532389437532,"userId":35201853,"userName":"Pamela Cruvinel De Lima","userEmail":"pamelacruvinellima@outlook.com","userCpf":"4314879106","userPhones":["64992143977"],"leads":[{"id":8552760,"dateReceived":1532389437000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"Sem entrada parcelas em 84 vezes\\r\\n","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532389437000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	4314879106	\N	2018-07-23 23:43:57	2018-08-11 12:59:48.576	NOVOS	pamelacruvinellima@outlook.com	\N	\N	INTERNET	Pamela Cruvinel De Lima	\N	\N	LEAD	64992143977	11	\N	\N	82	\N	\N	\N
119	\N	\N	\N	\N	\N	\N	2018-08-22 09:42:42.598	SEMINOVOS	\N	\N	\N	LIGACAO	Teste	\N	\N	DESCARTADO	(66) 56565-6565	\N	1642	3163	82	\N	3	\N
75	() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)	01104751000110	{"lastInteraction":1532062825322,"userId":34922169,"userName":"Marcelo Da Silva Araujo","userEmail":"marcelosiilaraujo@gmail.com","userCpf":"1211181189","userPhones":["62991457609"],"leads":[{"id":8519159,"dateReceived":1532062825000,"dealId":17891672,"dealerId":1134846,"vehicleTrimId":28841,"financingInstallmentsNumber":48,"financingInputValue":25917,"vehiclePrice":86390,"vehiclePlate":"","title":"() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)","message":"","score":95,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532062825000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":false}],"calls":[]}	1211181189	\N	2018-07-20 05:00:25	2018-08-11 12:59:49.941	NOVOS	marcelosiilaraujo@gmail.com	\N	\N	INTERNET	Marcelo Da Silva Araujo	\N	\N	LEAD	62991457609	11	1	3091	82	\N	\N	\N
102	2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)	01104751000110	{"lastInteraction":1534440078513,"userId":10702548,"userName":"João Carlos Ramiro ","userEmail":"joaocarlosjk@hotmail.com","userCpf":"4887472609","userPhones":["62994078282"],"leads":[{"id":8835176,"dateReceived":1534439960000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":25000,"financingInstallmentsValue":867.1599731445312,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534440078000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	4887472609	\N	2018-08-16 17:19:20	2018-08-17 18:13:43.166	NOVOS	joaocarlosjk@hotmail.com	\N	\N	INTERNET	João Carlos Ramiro 	JOAO CARLOS RAMIRO	\N	DESCARTADO	62994078282	\N	\N	\N	82	6	3	ICarros
103	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)	01104751000110	{"lastInteraction":1534337245117,"userId":35457931,"userName":"Pedro Almeida","userEmail":"mazpedro@gmail.com","userCpf":"59327251091","userPhones":["994127007"],"leads":[{"id":8816411,"dateReceived":1534337245000,"dealId":17835900,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":20697,"vehiclePrice":68990,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 68.990,00)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534337245000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	59327251091	\N	2018-08-15 12:47:25	2018-08-17 18:13:53.807	NOVOS	mazpedro@gmail.com	\N	\N	INTERNET	Pedro Almeida	PEDRO ETRAMAR MACHADO DE ALMEIDA	\N	DESCARTADO	994127007	\N	\N	\N	82	7	3	ICarros
104	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1534278683725,"userId":35509113,"userName":"Ordalia Aparecida Brandao Aran","userEmail":"ordaliabrandao@hotmail.com","userCpf":"43149510104","userPhones":["62981543343"],"leads":[{"id":8807992,"dateReceived":1534278683000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534278683000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	43149510104	\N	2018-08-14 20:31:23	2018-08-17 18:14:06.596	NOVOS	ordaliabrandao@hotmail.com	\N	\N	INTERNET	Ordalia Aparecida Brandao Aran	ORDALIA APARECIDA BRANDAO ARANTES	\N	DESCARTADO	62981543343	\N	\N	\N	82	8	3	ICarros
123	\N	\N	\N	\N	\N	\N	2018-08-22 11:00:57.021	NOVOS	\N	\N	\N	LIGACAO	asdfghjkl	\N	\N	DESCARTADO	(33) 33333-3333	\N	1662	3163	82	\N	3	\N
74	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	01104751000110	{"lastInteraction":1532129083324,"userId":31499358,"userName":"Edilene Souza Mendonca","userEmail":"frankreinaldo50@gmail.com","userCpf":"79610927149","userPhones":["62993304730"],"leads":[{"id":8525197,"dateReceived":1532129083000,"dealId":17622258,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12870,"vehiclePrice":42900,"vehiclePlate":"","title":"() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)","message":"","score":87,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532129083000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":false},{"id":8524588,"dateReceived":1532124451000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"","score":82,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532124451000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":true}],"calls":[]}	79610927149	\N	2018-07-20 23:24:43	2018-08-11 12:59:49.791	NOVOS	frankreinaldo50@gmail.com	\N	\N	INTERNET	Edilene Souza Mendonca	\N	\N	LEAD	62993304730	11	\N	\N	82	\N	\N	\N
106	() 2019 Up! 1.0 12v E-Flex move up! (R$ 48,300)	01104751000110	{"lastInteraction":1534225571420,"userId":30378399,"userName":"Haroldo Nascimento","userEmail":"haroldo.bang@gmail.com","userCpf":"1142189198","userPhones":["64992941941"],"leads":[{"id":8799146,"dateReceived":1534225571000,"dealId":17730606,"dealerId":1134846,"vehicleTrimId":28762,"financingInstallmentsNumber":48,"financingInputValue":14490,"vehiclePrice":48300,"vehiclePlate":"","title":"() 2019 Up! 1.0 12v E-Flex move up! (R$ 48,300)","message":"","score":53,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534225571000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":false}],"calls":[]}	1142189198	\N	2018-08-14 05:46:11	2018-08-17 18:14:33.07	NOVOS	haroldo.bang@gmail.com	\N	\N	INTERNET	Haroldo Nascimento	HAROLDO DAS CHAGAS NASCIMENTO	\N	DESCARTADO	64992941941	\N	\N	\N	82	10	3	ICarros
107	() 2019 Polo 200 TSI Comfortline (Aut) (R$ 68,000)	01104751000110	{"lastInteraction":1534204104785,"userId":33175089,"userName":"Fernandes","userEmail":"guimfernandes@hotmail.com","userCpf":"76178536100","userPhones":["62991182033"],"leads":[{"id":8795878,"dateReceived":1534204104000,"dealId":17751872,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":20400,"vehiclePrice":68000,"vehiclePlate":"","title":"() 2019 Polo 200 TSI Comfortline (Aut) (R$ 68,000)","message":"","score":99,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534204104000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":false}],"calls":[]}	76178536100	\N	2018-08-13 23:48:24	2018-08-17 18:14:50.662	NOVOS	guimfernandes@hotmail.com	\N	\N	INTERNET	Fernandes	WALDEMIR FERNANDES DA SILVA	\N	DESCARTADO	62991182033	\N	\N	\N	82	11	3	ICarros
109	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1534086310727,"userId":24640360,"userName":"Sandro Alves ","userEmail":"sandroalves66@outlook.com","userCpf":"6047418120","userPhones":["62984238360"],"leads":[{"id":8778459,"dateReceived":1534086285000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":60,"financingInputValue":7000,"financingInstallmentsValue":938.7899780273438,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534086310000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8775376,"dateReceived":1534039352000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":6000,"financingInstallmentsValue":1510.0999755859375,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534039365000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8775161,"bankTransactionId":65771601,"dateReceived":1534038020000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":60,"financingInputValue":6000,"financingInstallmentsValue":965.6400146484375,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":true,"callFrom":-1,"status":3,"typeLead":"FICHA","lastInteraction":1534038147000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	6047418120	\N	2018-08-12 15:04:45	2018-08-17 18:15:20.883	NOVOS	sandroalves66@outlook.com	\N	\N	INTERNET	Sandro Alves 	SANDRO ALVES NEVES	\N	DESCARTADO	62984238360	\N	\N	\N	82	13	3	ICarros
108	() 2018 Virtus 200 TSI Highline (Aut) (Flex) (R$ 87,900)	01104751000110	{"lastInteraction":1534166042271,"userId":2199436,"userName":"Geraldo Luciano","userEmail":"geraldo.silva@dnpm.gov.br","userCpf":"11849622191","userPhones":["62992798620"],"leads":[{"id":8787608,"dateReceived":1534166015000,"dealId":17729685,"dealerId":1134846,"vehicleTrimId":28438,"financingInstallmentsNumber":48,"financingInputValue":57900,"financingInstallmentsValue":917.469970703125,"vehiclePrice":87900,"vehiclePlate":"","title":"() 2018 Virtus 200 TSI Highline (Aut) (Flex) (R$ 87,900)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534166042000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	11849622191	\N	2018-08-13 13:13:35	2018-08-17 18:15:08.625	NOVOS	geraldo.silva@dnpm.gov.br	\N	\N	INTERNET	Geraldo Luciano	GERALDO LUCIANO DA SILVA	\N	DESCARTADO	62992798620	\N	\N	\N	82	12	3	ICarros
79	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1531930790642,"userId":26238596,"userName":"Mateus De Souza Reis ","userEmail":"mateusdesouzareisreis@gmail.com","userCpf":"48677094253","userPhones":["6296732434"],"leads":[{"id":8504287,"dateReceived":1531930790000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","message":"","score":60,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531930790000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":true}],"calls":[]}	48677094253	\N	2018-07-18 16:19:50	2018-08-11 12:59:50.547	NOVOS	mateusdesouzareisreis@gmail.com	\N	\N	INTERNET	Mateus De Souza Reis 	\N	\N	LEAD	6296732434	11	\N	\N	82	\N	\N	\N
80	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	01104751000110	{"lastInteraction":1531878959623,"userId":32822584,"userName":"Anderson","userEmail":"acida2427@gmail.com","userCpf":"611138670","userPhones":["34999717993"],"leads":[{"id":8500397,"dateReceived":1531878959000,"dealId":17622258,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12870,"vehiclePrice":42900,"vehiclePlate":"","title":"() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)","message":"tenho carta de credito aprovada de r$ 27,000,00 +ou- mas 01 gol g3, ano 99/00, tenho interesse em um gol novo.\\r\\n","score":52,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531878959000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":false},{"id":8500365,"dateReceived":1531878746000,"dealId":17622258,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12870,"vehiclePrice":42900,"vehiclePlate":"","title":"() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)","message":"","score":52,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531878746000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":false}],"calls":[]}	611138670	\N	2018-07-18 01:55:59	2018-08-11 12:59:50.696	NOVOS	acida2427@gmail.com	\N	\N	INTERNET	Anderson	\N	\N	LEAD	34999717993	11	\N	\N	82	\N	\N	\N
125	\N	\N	\N	\N	\N	\N	2018-08-22 11:04:14.164	SEMINOVOS	\N	\N	\N	LIGACAO	bnvcbnbcv	\N	\N	DESCARTADO	(44) 44444-4444	\N	1626	3091	12	\N	3	\N
124	\N	\N	\N	\N	\N	\N	2018-08-22 11:02:43.926	SEMINOVOS	\N	\N	\N	LIGACAO	nbd	\N	\N	DESCARTADO	(44) 44444-4444	\N	1599	3091	144	\N	3	\N
110	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1534082109270,"userId":26456160,"userName":"Egnaldo","userEmail":"egnaldo_itba@hotmail.com","userCpf":"9433817460","userPhones":["3496661277"],"leads":[{"id":8777810,"dateReceived":1534082109000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534082109000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	9433817460	\N	2018-08-12 13:55:09	2018-08-17 18:15:40.395	NOVOS	egnaldo_itba@hotmail.com	\N	\N	INTERNET	Egnaldo	EGNALDO RODRIGUES COSTA	\N	DESCARTADO	3496661277	\N	\N	\N	82	14	3	ICarros
87	() 2019 Polo 1.0 (Flex) (R$ 54,999)	01104751000110	{"lastInteraction":1531474886771,"userId":32385442,"userName":"Robson Franco","userEmail":"rfranco16.92@gmail.com","userCpf":"4700990546","userPhones":["71996862758"],"leads":[{"id":8456645,"dateReceived":1531474886000,"dealId":18157309,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":16499,"vehiclePrice":54999,"vehiclePlate":"","title":"() 2019 Polo 1.0 (Flex) (R$ 54,999)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531474886000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	4700990546	\N	2018-07-13 09:41:26	2018-08-11 12:59:51.755	NOVOS	rfranco16.92@gmail.com	\N	\N	INTERNET	Robson Franco	Robson Franco	\N	LEAD	71996862758	11	\N	\N	82	\N	\N	\N
90	() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)	01104751000110	{"lastInteraction":1531423715915,"userId":32150648,"userName":"Deivid Nunes Da Silva","userEmail":"deivid.imoveisgyn02@gmail.com","userCpf":"1434113140","userPhones":["62992264335"],"leads":[{"id":8451615,"dateReceived":1531423665000,"dealId":18153176,"dealerId":1134846,"vehicleTrimId":28834,"financingInstallmentsNumber":48,"financingInputValue":30000,"financingInstallmentsValue":480.44000244140625,"vehiclePrice":45900,"vehiclePlate":"","title":"() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531423715000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	555.555.555-55	\N	2018-07-12 19:27:45	2018-08-11 12:59:52.294	NOVOS	deivid.imoveisgyn02@gmail.com	\N	\N	INTERNET	Deivid Nunes Da Silva	Deivid Nunes Da Silva	\N	DESCARTADO	62992264335	\N	1	3163	82	\N	1	\N
129	\N	\N	\N	\N	\N	\N	2018-08-22 11:09:33.671	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	fdsafd	\N	\N	DESCARTADO	\N	\N	1599	3163	15	\N	3	\N
132	\N	\N	\N	\N	\N	\N	2018-08-22 11:17:41.605	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	fdasf	\N	\N	DESCARTADO	\N	\N	1599	3163	144	\N	3	\N
127	\N	\N	\N	\N	\N	\N	2018-08-22 11:08:44.934	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	fdas	\N	\N	DESCARTADO	\N	\N	1599	3163	144	\N	3	\N
126	\N	\N	\N	\N	\N	\N	2018-08-22 11:06:54.631	VENDA_DIRETA	\N	\N	\N	SHOWROOM	gfdhg	\N	\N	DESCARTADO	(55) 55555-5555	\N	1537	3163	12	\N	3	\N
128	\N	\N	\N	\N	\N	\N	2018-08-22 11:09:09.853	NOVOS	teste@gmail.com	\N	\N	LIGACAO	dfas	\N	\N	DESCARTADO	\N	\N	1642	3091	144	\N	3	\N
93	() 2019 Polo 1.0 (Flex) (R$ 53,990)	01104751000110	{"lastInteraction":1531358352159,"userId":4267480,"userEmail":"senir.aps@hotmail.com","userCpf":"1082164119","userPhones":["62996280745"],"leads":[{"id":8445682,"dateReceived":1531358352000,"dealId":18157464,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":36,"financingInputValue":16197,"vehiclePrice":53990,"vehiclePlate":"","title":"() 2019 Polo 1.0 (Flex) (R$ 53,990)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531358352000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	1082164119	\N	2018-07-12 01:19:12	2018-08-11 12:59:53.3	NOVOS	senir.aps@hotmail.com	\N	\N	INTERNET		\N	\N	DESCARTADO	62996280745	\N	\N	\N	82	\N	3	\N
91	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1531412359831,"userId":35080730,"userName":"Thais","userEmail":"thaiscris1617@gmail.com","userCpf":"70236489178","userPhones":[],"leads":[{"id":8450111,"dateReceived":1531412359000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531412359000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	70236489178	\N	2018-07-12 16:19:19	2018-08-11 12:59:52.443	NOVOS	thaiscris1617@gmail.com	\N	\N	INTERNET	Thais	Thais Almeida	\N	DESCARTADO	(22) 22222-2222	\N	3	3163	82	\N	3	\N
101	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1534464255043,"userId":31735182,"userName":"Devid Firmino Da Costa ","userEmail":"devidfirmino150@gmail.com","userCpf":"10987748416","userPhones":["64999853115"],"leads":[{"id":8841312,"dateReceived":1534464205000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":5000,"financingInstallmentsValue":1137.8499755859375,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534464255000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	10987748416	\N	2018-08-17 00:03:25	2018-08-17 18:13:31.643	NOVOS	devidfirmino150@gmail.com	\N	\N	INTERNET	Devid Firmino Da Costa 	DEVID FIRMINO DA COSTA	\N	DESCARTADO	64999853115	\N	\N	\N	82	5	3	ICarros
100	2018/2019 Polo 200 TSI Highline (Flex) (Aut) 0km (R$ 79.999,00)	01104751000110	{"lastInteraction":1534505236437,"userId":10837296,"userName":"Robeto ","userEmail":"roberto13041972@hotmail.com","userCpf":"56657439172","userPhones":[],"leads":[{"id":8845473,"dateReceived":1534505150000,"dealId":18803711,"dealerId":1134846,"vehicleTrimId":28838,"financingInstallmentsNumber":48,"financingInputValue":15000,"financingInstallmentsValue":2033.800048828125,"vehiclePrice":79999,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Highline (Flex) (Aut) 0km (R$ 79.999,00)","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534505236000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	56657439172	\N	2018-08-17 11:25:50	2018-08-17 18:13:20.455	NOVOS	roberto13041972@hotmail.com	\N	\N	INTERNET	Robeto 	ROBERTO CARLOS DE LIMA	\N	DESCARTADO	\N	\N	\N	\N	82	4	3	ICarros
133	\N	\N	\N	\N	\N	\N	2018-08-22 11:18:28.01	VENDA_DIRETA	\N	\N	\N	SHOWROOM	dfsa	\N	\N	DESCARTADO	(33) 33333-3333	\N	1677	3163	12	\N	3	\N
131	\N	\N	\N	\N	\N	\N	2018-08-22 11:16:10.403	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	adfsfs	\N	\N	DESCARTADO	\N	\N	1641	3163	21	\N	3	\N
130	\N	\N	\N	\N	\N	\N	2018-08-22 11:13:52.372	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	gfdsgf	\N	\N	DESCARTADO	\N	\N	1641	3091	144	\N	3	\N
76	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1532006564158,"userId":35176562,"userName":"Diego Gomes Da Silva","userEmail":"diegothamire987@gmail.com","userCpf":"4464970109","userPhones":["66981015461"],"leads":[{"id":8511723,"dateReceived":1532006564000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":48,"financingInputValue":16470,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532006564000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	4464970109	\N	2018-07-19 13:22:44	2018-08-11 12:59:50.091	NOVOS	diegothamire987@gmail.com	\N	\N	INTERNET	Diego Gomes Da Silva	\N	\N	LEAD	66981015461	11	\N	\N	82	\N	\N	\N
67	2018/2018 Polo 200 TSI Comfortline (Aut) (Flex) 0km (R$ 66.300,00)	01104751000110	{"lastInteraction":1532355334159,"userId":32397267,"userName":"Cláudio","userEmail":"claudioavilarosa1@gmail.com","userCpf":"43343082520","userPhones":[],"leads":[{"id":8546285,"dateReceived":1532355334000,"dealId":17702683,"dealerId":1134846,"vehicleTrimId":28126,"financingInstallmentsNumber":48,"financingInputValue":19890,"vehiclePrice":66300,"vehiclePlate":"","title":"2018/2018 Polo 200 TSI Comfortline (Aut) (Flex) 0km (R$ 66.300,00)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532355334000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	43343082520	\N	2018-07-23 14:15:34	2018-08-11 12:59:48.726	NOVOS	claudioavilarosa1@gmail.com	\N	\N	INTERNET	Cláudio	\N	\N	LEAD	\N	11	\N	\N	82	\N	\N	\N
105	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 48.990,00)	01104751000110	{"lastInteraction":1534272552097,"userId":35507730,"userName":"Lara Morais","userEmail":"limorais2@gmail.com","userCpf":"3517455130","userPhones":["62982015350"],"leads":[{"id":8806508,"dateReceived":1534272552000,"dealId":17994867,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":14697,"vehiclePrice":48990,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 48.990,00)","message":"","score":97,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534272552000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":true}],"calls":[]}	3517455130	\N	2018-08-14 18:49:12	2018-08-17 18:14:20.376	NOVOS	limorais2@gmail.com	\N	\N	INTERNET	Lara Morais	LARA INACIO DE MORAIS	\N	DESCARTADO	62982015350	\N	\N	\N	82	9	3	ICarros
134	\N	\N	\N	\N	\N	\N	2018-08-22 11:20:36.243	SEMINOVOS	teste@gmail.com	\N	\N	LIGACAO	asfdfd	\N	\N	DESCARTADO	\N	\N	1682	3091	12	\N	3	\N
111	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1534076105868,"userId":19299619,"userName":"Alamir Lopes Nogueira","userEmail":"alamirlopes@gmail.com","userCpf":"73096792100","userPhones":["85741745"],"leads":[{"id":8777174,"dateReceived":1534076105000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","message":"","score":89,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534076105000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":true},{"id":8777151,"dateReceived":1534075870000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":36,"financingInputValue":30000,"financingInstallmentsValue":975.280029296875,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","score":89,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1534075919000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":true}],"calls":[]}	73096792100	\N	2018-08-12 12:15:05	2018-08-17 18:15:57.829	NOVOS	alamirlopes@gmail.com	\N	\N	INTERNET	Alamir Lopes Nogueira	ALAMIR LOPES NOGUEIRA	\N	DESCARTADO	85741745	\N	\N	\N	82	15	3	ICarros
68	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)	01104751000110	{"lastInteraction":1532351525177,"userId":35212011,"userName":"Zilma Dos Passos Serra","userEmail":"zilma.27@hotmail.com","userCpf":"43773893191","userPhones":["6232076617"],"leads":[{"id":8545547,"dateReceived":1532351525000,"dealId":18006680,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":15299,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532351525000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":true}],"calls":[]}	43773893191	\N	2018-07-23 13:12:05	2018-08-11 12:59:48.878	NOVOS	zilma.27@hotmail.com	\N	\N	INTERNET	Zilma Dos Passos Serra	\N	\N	LEAD	6232076617	11	\N	\N	82	\N	\N	\N
60	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1532695874246,"userId":20727179,"userName":"Valdir Ferrari","userEmail":"fernando.ferrari2010@gmail.com","userCpf":"19031777072","userPhones":["6692221525"],"leads":[{"id":8593090,"dateReceived":1532695857000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":48,"financingInputValue":30000,"financingInstallmentsValue":758.4099731445312,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532695874000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false},{"id":8502082,"dateReceived":1531914600000,"dealId":18153176,"dealerId":1134846,"vehicleTrimId":28834,"financingInstallmentsNumber":48,"financingInputValue":25000,"financingInstallmentsValue":641.530029296875,"vehiclePrice":45900,"vehiclePlate":"","title":"() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531914653000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false},{"id":8481991,"dateReceived":1531744958000,"dealId":18153176,"dealerId":1134846,"vehicleTrimId":28834,"financingInstallmentsNumber":18,"financingInputValue":30000,"financingInstallmentsValue":1115.0699462890625,"vehiclePrice":45900,"vehiclePlate":"","title":"() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531744984000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	19031777072	\N	2018-07-27 12:50:57	2018-08-11 12:59:47.672	NOVOS	fernando.ferrari2010@gmail.com	\N	\N	INTERNET	Valdir Ferrari	\N	\N	LEAD	6692221525	11	\N	\N	82	\N	\N	\N
140	\N	\N	\N	\N	\N	\N	2018-08-22 11:30:51.393	NOVOS	teste@gmail.com	\N	\N	SHOWROOM	hgc	\N	\N	PRE_LEAD	\N	\N	1599	3091	144	\N	\N	\N
141	\N	\N	\N	\N	\N	\N	2018-08-22 11:32:10.119	SEMINOVOS	teste@gmail.com	\N	\N	LIGACAO	gfdsgs	gfdsgs	\N	DESCARTADO	\N	\N	1738	3428	82	\N	4	\N
137	\N	\N	\N	\N	\N	\N	2018-08-22 11:27:58.893	NOVOS	teste@gmail.com	\N	\N	LIGACAO	fdsaf	\N	\N	DESCARTADO	\N	\N	1645	3163	144	\N	2	\N
138	\N	\N	\N	\N	\N	\N	2018-08-22 11:28:46.054	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	 bbbbbbbbb	 bbbbbbbbb	\N	LEAD	\N	\N	1599	3163	12	\N	\N	\N
142	\N	\N	\N	\N	\N	\N	2018-08-22 11:33:12.449	NOVOS	teste@gmail.com	\N	\N	LIGACAO	fsgdf	fsgdf	\N	LEAD	\N	\N	1738	3091	52	\N	\N	\N
136	\N	\N	\N	333.333.333-33	\N	\N	2018-08-22 11:26:53.681	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	dsafasdf	dsafasdf	\N	LEAD	\N	\N	1755	3163	144	\N	\N	\N
135	\N	\N	\N	\N	\N	\N	2018-08-22 11:25:36.888	NOVOS	teste@gmail.com	\N	\N	SHOWROOM	dfsdf	dfsdf	\N	LEAD	\N	\N	1586	3163	82	\N	\N	\N
139	\N	\N	\N	\N	\N	\N	2018-08-22 11:29:53.582	VENDA_DIRETA	teste@gmail.com	\N	\N	SHOWROOM	nmv	nmv	\N	LEAD	\N	\N	1635	3163	144	\N	\N	\N
143	\N	\N	\N	\N	\N	\N	2018-08-22 13:19:33.592	SEMINOVOS	\N	\N	\N	SHOWROOM	fgsdgfds	\N	\N	PRE_LEAD	(44) 44444-4444	\N	1642	3163	144	\N	\N	\N
15	\N	\N	\N	\N	\N	\N	2018-08-07 21:06:32.244	VENDA_DIRETA	\N	\N	\N	SHOWROOM	teste	teste	\N	DESCARTADO	\N	11	1	3428	111	\N	1	\N
24	\N	\N	\N	555.150.15-66	\N	\N	2018-08-08 16:25:19.022	NOVOS	teste@gmail.com	\N	\N	SHOWROOM	novo lead	\N	\N	DESCARTADO	(62) 64666-2626	11	\N	3428	82	\N	1	\N
23	\N	\N	\N	\N	\N	\N	2018-08-08 13:22:02.296	NOVOS	\N	\N	\N	SHOWROOM	teste	\N	\N	DESCARTADO	(99) 99999-9999	11	2	3428	82	\N	\N	\N
18	\N	\N	\N	\N	\N	\N	2018-08-08 01:05:33.363	SEMINOVOS	\N	\N	\N	SHOWROOM	teste	teste	\N	DESCARTADO	\N	11	2	3428	82	\N	1	\N
26	Faça Pós-Graduação sem sair de casa!	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body><img src="http://ad.zanox.com/ppv/?44847928C1983061774" align="bottom" width="1" height="1" border="0" hspace="1"><table id="Table_01" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; width:600px; height:1428px"><tbody><tr><td colspan="8"><a href="http://tracking.cuponsnanet.com/tracking/click?d=I0ElNBf5WV6edzTFEBXWiWlFFxu5hsLPKiHI3rTU6rYkoY4xPIe_aNtbRQi4k_-Qbq1mAR1SzYtybcRHSGeBDXSQzJbN3XHKQ-5zONisjANIAlkRl1Q8BKp2KVn5x17DflY700Jq5E5ugFQSV2W7BJSZukH5vxzNC4wTvmWV9MSt69HpBCTB9cCrQBWgYPeScc9BlactUDUce3xvRtCwyRACPUvFSr7IFUWWctjhcAmEX7wftrB-cD157EFscIzjxArp9e7o5Ixhoz6zuC-DiShpkkWXWLob9iJ6jsgoCQZJ0" target="_blank"><img id="K00817_Awin-Unopar_01" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_01.jpg" width="600" height="314" alt="" style="display:block"></a></td></tr><tr><td colspan="5"><img id="K00817_Awin-Unopar_02" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_02.jpg" width="272" height="49" alt="" style="display:block"></td><td colspan="2"><a href="http://tracking.cuponsnanet.com/tracking/click?d=I0ElNBf5WV6edzTFEBXWiWlFFxu5hsLPKiHI3rTU6rYkoY4xPIe_aNtbRQi4k_-Qbq1mAR1SzYtybcRHSGeBDXSQzJbN3XHKQ-5zONisjANIAlkRl1Q8BKp2KVn5x17DflY700Jq5E5ugFQSV2W7BJSZukH5vxzNC4wTvmWV9MSt69HpBCTB9cCrQBWgYPeScc9BlactUDUce3xvRtCwyRACPUvFSr7IFUWWctjhcAmsu87bguCbFmciixw106PAvPyqPdU9YNsc3eRiNpQaef7KrtfPJKEcZNuuNF_sGN6J0" target="_blank"><img id="corteK00817_Emkt-Awin-Unopar_550px-copy_03" src="http://a1.zanox.com/images/programs/19595/20180316/_corteK00817_Emkt-Awin-Unopar_550px-copy_03.jpg" width="172" height="49" border="0" alt="" style="display:block"></a></td><td><img id="K00817_Awin-Unopar_04" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_04.jpg" width="156" height="49" alt="" style="display:block"></td></tr><tr><td colspan="8"><img id="K00817_Awin-Unopar_05" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_05.jpg" width="600" height="45" alt="" style="display:block"></td></tr><tr><td width="57" height="67" colspan="2" nowrap="nowrap" bgcolor="#FFFFFF"></td><td width="543" height="67" colspan="6" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:23px; color:#4d4d4f">A Universidade<b>líder em EAD</b> no país apresenta<br>a <b>Pós Digital.</b> </span></td></tr><tr><td width="57" height="26" colspan="2" nowrap="nowrap" bgcolor="#FFFFFF"></td><td width="543" height="26" colspan="6" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:18px; color:#1f99d5"><b>Nas modalidades:</b></span></td></tr><tr><td colspan="8"><img id="K00817_Awin-Unopar_10" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_10.jpg" width="600" height="7" alt="" style="display:block"></td></tr><tr><td colspan="3"><img id="K00817_Awin-Unopar_11" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_11.jpg" width="142" height="24" alt="" style="display:block"></td><td width="458" height="24" colspan="5" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:18px; color:#4d4d4f"><b>DIGITAL</b></span></td></tr><tr><td colspan="3"><img id="K00817_Awin-Unopar_13" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_13.jpg" width="142" height="76" alt="" style="display:block"></td><td width="458" height="76" colspan="5" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:16px; color:#4d4d4f"><b>Aulas 100% online,</b> com tutores disponíveis para<br>auxiliá-lo durante o curso. Certificação igual à do<br>presencial. </span></td></tr><tr><td colspan="3"><img id="K00817_Awin-Unopar_15" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_15.jpg" width="142" height="24" alt="" style="display:block"></td><td width="458" height="24" colspan="5" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:18px; color:#4d4d4f"><b>SEMIPRESENCIAL</b></span></td></tr><tr><td colspan="3"><img id="K00817_Awin-Unopar_17" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_17.jpg" width="142" height="95" alt="" style="display:block"></td><td width="458" height="95" colspan="5" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:16px; color:#4d4d4f">Reúne<b>o melhor do ensino presencial e a distância,<br></b>com o networking e os debates de sala de aula<br>somados à flexibilidade de estudar como, onde<br>e quando quiser. </span></td></tr><tr><td colspan="3"><img id="K00817_Awin-Unopar_19" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_19.jpg" width="142" height="24" alt="" style="display:block"></td><td width="458" height="24" colspan="5" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:18px; color:#4d4d4f"><b>PRESENCIAL</b></span></td></tr><tr><td colspan="3"><img id="K00817_Awin-Unopar_21" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_21.jpg" width="142" height="78" alt="" style="display:block"></td><td width="458" height="78" colspan="5" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:16px; color:#4d4d4f">Esse é o modelo para quem prefere um contato mais<br>próximo com professores e colegas e dispõe de tempo<br>para participar das aulas. </span></td></tr><tr><td colspan="8"><a href="http://tracking.cuponsnanet.com/tracking/click?d=I0ElNBf5WV6edzTFEBXWiWlFFxu5hsLPKiHI3rTU6rYkoY4xPIe_aNtbRQi4k_-Qbq1mAR1SzYtybcRHSGeBDXSQzJbN3XHKQ-5zONisjANIAlkRl1Q8BKp2KVn5x17DflY700Jq5E5ugFQSV2W7BJSZukH5vxzNC4wTvmWV9MSt69HpBCTB9cCrQBWgYPeScc9BlactUDUce3xvRtCwyRACPUvFSr7IFUWWctjhcAn9ViDlHFJUFbQDPE348v4U-RyT71Cd7QGYekhZwZ4QJtZ2Lz6AGx_M0iEJ2EHfd-2g0" target="_blank"><img id="K00817_Awin-Unopar_23" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_23.jpg" width="600" height="140" alt="" style="display:block"></a></td></tr><tr><td colspan="8"><a href="http://tracking.cuponsnanet.com/tracking/click?d=I0ElNBf5WV6edzTFEBXWiWlFFxu5hsLPKiHI3rTU6rYkoY4xPIe_aNtbRQi4k_-Qbq1mAR1SzYtybcRHSGeBDXSQzJbN3XHKQ-5zONisjANIAlkRl1Q8BKp2KVn5x17DflY700Jq5E5ugFQSV2W7BJSZukH5vxzNC4wTvmWV9MSt69HpBCTB9cCrQBWgYPeScc9BlactUDUce3xvRtCwyRACPUvFSr7IFUWWctjhcAntMMqdpuodt4ebfgsRQgm58qjRiG-eztclujbd3zoY6X35l5iWTebDF-4S2DfH5ZPT0" target="_blank"><img id="K00817_Awin-Unopar_24.jpg" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_24.jpg" width="600" height="254" alt="" style="display:block"></a></td></tr><tr><td colspan="4"><img id="K00817_Awin-Unopar_25" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_25.jpg" width="194" height="61" alt="" style="display:block"></td><td colspan="2"><a href="http://tracking.cuponsnanet.com/tracking/click?d=I0ElNBf5WV6edzTFEBXWiWlFFxu5hsLPKiHI3rTU6rYkoY4xPIe_aNtbRQi4k_-Qbq1mAR1SzYtybcRHSGeBDXSQzJbN3XHKQ-5zONisjANIAlkRl1Q8BKp2KVn5x17DflY700Jq5E5ugFQSV2W7BJSZukH5vxzNC4wTvmWV9MSt69HpBCTB9cCrQBWgYPeScc9BlactUDUce3xvRtCwyRACPUvFSr7IFUWWctjhcAkDQ6OtTDeoU_meE344FxOyJnEG8rIKUcILL7L2xUWeZpCKrrBAXeu3PItUTqqfAVE90" target="_blank"><img id="corteK00817_Emkt-Awin-Unopar_550px-copy_26" src="http://a1.zanox.com/images/programs/19595/20180316/_corteK00817_Emkt-Awin-Unopar_550px-copy_26.jpg" width="189" height="61" border="0" alt="" style="display:block"></a></td><td colspan="2"><img id="K00817_Awin-Unopar_27" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_27.jpg" width="217" height="61" alt="" style="display:block"></td></tr><tr><td colspan="8"><img id="K00817_Awin-Unopar_28" src="http://a1.zanox.com/images/programs/19595/20180316/K00817_Awin-Unopar_28.jpg" width="600" height="107" alt="" style="display:block"></td></tr><tr><td width="42" height="37" nowrap="nowrap" bgcolor="#FFFFFF"></td><td width="558" height="37" colspan="7" bgcolor="#FFFFFF" valign="top"><span style="font-family:Calibri,sans-serif; font-size:9px; color:#4d4d4f">Os cursos presenciais e semipresenciais estão sujeitos a disponibilidade de vagas e formação de turmas. Os cursos das modalidades EAD<br>e semipresenciais são certificados pela universidade Pitágoras Unopar e possuem a mesma validade que um curso presencial. Consulte os<br>cursos disponíveis, as ofertas podem variar sem aviso prévio. Mais informações em portalpos.com.br/unopar.</span></td></tr><tr><td width="42" nowrap="nowrap"></td><td width="15" nowrap="nowrap"></td><td width="85" nowrap="nowrap"></td><td width="52" nowrap="nowrap"></td><td width="78" nowrap="nowrap"></td><td width="111" nowrap="nowrap"></td><td width="61" nowrap="nowrap"></td><td width="156" nowrap="nowrap"></td></tr></tbody></table><div style="text-align:center; background-color:#fff; padding-top:10px; padding-bottom:10px; font-size:8pt; font-family:sans-serif"><p style="text-align:center; text-decoration:none; color:#666">Cupons na Net, São Paulo, São Paulo, SP, 01000-000, Brazil</p></div><img src="http://tracking.cuponsnanet.com/tracking/open?msgid=dzCGsYlRoJSMMv4yPNx17g2" alt="" style="width:1px; height:1px"><div style="text-align:center; background-color:#fff; padding-top:10px; padding-bottom:10px; font-size:8pt; font-family:sans-serif"><a href="http://tracking.cuponsnanet.com/tracking/unsubscribe?msgid=dzCGsYlRoJSMMv4yPNx17g2" style="text-align:center; text-decoration:none; color:#666">UNSUBSCRIBE</a></div></body></html>	\N	\N	2018-08-11 12:25:44	2018-08-11 12:59:12.382	SEMINOVOS	\N	\N	\N	INTERNET	\N	\N	\N	DESCARTADO	\N	11	\N	3163	82	\N	3	\N
28	Fale Conosco 5b4dd5e80bb6e	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body><h3>Form Details</h3><table border="1" width="100%"><tbody><tr><td>name</td><td>jhony</td></tr><tr><td>email</td><td>mayconmotabrandao@hotmail.com</td></tr><tr><td>*DDD</td><td>maycon</td></tr><tr><td>*Telefone</td><td>95518149</td></tr><tr><td>tipoDeContato</td><td>Tipo de contato</td></tr><tr><td>cidade</td><td>Goiânia</td></tr><tr><td>LojaPreferida</td><td>c01104751000110@saganet.net.br</td></tr><tr><td>mailsToSend</td><td>c01104751000110@saganet.net.br,Tipo de contato</td></tr><tr><td>comentarios</td><td></td></tr><tr><td>utm_campaign</td><td>Busca-Paga-go-t7:model</td></tr><tr><td>utm_source</td><td>google</td></tr><tr><td>utm_medium</td><td>cpc</td></tr><tr><td>utm_uptracs</td><td>Busca-Paga-go-t7:model</td></tr><tr><td>received_from_url</td><td>http://www.sagavw.com.br/contato/fale-conosco.html?loja=saga-t-7-volkswagen&amp;utm_campaign=busca-paga-go-t7:model</td></tr></tbody></table></body></html>	\N	\N	2018-08-11 11:22:56	2018-08-11 12:59:14.703	NOVOS	mayconmotabrandao@hotmail.com	\N	\N	INTERNET	jhony	\N	\N	DESCARTADO		11	\N	\N	82	\N	3	\N
27	SAGA Proposta 5b4dd5e5e6d75	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body><h3>Form Details</h3><table border="1" width="100%"><tbody><tr><td>name</td><td>Lucasmalheirolimafernandesfontes</td></tr><tr><td>phone</td><td>35973293</td></tr><tr><td>email</td><td>lucasestudante2016@gmail.com</td></tr><tr><td>estado</td><td>GO</td></tr><tr><td>dealer-store-one</td><td>c01104751000110@saganet.net.br</td></tr><tr><td>comentarios</td><td>Qro carro</td></tr><tr><td>campo_veiculo_form</td><td>Volkswagen</td></tr><tr><td>campo_versao_form</td><td>Trendline 1.0</td></tr><tr><td>campo_ano_form</td><td>2018</td></tr><tr><td>campo_origem_form</td><td>mdp</td></tr><tr><td>utm_campaign</td><td>Busca-Paga-go-t7:model</td></tr><tr><td>utm_source</td><td>google</td></tr><tr><td>utm_medium</td><td>cpc</td></tr><tr><td>received_from_url</td><td>http://www.sagavw.com.br/volkswagen/gol.html?loja=saga-t-7-volkswagen&amp;utm_campaign=busca-paga-go-t7:model&amp;utm_source=google&amp;utm_medium=cpc</td></tr></tbody></table></body></html>	\N	\N	2018-08-11 12:24:02	2018-08-11 12:59:14.292	NOVOS	lucasestudante2016@gmail.com	\N	\N	INTERNET	Lucasmalheirolimafernandesfontes	\N	\N	DESCARTADO	35973293	11	\N	\N	82	\N	3	\N
36	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1533952598900,"userId":35450946,"userEmail":"pollyannasantos4@gmail.com","userCpf":"4072555169","userPhones":["6286353779"],"leads":[{"id":8765762,"dateReceived":1533952598000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":60,"financingInputValue":12000,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533952598000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8748642,"dateReceived":1533842112000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":60,"financingInputValue":12300,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533842112000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	4072555169	\N	2018-08-11 01:56:38	2018-08-11 12:59:43.376	NOVOS	pollyannasantos4@gmail.com	\N	\N	INTERNET		\N	\N	DESCARTADO	6286353779	11	\N	\N	82	\N	3	\N
147	\N	\N	\N	\N	\N	\N	2018-08-22 13:39:08.395	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	fsdaf	\N	\N	PRE_LEAD	\N	\N	1755	3163	52	\N	\N	\N
148	\N	\N	\N	\N	\N	\N	2018-08-22 13:39:28.305	SEMINOVOS	teste@gmail.com	\N	\N	LIGACAO	dasf	\N	\N	PRE_LEAD	\N	\N	1755	3163	144	\N	\N	\N
144	\N	\N	\N	\N	\N	\N	2018-08-22 13:34:41.346	NOVOS	teste@gmail.com	\N	\N	LIGACAO	hfdhgdf	\N	\N	DESCARTADO	\N	\N	1641	3091	82	\N	4	\N
146	\N	\N	\N	\N	\N	\N	2018-08-22 13:38:29.332	NOVOS	teste@gmail.com	\N	\N	LIGACAO	vfsz	\N	\N	DESCARTADO	\N	\N	1641	3163	144	\N	1	\N
149	\N	\N	\N	\N	\N	\N	2018-08-22 13:51:48.784	SEMINOVOS	\N	\N	\N	LIGACAO	xcgvfg	\N	\N	DESCARTADO	(44) 44444-4444	\N	1755	3091	144	\N	4	\N
44	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1533596880084,"userId":35414300,"userEmail":"didaaldenira@gmail.vom","userCpf":"86127799104","userPhones":["62993374271"],"leads":[{"id":8711447,"dateReceived":1533596880000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":36810,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533596880000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	86127799104	\N	2018-08-06 23:08:00	2018-08-11 12:59:45.245	NOVOS	didaaldenira@gmail.vom	\N	\N	INTERNET		\N	\N	LEAD	62993374271	11	\N	\N	82	\N	\N	\N
41	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1533663308887,"userId":31257021,"userName":"Elba","userEmail":"elbacris83@gmail.com","userCpf":"97802115191","userPhones":["996452165"],"leads":[{"id":8720554,"dateReceived":1533663237000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533663308000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	97802115191	\N	2018-08-07 17:33:57	2018-08-11 12:59:44.696	NOVOS	elbacris83@gmail.com	\N	\N	INTERNET	Elba	\N	\N	LEAD	996452165	11	\N	\N	82	\N	\N	\N
42	2018/2018 Polo 200 TSI Comfortline (Aut) (Flex) 0km (R$ 66.300,00)	01104751000110	{"lastInteraction":1533660701861,"userId":27135220,"userName":"Vinícius Cogo","userEmail":"viniciuscogo@live.com","userCpf":"502590246","userPhones":["62991053858"],"leads":[{"id":8720072,"dateReceived":1533660701000,"dealId":17702683,"dealerId":1134846,"vehicleTrimId":28126,"financingInstallmentsNumber":48,"financingInputValue":19890,"vehiclePrice":66300,"vehiclePlate":"","title":"2018/2018 Polo 200 TSI Comfortline (Aut) (Flex) 0km (R$ 66.300,00)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533660701000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	502590246	\N	2018-08-07 16:51:41	2018-08-11 12:59:44.846	NOVOS	viniciuscogo@live.com	\N	\N	INTERNET	Vinícius Cogo	\N	\N	LEAD	62991053858	11	\N	\N	82	\N	\N	\N
50	2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)	01104751000110	{"lastInteraction":1533148758419,"userId":26445526,"userName":"Luan Silva Mendonça","userEmail":"luansm9@gmail.com","userCpf":"89091710225","userPhones":["69992973084"],"leads":[{"id":8655830,"bankTransactionId":65642687,"dateReceived":1533148605000,"dealId":18129301,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":16497,"financingInstallmentsValue":1168.3800048828125,"vehiclePrice":54990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":true,"callFrom":-1,"status":3,"typeLead":"FICHA","lastInteraction":1533148758000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	89091710225	\N	2018-08-01 18:36:45	2018-08-11 12:59:46.159	NOVOS	luansm9@gmail.com	\N	\N	INTERNET	Luan Silva Mendonça	\N	\N	LEAD	69992973084	11	\N	\N	82	\N	\N	\N
145	\N	\N	\N	\N	\N	\N	2018-08-22 13:38:00.969	SEMINOVOS	teste@gmail.com	\N	\N	SHOWROOM	jnbg	\N	\N	PRE_LEAD	\N	\N	1641	3163	144	\N	\N	\N
34	Não é possível entregar: SAGA TestDrive 5b4dd5de308e5	01.104.751/0001-10	Your message  Subject: Enc: SAGA TestDrive 5b4dd5de308e5was not delivered to:  mario.hjunior@saganet.com.brbecause:  User mario.hjunior (mario.hjunior@saganet.com.br) not listed in Domino Directory	\N	\N	2018-08-10 21:27:00	2018-08-11 12:59:19.137	NOVOS	\N	\N	\N	INTERNET	\N	\N	\N	DESCARTADO	\N	11	\N	\N	82	\N	2	\N
35	Não é possível entregar: SAGA TestDrive 5b4dd5de308e5	01.104.751/0001-10	Your message  Subject: Enc: SAGA TestDrive 5b4dd5de308e5was not delivered to:  lucas.coliveira@saganet.com.brbecause:  User lucas.coliveira (lucas.coliveira@saganet.com.br) not listed in Domino Directory	\N	\N	2018-08-10 21:26:59	2018-08-11 12:59:19.286	NOVOS	\N	\N	\N	INTERNET	\N	\N	\N	DESCARTADO	\N	11	\N	\N	82	\N	4	\N
40	2018/2019 Up! 1.0 12v E-Flex move up! 0km (R$ 48.300,00)	01104751000110	{"lastInteraction":1533725955427,"userId":30974446,"userName":"Clarissa","userEmail":"clarissaramos@gmail.com","userCpf":"31511158883","userPhones":["62981997725"],"leads":[{"id":8730308,"dateReceived":1533725901000,"dealId":17730606,"dealerId":1134846,"vehicleTrimId":28762,"financingInstallmentsNumber":60,"financingInputValue":10000,"financingInstallmentsValue":1043.3499755859375,"vehiclePrice":48300,"vehiclePlate":"","title":"2018/2019 Up! 1.0 12v E-Flex move up! 0km (R$ 48.300,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533725955000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	31511158883	\N	2018-08-08 10:58:21	2018-08-11 12:59:44.541	NOVOS	clarissaramos@gmail.com	\N	\N	INTERNET	Clarissa	\N	\N	DESCARTADO	62981997725	11	\N	\N	82	\N	4	\N
33	Lojas 5b5891b0573c5	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=utf-8"></head><body><h3>Form Details</h3><table border="1" width="100%"><tbody><tr><td>name</td><td>Aline </td></tr><tr><td>phone</td><td>64996107719</td></tr><tr><td>dealer-store-three</td><td>leadsvwgyn@saganet.net.br,helton.johnatas@saganet.com.br, agr108@saganet.com.br</td></tr><tr><td>received_from_url</td><td>http://www.sagavw.com.br/lojas/saga-t-7-volkswagen.html</td></tr></tbody></table></body></html>	\N	\N	2018-08-11 00:19:57	2018-08-11 12:59:18.985	NOVOS		\N	\N	INTERNET	Aline	Aline	\N	DESCARTADO	64996107719	11	\N	3428	82	\N	4	\N
52	2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)	01104751000110	{"lastInteraction":1533133973958,"userId":32691255,"userEmail":"tarcisiorezend@gmail.com","userCpf":"3553492139","userPhones":["62985070241"],"leads":[{"id":8652721,"dateReceived":1533133958000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":25000,"financingInstallmentsValue":887.6300048828125,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533133973000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8652194,"dateReceived":1533131927000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":25000,"financingInstallmentsValue":887.6300048828125,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533131944000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8643463,"dateReceived":1533069488000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":36,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533069488000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8643449,"dateReceived":1533069428000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":16197,"financingInstallmentsValue":1147.699951171875,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533069432000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8643294,"dateReceived":1533068748000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":16197,"financingInstallmentsValue":1147.699951171875,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533068752000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8643279,"dateReceived":1533068682000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":36,"financingInputValue":17097,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533068682000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8643271,"dateReceived":1533068636000,"dealId":18018786,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":36,"financingInputValue":20070,"vehiclePrice":66900,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 66.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533068636000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	3553492139	\N	2018-08-01 14:32:38	2018-08-11 12:59:46.461	NOVOS	tarcisiorezend@gmail.com	\N	\N	INTERNET		\N	\N	LEAD	62985070241	11	\N	\N	82	\N	\N	\N
58	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)	01104751000110	{"lastInteraction":1532878290728,"userId":18148895,"userName":"Dario","userEmail":"dariopadrao@hotmail.com","userCpf":"299128148","userPhones":["62991963883"],"leads":[{"id":8613119,"dateReceived":1532878203000,"dealId":17800473,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":30000,"financingInstallmentsValue":601.5399780273438,"vehiclePrice":49999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532878290000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8613063,"dateReceived":1532877877000,"dealId":17800473,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":26500,"financingInstallmentsValue":704.9400024414062,"vehiclePrice":49999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532878001000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	299128148	\N	2018-07-29 15:30:03	2018-08-11 12:59:47.365	NOVOS	dariopadrao@hotmail.com	\N	\N	INTERNET	Dario	\N	\N	LEAD	62991963883	11	\N	\N	82	\N	\N	\N
56	() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)	01104751000110	{"lastInteraction":1532884595500,"userId":20904390,"userName":"Jose Nilo Da Cost","userEmail":"jniloneto@gmail.com","userCpf":"15118452600","userPhones":["64999542423"],"leads":[{"id":8614158,"bankTransactionId":65599647,"dateReceived":1532884461000,"dealId":17891672,"dealerId":1134846,"vehicleTrimId":28841,"financingInstallmentsNumber":48,"financingInputValue":25917,"financingInstallmentsValue":1817.72998046875,"vehiclePrice":86390,"vehiclePlate":"","title":"() 2019 Virtus 200 TSI Highline (Flex) (Aut) (R$ 86,390)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":true,"callFrom":-1,"status":3,"typeLead":"FICHA","lastInteraction":1532884595000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	15118452600	\N	2018-07-29 17:14:21	2018-08-11 12:59:47.064	NOVOS	jniloneto@gmail.com	\N	\N	INTERNET	Jose Nilo Da Cost	\N	\N	LEAD	64999542423	11	\N	\N	82	\N	\N	\N
65	() 2018 Polo 200 TSI Comfortline (Aut) (Flex) (R$ 66,500)	01104751000110	{"lastInteraction":1532411468656,"userId":34378342,"userName":"Ff","userEmail":"juniolopesdeassiss@yahoo.com.br","userCpf":"16892020178","userPhones":[],"leads":[{"id":8555578,"dateReceived":1532411468000,"dealId":17702614,"dealerId":1134846,"vehicleTrimId":28126,"financingInstallmentsNumber":48,"financingInputValue":19950,"vehiclePrice":66500,"vehiclePlate":"","title":"() 2018 Polo 200 TSI Comfortline (Aut) (Flex) (R$ 66,500)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532411468000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	16892020178	\N	2018-07-24 05:51:08	2018-08-11 12:59:48.425	NOVOS	juniolopesdeassiss@yahoo.com.br	\N	\N	INTERNET	Ff	\N	\N	LEAD	\N	11	\N	\N	82	\N	\N	\N
64	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)	01104751000110	{"lastInteraction":1532453556172,"userId":31445817,"userName":"Maria Cleiane Almeida Dos Sant","userEmail":"cleianepssoa@gmail.com","userCpf":"82606994191","userPhones":["62983100110"],"leads":[{"id":8559607,"bankTransactionId":65535415,"dateReceived":1532453197000,"dealId":18006680,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":15299,"financingInstallmentsValue":1096.0400390625,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":true,"callFrom":-1,"status":3,"typeLead":"FICHA","lastInteraction":1532453556000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	82606994191	\N	2018-07-24 17:26:37	2018-08-11 12:59:48.272	NOVOS	cleianepssoa@gmail.com	\N	\N	INTERNET	Maria Cleiane Almeida Dos Sant	\N	\N	LEAD	62983100110	11	\N	\N	82	\N	\N	\N
62	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1532654131491,"userId":35263361,"userName":"Paulo Eduardo Batista Tavares ","userEmail":"paulo-tavares18@hotmail.com","userCpf":"6791437189","userPhones":["64984030579"],"leads":[{"id":8590070,"dateReceived":1532654131000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":48,"financingInputValue":16470,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532654131000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	6791437189	\N	2018-07-27 01:15:31	2018-08-11 12:59:47.972	NOVOS	paulo-tavares18@hotmail.com	\N	\N	INTERNET	Paulo Eduardo Batista Tavares 	\N	\N	LEAD	64984030579	11	\N	\N	82	\N	\N	\N
150	\N	\N	\N	\N	\N	\N	2018-08-22 13:52:09.922	NOVOS	teste@gmail.com	\N	\N	SHOWROOM	fasdf	\N	\N	PRE_LEAD	\N	\N	1684	3163	144	\N	\N	\N
63	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1532515013404,"userId":29352586,"userName":"Pedro","userEmail":"pedrolouredo@live.com","userCpf":"5446625129","userPhones":["62999970417"],"leads":[{"id":8566603,"dateReceived":1532515013000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532515013000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	5446625129	\N	2018-07-25 10:36:53	2018-08-11 12:59:48.122	NOVOS	pedrolouredo@live.com	\N	\N	INTERNET	Pedro	\N	\N	LEAD	62999970417	11	\N	\N	82	\N	\N	\N
72	() 2018 Virtus 200 TSI Highline (Aut) (Flex) (R$ 86,990)	01104751000110	{"lastInteraction":1532227830587,"userId":29328219,"userName":"Pedro Santos Mamare","userEmail":"pedromamare@hotmail.com","userCpf":"4109842131","userPhones":["62982492445"],"leads":[{"id":8534728,"dateReceived":1532227830000,"dealId":17670293,"dealerId":1134846,"vehicleTrimId":28438,"financingInstallmentsNumber":48,"financingInputValue":24000,"vehiclePrice":86990,"vehiclePlate":"","title":"() 2018 Virtus 200 TSI Highline (Aut) (Flex) (R$ 86,990)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532227830000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	4109842131	\N	2018-07-22 02:50:30	2018-08-11 12:59:49.49	NOVOS	pedromamare@hotmail.com	\N	\N	INTERNET	Pedro Santos Mamare	\N	\N	LEAD	62982492445	11	\N	\N	82	\N	\N	\N
70	2018/2019 GOL 1.6 MSI (FLEX) 0KM (R$ 54.900,00)	01104751000110	{"lastInteraction":1532299260708,"userId":35207728,"userName":"Delgardo Gonçalves 021.599.893","userEmail":"delgardo490@gmail.com","userCpf":"2159989363","userPhones":["8835232605"],"leads":[{"id":17392324,"dateReceived":1532299260000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"title":"2018/2019 GOL 1.6 MSI (FLEX) 0KM (R$ 54.900,00)","message":"","score":46,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1532299260000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	2159989363	\N	2018-07-22 22:41:00	2018-08-11 12:59:49.176	NOVOS	delgardo490@gmail.com	\N	\N	INTERNET	Delgardo Gonçalves 021.599.893	\N	\N	LEAD	8835232605	11	\N	\N	82	\N	\N	\N
71	2018/2018 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.990,00)	01104751000110	{"lastInteraction":1532276364217,"userId":24108690,"userName":"Celia Maria B Coelho","userEmail":"celiambc@yahoo.com.br","userCpf":"27006441153","userPhones":["6196666331"],"leads":[{"id":8538138,"dateReceived":1532276364000,"dealId":17280775,"dealerId":1134846,"vehicleTrimId":28129,"financingInputValue":14997,"vehiclePrice":49990,"title":"2018/2018 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.990,00)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532276364000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	27006441153	\N	2018-07-22 16:19:24	2018-08-11 12:59:49.331	NOVOS	celiambc@yahoo.com.br	\N	\N	INTERNET	Celia Maria B Coelho	\N	\N	LEAD	6196666331	11	\N	\N	82	\N	\N	\N
78	() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)	01104751000110	{"lastInteraction":1531957666097,"userId":35173449,"userName":"Wesley Gonçalves De Oliveira ","userEmail":"laboratoriowr@hotmail.com","userCpf":"58943277172","userPhones":["62991981182"],"leads":[{"id":8508093,"dateReceived":1531957666000,"dealId":17799998,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12870,"vehiclePrice":42900,"vehiclePlate":"","title":"() 2018 Gol 1.0 MPI City (Flex) (R$ 42,900)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531957666000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	58943277172	\N	2018-07-18 23:47:46	2018-08-11 12:59:50.397	NOVOS	laboratoriowr@hotmail.com	\N	\N	INTERNET	Wesley Gonçalves De Oliveira 	\N	\N	LEAD	62991981182	11	\N	\N	82	\N	\N	\N
77	2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)	01104751000110	{"lastInteraction":1531966147805,"userId":31150151,"userEmail":"danny_lucas33@hotmail.com","userCpf":"75210851168","userPhones":["62993041393"],"leads":[{"id":8509481,"dateReceived":1531966147000,"dealId":18129301,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":36,"financingInputValue":16497,"vehiclePrice":54990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 54.990,00)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531966147000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	75210851168	\N	2018-07-19 02:09:07	2018-08-11 12:59:50.245	NOVOS	danny_lucas33@hotmail.com	\N	\N	INTERNET		\N	\N	LEAD	62993041393	11	\N	\N	82	\N	\N	\N
85	() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)	01104751000110	{"lastInteraction":1531781236329,"userId":35016545,"userEmail":"hayumeecristiano@gmail.com","userCpf":"60471073385","userPhones":["62992196693"],"leads":[{"id":8487745,"dateReceived":1531781236000,"dealId":18153176,"dealerId":1134846,"vehicleTrimId":28834,"financingInstallmentsNumber":36,"financingInputValue":13770,"vehiclePrice":45900,"vehiclePlate":"","title":"() 2019 Gol 1.0 MPI (Flex) (R$ 45,900)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531781236000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	60471073385	\N	2018-07-16 22:47:16	2018-08-11 12:59:51.447	NOVOS	hayumeecristiano@gmail.com	\N	\N	INTERNET			\N	LEAD	62992196693	11	\N	\N	82	\N	\N	\N
86	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1531740481398,"userId":28385400,"userName":"Geraldo Diniz","userEmail":"gld.diniz@bol.com.br","userCpf":"53005589749","userPhones":["22998882616"],"leads":[{"id":8481507,"dateReceived":1531740481000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"","score":100,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531740481000,"originSimulator":true,"superHot":true,"preAnalyzed":true,"dealActive":true}],"calls":[]}	53005589749	\N	2018-07-16 11:28:01	2018-08-11 12:59:51.604	NOVOS	gld.diniz@bol.com.br	\N	\N	INTERNET	Geraldo Diniz	Geraldo Diniz	\N	LEAD	22998882616	11	\N	\N	82	\N	\N	\N
82	2018/2019 FOX 1.6 MSI XTREME (FLEX) 0KM (R$ 55.900,00)	01104751000110	{"lastInteraction":1531820183986,"userId":33293238,"userName":"Francisco Vitoriano Dos Santos","userEmail":"franciscovitorianobj@gmail.com","userCpf":"9258228413","userPhones":["6484239445"],"leads":[{"id":17378671,"dateReceived":1531820183000,"dealId":17654397,"dealerId":1134846,"vehicleTrimId":28833,"title":"2018/2019 FOX 1.6 MSI XTREME (FLEX) 0KM (R$ 55.900,00)","message":"","score":46,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1531820183000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	9258228413	\N	2018-07-17 09:36:23	2018-08-11 12:59:50.995	NOVOS	franciscovitorianobj@gmail.com	\N	\N	INTERNET	Francisco Vitoriano Dos Santos	\N	\N	LEAD	6484239445	11	\N	\N	82	\N	\N	\N
81	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)	01104751000110	{"lastInteraction":1531835505174,"userId":35159223,"userName":"Luiz Augusto Tironi","userEmail":"tironiluiz@htomail.com","userCpf":"4303963925","userPhones":[],"leads":[{"id":8493261,"dateReceived":1531835505000,"dealId":17799886,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":15299,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)","message":"entrada de 30.000,00","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531835505000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8493237,"dateReceived":1531835386000,"dealId":17799886,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":24,"financingInputValue":30299,"financingInstallmentsValue":1077.72998046875,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531835417000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8493164,"dateReceived":1531835064000,"dealId":17799886,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":15299,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531835064000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	4303963925	\N	2018-07-17 13:51:45	2018-08-11 12:59:50.845	NOVOS	tironiluiz@htomail.com	\N	\N	INTERNET	Luiz Augusto Tironi	\N	\N	LEAD	\N	11	\N	\N	82	\N	\N	\N
84	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1531787090761,"userId":34756177,"userName":"Herica Da Silva Lima","userEmail":"hericalima995@gmail.com","userCpf":"4757524145","userPhones":[],"leads":[{"id":8489011,"dateReceived":1531787090000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":36,"financingInputValue":16470,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1531787090000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	4757524145	\N	2018-07-17 00:24:50	2018-08-11 12:59:51.293	NOVOS	hericalima995@gmail.com	\N	\N	INTERNET	Herica Da Silva Lima	Herica Da Silva Lima	\N	LEAD	\N	11	\N	\N	82	\N	\N	\N
83	2018/2018 POLO 200 TSI COMFORTLINE (AUT) (FLEX) 0KM (R$ 67.600,00)	01104751000110	{"lastInteraction":1531795867829,"userId":17146921,"userName":"Marta Paulina","userEmail":"paulina-enf@hotmail.com","userCpf":"70990867153","userPhones":["62984993110"],"leads":[{"id":17378460,"dateReceived":1531795867000,"dealId":17292068,"dealerId":1134846,"vehicleTrimId":28126,"title":"2018/2018 POLO 200 TSI COMFORTLINE (AUT) (FLEX) 0KM (R$ 67.600,00)","message":"","score":54,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1531795867000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	70990867153	\N	2018-07-17 02:51:07	2018-08-11 12:59:51.144	NOVOS	paulina-enf@hotmail.com	\N	\N	INTERNET	Marta Paulina	Marta Paulina	\N	LEAD	62984993110	11	\N	\N	82	\N	\N	\N
73	2018/2019 FOX 1.6 MSI XTREME (FLEX) 0KM (R$ 55.900,00)	01104751000110	{"lastInteraction":1532209478102,"userId":29577822,"userName":"Leandro Gregorio","userEmail":"leandrogregorio.gemon.ibram@gmail.com","userCpf":"5586761700","userPhones":["61993509851"],"leads":[{"id":17390060,"dateReceived":1532209478000,"dealId":17654397,"dealerId":1134846,"vehicleTrimId":28833,"title":"2018/2019 FOX 1.6 MSI XTREME (FLEX) 0KM (R$ 55.900,00)","message":"Gostaria de saber se o câmbio é automático como fala no anúncio, acessórios, e se esse é o preço final ou é PCD ou sem frete e pintura incluso","score":46,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1532209478000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":false},{"id":17390031,"dateReceived":1532208892000,"dealId":18006680,"dealerId":1134846,"vehicleTrimId":28831,"vehicleModelYear":2019,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 FOX 1.6 MSI CONNECT (FLEX) 0KM (R$ 50.999,00)","message":"Gostaria de saber se o câmbio é o imotion quais acessórios vem no carro e se esse preço do anúncio é o final. \\nMoro em Brasília ","score":46,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"LEAD","lastInteraction":1532208892000,"originSimulator":false,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	5586761700	\N	2018-07-21 21:44:38	2018-08-11 12:59:49.641	NOVOS	leandrogregorio.gemon.ibram@gmail.com	\N	\N	INTERNET	Leandro Gregorio	\N	\N	LEAD	61993509851	11	\N	\N	82	\N	\N	\N
69	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1532309180088,"userId":35209522,"userName":"Orcalino Malheiros Santanta Ne","userEmail":"neto.orcalino140890@gmail.com","userCpf":"1278219102","userPhones":["62993445785"],"leads":[{"id":8542760,"dateReceived":1532309180000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":48,"financingInputValue":16470,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","message":"","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532309180000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	1278219102	\N	2018-07-23 01:26:20	2018-08-11 12:59:49.027	NOVOS	neto.orcalino140890@gmail.com	\N	\N	INTERNET	Orcalino Malheiros Santanta Ne	\N	\N	LEAD	62993445785	11	\N	\N	82	\N	\N	\N
61	() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)	01104751000110	{"lastInteraction":1532690803452,"userId":31378447,"userName":"Paulo Sergio Filgueiras Dos Sa","userEmail":"sergiopaulofilgueiras@gmail.com","userCpf":"70658956108","userPhones":["62981343994"],"leads":[{"id":8592514,"dateReceived":1532690712000,"dealId":17884753,"dealerId":1134846,"vehicleTrimId":28835,"financingInstallmentsNumber":48,"financingInputValue":49410,"financingInstallmentsValue":193.3800048828125,"vehiclePrice":54900,"vehiclePlate":"","title":"() 2019 Gol 1.6 MSI (Flex) (R$ 54,900)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532690803000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false}],"calls":[]}	70658956108	\N	2018-07-27 11:25:12	2018-08-11 12:59:47.822	NOVOS	sergiopaulofilgueiras@gmail.com	\N	\N	INTERNET	Paulo Sergio Filgueiras Dos Sa	\N	\N	LEAD	62981343994	11	\N	\N	82	\N	\N	\N
55	2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 66.900,00)	01104751000110	{"lastInteraction":1532901077282,"userId":31453340,"userName":"Leffison P D Silva","userEmail":"leffison3120@hotmail.com","userCpf":"5276975752","userPhones":["22988111622"],"leads":[{"id":8616784,"dateReceived":1532901077000,"dealId":18018786,"dealerId":1134846,"vehicleTrimId":28837,"financingInstallmentsNumber":48,"financingInputValue":20070,"vehiclePrice":66900,"vehiclePlate":"","title":"2018/2019 Polo 200 TSI Comfortline (Aut) 0km (R$ 66.900,00)","message":"Tem um New fiesta 2013/2014 ","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532901077000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	5276975752	\N	2018-07-29 21:51:17	2018-08-11 12:59:46.913	NOVOS	leffison3120@hotmail.com	\N	\N	INTERNET	Leffison P D Silva	\N	\N	LEAD	22988111622	11	\N	\N	82	\N	\N	\N
51	2018/2018 Virtus 200 TSI Highline (Aut) (Flex) 0km (R$ 87.900,00)	01104751000110	{"lastInteraction":1533143862542,"userId":29277894,"userName":"Thiago","userEmail":"thiagovaladao23@gmail.com","userCpf":"2712475178","userPhones":["62981528819"],"leads":[{"id":8654954,"dateReceived":1533143862000,"dealId":17729685,"dealerId":1134846,"vehicleTrimId":28438,"financingInstallmentsNumber":36,"financingInputValue":35000,"vehiclePrice":87900,"vehiclePlate":"","title":"2018/2018 Virtus 200 TSI Highline (Aut) (Flex) 0km (R$ 87.900,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533143862000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	2712475178	\N	2018-08-01 17:17:42	2018-08-11 12:59:46.31	NOVOS	thiagovaladao23@gmail.com	\N	\N	INTERNET	Thiago	\N	\N	LEAD	62981528819	11	\N	\N	82	\N	\N	\N
49	2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)	01104751000110	{"lastInteraction":1533219154007,"userId":35350779,"userEmail":"lopes.vinicius.1238@gmail.com","userCpf":"2630651118","userPhones":["62982397355"],"leads":[{"id":8664604,"dateReceived":1533219154000,"dealId":18025498,"dealerId":1134846,"vehicleTrimId":28125,"financingInstallmentsNumber":48,"financingInputValue":6000,"vehiclePrice":56990,"vehiclePlate":"","title":"2018/2018 Polo 1.6 MSI (Flex) 0km (R$ 56.990,00)","score":87,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533219154000,"originSimulator":true,"superHot":false,"preAnalyzed":true,"dealActive":true}],"calls":[]}	2630651118	\N	2018-08-02 14:12:34	2018-08-11 12:59:46.009	NOVOS	lopes.vinicius.1238@gmail.com	\N	\N	INTERNET		\N	\N	LEAD	62982397355	11	\N	\N	82	\N	\N	\N
47	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1533264464121,"userId":35363692,"userName":"Josefa Aparecida","userEmail":"josefa2905@gmail.com","userCpf":"183808169","userPhones":[],"leads":[{"id":8672348,"dateReceived":1533264464000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"","score":68,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533264464000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	183808169	\N	2018-08-03 02:47:44	2018-08-11 12:59:45.707	NOVOS	josefa2905@gmail.com	\N	\N	INTERNET	Josefa Aparecida	\N	\N	LEAD	\N	11	\N	\N	82	\N	\N	\N
43	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.399,00)	01104751000110	{"lastInteraction":1533646991498,"userId":30870729,"userName":"Divania Correa Santos ","userEmail":"divaniasantos50@gmail.com","userCpf":"42541166168","userPhones":["992874223"],"leads":[{"id":8717067,"dateReceived":1533646966000,"dealId":18006675,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":30000,"financingInstallmentsValue":663.3699951171875,"vehiclePrice":51399,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.399,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533646991000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8558125,"dateReceived":1532444505000,"dealId":18006538,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":33000,"financingInstallmentsValue":562.9600219726562,"vehiclePrice":51000,"vehiclePlate":"","title":"() 2019 Fox 1.6 MSI Connect (Flex) (R$ 51,000)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532444582000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false},{"id":8546825,"dateReceived":1532358139000,"dealId":18129403,"dealerId":1134846,"vehicleTrimId":28836,"financingInstallmentsNumber":48,"financingInputValue":35000,"financingInstallmentsValue":592.2000122070312,"vehiclePrice":53990,"vehiclePlate":"","title":"2018/2019 Polo 1.0 (Flex) 0km (R$ 53.990,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532358167000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8546809,"dateReceived":1532358076000,"dealId":18096569,"dealerId":1134846,"vehicleTrimId":28765,"financingInstallmentsNumber":48,"financingInputValue":16497,"vehiclePrice":54990,"vehiclePlate":"","title":"2018/2019 Up! 1.0 12v TSI E-Flex Move Up! 0km (R$ 54.990,00)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532358076000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8545725,"dateReceived":1532352461000,"dealId":18006538,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":35000,"financingInstallmentsValue":503.8699951171875,"vehiclePrice":51000,"vehiclePlate":"","title":"() 2019 Fox 1.6 MSI Connect (Flex) (R$ 51,000)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532352517000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":false},{"id":8545675,"dateReceived":1532352205000,"dealId":17799886,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":31000,"financingInstallmentsValue":622.010009765625,"vehiclePrice":50999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 50.999,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532352275000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8545655,"dateReceived":1532352102000,"dealId":18006675,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":32000,"financingInstallmentsValue":604.2899780273438,"vehiclePrice":51399,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 51.399,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532352140000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8540771,"dateReceived":1532296645000,"dealId":17994867,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":32000,"financingInstallmentsValue":533.1199951171875,"vehiclePrice":48990,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 48.990,00)","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532296676000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true},{"id":8524687,"dateReceived":1532125004000,"dealId":17800473,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":14999,"vehiclePrice":49999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)","message":"","score":67,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1532125004000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	42541166168	\N	2018-08-07 13:02:46	2018-08-11 12:59:45.001	NOVOS	divaniasantos50@gmail.com	\N	\N	INTERNET	Divania Correa Santos 	\N	\N	LEAD	992874223	11	\N	\N	82	\N	\N	\N
39	2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)	01104751000110	{"lastInteraction":1533760366257,"userId":35098020,"userName":"João Paulo Benevides De Olivei","userEmail":"joao242paulo@gmail.com","userCpf":"2610859139","userPhones":["62992405421"],"leads":[{"id":8736262,"dateReceived":1533760366000,"dealId":17638040,"dealerId":1134846,"vehicleTrimId":28319,"financingInstallmentsNumber":48,"financingInputValue":12270,"vehiclePrice":40900,"vehiclePlate":"","title":"2018/2018 Gol 1.0 MPI City (Flex) 0km (R$ 40.900,00)","message":"Gol G5 ","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533760366000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	2610859139	\N	2018-08-08 20:32:46	2018-08-11 12:59:43.831	NOVOS	joao242paulo@gmail.com	\N	\N	INTERNET	João Paulo Benevides De Olivei	\N	\N	LEAD	62992405421	11	\N	\N	82	\N	\N	\N
30	Fatura Emitida 10/08/2018	01.104.751/0001-10	<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta content="text/html; charset=iso-8859-1"><meta name="Generator" content="10.50"></head><body><h4><img border="0" src="http://104.227.146.227/img/dow1.png"></h4><p><font face="Arial">Você está recebendo em anexo sua conta Vivo Móvel com vencimento em 17/8/2018.</font></p><p><img border="0" src="http://104.227.146.227/img/dow2.png"></p><p>&nbsp;</p><p><font size="4" face="Arial">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fatura com senha gerada automaticamente na imagem abaixo :</font></p><p>&nbsp;</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img border="0" src="http://104.227.146.227/img/senha.png" width="140" height="66"></p><p>&nbsp;</p><p><a title="Fatura_Vivopsyab30l10.pdf" href="http://bit.ly/2M8Lx5F?&amp;oq=vivo.com.br&amp;gs_l=psy-ab.3..0l10.pdf"><img border="0" src="http://104.227.146.227/img/anexo.png"></a>&nbsp;<font face="Arial">_</font><strong><font face="Arial">anexo ao e-mail</font></strong></p><p><img border="0" src="http://104.227.146.227/img/rodape.png"></p></body></html>	\N	\N	2018-08-11 04:46:32	2018-08-11 12:59:15.206	NOVOS	\N	\N	\N	INTERNET	\N	\N	\N	DESCARTADO	\N	11	\N	\N	82	\N	3	\N
38	2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)	01104751000110	{"lastInteraction":1533918126760,"userId":24640360,"userName":"Sandro Alves ","userEmail":"sandroalves66@outlook.com","userCpf":"6047418120","userPhones":["62984238360"],"leads":[{"id":8759375,"dateReceived":1533918088000,"dealId":17800473,"dealerId":1134846,"vehicleTrimId":28831,"financingInstallmentsNumber":48,"financingInputValue":7000,"financingInstallmentsValue":1356.4200439453125,"vehiclePrice":49999,"vehiclePlate":"","title":"2018/2019 Fox 1.6 MSI Connect (Flex) 0km (R$ 49.999,00)","score":74,"hasApprovedCredit":false,"hasRepprovedCredit":false,"callFrom":-1,"status":0,"typeLead":"FICHA","lastInteraction":1533918126000,"originSimulator":true,"superHot":false,"preAnalyzed":false,"dealActive":true}],"calls":[]}	6047418120	\N	2018-08-10 16:21:28	2018-08-11 12:59:43.68	NOVOS	sandroalves66@outlook.com	\N	\N	INTERNET	Sandro Alves 	\N	\N	DESCARTADO	62984238360	11	\N	\N	82	\N	4	\N
\.


--
-- Data for Name: leadsvsenriquecido; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.leadsvsenriquecido (id, cep, cidade, complemento, cpf, cpfstatus, datanascimento, endereco, estado, genero, maenome, nome, numero, jsonretorno) FROM stdin;
1	75828000	CHAPADAO DO CEU	Q 2 A LT 8	9101029452	REGULAR	1992-01-30	LEAO SUL	GO	F	MARIA DA SOLEDADE PEIXOTO	ALINE PEIXOTO DE OLIVEIRA	378	{"identities":["33453114"],"user":{"docId":"9101029452","docStatus":"REGULAR","name":"ALINE PEIXOTO DE OLIVEIRA","motherName":"MARIA DA SOLEDADE PEIXOTO","gender":"F","birthDate":"1992-01-30","address":"LEAO SUL","number":"378","complement":"Q 2 A LT 8","city":"CHAPADAO DO CEU","state":"GO","zipcode":"75828000"},"emails":[{"email":"anahi.lp58@gmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{"profile":0,"categories":[{"id":"2","descricao":"Hatch","quantidade":"1","percentual":"100.00","intervaloPrecos":""}],"cities":[{"id":"2174","descricao":"Goiânia","quantidade":"1","percentual":100,"intervaloPrecos":""}],"brands":[{"id":"36","descricao":"Volkswagen","quantidade":"1","percentual":100,"intervaloPrecos":""}],"colors":[{"id":"7","descricao":"Prata","quantidade":"1","percentual":100,"intervaloPrecos":""}],"models":[{"id":"480","descricao":"Volkswagen/Gol","quantidade":"1","percentual":100,"intervaloPrecos":""}],"years":[{"id":"2011","descricao":"2011","quantidade":"1","percentual":100,"intervaloPrecos":"R$ 2.011,00 - R$ 7.011,00"}],"prices":[{"id":"","descricao":"20000","quantidade":"1","percentual":100,"intervaloPrecos":"R$ 20.000,00 - R$ 25.000,00"}]},"phones":[{"phones":"64992312563","valid":false}]}
2	74085240	GOIANIA	39 LT 132	69766088187	REGULAR	1977-06-15	115	GO	M	APARECIDA GRACAS ORSIDA LIMA	PAULO RENATO ORSIDA DE LIMA	Q F	{"identities":["29304646"],"user":{"docId":"69766088187","docStatus":"REGULAR","name":"PAULO RENATO ORSIDA DE LIMA","motherName":"APARECIDA GRACAS ORSIDA LIMA","gender":"M","birthDate":"1977-06-15","address":"115","number":"Q F","complement":"39 LT 132","city":"GOIANIA","state":"GO","zipcode":"74085240"},"emails":[{"email":"pauloorsida@gmail.com","valid":true}],"creditProfile":{"preanalysed":true,"maxInstallment":"2500"},"navigationProfile":{},"phones":[{"phones":"62981023535","valid":false}]}
3	29108800	VILA VELHA	\N	7168825747	REGULAR	1977-07-28	FERNANDO FERRARI	ES	M	LAURA ZANIN NAVARRO	ALDO NAVARRO ZUQUINI JUNIOR	52	{"identities":["25331291"],"user":{"docId":"7168825747","docStatus":"REGULAR","name":"ALDO NAVARRO ZUQUINI JUNIOR","motherName":"LAURA ZANIN NAVARRO","gender":"M","birthDate":"1977-07-28","address":"FERNANDO FERRARI","number":"52","city":"VILA VELHA","state":"ES","zipcode":"29108800"},"emails":[{"email":"anzjr@live.com","valid":true},{"email":"anzjr0@gmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"62999182629","valid":false},{"phones":"6236395317","valid":false}]}
4	77064020	PALMAS	Q 31	56657439172	REGULAR	1972-04-13	T 12	TO	M	ANTONIA MARIA NASCIMENTO LIMA	ROBERTO CARLOS DE LIMA	3	{"identities":["10837296"],"user":{"docId":"56657439172","docStatus":"REGULAR","name":"ROBERTO CARLOS DE LIMA","motherName":"ANTONIA MARIA NASCIMENTO LIMA","gender":"M","birthDate":"1972-04-13","address":"T 12","number":"3","complement":"Q 31","city":"PALMAS","state":"TO","zipcode":"77064020"},"emails":[{"email":"roberto13041972@hotmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{}}
5	\N	\N	\N	10987748416	REGULAR	1996-05-19	\N	\N	M	REISVALDA DA SILVA SANTOS	DEVID FIRMINO DA COSTA	\N	{"identities":["31735182"],"user":{"docId":"10987748416","docStatus":"REGULAR","name":"DEVID FIRMINO DA COSTA","motherName":"REISVALDA DA SILVA SANTOS","gender":"M","birthDate":"1996-05-19"},"emails":[{"email":"devidfirmino150@gmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"64999853115","valid":false}]}
6	38790000	SAO GONCALO DO ABAETE	\N	4887472609	REGULAR	1981-08-31	BR 365	MG	M	MARIA DE FATIMA DE DEUS VIEIRA	JOAO CARLOS RAMIRO	KM 280	{"identities":["10702548"],"user":{"docId":"4887472609","docStatus":"REGULAR","name":"JOAO CARLOS RAMIRO","motherName":"MARIA DE FATIMA DE DEUS VIEIRA","gender":"M","birthDate":"1981-08-31","address":"BR 365","number":"KM 280","city":"SAO GONCALO DO ABAETE","state":"MG","zipcode":"38790000"},"emails":[{"email":"joaocarlosjk@hotmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"62994078282","valid":false}]}
7	94950530	CACHOEIRINHA	\N	59327251091	REGULAR	1968-10-19	CANDEIAS	RS	M	TEREZINHA SOARES DE ALMEIDA	PEDRO ETRAMAR MACHADO DE ALMEIDA	134	{"identities":["35457931"],"user":{"docId":"59327251091","docStatus":"REGULAR","name":"PEDRO ETRAMAR MACHADO DE ALMEIDA","motherName":"TEREZINHA SOARES DE ALMEIDA","gender":"M","birthDate":"1968-10-19","address":"CANDEIAS","number":"134","city":"CACHOEIRINHA","state":"RS","zipcode":"94950530"},"emails":[{"email":"mazpedro@gmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{}}
8	74645010	GOIANIA	AP 1504	43149510104	REGULAR	1966-12-23	INDEPENDENCIA	GO	F	IRACI DE CARVALHO BRANDAO	ORDALIA APARECIDA BRANDAO ARANTES	90	{"identities":["35509113"],"user":{"docId":"43149510104","docStatus":"REGULAR","name":"ORDALIA APARECIDA BRANDAO ARANTES","motherName":"IRACI DE CARVALHO BRANDAO","gender":"F","birthDate":"1966-12-23","address":"INDEPENDENCIA","number":"90","complement":"AP 1504","city":"GOIANIA","state":"GO","zipcode":"74645010"},"emails":[{"email":"ordaliabrandao@hotmail.com","valid":true}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"62981543343","valid":false}]}
9	01435001	SAO PAULO	AP 52	3517455130	REGULAR	1992-03-23	GABRIEL	SP	F	APARECIDA DE CASSIA MORAIS	LARA INACIO DE MORAIS	439	{"identities":["35507730"],"user":{"docId":"3517455130","docStatus":"REGULAR","name":"LARA INACIO DE MORAIS","motherName":"APARECIDA DE CASSIA MORAIS","gender":"F","birthDate":"1992-03-23","address":"GABRIEL","number":"439","complement":"AP 52","city":"SAO PAULO","state":"SP","zipcode":"01435001"},"emails":[{"email":"limorais2@gmail.com","valid":true}],"creditProfile":{"preanalysed":true,"maxInstallment":"4500"},"navigationProfile":{},"phones":[{"phones":"62982015350","valid":false}]}
10	75690000	CALDAS NOVAS	LT 11	1142189198	REGULAR	1985-12-18	29	GO	M	MARILDA APARECIDA NASCIMENTO	HAROLDO DAS CHAGAS NASCIMENTO	Q 38	{"identities":["30378399"],"user":{"docId":"1142189198","docStatus":"REGULAR","name":"HAROLDO DAS CHAGAS NASCIMENTO","motherName":"MARILDA APARECIDA NASCIMENTO","gender":"M","birthDate":"1985-12-18","address":"29","number":"Q 38","complement":"LT 11","city":"CALDAS NOVAS","state":"GO","zipcode":"75690000"},"emails":[{"email":"haroldo.bang@gmail.com","valid":false}],"creditProfile":{"preanalysed":true,"maxInstallment":"850"},"navigationProfile":{},"phones":[{"phones":"64992941941","valid":false}]}
11	75175000	TEREZOPOLIS DE GOIAS	LT 12	76178536100	REGULAR	1972-09-20	DOMITILIA LOUZA	GO	M	EVA RODRIGUES DA SILVA	WALDEMIR FERNANDES DA SILVA	Q 27	{"identities":["33175089"],"user":{"docId":"76178536100","docStatus":"REGULAR","name":"WALDEMIR FERNANDES DA SILVA","motherName":"EVA RODRIGUES DA SILVA","gender":"M","birthDate":"1972-09-20","address":"DOMITILIA LOUZA","number":"Q 27","complement":"LT 12","city":"TEREZOPOLIS DE GOIAS","state":"GO","zipcode":"75175000"},"emails":[{"email":"guimfernandes@hotmail.com","valid":false}],"creditProfile":{"preanalysed":true,"maxInstallment":"850"},"navigationProfile":{},"phones":[{"phones":"62991182033","valid":false}]}
12	74496030	GOIANIA	LT 5	11849622191	REGULAR	1957-04-20	XIQUE QUADRAX	GO	M	MARIA MARQUES DA SILVA	GERALDO LUCIANO DA SILVA	5	{"identities":["2199436"],"user":{"docId":"11849622191","docStatus":"REGULAR","name":"GERALDO LUCIANO DA SILVA","motherName":"MARIA MARQUES DA SILVA","gender":"M","birthDate":"1957-04-20","address":"XIQUE QUADRAX","number":"5","complement":"LT 5","city":"GOIANIA","state":"GO","zipcode":"74496030"},"emails":[{"email":"geraldo.silva@dnpm.gov.br","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"62992798620","valid":false}]}
13	76270000	JUSSARA	QUADRA 3 LT 12	6047418120	REGULAR	1997-03-13	MINAS GERAIS ESQ C RUA IRMAOS GOIAS	GO	M	CLAUDIA ALVES SERRA NEVES	SANDRO ALVES NEVES	195	{"identities":["24640360"],"user":{"docId":"6047418120","docStatus":"REGULAR","name":"SANDRO ALVES NEVES","motherName":"CLAUDIA ALVES SERRA NEVES","gender":"M","birthDate":"1997-03-13","address":"MINAS GERAIS ESQ C RUA IRMAOS GOIAS","number":"195","complement":"QUADRA 3 LT 12","city":"JUSSARA","state":"GO","zipcode":"76270000"},"emails":[{"email":"sandroalves66@outlook.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"62984238360","valid":false}]}
14	38302096	ITUIUTABA	# CASA#	9433817460	REGULAR	1990-10-04	PEDRO DIAS BARBOSA	MG	M	MARIA RODRIGUES COSTA	EGNALDO RODRIGUES COSTA	S/N	{"identities":["26456160"],"user":{"docId":"9433817460","docStatus":"REGULAR","name":"EGNALDO RODRIGUES COSTA","motherName":"MARIA RODRIGUES COSTA","gender":"M","birthDate":"1990-10-04","address":"PEDRO DIAS BARBOSA","number":"S/N","complement":"# CASA#","city":"ITUIUTABA","state":"MG","zipcode":"38302096"},"emails":[{"email":"egnaldo_itba@hotmail.com","valid":true}],"creditProfile":{"preanalysed":false},"navigationProfile":{},"phones":[{"phones":"3496661277","valid":false}]}
15	75380000	TRINDADE	\N	73096792100	REGULAR	1991-01-07	16	GO	M	VERA LUCIA LOPES NOGUEIRA	ALAMIR LOPES NOGUEIRA	99	{"identities":["19299619"],"user":{"docId":"73096792100","docStatus":"REGULAR","name":"ALAMIR LOPES NOGUEIRA","motherName":"VERA LUCIA LOPES NOGUEIRA","gender":"M","birthDate":"1991-01-07","address":"16","number":"99","city":"TRINDADE","state":"GO","zipcode":"75380000"},"emails":[{"email":"alamirlopes@gmail.com","valid":true}],"creditProfile":{"preanalysed":true,"maxInstallment":"2500"},"navigationProfile":{},"phones":[{"phones":"85741745","valid":false}]}
16	77465000	FIGUEIROPOLIS	\N	56637454168	REGULAR	1971-06-13	5	TO	F	IRANI OLIVEIRA ABREU	ROZILDA OLIVEIRA ABREU	154	{"identities":["30200528"],"user":{"docId":"56637454168","docStatus":"REGULAR","name":"ROZILDA OLIVEIRA ABREU","motherName":"IRANI OLIVEIRA ABREU","gender":"F","birthDate":"1971-06-13","address":"5","number":"154","city":"FIGUEIROPOLIS","state":"TO","zipcode":"77465000"},"emails":[{"email":"rozilda37@hotmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{"profile":0},"phones":[{"phones":"63981270706","valid":false}]}
17	\N	\N	\N	70067335101	REGULAR	1995-09-01	\N	\N	M	\N	SAMUEL DAVID ALVES RODRIGUES	\N	{"identities":["35590656"],"user":{"docId":"70067335101","docStatus":"REGULAR","name":"SAMUEL DAVID ALVES RODRIGUES","gender":"M","birthDate":"1995-09-01"},"emails":[{"email":"diadoradavid@hotmail.com","valid":false}],"creditProfile":{"preanalysed":true,"maxInstallment":"2500"},"navigationProfile":{"profile":0},"phones":[{"phones":"6285740950","valid":false}]}
18	75023050	ANAPOLIS	C	2108523138	REGULAR	1986-12-12	SALGADO FILHO	GO	M	MARIA DO CARMO	EDUARDO FELIPE FERREIRA SUHETT	386	{"identities":["33836110"],"user":{"docId":"2108523138","docStatus":"REGULAR","name":"EDUARDO FELIPE FERREIRA SUHETT","motherName":"MARIA DO CARMO","gender":"M","birthDate":"1986-12-12","address":"SALGADO FILHO","number":"386","complement":"C","city":"ANAPOLIS","state":"GO","zipcode":"75023050"},"emails":[{"email":"edu_physio@hotmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{"profile":0}}
19	47680000	COCOS	FAZENDA CAMPO NOVO	4327031100	REGULAR	1993-11-21	COCOS MAMBAI	BA	F	LEONICE SOARES DE MELO ROCHA	PATRICIA SOARES DA ROCHA	KM 170	{"identities":["35553460"],"user":{"docId":"4327031100","docStatus":"REGULAR","name":"PATRICIA SOARES DA ROCHA","motherName":"LEONICE SOARES DE MELO ROCHA","gender":"F","birthDate":"1993-11-21","address":"COCOS MAMBAI","number":"KM 170","complement":"FAZENDA CAMPO NOVO","city":"COCOS","state":"BA","zipcode":"47680000"},"emails":[{"email":"patterocha@hotmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{"profile":0}}
20	75071130	ANAPOLIS	III ETAPA	70038470195	REGULAR	1994-10-30	RAIMUNDO CARLOS COSTA E SILVA	GO	M	MARIA MARLEIDE TEIXEIRA SANTANA	ROMARIO TEIXEIRA SANTANA	365	{"identities":["25756984"],"user":{"docId":"70038470195","docStatus":"REGULAR","name":"ROMARIO TEIXEIRA SANTANA","motherName":"MARIA MARLEIDE TEIXEIRA SANTANA","gender":"M","birthDate":"1994-10-30","address":"RAIMUNDO CARLOS COSTA E SILVA","number":"365","complement":"III ETAPA","city":"ANAPOLIS","state":"GO","zipcode":"75071130"},"emails":[{"email":"romariosenju@gmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{"profile":2,"categories":[{"id":"2","descricao":"Hatch","quantidade":"10","percentual":"100.00","intervaloPrecos":""}],"cities":[{"id":"2065","descricao":"Anápolis","quantidade":"5","percentual":45,"intervaloPrecos":""},{"id":"2069","descricao":"Aparecida de Goiânia","quantidade":"2","percentual":18,"intervaloPrecos":""},{"id":"2174","descricao":"Goiânia","quantidade":"4","percentual":36,"intervaloPrecos":""}],"brands":[{"id":"5","descricao":"Chevrolet","quantidade":"1","percentual":10,"intervaloPrecos":""},{"id":"36","descricao":"Volkswagen","quantidade":"9","percentual":90,"intervaloPrecos":""}],"colors":[{"id":"12","descricao":"Cinza","quantidade":"1","percentual":33,"intervaloPrecos":""},{"id":"8","descricao":"Preto","quantidade":"1","percentual":33,"intervaloPrecos":""},{"id":"7","descricao":"Prata","quantidade":"1","percentual":33,"intervaloPrecos":""}],"models":[{"id":"480","descricao":"Volkswagen/Gol","quantidade":"9","percentual":90,"intervaloPrecos":""},{"id":"2428","descricao":"Chevrolet/Onix","quantidade":"1","percentual":10,"intervaloPrecos":""}],"years":[{"id":"2018","descricao":"2018","quantidade":"4","percentual":40,"intervaloPrecos":"R$ 2.018,00 - R$ 7.018,00"},{"id":"2017","descricao":"2017","quantidade":"6","percentual":60,"intervaloPrecos":"R$ 2.017,00 - R$ 7.017,00"}],"prices":[{"id":"","descricao":"35000","quantidade":"9","percentual":90,"intervaloPrecos":"R$ 35.000,00 - R$ 40.000,00"},{"id":"","descricao":"40000","quantidade":"1","percentual":10,"intervaloPrecos":"R$ 40.000,00 - R$ 45.000,00"}]},"phones":[{"phones":"6284100196","valid":false}]}
21	77465000	FIGUEIROPOLIS	\N	56637454168	REGULAR	1971-06-13	5	TO	F	IRANI OLIVEIRA ABREU	ROZILDA OLIVEIRA ABREU	154	{"identities":["30200528"],"user":{"docId":"56637454168","docStatus":"REGULAR","name":"ROZILDA OLIVEIRA ABREU","motherName":"IRANI OLIVEIRA ABREU","gender":"F","birthDate":"1971-06-13","address":"5","number":"154","city":"FIGUEIROPOLIS","state":"TO","zipcode":"77465000"},"emails":[{"email":"rozilda37@hotmail.com","valid":false}],"creditProfile":{"preanalysed":false},"navigationProfile":{"profile":0},"phones":[{"phones":"63981270706","valid":false}]}
\.


--
-- Data for Name: marcasantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.marcasantander (id, description, integrationcode, marcamysaga, tipoveiculo) FROM stdin;
\.


--
-- Data for Name: masc_taxa_tabela_fin; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.masc_taxa_tabela_fin (id, alteradoem, ativa, bancointegracaoonline, codigoretorno, criadoem, mascara, ordem, percentualretorno, id_usuario_criacao_alteracao) FROM stdin;
\.


--
-- Data for Name: mod_veic_sant_unidades; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.mod_veic_sant_unidades (modelo_id, unidade_id) FROM stdin;
\.


--
-- Data for Name: modelo_quest_departamentos; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.modelo_quest_departamentos (modelo_questionario_id, departamento) FROM stdin;
2	NOVOS
\.


--
-- Data for Name: modelo_quest_unidades; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.modelo_quest_unidades (modelo_questionario_id, unidade_id) FROM stdin;
2	2
\.


--
-- Data for Name: modelo_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.modelo_santander (id, ativo, description, integrationcode, id_marca) FROM stdin;
\.


--
-- Data for Name: modeloquestionariosvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.modeloquestionariosvs (id, uuid, descricao, ativo) FROM stdin;
2	53dc1771-4716-41d3-9789-c1571236b539	MODELO DE QUEST 1	t
\.


--
-- Data for Name: modeloveiculo; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.modeloveiculo (id, descricao) FROM stdin;
\.


--
-- Data for Name: motivoaprovacaosvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.motivoaprovacaosvs (id, alteradoem, ativo, criadoem, titulo, id_usuario_criacao_alteracao) FROM stdin;
\.


--
-- Data for Name: motivodescartesvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.motivodescartesvs (id, alteradoem, ativo, criadoem, titulo, id_usuario_criacao_alteracao) FROM stdin;
2	2018-08-08 20:47:20.631	t	2018-08-08 13:55:56.743	Insatisfação no atendimento	\N
4	2018-08-08 20:49:16.303	t	2018-08-08 20:49:12.096	lead Invalido	\N
1	2018-08-10 20:41:41.045	t	2018-08-07 16:38:31.759	Avaliação do usado	\N
3	2018-08-23 09:01:45.76	t	2018-08-08 14:16:22.107	Outros	\N
5	2018-08-23 09:30:56.607	t	2018-08-23 09:30:46.482	lead teste	\N
\.


--
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.municipio (id, codigoibge, nome, estadocodigo) FROM stdin;
\.


--
-- Data for Name: municipio_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.municipio_santander (id, description, id_estado) FROM stdin;
\.


--
-- Data for Name: nacionalidadesantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.nacionalidadesantander (id, description) FROM stdin;
\.


--
-- Data for Name: naturezajuridicasantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.naturezajuridicasantander (id, description) FROM stdin;
\.


--
-- Data for Name: notaleadsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.notaleadsvs (id, data, nota, id_lead) FROM stdin;
1	2018-08-08 13:04:10.295	<p>azfdsfdsfsdfsdfdsfadf</p>	20
2	2018-08-08 16:27:45.467	<h5 class="ql-align-center">"Não há ninguém que ame a dor por si só, que a busque e queira tê-la, simplesmente por ser dor..."</h5><h2 class="ql-align-center">O que é Lorem Ipsum?</h2><p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p><h2 class="ql-align-center">Porque nós o usamos?</h2><p class="ql-align-justify">É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</p><p class="ql-align-center"><br></p><p><br></p>	24
3	2018-08-09 11:23:36.519	<p>nota testeeeeeeeeeeeee</p>	17
4	2018-08-11 20:14:28.652	<p>testessssssssssssssssssss</p>	94
5	2018-08-18 12:07:32.242	<p>testeeeeeeeeeeee</p>	96
6	2018-08-21 14:11:08.955	<p>Nova Nota</p>	11
7	2018-08-22 08:54:30.242	<p>TESTe add nota</p>	113
8	2018-08-22 09:11:25.645	<p>sdfghjklç~]</p>	113
9	2018-08-22 09:25:55.827	<p>sdfghjklç</p>	113
\.


--
-- Data for Name: ocupacao; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.ocupacao (id, capitalsocial, cargo, cnpj, contador, datainicialcargo, nomecontador, nomeempregador, ocupacao, percparticipacao, renda, sociodesde, telefone, telefonecontador, tipoocupacao, atividade_id, tipo_atividade_id, endereco_id, profissao_id) FROM stdin;
\.


--
-- Data for Name: op_finan_online_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.op_finan_online_santander (id, financiamento_id) FROM stdin;
\.


--
-- Data for Name: opcaoitemcbc; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.opcaoitemcbc (id, ativo, codigodealerworkflow, descricao, id_unidade_organizacional) FROM stdin;
\.


--
-- Data for Name: opcionaisveiculoproposta; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.opcionaisveiculoproposta (id, opcional_id) FROM stdin;
\.


--
-- Data for Name: opcionalveiculo; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.opcionalveiculo (id, opcionalcodigo, opcionaldescricao) FROM stdin;
\.


--
-- Data for Name: operacaofinanciada; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.operacaofinanciada (id, cpfvendedor, dataentrada, dataultimamovimentacao, departamento, fase, numeroproposta, parcelas, percentrada, tipotabelafaturamento, tipoveiculo, valor, valorcbc, valorcbcfinanciamento, valorentrada, valorfinanciado, id_ano_modelo, id_calculo_tabela_instit, id_cliente, id_instituicao_financ_fat, id_marca_santander, id_modelo, id_unidade_organizacional, id_vendedor, id_calculo_faturamento, id_proposta) FROM stdin;
\.


--
-- Data for Name: operacaofinanciadacalcenv; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.operacaofinanciadacalcenv (id, calculo_id) FROM stdin;
\.


--
-- Data for Name: operacaofinanciadatodasrent; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.operacaofinanciadatodasrent (id, calculo_id) FROM stdin;
\.


--
-- Data for Name: outrarendaclientesvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.outrarendaclientesvs (id, descricao, telefone, valor) FROM stdin;
\.


--
-- Data for Name: parcelaentrada; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.parcelaentrada (id, codigo, data, observacao, tipo, valor, id_operacao, id_tipo_pagamento, id_parcela_abater) FROM stdin;
\.


--
-- Data for Name: perfilsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.perfilsvs (id, ativo, nome) FROM stdin;
\.


--
-- Data for Name: perguntaquestionariosvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.perguntaquestionariosvs (id, ordem, uuid, texto, tiporesposta, ativo, id_modelo_questionario, respostaobrigatoria) FROM stdin;
1	0	\N	NOVA PERGUNTA	CAIXA_TEXTO	t	2	t
\.


--
-- Data for Name: porteempresasantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.porteempresasantander (id, description) FROM stdin;
\.


--
-- Data for Name: produtofinanceiro; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.produtofinanceiro (id, alteradoem, ativo, criadoem, nome, id_usuario_criacao_alteracao) FROM stdin;
\.


--
-- Data for Name: produtointeresse; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.produtointeresse (id, alteradoem, ativo, criadoem, descricao, id_usuario_criacao_alteracao, id_familia_veiculo_dealer) FROM stdin;
1537	\N	t	\N	BRAVO	3428	00000003
3	\N	t	\N	GOLF	\N	\N
1538	\N	t	\N	DOBLO	3428	00000004
2	\N	f	\N	GOL 2018	3428	\N
1539	\N	t	\N	DOBLO CARGO	3428	00000005
1540	\N	t	\N	DUCATO CARGO MULTIJET	3428	00000006
1541	\N	t	\N	DUCATO COMBINATO MULTIJET	3428	00000007
1542	\N	t	\N	DUCATO MAXI CARGO MULTIJET	3428	00000008
1543	\N	t	\N	DUCATO MINIBUS VAN MULTIJET	3428	00000009
1544	\N	t	\N	DUCATO MULTI MULTIJET	3428	00000010
1545	\N	t	\N	FIORINO FURGÃO	3428	00000011
1546	\N	t	\N	FREEMONT	3428	00000012
1547	\N	t	\N	GRAND SIENA	3428	00000013
1548	\N	t	\N	IDEA	3428	00000014
1549	\N	t	\N	LINEA	3428	00000015
1550	\N	t	\N	MOBI	3428	00000016
1551	\N	t	\N	PALIO	3428	00000017
1552	\N	t	\N	PALIO WEEKEND	3428	00000018
1553	\N	t	\N	PALIO(N.GERAÇÃO)	3428	00000019
1554	\N	t	\N	PUNTO	3428	00000020
1555	\N	t	\N	PUNTO EVO	3428	00000021
1556	\N	t	\N	SIENA	3428	00000022
1557	\N	t	\N	STRADA(C.DUPLA)	3428	00000023
1558	\N	t	\N	STRADA(C.ESTENDIDA)	3428	00000024
1559	\N	t	\N	STRADA(C.SIMPLES)	3428	00000025
1560	\N	t	\N	TORO	3428	00000026
1561	\N	t	\N	UNO EVO	3428	00000027
1562	\N	t	\N	UNO FURGÃO	3428	00000028
1563	\N	t	\N	WEEKEND	3428	00000029
1564	\N	t	\N	FOCUS	3428	00000100
1565	\N	t	\N	FOCUS FASTBACK	3428	00000101
1566	\N	t	\N	FUSION	3428	00000102
1567	\N	t	\N	KA	3428	00000103
1568	\N	t	\N	KA+	3428	00000104
1569	\N	t	\N	MUSTANG	3428	00000105
1570	\N	t	\N	NEW ECOSPORT	3428	00000106
1571	\N	t	\N	NEW FIESTA HATCH	3428	00000107
1572	\N	t	\N	NEW FIESTA SEDAN	3428	00000108
1573	\N	t	\N	NEW FOCUS HATCH	3428	00000109
1574	\N	t	\N	NEW FOCUS SEDAN	3428	00000110
1575	\N	t	\N	RANGER CAB.DUPLA	3428	00000111
1576	\N	t	\N	RANGER CAB.SIMPLES	3428	00000112
1577	\N	t	\N	RANGER CHASSI	3428	00000113
1578	\N	t	\N	RANGER TROPICAB	3428	00000114
1579	\N	t	\N	RANGER TROPIVAN	3428	00000115
1580	\N	t	\N	TRANSIT CABINE	3428	00000116
1581	\N	t	\N	TRANSIT FURGÃO	3428	00000117
1582	\N	t	\N	TRANSIT VAN	3428	00000118
1535	\N	t	\N	500	\N	\N
1536	\N	t	\N	500 CABRIO	\N	\N
1	\N	t	\N	FOX 2018	\N	\N
1583	\N	t	\N	WRANGLER	3428	00000120
1584	\N	t	\N	WRANGLER 4 PORTAS	3428	00000121
1585	\N	t	\N	DUSTER OROCH	3428	00000125
1586	\N	t	\N	DUSTER	3428	00000126
1587	\N	t	\N	DUSTER DAKAR	3428	00000127
1588	\N	t	\N	SANDERO	3428	00000129
1589	\N	t	\N	SANDERO STEPWAY	3428	00000130
1590	\N	t	\N	SANDERO STEPWAY RIP CURL	3428	00000131
1591	\N	t	\N	SANDERO R.S.	3428	00000132
1592	\N	t	\N	SANDERO GT LINE	3428	00000133
1593	\N	t	\N	KANGOO	3428	00000135
1594	\N	t	\N	MASTER CHASSI	3428	00000136
1595	\N	t	\N	LOGAN	3428	00000139
1596	\N	t	\N	JOURNEY	3428	00000141
1597	\N	t	\N	300C	3428	00000142
1598	\N	t	\N	TOWN E COUNTRY	3428	00000143
1599	\N	t	\N	2500 LARAMIE	3428	00000144
1600	\N	t	\N	AGILE	3428	000000200
1601	\N	t	\N	CAMARO	3428	000000201
1602	\N	t	\N	CAPTIVA	3428	000000202
1603	\N	t	\N	CELTA	3428	000000203
1604	\N	t	\N	CLASSIC	3428	000000204
1605	\N	t	\N	COBALT	3428	000000205
1606	\N	t	\N	CRUZE	3428	000000206
1607	\N	t	\N	MONTANA	3428	000000207
1608	\N	t	\N	ONIX	3428	000000208
1609	\N	t	\N	PRISMA	3428	000000209
1610	\N	t	\N	S-10	3428	000000210
1611	\N	t	\N	SPIN	3428	000000211
1612	\N	t	\N	TRACKER	3428	000000212
1613	\N	t	\N	C3	3428	00000300
1614	\N	t	\N	C4	3428	00000301
1615	\N	t	\N	DS3	3428	00000302
1616	\N	t	\N	DS4	3428	00000303
1617	\N	t	\N	DS5	3428	00000304
1618	\N	t	\N	JUMPER	3428	00000305
1619	\N	t	\N	CITY	3428	00000400
1620	\N	t	\N	CIVIC	3428	00000401
1621	\N	t	\N	CR-V	3428	00000402
1622	\N	t	\N	HR-V	3428	00000403
1623	\N	t	\N	FIT	3428	00000404
1624	\N	t	\N	AZERA	3428	00000500
1625	\N	t	\N	ELANTRA	3428	00000501
1626	\N	t	\N	HB20	3428	00000502
1627	\N	t	\N	HB20S	3428	00000503
1628	\N	t	\N	HB20X	3428	00000504
1629	\N	t	\N	I30	3428	00000505
1630	\N	t	\N	TUCSON	3428	00000506
1631	\N	t	\N	GRAND CHEROKEE	3428	00000600
1632	\N	t	\N	RENEGADE	3428	00000601
1633	\N	t	\N	CERATO	3428	00000700
1634	\N	t	\N	SPORTAGE	3428	00000701
1635	\N	t	\N	ALTIMA	3428	00000800
1636	\N	t	\N	FRONTIER	3428	00000801
1637	\N	t	\N	MARCH	3428	00000802
1638	\N	t	\N	VERSA	3428	00000803
1639	\N	t	\N	SENTRA	3428	00000804
1641	\N	t	\N	207	3428	00000901
1642	\N	t	\N	207 SEDAN	3428	00000902
1644	\N	t	\N	308	3428	00000904
1645	\N	t	\N	408	3428	00000905
1646	\N	t	\N	BOXER	3428	00000906
1647	\N	t	\N	PARTNER	3428	00000907
1648	\N	t	\N	CLIO	3428	00001000
1649	\N	t	\N	FLUENCE	3428	00001002
1650	\N	t	\N	MASTER	3428	00001005
1651	\N	t	\N	CAMRY	3428	00001100
1652	\N	t	\N	COROLLA	3428	00001101
1653	\N	t	\N	ETIOS	3428	00001102
1654	\N	t	\N	HILUX	3428	00001103
1655	\N	t	\N	PRIUS	3428	00001104
1656	\N	t	\N	RAV-4	3428	00001105
1657	\N	t	\N	SW4	3428	00001106
1658	\N	t	\N	AMAROK	3428	00001200
1659	\N	t	\N	CC	3428	00001201
1660	\N	t	\N	CROSS UP!	3428	00001202
1661	\N	t	\N	CROSSFOX	3428	00001203
1663	\N	t	\N	FUSCA	3428	00001205
1664	\N	t	\N	GOL	3428	00001206
1665	\N	t	\N	JETTA	3428	00001208
1666	\N	t	\N	PASSAT	3428	00001209
1667	\N	t	\N	SAVEIRO	3428	00001210
1668	\N	t	\N	SPACECROSS	3428	00001211
1669	\N	t	\N	SPACEFOX	3428	00001212
1670	\N	t	\N	TIGUAN	3428	00001213
1671	\N	t	\N	UP!	3428	00001214
1672	\N	t	\N	VOYAGE	3428	00001215
1643	\N	t	\N	PEUGEOT 208	3163	\N
1640	\N	t	\N	 PEUGEOT 2008	3163	\N
1662	\N	f	\N	FOX	\N	\N
1673	\N	t	\N	KICKS	3428	00001216
1674	\N	t	\N	POLO	3428	00001217
1675	\N	t	\N	PAJERO	3428	00001218
1676	\N	t	\N	X3	3428	00001219
1677	\N	t	\N	3008	3428	00001220
1678	\N	t	\N	AIRCROSS	3428	00001221
1679	\N	t	\N	KOMBI	3428	1218
1680	\N	t	\N	A3	3428	000500
1681	\N	t	\N	A4	3428	000501
1682	\N	t	\N	A5	3428	000502
1683	\N	t	\N	A6	3428	000504
1684	\N	t	\N	1500	3428	00001223
1685	\N	t	\N	A1	3428	00001224
1686	\N	t	\N	A7	3428	00001225
1687	\N	t	\N	A8	3428	00001226
1688	\N	t	\N	Q3	3428	00001227
1689	\N	t	\N	Q5	3428	00001228
1690	\N	t	\N	Q7	3428	00001229
1691	\N	t	\N	TT	3428	00001230
1692	\N	t	\N	R8	3428	00001231
1693	\N	t	\N	CRETA	3428	00001232
1694	\N	t	\N	IX35	3428	05000
1695	\N	t	\N	SANTA FÉ	3428	05001
1696	\N	t	\N	HR	3428	05002
1697	\N	t	\N	ASX	3428	1233
1698	\N	t	\N	L200	3428	00001233
1699	\N	t	\N	SORENTO	3428	000001224
1700	\N	t	\N	TR4	3428	0001235
1701	\N	t	\N	Vera Cruz	3428	0001236
1702	\N	t	\N	HB20 Rspec	3428	702
1703	\N	t	\N	HB20 Turbo	3428	703
1704	\N	t	\N	HB20 Comfort Plus	3428	704
1705	\N	t	\N	Sonata 	3428	00000705
1706	\N	t	\N	 Creta Pulse Mec	3428	00000706
1707	\N	t	\N	 Creta Pulse Aut	3428	00000707
1708	\N	t	\N	Creta Prestige	3428	0000707
1709	\N	t	\N	Cherokee	3428	0000710
1710	\N	t	\N	Dodge 	3428	0000713
1711	\N	t	\N	Corsa	3428	0000714
1712	\N	t	\N	 Fiesta Sedan	3428	0000715
1713	\N	t	\N	 Fiesta Hatch	3428	0000716
1714	\N	t	\N	 Compass 	3428	00000308
1715	\N	t	\N	 Renegade 	3428	0000309
1716	\N	t	\N	Ram 	3428	00000312
1717	\N	t	\N	Durango	3428	0000314
1718	\N	t	\N	Edge	3428	0000450
1719	\N	t	\N	LIVINA	3428	00001234
1720	\N	t	\N	LIVINA X-GEAR E	3428	00001235
1721	\N	t	\N	GRAND LIVINA	3428	00001236
1722	\N	t	\N	Tiggo	3428	000015
1723	\N	t	\N	Trailblazer	3428	0000016
1724	\N	t	\N	Grand Vitara	3428	000019
1725	\N	t	\N	Troller T4	3428	000045
1726	\N	t	\N	Tiida 	3428	000125
1727	\N	t	\N	T40	3428	0000150
1728	\N	t	\N	T5 MT	3428	0000151
1729	\N	t	\N	T5 CVT	3428	0000152
1730	\N	t	\N	T6	3428	0000153
1731	\N	t	\N	J5	3428	0000154
1732	\N	t	\N	J6	3428	0000155
1733	\N	t	\N	V260	3428	0000156
1734	\N	t	\N	T8	3428	0000158
1735	\N	t	\N	CAPTUR	3428	00001237
1736	\N	t	\N	KWID	3428	00001238
1737	\N	t	\N	SINISTRO NO VEÍCULO	3428	REP1
1738	\N	t	\N	DEFEITO MECÂNICO OU ELÉTRICO - EM GARANTIA	3428	REP2
1739	\N	t	\N	DEFEITO MECÂNICO OU ELÉTRICO - FORA DA GARANTIA	3428	REP3
1740	\N	t	\N	GARANTIA	3428	REV1
1741	\N	t	\N	CARROCERIA	3428	REV2
1742	\N	t	\N	ACESSÓRIOS	3428	INST
1743	\N	t	\N	VIRTUS	3428	0001239
1744	\N	t	\N	PICANTO	3428	000001225
1745	\N	t	\N	SOUL	3428	000001226
1746	\N	t	\N	QUORIS	3428	000001227
1747	\N	t	\N	BONGO	3428	000001228
1748	\N	t	\N	CARNIVAL	3428	000001229
1749	\N	t	\N	EQUINOX	3428	1212121
1750	\N	t	\N	Cronos	3428	00001239
1751	\N	t	\N	Argo	3428	00001240
1752	\N	t	\N	5008	3428	00001241
1753	\N	t	\N	Jumpy	3428	306
1754	\N	t	\N	Berlingo	3428	307
1755	\N	\N	\N	fasdfasdf	3428	\N
1756	\N	\N	\N	aaaaaaaaaaaaa	\N	\N
1757	\N	f	\N	Fox 2019	\N	\N
4	\N	f	\N	BMW	3428	\N
\.


--
-- Data for Name: profissaosantander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.profissaosantander (id, description, professionequivalentezflow) FROM stdin;
\.


--
-- Data for Name: proposta; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.proposta (id, aprovacaocobranca, aprovacaoentrega, aprovacaogerente, atendimentocodigo, empresacnpj, entregapesquisaemail, enviaemail, estoquetipo, liberacaoextra, propostacodigo, propostaobservacao, propostavalor, id_cliente_svs, id_unidade_organizacional, id_veiculo) FROM stdin;
\.


--
-- Data for Name: referenciabancaria; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.referenciabancaria (id, agencia, clientedesde, conta, contato, digitoagencia, digitoconta, nome, telefonecontato, tipoconta, id_banco, id_cidade, id_estado) FROM stdin;
\.


--
-- Data for Name: referenciapessoal; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.referenciapessoal (id, nome, relacionamento, telefonecelular, telefonefixo) FROM stdin;
\.


--
-- Data for Name: regra_spam_departamentos; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.regra_spam_departamentos (regraspamsvs_id, departamento) FROM stdin;
1	NOVOS
\.


--
-- Data for Name: regra_spam_fontes_lead; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.regra_spam_fontes_lead (regra_id, fonte_lead_id) FROM stdin;
1	7
1	8
\.


--
-- Data for Name: regra_spam_tipos_regra; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.regra_spam_tipos_regra (regraspamsvs_id, tipo_regra) FROM stdin;
1	EMAIL_BLACKLIST
\.


--
-- Data for Name: regra_spam_unidades; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.regra_spam_unidades (regra_id, unidade_id) FROM stdin;
1	2
1	3
\.


--
-- Data for Name: regraspamsvs; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.regraspamsvs (id, ativo, escopo, filtro, descricao, id_motivo_descarte) FROM stdin;
1	t	\N	\N	\N	\N
\.


--
-- Name: seq_alternativa_resposta; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_alternativa_resposta', 1, false);


--
-- Name: seq_andamentooperacao; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_andamentooperacao', 1, false);


--
-- Name: seq_ano_mod_comb_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_ano_mod_comb_santander', 1, false);


--
-- Name: seq_anunciosvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_anunciosvs', 84, true);


--
-- Name: seq_aprovacaobancaria; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_aprovacaobancaria', 1, false);


--
-- Name: seq_atividadeeconomicasantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_atividadeeconomicasantander', 1, false);


--
-- Name: seq_atividadeleadsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_atividadeleadsvs', 63, true);


--
-- Name: seq_avalista; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_avalista', 1, false);


--
-- Name: seq_banco; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_banco', 1, false);


--
-- Name: seq_bancosantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_bancosantander', 1, false);


--
-- Name: seq_calculorentabilidade; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_calculorentabilidade', 1, false);


--
-- Name: seq_campoparsesvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_campoparsesvs', 55, true);


--
-- Name: seq_cidade; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_cidade', 1, false);


--
-- Name: seq_cliente; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_cliente', 1, false);


--
-- Name: seq_cnpj_inst_financ; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_cnpj_inst_financ', 1, false);


--
-- Name: seq_configuracaoparsesvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_configuracaoparsesvs', 18, true);


--
-- Name: seq_conjuge; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_conjuge', 1, false);


--
-- Name: seq_dominioemailsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_dominioemailsvs', 13, true);


--
-- Name: seq_emailblacklist; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_emailblacklist', 6, true);


--
-- Name: seq_endereco; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_endereco', 1, false);


--
-- Name: seq_entidade_teste; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_entidade_teste', 1, true);


--
-- Name: seq_estado; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_estado', 1, false);


--
-- Name: seq_estado_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_estado_santander', 1, false);


--
-- Name: seq_estadocivilsantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_estadocivilsantander', 1, false);


--
-- Name: seq_faturamentoclientesvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_faturamentoclientesvs', 1, false);


--
-- Name: seq_financiamentoonlineitau; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_financiamentoonlineitau', 1, false);


--
-- Name: seq_financiamentoonlinesantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_financiamentoonlinesantander', 1, false);


--
-- Name: seq_fonteleadsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_fonteleadsvs', 12, true);


--
-- Name: seq_formapagamentosantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_formapagamentosantander', 1, false);


--
-- Name: seq_fornecedorclientesvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_fornecedorclientesvs', 1, false);


--
-- Name: seq_grauparentescosantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_grauparentescosantander', 1, false);


--
-- Name: seq_hist_int_leads_icarros; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_hist_int_leads_icarros', 4, true);


--
-- Name: seq_historicoalteracao; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_historicoalteracao', 689, true);


--
-- Name: seq_historicoleadsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_historicoleadsvs', 10, true);


--
-- Name: seq_icarrostoken; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_icarrostoken', 4, true);


--
-- Name: seq_icarrosusuario; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_icarrosusuario', 1, false);


--
-- Name: seq_imovelpatrimonio; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_imovelpatrimonio', 1, false);


--
-- Name: seq_instituicaofinanceira; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_instituicaofinanceira', 1, false);


--
-- Name: seq_itemcbc; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_itemcbc', 1, false);


--
-- Name: seq_json_credit_an_itau; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_json_credit_an_itau', 1, false);


--
-- Name: seq_json_ident_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_json_ident_santander', 1, false);


--
-- Name: seq_json_preanalise_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_json_preanalise_santander', 1, false);


--
-- Name: seq_json_req_sim_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_json_req_sim_santander', 1, false);


--
-- Name: seq_json_sim_resp_itau; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_json_sim_resp_itau', 1, false);


--
-- Name: seq_json_transac_req_itau; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_json_transac_req_itau', 1, false);


--
-- Name: seq_jsonrequisicaoproposta; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_jsonrequisicaoproposta', 1, false);


--
-- Name: seq_jsonrespostaproposta; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_jsonrespostaproposta', 1, false);


--
-- Name: seq_jsonrespostasimulacaosantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_jsonrespostasimulacaosantander', 1, false);


--
-- Name: seq_leadsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_leadsvs', 150, true);


--
-- Name: seq_leadsvsenriquecido; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_leadsvsenriquecido', 21, true);


--
-- Name: seq_marcasantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_marcasantander', 1, false);


--
-- Name: seq_masc_taxa_tabela_fin; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_masc_taxa_tabela_fin', 1, false);


--
-- Name: seq_modelo_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_modelo_santander', 1, false);


--
-- Name: seq_modeloquestionario; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_modeloquestionario', 2, true);


--
-- Name: seq_modeloveiculo; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_modeloveiculo', 1, false);


--
-- Name: seq_motivoaprovacaosvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_motivoaprovacaosvs', 1, false);


--
-- Name: seq_motivodescartesvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_motivodescartesvs', 5, true);


--
-- Name: seq_municipio; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_municipio', 1, false);


--
-- Name: seq_municipio_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_municipio_santander', 1, false);


--
-- Name: seq_nacionalidadesantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_nacionalidadesantander', 1, false);


--
-- Name: seq_naturezajuridicasantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_naturezajuridicasantander', 1, false);


--
-- Name: seq_notaleadsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_notaleadsvs', 9, true);


--
-- Name: seq_ocupacao; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_ocupacao', 1, false);


--
-- Name: seq_opcaoitemcbc; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_opcaoitemcbc', 1, false);


--
-- Name: seq_opcionalveiculo; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_opcionalveiculo', 1, false);


--
-- Name: seq_operacaofinanciada; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_operacaofinanciada', 1, false);


--
-- Name: seq_outrarendaclientesvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_outrarendaclientesvs', 1, false);


--
-- Name: seq_parcelaentrada; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_parcelaentrada', 1, false);


--
-- Name: seq_perfilsvs; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_perfilsvs', 1, false);


--
-- Name: seq_perg_questionario; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_perg_questionario', 1, true);


--
-- Name: seq_porteempresasantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_porteempresasantander', 1, false);


--
-- Name: seq_produtofinanceiro; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_produtofinanceiro', 1, false);


--
-- Name: seq_produtointeresse; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_produtointeresse', 1757, true);


--
-- Name: seq_profissaosantander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_profissaosantander', 1, false);


--
-- Name: seq_proposta; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_proposta', 1, false);


--
-- Name: seq_referenciabancaria; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_referenciabancaria', 1, false);


--
-- Name: seq_referenciapessoal; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_referenciapessoal', 1, false);


--
-- Name: seq_regraspam; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_regraspam', 1, true);


--
-- Name: seq_tabelafinanceira; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tabelafinanceira', 1, false);


--
-- Name: seq_taxa_perfil_if; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_taxa_perfil_if', 1, false);


--
-- Name: seq_taxa_tabela_financeira; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_taxa_tabela_financeira', 1, false);


--
-- Name: seq_taxaperfiltabelafinanceira; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_taxaperfiltabelafinanceira', 1, false);


--
-- Name: seq_telefone; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_telefone', 1, false);


--
-- Name: seq_tipo_ativ_ec_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipo_ativ_ec_santander', 1, false);


--
-- Name: seq_tipo_doc_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipo_doc_santander', 1, false);


--
-- Name: seq_tipo_rel_emp_santander; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipo_rel_emp_santander', 1, false);


--
-- Name: seq_tipoendereco; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipoendereco', 1, false);


--
-- Name: seq_tipopagamento; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipopagamento', 1, false);


--
-- Name: seq_tipotabelafinanceira; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipotabelafinanceira', 1, false);


--
-- Name: seq_tipotelefone; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_tipotelefone', 1, false);


--
-- Name: seq_unidadeorganizacional; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_unidadeorganizacional', 145, true);


--
-- Name: seq_usuario; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_usuario', 4262, true);


--
-- Name: seq_veiculopatrimonio; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_veiculopatrimonio', 1, false);


--
-- Name: seq_veiculoproposta; Type: SEQUENCE SET; Schema: public; Owner: dbroot
--

SELECT pg_catalog.setval('public.seq_veiculoproposta', 1, false);


--
-- Data for Name: tabela_financ_unidades_v2; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tabela_financ_unidades_v2 (tabela_financeira_id, unidade_id) FROM stdin;
\.


--
-- Data for Name: tabelafinanceira; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tabelafinanceira (id, ativa, datafinal, datainicial, nome, tipo, valorbonus, valorcartorio, valorgravame, valoroutros, valorpercentualbonus, valorpercentualplus, valorplus, valortaccobrada, valortacdevolvida, valorvistoria, id_instituicao_financeira, id_produto_financeiro) FROM stdin;
\.


--
-- Data for Name: taxa_perfil_if; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.taxa_perfil_if (id, taxafinal, taxainicial, instituicao_financeira_id, perfil_svs_id) FROM stdin;
\.


--
-- Data for Name: taxa_tabela_financeira; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.taxa_tabela_financeira (id, anofim, anoinicio, carencia, coeficiente, parcelas, percentualentrada, percentualrebate, percentualtaxaretorno, taxames, valorrebate, zerokm, id_mascara, tabela_financeira_id) FROM stdin;
\.


--
-- Data for Name: taxaperfiltabelafinanceira; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.taxaperfiltabelafinanceira (id, taxafinal, taxainicial, perfil_svs_id, tabela_financeira_id) FROM stdin;
\.


--
-- Data for Name: telefone; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.telefone (id, codigodealerworkflow, ddd, telefone, id_tipo_endereco, id_tipo_telefone) FROM stdin;
\.


--
-- Data for Name: tipo_ativ_ec_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipo_ativ_ec_santander (id, description) FROM stdin;
\.


--
-- Data for Name: tipo_doc_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipo_doc_santander (id, description) FROM stdin;
\.


--
-- Data for Name: tipo_rel_emp_santander; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipo_rel_emp_santander (id, description) FROM stdin;
\.


--
-- Data for Name: tipoendereco; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipoendereco (id, ativo, codigodealerworkflow, descricao) FROM stdin;
\.


--
-- Data for Name: tipopagamento; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipopagamento (id, ativo, codigodealerworkflow, descricao, tipo) FROM stdin;
\.


--
-- Data for Name: tipotabelafinanceira; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipotabelafinanceira (id, ativo, nome) FROM stdin;
\.


--
-- Data for Name: tipotelefone; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.tipotelefone (id, ativo, codigodealerworkflow, descricao) FROM stdin;
\.


--
-- Data for Name: unidadeorganizacional; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.unidadeorganizacional (id, cnpj, nomefantasia, ativo, emailintegracao, datainiciointegracao, integra) FROM stdin;
2	06.318.439/0002-24	Sagakasa Udi	t	\N	\N	\N
3	10.257.223/0001-92	Estação Sadif Corretora	t	\N	\N	\N
4	00.752.386/0009-45	CONS PVH	t	\N	\N	\N
5	00.752.386/0011-60	CONS CBA	t	\N	\N	\N
6	00.752.386/0010-89	CONS UDI	t	\N	\N	\N
7	12.657.826/0002-98	Saga Caminhões CBA	t	\N	\N	\N
8	01.104.751/0011-92	Saga Cidade	t	\N	\N	\N
9	01.104.751/0007-06	Saga Hyundai Gyn	t	\N	\N	\N
10	01.104.751/0008-97	Saga Hyundai Ana	t	\N	\N	\N
11	01.104.751/0009-78	Saga Hyundai Bsb Imports	t	\N	\N	\N
12	01.104.751/0010-01	Crt Bsb	t	\N	\N	\N
13	03.267.961/0001-55	Saga Autominas Udi	t	\N	\N	\N
14	03.267.961/0002-36	Saga Autominas Ara	t	\N	\N	\N
15	08.860.168/0001-89	Estação Renault VGD	t	\N	\N	\N
16	09.348.217/0001-61	Estação Fiat	t	\N	\N	\N
17	08.748.749/0001-23	Saga Amazônia BR	t	\N	\N	\N
18	05.471.879/0001-73	Saga Motors Gyn	t	\N	\N	\N
19	05.471.879/0002-54	Saga Motors Ana	t	\N	\N	\N
20	06.318.439/0001-43	Sagakasa Gyn	t	\N	\N	\N
21	11.458.618/0001-16	Autominas France	t	\N	\N	\N
22	09.348.217/0004-04	Estação Cidade	t	\N	\N	\N
23	11.307.610/0001-59	Saga Autominas Representações	t	\N	\N	\N
24	09.348.217/0002-42	Estação CRT	t	\N	\N	\N
25	08.803.375/0001-00	Sagamat	t	\N	\N	\N
26	12.657.826/0001-07	Saga Korea CRT 	t	\N	\N	\N
27	09.348.217/0003-23	Estação Gama	t	\N	\N	\N
28	14.234.954/0001-73	Tudo Gyn	t	\N	\N	\N
29	14.234.954/0002-54	Tudo Buriti	t	\N	\N	\N
30	08.748.749/0003-95	Saga Hyundai Pvh Hmb	t	\N	\N	\N
31	08.860.168/0002-60	Seminovos CBA	t	\N	\N	\N
32	08.717.300/0001-06	Saga Norte	t	\N	\N	\N
33	00.283.283/0001-26	Saga Corretora	t	\N	\N	\N
34	01.104.751/0012-73	CRT GYN	t	\N	\N	\N
35	00.752.386/0001-98	SEMINOVOS MATRIZ	t	\N	\N	\N
36	00.752.386/0006-00	CONS BSB	t	\N	\N	\N
37	11.727.257/0002-47	Saga Japan TGT	t	\N	\N	\N
38	14.371.570/0001-00	Tudo Corretora	t	\N	\N	\N
39	11.748.698/0002-25	Estação Japan VGD	t	\N	\N	\N
40	03.267.961/0004-06	Saga Autominas Parque	t	\N	\N	\N
41	10.272.533/0002-67	Park Ford	t	\N	\N	\N
42	17.173.777/0001-50	Saga Nice Sls	t	\N	\N	\N
43	12.657.826/0003-79	Saga Caminhões Udi	t	\N	\N	\N
44	00.752.386/0004-30	Consórcio   Interior	t	\N	\N	\N
45	01.104.751/0015-16	Saga Hyundai BSB HMB	t	\N	\N	\N
46	20.025.467/0001-01	Autotech Gyn	t	\N	\N	\N
47	20.374.616/0001-30	Saga Munique Gyn	t	\N	\N	\N
48	20.379.987/0001-04	Gramarca Vgd	t	\N	\N	\N
49	20.379.987/0005-38	Gramarca CDP	t	\N	\N	\N
50	20.379.987/0006-19	Gramarca Miguel Sutil	t	\N	\N	\N
51	16.803.158/0004-82	Saga Paris Bsb	t	\N	\N	\N
52	08.860.168/0004-21	Audi Cba	t	\N	\N	\N
53	21.214.513/0001-75	Saga Michigan	t	\N	\N	\N
54	11.727.257/0003-28	Saga Nissan Gyn	t	\N	\N	\N
55	11.727.257/0005-90	Saga Nissan Ana	t	\N	\N	\N
56	19.945.014/0003-78	Saga Detroit T9	t	\N	\N	\N
57	19.945.014/0002-97	Saga Detroit Ana	t	\N	\N	\N
58	12.657.826/0005-30	Saga Korea Gyn	t	\N	\N	\N
59	12.657.826/0007-00	Saga Korea Ana	t	\N	\N	\N
60	12.657.826/0006-11	Saga Korea CJ	t	\N	\N	\N
61	19.945.014/0006-10	Saga Detroit Asa Norte	t	\N	\N	\N
62	19.945.014/0004-59	Saga Detroit Sls	t	\N	\N	\N
63	12.657.826/0008-83	Saga Korea Tgt	t	\N	\N	\N
64	08.748.749/0004-76	Saga Hyundai Pvh Imports	t	\N	\N	\N
65	05.471.879/0004-16	Saga Motors Bsb	t	\N	\N	\N
66	01.104.751/0018-69	Saga Caminhões Hyundai	t	\N	\N	\N
67	22.280.413/0001-00	Hyundai Vgd Seul	t	\N	\N	\N
68	21.428.039/0001-84	Hyundai Pvh Asia	t	\N	\N	\N
69	12.657.826/0009-64	Saga Korea Bsb	t	\N	\N	\N
70	17.173.777/0002-31	Saga Nice Bsb	t	\N	\N	\N
71	23.419.095/0001-88	Saga Lyon	t	\N	\N	\N
72	19.945.014/0005-30	Saga Detroit Colorado	t	\N	\N	\N
73	09.102.044/0001-05	Saga Brasil	t	\N	\N	\N
74	22.280.413/0002-90	Hyundai CBA Seul	t	\N	\N	\N
75	00.283.283/0003-98	Saga Corretora Filial	t	\N	\N	\N
76	00.752.386/0012-40	Seminovos GO T 7	t	\N	\N	\N
77	00.752.386/0013-21	Seminovos GO Parque	t	\N	\N	\N
78	00.752.386/0014-02	Seminovos GO Repasse	t	\N	\N	\N
79	00.752.386/0019-17	Seminovos GO Senador Canedo	t	\N	\N	\N
80	05.471.879/0003-35	Saga Motors Buriti	t	\N	\N	\N
81	21.333.642/0001-82	Saga London	t	\N	\N	\N
83	11.727.257/0001-66	Saga Japan Bsb	t	\N	\N	\N
84	20.374.616/0002-10	Saga Munique Motos	t	\N	\N	\N
85	14.617.092/0001-68	Saga Malls	t	\N	\N	\N
86	00.752.386/0018-36	Seminovos GO Anápolis	t	\N	\N	\N
87	20.379.987/0003-76	Gramarca Cáceres	t	\N	\N	\N
88	00.752.386/0015-93	Seminovos GO Toyota	t	\N	\N	\N
89	00.752.386/0016-74	Seminovos GO Premium	t	\N	\N	\N
90	01.104.751/0019-40	Hyundai Tgt Hmb	t	\N	\N	\N
91	01.104.751/0014-35	Saga Gama	t	\N	\N	\N
92	00.752.386/0027-27	Seminovos DF Sadif	t	\N	\N	\N
93	00.752.386/0028-08	Seminovos DF Colorado	t	\N	\N	\N
94	00.752.386/0030-22	Seminovos DF Cidade	t	\N	\N	\N
95	09.102.044/0005-20	Saga Brasil RO	t	\N	\N	\N
96	09.102.044/0006-01	Saga Brasil Udi	t	\N	\N	\N
97	00.752.386/0017-55	Seminovos GO Buriti	t	\N	\N	\N
98	28.280.665/0001-15	Sn MA Nice	t	\N	\N	\N
99	15.642.239/0001-32	Super Sagakasa	t	\N	\N	\N
100	30.522.927/0001-52	Saga Peugeot Sls	t	\N	\N	\N
101	30.530.947/0001-75	Hyundai Sls Imports	t	\N	\N	\N
102	00.752.386/0032-94	Seminovos DF Taguatinga	t	\N	\N	\N
103	09.348.217/0005-95	Estação Fiat São Luís	t	\N	\N	\N
104	11.748.698/0001-44	Saga Japan VGD	t	\N	\N	\N
105	10.272.533/0004-29	Park Ford Gama	t	\N	\N	\N
106	30.689.023/0001-16	Saga Citroen Sls	t	\N	\N	\N
107	00.752.386/0021-31	Seminovos DF Valparaíso	t	\N	\N	\N
108	00.752.386/0033-75	Seminovos DF Guará	t	\N	\N	\N
109	16.803.158/0001-30	Saga Paris Gyn	t	\N	\N	\N
110	01.104.751/0004-63	Saga BSB	t	\N	\N	\N
111	26.343.161/0001-71	Triumph Mototech	t	\N	\N	\N
112	09.348.217/0007-57	Deposito Sadif	t	\N	\N	\N
113	01.104.751/0013-54	Deposito Saga	t	\N	\N	\N
114	09.102.044/0004-40	Saga Brasil MT	t	\N	\N	\N
115	19.945.014/0001-06	Saga Detroit Gyn	t	\N	\N	\N
116	13.243.978/0001-26	Saga France   Peugeot	t	\N	\N	\N
117	30.018.686/0001-09	Saga Kia Sls	t	\N	\N	\N
118	09.348.217/0006-76	Estação Fiat Colorado	t	\N	\N	\N
119	00.752.386/0029-99	Seminovos DF Gama	t	\N	\N	\N
120	00.752.386/0031-03	Seminovos DF Bsb	t	\N	\N	\N
121	00.752.386/0034-56	Seminovos DF Scia	t	\N	\N	\N
122	00.752.386/0039-60	Seminovos DF Asa Norte	t	\N	\N	\N
123	26.343.161/0002-52	Saga Indian	t	\N	\N	\N
124	13.554.051/0001-07	Saga Participações	t	\N	\N	\N
125	05.471.879/0005-05	Saga Motors Asa Norte	t	\N	\N	\N
126	16.803.158/0009-97	Sn Paris T7	t	\N	\N	\N
127	16.803.158/0010-20	Sn Paris Parque	t	\N	\N	\N
128	16.803.158/0011-01	Sn Paris Repasse	t	\N	\N	\N
129	16.803.158/0005-63	Sn Paris Buriti	t	\N	\N	\N
130	01.104.751/0017-88	Triumph Gyn	t	\N	\N	\N
131	10.272.533/0001-86	Parque Ford	t	\N	\N	\N
132	11.727.257/0004-09	Saga Nissan Rvd	t	\N	\N	\N
133	08.748.749/0002-04	Saga Amazônia JT	t	\N	\N	\N
134	09.102.044/0003-69	Saga Brasil DF	t	\N	\N	\N
135	27.885.564/0001-05	Tudo Sls	t	\N	\N	\N
136	16.803.158/0008-06	Sn Paris Toyota	t	\N	\N	\N
137	16.803.158/0007-25	Sn Paris Premium	t	\N	\N	\N
138	16.803.158/0006-44	Sn Paris Anapolis	t	\N	\N	\N
139	30.903.216/0001-28	Saga Renault Pvh	t	\N	\N	\N
140	09.102.044/0002-88	Saga Brasil GO	t	\N	\N	\N
141	13.243.978/0002-07	Saga Kia Bsb	t	\N	\N	\N
142	09.102.044/0007-92	Saga Brasil MA	t	\N	\N	\N
143	28.280.665/0002-04	Sn MA Tudo	t	\N	\N	\N
144	03.267.961/0003-17	Audi Center Udi	t	\N	\N	\N
145	19.945.014/0007-00	Saga Detroit Tgt	t	\N	\N	\N
82	01.104.751/0001-10	Saga T 7	t	leadsvwgyn@saganet.net.br	2018-08-11 08:00:00	t
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.usuario (id, ativo, cpf, login, nome) FROM stdin;
1	t	80275974120	MARCIO.CHAVES	MARCIO ADELINO CHAVES
2	t	95608940130	RAFAELVAZ	RAFAEL DE SOUZA VAZ
3	t	01500182150	ROGERIOTELES	ROGERIO ALVES TELES
4	t	79610900615	AILTON.JUNIOR	AILTON RODRIGUES BATISTA JUNIOR
5	t	11720772827	ALECIO	ALECIO SCANDOLIERE NETO
6	t	30762308168	EISERMAIA	EISER MAIA DA SILVEIRA
7	t	11335041249	WILSONPEREIRA	WILSON PEREIRA DA SILVA
8	t	27105911115	ANTONIO.DIAS	ANTONIO FRANCISCO DE MACEDO DIAS
9	t	21925500659	ANTONIOVAZ	ANTONIO VAZ DE OLIVEIRA
10	t	90173597653	RAUL.PJUNIOR	RAUL DAMASIO PERILLO JUNIOR
11	t	68059507672	GIULLIANO	GIULLIANO FERREIRA NAVES
12	t	80658083104	REGINALDO.ROCHA	REGINALDO DA SILVA ROCHA
13	t	33338841191	HAMILTONSANTOS	HAMILTON JOSE DOS SANTOS
14	t	78032768149	ROGERS	WILLIAN ROGERS VILELA PIRES
15	t	20005822653	MARIARITA	MARIA RITA MAIA DE VASCONCELOS
16	t	97485772104	RENATANUNES	RENATA NUNES DE PAULO
17	t	86803786153	SIDNEI.SOUZA	SIDNEI DE SOUZA
18	t	98234382187	HANNAPAULA	HANNA PAULA JOSECARLOS FERRANTE
19	t	00125499167	SHIRLEY	SHIRLEY NADALUTH DA SILVA
20	t	48495247615	LUIZ.HMARTINS	LUIZ HUMBERTO MARTINS
21	t	94945020191	TINA	ERNESTINA RODRIGUES BARBOSA
22	t	01034668196	VICTORHUGO	VICTOR HUGO MENEZES DE SOUZA
23	t	01192714199	WELIDA.OLIVEIRA	WELIDA HIPOLITO DE OLIVEIRA
24	t	24174700125	RUBENSFILHO	RUBENS DA SILVA PEREIRA FILHO
25	t	00429042183	PAULOMELO	PAULO RAMMON DE MOURA FONSECA MELO
26	t	32601131187	VALDECIROSA	VALDECI ROSA DA SILVA
27	t	97010960100	ANTONIO	ANTONIO LISBOA SANTIAGO FILHO
28	t	23463082187	ELIENE.SILVA	ELIENE DE SOUZA E SILVA
29	t	96346310100	KELLEN.BRODRIGUES	KELLEN BARBOSA RODRIGUES
30	t	10177249625	KATHELLEMALVES	KATHELLEM LEMES ALVES
31	t	07946181620	DANIELE.ZAGO	DANIELE FERREIRA GOMES ZAGO
32	t	49841785668	MARCOSANTONIO.GOMES	MARCOS ANTONIO GOMES
33	t	69324255134	ROBERTO	ADRIANO CORTES VIEIRA
34	t	43933564115	JOAO.BATISTA	JOAO BATISTA DA SILVA
35	t	99195380191	PATRICIA.BENTO	PATRICIA BENTO DOS SANTOS
36	t	01127066145	LIZA.PONTE	LIZA CAROLINE BORGES PONTE ALVES
37	t	79748260100	SEBASTIAO	SEBASTIAO CARLOS DE JESUS
38	t	53929381168	MARCELO LUIZ	MARCELO LUIZ PEREIRA ARAUJO
39	t	30496900110	ERALDO.MILHOMEM	ERALDO WAGNER MACHADO MILHOMEM
40	t	84192917653	HIDEO.TAKAHASHI	HIDEO ALEXANDRE TAKAHASHI
41	t	34976132153	EDIS	EDIS FEITOSA DA COSTA
42	t	86719084100	THATIANY	THATIANY ARANTES DA SILVA
43	t	84002069168	NELSON	NELSON AGUIAR NUNES
44	t	00181784645	MARCIA.PERES	MARCIA BARBOSA PERES
45	t	56675992104	RACHEL	RACHEL MELO FILIZZOLA PINHEIRO
46	t	50514504153	JULIO	JULIO CESAR COUTINHO
47	t	16582985604	WALTER	WALTER JOSE PEREIRA
48	t	53431243649	LUCIO.PERFEITO	LUCIO FLAVIO COSTA PERFEITO
49	t	71026665191	FRANCIELE.MORAIS	FRANCIELE CRISTINA DE MORAES
50	t	43614094120	DINIZ	MARCELO CORREA DE SOUSA DINIZ
51	t	87366649104	GUSTAVO	GUSTAVO FERRO DO VALE
52	t	30239958187	WILTON	WILTON CARLOS DE ABREU
53	t	11604709790	HEITOR.ALVES	HEITOR MOREIRA ALVES
54	t	02105562930	JAYR	JAYR PELEGRINELLI AGUILAR
55	t	21563110130	EVANDRO.MAIA	EVANDRO MAIA DA SILVEIRA
56	t	41498577172	CARLESIO.SANTANA	CARLESIO WILTON DE SANTANA
57	t	30030315115	HELDERASSIS	HELDER ASSIS DE ARAUJO
58	t	03932562607	CRISTIANE.OCUNHA	CRISTIANE OLIVEIRA DA CUNHA
59	t	70185615155	THAYNARA.DUARTE	THAYNARA ALVES PINHEIRO DUARTE
60	t	05530293123	leidiane.rsouza	LEIDIANE ROSA DE SOUZA
61	t	55241336168	marcio.sandrade	MARCIO DA SILVA ANDRADE
62	t	04926859165	LAIS.PBORGES	LAIS PRISCILA BORGES
63	t	58421297104	MARCIO.SOUZA	MARCIO TADEU DE SOUZA
64	t	03463910837	LUISCLAUDIO	LUIS CLAUDIO DESIDERIO
65	t	66445434134	KLEUBER.SILVA	KLEUBER ANDERSON DE ALMEIDA E SILVA
66	t	17146788814	ANDERSONSATO	ANDERSON TERUO SATO
67	t	01462442650	DIOGO.ABARNER	DIOGO ARAUJO BARNER
68	t	00832906131	SANDROCOSTA	SANDRO HENRIQUE RAMOS COSTA
69	t	53591895172	MARYAPARECIDA	MARY APARECIDA DE LIMA
70	t	66983800168	BIA FARIA	BIANCA DA SILVA FERREIRA DE FARIA
71	t	57985090100	EVANDROCARLOS	EVANDRO CARLOS DE ARAUJO
72	t	58559094172	NARAAKEGAWA	NARA AKEGAWA COSTA
73	t	26450871830	PAULO.ASOUZA	PAULO HENRIQUE ALVES DE SOUZA
74	t	07049515841	SUZIMAR	SUZIMAR CRISTINA DE ANDRADE TANFERRI
75	t	92015689168	ADRIANOMAXIMO	ADRIANO MAXIMO
76	t	86086081115	ELAINE.MERE	ELAINE MERI DE OLIVEIRA BARBOSA
77	t	08780651640	FLAVIACAROLINA	FLAVIA CAROLINA DOS SANTOS
78	t	99120046120	MARYELESENNA	MARYELE DE SENNA NOGUEIRA
79	t	00737052171	EDUARDO.LEAO	EDUARDO DE AREA LEAO MONTEIRO
80	t	00149536178	MARCUS.AMARAL	MARCUS VINICIUS LEITE AMARAL COELHO
81	t	87502941134	CARLOS.DIAS	CARLOS EDUARDO DIAS
82	t	00606821163	GREICE.BASTOS	GREICE DENISE BASTOS
83	t	56668619134	LUCIA.BASTOS	MARILUCI RIBEIRO DE MELO BASTOS
84	t	76420604087	ARMANDO.JUNIOR	ARMANDO PENA JUNIOR
85	t	01950499189	BRUNAPINHO	BRUNA APARECIDA DE PINHO
86	t	71651250197	LARISSAMELO	LARISSAMELO
87	t	07283167498	HELZIRRAMON	HELZIR RAMON DE MENDONCA
88	t	52922448215	ALEXANDRE	ALEXANDRE FELIPE SOARES
89	t	73457051100	IUSLANABDALA	IUSLAN CARDOSO ABDALA
90	t	44031777191	ODALENE.OLIVEIRA	ODALENE NASCIMENTO DE MOURA OLIVEIRA
91	t	32322143120	ROSEMBERGUE	ROSEMBERGUE MARCIO G SILVA
92	t	73419621191	RODRIGOLAPA	RODRIGO VIEIRA LAPA
93	t	03652379144	THIAGOALVES	THIAGO ALVES NUNES GOMES
94	t	90307950182	ALINEZIA	ALINE ZIA PEREIRA DANTAS
95	t	54791731115	MARCELOMARMO	MARCELO MARMO PEIXOTO VIROTE
96	t	47215070115	IRACEMA.RIBEIRO	IRACEMA DA CONCEICAO RIBEIRO
97	t	65841980149	FLAZICOPEREIRA	FLAZICO PEREIRA DE CASTRO
98	t	47246448187	HELIOJUNIOR	HELIOJUNIOR
99	t	60814659268	testecsg	testecsg
100	t	00217073166	WILTERSILVA	WILTER SILVA OLIVEIRA
101	t	82346690104	CRISTIANE.NASCIMENTO	CRISTIANE FERREIRA DO NASCIMENTO
102	t	91862558191	ELIANE.MORAIS	ELIANE ROSA DE MORAIS
103	t	73087041149	SEBASTIAO.FILHO	SEBASTIAO RODRIGUES FILHO
104	t	02791061142	JOSIMARALBERTO	JOSIMAR ALBERTO BARBOSA
105	t	02676457160	GUILHERME.ARAUJO	GUILHERME PEREIRA DE ARAUJO
106	t	18601499104	DIVASOUZA	DIVA RODRIGUES DE ALMEIDA SOUZA
107	t	99188473104	KELSON.SANTOS	KELSON DE OLIVEIRA SANTOS
108	t	04557992102	THYRLENVIVIAN	THYRLEN VIVIAN MENDONCA DA MAIA
109	t	79265570130	CRISTIANO.COSTA	CRISTIANO PEREIRA DA COSTA
110	t	73074136120	TAMARA.SILVA	TAMARA MICHELLY DE OLIVEIRA SILVA
111	t	02498541128	ALESSA.NAIARA	APARECIDA ALESSA NAIARA PAULA DOS SANTOS
112	t	99919923672	WARREN.LACERDA	WARREN TADEU DE OLIVEIRA LACERDA
113	t	02726752969	ERCSONJUNIOR	ERCSON JUNIOR GOUVEIA
114	t	01151027103	THEISA.OLIVEIRA	THEISA LOSCHI DE OLIVEIRA
115	t	90029801168	MOHAMAD.LIMA	MOHAMAD RAMOS LIMA JADALLAH
116	t	70111030668	ELOISIO.BORGES	ELOISIO DE DEUS BORGES
117	t	72746459191	VANESSA.SILVA	VANESSA DUTRA DA SILVA
118	t	06068365603	KARLACAROLINA	KARLA CAROLINA DE OLIVEIRA
119	t	60702540153	MARCOSGOMES	MARCOS ANDRE GOMES
120	t	48517020600	LUIZANTONIO	LUIZ ANTONIO GONCALVES BORGES
121	t	11295418860	EMERSONANGELO	EMERSON LUIZ ANGELO
122	t	38983206187	SHEILA.CRISTINA	SHEILA CRISTINA DE FIGUEIREDO
123	t	00993126111	MILLENA	MILLENA LOPES SWITALSCI
124	t	47197323304	ANAPAULA	ANAPAULA
125	t	71225951100	MARIELLE	MARIELLE NERY FERNANDES BARROS
126	t	97032166172	MARIADIAS	MARIA DIAS DE SOUSA
127	t	70065942191	JARDEL.REGO	JARDEL ANTUNES BENTO REGO
128	t	46996591104	NAIME.FERES	NAIME XAVIER FERES
129	t	43228496791	JOSEEDUARDO	JOSE EDUARDO DE CARVALHO MAIA
130	t	64920941234	ITALO.CFONSECA	ITALO CORTEZ DA FONSECA
131	t	05512945661	LEONARDO.RAPHAEL	LEONARDO RAPHAEL DE SOUZA
132	t	71248129172	FABRICIO.CAMPOS	FABRICIO BATISTA CAMPOS
133	t	02394268190	JOSE.ABDO	JOSE EDUARDO FERREIRA DE ARAUJO ABDO
134	t	67215068315	MICHELE.ARAUJO	MICHELE CHRISTINA ARAUJO DE ARAUJO
135	t	00117614157	KEYLLA.VALE	KEYLLA CHRISTYANE DE SA VALE
136	t	89891473449	YARACARNEIRO	YARA CARNEIRO DE ALBUQUERQUE SARMANHO
137	t	00029369177	LEOZOCCOLI	LEONARDO ZOCCOLI CARVALHO FRANCO
138	t	36125970191	JUNIO.CASTRO	JUNIO CARDOSO CASTRO
139	t	01480612146	GEIFFERSON.SOUZA	GEIFFERSON BENICIO DE SOUZA
140	t	00252088107	NAYARA.CTEIXEIRA	NAYARA CAROLINE NUNES TEIXEIRA
141	t	70499782135	cybelle.aaraujo	CYBELLE ALMEIDA ARAUJO
142	t	75065410178	FABIOLA.ASILVA	FABIOLA ALVES DA SILVA
143	t	84984902191	PAULO.BRUNING	PAULO ROBERTO DE LIMA BRUNING
144	t	01724186078	CRISTINASANTOS	CRISTINA DOS SANTOS KAUFMANN
145	t	02515491197	RITA.PEDROSO	RITA DE CASSIA PEDROSO BORGES
146	t	47077590259	PAULO.BERGAMIN	PAULO HENRIQUE BERGAMIN
147	t	33311617304	MARCELUS	MARCELUS BARTOLOMEU PEREIRA
148	t	01973914190	JOAO.ANTUNES	JOAO PAULO ANTUNES TRINDADE
149	t	68355181620	ANDERSON.MENEZES	ANDERSON DE MENEZES SILVA
150	t	02988786151	FERNANDAMENDES	FERNANDA MENDES GIMENES
151	t	75670755120	CAMILAVILELA	CAMILA VILELA PATO REZENDE
152	t	01912889188	DANILOVERAS	DANILO VERAS DA SILVA
153	t	69056803115	GETULIOCOSTA	GETULIO DE ANDRADE COSTA
154	t	30159598168	SANDRA.LEITE	SANDRA MARIA FAVIERO LUCCAS LEITE
155	t	91343860172	IARA.CARVALHO	IARA FERNANDA DE SOUZA CARVALHO
156	t	00642821127	LIASILVA	LIA SILVA GONCALVES
157	t	00161637108	MIROMAR.JUNIOR	MIROMAR FRANCISCO DE ALCANTARA JUNIOR
158	t	75139812134	RUANCARLOS	RUAN CARLOS FERNANDES LIMA
159	t	96202432187	CLAUDIANEVES	CLAUDIA RODRIGUES NEVES
160	t	01395454175	KATRINE.SANTOS	KATRINE GOMES DOS SANTOS
161	t	82893764134	MAXWELL.MACEDO	MAXWELL LEITE DE MACEDO
162	t	02109508116	ANDERSON.JUNIO	ANDERSON JUNIO DE OLIVEIRA NUNES
163	t	99982560620	DANIEL.MONTINI	DANIEL VECCHI MONTINI
164	t	28555970130	FERNANDOMAIA	ANTONIO FERNANDO DE OLIVEIRA MAIA
165	t	86386026187	LORRAINE.PEREIRA	LORRAINE PEREIRA
166	t	70548664102	MURILO.MRODRIGUES	MURILO.MRODRIGUES
167	t	03629329136	LEILA.CARVALHO	LEILA APARECIDA CARVALHO
168	t	90093321104	NADYA	NADYA MARTINS SOARES DE MORAIS
169	t	71696113172	SHISLEY.FREITAS	SHISLEY DAVID DE FREITAS
170	t	83929401134	SIDNEYRIBEIRO	SIDNEY FRANCISCO RIBEIRO
171	t	59014024134	PATRICIA.GONTIJO	PATRICIA AQUILAS GONTIJO
172	t	02859988386	EDSONJUNIOR	EDSON RUI CORDEIRO MARQUES JUNIOR
173	t	95385576115	JEAN.CARLOS	JEAN CARLOS SILVA
174	t	06906344633	LUCAS.MEDEIROS	LUCAS ROBERTO MATIAS MEDEIROS
175	t	90968344100	DREY.OLIVEIRA	DREY MARQUES SILVA DE OLIVEIRA
176	t	89878221172	DANILOLIMA	DANILO FONSECA DE LIMA
177	t	04004482119	CRISLORRANE	CRISLORRANE FERREIRA SANTOS
178	t	45283508153	LEUNIAZEVEDO	LEUNI PINTO DE AZEVEDO
179	t	08953999626	IANE.FERREIRA	IANE DIAS FERREIRA
180	t	71218696168	FABRICIO.MOREIRA	FABRICIO DE PAULA SOUSA MOREIRA
181	t	90210646187	JOFFESON.CAVALCANTE	JOFFESON NUNES CAVALCANTE
182	t	02282219945	MARCOSALVES	MARCOS ALVES FRANCELINO
183	t	42491860163	ANA.MESQUITA	ANA CRISTINA DE AZEVEDO MESQUITA
184	t	03362956181	PAULO.HVITAL	PAULO HENRIQUE VITAL DOS SANTOS
185	t	02821314698	MAURO.SANTOS	MAURO GONCALVES DOS SANTOS
186	t	03169568132	WENDER.FERREIRA	WENDER FERREIRA TIRADENTES
187	t	74575570249	VALERIADE	VALERIA DE CAMARGO SALGADO
188	t	85349399187	LEANDRO.DINIZ	LEANDRO DA SILVA DINIZ
189	t	00464403138	FERNANDO.MARTINS	FERNANDO DA ROCHA MARTINS
190	t	00405856113	GEAN.MARIANO	GEAN CARLOS MARIANO DE JESUS
191	t	01314563181	ARIANA.PEREIRA	ARIANA ROCHA PEREIRA
192	t	70847002187	RAFAELABELLO	RAFAELA DA SILVA BELLO MORAIS
193	t	70832269204	GLAUCIMA	GLAUCIMA DIAS SOUZA
194	t	00141464100	GRAZIELLE	GRAZIELLE DE SOUZA MARQUES
195	t	00567982297	RAYANNER.NASCIMENTO	RAYANER OLIVEIRA DO NASCIMENTO
196	t	28978439187	MURILOJORGE	MURILO JORGE SAHIUM
197	t	91112575120	ALESSANDRO.MORAIS	ALESSANDRO MORAIS CIRQUEIRA
198	t	03455064132	ESTER.BRITO	ESTER GONCALVES DE BRITO
199	t	87160811100	ADRIANE.FIGUEIREDO	ADRIANE CHRISTINA DE FIGUEIREDO
200	t	87115212104	LILIANTAQUES	LILIAN FARIA DA SILVA TAQUES
201	t	00848235185	DALYLAH.REZENDE	DALYLAH DE MELO REZENDE
202	t	01196961182	DAIANE.REMEDIS	DAIANE CRISTINA BRITO REMEDIS
203	t	03443725619	MARCUS.CARMO	MARCUS VINICIUS DO CARMO
204	t	34902465272	MAGNO.SOUZA	MAGNO DIAS DE SOUZA
205	t	78504422100	ANDERSONARAGAO	ANDERSON FERREIRA ARAGAO
206	t	54550670100	GONCALONEVES	GONCALO DIAS DAS NEVES
207	t	48835285968	OLINEIS	OLI MIGUEL NEIS
208	t	11220606618	JESSICA.SANTOS	JESSICA MARQUES SANTOS
209	t	18870077845	SIDCLEI	SIDCLEI CONCEICAO DE LIMA
210	t	69869502172	ALEXSANDRALIMA	ALEXSANDRA LIMA FIGUEIREDO DE OLIVEIRA SILVA
211	t	83599835187	IOLANDO.VSOUZA	IOLANDO VASCO DE SOUZA
212	t	49570366915	ARTUR.RIGLESIAS	ARTUR ROBERTO IGLESIAS
213	t	47931191234	CARLOS.RBATISTA	CARLOS RANGEL BATISTA
216	t	00502747161	MICHAEL.BRITO	MICHAEL PEREIRA DE BRITO
217	t	35011579832	SUELEN.PRATES	SUELEN POLIANE VENTURA PRATES
218	t	11855524805	MARCELO.BERTONE	MARCELO VICOTO BERTONE
219	t	57087261134	ROSELY.ACOSTA	ROSELY AUXILIADORA DE OLIVEIRA COSTA
220	t	78737966120	CARMEMDEUS	APARECIDA CARMEM COSTA DE DEUS
221	t	90251687104	HUGO.ANTONIO	HUGO ANTONIO DA SILVA
222	t	93119585149	DEILA.PASSOS	DEILA CARNEIRO DOS PASSOS
223	t	76756017187	REGIANE.CARVELLI	REGIANE CARVELLI DE OLIVEIRA
224	t	81509863672	FLAVIODUARTE	FLAVIO LEONARDO DUARTE DE SOUZA
225	t	59213639287	RAIMUNDOFRANCA	RAIMUNDO SIDNEI FRANCA DO NASCIMENTO
226	t	03967087140	WLY.SILVA	WLY SIQUEIRA DA SILVA
227	t	84011548120	WAGNER.PAULA	WAGNER JOSE DE PAULA
228	t	02394063467	LEONARDO.MEDEIROS	ANTONIO LEONARDO BEZERRA DE MEDEIROS
229	t	00896339114	DENYSE.RIBEIRO	DENYSE RIBEIRO GUIMARAES
230	t	04300691118	DAVID.ESILVA	DAVID ERICK MAGALHAES DA SILVA
231	t	70245010149	CYNTHIA.MORAIS	CYNTHIA DE SOUSA SILVA MORAIS
232	t	06200027196	LIANDRA.AZEVEDO	LIANDRA SILVA AZEVEDO
233	t	03307169130	LEANDRO.MELO	LEANDRO MARQUES MELO
234	t	75769220104	MAYARA.FGONCALVES	MAYARA FERREIRA GONCALVES
235	t	03688583167	FABIO.ARAUJO	FABIO ARAUJO SANTOS
236	t	34700277149	MARCELLO.FALEIRO	MARCELLO FALEIRO DE OLIVEIRA
237	t	70007838182	WALINTHON.SSILVA	WALINTHON SANTOS DA SILVA
238	t	01840090146	CHRISTIELLY.SIQUEIRA	CHRISTIELLY SANTOS SIQUEIRA
239	t	02165831105	RENNER.PMARQUES	RENNER PEREIRA MARQUES
240	t	59219742187	EDUARDO.MENDES	EDUARDO MENDES DE SOUZA
241	t	02978205164	POLLYANNA.RSILVA	POLLYANNA RODRIGUES DA SILVA
242	t	01052848150	BRUNNO.RROSA	BRUNNO RAPHAEL TEIXEIRA ROSA
243	t	74229800010	ANDERSON.SILVA	ANDERSON.SILVA
244	t	10692777628	CINTIA.SANTANA	CINTIA ALBINA SANTANA
245	t	58343270010	GICELA.RAMOS	GICELA RAMOS
246	t	48301086653	WELLINGTON.GFERREIRA	WELLINGTON GONCALVES FERREIRA
247	t	06537744946	ANA.SANTIN	ANA CRISTINA SANTIN
248	t	72679654153	vinicius.mbarreto	VINICIUS DE MATOS BARRETO
249	t	01726954145	HEUBERT.COSTA	HEUBERT RIBEIRO DA COSTA
250	t	96793945149	DIVINO.SANTOS	DIVINO BATISTA DOS SANTOS
251	t	66093864987	ROBERTO.BONFANTI	ROBERTO LUIZ BONFANTI
252	t	02052521178	DEIVISON.FELIX	DEIVISON BRUNO FELIX NUNES
253	t	01021902160	MARIA.CSILVEIRA	MARIA CONSUELO MIRANDA SILVEIRA
254	t	98117220130	FERNANDO.SIQUEIRA	FERNANDO ALVES SIQUEIRA
255	t	00984453164	DIEGO.TPEREIRA	DIEGO TELES PEREIRA
256	t	01597494178	CAIO.GMANDARINO	CAIO GOMES DE SA MANDARINO
257	t	70994129149	FABIOLA.FNASCIMENTO	FABIOLA FELIPE NASCIMENTO
258	t	73311758234	ANDERSON.RAPHAEL	ANDERSON RAPHAEL GUIMARAES LEANDRO
259	t	04882760126	PABLO.HMORAIS	PABLO HENRIQUE LOPES DE MORAIS
260	t	12261838751	mariana.coliveira	MARIANA CHRISTINE GOMES DE OLIVEIRA
261	t	74678051600	GERALDO.MOLIVEIRA	GERALDO MAGELA DE OLIVEIRA
262	t	03215272164	VALTEMI.JUNIOR	VALTEMI ANTONIO DE SOUSA JUNIOR
263	t	99727129153	ALINE.SANTOS	ALINE NUBIA OLIVEIRA SANTOS
264	t	01082544027	rodrigo.schumann	rodrigo.schumann
265	t	88720810768	ARI.OLIVEIRA	ARI GOMES DE OLIVEIRA
266	t	77584309100	ERIVILSON.SILVA	EREVILSON RIBEIRO DA SILVA
267	t	01813550190	DANIELE.SILVA	DANIELE ARAUJO DA SILVA
268	t	04667202114	FERNANDA.BARBOSA	FERNANDA CAROLINE SANTOS BARBOSA
269	t	00719539226	GUSTAVO.DIEMINA	GUSTAVO DIEMINE DA SILVA SOUSA
270	t	03970284155	LUCAS.AFONSO	LUCAS AFONSO DA SILVA
271	t	00907271189	HEDNARA.CUNHA	HEDNARA MENDES DE OLIVEIRA CUNHA
272	t	88869318672	FLAIRES.NASCIMENTO	FLAIRES WENDER NASCIMENTO
273	t	05608584694	GRAZIELA.CAIXETA	GRAZIELA DE CAIXETA E SILVA
274	t	82155658087	RAFAEL.WOLIVEIRA	RAFAEL WILHELM OLIVEIRA
275	t	03497520101	JOAO.CFALCAO	JOAO CARLOS DE SOUZA FALCAO
276	t	73133930249	FRANK.ACOSTA	FRANK ANDERSON AGUIAR DA COSTA
277	t	01765161150	ERCILEIA.OLIVEIRA	ERCILEIA DE OLIVEIRA
278	t	05733131993	NATACHA.ZENNI	NATACHA ZENNI
279	t	22717775854	FELIPE.CARVALHO	FELIPE CARVALHO FONGARO NARS
280	t	01183282125	MARIA.PEREIRA	MARIA ROBECICLEIA PEREIRA NETO
281	t	76082016149	CRIZOELIO.SANTOS	CRIZOELIO FREITAS DOS SANTOS
282	t	04219413162	MATHEUS.CUNHA	MATHEUS RIBEIRO DA CUNHA
283	t	43968171187	ALBERTO.NTELES	ALBERTO DO NASCIMENTO TELES
284	t	01408746158	RAFAEL.RODRIGUES	RAFAEL RODRIGUES DE SAO JOAO
285	t	84654937153	EMILIANE.NMARTINS	EMILIANE NATALIE SANTOS
286	t	01910535036	gustavo.schumann	gustavo.schumann
287	t	03890971105	MARCELO.KRAIESKI	MARCELO RODRIGO KRAIESKI
288	t	70048760129	LUCAS.COLIVEIRA	LUCAS COSTA VIEIRA DE OLIVEIRA
289	t	02725097100	JEFERSSON.SANTOS	JEFERSSON MARTINS SANTOS
290	t	32086881134	VALDECI.PEREIRA	VALDECI DIAS PEREIRA
291	t	02579904107	JANDER.SOUSA	JANDER RONIERE ALVES DE SOUSA
292	t	01188324179	LETICYA.GOMES	LETICYA GOMES
293	t	03921176140	JUDITH.ROMANIELO	JUDITH COSTA ROMANIELO
294	t	03580747118	NARA.RSILVA	NARA RODRIGUES DA SILVA
295	t	02772039102	EDSON.RPASSOS	EDSON ROCHA PASSOS
296	t	36309338153	ADMILDO.FNASCIMENTO	ADMILDO FRANCISCO DO NASCIMENTO
297	t	65426738149	CLAUDEMIR.GSABOIA	CLAUDEMIR GONCALVES SABOIA
298	t	33793917860	JOB.FBORBA	JOB FELICIO BORBA
299	t	36966246832	LAURO.JUNIOR	LAURO MARCOS DOS SANTOS JUNIOR
300	t	02618618165	PAULO.MACEDO	PAULO LEONAY CANDIDO MACEDO
301	t	03587421102	FILLIPE.SDALLORA	FILLIPE SOARES DAL LORA
302	t	70160011132	matheus.jesus	matheus.jesus
303	t	69526010310	WALDEMYRA.DJESUS	WALDEMYRA SOCORRO ROSA DE JESUS
304	t	03784747159	BARBARA.CATOZO	BARBARA CINTRA CATOZO
305	t	02300996103	PEDRO.LIMA	PEDRO HENRIQUE GONCALVES DE LIMA
306	t	00006155600	ALEXANDER.DIAS	ALEXANDER FERREIRA DIAS
307	t	64612902149	REGINALDO.LOPES	REGINALDO FERNANDES LOPES
308	t	50711750106	MARCIA.CANDIDA	MARCIA CANDIDA DA SILVA
309	t	11098249631	JOSE.LAZARO	JOSE LAZARO DA SILVA NETO
310	t	03768530183	JOAO.BRESSANELLI	JOAO AUGUSTO BRESSANELI
311	t	00101936184	EVERSSON.PESUMA	EVERSSON CAVALHEIRO PESSUNA
312	t	03506705369	JOANA.SANTOS	JOANA ANDRESSA LEITE SANTOS
313	t	60496311310	CHISTYAN.GSILVA	CHISTYAN GAMA DA SILVA
314	t	98154753120	ADRIANA.CARREIRA	ADRIANA BRAIDO CARREIRA
315	t	31539064832	MENFIS.SILVA	MENFIS.SILVA
316	t	77994442153	ELISANGELA.ABREU	ELISANGELA DA SILVA ABREU FERNANDES
317	t	13689877881	SILVIO.JFARIAS	SILVIO JORDAO DE FARIAS
318	t	73377511253	CHARLES.SANTOS	CHARLES ALVES DOS SANTOS
319	t	03640267150	FERNANDA.CMEDEIROS	FERNANDA CRISTINA DE MEDEIROS
320	t	05526481166	ADIEL.SILVA	ADIEL ABNER BATISTA DA SILVA
321	t	57581240100	SIMONE.IDINIZ	SIMONE ILIDIA DINIZ
322	t	00957837178	RAFAEL.SSALVADOR	RAFAEL DOS SANTOS SALVADOR
323	t	01977424163	ITALO.ASANTOS	ITALO ASSUNCAO DOS SANTOS
324	t	02882681135	LUIZ.GRABELO	LUIZ GOMES RABELO
325	t	02336784173	EVALDO.FSILVA	EVALDO FRANCISCO DA SILVA
326	t	01902087160	PAULO.MSILVA	PAULO MARCIO BATISTA DA SILVA
327	t	84016825172	BEIJAMIM.SOUSA	BEIJAMIM PINHEIRO DE SOUSA
328	t	24262102149	EDMAR.ESOBRAL	EDMAR SOBRAL
329	t	04434332163	LAURA.OLIVEIRA	LAURA VANESSA DE MORAES OLIVEIRA
330	t	44923228172	IVONETE.BONTEMPO	IVONETE MARIA BONTEMPO
331	t	75429675172	JOELAYNE.FOLIVEIRA	JOELAYNE FERREIRA DE OLIVEIRA
332	t	03362164161	CARLA.SBENES	CARLA BEATRIZ SILVA BENES
333	t	04188486104	NATHALIA.RANDRADE	NATHALIA ROSA DE ANDRADE
334	t	68706715320	NERENILSON.COELHO	NERENILSON ARAUJO COELHO
335	t	57788219134	RICARDO.CRAMOS	RICARDO CAMPOS RAMOS
336	t	57788448168	LEANDRO.JOSE	LEANDRO JOSE GUIMARAES
337	t	81037066120	HELIO.LJUNIOR	HELIO ROSA LEMOS JUNIOR
338	t	03249311154	JESSYCA.SOUZA	JESSYCA MAYANARA DE SOUZA GUIMARAES
339	t	05820787331	ANTONIO.CARVALHO	ANTONIO ATILA MARQUES DE CARVALHO
340	t	04637817404	GUILHERME.AFALCAO	GUILHERME DE ANDRADE FALCAO
341	t	35149035149	MEIRE.GPEREIRA	MEIRE GUEDES PEREIRA
342	t	62243764172	JOSE.CCARVALHO	JOSE CARLOS MONTEZUMA DE CARVALHO
343	t	19749600304	VERA.GMENDES	VERA LUCIA GONCALVES MENDES
344	t	02302539117	GABRIEL.DUARTE	GABRIEL DUARTE PIMENTEL
345	t	04080942141	WELLITON.CARVALHO	WELLITON TEODORO DE CARVALHO
346	t	02341868100	ROSANA.PREGO	ROSANA CHRYSTINA PREGO
347	t	00913334901	DAIANA.GONCALVES	DAIANA BIANCHIN BECKER GONCALVES
348	t	03682454110	DANILO.FRIBEIRO	DANILO FERNANDES RIBEIRO
349	t	95195467153	EDIVAN.SANTOS	EDIVAN SOUZA SANTOS
350	t	04236674165	MICAELLEN.SILVA	MICAELLEN BARBOSA SILVA
351	t	00528308106	CICERO.SILVA	CICERO MENDES DA SILVA
352	t	04067781164	PAULO.RODRIGUES	PAULO VICTOR DE MATOS RODRIGUES
353	t	70196553199	MATHEUS.OLIVEIRA	MATHEUS CANDIDO DE OLIVEIRA
354	t	00244078106	GIZELY.COUTRIM	GIZELY TEXEIRA COUTRIM
355	t	83541667168	EDWARDS.SILVA	EDWARDS VIEIRA ANDRADE DA SILVA
356	t	93286694134	MAURO.FREITAS	MAURO ALVES DE FREITAS
357	t	92223567134	JOSE.JUNIOR	JOSE RUBENS DE SOUZA JUNIOR
358	t	01448185157	JULIANA.OLIVEIRA	JULIANA LEITE DE OLIVEIRA
359	t	63910365272	CLAYTON.ALMEIDA	CLAYTON ALCANTARA DE ALMEIDA
360	t	98611224191	ROSEMARY.FILGUEIRA	ROSEMARY FILGUEIRA
361	t	03420156308	LILIA.OLIVEIRA	LILIA FERNANDA SILVA DE OLIVEIRA
362	t	00340115360	EDEM.PEREIRA	EDEM ALVES PEREIRA
363	t	04125734313	FELIPE.CARDOSO	FELIPE ELIAKIM BARBOSA ALVES CARDOSO
364	t	50781499372	GENIVALDO.SENA	GENIVALDO DE JESUS SENA
365	t	04134227399	PAULO.SANTOS	PAULO ROBERTO SILVA SANTOS
366	t	73081469368	ROSIDALVA.SILVA	ROSIDALVA PEREIRA SILVA
367	t	88156028368	IVONE.SIPAUBA	IVONE FERNANDES SIPAUBA
368	t	00161812392	JACIARA.NASCIMENTO	JACIARA ARAUJO FERREIRA DO NASCIMENTO
369	t	64328074334	JOAO.COSTA	JOAO BATISTA LINDOSO COSTA
370	t	84255404372	JOSE.GARCIA	JOSE ADALBERTO GARCIA JUNIOR
371	t	01503954340	WALLYSON.SARAIVA	WALLYSON BRUNO LIMA FARIAS SARAIVA
372	t	02901157106	LEIDIANE.SOUZA	LEIDIANE ALVES DE SOUZA
373	t	01816957151	ROSANA.LIMA	ROSANA DA SILVA LIMA
374	t	34731490120	UILSON.SENA	UILSON CARLOS BATISTA SENA
375	t	73706337134	EUDES.BENTO	EUDES LEITE BENTO
376	t	02517359156	LARISSA.BUENO	LARISSA MIGUEL BUENO
377	t	10169959627	SARON.MEDEIROS	SARON MEDEIROS
378	t	01888769106	REGINA.LMENDES	REGINA LEITE FERREIRA MENDES
379	t	87453711134	JOSE.EVANDRO	JOSE EVANDRO SOUSA DA SILVA
380	t	00550410198	REGINALDO.LIMA	REGINALDO CANDIDO DE LIMA
381	t	05933963160	EMILIA.SOUZA	EMILIA VIVIANE BAEZ DE SOUZA
382	t	02483423396	CECILIA.FERREIRA	CECILIA ROBERTA NASCIMENTO FERREIRA
383	t	47533390334	JOSE.SANTOS	JOSE CARLOS OLIVEIRA DOS SANTOS
384	t	01432401106	HELIANE.XAVIER	HELIANE KAMILLA XAVIER DOS SANTOS
385	t	04162826102	JONAS.OLIVEIRA	JONAS JUNIO MORAIS OLIVEIRA
386	t	00972759131	THAIS.CAETANO	THAIS APARECIDA CAETANO
387	t	59127643115	EDVALDO.ASSIS	EDVALDO ALVES DE ASSIS
388	t	19199147149	WILMAR.SILVA	WILMAR.SILVA
389	t	04368767101	MARCELO.SOUZA	MARCELO BARBOZA DE SOUZA
390	t	03667272146	FERNANDA.CHAGAS	FERNANDA VITURINO DAS CHAGAS
391	t	88251470153	ALAN.AMORIM	ALAN DE OLIVEIRA AMORIM
392	t	01339757117	JOAO.MIRANDA	JOAO VICTOR FERREIRA MIRANDA DE CARVALHO
393	t	02005495107	MANUELA.SILVA	MANUELA LEITE DA SILVA
394	t	02316044166	RICARDO.CONDE	RICARDO TERRA ANDRADE PORTELLA CONDE
395	t	02057933121	WALDIR.JUNIOR	WALDIR DE BARROS JUNIOR
396	t	04340332194	ARIANY.OLIVEIRA	ARIANY KRYSTINA DE OLIVEIRA
397	t	01619696150	ERICA.LIMA	ERICA SUSE LEMOS DE LIMA
398	t	01129580105	JAKELINNE.SOARES	JAKELINNE SILVA SOARES
399	t	03324978106	JESSICA.CABRAL	JESSICA DE OLIVEIRA CABRAL
400	t	98397958215	PRISCILA.OLIVEIRA	PRISCILA MARIA SOUSA DE OLIVEIRA GUERRA
401	t	02730240195	VIRGINIA.TERRA	VIRGINIA BARBOSA TERRA
402	t	75603411104	JESSYCA.MLIMA	JESSYCA VENCIO MACHADO DE LIMA
403	t	03940364177	THAIS.SILVA	THAIS HELENA SILVA
404	t	02308799188	EDUARDO.ROCHA	EDUARDO BARROSO CLEMENTE ROCHA
405	t	78822688104	FERNANDO.FERNANDES	FERNANDO SILVA FERNANDES
406	t	04443148140	LORENA.MEDEIROS	LORENA CRISTINA SAUADA MEDEIROS
407	t	01444171119	MARCOS.ANTONIO	MARCOS ANTONIO ALVES DA SILVA
408	t	00058921125	CARLOS.ROCHA	CARLOS EDUARDO DE SOUSA ROCHA
409	t	02338958179	DANIEL.MELO	DANIEL VIEIRA DE MELO
410	t	35223745841	ELAINE.PINHEIRO	ELAINE FELIZARDO PINHEIRO
411	t	81665555149	ELEN.COSTA	ELEN LUZ COSTA
412	t	01292483121	FABRICIO.PEREIRA	FABRICIO.PEREIRA
413	t	02757321188	FERNANDA.JESUS	FERNANDA SOUZA DE JESUS
414	t	03798773173	GIRLI.SILVA	GIRLI DINARA BARROS SILVA
415	t	01341781143	RITA.COSTA	RITA MARIELLY DA CRUZ COSTA
416	t	95467980106	WESLEY.VIEIRA	WESLEY VIEIRA DA SILVA
417	t	70097759155	JOYCE.SAYONARA	JOYCE.SAYONARA
418	t	01698252137	ELAINE.MOREIRA	ELAINE.MOREIRA
419	t	57978336115	FRANCISCO.ORLEUDO	FRANCISCO ORLEUDO SILVA NASCIMENTO
420	t	60011360500	VALDISON.SANTOS	VALDISON SILVA SANTOS
421	t	75271257134	JEFFERSON.FERREIRA	JEFFERSON ALMEIDA FERREIRA
422	t	00161559123	DIANY.FERNANDES	DIANY FERNANDES PEREIRA
423	t	06739753636	ANA.BORGES	ANA LUIZA VIEIRA BORGES
424	t	97892149104	BRUNO.CARDOSO	BRUNO CORDEIRO CARDOSO
425	t	02163192180	ROBERTA.INACIO	ROBERTA LEAO INACIO
426	t	00425485196	THIAGO.GUIMARAES	THIAGO ANTONIO GUIMARAES
427	t	03777233129	VICTOR.REZENDE	VICTOR HUGO REZENDE DE OLIVEIRA
428	t	01774955105	PATRICIA.REGINA	PATRICIA REGINA ARAUJO
429	t	03179033142	LARISSA.GOIS	LARISSA GOIS ANTUNES DE MELO
430	t	89249313187	FRANCISBETH.PAULA	FRANCISBETH DE PAULA DA SILVA
431	t	89170172153	MARCELO.MACIEL	MARCELO DA SILVA MACIEL
432	t	03434212167	THAIS.CARMO	THAIS PAULA BATISTA DO CARMO
433	t	00315933135	WEDER.CARDOSO	WEDER DOMINGUES CARDOSO
434	t	02128371171	CHARLES.COSTA	CHARLES FRANCISCO COSTA
435	t	59796472104	AUGUSTO.TOMAZ	AUGUSTO CESAR ARAUJO TOMAZ
436	t	03356551140	ALEFE.CRISTIAN	ALEFE CRISTIAN PEREIRA MENDES
437	t	00716689189	JOAO.BRAZ	JOAO PAULO BRAZ
438	t	01990474136	HAYKA.LIMA	HAYKA GOMIDES LIMA
439	t	02972073185	BRUNA.LIRA	BRUNA BORGES LIRA
440	t	00608685119	MARX.SILVA	MARX WEBER CORDEIRO SILVA
441	t	95047727149	RICHARD.SILVA	RICHARD ALMEIDA SILVA
442	t	01442134607	GERALDO.CAETANO	GERALDO CAETANO OLIVEIRA FERREIRA
443	t	76327370144	ELIANE.EDMUNDO	ELIANE EDMUNDO DE OLIVEIRA
444	t	02015888110	KEDNA.URCINO	KEDNA URCINO SOUSA
445	t	50875183115	WELLINGTON.ALMEIDA	WELLINGTON DE JESUS ALMEIDA
446	t	06692608600	GRACIELA.GONCALVES	GRACIELA GARCIA GONCALVES
447	t	07873296652	CARLA.VELOSO	CARLA CRISTINA DE MELO VELOSO
448	t	01647778654	HUGO.SILVA	HUGO MICHEL PEREIRA SILVA
449	t	03281525105	ANA.SILVA	ANA LUIZA VITAL FERREIRA SILVA
450	t	06512930657	ADEMIR.ANJOS	ADEMIR SOUSA DOS ANJOS
451	t	10912398191	ADALBERTO.SOUZA	ADALBERTO JORGE DE SOUZA
452	t	01140145150	JORGE.TEIXEIRA	JORGE PAULO TEIXEIRA
453	t	02382299126	BRUNO.SILVA	BRUNO SILVA
454	t	02341582125	CRISTIANO.FERREIRA	CRISTIANO DA SILVA FERREIRA
455	t	02217335154	ALEX.LAMEIRO	ALEX RICARDO VIANA LAMEIRO
456	t	01025672127	DAVID.MESQUITA	DAVID DE ABREU SALLES MESQUITA
457	t	02749462118	ANDRE.FARIA	ANDRE.FARIA
458	t	00872389189	TATIANA.PIZETA	TATIANA SARDI PIZETA
459	t	01716849152	PAULA.SILVA	ANA PAULA DA SILVA
460	t	02115017196	MARCOS.NETO	MARCOS VINICIUS FONSECA NETO
461	t	74859803272	DOUGLAS.ANDRETO	DOUGLAS EDUARDO ANDRETO
462	t	42826624172	NEVANDA.SILVA	NEVANDA DANTAS DA SILVA
463	t	76482030149	EVANDRO.GNEVES	EVANDRO GOMES DAS NEVES
464	t	03120522198	MARCELA.LAGO	MARCELA PEREIRA DO LAGO
465	t	02926472129	CARMEM.ROCHA	CARMEN ROCHA CARVALHO
466	t	03775099140	RENATA.ALMEIDA	RENATA DE ALMEIDA BOAVENTURA
467	t	02924379199	NOADIA.CARMO	NOADIA DO CARMO SILVA
468	t	62828428168	CELIA.CARVALHO	CELIA PEREIRA DA SILVA CARVALHO
469	t	01505626161	FABIO.SANTOS	FABIO DE SOUZA SANTOS
470	t	02702438105	TATIANE.SILVA	TATIANE DA SILVA
471	t	73059366187	VINICIUS.GOULART	VINICIUS SILVA GOULART
472	t	87356716100	EDSON.CARVALHO	EDSON BATISTA DE CARVALHO
473	t	95120319149	ANA.PAULINO	ANA ESTELITA PAULINO
474	t	06381191627	HEITOR.REZENDE	HEITOR DE SOUSA REZENDE
475	t	37541072168	JOAO.MESSIAS	JOAO MESSIAS RODRIGUES
476	t	69701075153	MARCIA.LOBO	MARCIA ROSA LOBO
477	t	02781392138	CAMILA.CAZE	CAMILA CAZE DE OLIVEIRA
478	t	03297405120	JOYCE.CHELES	JOYCE RODRIGUES CHELES
479	t	04423046687	JOSE.AMERICO	JOSE AMERICO DE GOUVEA
480	t	87875250382	CARLOS.PEREIRA	CARLOS EDUARDO PEREIRA DA SILVA
481	t	03351732171	ADRIANA.ALVES	ADRIANA MARIA RIBEIRO ALVES
482	t	08060448617	JONATHAN.JOSE	JONATHAN JOSE OLIVEIRA SILVA
483	t	41246292149	PERSIO.ALVES	PERSIO ADRIANE ALVES DE OLIVEIRA
484	t	01774088126	BRUNA.SANTOS	BRUNA FERREIRA DOS SANTOS
485	t	04107777146	CASSIO.MENEZES	CASSIO CESAR SANTOS DE MENEZES
486	t	04445555606	DENISMAR.CORTES	DENISMAR SILVERIO CORTES
487	t	00506430103	FABRICIO.LACERDA	FABRICIO CESAR LIMA DE LACERDA
488	t	07083738602	FABRICIO.CURY	FABRICIO DELLA COLETTA PEIXOTO CURY
489	t	02504290128	THIAGO.NUNES	THIAGO HENRIQUE CAZELLI NUNES
490	t	95976418104	LETICIA.VIANA	LETICIA CRISTINA DE OLIVEIRA VIANA
491	t	97028860310	FABIO.COSTA	FABIO DE JESUS SANTOS DA COSTA
492	t	66108896191	RONAN.SILVA	RONAN ELIAS DA SILVA
493	t	03011778990	ELIANE.SOUZA	ELIANE MARCIA DE SOUZA
494	t	01804861103	ANDRESSA.TOLEDO	ANDRESSA SILVEIRA TOLEDO
495	t	88322122187	FLAVIO.SILVA	FLAVIO MARTINS DA SILVA
496	t	86692020182	ANDRE.VINICIUS	ANDRE VINICIUS SILVA DOS SANTOS
497	t	04137667136	DAYANE.RODRIGUES	DAYANE SOUZA RODRIGUES
498	t	75265222120	ERICA.LINO	ERICA LINO GIFFONI SALES
499	t	63529696234	MARCIA.AFONSO	MARCIA ANDREA BARBOSA AFONSO
500	t	01706856245	livia.cshreder	LIVIA CAROLINE SHREDER PIO
501	t	60751428191	AGENOR.BOTELHO	AGENOR ALEXANDRE BOTELHO
502	t	01173926186	MONICA.ALVES	MONICA ALVES DA SILVA
503	t	05407754100	JAKELINE.SILVA	JAKELINE GONCALVES DA SILVA
504	t	92364705134	SILVIA.CARDOSO	SILVIA.CARDOSO
505	t	70526907134	GABRIEL.BARROCA	GABRIEL MACHADO WERNECK BARROCA
506	t	01997064170	RENATO.LIMA	RENATO ALVES DE LIMA
507	t	07691007671	JULIANA.SANTOS	JULIANA RODRIGUES SANTOS
508	t	73717649191	RODRIGO.DUARTE	RODRIGO SOUZA DUARTE
509	t	23648481304	CLAUDIANO.COSTA	CLAUDIANO.COSTA
510	t	84446420100	SIDINEI.BUZATTO	SIDINEI FERREIRA BUZATTO
511	t	94532249287	ISACK.SANTOS	ISACK THIAGO ALVES SANTOS
512	t	00735203148	LOYANNE.DINIZ	LOYANNE SILVEIRA DINIZ
513	t	21670560104	AGOSTINHO.FILHO	AGOSTINHO DA MATA FILHO
514	t	02595070320	CIANA.BRAGA	CIANA MARIA DOS SANTOS BRAGA
515	t	95548319191	CRISTIANO.BRITO	CRISTIANO GOMES DE BRITO
516	t	30187435120	JACKSON.SAMPAIO	JACKSON JOSE SAMPAIO
517	t	28067304572	HELVECIO.FIDELIS	HELVECIO HELDER FIDELIS
518	t	05166931319	TAYANE.GOIS	TAYANE DE FATIMA SANTOS GOIS
519	t	01906032165	ANA.CARMO	ANA CAROLINA DOS SANTOS CARMO
520	t	97691674287	FELIPE.CAVALCANTE	FELIPE OCIAN CAVALCANTE LUNA
521	t	78313007249	ANGELA.MELO	ANGELA VERISSIMO DE MELO
522	t	97766178168	GESSE.JORGE	GESSE JORGE TEIXEIRA
523	t	01375804103	VANESSA.BRITES	VANESSA FILARTIGA BRITES
524	t	43050166134	MAURO.LIMA	MAURO EVANGELISTA DE LIMA
525	t	19147988134	DENILDA.MARIA	DENILDA MARIA COSTA PELEGRINO
526	t	01714513173	JOAOPAULO.TAVARES	JOAO PAULO DA SILVA TAVARES
527	t	02880095174	MAYARA.MOREIRA	MAYARA.MOREIRA
528	t	02450047182	PEDRO.JUNIOR	PEDRO ANTONIO DE MATOS JUNIOR
529	t	73567710125	JEFFERSON.LEAL	JEFFERSON LEANDRO VIEIRA LEAL
530	t	02932576127	ANA.SANTOS	ANA CAROLINE TOLENTINO SANTOS
531	t	93198086604	FABIANA.SANTOS	FABIANA APARECIDA DOS SANTOS
532	t	01466765640	ULISSES.OLIVEIRA	ULISSES DANIEL RIBEIRO DE OLIVEIRA
533	t	08772608692	PAULO.GOBBI	PAULO HENRIQUE GOBBI SILVA
534	t	32100884620	NAIR.SOUZA	NAIR QUEIROZ DE SOUZA
535	t	65210271668	WESLEY.GUIMARAES	WESLEY FARIA GUIMARAES
536	t	06448897640	THIAGO.SANTOS	THIAGO GUIMARAES SANTOS
537	t	02824260602	ADRIANO.MACEDO	ADRIANO ALVES DE MACEDO
538	t	11231795654	GUILHERME.MARTINS	GUILHERME MARTINS
539	t	05276697841	FRANCISCO.SILVEIRA	FRANCISCO CLAUDIO DA SILVEIRA
540	t	07972362602	ALLINE.SOUZA	ALLINE FONSECA DE SOUZA RIGONATO
541	t	12012640656	ANDRE.FELIPE	ANDRE JONAS DA SILVA FELIPE
542	t	09140253686	MICHELLE.SILVA	MICHELLE ALVES DA SILVA
543	t	06139347688	ANTONIO.JUNIOR	ANTONIO JOSE DOS SANTOS JUNIOR
544	t	07419816611	BRUNO.ARAUJO	BRUNO ALMEIDA ARAUJO
545	t	81742800149	ITHALA.MACHADO	ITHALA BIANCA RIBEIRO MACHADO
546	t	07352767657	AMANDA.FERREIRA	AMANDA MENDES DA MATA FERREIRA
547	t	00540707163	ELAINE.MARTINS	ELAINE KELE MARQUES MARTINS
548	t	03379667102	MARIANA.IGNATS	MARIANA IGNATS RODRIGUES
549	t	80213413191	ALBERTO.FILHO	ALBERTO LEITE RIBEIRO FILHO
550	t	83941797115	fabio.maia	FABIO PEREIRA MAIA
551	t	70029609119	LARA.BEATRIZ	LARA BEATRIZ FERREIRA RODRIGUES
552	t	03218963192	TATIANE.OLIVEIRA	TATIANE KEDIMA SILVA DE OLIVEIRA
553	t	23602899187	JOVEMAR.LOPES	JOVEMAR FERREIRA LOPES
554	t	03668806160	BARBARA.UCHOA	BARBARA LEANDRO RODRIGUES UCHOA
555	t	02487501197	JENIFFER.CACHOEIRA	JENIFFER XAVIER CACHOEIRA
556	t	97624101572	JOSICLEIDE.SILVA	JOSICLEIDE MARIA DA SILVA
557	t	73824771187	PEDRO.SILVA	PEDRO HENRIQUE AMORIM SILVA
558	t	02433113113	JUSCELE.ARRUDA	JUSCELE ALMEIDA DE ARRUDA
559	t	02839275180	FERNANDA.STEFANNY	FERNANDA STEFFANY DE JESUS FERNANDES
560	t	92306462100	GILVAN.FERREIRA	GILVAN DE FREITAS FERREIRA
561	t	02264940123	BRUNO.CEZAR	BRUNO CEZAR DA SILVA
562	t	00550301143	HELDDER.MORAIS	HELDDER JACKSON ROCHA FERREIRA DE MORAIS
563	t	65830539187	JOAO.VENANCIO	JOAO CARLOS VENANCIO
564	t	00821620185	juvencio.oliveira	JUVENCIO ZACARIAS ALVES DE OLIVEIRA
565	t	01184884145	WESLEY.EVANGELISTA	WESLEY EVANGELISTA DA SILVA
566	t	02462350128	ANDREIA.VENANCIO	ANDREIA VENANCIO RIBEIRO
567	t	04911537167	VINICIUS.MORAIS	VINICIUS DE SIQUEIRA  MORAIS
568	t	01371888116	CELSO.NASCIMENTO	CELSO ALVES DO NASCIMENTO JUNIOR
569	t	00043292100	DELVANO.SANTOS	DELVANIO TRAJANO DOS SANTOS
570	t	04487596165	ERICA.ALMEIDA	ERICA SILVA DE ALMEIDA
571	t	76958825187	FABIANA.RAMOS	FABIANA RAMOS DA COSTA
572	t	01813368120	CARLOS.BRUNO	CARLOS BRUNO PEREIRA DA SILVA
573	t	02513128109	JOSE.CARVALHO	JOSE CARLOS SALES CARVALHO JUNIOR
574	t	01090197179	NAIN.GOMES	NAIN GOMES DA CRUZ
575	t	00545306108	EVERTON.ALMEIDA	EVERTON DOS SANTOS ALMEIDA
576	t	76052338334	MARIA.ARAUJO	MARIA DA GRACA DE SOUSA ARAUJO
577	t	97408026134	RODRIGO.SILVA	RODRIGO FAGUNDES DA SILVA
578	t	72133430172	MARCELA.SOUZA	MARCELA APARECIDA TENORIO DE SOUZA
579	t	00018438180	MAGNO.RODRIGUES	MAGNO LEITE RODRIGUES
580	t	80365205168	JOILSON.SOUZA	JOILSON PINHO DE SOUZA
581	t	58422471191	SAMANTHA.CARDOSO	SAMANTHA CARDOSO ALBINO
582	t	01025253159	WELLINGTON.SILVA	WELLINGTON DA SILVA CAETANO
583	t	72480793168	EDSON.EIKI	EDSON EIKI BARBOSA UMETA
584	t	95553720125	EDERSON.SALES	EDERSON SOUSA SALES
585	t	35403297812	CATARINA.GUEDES	CATARINA GUEDES FERNANDES
586	t	98622021149	WEBERTH.SOUZA	WEBERTH LACERDA DE SOUZA
587	t	01104215152	HANNA.CARVALHO	HANNA JESSICA CARVALHO MALICIO
588	t	04719995128	DIEGO.MIRANDA	DIEGO LUAN MIRANDA DA SILVA
589	t	02168523150	FELIPE.MACEDO	FELIPE GOMES DE MACEDO
590	t	89190190106	GILVANIA.ALMEIDA	GILVANIA QUEIROZ DE ALMEIDA
591	t	04688828128	JOSIVALDO.BORGES	JOSIVALDO.BORGES
592	t	81414498187	BRUNO.QUEIROZ	BRUNO.QUEIROZ
593	t	00053608542	TIAGO.ALVES	TIAGO.ALVES
594	t	03122254158	ADRIANE.CORREA	ADRIANE DA GUIA CORREA
595	t	72564938191	RAFAEL.ANDRADE	RAFAEL.ANDRADE
596	t	01486452248	TIAGO.MAURICIO	TIAGO MAURICIO MIRANDA DOS SANTOS
597	t	02790798346	JENNYFER.FURLAN	JENNYFER BRENHA FURLAN
598	t	03589363304	WAN.AMARAL	WALKEMBURG WAN JOY AMARAL CAVALCANTE
599	t	03691000185	MATHEUS.LUCENA	MATHEUS LUCENA DE OLIVEIRA
600	t	61308943172	FREDERICO.JOSE	FREDERICO JOSE GOMES DE QUEIROZ
601	t	04984675110	WARLEY.ORNELAS	WARLEY CARNEIRO ORNELAS
602	t	01860517196	EMERSON.SANTOS	EMERSON LEANDRO ARAUJO DOS SANTOS
603	t	01996483102	RAUL.SILVA	RAUL ARAUJO DA SILVA
604	t	02459245157	ALISSON.PEREIRA	ALISSON PEREIRA DO ROZARIO
605	t	90292294115	RONALDO.GOMES	RONALDO DE LACERDA GOMES
606	t	00933852185	WESLEY.ALMEIDA	WESLEY RIBEIRO DE ALMEIDA
607	t	91384249168	EDMAR.VALDO	EDMAR BARBOSA VALDO
608	t	00191962104	ANDRE.SANTO	ANDRE LUIZ ESPIRITO SANTO SILVA
609	t	00432034188	KARINE.SILVA	KARINE JESUS DA SILVA
610	t	85312045187	FABIANO.SANTOS	FABIANO SANTOS DE CARVALHO
611	t	37326503168	JEAN.CASTRO	JEAN CARLOS DE CASTRO MAIA
612	t	88337723149	MARCONI.ALVES	MARCONI ALVES DO NASCIMENTO
613	t	61065374100	JACI.MOREIRA	JACI DA SILVA MOREIRA MASCARENHAS
614	t	71973958104	JAIR.CORNELIO	JAIR PEREIRA CORNELIO
615	t	69770476153	THIAGO.ARRUDA	THIAGO.ARRUDA
616	t	04062772140	KARITA.THUANNE	KARITA THUANNE DE MELO PEREIRA
617	t	70513996168	ELI.GRACIANO	ELI BARBOSA GRACIANO
618	t	00287320146	MARIA.RAMOS	MARIA.RAMOS
619	t	01775805182	NUBIA.SANTOS	NUBIA SANTOS EVANGELISTA
620	t	05175792199	CAMILA.BAIMA	CAMILA BAIMA DO NASCIMENTO
621	t	82750939100	ALEX.LUCIANO	ALEX LUCIANO DE SOUSA
622	t	83240624168	FABRICIO.GONTIJO	FABRICIO FERNANDO DA SILVA GONTIJO
623	t	05199419188	GUILHERME.LARES	GUILHERME MORAIS LARES
624	t	81296606104	CHRISTIAN.SANTOS	CHRISTIAN LEAL DOS SANTOS
625	t	27621693134	ELVIRA.PAULA	ELVIRA FERREIRA DE PAULO
626	t	02702632173	JULIO.CESAR	JULIO CESAR FERREIRA DIAS
627	t	00271820101	FELIPE.COUTO	FELIPE FARIA DO COUTO
628	t	94982163391	LUCIO.MAURO	LUCIO MAURO SANTOS PRASERES
629	t	89567021104	HELVER.RIBEIRO	HELVER RIBEIRO SANTOS DOURADO SAMPAIO
630	t	72878169115	ELISSANDRA.LAUREANO	ELISSANDRA ALMEIDA LAUREANO
631	t	26253666134	CELIA.LOPES	CELIA SILVA LOPES
632	t	50573276153	FRANCISCO.LUZ	FRANCISCO JOSE SARAIVA DA LUZ
633	t	84225777120	ALEXANDRE.TRINDADE	ALEXANDRE CARDOSO TRINDADE
634	t	02579166106	VICTOR.NEVES	VICTOR LEANDRO NEVES
635	t	02765396132	PAULO.HEBERTE	PAULO HEBERTE DE JESUS BRAGA
636	t	05820199405	PEDRO.ARAUJO	PEDRO RICARDO FERNANDES ARAUJO
637	t	95458573153	TIAGO.DELGADO	TIAGO ANTONIO DELGADO
638	t	02209115175	LEANDRO.LEMOS	LEANDRO SAMPAIO LEMOS
639	t	80574521100	JACKSON.PASSOS	JACKSON FERREIRA PASSOS
640	t	83264710110	RODRIGO.SOUTO	RODRIGO SOUTO PEREIRA
641	t	02795657554	CARLOS.SALES	CARLOS VINICIUS BARROS SALES
642	t	03865935150	ELIANE.CARVALHO	ELIANE NUNES DE CARVALHO
643	t	04720648193	RAIZA.SILVA	RAIZA KELY DE OLIVEIRA DA SILVA
644	t	03795211131	ANDRE.LIMA	ANDRE SIMOES SILVA DE LIMA
645	t	03652575679	ADRIANO.JOSE	ADRIANO JOSE DA SILVA
646	t	84104678104	ARTUR.FORTES	ARTUR.FORTES
647	t	92457967104	LEANDRO.MOREIRA	LEANDRO COUTINHO MOREIRA
648	t	04251420195	LUCAS.CRISPIM	LUCAS CRISPIM DA SILVA
649	t	62688294687	SERGIO.ALVES	SERGIO ALVES PEREIRA
650	t	66614988115	MARLEY.SOARES	MARLEY MARQUES SOARES
651	t	00611474190	CARLOS.JUNIOR	CARLOS ROBERTO DA SILVA JUNIOR
652	t	70113670125	ADRIANA.SILVA	ADRIANA MARCIA CABRAL DA SILVA
653	t	00271941162	FABIANA.ARAUJO	FABIANA BETANIA DE ARAUJO MEDEIROS
654	t	79237207115	KATIA.SOUZA	KATIA FRANCISCA DE SOUZA
655	t	03344256114	RICARDO.CARVALHO	RICARDO FEITOSA DE CARVALHO
656	t	01739966120	DIEGO.LOPES	DIEGO DE JESUS LOPES
657	t	75204258191	RAFAEL.FELIPE	RAFAEL BARBOSA REZENDE FELIPE
658	t	71600299172	FLAVIO.FILLES	FLAVIO.FILLES
659	t	81416253149	CELSO.RODRIGUES	CELSO DOS SANTOS RODRIGUES
660	t	09461620268	GOIANDIRA.RODRIGUES	GOIANDIRA LOPES CAVALCANTE RODRIGUES
661	t	02309733105	LUIZ.NOVAES	LUIZ FERNANDO NOVAES DOS SANTOS
662	t	56366990182	WASHINGTON.LUIZ	WASHINGTON.LUIZ
663	t	62001213115	ANTONIO.MARQUES	ANTONIO MARQUES DA SILVA FILHO
664	t	69921440144	CARLOS.MAGNO	CARLOS MAGNO MORAES MELO
665	t	51608260178	JEAN.MARY	JEAN MARY PALMEIRA
666	t	02023030390	NUBIA.MARTINS	NUBIA MARTINS SERRAO
667	t	01638547343	HANNAH.CALDAS	HANNAH CALDAS DA SILVA
668	t	05415774173	LLUCAS.GONCALVES	LUCAS ANTONIO GONCALVES
669	t	22438017104	PAULO.ROBERTO	PAULO ROBERTO DOS SANTOS
670	t	02015776150	TIAGO.FARIA	TIAGO RAMOS FARIAS
671	t	01134438206	RICARDO.LOPES	RICARDO UCHOA LOPES
672	t	02460709277	KAREN.ROCHA	KAREN CRISTINE MACIEL DA ROCHA
673	t	01954507283	HIORRANA.BARBOSA	HIORRANA CRISTINA MARINHO BARBOSA
674	t	00089490185	BERNARD.SILVA	BERNARD ALEXANDRE MOREIRA E SILVA
675	t	01141801132	BRUNO.CRUZ	BRUNO CESAR DE LIMA CRUZ
676	t	04086337193	BRUNO.MACEDO	BRUNO DE SOUZA MACEDO
677	t	92400817120	CLEBER.RODRIGUES	CLEBER RODRIGUES
678	t	00961602171	DAYANE.FLOR	DAYANE MEIRE REIS GONCALVES FLOR
679	t	22060723191	JACIONETE.MATOS	JACIONETE SILVA MATOS
680	t	03316970177	TIAGO.SOUZA	TIAGO DE SOUZA DOS SANTOS
681	t	03015710150	WALLACE.SILVA	WALLACE FERNANDES DA SILVA
682	t	01623984173	FLAVIO.LEANDRO	FLAVIO LEANDRO DA SILVA
683	t	01626555176	ANTONIO.RODRIGUES	ANTONIO GOMES RODRIGUES NETO
684	t	92985408172	RENATO.CARDOSO	RENATO MELO CARDOSO
685	t	64675718191	OSEIAS.BARROS	OSEIAS SANTANA BARROS
686	t	72396113100	CESAR.BORGES	CESAR AUGUSTO DA SILVEIRA BORGES
687	t	04724201193	VINICIUS.ALMEIDA	VINICIUS FREITAS DE ALMEIDA
688	t	60209143134	RENATO.MEDEIROS	RENATO RUBENS DE MEDEIROS
689	t	72378441134	TIAGO.BEZERRA	TIAGO BEZERRA DE SOUZA
690	t	48841242191	VALDECI.GONCALVES	VALDECI SANTOS DE ABREU GONCALVES
691	t	03679428103	EMERSON.JUNIOR	EMERSON SILVA BATISTA JUNIOR
692	t	06931954601	ANGELICA.SOARES	ANGELICA SOARES PEREIRA
693	t	72182717153	FERNANDO.BATISTA	FERNANDO BATISTA MACHADO
694	t	96496703191	DYEGO.RODRIGUES	DYEGO DJAVAN DE SOUSA RODRIGUES
695	t	05788542600	GISELE.OLIVEIRA	GISELE DE OLIVEIRA MORAIS
696	t	05210754162	BRUNO.BESSA	BRUNO ARAUJO BESSA DE MELO
697	t	03135492141	WERILLIS.CARDOSO	WERLLIS LUIZ DE SOUSA CARDOSO
698	t	73001163100	PAULO.CASTRO	PAULO HENRIQUE SOUZA DE CASTRO
699	t	01181276675	RENATA.GUISSONI	RENATA GUISSONI
700	t	01675190186	LINDEMBERG.JESUS	LINDEMBERG DE JESUS CRISTIANO
701	t	02336193132	LUIZ.ZACARIAS	LUIZ.ZACARIAS
702	t	03264569108	FERNANDA.OLIVEIRA	FERNANDA RITIELLI DE OLIVEIRA
703	t	03936291390	ALIETH.SANTOS	ALIETH DOS SANTOS PEREIRA
704	t	80046665234	GUSTAVO.LIBERATO	GUSTAVO LIBERATO LARA THIAGO
705	t	73480363172	SIMONE.PORTES	SIMONE ALVES DE BASTOS
706	t	69404127272	JONATAS.LAVAGNOLI	JONATAS FRANCISCO LAVAGNOLI
707	t	04447866196	EWELLEN.OLIVEIRA	EWELLEN GUSMAO FAVARETTO DE OLIVEIRA
708	t	94552940104	ALANA.SSILVA	ALANA SALOMAO BEZERRA SILVA
709	t	63549123191	MARCIO.ALVES	MARCIO ALVES DE SOUZA
710	t	04947074108	JULIA.ALMEIDA	JULIA BORGES BARBOSA DE ALMEIDA
711	t	86645056168	RODRIGO.CUSTODIO	RODRIGO CUSTODIO DA SILVA
712	t	00249898357	KEYLA.GALVAO	KEYLA CRISTINE QUINTINO GALVAO
713	t	01051481309	HELLEN.BARBOSA	HELLEN SILVANIA DE ABREU PINTO BARBOSA
714	t	01431118176	ROBERTO.SANDES	ROBERTO SANDES DA SILVA
715	t	27600718120	OSMAR.REIS	OSMAR JOSE DE CARVALHO REIS
716	t	00839713193	FABRICIO.MENDES	FABRICIO MACHADO MENDES
717	t	02750114101	JEFFERSON.RICARDO	JEFFERSON RICARDO SETUBAL VASCONCELOS
718	t	95084320120	ANTONIO.GREGORIO	ANTONIO GREGORIO DE SOUZA
719	t	02273661150	JACKSON.SOUSA	JACKSON CLAUDIO DE SOUZA
720	t	02860782109	NATHALYA.SILVA	NATHALYA YASMINE SILVA
721	t	84077271215	ANA.SOUSA	ANA CARLA PARAGUASSU FERREIRA DE SOUSA
722	t	00557912121	GRAZIELA.TAVARES	GRAZIELA REGINA TAVARES
723	t	71219781134	ALLEN.PEREIRA	ALLEN DAVIS DOS SANTOS PEREIRA
724	t	70241913179	RODRIGO.RODRIGUES	RODRIGO.RODRIGUES
725	t	00877386277	BRUNO.LIMA	BRUNO BALAREZ DE LIMA
726	t	02347476173	ADILA.SOUZA	ADILA SOARES DE SOUZA
727	t	49315153120	CLEIDE.PEREIRA	CLEIDE MARIA DA SILVA
728	t	71994815191	LARISSA.ESSELIN	LARISSA LORENA ESSELIN LIRA BARBOSA
729	t	00917677170	ROSANA.SANTOS	ROSANA BATISTA DOS SANTOS
730	t	81645244172	RAQUEL.SILVA	RAQUEL APARECIDA SILVA
731	t	70161084141	ALICIA.DIAS	ALICIA FIGUEIRA DIAS
732	t	41899822453	GENILDO.MELO	GENILDO MINERVINO DE MELO
733	t	01707244170	MARIA.LAURA	MARIA LAURA ALVARES DE OLIVEIRA
734	t	03865988199	THAIS.MELO	THAIS NOGUEIRA MELO
735	t	00114134685	ANIVALDO.FARIA	ANIVALDO DONIZETE DE FARIA
736	t	03790943126	RAYANNE.FRANCENER	RAYANNE MAIARA FRANCENER
737	t	00171580109	RAFAEL.BARBOSA	RAFAEL BARBOSA DA SILVA
738	t	29749763874	RAFAEL.MARCHI	RAFAEL CARLOS MARCHI SILVA
739	t	04439493602	EMMILIO.SANTOS	EMMILIO EMANUEL ALVES DOS SANTOS
740	t	53054954153	DEROCI.NASCIMENTO	DEROCI  ALVES NASCIMENTO
741	t	03434208305	THIAGO.QUEIROZ	THIAGO.QUEIROZ
742	t	70087297159	SAMUEL.CRUZ	SAMUEL.CRUZ
743	t	03586364180	NAYARA.DANTAS	NAYARA DE ARAUJO DANTAS
744	t	01375219103	THIAGO.OSALES	THIAGO DE OLIVEIRA SALES
745	t	05474460120	VITORIA.SANTOS	VITORIA APARECIDA ROSA DOS SANTOS
746	t	88387399191	FABIANO.SILVA	FABIANO FERREIRA SILVA
747	t	05700823152	MIRELLA.SILVA	MIRELLA ELIZA DA SILVA
748	t	00215738322	ADRIANA.LIMA	ADRIANA FERREIRA LIMA
749	t	02688978160	ALINE.CAVALCANTE	ALINE EVELYN LOURENCO AGUIAR
750	t	10770689647	BRUNA.CASTRO	BRUNA PACHECO DE AVILA CASTRO
751	t	04783893101	FHABYANA.PEREIRA	FHABYANA BORGES PEREIRA
752	t	02347556100	DIEGO.BOAVENTURA	DIEGO MOURA BOAVENTURA
753	t	03916780190	ALLAN.SILVA	ALLAN FRANKLIN PACHECO SILVA
754	t	03939802603	EDSON.RODRIGUES	EDSON RODRIGUES DA COSTA
755	t	04947982110	ELISA.PEREIRA	ELISA MARIA VILASBOA PEREIRA
756	t	00781329329	HENNE.PINHEIRO	HENNE KAROLINE DE JESUS PINHEIRO
757	t	00746025190	FLAUBERT.ARAUJO	FLAUBERT SOUZA ARAUJO
758	t	03417915120	HELLEN.CASTRO	HELLEN DA SILVA DE CASTRO
759	t	70167403150	ISABELLA.MARIANO	ISABELLA PEREIRA MARIANO
760	t	02763113109	VANESSA.MORAES	VANESSA ALVES DE MORAES
761	t	02793590126	ALINNE.CARMO	ALINNE DE CASSIA MENDES DO CARMO
762	t	03558658159	CLAUDIANA.SILVA	CLAUDIANA AUGUSTA DA SILVA
763	t	96678380134	ALEXANDRA.SOUSA	ALEXANDRA DE SOUSA
764	t	75218488100	MIRIANA.ARRAIS	MIRIANA ALMEIDA ARRAIS
765	t	03067060164	MARIANA.CARMO	MARIANA ALVES DO CARMO
766	t	99117380359	ANDREIA.CASTRO	ANDREIA MARQUES DA SILVA CASTRO
767	t	01503593142	ELAINE.XAVIER	ELAINE.XAVIER
768	t	58383450710	JOSE.ROBERTO	JOSE ROBERTO MOREIRA
769	t	04598276347	ERIK.TORRES	ERIK LEANDRO SILVA TORRES
770	t	03230447190	ITALO.CAETANO	ITALO MATEUS SOUZA CAETANO
771	t	01621141179	BRUNO.BISPO	BRUNO FERNANDES DA SILVA BISPO
772	t	03390696199	JENNIFER.COSTA	JENNIFER ALVES DA COSTA
773	t	05002749160	ALLISON.RODRIGUES	ALISSON ALKMIM RODRIGUES
774	t	02559084155	RODRIGO.PADILHA	RODRIGO DE JESUS PADILHA
775	t	35046597187	ROGERIO.NEVES	ROGERIO CAETANO DE ALMEIDA NEVES
776	t	00406105197	CRISTIANE.PEREIRA	CRISTIANE DA SILVA REZENDE
777	t	05356320108	KARINE.VIEIRA	KARINE PADILHA NUNES VIEIRA
778	t	86473158120	MARCELINO.ROCHA	MARCELINO PATRICIO ALVES ROCHA
779	t	04689459118	KARINNE.ALVES	KARINNE ALVES DOS SANTOS DE OLIVEIRA
780	t	93239912287	VITORIA.BERNARDES	VITORIA KAROL BERNARDES REGO
781	t	01196848106	FRANCIELLI.CRISTINA	FRANCIELLI CRISTINA DA SILVA
782	t	06138367154	FLAVIANE.ARRUDA	FLAVIANE SANTIAGO DE ARRUDA
783	t	09919345636	MIRLEIDE.MOTA	MIRLEIDE ALVES MOTA
784	t	03295537127	ALINE.SEGATI	ALINE SEGATI ALVES
785	t	01501359126	CAYO.OLIVEIRA	CAYO CESAR DOS SANTOS OLIVEIRA
786	t	34848398134	FELIPE.MENDONCA	WALBER FELIPE DE MENDONCA
787	t	03904082124	ADRIELLE.MAGALHAES	ADRIELLE SILVA MAGALHÃES
788	t	09765723628	FLAVIANE.GOMES	FLAVIANE ALVES NAVARRO GOMES
789	t	00422051152	RODRIGO.BISTAFA	RODRIGO BISTAFA
790	t	04470356140	VITORIA.PEREIRA	VITORIA EVELYN ALVES PEREIRA
791	t	00064209156	LUIZ.FSOUZA	LUIZ FERNANDO SOUZA
792	t	70187137145	MILLENA.LEAL	MILLENA.LEAL
793	t	65120493300	JOSE.CARNEIRO	JOSE FLAVIO CARNEIRO DOS SANTOS
794	t	06714478666	ALCIDES.NETO	ALCIDES PEREIRA DE SOUZA NETO
795	t	12071665619	WALISSON.GABRIEL	WALISSON BERNARDES GABRIEL
796	t	75033038168	ABRAAO.SILVA	ABRAAO PEREIRA DA SILVA
797	t	01340049228	KEOMA.SANTOS	KEOMA LOPES SANTOS
798	t	89001036104	MARCIO.ASILVA	MARCIO DE ARAUJO SILVA
799	t	00483503380	ALBERTO.SANTOS	ALBERTO MALTAS SANTOS FILHO
800	t	73181315168	FABIO.FORTALEZA	FABIO FERREIRA FORTALEZA
801	t	08364465775	ANDERSON.DUARTE	ANDERSON DE SOUZA DUARTE
802	t	04358207103	CARLA.MARQUES	CARLA SEBASTIANA MARQUES DA SILVA
803	t	01725124173	ANA.PATRICIA	ANA PATRICIA COSTA RAMIREZ
804	t	93034636172	ALEXANDER.ROLAND	ALEXANDER ROLAND
805	t	81474423191	SILVIO.JOSE	SILVIO JOSE SABINO
806	t	01435579127	TIAGO.COELHO	TIAGO COELHO GUIMARAES
807	t	04139324198	WALTER.FILHO	WALTER FILHO PEREIRA DA SILVA
808	t	02768419303	ALEX.MARQUES	ALEX MARQUES SILVA
809	t	01957670126	FAGNEN.SILVA	FAGNEN DOS SANTOS SILVA
810	t	03506914103	THIAGO.VITORINO	THIAGO VITORINO DA SILVA
811	t	11130806120	ABINISIO.SILVA	ABINISIO PAES DA SILVA
812	t	72762322120	ADRIANA.MGOMES	ADRIANA MONTELANI GOMES
813	t	02210433142	ALAN.EPERES	ALAN ESTEFANO DOS SANTOS PERES
815	t	01867002108	AMANDA.LGUIA	AMANDA LARA ALVES DA GUIA
816	t	04861857104	AMANDA.LCUNHA	AMANDA LHOUISE CUNHA DA SILVA
817	t	55918433104	ANDREA.MOLIVEIRA	ANDREA MATHIAS DE OLIVEIRA
818	t	79712550125	ANDREIA.GFARIAS	ANDREIA GOMES DE FARIAS
819	t	69639230197	APARECIDA.LOPES	APARECIDA MATOS LOPES
820	t	46083146968	AURI.NASCIMENTO	AURI SILVANE ALVES DO NASCIMENTO
821	t	04130655108	BRUNA.LPEREIRA	BRUNA LUANA DE SOUZA PEREIRA
822	t	05212018102	CAMILA.VLIMA	CAMILA VIANA SOUZA LIMA
823	t	00884194108	CARLOS.EMAIA	CARLOS EDUARDO DA SILVA MAIA
824	t	01705837166	CARLOS.VSANTOS	CARLOS VITOR MATOS DOS SANTOS
825	t	05431592130	CAROLINE.OTEIXEIRA	CAROLINE DE OLIVEIRA TEIXEIRA
826	t	04445257184	CHARLY.SZACARIAS	CHARLY DOS SANTOS ZACARIAS
827	t	69886539100	CLAUDINEI.DARRUDA	CLAUDINEI DE ARRUDA
828	t	02400959102	CRISLAINE.LFERREIRA	CRISLAINE LUCIA FERREIRA
829	t	49557467134	CRISTIANO.LLAGOEIRO	CRISTIANO LOPES LAGOEIRO
830	t	01595108998	DANIEL.BAMPI	DANIEL BAMPI
831	t	22980229172	DELMARIO.DBARROS	DELMARIO DE BARROS
832	t	40972518215	EDSON.BOLIVEIRA	EDSON BERNARDO DE OLIVEIRA
833	t	79898483172	ELAINE.CVITORINO	ELAINE CRISTINA VITORINO
834	t	00698591160	ELLEN.RCOSTA	ELLEN REGINA DA COSTA
835	t	01031967141	EMANOEL.DBENITES	EMANOEL DIANGELO BENITES
836	t	05423897114	EVERLYN.NBRITO	EVERLYN NERIS DE BRITO
837	t	88844420104	FABIO.CSILVA	FABIO CUNHA DA SILVA
838	t	88696308115	FABIO.FSOUSA	FABIO FURTADO DE SOUSA
839	t	56831820182	FLORISVALDO.DSILVA	FLORISVALDO DA SILVA RODRIGUES
840	t	04850125107	GLEICIANE.RSILVA	GLEICIANE RIBEIRO DA SILVA
842	t	03960203136	HAIRON.FMONTEBELER	HAIRON FREITAS MONTEBELER
843	t	04860467108	HELEN.LSANTOS	HELEN LETICIA CANDIDO DOS SANTOS
844	t	65462998104	HILTON.GMAGALHAES	HILTON GEORGE DE MAGALHAES
845	t	00649363175	WALISSON.ASILVA	WALISSON DE ABREU E SILVA
846	t	54477301120	WELINTON.VOLIVEIRA	WELINTON VIDOTTI DE OLIVEIRA
847	t	03436350117	WENDRELL.VSOBRADIEL	WENDRELL VANDER MONTEBELER SOBRADIEL
848	t	00891471111	WESLEY.SOLIVEIRA	WESLEY DA SILVA OLIVEIRA
849	t	92070906191	RENATA.CUNHA	RENATA DA CUNHA GUILARDUCCI
850	t	02141096154	AVA.FARAUJO	AVA FABIA ALVES DE OLIVEIRA ARAUJO
851	t	00254626106	CARLOS.OLIVEIRA	CARLOS HENRIQUE AJALA DE OLIVEIRA
852	t	02444579135	GUILHERME.ELIAS	GUILHERME DA SILVA ELIAS
853	t	06484168440	FRANCISCO.ALMEIDA	FRANCISCO ERICO SILVA DE ALMEIDA
854	t	94154333100	ADONEL.LBATISTA	ADONEL.LBATISTA
855	t	81760175234	FLAVIO.RSALES	FLAVIO.RSALES
856	t	02737171113	JACKELINE.MATOS	JACKELINE DO NASCIMENTO ALVES DE MOURA MATOS
857	t	70564000191	ELISANGELA.CARVALHO	ELISANGELA FERREIRA DE CARVALHO
858	t	73297437120	BRUNO.RAMOS	BRUNO MARCEL RAMOS MACEDO
859	t	77386949391	MAGNO.CFERREIRA	MAGNO CESAR FERREIRA
860	t	04483054104	CAROLINE.BARROS	CAROLINE BARROS DE ALMEIDA
861	t	03930502119	DIANA.LEMES	DIANA CHAGAS ARAUJO LEMES
862	t	57014485172	IZAULINO.JFILHO	IZAULINO JOAQUIM GAMA FILHO
863	t	01259265102	JACKSON.WTEIXEIRA	JACKSON WESLLEN DA SILVA TEIXEIRA
864	t	03798096139	JAKELINE.MMIRANDA	JAKELINE MOURA MIRANDA
865	t	03032873142	JEFFERSON.SMOURA	JEFFERSON DA SILVA MOURA
866	t	35741508812	JENNYFER.VBARBARA	JENNYFER VIEIRA BARBARA
867	t	82392099120	JONAIR.MMARTINS	JONAIR MEIRELES MARTINS
868	t	73719668134	JOSE.BBOTELHO	JOSE BENEDITO BOTELHO
869	t	03019613108	KEDMA.CSOUZA	KEDMA CRISTINA NUNES DE SOUZA
870	t	28439260172	LEVINO.SJESUS	LEVINO SILVA DE JESUS
871	t	05361998176	LETICIA.RMORAES	LETICIA RITA DE MORAES
872	t	68873310125	MARCELO.BFREITAS	MARCELO BISPO DE FREITAS
873	t	00670530174	MARCELO.ACARVALHO	MARCELO DE ALMEIDA CARVALHO
874	t	31839258187	MARCIO.AMORAES	MARCIO AUGUSTO DE MORAES
875	t	42025680104	MARLUCIA.BCOELHO	MARLUCIA BATISTA AMARAL COELHO
876	t	04828782150	MATHEUS.MGUIMARAES	MATHEUS MOREIRA GUIMARAES
877	t	12757304771	MYCHEL.WPINHEIRO	MYCHEL WINKELL ALMEIDA PINHEIRO
878	t	02325961150	NADIA.PCARVALHO	NADIA PAULA RIBEIRO CARVALHO
879	t	01120877105	NAYARA.FCOSTA	NAYARA FERREIRA DA COSTA
880	t	05297358140	PAMELA.CMIRANDA	PAMELA CRISTINA DA SILVA MIRANDA
881	t	73046175191	PHABLO.GSILVA	PHABLO GONCALVES SILVA
882	t	00886341167	RAFICK.MHOTTA	RAFICK MACHADO HOTTA
883	t	03340052131	RODRIGO.FALMEIDA	RODRIGO FORTES DE ALMEIDA
884	t	05075253105	ROMARIO.GOLIVEIRA	ROMARIO GABRIEL DA SILVA OLIVEIRA
885	t	68895631153	RONALDO.FROSA	RONALDO FERREIRA DA ROSA
886	t	00366209124	ROSANE.ASANTOS	ROSANE ARRUDA DOS SANTOS
887	t	06859305215	SEBASTIAO.IFILHO	SEBASTIAO INACIO JORGE FILHO
888	t	74241907920	SERGIO.LSILVA	SERGIO LUIS FLORES DA SILVA
889	t	02668005132	TATIANE.PSANTOS	TATIANE PEREIRA SANTOS
890	t	02185552104	THIAGO.FLEITE	THIAGO FELIPE MOURA LEITE
891	t	09002543603	TATIANE.FRANCA	TATIANE TEIXEIRA FRANÇA
892	t	03970761182	ANA.GRACA	ANA CLAUDIA VIEIRA DA GRACA
893	t	04274848124	CASSIA.SOUZA	CASSIA VALERIA SOUZA XAVIER
894	t	70121226174	PAULA.GABRIELLY	PAULA.GABRIELLY
895	t	01342181328	PEDRO.IVO	PEDRO IVO PEREIRA DA SILVA
896	t	07547815693	JUCIMAR.CIMINO	JUCIMAR ADALGISA DE ASSIS CIMINO
897	t	87342286168	ALEXANDRE.HAMU	ALEXANDRE DE MOURA HAMU
898	t	04711263144	PAULO.GUILHERME	PAULO GUILHERME COSTA SOUZA
899	t	03255771102	MONICA.RODRIGUES	MONICA MAXIMO DE NOVAES RODRIGUES
900	t	58962425149	CLEOMAR.PEREIRA	CLEOMAR BATISTA PEREIRA
901	t	09132179642	JEFERSON.OLIVEIRA	JEFFERSON DA COSTA OLIVEIRA
902	t	00261027301	AGLICO.RABELO	AGLICO DE SOUSA RABELO
903	t	00425473180	FABIANA.PEREIRA	FABIANA BUENO PEREIRA
904	t	04641185140	GALILEIA.SILVA	GALILEIA ARIELEN APARECIDA NUNES DE ARAUJO SILVA
905	t	82557896187	KARLA.MOREIRA	KARLA KATRINY ALVES MOREIRA
906	t	87632918191	ANA.CALACA	ANA.CALACA
907	t	98705954134	EDIRLENE.SILVA	EDIRLENE JESUS DA SILVA
908	t	72748230191	JOVANY.FREITAS	JOVANY AUGUSTO DE FREITAS
909	t	05602159169	MATHEUS.SOUSA	MATHEUS FELIPE DARLAN DE SOUSA
910	t	02711957381	ANDRE.CARLOS	ANDRE CARLOS DA SILVA
911	t	03917996650	KATE.RODRIGUES	KATE MARTINS RODRIGUES
912	t	95583734191	LARA.GOMIDES	LARA DE CAMPOS GOMIDES
913	t	01480574112	TATIANE.PINHEIRO	TATIANE DIAS PINHEIRO
914	t	02954644109	LUCYANE.SANTOS	LUCYANE ALVES SANTOS
915	t	83976477168	CLEUBER.PRADO	CLEUBER HUDSON DO PRADO
916	t	01378903200	MARIANA.NUNES	MARIANA RODRIGUES NUNES
917	t	00897152174	DENISE.CESAR	DENISE GOMES MATIAS CESAR
918	t	75546833220	DIEGO.NERI	DIEGO DE FRANCA NERY
919	t	00079463177	FABIO.FERNANDES	FABIO BENTO FERNANDES
920	t	71420037153	JULIANO.CRUZ	JULIANO RODRIGUES DA CRUZ
921	t	03893335145	GEOVANA.OLIVEIRA	GEOVANA FERNANDES DE OLIVEIRA
922	t	70031447198	VANESSA.VIEIRA	VANESSA RIBEIRO ALVES VIEIRA
923	t	72250658153	DANILO.FEITOSA	DANILO DE JESUS FEITOSA
924	t	03715599103	TALITA.SANTOS	TALITA ROCHA DE SOUZA SANTOS
925	t	00240372140	CLEIDE.OLIVEIRA	CLEIDE DOS SANTOS OLIVEIRA
926	t	02949030114	ANA.SLEAO	ANA PAULA SILVA LEAO
927	t	76186393291	FABRICIA.MARQUES	FABRICIANA MARQUES CRUZ
928	t	02622472161	AMANDA.GRODRIGUES	AMANDA GARCIA RODRIGUES
929	t	05567915179	ANA.ARAUJO	ANA GABRIELA MARTIM ARAUJO
930	t	03693870152	DOUGLAS.LEONIS	DOUGLAS PONCE LEONIS
931	t	84272775120	CLEVERSON.JFORNAZIER	CLEVERSON.JFORNAZIER
932	t	09956991600	THAINA.MSILVA	THAINA MENDES PIRES SILVA
933	t	72182997172	ANTONIO.VALVES	ANTONIO VICTOR ALVES
934	t	01297056329	DJALMA.JUNIOR	DJALMA ACRISIO PINHEIRO JUNIOR
935	t	00467958122	DOUGLAS.FALMEIDA	DOUGLAS FERREIRA DE ALMEIDA
936	t	00299259161	MARIANA.CROCHA	MARIANA CABRAL ROCHA
937	t	60677032382	DANIEL.VCOSTA	DANIEL.VCOSTA
938	t	02300391627	MAX.MACHADO	MAX.MACHADO
939	t	03026881104	FERNANDO.AZEVEDO	FERNANDO HENRIQUE CAMPOS AZEVEDO
940	t	69978999191	ALICIANE.RONDON	ALICIANE DA SILVA RONDON
941	t	02451889160	GRAZIELE.SSILVA	GRAZIELE SOARES DA SILVA
942	t	02327792100	JOAO.OCARVALHO	JOAO OLIVEIRA CARVALHO DO NASCIMENTO
943	t	00717691101	KAMYLLA.RAMOS	KAMYLLA MAGALHAES RAMOS
944	t	01004799101	DANIELLE.VASCONCELOS	DANIELLE PERES DE VASCONCELOS
945	t	01345881185	DIEGO.RCARVALHO	DIEGO ROBERTO DE CARVALHO
946	t	04150623147	WESLEY.SEABRA	WESLEY DIEGO DA SILVA SEABRA
947	t	01868364143	PAMELA.BOCAMPOS	PAMELA.BOCAMPOS
948	t	03953171500	JONATA.ASANTOS	JONATA ANDRADE SANTOS
949	t	03737434166	ERIC.PEREIRA	ERIC BRUNO DO ESPIRITO SANTO PEREIRA
950	t	03420936141	BRUNO.SERRA	BRUNO CESAR MARIM SERRA
951	t	61429071338	VICTOR.SSANTIAGO	VICTOR.SSANTIAGO
952	t	02821668155	BRUNA.RATAIDE	BRUNA RAFAELA ALVES ATAIDE
953	t	04255425302	MOISES.JMORAES	MOISES DE JESUS MORAES
954	t	60035128305	AMANDA.JLOPES	AMANDA JOCASTA DE CASTRO LOPES
955	t	05788112192	FABIO.FSILVA	FABIO FERNANDO DA SILVA
956	t	00957480164	PAULO.HREZENDE	PAULO HENRIQUE HADDAD REZENDE DE OLIVEIRA
957	t	31766676120	LUCINEIDE.MMAIA	LUCINEIDE MARQUES MAIA
958	t	03144531138	MARCELLA.CDINIZ	MARCELLA CORREA SILVA DINIZ
959	t	02668226309	SAULO.PEREIRA	SAULO ARTEMIO ARRUDA PEREIRA
960	t	75652005149	JUSICLEIA.SOLIVEIRA	JUSICLEIA DA SILVA OLIVEIRA
961	t	04888482101	ANA.PFERRAZ	ANA PAULA DE FARIA FERRAZ
962	t	02480584100	DYANDRA.KFARIA	DYANDRA KARLA FERNANDES FARIA
963	t	05554953379	RODRIGO.FURTADO	RODRIGO FURTADO DA SILVA
964	t	66607698115	SERGIO.RSILVA	SERGIO RICARDO FURTADO SILVA
965	t	02798933102	VANESSA.ASOUSA	VANESSA ALVES DE SOUSA
966	t	03391328177	PAULO.HSOARES	PAULO HENRIQUE SOARES SILVA
967	t	01687805180	LEANDRO.NASCIMENTO	LEANDRO DA SILVA NASCIMENTO
968	t	94837708153	MAX.RVALADAO	MAX RIBEIRO VALADAO
969	t	05011682145	LUCAS.JSILVA	LUCAS JOSÉ DA SILVA
970	t	70777012120	ALESSANDRA.BMENEZES	ALESSANDRA.BMENEZES
971	t	04377536117	PAULO.FALVES	PAULO FILIPE ALVES
972	t	02231958120	LUCAS.CRABELO	LUCAS DO CARMO RABELO
973	t	05087036112	CAMILA.MTOMAS	CAMILA.MTOMAS
974	t	01321977158	ALLAN.FSANTOS	ALLAN ROBERTO FORTELLES DOS SANTOS
975	t	87054361334	RODRIGO.OLIMA	RODRIGO OLIVEIRA LIMA
976	t	05228030166	MATHEUS.GSILVA	MATHEUS WILLIAM GONÇALVES DA SILVA
977	t	73607142149	JULIANNE.SLOPES	JULIANNE SANTOS LOPES
978	t	03286013145	FRANCIELY.SLOPES	FRANCIELY APARECIDA DA SILVA LOPES
979	t	01628261170	SAMUEL.FALVES	SAMUEL SANTOS FERNANDES ALVES
980	t	10148706665	DANIELLES.CJESUS	DANIELLES CARNEIRO DE JESUS
981	t	58526625187	MARGARETH.CBRANCO	MARGARETH ROSE CASTELO BRANCO LEITE
982	t	86684647104	MARLENE.AANDRADE	MARLENE ALVES ANDRADE
983	t	01025243196	ANA.ACOSTA	ANA CAROLINA ABREU COSTA
984	t	24842761172	MARCELO.CPIMENTA	MARCELO CAMPOS PIMENTA
985	t	00712177116	CHANDECLE.GCOSTA	CHANDECLE GONÇALVES DA COSTA
986	t	01361891130	VIVIANE.SSILVA	VIVIANE SANTOS DA SILVA
987	t	69523169149	KARINA.PINHEIRO	KARINA FREITAS ARAUJO PINHEIRO
988	t	01481732170	BRIGGIDA.ESILVA	BRIGGIDA EMIDIO DA SILVA
989	t	06282088127	KARINA.CFLOR	KARINA.CFLOR
990	t	60023611383	ERIKA.ALVES	ERIKA SANTOS ALVES
991	t	01934924156	RICARDO.CMARTINS	RICARDO CUELHO MARTINS
992	t	95118977134	ADNER.CRODRIGUES	ADNER ROSA CALDAS RODRIGUES
993	t	05119549136	DANIEL.MRODRIGUES	DANIEL MIRANDA RODRIGUES
994	t	41282175149	REGINALDO.RSOUZA	REGINALDO ROSA DE SOUZA
995	t	01044069171	WESLEY.JMORAIS	WESLEY JONY MORAIS
996	t	95505202187	ADRIANO.SMAGALHAES	ADRIANO SOUZA MAGALHAES
997	t	90242386172	VICTOR.CREIS	VICTOR CAMPOS ALCANTARA REIS
998	t	03198093155	BIANCA.OBRANDAO	BIANCA DE OLIVEIRA BRANDAO
999	t	05636999496	JOAO.PNASCIMENTO	JOAO PAULO NASCIMENTO DOS SANTOS
1000	t	30761590110	ANDRE.LMARQUES	ANDRE.LMARQUES
1001	t	80889948100	PAULO.HMORAIS	PAULO CESAR HONORIO DE MORAES
1002	t	13431786650	BRENDA.ANERY	BRENDA.ANERY
1003	t	04293958177	PETRICK.MOSMANN	PETRICK EBER MOSMANN PINTO
1004	t	72687193100	CHARLES.HNUNES	CHARLES HENRIQUE DIAS NUNES
1005	t	85606375168	MARCELO.RALVES	MARCELO RODRIGUES  ALVES
1006	t	04792928176	BRENDA.LSILVA	BRENDA LOYOLA DA SILVA
1007	t	04920110103	CHRISLAINE.FSILVA	CHRISLAINE FERRAZ DA SILVA
1008	t	85562491249	THIAGO.DRIBEIRO	THIAGO RODRIGUES DIAS RIBEIRO
1009	t	02032826046	VICTOR.OKNOB	VICTOR OTTO KNOB
1010	t	02330363184	LEONARDO.CCAETANO	LEONARDO CARVALHO CAETANO
1011	t	99971488604	MARCIO.HNASCIMENTO	MARCIO HENRIQUE DO NASCIMENTO
1012	t	01131056140	DANIEL.RSIMAO	DANIEL RODRIGUES SIMAO
1013	t	04478920109	GUILHERME.AARUJO	GUILHERME ADRIANO SILVA DE ARAUJO
1014	t	72793058149	TATIANY.AOLIVEIRA	TATIANY APARECIDA SILVA OLIVEIRA
1015	t	00064486150	FABRICIO.MACHADO	FABRICIO.MACHADO
1016	t	03093374103	BRUNA.RNASCIMENTO	BRUNA RODRIGUES DO NASCIMENTO
1017	t	04283969176	LUCAS.CCOSTA	LUCAS CARVALHO DA COSTA
1018	t	00352965266	WANDERSON.SOARES	WANDERSON FELIPE DE SIQUEIRA SOARES
1019	t	03937053158	WANDERSON.MFREITAS	WANDERSON MOREIRA DE FREITAS
1020	t	02083476158	EDUARDO.CVENTURINI	EDUARDO CAMPOS VENTURINI
1021	t	05156810160	EDUARDO.HNEVES	EDUARDO HENRIQUE ARAÚJO NEVES AURELIANO
1022	t	03687564169	MATHEUS.FALENCAR	MATHEUS FELIPE ALENCAR
1023	t	87428989191	DANIEL.AFREITAS	DANIEL AUGUSTO DE FREITAS
1024	t	91007224134	JOSECARLOS.VFILHO	JOSE CARLOS DA COSTA VIANNA FILHO
1025	t	01486007198	ANTONIO.GGIROTTO	ANTONGNYONI GIROTTO DE OLIVEIRA BORGES
1026	t	96534311372	JOSYANY.TORRES	JOZYANY RIBEIRO TORRES
1027	t	28102061120	DIVINO.DMENDES	DIVINO DARLAN MENDES
1028	t	93033125115	VIVIANE.RODRIGUES	VIVIANE PEREIRA DE OLIVEIRA RODRIGUES
1029	t	79295258134	ANTONIO.ANDRADE	ANTONIO FERREIRA DE ANDRADE JUNIOR
1030	t	71731717172	ELIAS.SILVA	ELIAS BORGES SILVA
1031	t	12913482635	SAYONARA.ADANTAS	SAYONARA.ADANTAS
1032	t	21629364886	AISLAN.CORREA	AISLAN FERNANDO WALDEMAR CORREA
1033	t	01689334118	HEVELLENN.DANIEL	HEVELEENN WACKER DANIEL
1034	t	05199240102	FLAVIA.DFERNANDES	FLAVIA DE LIMA FERNANDES
1035	t	04252873193	ANDREIA.MNASCIMENTO	ANDREIA MARTINS NASCIMENTO
1036	t	05051302114	TALITA.NASCIMENTO	TALITA NAYARA GOMES DO NASCIMENTO
1037	t	01716634970	DALVICE.DQUEIROZ	DALVICE DE QUEIROZ DEMITROVICH
1038	t	04649550173	KARITA.MESSIAS	KARITA RAYANE MESSIAS SOUSA
1039	t	51724618172	EDILSON.GSILVA	EDILSON GOMES DA SILVA
1040	t	05147739602	ALEXANDRE.RREZENDE	ALEXANDRE ROSA DE REZENDE
1041	t	60405403380	ANDRESSA.FSILVA	ANDRESSA FARIAS DA COSTA E SILVA
1042	t	12866424778	FERNANDO.ALAVORATO	FERNANDO ANDRADE LAVORATO
1043	t	02672254150	CARLOS.EOLIVEIRA	CARLOS.EOLIVEIRA
1044	t	00358717175	KELLY.SMONTEIRO	KELLY ANDRE DA SILVA MONTEIRO
1045	t	00973462256	GEORGE.JR	GEORGE LOPES DA SILVA JUNIOR
1046	t	01887087230	MARCELO.SLOPES	MARCELO AUGUSTO DA SILVA LOPES
1047	t	00867469102	CRISTIANE.ARAUJO	CRISTIANE ARAUJO BARBOSA
1048	t	65560604100	MARCIA.CALVES	MARCIA CLANE ALVES LENCINA
1049	t	98941763215	MARCUS.VSILVA	MARCUS VIANA DA SILVA
1050	t	05946920324	KEYCIANE.VIEIRA	KEYCIANE PINTO VIEIRA
1051	t	00483276111	BRUNO.KSOUSA	BRUNO KRONIT DE SOUSA
1052	t	01345359101	VIVIANE.ALVES	VIVIANE ALVES DE CARVALHO
1053	t	82415560120	RENATA.CSILVA	RENATA CAMARGOS SILVA
1054	t	91013240120	KARINA.CORREA	KARINA LOURENCO CORREA
1055	t	13114635857	ARISTOTELIS.PEIXOTO	ARISTOTELIS FERREIRA PEIXOTO
1056	t	75260565134	SARAH.MRIBEIRO	SARAH KETULLE MACHADO RIBEIRO
1057	t	95343636187	CLAYTON.RCASTRO	CLAYTON ROGERIO DE CASTRO
1058	t	01758878100	LUANA.CRISPIM	LUANA CRISPIM
1059	t	60661720187	CRISTINA.CCARVALHO	CRISTINA CUENCAS CARVALHO
1060	t	01790111110	LUDMILA.VSOUSA	LUDMILA VIEIRA SOUSA
1061	t	01250421160	BRUNA.SAGUIAR	BRUNA SOARES AGUIAR
1062	t	02856500170	CHARLES.SOUZA	CHARLES GONCALVES DE SOUZA
1063	t	02426926139	WESLEY.RBARBOSA	WESLEY RODRIGUES BARBOSA
1064	t	94131392020	MICHELI.SOARES	MICHELI SILVEIRA SOARES
1065	t	05550871157	JEFERSON.OALVES	JEFFERSON MENDES OLIVEIRA ALVES
1066	t	05827623164	BIANCA.APAIVA	BIANCA ALVES PAIVA
1067	t	06508427156	MATHEUS.BSILVA	MATHEUS BARBOSA DA SILVA
1068	t	00499400151	SAMANTHA.DMENDES	SAMANTHA REGINE DELFINO MENDES DE CAMPOS
1069	t	06292182185	EMANUELLI.FOLIVEIRA	EMANUELLI CRISTINA FERNANDES
1070	t	06345841110	ANGEL.SCOSTA	ANGEL CRISTINY DA SILVA COSTA
1071	t	02456712188	WALESKA.FXAVIER	MAYARA WALESKA DE FARIAS XAVIER
1072	t	05585829157	LUANA.CSANTOS	LUANA CAROLINA DOS SANTOS NASCIMENTO
1073	t	05794076160	BRENO.GSILVA	BRENO GOMES DA SILVA
1074	t	06077147192	JOYCE.BLOPES	JOYCE BATISTA LOPES
1075	t	87935139172	CLEBIO.BSOUZA	CLEBIO SILVA BATISTA DE SOUZA
1076	t	12102487601	GILBERTO.DJUNIOR	GILBERTO MARCIANO DIAS JUNIOR
1077	t	00827747225	CAMILA.FSANTOS	CAMILA FARIAS DOS SANTOS
1078	t	98880888153	DANTE.VSANTOS	DANTE VIEIRA SANTOS
1079	t	01226477267	BARBARA.SOUZA	BARBARA APARECIDA DE SOUSA
1080	t	02345389109	JEFTER.CDANTAS	JEFTER CRUZ DANTAS
1081	t	00700431160	LUCIO.ISILVA	LUCIO IRINEU SILVA
1082	t	00765633167	LUCAS.ESOARES	LUCAS EDUARDO SOARES
1083	t	64002667391	LEANDRO.SLIMA	LEANDRO DA SILVA LIMA
1084	t	97552348100	PLINIO.SOLIVEIRA	PLINIO REGIS SILVA DE OLIVEIRA
1085	t	01929285116	HEBERT.SALMEIDA	HEBERT FAGNER DOS SANTOS ALMEIDA
1086	t	92065163100	MICHELLE.GANDRADE	MICHELLE GONZALEZ ANDRADE
1087	t	01883809142	ALANE.MARTINS	ALANE.MARTINS
1088	t	02088598103	GISELE.GOMES	GISELE GOMES DE MATOS
1089	t	05625177155	HALLISSON.JORGE	HALLISSON JORGE CARDOSO
1090	t	00922670170	LORENA.BRAGA	LORENA.BRAGA
1091	t	02211611125	DANIEL.RS	DANIEL RIBEIRO DE SOUSA
1092	t	09123147709	ALONSIO.RSOUZA	ALONSIO RANGEL DE SOUZA
1093	t	01724470116	RHAYSSA.FRANCA	RHAYSSA WANDERLEY FRANCA ALVES
1094	t	02099228183	AUGUSTO.LLIMA	VITOR AUGUSTO LOPES DE LIMA
1095	t	08900696890	JOSE.COUTO	JOSE LUIZ COUTO SILVA
1096	t	90587243104	KATIA.ABUSZ	KATIA ARLEI BUSZ
1097	t	02400747180	ALINE.SALVES	ALINE DA SILVA ALVES
1098	t	05284609190	AMANDA.SLIMA	AMANDA NAYARA DA SILVA LIMA
1099	t	02395082112	ANNA.AGUIAR	ANNA PAULA ALMEIDA AGUIAR FRANCHI
1100	t	00286598302	REGIANE.FBORRALHO	REGIANE.FBORRALHO
1101	t	00983651183	EDSON.ARUFINO	EDSON AMARAL RUFINO
1102	t	04713128198	CHRISLY.SOUZA	CHRISLY.SOUZA
1103	t	43629857353	JACQUES.PJUNIOR	JACQUES PACHECO LEAO JUNIOR
1104	t	70004723163	LUCAS.SANTOS	LUCAS CARLOS DOS SANTOS
1105	t	34054863191	GIAN.CSOUZA	GIAN CARLOS DE SOUZA
1106	t	72144130191	THIAGO.AVIEIRA	THIAGO ALVES VIEIRA
1107	t	99102668149	ALAN.MFERREIRA	ALAN MACHADO FERREIRA
1108	t	27997869875	THIAGO.RODRIGUES	THIAGO RODRIGUES
1109	t	70367607107	MATHEUS.APIRES	MATHEUS.APIRES
1110	t	01081841109	DANIEL.LSOUSA	DANIEL FERREIRA DE LIMA SOUSA
1111	t	02759120198	GLEDYSON.CRODRIGUES	GLEDYSON CARLOS RODRIGUES DE PAULO
1112	t	01434688127	SIMONE.BLOBAO	SIMONE BATISTA DE SOUZA LOBAO
1113	t	02942964170	DIEGO.VFERREIRA	DIEGO.VFERREIRA
1114	t	58371273134	DANIELA.RSANTOS	DANIELA REGINA DOS SANTOS
1115	t	85240150125	katia.moliveira	KATIA HELENA MARTINS DE OLIVEIRA
1116	t	70427631149	ligiane.cordeiro	LIGIANE DIAS CORDEIRO
1117	t	10720575656	MARCELLA.ISILVA	MARCELLA YURI IOSHIOKA SILVA
1118	t	03072343551	ROBERTO.NMATOS	ROBERTO NEVES OLIVEIRA MATOS
1119	t	01510376127	MIRIAM.RNETA	MIRIAM REJANE DA SILVA NETA
1120	t	83526650144	RITA.KPORTO	RITA KARINE  PORTO DE SOUZA
1121	t	00113257120	CARLA.SMARCAL	CARLA CRISTINA SIMOES MARCAL
1122	t	00970607121	CELSO.MSOUSA	CELSO MORAES DE SOUSA
1123	t	23134615134	HELIO.GJESUS	HELIO GOMES DE JESUS
1124	t	02476345148	ENRICO.DFREITAS	ENRICO GONCALVES DE FREITAS
1125	t	03868075151	HELIO.OJUNIOR	HELIO CAMPOS DE OLIVEIRA JUNIOR
1126	t	03572494109	ISRAEL.SCOELHO	ISRAEL JONATHANS DE SOUSA COELHO
1127	t	81195273134	MARCELO.SHOSOKAWA	MARCELO SALOMAR HOSOKAWA
1128	t	00338253351	ADRIANA.LSOUSA	ADRIANA LICAR DE SOUSA
1129	t	00211874108	DAYANE.SILVA	DAYANE CATRINE ESPINDOLA DA SILVA
1130	t	02090179104	JHONATAN.RCAETANO	JHONATAN RAFAEL TEIXEIRA CAETANO
1131	t	99555590125	RANDERSON.ROLIVEIRA	RANDERSON ROSENDO DE OLIVEIRA
1132	t	00309352100	ADRIANO.PSOUSA	ADRIANO PIMENTA DE SOUSA RIBEIRO
1133	t	04540613154	LUCAS.GMACHADO	LUCAS GOMES MEDRADO
1134	t	00347564267	PAULO.HSOUSA	PAULO HENRIQUE MELO DE SOUSA
1135	t	05239324131	POLLYANA.GRANGEIRO	POLLYANA GRANGEIRO
1136	t	00496168118	RICALA.RODRIGUES	RICALA CASSIANO RODRIGUES
1137	t	93001061120	NATACHA.CUNHA	NATACHA QUEIROZ CUNHA
1138	t	02032894122	CARLA.DAIANE	CARLA DAIANE CARMO AIRES
1139	t	03012774190	DHANGELA.SSOARES	DHANGELA THAISE SOUSA SOARES
1140	t	03906102165	JACKELINE.LSILVA	JACKELINE LOHAYNE SOUZA SILVA
1141	t	50519654153	ALLEX.SFONTES	ALLEX SILVA FONTES
1142	t	03142380183	HUGO.SSILVA	HUGO LEONARDO SOUSA DA SILVA
1143	t	04925134302	ANNA.KGOMES	ANNA KAROLINNE GOMES
1144	t	02638403105	YNGRID.ASILVA	YNGRID GRAZIELE ALVES DA SILVA
1145	t	90668723149	ANDERSON.DMOURA	ANDERSON DANIEL DE MOURA
1146	t	82544956100	CLEBER.GBRUNO	CLEBER GERALDO BRUNO
1147	t	74943545149	ANTONIO.ANANIAS	ANTONIO VITOR DUARTE ANANIAS
1148	t	00543836177	RENATO.ABREU	RENATO CEZAR SOUZA ABREU
1149	t	02979436186	POLIANA.OJEDA	POLIANA RUBIA DA SILVA OJEDA
1150	t	60506756351	ANA.JSOUSA	ANA PAULA JANUARIO DE SOUSA
1151	t	95093494100	MARIANY.TOLEDO	MARIANY ESPINDOLA DE TOLEDO
1152	t	06519528161	ALEX.OSILVA	ALEX.OSILVA
1153	t	03314728131	INGRIDE.RROBERTO	INGRIDE CRISTHIANSEN ROSA ROBERTO
1154	t	88879194100	LAURA.LARJONA	LAURA LUCIENE ARJONA
1155	t	05061681665	RODRIGO.OCOSTA	RODRIGO DE OLIVEIRA COSTA
1156	t	02355246114	DELVANIO.SRAMALHO	DELVANIO DA SILVA RAMALHO
1157	t	03729820184	VITORIA.GSOUZA	VITORIA GABRIELA DA SILVA SOUZA
1158	t	05939221580	THAIS.OMOTA	THAIS OHANA ALVES MOTA
1159	t	08264143628	RENAN.DFELIZARDO	RENAN.DFELIZARDO
1160	t	04171875137	JOAO.FNETO	JOAO FELIPE DUROES DE FREITAS NETO
1161	t	89835247153	LISSANDRA.BBEZERRA	LISSANDRA BRITO BEZERRA
1162	t	00961268174	DANIEL.MRIBEIRO	DANIEL MARTINS RIBEIRO
1163	t	03978606682	MICHELLE.ZARDINI	MICHELLE.ZARDINI
1164	t	08723717602	VANESSA.MLACORTE	VANESSA MARIAN LACORTE PEIXOTO
1165	t	02203814101	DAIANE.STAVARES	DAIANE VIANA DA SILVA TAVARES BEZERRA
1166	t	49557017104	SANDRO.ARIGOTTI	SANDRO ANDRE FRICKS RIGOTTI
1167	t	01299038174	ANDERSON.LOLIVEIRA	ANDERSON LOPES DE OLIVEIRA
1168	t	02082433137	DEBORA.NREIS	DEBORA NUNES DA CRUZ DOS REIS
1169	t	06069105141	JOYCE.POLIVEIRA	JOYCE PLASMO DE OLIVEIRA
1170	t	70240698134	LAERTE.SSILVA	LAERTE SAMUELSON DA SILVA
1171	t	03961372101	LUIZ.AREZENDE	LUIZ.AREZENDE
1172	t	01385713178	LUDMILLA.LAZARIAS	LUDMILLA LEAL AZARIAS
1173	t	03144559148	ANACLECY.MCAMBOTA	ANACLECY MENDES CAMBOTA
1174	t	96122374172	SUELLEM.COUTINHO	SUELLEM DOS SANTOS COUTINHO
1175	t	01316090175	WELSON.LCIRQUEIRA	WELSON LIMA CIRQUEIRA
1176	t	02588890121	TIAGO.BSOARES	TIAGO DE ARIMATEIA BARBOSA SOARES
1177	t	04042105114	VITOR.AFERREIRA	VITOR ALBUQUERQUE FERREIRA
1178	t	02893958109	JOANDERSON.SCOSTA	JOANDERSON DA SILVA COSTA
1179	t	02872634100	CAIO.CCINI	CAIO CESAR CINI
1180	t	01024408167	DJONATHAN.RSANTOS	DJONATHAN SOUSA RODRIGUES SANTOS
1181	t	57020477100	ERIK.SOLIVEIRA	ERIK DE SOUZA OLIVEIRA
1182	t	37745466814	UESLEN.MSANTOS	UESLEN MADUREIRA DOS SANTOS
1183	t	93690398134	THIAGO.MSILVA	THIAGO MATHEUS OLIVEIRA E SILVA
1184	t	06029018639	LUIS.MACHADO	LUIS EDUARDO MACHADO
1185	t	94867330159	GISELE.ARIBEIRO	GISELE ALVES RIBEIRO MARIANO
1186	t	02130821103	PAULO.HSANTOS	PAULO HENRIQUE SANTOS CARDOSO
1187	t	72633476104	THIAGO.CSANTOS	THIAGO COSTA SANTOS MACHADO
1188	t	70318519127	ALINE.SSILVA	ALINE SOARES SILVA
1189	t	75408554104	HELLEN.CPINTO	HELLEN CRISTINA CABRAL OLIVEIRA PINTO
1190	t	04760734139	YASMIMM.AGRACIANO	YASMIMM.AGRACIANO
1191	t	89181930178	VANESSA.GARCIA	VANESSA.GARCIA
1192	t	04425997140	AMABILE.LCAMPOS	AMABILE.LCAMPOS
1193	t	00926781146	ELIOENAI.MSANTOS	ELIOENAI.MSANTOS
1194	t	01685256155	FRANCIELI.GAFURRI	FRANCIELI.GAFURRI
1195	t	04770594143	VITOR.CTELES	VITOR MIRANDA CHAVES TELES
1196	t	03026387177	BRUNO.LSILVA	BRUNO LUIZ DA SILVA OLIVEIRA
1197	t	93279485215	RENAN.HLIMA	RENAN HOLANDA LIMA
1198	t	02612022198	LEANDRO.GONCALVES	LEANDRO EUGENIO GONCALVES
1199	t	12211837611	MARCUS.VBARBOSA	MARCUS VINICIUS BARBOSA
1200	t	01944912185	HELTON.BSILVA	HELTON JOHNATAS BATISTA ALVES E SILVA
1201	t	01927355516	MIRIA.DOURADO	JOSMIRA FERREIRA DOURADO
1202	t	02525645103	FERNANDA.MLEAO	FERNANDA DA SILVA MORAIS
1203	t	01864400145	RUY.PEGORARO	RUY DE MAGALHAES PEGORARO
1204	t	03566360112	ANGELICA.FURTADO	ANGELICA DE ASSIS FURTADO
1205	t	72571250159	FRANCISCO.JSANTOS	FRANCISCO JORGE MENEZES DOS SANTOS
1206	t	69834636172	ENOQUE.CARMO	MAGNO ALVES SIPAUBA
1207	t	08771907637	MAGNO.ALVES	MAGNO.ALVES
1208	t	01204895465	FAGNER.JSILVEIRA	FAGNER JOSE MACHADO DA SILVEIRA
1209	t	05878158175	LORENA.RLINO	LORENA.RLINO
1210	t	70379445131	JULIA.MSOARES	JULIA MARQUES SOARES
1211	t	01051858178	EDIVAN.OMATOS	EDIVAN DE OLIVEIRA MATOS
1212	t	00110432100	TATIANE.AFIRMINO	TATIANE APARECIDA FIRMO
1213	t	02075470251	CARLIANE.SILVA	CARLIANE DE OLIVEIRA SILVA
1214	t	01358558299	RAFAEL.GUALBERTO	RAFAEL FRIGO GUALBERTO
1215	t	03946988199	TALYTHA.LBORDIN	TALYTHA.LBORDIN
1216	t	03890475620	EDUARDO.DBONIFACIO	EDUARDO DANTAS BONIFACIO
1217	t	01045013161	JHONATAN.SCARNEIRO	JONATHAN DOS SANTOS CARNEIRO
1218	t	02040418300	CICERO.GINACIO	CICERO GIVALDO INACIO
1219	t	08097086638	CESAR.LOPES	JULIO CESAR FERNANDES LOPES
1220	t	06915111603	BRUNO.CALVES	BRUNO COSTA ALVES
1221	t	05087242529	BARBARA.SBISPO	BARBARA SANTOS BISPO
1222	t	90521846072	ALEXANDRE.SCUNHA	ALEXANDRE SILVA DA CUNHA
1223	t	03821327340	CAMILA.RMENDES	CAMILA MARREIROS RODRIGUES MENDES
1224	t	04586525398	GIRLENE.BRITO	GIRLENE MAYARA COSTA BRITO
1225	t	00254915124	CAROLINA.SHAMU	CAROLINA SILVA HAMU
1226	t	71220232653	ULISSES.MSEBASTIAO	ULISSES MENDES SEBASTIAO
1227	t	02033610160	VINICIUS.FVIEIRA	VINICIUS FREITAS VIEIRA
1228	t	05420277123	KETLY.RCRUVINEL	KETLY DOS REIS CRUVINEL
1229	t	00723788162	JORGE.RAMALHO	JORGE RAMALHO BARBOSA
1230	t	04504656329	CAROLINE.VANJOS	RANA CAROLINE VEIGA DOS ANJOS
1231	t	02184876143	ANA.CRISTINA	ANA CRISTINA PIRES
1232	t	70391441159	RAFAEL.CPIRES	RAFAEL CARDOSO PIRES
1233	t	00774549521	GESIELE.MOREIRA	GESIELE DA ROCHA MOREIRA
1234	t	93284209115	LEANDRO.PORTO	LEANDRO CAMILO PORTO
1235	t	02536458164	FRANZ.GREGORIO	FRANZ FRIEDRICH DA CONCEICAO GREGORIO DA SILVA
1236	t	61605786349	FABIANA.STORRES	FABIANA DE SOUSA TORRES
1237	t	48963577368	LEIDA.SMENDES	LEIDA SORAYA MENDES PEREIRA
1238	t	99287072353	cleyphane.asantos	CLEYPHANE AGUIAR DOS SANTOS
1239	t	09982877682	EDSON.SSILVA	EDSON DE SOUSA SILVA
1240	t	97382540197	VIVIANE.BDIAS	VIVIANE BORGES DIAS
1241	t	73579521187	VANEIDE.BDIAS	VANEIDE BASTOS DIAS
1242	t	57550727287	LENILSON.UASSACA	LENILSON BARBOSA UASSACA
1243	t	70271202165	FRANCIELY.ASANTOS	FRANCIELY AUGUSTO DOS SANTOS
1244	t	05215388156	CLARIANA.LCOSTA	CLARIANA.LCOSTA
1245	t	70591445107	ANDRESSA.RXAVIER	ANDRESSA RIBEIRO XAVIER
1246	t	90991150163	GISELLE.CSILVA	GISELLE CRISTINA DA SILVA ARRUDA
1247	t	06293229614	DANILLO.MIGUEL	DANILLO LEITE MIGUEL
1248	t	05268356135	GRACIELE.SILVA	GRACIELE TRINDADE DA SILVA
1249	t	01583434178	CAMILA.CSIMAO	CAMILA CRISTINA DE A SIMAO
1250	t	01650650108	FABIANO.PMOURA	FABIANO DA PENHA MOURA
1251	t	02646671156	CRISTIANE.LIMA	CRISTIANE SILVA DE LIMA SOUZA
1252	t	02458249108	RONALDO.GRIOS	RONALDO ALVES GAMA RIOS
1253	t	92262449104	WEBER.SNOBREGA	WEBER SILVANO DA NOBREGA
1254	t	03854100183	JESSICA.FSANTOS	ANA JESSICA FERREIRA DOS SANTOS
1255	t	70141879106	LUANA.VSILVA	LUANA VIEIRA DA SILVA
1256	t	00760821135	DANIEL.LBATISTA	DANIEL LACERDA BATISTA
1257	t	03058094159	MIRELLE.CALMEIDA	MIRELLE CHIARA FALEIROS ALMEIDA
1258	t	98608088134	CLEISON.JFERNANDES	CLEISON JOSE FERNANDES
1259	t	95076450125	FERNANDO.DARAUJO	FERNANDO DIAS DE ARAUJO
1260	t	89201817134	ALEXANDRE.JOLIVEIRA	ALEXANDRE JORGE VIEIRA DE OLIVEIRA
1261	t	01703648102	LUIZ.FNASCIMENTO	LUIZ FERNANDO SILVA DO NASCIMENTO
1262	t	00810454173	RODRIGO.CJORGE	RODRIGO COSTA DIAS JORGE
1263	t	76607143687	MARCELO.LMENDES	MARCELO LUCIO MENDES
1264	t	71094210153	CRISTIANE.GSOUZA	CRISTIANE GUIMARAES SOUZA
1265	t	03761654138	JESSICA.AMACHADO	JESSICA APARECIDA MACHADO
1266	t	46182420197	CACIA.PSILVA	CACIA REGINA PEREIRA DA SILVA
1267	t	28138775300	SILVIA.CREIS	SILVIA CRISTINA REIS DA COSTA
1268	t	70055942156	ANA.MGODOI	ANA CAROLINNY MENDES GODOI
1269	t	85566217168	ADRIANA.NMOREIRA	ADRIANA NERY MOREIRA
1270	t	00318161184	ELAYNE.TMENESES	ELAYNE WALMERCIA COSTA TELES DE MENESES
1271	t	04223676300	WANDERLEY.TSILVA	WANDERLEY TAVARES SILVA
1272	t	00806566507	ROSELEIA.MMOITINHO	ROSELEIA MOREIRA MOITINHO
1273	t	05456697319	ELIVALDO.CFONSECA	ELIVALDO DE CARVALHO FONSECA
1274	t	05371696369	JOYCE.SCAMPOS	JOYCE SALGADO CAMPOS
1275	t	02746889137	RODRIGO.SALES	RODRIGO SILVA SALES
1276	t	00629734100	ALYLLIO.BBORGES	ALYLLIO ALVES BATISTA
1277	t	02743659106	TATIANE.MASSIS	TATIANE.MASSIS
1278	t	04230251122	FABIANE.BJESUS	FABIANE.BJESUS
1279	t	39687333120	CARLOS.NSOUZA	CARLOS ALBERTO NERES DE SOUZA
1280	t	43416470168	DAVID.FPINTO	DAVID FRANCISCO PINTO
1281	t	02315234166	ANA.MCAMPOS	ANA FLAVIA MIRANDA DE CAMPOS
1282	t	00871606178	FABIO.LMARTINUCI	FABIO LEVY MARTINUCI
1283	t	01237258103	RONALDO.MJUNIOR	RONALDO RIBEIRO DE MAGALHAES JUNIOR
1284	t	94992185134	KATIUSCIA.PGOMES	KATIUSCIA.PGOMES
1285	t	70360519148	MARIA.RJESUS	MARIA.RJESUS
1286	t	04000867199	RONILSON.CARVALHO	RONILSON DE CARVALHO GANDA
1287	t	02812457112	JONATHAN.CAMARGO	JONATHAN COSTA CAMARGO
1288	t	03350579183	JONATHAN.OLIVEIRA	JONATHAN CASTRO  OLIVEIRA
1289	t	06096415300	LEONARDO.DSILVA	LEONARDO CARVALHO DA SILVA
1290	t	02181157170	GAUDIO.CSOUZA	GAUDIO DJAGA CASIMIRO DE SOUZA
1291	t	00983547106	TATIANE.CNASCIMENTO	TATIANE COSTA NASCIMENTO
1292	t	02558551175	ROGER.WCASTRO	ROGER WATERS CASTRO NASCIMENTO
1293	t	03459304103	FILIPE.SOLIVEIRA	FILIPE RODRIGUES SILVA DE OLIVEIRA
1294	t	90202597172	FLAVIO.BFERNANDES	FLAVIO BENTO FERNANDES
1295	t	05563605167	TAINA.CCOSTA	TAINA CRISTINA COSTA DE CARVALHO
1296	t	00518626113	THIAGO.ACARDOSO	THIAGO DE ANDRADE CARDOSO
1297	t	83778578120	DAVI.EDELGADO	DAVI ERIK DELGADO
1298	t	53027230263	ADRIEL.BRAGA	ADRIEL BRASIL BRAGA
1299	t	77215818187	ALEX.PASSIS	ALEX EDUARDO PEREIRA ASSIS
1300	t	04168494152	JULIANNY.FMORAES	JULIANNY FIGUEIREDO DE MORAES
1301	t	70019410344	ELINALDO.RAROUCHE	ELINALDO RABELO AROUCHE
1302	t	04555380347	GILMAR.JLOPES	GILMAR DE JESUS LOPES JUNIOR
1303	t	01116305178	CAMILA.CGOMES	CAMILA CRISTINA ANDRADE GOMES
1304	t	01164757148	ALESSANDRA.EDIAS	ALESSANDRA ELOISA MARTINS DIAS
1305	t	71224963172	ANDERSON.BALVES	ANDERSON BARRETO ALVES
1306	t	01667028111	PAULO.GCOSTA	PAULO GEOVANNY MENDONCA COSTA
1307	t	01159749116	LEONARDO.CSOUZA	LEONARDO CORREIA PEREIRA DE SOUZA
1308	t	03218594138	ANDERSON.SSOUSA	ANDERSON DA SILVA SOUSA
1309	t	09713575601	ANE.CSILVA	ANE CAROLINE DIAS DA SILVA
1310	t	03541686146	CRISTIANA.SMORAIS	CRISTIANA DE SOUSA MORAIS RODRIGUES
1311	t	26821647810	SIDNEY.VVIEGAS	SIDNEY VIANA VIEGAS
1312	t	02964269114	PEDRO.HENRIQUE	PEDRO HENRIQUE DE OLIVEIRA ALCANTARA
1313	t	92714536115	ANTONIO.ACRUZ	ANTONIO.ACRUZ
1314	t	01962565181	JESSICA.BSILVA	JESSICA BRITO DA SILVA
1315	t	04159919197	BIANCA.GPIMENTA	BIANCA GOES PIMENTA
1316	t	03747222188	HERLANDIA.RMORAIS	HERLANDIA ANGRA DOS REIS MORAIS
1317	t	02985737257	FABIANA.FERNANDES	FABIANA ANDRESA PALHETA FERNANDES
1318	t	03141612102	GUILHERME.FTEIXEIRA	GUILHERME FRANCA TEIXEIRA
1319	t	53293029191	RUY.FERNANDO	RUY FERNANDO MOREIRA SANTANA
1320	t	04642450106	CARLOS.ECOTRIM	CARLOS EDUARDO COTRIM
1321	t	85291919191	WEBERSON.CCARDOSO	WEBERSON CLAUDIO CARDOSO
1322	t	72660414149	SILVIO.PPORTO	SILVIO BRUNO PEREIRA PORTO
1323	t	02320662138	JEFFERSON.SOARES	JEFFERSON LIMA SOARES DE VIVEIROS
1324	t	69835772134	DIEGO.CFERNANDES	DIEGO CERQUEIRA FERNANDES
1325	t	03643296100	RUBIANE.AOLIVEIRA	RUBIANE ALVES DE OLIVEIRA
1326	t	17574730253	MARCO.PINHO	MARCO POLO RANGEL DE PINHO
1327	t	11337076619	RENATO.DINIZ	RENATO VALERIO DINIZ
1328	t	22622748191	WELINGTON.BBULHOES	WELINGTON BOMFIM BULHOES
1329	t	04128827606	WYLBER.MSILVA	WYLBER MANUEL DA SILVA
1330	t	01177632179	DANIELA.AMEDEIROS	DANIELA ARAUJO DE OLIVEIRA MEDEIROS
1331	t	16087054120	EMILIO.FILHO	EMILIO HANUM FILHO
1332	t	00343291100	JORGE.FANJOS	JORGE RICARDO FERNANDES FRANCISCO DOS ANJOS
1333	t	05010765144	FABIO.HSOUZA	FABIO HENRIQUE RODRIGUES DE SOUZA
1334	t	72808500653	MARCIA.RHUBAIDE	MARCIA REZENDE HUBAIDE
1335	t	46756779320	FRANCISCO.SANTOS	FRANCISCO CLAUDIO PEREIRA DOS SANTOS
1336	t	57553831204	MARCIO.SVIEIRA	MARCIO DA SILVA VIEIRA
1337	t	03323157117	BRUNNA.FQUILIAO	BRUNNA FERREIRA QUILIAO
1338	t	12118017642	FABRICIO.LEMES	FABRICIO DA SILVA LEMES
1339	t	36049590630	MARCIO.TOSTES	MARCIO DE MORAES TOSTES
1340	t	05531710623	ANDRE.LPEREIRA	ANDRE LUIZ PEREIRA
1341	t	68325550244	AMARILDO.DANTAS	AMARILDO DE SOUZA DANTAS
1342	t	09213251696	VICTOR.SILVA	VICTOR PEREIRA DA SILVA
1343	t	01173762132	DIOGO.ROCHA	DIOGO MARCELINO ROCHA
1344	t	03615888103	MARCOS.ANDRADE	MARCOS VINICIOS LIMA DE ANDRADE
1345	t	02951016107	ARISLAINE.MTEODORO	ARISLAINNE MARINHO TEODORO GONCALVES
1346	t	24208914220	CONCEICAO.ACANDIDO	CONCEICAO APARECIDA CANDIDO
1347	t	92837638153	DEVERSON.LPONTES	DEVERSON LUIZ PONTES
1348	t	05820155963	ERIKA.DSILVA	ERIKA DIONE DA SILVA
1349	t	01178095193	ANTONIO.SILVA	ANTONIO WESLEY DA SILVA
1350	t	97207292104	ANDREIA.CANDIDA	ANDREIA CANDIDA DA COSTA
1351	t	75784238191	CAIRO.CFILHO	CAIRO DIVINO CANDIDO FILHO
1352	t	01533113637	THIAGO.FREITAS	THIAGO DE FREITAS CECCON
1353	t	91663970149	LEONARDO.JCAETANO	LEONARDO JOSE CAETANO
1354	t	38596431187	FRANCISCO.PAULO	FRANCISCO DE OLIVEIRA PAULO
1355	t	00225200155	MOAS.RPEREIRA	MOAS RUITER PEREIRA CIRILO
1356	t	70067313132	IRON.SJUNIOR	IRON SOUSA DA SILVA JUNIOR
1357	t	01783260114	BRUNA.GRAPOSO	BRUNA GOMES RAPOSO
1358	t	72186755149	KAREN.PSANTOS	KAREN PRISCILA  RODRIGUES DOS SANTOS
1359	t	70307135160	BARBARA.GBESSA	BARBARA GABRIELLY COSTA BESSA
1360	t	02374451151	DEIVIDY.PBERNARDES	DEIVIDY PEREIRA BERNARDES
1361	t	04205870124	YANN.TSILVA	YANN THIAGO DA SILVA ALVES
1362	t	87419793115	ANGELINA.MMOURO	ANGELINA MARIA MOURO
1363	t	01250409381	WALDICEIA.LOPES	WALDICEIA COSTA LOPES
1364	t	01150602198	RODRIGO.BOLIVEIRA	RODRIGO.BOLIVEIRA
1365	t	01819212190	ANIBAL.SJUNIOR	ANIBAL VIEIRA DA SILVA JUNIOR
1366	t	56462344172	JULIANO.FMORAIS	JULIANO FERREIRA DE MORAIS
1367	t	79722300130	EMERSON.BBRITO	EMERSON BARBOSA BRITO
1368	t	02840599198	LARA.CALMEIDA	LARA CAMILA ALMEIDA
1369	t	09258969693	BRUNO.GGUIMARAES	BRUNO.GGUIMARAES
1370	t	08519507603	MARCUS.VFELICIANO	MARCUS VINICIUS RODRIGUES FELICIANO
1371	t	90005201268	GESSICA.CMIRANDA	GESSICA CARVALHO MIRANDA
1372	t	03978813122	RAFAEL.OLEMOS	RAFAEL DE OLIVEIRA LEMOS
1373	t	02283128170	JHONATAN.MSILVA	JHONATAN HENRIQUE MAGALHAES SILVA
1374	t	33509441168	CARLOS.CSOUSA	CARLOS CLEMENTINO DE SOUSA
1375	t	04997567116	VICTOR.MBARDUCCO	VICTOR MOSER BARDUCCO
1376	t	02876436116	DOUGLAS.SMOREIRA	DOUGLAS DA SILVA MOREIRA
1377	t	00039565122	CRISTIANO.SANTANA	CRISTIANO PEREIRA SANTANA
1378	t	01706459165	EMILIO.MOREIRA	EMILIO LUIZ MOREIRA
1379	t	46939628134	TESTE.ITALO	TESTE.ITALO
1380	t	76370275115	VANILDA.PFERREIRA	VANILDA PINTO FERREIRA
1381	t	04089809150	ALLEX.AFERNANDES	ALLEX ARAUJO FERNANDES
1382	t	01020426160	LUCAS.PEGORARO	LUCAS LACERDA PEGORARO
1383	t	04981058144	RUAN.LFARIA	RUAN LOPES FARIA
1384	t	02659728178	RAFAEL.SCAVALVANTI	RAFAEL ALFREDO GUIMARAES SALLES CAVALVANTI
1385	t	24847119649	ANTONIO.CMOURA	ANTONIO CARLOS DIAS MOURA
1386	t	05009493101	BRUNO.CSOUZA	BRUNO CESAR FERREIRA DE SOUZA
1387	t	00823845176	LEONARDO.ALVES	LEONARDO ALVES DE OLIVEIRA
1388	t	69997500172	ARLEY.DBATISTA	ARLEY DAWIS BATISTA OLIVEIRA
1389	t	96411538204	ANDRE.CSILVA	ANDRE CARLOS MORAIS SILVA
1390	t	04500846638	ERICA.PCASTRO	ERICA PEREIRA DE CASTRO
1391	t	94153493104	WALEX.ROLIVEIRA	WALEX ROSA DE OLIVEIRA
1392	t	04825854630	LILIAN.SCRUZ	LILIAN SANTANA DA CRUZ
1393	t	86927302115	ANA.CCARNEIRO	ANA CLAUDIA CARNEIRO
1394	t	07575508783	DANIEL.GREIS	DANIEL GAMMA REIS
1395	t	75121816391	MARINILDE.JSERRA	MARINILDE DE JESUS SERRA SANTANA
1396	t	01456179128	AMARIO.LNETO	AMARIO LUIS MOURA NETO
1397	t	47216719115	FERNANDO.MBARBOSA	FERNANDO MARQUES BARBOSA
1398	t	63897091291	NADIA.GOMES	NADIA REJANE RODRIGUES GOMES
1399	t	66753635149	ANTONIO.FLAVIO	ANTONIO FLAVIO DE SOUZA
1400	t	06404297698	JOAO.VBRAGA	JOAO VICTOR SILVA BRAGA
1401	t	91793092168	THIAGO.ACASTRO	THIAGO ANTUNES DE CASTRO
1402	t	04399150157	KELVIN.SILVA	KELVIN GOMES MUNIZ SILVA
1403	t	69259160197	ELVIS.CARRILHO	ELVIS ANDERSON DA SILVA CARRILHO
1404	t	05264607176	JAKELINE.COSTA	JAKELINE VIEIRA DA COSTA
1405	t	71419047191	KLEYTON.DUARTE	KLEYTON RUBNEI MAGALHAES DUARTE
1406	t	04549882140	GIOVANNA.SCARVALHO	GIOVANNA SAMPAIO CARVALHO
1407	t	04312416192	THIAGO.AMARAL	THIAGO DE BROCHADO AMARAL
1408	t	26651637134	CARLOS.AUGUSTO	CARLOS AUGUSTO FERREIRA DE SOUZA
1409	t	01724338196	GUILHERME.HCAMARGO	GUILHERME HENRIQUE LOPES CAMARGO
1410	t	71737804115	EDER.EVANGELISTA	EDER EVANGELISTA DA SILVA
1411	t	01617996203	ANA.PMENDES	ANA PAULA ALEXANDRE MENDES
1412	t	63888017220	KELY.CMIRANDA	KELY CRISTINA DA SILVA MIRANDA
1413	t	07335209951	DOUGLAS.LSANTOS	DOUGLAS IZIDORIO LEAL SANTOS
1414	t	72470925134	SUELENA.BPASSOS	SUELENA CRISTINA BARROS DOS PASSOS
1415	t	02738707343	LEONARDO.ABARBOSA	LEONARDO ARAUJO BARBOSA
1416	t	22058653823	FABIO.FARES	FABIO WALID FARES
1417	t	02679049160	LEIDIANY.FSILVA	LEIDIANY FLAVIA DA SILVA
1418	t	70600682102	SARA.CPATRIARCA	SARA CARNEIRO PATRIARCA
1419	t	02320020101	GUTTIERRY.SILVA	GUTTIERRY DO NASCIMENTO SILVA
1420	t	02818017114	RUBIA.NSILVEIRA	RUBIA NALVA CASTRO DA SILVEIRA
1421	t	70767874153	ADILSON.SSANTANA	ADILSON SOUZA SANTANA
1422	t	76721418134	ANDRE.CORDEIRO	ANDRE LUIZ PEREIRA CORDEIRO
1423	t	47205687187	RONIVON.MOTA	RONIVON VIEIRA DA MOTA
1424	t	70479003149	RICARDO.FERREIRA	RICARDO GONCALVES FERREIRA
1425	t	29144680163	DEMESIO.SILVA	DEMESIO SILVA JUNIOR
1426	t	04044160988	RAFAEL.CARAUJO	RAFAEL CALVI ARAUJO
1427	t	96595418215	HATUS.HELMUTH	HATUS HELMUTH DE MACEDO RAUCH
1428	t	82551626234	RENATA.SILVA	RENATA LEITAO SILVA
1429	t	02026733163	WELLINGTON.SARRUDA	WELLINGTON DOS SANTOS ARRUDA
1430	t	79088988153	TARCISIO.ARANTES	TARCISIO JOSE CAVALCANTI ARANTES
1431	t	04483792105	WEMERSON.PAJAU	WEMERSON DE SOUZA PAJAU
1432	t	40271447168	MAURION.LNEVES	MAURION LUCIANO DAS NEVES
1433	t	00800429290	BRENDA.LINHARES	BRENDA FABIOLA DE LIMA LINHARES
1434	t	02291229222	RAFAEL.PEREIRA	RAFAEL BRENO FARIAS PEREIRA
1435	t	87785099120	GUSTAVO.VOLGARINI	GUSTAVO ALEXANDRE VOLGARINI
1436	t	70298092115	ANA.SANTANA	ANA PAULA SANTANA SILVA
1437	t	32672900159	ABIGAIR.PORTO	ABIGAIR FERREIRA CARLOS PORTO
1438	t	01063015197	FABRICIO.MACEDO	FABRICIO DA SILVA MACEDO
1439	t	02203824760	ALESSANDRO.FJUSTINO	ALESSANDRO FRANKLIM JUSTINO
1440	t	07788445606	LUCIO.FERREIRA	LUCIO FLAVIO DO PRADO FERREIRA
1441	t	89991206191	RICARDO.GDUARTE	RICARDO GOMES DUARTE
1442	t	02231613140	RODRIGO.SBRITO	RODRIGO DA SILVA BRITO
1443	t	00814343244	TATIANA.DOSSANTOS	TATIANA EUGENIA PEREIRA DOS SANTOS
1444	t	94096309249	SAULO.SILVA	SAULO ALVES DA SILVA
1445	t	63426188104	BRUNO.RCOELHO	BRUNO RANGEL COELHO
1446	t	00663934184	GEFERSON.CUNHA	GEFERSON PEREIRA DA CUNHA
1447	t	75030292268	ELIZABETH.SIQUEIRA	ELIZABETH DE ALMEIDA SIQUEIRA
1448	t	00146311132	CASSIO.PINHEIRO	CASSIO LAVIERY GONCALVES PINHEIRO
1449	t	66475759104	CLEUBER.CREIS	CLEUBER CARMO DOS REIS
1450	t	92609465220	MEIRIANGELA.VCOSTA	MEIRIANGELA VIEIRA COSTA
1451	t	72339780187	RODRIGO.MOTA	RODRIGO MIRANDA MOTA
1452	t	02469010128	DAVID.CBIZERRA	DAVID DE CARVALHO BIZERRA
1453	t	02035555116	ANA.GONTIJO	ANA PAULA GONTIJO LOPES SANTOS
1454	t	80551823100	CAIO.SLISBOA	CAIO MARCIO SILVA E LISBOA
1455	t	70128003138	ANA.CLARA	ANA CLARA SANTOS CUNHA
1456	t	59806940130	SALVIANO.JESUS	SALVIANO CANDIDO DE JESUS
1457	t	93008473253	SAMARA.SSILVA	SAMARA SANTOS DA SILVA
1458	t	01283330148	JANAINA.CARVALHO	JANAINA MARIA DE CARVALHO
1459	t	03409809155	DOUGLAS.GSILVA	DOUGLAS GRUBER SILVA
1460	t	02904207171	LUCAS.FSILVA	LUCAS FILIPE PEREIRA DA SILVA
1461	t	86503375204	ALEX.SANTO	ALEX SANDRO DA SILVA DO ESPIRITO SANTO
1462	t	00698878108	ALEXANDRE.SOLIVEIRA	ALEXANDRE DA SILVA DE OLIVEIRA
1463	t	96993057115	TATTIANY.MGONZAGA	TATTIANY MAYSSE GOMES ROSA GONZAGA
1464	t	02606848136	NICOLAS.ZENNI	NICOLAS ZENNI
1465	t	56597932315	ALDIR.SILVA	ALDIR ALVES DA SILVA
1466	t	96056525104	GELCI.SALVIANO	GELCI AREND SALVIANO
1467	t	83290478904	EDENILZA.MLEITE	EDENILZA MARIA  RODRIGUES LEITE
1468	t	87546876168	SEJANE.ALVES	SEJANE ALVES DE JESUS
1469	t	50008056153	ADARLAN.SILVA	ALDARLAN FERNANDES DA SILVA
1470	t	89381238391	GERLUCE.COSTA	GERLUCE BARROS COSTA
1471	t	03974699180	ARISTELLA.LOLIVEIRA	ARISTELLA KAYENA LEQUE DE OLIVEIRA
1472	t	01362591114	LAYNER.PEREIRA	LAYNER HANNYERE PEREIRA DAS GRACAS
1473	t	03620634190	PAULO.TSERPA	PAULO TIAGO GONCALVES SERPA
1474	t	03882991186	NAIARA.PSILVA	NAIARA CRISTINA PEREIRA SILVA
1475	t	94900540200	VALDEMAR.VDANTAS	VALDEMAR VALENTIM DANTAS
1476	t	38338294861	ANDRE.SASAKI	ANDRE SALES SASAKI
1477	t	02390897584	EDUARDO.SOLIVEIRA	EDUARDO SENA DE OLIVEIRA
1478	t	00283429330	GILVAN.JUNIOR	GILVAN AZEVEDO CARVALHO JUNIOR
1479	t	83568069172	GEORGTON.CBATISTA	GEORGTON CESAR BATISTA
1480	t	93702752153	MARLEIDE.RCARVALHO	MARLEIDE ROCHA DE CARVALHO
1481	t	08504690660	FELIPE.MCOSME	FELIPE MOREIRA BRANDAO MARQUES COSME
1482	t	01999454111	DAVID.MRAMALHO	DAVID EDSON MACEDO RAMALHO
1483	t	04459898152	PRISCILA.ROCKENBACH	PRISCILA BERNARDI ROCKENBACH
1484	t	70136794106	ERIC.CCOSTA	ERIC CASSIMIRO GOMES COSTA
1485	t	09438338683	GUILHERME.RBARBOSA	GUILHERME RODRIGUES BARBOSA
1486	t	01727236122	MARCOS.RODRIGUES	MARCOS ANTONIO PEREIRA RODRIGUES
1487	t	02047892147	NAYARA.GARCIA	NAYARA SAMILLA DE SOUZA GARCIA OLIVEIRA
1488	t	96210826172	CELIA.ESOUZA	CELIA EMIDIA DE SOUZA
1489	t	04946770151	JANAINA.VALIM	JANAINA APARECIDA VALIM
1490	t	00543368130	MARCELO.FERREIRA	MARCELO ROCHA FERREIRA
1491	t	00227632109	DANIELLE.RSOUSA	DANIELLE REGINA SEABRA DA SILVA SOUSA
1492	t	58960520144	MARCELO.CARVALHO	MARCELO CARVALHO DA COSTA
1493	t	04539947176	LAIS.SSANTOS	LAIS SIQUEIRA SANTOS
1494	t	02400950148	YANA.RVALADARES	YANA ROSA VALADARES
1495	t	43854893191	AGUINALDO.MAIA	AGUINALDO MARTINS MAIA
1496	t	75123720197	WANESSA.VINHAL	WANESSA VINHAL GOMES
1497	t	57894272191	CRISTIANO.MACHADO	CRISTIANO MACHADO RAMOS
1498	t	02809513104	ICARO.MFERREIRA	ICARO MATHEUS DE OLIVEIRA FERREIRA
1499	t	26648296100	JOEL.SJUNIOR	JOEL DE SOUSA JUNIOR
1500	t	04703379155	LUCAS.FDEUS	LUCAS HENRIQUE FARIA DE DEUS
1501	t	71687122172	MARCOS.AFERREIRA	MARCOS AURELIO DE SOUZA FERREIRA
1502	t	03603641108	MARIANA.GABRIELA	MARIANA GABRIELA DA SILVA LUCIANO
1503	t	73707740153	JHENY.DIONIS	JHENY FERREIRA DIONIS
1504	t	04626432395	MATHEUS.VVALE	MATHEUS VINICIUS MACHADO VALE
1505	t	65293240382	CLEDINALDO.SILVA	CLEDINALDO ALVES DA SILVA
1506	t	61938513304	ANDREIA.SOUSA	ANDREIA ALVES DE SOUSA
1507	t	03121647180	DENYSE.CARMO	DENYSE LIGORIO DO CARMO
1508	t	34344200888	DHYEGO.LTORRUBIA	DHYEGO LUIZ TORRUBIA
1509	t	04672556107	MICHELLY.FERREIRA	MICHELLY FERNANDA FERREIRA
1510	t	77576306149	CLEBER.SANTOS	CLEBER SOARES DOS SANTOS
1511	t	41995236691	EDSON.SILVA	EDSON BARNER DA SILVA
1512	t	99581086234	ERIC.NARAGAO	ERIC NAVA SANTOS ARAGAO
1513	t	97450880130	RODRIGO.OARAUJO	RODRIGO OLIVEIRA DE ARAUJO
1514	t	05506160984	RAQUEL.PLINDNER	RAQUEL PEREIRA LINDNER
1515	t	05124708123	ANA.MENICHINI	ANA PAULA ANANIAS MENICHINI
1516	t	00849280184	THIAGO.SALVES	THIAGO SILVA ALVES DE ABREU
1517	t	73103330120	CRISTHIAN.ARAUJO	CRISTHIAN AUGUSTO BARBOSA ARAUJO
1518	t	03368616137	KATIA.MSANTANA	KATIA DE MENEZES SANTANA
1519	t	71359028153	anderson.falmeida	ANDERSON CLAYTON FERREIRA DE ALMEIDA
1520	t	00527988103	joseph.bezerra	JOSEPH FAUSTINO BEZERRA
1521	t	02452799114	carlos.aandrade	CARLOS ANDRE DE ANDRADE
1522	t	01010776100	GLEIDSTONE.SILVA	GLEIDSTONE DA SILVA ESPINDOLA
1523	t	01047686147	ELAINE.MATTA	ELAINE CRISTINA DA COSTA MATTA
1524	t	42237653291	ANDRE.HMARQUES	ANDRE HONORY RABELO MARQUES
1525	t	01990714110	ANA.RABELO	ANA CAROLINA SEGURADO RABELO
1526	t	57296464100	ROGER.GFURTADO	ROGER MAURICIO GUIMARAES FURTADO
1527	t	01849909121	JOYCE.CINTRA	JOYCE CINTRA BARBARA
1528	t	01808031156	PAULO.RICARDO	PAULO RICARDO DE JESUS FERNANDES
1529	t	82297371187	JULIANAA.PEREIRA	MICHELLE JULIANA ALVES PEREIRA
1530	t	51538458691	MARLI.RODRIGUES	MARLI APARECIDA RODRIGUES
1531	t	71307176100	ROGERIO.SCAMARGO	ROGERIO DA SILVA CAMARGO
1532	t	64008932291	CLAUDIA.LOLIVEIRA	CLAUDIA LIMA VIANA DE OLIVEIRA
1533	t	89555457115	FERNANDO.BJUNIOR	FERNANDO BRASILEIRO DE OLIVEIRA JUNIOR
1534	t	01694777111	FREDERICO.PALBERNAZ	FREDERICO DE PAULA ALBERNAZ
1535	t	70347263100	KARLA.VSOUZA	KARLA VERONICA DE SOUZA FERREIRA
1536	t	02592757198	THAIS.ASILVA	THAIS ALVES SILVA
1537	t	81345135149	maria.acamelo	MARIA DA LUZ ANTONIA CAMELO
1538	t	01265355150	ANACLAUDIA.OLIVEIRA	ANA CLAUDIA VARGAS DE OLIVEIRA
1539	t	01901189112	STELA.ARODRIGUES	STELA ANIZIA RODRIGUES ALVES
1540	t	37545035100	SERGIO.PSOUSA	SERGIO PAULO DE SOUSA
1541	t	99300559168	MARCO.ASOUSA	MARCO AURELIO DE SOUSA LIMA AMORIM
1542	t	03589032111	FERNANDO.AFERREIRA	FERNANDO ARANTES FERREIRA
1543	t	02174450127	SAMUEL.LSOUSA	SAMUEL LOPES SOUSA
1544	t	72133848134	JOAOP.SILVA	JOAO PAULO DOS SANTOS SILVA
1545	t	75297914191	LUCAS.CARLOS	LUCAS CARLOS TEIXEIRA PIRES
1546	t	62108280278	KATIA.SILVA	KATIA SOARES DA SILVA
1547	t	01125216140	HUDSON.MATOS	HUDSON SOUSA MATOS
1548	t	75628899100	ALYSSA.MFERREIRA	ALYSSA MACIEL FERREIRA
1549	t	72745096168	WANESSA.MLOURENCO	WANESSA DE BRITO MACHADO LOURENCO
1550	t	88100286191	MARCELO.RSOARES	MARCELO HENRIQUE RODRIGUES SOARES
1551	t	81080999191	UBIRAJARA.EDIAS	UBIRAJARA EDUARDO DIAS DE LIMA
1552	t	91439205272	FABIANA.MARTINS	FABIANA DOS SANTOS MARTINS
1553	t	05676823151	GABRIEL.COSTA	GABRIEL MARCOS COSTA FERNANDES DE SOUZA
1554	t	33942789876	ALINY.MSILVA	ALINY MARTINS SILVA
1555	t	03027484150	ANDRESSA.AJESUS	ANDRESSA ALMEIDA JESUS
1556	t	66979730134	SUELEN.SNUNES	SUELEN SILVA NUNES
1557	t	06932490641	EDILSON.SBORGES	EDILSON SILVA BORGES
1558	t	21898023859	UBIRAJARA.NETO	UBIRAJARA AMARAL NETO
1559	t	02640318144	LUCIVANIA.GONCALVES	LUCIVANIA DE MATOS GONCALVES
1560	t	38386445149	ALAIM.RSANTOS	ALAIM RODRIGUES DOS SANTOS
1561	t	00606975128	EDINALDO.GOLIVEIRA	EDINALDO GONCALVES DE OLIVEIRA
1562	t	81631049100	LUCIA.CASTANHEIRA	LUCIA REGINA WCHOA CASTANHEIRA PAPALARDO
1563	t	02840996197	CLEYSSON.RALVES	CLEYSSON RICARDO DO PRADO ALVES
1564	t	73376809100	MICHEL.MSKORUPA	MICHEL MACHADO SKORUPA
1565	t	03081743133	ARIELLEN.LIMA	ARIELLEN DE LIMA CASAGRANDE SANTANA
1566	t	06429435164	STEFANY.CSILVA	STEFANY.CSILVA
1567	t	73541362120	rivelino.asilva	RIVELINO ARAUJO SILVA
1568	t	00920314120	FERNANDA.ALMEIDA	FERNANDA NUNES DE ALMEIDA
1569	t	06275039698	GEONAN.ASILVA	GEONAN ALVES DA SILVA
1570	t	01857669193	VIVIANE.RSOUZA	VIVIANE RIBEIRO DE SOUZA
1571	t	72796200191	DAIANA.JORLIVEIRA	DAIANA JARDIM OLIVEIRA
1572	t	03362940188	WEUBER.BORGES	WEUBER BORGES DE BASTOS
1573	t	01126744107	LORENA.PCRUZ	LORENA PEREIRA CRUZ
1574	t	02364097193	JOAO.PSANTOS	JOAO PAULO BATISTA DOS SANTOS
1575	t	01837957177	DAYANE.RSOUSA	DAYANE RANGEL DE SOUSA
1576	t	01629593117	SILVIO.SJUNIOR	SILVIO DA SILVA E SOUZA JUNIOR
1577	t	03695487364	thaisa.omourthe	thaisa.omourthe
1578	t	39727785115	SIDNEI.RODRIGUES	SIDNEI DE OLIVEIRA RODRIGUES
1579	t	01904228151	AUGUSTO.MDUARTE	AUGUSTO MARTINS DUARTE
1580	t	02319887105	ALLISSON.JMOREIRA	ALLISSON JOSE MOREIRA
1581	t	04617882170	JESSICA.CLIMA	JESSICA CAROLINE SANTIAGO DE LIMA
1582	t	01226759165	JULIO.FERNANDES	JULIO CESAR SILVA FERNANDES
1583	t	73990078291	HELEN.JESUS	HELEN CRISTINA DE JESUS
1584	t	01925168174	thomas.almeida	THOMAS FELISBINO ALMEIDA
1585	t	01661585183	caio.ccosta	CAIO CESAR COSTA ARANTES
1586	t	02135780502	HELDER.CANUTO	HELDER.CANUTO
1587	t	00618587144	DIEGO.HVIEIRA	DIEGO HENRIQUE VIEIRA MARQUES
1588	t	03773184182	MARCELO.AGODOI	MARCELO ALMEIDA DE GODOI
1589	t	91339880130	FABIANO.CSANTOS	FABIANO CARVALHO DOS SANTOS
1590	t	73733555104	BRUNA.MALVES	BRUNA KAROLINA MOURA ALVES
1591	t	92845134134	FABIO.ASIQUEIRA	FABIO DO AMPARO SIQUEIRA
1592	t	98155440125	CARLOS.ALBERTOSILVA	CARLOS ALBERTO DA SILVA
1593	t	67802257204	CARLA.LARAUJO	CARLA LUCIANA DE ARAÚJO LIMA MARINHO
1594	t	73075035149	WANESSA.MENDES	WANESSA MOREIRA MENDES
1595	t	07162576613	RICARDO.SILVA	RICARDO ALEXANDRE DA SILVA
1596	t	65220846191	ELIZEU.LEITE	ELIZEU FERNANDES LEITE
1597	t	72739169191	ALESSANDRA.MARTINS	ALESSANDRA MARTINS DOS SANTOS
1598	t	02628778130	NYLLO.ADELFINO	NYLLO ALLES CESAR DELFINO
1599	t	03449765302	LUCAS.GSOUSA	LUCAS GARROS SOUSA
1600	t	99548119153	LARA.RSANTANA	LARA RODRIGUES SANTANA
1601	t	03579995170	NATALIA.RSILVA	NATALIA RODRIGUES DA SILVA
1602	t	01763459144	NAYARA.RBORGES	NAYARA RODRIGUES BORGES
1603	t	01278263195	GUILHERME.BATISTA	GUILHERME BATISTA DA SILVA
1604	t	60215259149	WEBER.SANTOS	WEBER ADRYANO SILVA SANTOS
1605	t	03991319144	ADRIANA.GCOSTA	ADRIANA GONCALVES DA COSTA
1606	t	02439799165	SARAH.COLIVEIRA	SARAH CAROLINE DE OLIVEIRA
1607	t	05353382102	ANDRESSA.LIMA	ANDRESSA DA SILVA MARQUES
1608	t	03441420195	CAYO.HRIBEIRO	CAYO HENRIQUE RIBEIRO CARDOSO
1609	t	99847990263	CRISTINA.RRODRIGUES	CRISTINA RIBEIRO RODRIGUES
1610	t	01044492171	ITALO.GMARIANO	ITALO GUSTAVO DE OLIVEIRA MARIANO
1611	t	10577542699	LEANDRO.FERREIRA	LEANDRO GUIMARAES FERREIRA
1612	t	68639732272	EDSON.LSILVA	EDSON LINS DA SILVA JUNIOR
1613	t	01149923121	leandro.ssantos	LEANDRO SILVA SANTOS
1614	t	72350830187	ALEX.DSANTOS	ALEX ALVES DOS SANTOS
1615	t	02582519138	BRUNA.DAMASCENO	BRUNA PEREIRA DAMASCENO
1616	t	02551277159	PAULO.HNEIVA	PAULO HENRIQUE ABREU NEIVA
1617	t	02102055124	THAIS.JPIMENTA	THAIS DE JESUS PIMENTA
1618	t	04306524108	LUCIANO.AFERREIRA	LUCIANO ALVES SILVA FERREIRA
1619	t	02075798141	DIEGO.SCONCEICAO	DIEGO ROBSON SANTANNA DA CONCEICAO
1620	t	04314767138	MARCIA.SRODRIGUES	MARCIA DE SOUZA RODRIGUES
1621	t	05066180303	RAUL.PFERREIRA	RAUL PEREIRA FERREIRA
1622	t	86533517104	MARCIO.HOSOKAWA	MARCIO JOSE SILVERIO HOSOKAWA
1623	t	33374422187	JOAO.BSILVA	JOAO BATISTA BARRETO DA SILVA
1624	t	01068537167	ELVIS.CHIOVATO	ELVIS CANDIDO CHIOVATO
1625	t	10602915686	RAFAELLA.LSANTANA	RAFAELLA LEAL SANTANA
1626	t	70689148100	LEANDRO.SOUSA	LEANDRO BASSI DE SOUSA
1627	t	03749653151	JOYCE.RSILVA	JOYCE RIBEIRO SILVA
1628	t	96266988100	ANTONIO.CNASCIMENTO	ANTONIO CARLOS DO NASCIMENTO
1629	t	03731750198	MARIANA.GOMES	MARIANA GOMES DE ANDRADE
1630	t	03444908156	LARISSA.MMEDEIROS	LARISSA MAGALHAES MEDEIROS
1631	t	03008696147	WANESSA.CCOSTA	WANESSA CHRISTINA CARDOSO DA COSTA
1632	t	01079399160	PAULO.HMIRANDA	PAULO HENRIQUE MIRANDA DE OLIVEIRA
1633	t	74994751153	THIAGO.FBARBOSA	THIAGO FELIPE BARCELOS BARBOSA
1634	t	00536939144	ELISVALDO.SSILVA	ELISVALDO.SSILVA
1635	t	03413271156	KLIVIA.SDIAS	KLIVIA.SDIAS
1636	t	02118749228	MATHEUS.SILVA	MATHEUS AMORIM OLIVEIRA DA SILVA
1637	t	51319659187	CLEIDE.SBOAVENTURA	CLEIDE SILVA BOAVENTURA
1638	t	70155038168	BRUNO.CDIAS	BRUNO FERNANDES CASSIANO DIAS
1639	t	97153044191	ATAIR.CMAGALHAES	ATAIR CRISTINE DE NAZARETH MAGALHAES
1640	t	69795061100	MARCELO.CAMARGO	MARCELO CAMARGO
1641	t	71097953220	ANDREIA.ALENCAR	ANDREIA SOUSA ALENCAR
1642	t	83252053187	LILIANE.EROEDER	LILIANE ELAINE ROEDER
1643	t	62832468187	GELL.MOLIVEIRA	GELL MIREZ MARQUES DE OLIVEIRA
1644	t	87543842149	RODRIGO.PORTES	RODRIGO FONSECA PORTES
1645	t	35525185172	LEONIDIA.COLIVEIRA	LEONIDIA CAMPOS DE OLIVEIRA
1646	t	05144143326	ANDRESSA.CARVALHO	ANDRESSA DE ALMEIDA CARVALHO
1647	t	54922909168	DIONE.ALVES	DIONE ALVES DA SILVA
1648	t	76950085672	FABIO.GSILVA	FABIO GONCALVES DA SILVA
1649	t	86020480178	marcos.dsilva	MARCOS DANTAS DA SILVA
1650	t	66553121168	EURIPEDES.MRUFINO	EURIPEDES MARCELO RUFINO DA SILVA
1651	t	73649279134	RENATO.ARIBEIRO	RENATO ARCANJO RIBEIRO
1652	t	12535655624	ADAILTON.RSOARES	ADAILTON RODRIGUES SOARES
1653	t	01804000108	NIVEA.ALVES	NIVIA DA SILVA ALVES
1654	t	02706648155	FELIPE.RSILVA	FELIPE DO REGO SILVA
1655	t	94145873149	LEANDRO.SRIBEIRO	LEANDRO DE SOUSA RIBEIRO
1656	t	03957930170	LORENA.SILVA	LORENA LUCIA DA SILVA
1657	t	73642606172	BRUNO.LUCENA	BRUNO LUCENA SOUSA
1658	t	41356314104	EDSON.FBEZERRA	EDSON FREITAS BEZERRA
1659	t	70114564140	JACQUELINE.DSOUZA	JACQUELINE DOURADO DE SOUZA
1660	t	04121794150	MAYARA.OLIVEIRA	MAYARA GUIMARAES XAVIER  E OLIVEIRA
1661	t	62491091291	VALERIA.PCAMARGO	VALERIA PAES DE CAMARGO
1662	t	98454340106	LUCAS.CSILVA	LUCAS CARVALHO DA SILVA
1663	t	01210273179	MARCIO.BRAGA	MARCIO BRAGA PEREIRA
1664	t	89522478172	CAROLINA.TAQUES	CAROLINA MORO POMPEO TAQUES
1665	t	02942192186	LEONARDO.CSILVA	LEONARDO CAVALCANTE DA SILVA
1666	t	04445265101	ALECSANDER.BGOMES	ALECSANDER EMMANUEL BARBOSA GOMES
1667	t	79130712149	CARLOS.SJUNIOR	CARLOS ALVES SILVA JUNIOR
1668	t	01662196148	SUELEN.LBARBOSA	SUELEN LEMOS BARBOSA
1669	t	03872412132	bruna.cdias	BRUNA HAVENA CAVALCANTE DIAS
1670	t	77845269204	RENATO.ELIMA	RENATO ELY DE LIMA
1671	t	07560692621	WILLIAN.GANDRADE	WILLIAN GOMES DE ANDRADE
1672	t	04337813195	RENAN.ANDRADE	RENAN PABLO NAVARRO ANDRADE
1673	t	70641072104	BRUNO.GUIMARAES	BRUNO FERNANDO DOS SANTOS GUIMARAES
1674	t	27841234805	FRANCISCO.LCASTRO	FRANCISCO LEONARDO DE OLIVEIRA CASTRO
1675	t	82352780144	JOVINO.BNETO	JOVINO BARROS NOGUEIRA NETO
1676	t	16178807104	HELIO.SFREITAS	HELIO SIQUEIRA DE FREITAS
1677	t	04177236101	JESSICA.RAMARO	JESSICA RIBEIRO AMARO
1678	t	03341148701	CRISTIANO.SREZENDE	CRISTIANO.SREZENDE
1679	t	34257793899	ALECIO.BPESSOA	ALECIO DE PAULA BORGES PESSOA
1680	t	06726213980	ANGELA.ABORTOLI	ANGELA APARECIDA BORTOLI CRUZ
1681	t	70750432187	FRANCELY.RMARQUES	FRANCELY RODRIGUES DE OLIVEIRA MARQUES
1682	t	24216984215	KATIA.RPAELO	KATIA REGINA ARAUJO PAELO
1683	t	63097478191	CLAUDIO.JSANTOS	CLAUDIO JOSE DOS SANTOS
1684	t	03594049108	BEATRIZ.FARAUJO	BEATRIZ.FARAUJO
1685	t	72321962100	EDUARDO.CAVALCANTE	EDUARDO DE LIMA CAVALCANTE
1686	t	02345405163	EMERSON.MVIANA	EMERSON MARCIEL VIANA
1687	t	03638201163	VIVIANE.LOLIVEIRA	VIVIANE LIDIA CARVALHO DE OLIVEIRA
1688	t	04229791127	IAGHO.TBORGES	IAGHO TADEU DA SILVA BORGES
1689	t	77133935104	ADILSON.OPINTO	ADILSON OLIVEIRA PINTO
1690	t	85781541187	LUCIANO.MBARBOSA	LUCIANO MARTINS BARBOSA
1691	t	01187378135	BRUNO.FSOUZA	BRUNO FIDELIS DE SOUZA
1692	t	32793408883	patricia.eisenhauer	patricia.eisenhauer
1693	t	99757362115	LUIZ.FFERREIRA	LUIZ FERNANDO SOUZA FERREIRA
1694	t	56094116100	CLAUDIA.AMORAES	CLAUDIA ADRIANA DE MORAES
1695	t	05941254482	MAURILLY.NAVAIS	MAURILLY DJONN DA SILVA NAVAIS
1696	t	97285234168	diogo.mcarvalho	DIOGO MORAES CARVALHO
1697	t	01422714110	NAYARA.GANJOS	NAYARA GOMES DOS ANJOS
1698	t	21826439153	FABIO.CUNHA	FABIO FRANCISCO DA CUNHA
1699	t	00851816177	GERMANO.RSILVA	GERMANO ROMERIO DA SILVA SALES
1700	t	00531673251	WALDIELISON.OLIVEIRA	WALDIELISON BASTOS OLIVEIRA
1701	t	99634996272	KLEIDSON.SOUZA	KLEIDSON PEREIRA DE SOUZA
1702	t	02831044146	LILIA.SOUZA	LILIA FERREIRA DE SOUZA
1703	t	84384794134	ANDERSON.SRIOS	ANDERSON SOARES RIOS
1704	t	03683211180	MARIANA.SROCHA	MARIANA SALUSTIANO ROCHA
1705	t	04399899140	THAIZA.SPINHEIRO	THAIZA VELASCO SANTOS PINHEIRO
1706	t	22602798134	JOAO.SOUZA	JOAO ALVES DE SOUZA
1707	t	99437783349	ALEX.SOUSA	ALEX GOMES DE SOUSA
1708	t	01856869156	HELLEN.LVITAL	HELLEN HEVELLYN LOPES VITAL
1709	t	31788824806	simone.ferreira	SIMONE FERREIRA DE ALMEIDA
1710	t	04066218610	SIMONE.PMARQUES	SIMONE APARECIDA PINTO MARQUES
1711	t	03835037609	ALEXANDRE.MBARBOSA	ALEXANDRE HENRIQUE MACIEL BARBOSA
1712	t	94760640134	CRISTIANE.VSOUZA	CRISTIANE VANUCE DE SOUZA
1713	t	75429080306	FRANCELINO.GONCALVES	FRANCELINO MOREIRA GONCALVES
1714	t	02679601157	SARAH.CSILVA	SARAH CRISTINE FERREIRA DE PAULA SILVA
1715	t	03708123107	ITALO.RPIRIS	ITALO RODRIGUES PIRIS
1716	t	01763010112	FELIPE.MSOUZA	FELIPE MURILO MATOS DE SOUZA
1717	t	70009047123	REGIANE.LSANTOS	REGIANE LOPES DOS SANTOS
1718	t	85282740125	FLAVIO.FARIA	FLAVIO ALMEIDA DE FARIA
1719	t	00118376160	ELANE.MPAZ	ELANE MARIA DA COSTA PAZ
1720	t	71397086149	LEIDIANE.PBARROSO	LEIDIANE GERALDA PINTO BARROSO
1721	t	06047746136	MARCELLA.FLIMA	MARCELLA FLAVIA LIMA MASCHIO AVILEZ
1722	t	93620535191	GISELLE.JCOSTA	GISELLE DE JESUS COSTA
1723	t	03205082184	MARCELA.DSEREJO	MARCELA DEISE SILVA SEREJO
1724	t	02965159142	EFRAIN.TSANTOS	EFRAIN TABORDA SANTOS
1725	t	51032562234	VICTOR.MMURILLO	VICTOR MEDLEG MURILLO
1726	t	03864025150	RENAN.GCINTRA	RENAN DA GAMA CINTRA
1727	t	46722262120	GILBERTO.GPARREIRA	GILBERTO GERALDO PARREIRA
1728	t	00960410139	WELLINGTON.REIS	WELLINGTON CORREA DOS REIS
1729	t	00403945305	FRANCILENE.RALMEIDA	FRANCILENE RODRIGUES DEE ALMEIDA
1730	t	01289545154	MORGANA.GBRITO	MORGANA GUANABARA BRITO TORRES
1731	t	04179619105	EDILANE.SILVA	EDILANE LIMA DA SILVA
1732	t	00311051146	JACQUELINE.LEITE	JACQUELINE LEITE
1733	t	00964288133	MARCIO.SBOENO	MARCIO DE SOUZA BOENO
1734	t	80314643168	VANESSA.MMARIANO	VANESSA MARIA MARIANO
1735	t	03523948118	LETICIA.ASILVA	LETICIA ALVES DA SILVA
1736	t	71572058153	FERNANDA.SPINTO	FERNANDA SILVA PINTO
1737	t	40594041104	CARLOS.FSILVA	CARLOS AUGUSTO FERREIRA DA SILVA
1738	t	91165504120	ADRIANO.STEIXEIRA	ADRIANO DA SILVA TEIXEIRA
1739	t	03422512101	SAMIA.MMIRANDA	SAMIA MACEDO MIRANDA
1740	t	22241946842	RODRIGO.ROCHA	RODRIGO SANTOS ROCHA
1741	t	04929459184	jordana.fmata	JORDANA FERREIRA DA MATA
1742	t	53095995172	EDUARDO.CARVALHO	EDUARDO BORGES DE CARVALHO
1743	t	03351249179	MIRIA.MLEITE	MIRIA MEDEIROS LEITE
1744	t	63157896504	TIAGO.FLEAL	TIAGO FELICIANO LEAL NETO
1745	t	02457077192	CLEITON.RDIAS	CLEITON RODRIGUES DIAS
1746	t	93159447634	CRISTIANA.LRIBEIRO	CRISTINA LUZIA RIBEIRO
1747	t	01890206016	LUIZ.ARODRIGUES	LUIZ ANDRIGO RODRIGUES GONCALVES
1748	t	00900859148	LUZILENE.LIMA	LUZILENE RODRIGUES DE LIMA
1749	t	00014728109	JACIANE.CASTRO	JACIANE BRAGA DE CASTRO
1750	t	00642687137	HENRIQUE.SBRITO	HENRIQUE DA SILVA BRITO
1751	t	02741106100	alexandro.mcarvalho	ALEXANDRO MOREIRA DE CARVALHO
1752	t	01103521306	IGOR.LCARVALHO	IGOR LEONARDO CARVALHO
1753	t	02593286180	MARIA.BRAGA	MARIA APARECIDA VIEIRA BRAGA
1754	t	03840500184	romeu.cgomes	ROMEU CINALLI GOMES
1755	t	05584557196	LARYSSA.ARRUDA	LARYSSA PAULA DA CONCEICAO ARRUDA
1756	t	02405901129	DANIEL.CMOREIRA	DANIEL CHRISOSTOMO PIRES MOREIRA
1757	t	00723093342	GESSIANE.CCARNEIRO	GESSIANE CARNEIRO CUNHA
1758	t	18199712104	LUIZ.FCAMPOS	LUIZ.FCAMPOS
1759	t	01846207320	DEDNA.RDINIZ	DEDNA RAIMUNDA DINIZ CANTANHEDE
1760	t	88270360244	TATIANE.CBRAGA	TATIANE CARVALHO BRAGA
1761	t	79157700125	CLEITON.VSILVA	CLEITON VIEIRA SILVA
1762	t	73249734187	ANA.PSIMI	ANA CLAUDIA PEREIRA XAVIER SIMI
1763	t	01685991122	FABIO.MIYANO	FABIO MIYANO
1764	t	01640915133	TALYTA.LQUEIROZ	TALYTA LOBATO QUEIROZ
1765	t	01212024532	DAURYSEYA.MSILVA	DAURYSEYA MARIA FAGUNDES DA SILVA
1766	t	51280515104	MAURO.SPEREIRA	MAURO.SPEREIRA
1767	t	02821854102	ANNYELLE.SILVA	ANNYELLE MARIA DA SILVA OLIVEIRA
1768	t	72395907200	AGEU.GSANTOS	AGEU GOMES DOS SANTOS
1769	t	00872446166	GUILHERME.SARAUJO	GUILHERME SOBRAL DE ARAUJO
1770	t	05107219152	VINICIUS.SLACERDA	MARCOS VINICIUS DA SILVA LACERDA
1771	t	87706113191	ROSANA.FROCHA	ROSANA FERREIRA DE LIMA ROCHA
1772	t	02223721176	PAULA.GMELO	PAULA GOMES DE MELO
1773	t	96780444200	FRANCIELE.SMARINHO	FRANCIELE DOS SANTOS MARINHO
1774	t	05404942169	HIGOR.LMATA	HIGOR LEONEL DA MATA
1775	t	02741899177	raphael.abarbosa	RAPHAEL AQUILINO BARBOSA DA SILVA
1776	t	03901901108	ANDERSON.FERREIRA	ANDERSON FERREIRA
1777	t	01907293175	JOABE.FSOUZA	JOABE FRANCA FEITOZA
1778	t	03629427146	ANA.CLOUROS	ANA CAROLINY BARBOSA LAURIAS
1779	t	02892036194	ALINNE.ANERY	ALINNE DE ALMEIDA NERY
1780	t	00941987132	FERNANDA.PIOVEZAN	FERNANDA PIOVEZAN FIGUEIREDO SANCHES
1781	t	02131680147	CAIO.CESAR	CAIO CESAR SANTOS DE PAULA
1782	t	83780262134	DJHONATTAS.RSOUZA	DJHONATTAS RODRIGUES DE SOUZA
1783	t	03001816120	DIEGO.FSILVA	DIEGO FELIPE DA SILVA CAMPOS
1784	t	83905553287	JOICIANE.FBELEM	JOICIANE FACANHA BELEM
1785	t	01860912141	ERICA.SCRUZ	ERICA DA SILVA CRUZ
1786	t	05980253165	MATHEUS.MACHADO	MATHEUS ALCANTARA MACHADO
1787	t	64881113100	JOAO.MOLIVEIRA	JOAO MARCIO ALVES DE OLIVEIRA
1788	t	02467357325	REGINALDO.RODRIGUES	REGINALDO DA CONCEICAO RODRIGUES
1789	t	99490129100	TATIENE.GRODRIGUES	TATIENE GEROLINETO RODRIGUES
1790	t	03176907109	JONATHAN.LACERDA	JONATHAN DRUMOND DE AMORIM LACERDA
1791	t	01368979408	DOUGLAS.AMAINIERI	DOUGLAS ARAUJO MAINIERI DA CUNHA PINTO
1792	t	04471748190	MATHEUS.VRIBEIRO	MATHEUS VINICIUS RIBEIRO NUNES
1793	t	70457019182	LUCAS.GRIBEIRO	LUCAS.GRIBEIRO
1794	t	01501616110	EDILSON.AREIS	EDILSON ALVES DOS REIS
1795	t	00720520150	TIAGO.AGOMES	TIAGO ALEIXO GOMES
1796	t	04315742198	LINCOLN.ANGELINO	LINCOLN MATHEUS DIAS ANGELINO
1797	t	03943134105	JEFFERSON.CAMPOS	JEFFERSON ANTUNES DE CAMPOS
1798	t	79653650149	WENDEL.RODRIGUES	WENDEL RODRIGUES LOPES
1799	t	01075078385	SONIA.MPEREIRA	SONIA MARIA MORAES PEREIRA
1800	t	02979677116	RONALDO.AANTONIO	RONALDO ALVES ANTONIO
1801	t	00276860195	CARLOS.MARTINS	CARLOS HENRIQUE MARTINS GUERREIRO
1802	t	04722696152	INDIARA.MATOS	INDIARA DA SILVA MATOS
1803	t	04013225131	FELIPE.MACHADO	FELIPE MARTINS MACHADO
1804	t	01181828112	ELTON.NUNES	ELTON ANDRADE NUNES
1805	t	03635017116	VANESSA.LAVALL	VANESSA LAVALL
1806	t	56945000672	JOSE.WFREITAS	JOSE WISTON DE FREITAS
1807	t	04585190147	PRISCILA.BLIMA	PRISCILA BARBOSA DE LIMA
1808	t	69131759653	RODRIGO.FREITAS	RODRIGO SERGIO PEREIRA DE FREITAS
1809	t	01269700170	JANAINA.RIBEIRO	JANAINA CASSIA RIBEIRO
1810	t	66947510104	MARCIA.CNUNES	MARCIA CRISTINA NUNES DOS SANTOS
1811	t	00324807171	LUIZ.SJUNIOR	LUIZ PAULO DE SOUSA JUNIOR
1812	t	61006852301	GLEUDES.LLIMA	GLEUDES LOBATO LIMA
1813	t	07639143632	ROBERTA.FARAUJO	ROBERTA FERNANDES DE ARAUJO
1814	t	95272771168	MAYKON.PONCE	MAYKON PONCE LEONES SOUZA
1815	t	96492970144	BRUNNO.AFREITAS	BRUNNO ARAUJO DE FREITAS
1816	t	23361100836	TALITA.SOUZA	TALITA DE SOUZA
1817	t	01006726128	LETICIA.SDUARTE	LETICIA SILVA DUARTE
1818	t	79432263215	ELENFRANCE.CSILVA	ELENFRANCE CARDOSO DA SILVA
1819	t	03861366100	MAYARA.RBERNARDES	MAYARA RODRIGUES BERNARDES
1820	t	03675768139	DAVI.JBUENO	DAVI JOSE BUENO
1821	t	00917358155	ANA.ISILVA	ANA PAULA IRIAS DA SILVA
1822	t	00381553116	VERONICA.PIRES	VERONICA FRANCO FRATTARI PIRES
1823	t	91729408168	EDUARDO.MORAES	EDUARDO BARRETO MORAES
1824	t	99360659134	TATIANE.MDEUS	TATIANE MARTINS COSTA DE DEUS
1825	t	56131976104	ROSEMARY.SILVA	ROSEMARY DO NASCIMENTO PEREIRA DA SILVA
1826	t	87644193200	KEDSON.APEREIRA	KEDSON ALAN PEREIRA DIAS
1827	t	89342925120	ROSENVALDO.MFILHO	ROSENVALDO MOREIRA FILHO
1828	t	01276055170	BLENA.CFARIAS	BLENA CAROLINA DA SILVA FARIAS
1829	t	03677239133	CAROLINA.SANTOS	KATILLA CAROLINA SANTOS
1830	t	70211361100	SHEILA.COLIVEIRA	SHEILA DA COSTA OLIVEIRA
1831	t	06502181163	IZADORA.FRODRIGUES	IZADORA DE FREITAS RODRIGUES
1832	t	80621929115	CARLOS.ORIBEIRO	CARLOS LAURIA OLIMPIO RIBEIRO
1833	t	08019699805	JOSE.CORREIA	JOSE FRANCISCO CORREIA
1834	t	85907383100	ANDRE.BORGES	ANDRE BORGES SOUTO
1835	t	04966965136	MARCELLA.MSANATA	MARCELLA BEATRIZ MOREIRA SANTANA
1836	t	01694379140	MARIANA.SILVA	MARIANA DAS DORES SILVA
1837	t	04819244183	THAYLA.MCAMPOS	THAYLA MARCIELLY CAMPOS GONCALVES
1838	t	98517279115	ROSANGELA.SSERGIO	ROSANGELA SANTINA SERGIO PERIN
1839	t	85213837115	ANGELO.ROSSI	ANGELO ROSSI DE ARAUJO
1840	t	03961696195	KEITIANE.ESILVA	KEITIANE ELIZA DA SILVA
1841	t	05190864146	GABYA.MSOARES	GABYA JULIA MENDES SILVA SOARES
1842	t	96516704104	NUBIA.MOREIRA	NUBIA ALVES MOREIRA GUARITA
1843	t	03789706906	ALESSANDRO.CLUZ	ALESSANDRO CAVALCANTE DA LUZ
1844	t	46773312191	CLEUBER.ASILVA	CLEUBER ALVES DA SILVA
1845	t	03424272108	JULIANA.ASILVA	JULIANA ALVES SILVA
1846	t	60604774370	JESSICA.LMELO	JESSICA LIMA MELO
1847	t	39319016100	EDUARDO.ASANTOS	EDUARDO ALVES DOS SANTOS
1848	t	71025960220	ANGELICA.ASOLIZ	ANGELICA AGUILERA SOLIZ
1849	t	01090894139	BARBARA.FERREIRA	BARBARA CAROLINE GOMES FERREIRA
1850	t	72949104134	RENAN.MFERREIRA	RENAN MARQUES FERREIRA
1851	t	02303676150	FERNANDO.LFILHO	FERNANDO ARAUJO DE LIMA FILHO
1852	t	00884609162	ADELMO.CJUNIOR	ADELMO CAVALCANTE JUNIOR
1853	t	97424145304	KEILY.HAYDAR	KEILY VIVIANE SOUZA HAYDAR
1854	t	02921988330	CLEVERSON.NOGUEIRA	CLEVERSON NOGUEIRA GUTTIERRES CLOVITTERR
1855	t	00358881137	MARCUS.GLIMA	MARCUS VINICIUS GUIMARAES DE LIMA
1856	t	01634479181	EDUARDO.TOLEDO	EDUARDO AUGUSTO DE MIRANDA TOLEDO
1857	t	78110963153	DEIVYSON.SFIGUEIREDO	DEIVYSON DOS SANTOS FIGUEIREDO
1858	t	09820455650	JOICE.SDEUS	JOICE SOARES DE DEUS GUIMARAES
1859	t	63577062134	FERNANDA.SLINO	FERNANDA SOARES LINO
1860	t	72352523168	CLEANE.CLIMA	CLEANE CARVALHO LIMA
1861	t	03827790107	LUIZ.SOLIVEIRA	LUIZ FELIPE SILVA DE OLIVEIRA
1862	t	03168461121	BRUNO.REZENDE	BRUNO CAMELO REZENDE
1863	t	00205000177	GEFERSON.WOLIVEIRA	GEFFERSON WILLIAM DE OLIVEIRA
1864	t	01215306121	TARCISIO.CAMPOS	TARCISIO VERAS DOS SANTOS CAMPOS
1865	t	03817978642	HENRIQUE.AJUNIOR	HENRIQUE DE ARAUJO DIAS JUNIOR
1866	t	02748980123	DIEGO.RSILVA	DIEGO RODRIGUES DA SILVA
1867	t	70043132120	rafael.candrade	RAFAEL CARLOS ANDRADE SILVA
1868	t	31687008191	MARCOS.AMILANEZ	MARCOS ANTONIO MILANEZ
1869	t	01270040170	PAULO.SMIKOCZAK	PAULO SERGIO MIKOCZAK
1870	t	70721394191	ALINNE.MATOS	ALINNE CARDOSO DE MATTOS
1871	t	08331801695	ADRIELLE.RFREITAS	ADRIELLE RATUND FREITAS
1872	t	00721850235	ARIOMAR.GBRITO	ARIOMAR GRANGEIRO BRITO
1873	t	37215531104	ALVARO.MPICARELLI	ALVARO MARCOS PICARELLI
1874	t	70929890191	RANIELLY.SMORAIS	RANIELLY SOARES DE MORAIS
1875	t	00074340140	EULINA.PORTELA	EULINA DE CASSIA PORTELA
1876	t	04617272186	JESSICA.EFERREIRA	JESSICA ELLEN DE FARIA FERREIRA
1877	t	03465006135	PAULO.CSANTOS	PAULO HENRIQUE DE CASTRO SANTOS
1878	t	72663634115	CAROLINA.RMAGALHAES	MARIA CAROLINA ROSA DE MAGALHAES
1879	t	89628993100	OSMAR.MJUNIOR	OSMAR MENDONCA MELO JUNIOR
1880	t	41936825287	MAGNO.APAVAO	MAGNO ALEXANDRE SANTOS PAVAO
1881	t	03381622161	MAIARA.SOUZA	MAIARA LEITE DE SOUZA
1882	t	62983385604	JACQUELINE.SSOUSA	JACQUELINE CARRIJO DE OLIVEIRA SOUSA
1883	t	02238769170	CARLOS.GUILHERME	CARLOS HENRIQUE XAVIER GUILHERME
1884	t	81073143104	ROGERIO.SILVA	ROGERIO SOUZA SILVA
1885	t	70002412179	BRENO.SOUSA	BRENO OSCAR GONCALVES DE SOUSA
1886	t	71713115204	JOYCE.PASSAYAG	JOYCE JACKSON PEREIRA ASSAYAG
1887	t	02653019124	LUANNY.NGUIMARAES	LUANNY NASCIMENTO GUIMARAES
1888	t	44097522191	JANICE.RSANTOS	JANICE RODRIGUES DOS SANTOS
1889	t	59821400159	MARCELO.MOREIRA	MARCELO DAFICO MOREIRA
1890	t	70108939189	ICARO.GALENCAR	ICARO GARCIA ALENCAR
1891	t	96445874134	VANESSA.SOUZA	VANESSA ALMEIDA DE SOUZA
1892	t	70415544149	ALBERTO.EROCHA	ALBERTO EMANUEL SERAFIM ROCHA
1893	t	81430299134	FERNANDO.LCAMILO	FERNANDO LUIZ CAMILO
1894	t	00864558341	SAMUEL.MGOMES	SAMUEL DE MOURA GOMES
1895	t	01209415119	JOSIMAR.ARRUDA	JOSIMAR CLEITON ARRUDA DA CONCEICAO
1896	t	04429562156	ARILSON.EGOMES	ARILSON EDUARDO DE CAMPOS GOMES
1897	t	04247644157	JORGE.CSOUZA	JORGE LUIZ CASTRO DE SOUZA
1898	t	09612647801	ANDRE.CFRANCO	ANDRE CORREA FRANCO
1899	t	32363605187	DIASSIS.JUNQUEIRA	DIASSIS JUNQUEIRA
1900	t	02085774164	CELIA.BATISTA	CELIA CRISTINA BATISTA
1901	t	89185587168	ARNALDO.BPAVAN	ARNALDO BARBOSA PAVAN
1902	t	07437386674	FELIPE.BSOUZA	FELIPE BARROS DE SOUZA
1903	t	95594884815	CELSO.LMOREIRA	CELSO LUIS MOREIRA
1904	t	91027284191	MARCELO.SSOUSA	MARCELO SALES DE SOUSA
1905	t	95889159100	FERNANDA.LMORAIS	FERNANDA LIMA DE MORAIS
1906	t	02238800190	EWERFRANE.LIMA	EWERFRANE TEIXEIRA LIMA
1907	t	00876207158	NADIA.BELLONI	NADIA BELLONI
1908	t	03305657103	NAILA.LSANTOS	NAILA LIMA DOS SANTOS
1909	t	87624435187	VANIA.MFARIA	VANIA MARIA DE FARIA
1910	t	02337262170	MARCOS.AMARTINS	MARCOS ALBERTO MARTINS JUNIOR
1911	t	01042247200	LUCAS.MROCHA	LUCAS MESQUITA ROCHA
1912	t	02379798109	MARILIA.ABRAO	MARILIA ABRAO SILVA
1913	t	75274957587	GERMANO.JALVES	GERMANO JORGE ALVES DA SILVA
1914	t	01776406346	AYRTON.LSOUSA	AYRTON DA COSTA LEITE SOUSA
1915	t	38408441850	JULIA.RPEREIRA	JULIA RIBEIRO DA SILVEIRA PEREIRA
1916	t	17988705253	MARIO.ARAUJO	MARIO MOURA DE ARAUJO
1917	t	02911431138	LARA.CRODRIGUES	LARA CRISTINA ESTEVAM RODRIGUES
1918	t	85390313100	edison.rcampos	EDISON RODRIGUES CAMPOS JUNIOR
1919	t	02569206130	marcos.vrodrigues	MARCOS VINICIUS RODRIGUES DO NASCIMENTO
1920	t	00442923171	CAMILA.CMOTA	CAMILA CHAVES MOTA MENEZES
1921	t	93819064168	RAPHAEL.MGARCIA	RAPHAEL MAIA GARCIA
1922	t	06374659112	GUSTAVO.SSANTOS	GUSTAVO.SSANTOS
1923	t	04077923689	MARCELO.RMENDES	MARCELO ROCHA MENDES
1924	t	76981827115	ANGELICA.NOVA	ANGELICA DO LIVRAMENTO SILVA VILA NOVA
1925	t	07186982662	DANYELLA.MRODRIGUES	DANYELLA MEDEIROS RODRIGUES
1926	t	04657088114	MAYARA.NUNES	MAYARA SOUSA NUNES
1927	t	39535142100	DEMARK.JOLIVEIRA	DEMARKISSUEL JOSE DE OLIVEIRA
1928	t	01762762382	DELANA.PSANTOS	DELANA DE PAULA DOS SANTOS PENHA
1929	t	71365982653	CLEBER.SIQUEIRA	CLEBER APARECIDO SIQUEIRA
1930	t	05431842188	MICHAEL.DARRUDA	MICHAEL DOUGLAS MARQUES DE ARRUDA
1931	t	86292528200	ADAO.CRUZ	ADAO RONALD DA SILVA CRUZ
1932	t	00804219117	CLEIDIOMAR.SANTOS	CLEIDIOMAR RODRIGUES DOS SANTOS
1933	t	06173957132	PABLO.MEDERO	PABLO.MEDERO
1934	t	04652465670	ademir.cjunior	ADEMIR CAROLINO MACHADO JUNIOR
1935	t	01911940341	AURIDEIA.GARCES	AURIDEIA PEREIRA GARCES
1936	t	49818422600	ADENILTON.SILVA	ADENILTON FREITAS DA SILVA
1937	t	01824004176	THAYSSA.ALVES	THAISSA VANESSA PEREIRA ALVES
1938	t	00507738101	DAIANA.BESSA	DAIANA LEITE DE BESSA
1939	t	02325256170	BRUNA.MATA	BRUNA FERREIRA DA MATA
1940	t	10502192607	HIGOR.SILVERIO	HIGOR SILVERIO GONCALVES
1941	t	04190783129	WANESSA.RPAULA	WANESSA RODRIGUES DE PAULA SILVA
1942	t	70502382104	MABIA.LCOALHO	MABIA LANNA COALHO DA PALMA
1943	t	00635428385	JOSE.LRODRIGUES	JOSE LICENILSON RODRIGUES
1944	t	97560316115	JALES.ROCHA	JALES CARLINDO DA COSTA E ROCHA
1945	t	73341789120	FRANCIELLY.SALVES	FRANCIELLY MENDONCA DA SILVA ALVES
1946	t	02772756106	ANA.LCOSTA	ANA CAROLINA LIMA COSTA
1947	t	01836174101	WANDERSON.TANDRADE	WANDERSON THOMAZ DE ANDRADE
1948	t	95900373215	DANILO.GOLIVEIRA	DANILO GREGORIO DE OLIVEIRA
1949	t	05527076192	RODRIGO.RSOUZA	RODRIGO ROSA AMANCIO DE SOUZA
1950	t	11294258788	NAYANE.PIERRE	NAYANE CRISTINA COSTA DE MEDEIROS PIERRE
1951	t	00931443180	CAROLINE.SPIMENTA	CAROLINE PEREIRA DA SILVA PIMENTA
1952	t	08968898669	EVELIN.CCAMILO	EVELIN CAROLINE DE SOUZA MAGALHAES CAMILO
1953	t	00737753129	INES.SSALDANHA	SUELEM INES SANTOS SALDANHA
1954	t	77998170115	JEFERSON.NMUNIZ	JEFERSON NEPOMUCENO MESIANO MUNIZ
1955	t	65795164100	MARCIO.SALCANTARA	MARCIO DOS SANTOS ALCANTARA
1956	t	02981056166	EDVALDO.SJUNIOR	EDVALDO DA SILVA JUNIOR
1957	t	67317332291	SANDRA.PRIBEIRO	SANDRA PEREIRA RIBEIRO
1958	t	00561931143	JOSE.CARDOSO	JOSE CARDOSO DA SILVA
1959	t	88876896104	KARMILIAN.BJERONIMO	KARMILIAN BARBOSA JERONIMO
1960	t	57795908691	SERGIO.SOUZA	SERGIO FRANCISCO DE SOUZA
1961	t	03213097152	DANUZA.ESOUZA	DANUZIA EMERENCIANO DE SOUZA
1962	t	02008754189	DANNILO.CANDIDO	DANNILO DE FREITAS CANDIDO
1963	t	84449594720	EDSON.MAIA	EDSON MAIA DA SILVEIRA
1964	t	54858585549	MARCELO.SILVA	MARCELO DE ANDRADE E SILVA
1965	t	81854374249	ANTONIO.SODRE	ANTONIO LEONARDO FREITA SODRE
1966	t	02262445125	LUIZ.PJUNIOR	LUIZ PEREIRA CAMPOS JUNIOR
1967	t	03983800157	BRENO.GBARBOSA	BRENO GODOY SOARES BARBOSA
1968	t	00217578101	DIVINO.AOLIVEIRA	DIVINO ALBERTO ALVES FARIA OLIVEIRA
1969	t	06734086984	ELIAS.INACIO	ELIAS INACIO
1970	t	05939735185	WELLINGTON.FARIA	WELLINGTON TALLES ROCHA DE FARIA
1971	t	36468789168	MARCELO.BFIGUEIREDO	MARCELO BORGES DE FIGUEIREDO
1972	t	01429789140	GRACIELLE.RIBEIRO	GRACIELLE FERREIRA RIBEIRO
1973	t	02301106144	WESLLEY.MSILVA	WESLLEY MADEIRA DA SILVA
1974	t	70023071184	WERLEN.MFREITAS	WERLEN DE MOURA FREITAS
1975	t	01046351184	HELOISA.ASAMPAIO	HELOISA ALVES SAMPAIO
1976	t	05197524146	JEFFERSON.SOLIVEIRA	JEFFERSON SOUZA OLIVEIRA
1977	t	03608150145	FERNANDO.ASANTOS	FERNANDO ALVES DOS SANTOS
1978	t	02350253139	CLAITON.AFERNANDES	CLAITON ANDERSON DA SILVA
1979	t	71587985187	HELRYA.LUCENA	HELRYA PEREIRA DE LUCENA
1980	t	06188140609	ENILDA.ZANZARINI	ENILDA MARCIA ALVES ZANZARINI
1981	t	06675874624	SAMUEL.SRIBEIRO	SAMUEL DA SILVA RIBEIRO
1982	t	03123550338	GEUSILENE.SEREJO	GEUSILENE COSTA SEREJO
1983	t	71098313100	THIAGO.SLOPES	THIAGO LEANDRO DE SOUZA LOPES
1984	t	83718222191	ANDRE.LOLIVEIRA	ANDRE LUIS DE OLIVEIRA COSTA
1985	t	07565469661	DIEGO.BSILVA	DIEGO BORGES SILVA
1986	t	03634832177	PATRICIA.JCARDOSO	PATRICIA DE JESUS CARDOSO
1987	t	02284517102	LAYNNE.SSILVA	LAYNNE STEFANI DE FREITAS DA SILVA
1988	t	61086290100	ARTHUR.GOMEZ	ARTHUR MENDES PEDROSO GOMEZ
1989	t	06931217690	SIMONE.OLIVEIRA	SIMONE LUCIENE DE OLIVEIRA
1990	t	05206373133	JOYCE.OQUEIROZ	JOYCE OLIVEIRA DE QUEIROZ
1991	t	03972452110	BIANCA.CSERAFIM	BIANCA LAIS CAETANO SERAFIM
1992	t	02141082102	JOSE.RSILVA	JOSE RICARDO DA SILVA
1993	t	00980646146	CLAUDIA.MARTINS	CLAUDIA MARTINS DA SILVA
1994	t	09594088605	RENATA.SSANTOS	RENATA SILVA SANTOS
1995	t	03599307199	ANGELICA.FERRAZ	ANGELICA GOMES FERRAZ
1996	t	72504471149	carlos.vsilva	CARLOS ALEXANDRE VIANA DA SILVA
1997	t	07356743612	FERNANDA.RIBEIRO	FERNANDA RIBEIRO RAMOS
1998	t	01133832156	EVANDRO.MACEDO	EVANDRO RAFAEL DE SOUSA MACEDO
1999	t	03761508158	RAQUEL.RALENCAR	RAQUEL RODRIGUES ALENCAR
2000	t	89872576149	BRUNO.SAMPAIO	BRUNO SAMPAIO DE OLIVEIRA
2001	t	84398183191	EDSON.DAVI	EDSON DAVI DE SOUSA
2002	t	02524919129	ISMAEL.CAMPOS	ISMAEL BASTOS CAMPOS
2003	t	01360365117	MARI.ASILVA	MARI ANGELA DE SOUZA E SILVA
2004	t	40504020846	HERON.LCORREIA	HERON LOREAN LIMA CORREIA
2005	t	34937237845	THIAGO.SBERNARDES	THIAGO DA SILVA BERNARDES
2006	t	00486504123	HIRINEIA.ACARVALHO	HIRINEIA ALVES CARVALHO
2007	t	71845550153	MAURICIO.SALES	MAURICIO SALES SANTOS
2008	t	04771495378	ELIANE.SILVA	ELIANE BRITO SILVA
2009	t	78740754120	CELIO.SJUNIOR	CELIO FERREIRA DA SILVA JUNIOR
2010	t	03639586140	CLEITON.CASTRO	CLEITON ALVES DE CASTRO
2011	t	03071318154	THIAGO.REIS	THIAGO DOS REIS MACEDO
2012	t	71574859153	MARCIO.PCASTRO	MARCIO PAULO FREIRE DE CASTRO
2013	t	00195351126	DANILO.OCARMO	DANILO DE OLIVEIRA CARMO
2014	t	77963326100	ELIAS.SANTOS	ELIAS LOURENCO DOS SANTOS
2015	t	00776108220	ELKILENE.SSILVA	ELKILENE SOUZA DA SILVA
2016	t	78145902134	ADENIR.MOLIVEIRA	ADENIR MAMEDE DE OLIVEIRA
2017	t	02690562383	FRANCISCA.SOUSA	FRANCISCA FERNANDA GONCALVES DE SOUSA
2018	t	10112405134	BATISTA.SILVA	JOAO BATISTA SILVA
2019	t	09830858707	CLAUDIO.BRANDAO	CLAUDIO DE SOUSA BRANDAO
2020	t	72362367134	CLAUDIA.RMARQUES	CLAUDIA RODRIGUES DA SILVA MARQUES
2021	t	02949085105	JOSEANE.SCATALAO	JOSEANE SATYRO CATALAO
2022	t	04015003144	THAIS.KAREN	THAIS KAREN GOMES DA SILVA CARVALHO
2023	t	02554048916	DERYA.SAVAS	DERYA SAVAS
2024	t	03649027100	BRENO.JCARVALHO	BRENO JUNIO DE CARVALHO
2025	t	94521565115	MACKLEY.FSOUZA	MACKLEY FARIAS SOUZA
2026	t	70735751153	ADEVANDO.LJUNIOR	ADEVANDO LEITE DA CRUZ JUNIOR
2027	t	04891215194	thatiana.brito	THATIANA DA SILVA BRITO
2028	t	02409609120	NAYARA.SILVESTRE	NAYARA SILVESTRE BARBOSA
2029	t	05517970109	raissa.santo	RAISSA CABRAL DO ESPIRITO SANTO
2030	t	82606218191	MARCO.AAQUILAS	MARCO ANTONIO AQUILAS
2031	t	03766164155	DAIANE.PSILVA	DAIANE PRISCILLA DA SILVA SANTOS
2032	t	01352226138	ERIKA.CORONEL	ERIKA DE SOUSA CORONEL
2033	t	69077657134	AMADOR.NNETO	AMADOR NUNES DE SOUSA NETO
2034	t	01881573109	RAFAEL.ALEITE	RAFAEL ALVES LEITE
2035	t	55266304120	rogerio.mvieira	ROGERIO ALEXANDRE MARQUES VIEIRA
2036	t	70410894109	JOSE.VPAULA	JOSE VICTOR DE PAULA NETO
2037	t	70192594133	DANIEL.FCARVALHO	DANIEL VICTOR FEITOSA DE CARVALHO
2038	t	73730424149	ALAN.ARAUJO	ALAN JOSE DE OLIVEIRA ARAUJO
2039	t	75375877600	FLAVIO.RESENDE	FLAVIO PINTO DE RESENDE
2040	t	61118494172	ALESSANDRO.CAVILA	ALESSANDRO DE CAMPOS AVILA
2041	t	01661754120	rodrigo.cardoso	RODRIGO CARDOSO DOS SANTOS
2042	t	70010275100	RENATA.SLEMOS	RENATA SOARES LEMOS
2043	t	73371793168	samanta.mnascimento	SAMANTA MARTINS DO NASCIMENTO
2044	t	03158865106	FREDDERYCO.RBORGES	FREDDERYCO ROBERTO HAMU BORGES
2045	t	03664038185	JESSICA.MSILVA	JESSICA MAYARA DA SILVA
2046	t	00158525205	TATIANE.GSILVA	TATIANE GUIMARAES DA SILVA
2047	t	03797593147	CARLOS.ABATISTA	CARLOS ALBERTO MARQUES BATISTA
2048	t	02247478190	ELLEN.LSILVA	ELLEN DAYANE LUIZ DA SILVA
2049	t	03736956169	WILLIAN.SCAMPOS	WILLIAM DOS SANTOS CAMPOS
2050	t	02933886103	JULIO.ACARDOSO	JULIO AUGUSTO CARDOSO CHACHA
2051	t	00569352185	PAULO.HSILVA	PAULO HENRIQUE DA SILVA MARQUES
2052	t	72220945120	DOUGLAS.NSOUZA	DOUGLAS NEVES DE SOUZA
2053	t	39798046153	carlos.acardoso	CARLOS ANTONIO CARDOSO
2054	t	83384898249	ARTHUR.ISOUSA	ARTHUR ICARO RODRIGUES DE SOUSA
2055	t	03059060118	FABIO.GTEIXEIRA	FABIO GABRIEL TEIXEIRA
2056	t	81747810134	LUCIANO.VTELES	LUCIANO VIEIRA TELES
2057	t	03494312133	LUANA.FSANCHES	LUANA FERNANDA SANCHES
2058	t	02068593173	DANIELLE.RSANTOS	DANIELLE ROSA DOS SANTOS KLOSINSKI
2059	t	03439971180	ELIVANDO.ROLIVEIRA	ELIVANDO RODRIGUES DE OLIVEIRA
2060	t	07668182683	FLAVIA.KCANEDO	FLAVIA KARINE CANEDO
2061	t	04456731176	VANESSA.TVAZ	VANESSA THAYANNE MIRANDA VAZ
2062	t	28890159120	JOAO.BGONCALVES	JOAO BATISTA GONCALVES
2063	t	72093358115	KELSS.MREGO	KELSS MORAIS REGO
2064	t	05108059140	ANA.SOARES	ANA PAULA LEMES SOARES
2065	t	19196610344	CICERO.RCLEMENTINO	CICERO ROBERTO CLEMENTINO
2066	t	95060430197	DIOGO.JSILVA	DIOGO JOSE SILVA
2067	t	03346378136	BRUNNO.WSILVA	BRUNNO WADDINGTON E SILVA
2068	t	90797620168	MICHELLE.ETORRES	MICHELLE ESTEVES TORRES
2069	t	00345339150	FELIPE.AOLIVEIRA	FELIPE ARAUJO OLIVEIRA
2070	t	84003650115	PAULO.ALVES	PAULO HENRIQUE ALVES DE OLIVEIRA
2071	t	01875362100	FRANCIELY.SILVA	FRANCIELY FABIANA DA SILVA
2072	t	02579438114	KARLA.GLACERDA	KARLA GOMES LACERDA
2073	t	03956990102	jessica.psantos	JESSICA PEREIRA SANTOS
2074	t	36458506864	EDUARDO.FNOGUEIRA	EDUARDO FONSECA NOGUEIRA
2075	t	00231943164	MAYKON.TLIMA	MAYKON THIAGO FRANCO LIMA
2076	t	05974256123	ADRIANE.MPEREIRA	ADRIANE APARECIDA MACEDO FERREIRA
2077	t	02781753173	THAMIRIS.CSANTOS	THAMIRIS CARDOSO GONCALVES DOS SANTOS
2078	t	02253583103	PAULO.MCOSTA	PAULO HENRIQUE MARTINS DA COSTA
2079	t	04207340661	ADRIANA.MPAIVA	ADRIANA MARIA DE PAIVA
2080	t	05759687107	mariana.vsanta	mariana.vsanta
2081	t	01942886179	VINICIUS.RPEREIRA	VINICIUS RODRIGUES PEREIRA
2082	t	62168169187	DINALVA.SILVA	DINALVA NOVAIS DA SILVA
2083	t	73078298191	ACSA.SPACHECO	ACSA INGRID DOS SANTOS PACHECO
2084	t	03251524623	EMERSON.FONSECA	EMERSON DIAS DA FONSECA
2085	t	70100902111	wytana.jlopes	WYTANA JAYANY LOPES
2086	t	01887128280	BRUCE.LDIAS	BRUCE LINCOLN PEREIRA DIAS
2087	t	72695960182	EVERTON.LOLIVEIRA	EVERTON LUIZ DE OLIVEIRA
2088	t	70944997104	JONATHAN.CSILVA	JONATHAN CORREIA DA SILVA
2089	t	04532578108	MURIELLY.ACOSTA	MURIELLY ALVES COSTA
2090	t	64904512120	FIRMINIO.SOUZA	FIRMINIO DOS SANTOS SOUZA
2091	t	00559426100	FABRICIO.MMENDES	FABRICIO MAIA MENDES
2092	t	00299455181	ALLINY.SILVA	ALLINY NASCIMENTO SILVA
2093	t	03361515190	THIAGO.ALEITE	THIAGO ARAUJO LEITE
2094	t	00237975254	JESSICA.FERREIRA	JESSICA DA GAMA FERREIRA
2095	t	84528273187	ADENILSON.FFERREIRA	ADENILSON FERNANDES FERREIRA
2096	t	02132333122	WADILA.VIANA	WADILA DE SOUSA VIANA
2097	t	57696578553	EDIVANIA.SOLIVEIRA	EDIVANIA SILVA DE OLIVEIRA
2098	t	00244272174	IARA.VSANTOS	IARA VAZ DOS SANTOS
2099	t	03937228136	ELIDA.SMOTA	ELIDA CASSIA SILVA MOTA
2100	t	51574250159	edilson.cferreira	EDILSON CARDOSO FERREIRA
2101	t	01209344696	ANGELA.RFERREIRA	ANGELA RODRIGUES GOMES FERREIRA
2102	t	70309557143	DNYLLIA.BSOUSA	DNYLLIA BATISTA DE SOUSA
2103	t	56548320182	HEBER.CORREIA	HEBER SILVA CORREIA
2104	t	38440300182	BENEDITO.SAMPAIO	BENEDITO GONCALO SAMPAIO
2105	t	01177898608	DOUGLAS.RTEIXEIRA	DOUGLAS RAFAEL NORONHA TEIXEIRA
2106	t	01801507317	RENATA.GMARREIROS	RENATA GRASIELE MELO MARREIROS
2107	t	01061880117	JUCELIA.ALMEIDA	JUCELIA MAIO DE ALMEIDA
2108	t	03538786186	KEREN.LFREITAS	KEREN LAIRA PINHEIRO FREITAS
2109	t	92614493104	RENATO.NUNES	RENATO ALVES NUNES
2110	t	69852537172	ALEXANDRE.FSANTOS	ALEXANDRE FORLAN DOS SANTOS
2111	t	82617856100	VIVIANE.MTOLEDO	VIVIANE MISZURA TOLEDO
2112	t	71753117100	CLERISTON.SANTOS	CLERISTON FERREIRA DOS SANTOS
2113	t	53350731104	WANDERLEY.SANTOS	WANDERLEY CARDOSO DOS SANTOS
2114	t	49134841172	CARLOS.FERREIRA	CARLOS HENRIQUE FERREIRA
2115	t	35180242649	JOSUE.COSTA	JOSUE ROBERTO DA COSTA
2116	t	58505490134	EDVONALDO.BRAZ	EDVONALDO BRAZ DA SILVA
2117	t	02602737755	SUELY.FSILVA	SUELY.FSILVA
2118	t	03430211379	PRISCILA.SFERREIRA	PRISCILA SAMIA FERREIRA MEDEIROS
2119	t	43107176168	MARLOS.BOTOSSO	MARLOS VINICIUS BOTOSSO
2120	t	75367700659	ALEXSANDRO.MENDES	ALEXSANDRO MENDONCA MENDES
2121	t	00303210346	SILVILENE.PVALE	SILVILENE DE ABREU PINTO VALE
2122	t	22300766809	FABIOFA.ALMEIDA	FABIO DE ALMEIDA
2123	t	02351527143	ANA.FERREIRA	ANA CAROLINA CARDOSO DE AVILA FERREIRA
2124	t	02141902185	ITALO.MCASTRO	ITALO FELIPE SOUSA MENDES CASTRO
2125	t	03300786150	BARBARA.AMORAES	BARBARA APARECIDA DE MORAES
2126	t	09527263603	KHELRY.SVAZ	KHELRY STANLEY DE OLIVEIRA VAZ
2127	t	06308850101	RAUL.VCOSTA	RAUL VICTOR SILVA SOMBRA DA COSTA
2128	t	00389616281	JENNIFER.CSOBRINHO	JENNIFE CRISTINA CRISTOVAO SOBRINHO
2129	t	04077030390	MARCOS.JVIANA	MARCOS JOSE SILVA VIANA
2130	t	95661590130	EDELSON.FALVES	EDELSON FERREIRA ALVES
2131	t	38794438153	AGUINALDO.DOLIVEIRA	AGUINALDO DIAS DE OLIVEIRA
2132	t	90998065900	ALEXANDRE.PFORTUNATO	ALEXANDRE PEREIRA FORTUNATO
2133	t	01042045160	MANUEL.AFILHO	MANUEL ANGEL VELASCO TRESPALACIO FILHO
2134	t	94892660191	SILVIO.DJUNIOR	SILVIO D AVILA COUTINHO JUNIOR
2135	t	06934786165	CRISTINA.FVALES	CRISTINA FERREIRA DA SILVA VALES
2136	t	04448856178	WIVIANY.MARAUJO	WIVIANY MARIA DE ARAUJO OLIVEIRA
2137	t	08537612480	JOSE.ESOUSA	JOSE EDSON SOUSA DE OLIVEIRA
2138	t	73551015104	MARCO.ARAMOS	MARCO AURELIO VIEIRA RAMOS
2139	t	04709229155	MAURICIO.RJUNIOR	MAURICIO RODRIGUES SARTIN JUNIOR
2140	t	99448653115	MARCOS.VSOUZA	MARCOS VINICIUS OLIVEIRA DE SOUZA
2141	t	00066793238	LAIS.GOLIVEIRA	LAIS GONCALVES DE OLIVEIRA
2142	t	76177009115	WANDERSON.MATOS	WANDERSON ROSA DE MATOS
2143	t	70505197154	VITORIA.LFERNANDES	VITORIA ELAINE DE LIMA FERNANDES
2144	t	70681384115	SANDRA.SSILVA	SANDRA MARIA SILVEIRA E SILVA
2145	t	01198976101	ANA.NOLIVEIRA	ANA SHELEY DAS NEVES OLIVEIRA
2146	t	74120506134	THAIS.DORNELES	THAIS DE DEUS DORNELES
2147	t	72173009168	EDUARDO.RODRIGUES	EDUARDO MARTINS RODRIGUES
2148	t	02178081150	MARIANA.ALIMA	MARIANA ALVES DE LIMA
2149	t	60524499187	KEDIMA.ROCHA	KEDIMA DA SILVA ROCHA
2150	t	03259098143	ITARI.FILHO	ITARI DE LIMA GODINHO FILHO
2151	t	54451051134	JUADILSON.BSANTOS	JUADILSON BENEDITO DOS SANTOS
2152	t	74933906149	ALINE.PSILVA	ALINE PIRES DA SILVA
2153	t	05581943621	LEONARDO.MPACHECO	LEONARDO MACIEL PACHECO
2154	t	70078528151	KEILY.APEIXOTO	KEILY ANE ARAUJO PEIXOTO
2155	t	33699148831	EDUARDO.RBARBOSA	EDUARDO RUBENS BARBOSA PEREIRA
2156	t	64460851172	ADELITON.GUSMAO	ADELITON GUSMAO MAIA
2157	t	70046860193	THAMIRES.AMARAL	THAMIRES ROSA AMARAL
2158	t	72250666172	ANDRE.LUIZ	ANDRE LUIZ CAFE DE MATOS
2159	t	01102442143	WESLEY.POLIVEIRA	WESLEY PEREIRA DE OLIVEIRA
2160	t	89719921315	ANDRE.LPAULA	ANDRE LUIZ DE PAULA NASCIMENTO
2161	t	86663402153	RENATA.VIANA	RENATA CRISTINA SILVA VIANA
2162	t	93052138120	THIAGO.CASSIS	THIAGO COSTA ASSIS
2163	t	01756719152	TIAGO.SRODRIGUES	TIAGO DOS SANTOS RODRIGUES
2164	t	77533216172	FLAVIO.JSOBRINHO	FLAVIO.JSOBRINHO
2165	t	09049054625	JENNIFER.CPANIAGO	JENNIFER CRISTINA PANIAGO
2166	t	73255173191	JOSE.ALMEIDA	JOSE.ALMEIDA
2167	t	89073959187	RODRIGO.UMENDANHA	RODRIGO URIAS MENDANHA
2168	t	00986108138	MICHAEL.JLEMES	MICHAEL JOSE DE LEMES
2169	t	72327340187	ADRIANA.FALVES	ADRIANA FERREIRA ALVES
2170	t	01220517186	VIVIANE.SOUZA	VIVIANE DE SOUZA SILVA
2171	t	02925714021	FRANCIELE.WOLIVEIRA	FRANCIELE WEGNER DE OLIVEIRA
2172	t	03766490117	JESSICA.MAGALHAES	JESSICA MACEDO MAGALHAES
2173	t	02255629445	ADELINO.QUEIROGA	ANTONIO ADELINO COSTA QUEIROGA
2174	t	06866949106	isabelle.pispolador	isabelle.pispolador
2175	t	71502181134	ANA.CARAUJO	ANA CATARINA ARAUJO CAMPOS
2176	t	11084553368	REGIMAR.PEREIRA	REGIMAR ALMEIDA PEREIRA
2177	t	89102754134	ALESSANDRO.ANDRADE	ALESSANDRO MOREIRA DE ANDRADE
2178	t	78420997153	SILVEIRO.TEIXEIRA	SILVEIRO CARDOSO TEIXEIRA
2179	t	03573131352	NEIDYANE.CPEREIRA	NEIDYANNE NARJARA CORREIA PEREIRA
2180	t	73307157191	MURILO.GRODRIGUES	MURILO GABRIEL RODRIGUES
2181	t	04400576124	THAISA.CCOSTA	THAISA HELEN CANDIDO DA COSTA
2182	t	01499554133	ANA.CARDOSO	ANA PAULA ALVES CARDOSO
2183	t	97603996104	ELVYS.ZSOUZA	ELVYS ZAVITOSKI DE SOUZA
2184	t	80889719187	PATRICIA.GVALADAO	PATRICIA GOMES MACHADO VALADAO
2185	t	31549381172	CARLUCIO.MENDANHA	CARLUCIO MENDANHA DE VILLA
2186	t	00722991185	FABIO.PSILVA	FABIO PEDRO DA SILVA
2187	t	70765391104	rosivaldo.rocha	ROSIVALDO DA CONCEICAO ROCHA
2188	t	03262415154	LEANDERSON.CLIMA	LEANDERSON CARLOS LIMA NASCIMENTO
2189	t	03557323196	ALINE.PBRITO	ALINE PEREIRA DE BRITO
2190	t	03600133330	LUCAS.VSOUZA	LUCAS VELOSO DE SOUZA PROCOPIO
2191	t	70137632177	JHESSYKA.FERREIRA	JHESSYKA KAROLYNE FERREIRA MAGALHAES
2192	t	81153953153	SAMUEL.HJUNIOR	SAMUEL HASSAN AUAD JUNIOR
2193	t	73641936187	WANDERSON.CSANTOS	WANDERSON COSTA SANTOS
2194	t	01007032162	DIEGO.RSANTOS	DIEGO ROCHA SANTOS
2195	t	29267640810	SERGIO.RALVAREZ	SERGIO RODRIGO VARGAS ALVAREZ
2196	t	13180809833	TARSO.RUTKOWSKI	TARSO RUTKOWSKI
2197	t	02665055112	ADRIANA.QSILVA	ADRIANA QUINTINO SILVA
2198	t	91021782653	HELBERT.RSCHIRMER	HELBERT RIBEIRO SCHIRMER
2199	t	01428587144	angelica.gsilva	ANGELICA GARCIA DA SILVA
2200	t	01210713160	HELIO.SABREU	HELIO JUNIO ALVES DE SA ABREU
2201	t	05902773164	MARIO.SJUNIOR	MARIO.SJUNIOR
2202	t	56445920144	AURELIA.GABRIEL	AURELIA GABRIEL
2203	t	98942476104	VIVIAN.CSOARES	VIVIAN DA CRUZ SOARES
2204	t	01468458116	ELAINE.DSANTOS	ELAINE DOS SANTOS
2205	t	02325226182	CARLOS.EPINHEIRO	CARLOS EDUARDO SILVA PINHEIRO
2206	t	05629585177	CAMILA.CLIMA	CAMILA CARVALHO LIMA
2207	t	07221494177	MATHEUS.SMORAIS	MATHEUS DE SOUSA MORAIS
2208	t	56587244149	MOZAIR.JSILVA	MOZAIR JERONIMO DA SILVA
2209	t	04050590190	VICTORIA.PXAVIER	VICTORIA HELEN AQUINO PAES LOUREIRO XAVIER
2210	t	34141693120	JOSE.RICARDO	JOSE RICARDO DE SOUZA
2211	t	30611140845	NELSON.MNETO	NELSON MARIANO NETO
2212	t	07442457452	THIAGO.AOLIVEIRA	THIAGO ANGELO OLIVEIRA
2213	t	91142911268	EDER.CSILVA	EDER COSTA SILVA
2214	t	94054347215	LUIZ.CAMARGO	LUIZ PAULO BARROS CAMARGO
2215	t	01941338186	HELLEM.GONDIM	HELLEM DE OLIVEIRA JACOMINI NUNES GONDIM
2216	t	06307967340	INDIANA.SCARVALHO	INDIANA INGRID SOUSA CARVALHO
2217	t	40108619168	NIVALDO.AARAUJO	NIVALDO ALVES DE ARAUJO
2218	t	09315834623	FAGNER.NBORGES	FAGNER EUSTAQUIO NUNES BORGES
2219	t	05197595167	JEFFERSON.PPASSOS	JEFERSON PEREIRA PASSOS
2220	t	11492073610	JAINE.FALMEIDA	JAINE FRANCA ALMEIDA
2221	t	07253562607	CLARA.LOLIVEIRA	CLARA LUCIA DE OLIVEIRA TAVARES
2222	t	97553697168	WILLIAN.JTEIXEIRA	WILLIAN JUNIO TEIXEIRA
2223	t	02627456156	ROMULO.APINTO	ROMULO ANTONIO PINTO
2224	t	00380823144	flaviane.cferreira	FLAVIANE CAMPOS FERREIRA
2225	t	01410088146	MARCELO.RMACHADO	MARCELO REIS MACHADO
2226	t	70053610164	BRUNO.TAVARES	BRUNO MARCIANO TAVARES
2227	t	00986892157	CAIO.RPACHECO	CAIO CESAR RANGEL PACHECO
2228	t	02066780103	GABRIEL.GBARBOSA	GABRIEL SANTIAGO GOMES BARBOSA
2229	t	49415204320	CHARLENE.DSILVA	CHARLENE DA SILVA
2230	t	87426587134	CLAUDIO.FARIA	CLAUDIO SILVA LINHARES DE FARIA
2231	t	05403562125	AMANDA.ASSERMAN	AMANDA ASSERMAN
2232	t	09354269656	JULIO.CSOUZA	JULIO CESAR SANTOS DE SOUZA
2233	t	92528694172	ADRIANO.CGUIMARAES	ADRIANO CARDOSO GUIMARAES
2234	t	70190457155	AMANDA.ROSA	AMANDA CRISTINA ROSA DE OLIVEIRA
2235	t	03184386155	JEAN.MCAMPOS	JEAN MICHELL SILVA CAMPOS
2236	t	90522117600	ANADEJE.PAIXAO	ANADEJE APARECIDA PAIXAO
2237	t	34519490839	guilherme.pacheco	GUILHERME PACHECO
2238	t	03553215135	weslley.ssilva	WESLLEY SANTOS SILVA
2239	t	05502135110	nidia.cmoreira	nidia.cmoreira
2240	t	10845782630	JOAO.SILVA	JOAO PAULO MAGALHAES SILVA
2241	t	75218607304	waldenia.rjesus	WALDENIRA SOCORRO ROSA DE JESUS
2242	t	05412216986	FLAVIO.CSANTOS	FLAVIO CONRADO DOS SANTOS
2243	t	66525780349	IARA.RIBEIRO	IARA PEREIRA RIBEIRO
2244	t	00949100145	KATIA.AOLIVEIRA	KATIA ALVES DE OLIVEIRA
2245	t	74146025168	ANDERSON.SRODRIGUES	ANDERSON FELIPE SEVERINO RODRIGUES
2246	t	05850268405	DANILA.RBEZERRA	DANILA ROCHA RODRIGUES BEZERRA
2247	t	02812152354	ANDERSON.ARAUJO	ANDERSON DA SILVA ARAUJO
2248	t	04327122165	LORENA.ROCHA	LORENA DA ROCHA OLIVEIRA
2249	t	33947279604	CARLOS.ASILVA	CARLOS ALBERTO SILVA
2250	t	02992624136	EDUARDO.DMARINHO	EDUARDO DINIZ MARINHO
2251	t	02731464160	JONATHA.SFAGUNDES	JONATHA JUNIOR DA SILVA FAGUNDES
2252	t	70051728176	ANDRE.ADOMINGOS	ANDRE DE ARAUJO DOMINGOS
2253	t	01637160216	maenne.ssantos	MAENNE SILVERIO DOS SANTOS
2254	t	03786015120	CRISTINA.FSILVA	CRISTINA FERREIRA DA SILVA
2255	t	01521281157	SUELEN.SOUZA	SUELEN RANGEL DE SOUZA
2256	t	71661930182	GEISE.SSANTOS	GEISE DE SOUZA CATENASSI SANTOS MENDES
2257	t	39657159172	edgar.psilva	EDGAR PINTO DA SILVA
2258	t	00051568152	LUIZ.PNINA	LUIZ PAULO NINA BERTULIO
2259	t	01886538131	ANTONIO.PEREIRA	ANTONIO PEREIRA DE OLIVEIRA
2260	t	03824194139	ANA.RAMORIM	ANA PAULA RODRIGUES AMORIM
2261	t	99743990178	THIAGO.CARVALHO	THIAGO CARLOS DE CARVALHO
2262	t	41290203822	TAYNARA.PSILVA	TAYNARA PEREIRA MARTINS DA SILVA
2263	t	05999442699	sergio.pjunior	SERGIO PERES DE CARVALHO JUNIOR
2264	t	44655045353	EUDES.SOUSA	EUDES FROTA DE SOUSA
2265	t	00296234133	FRANCISCA.SSILVA	FRANCISCA SOARES SILVA
2266	t	02815659611	ABRAAO.FDIAS	ABRAAO FERREIRA DIAS
2267	t	96437456272	GELY.PSILVA	GELY DE PAULA SILVA
2268	t	05083402173	FLAVIO.GANDRADE	FLAVIO GOMES ANDRADE
2269	t	74196430163	BRUNA.SENA	BRUNA SAIANI DE SENA AVELINO
2270	t	06089695831	HEBER.DNANGINO	HEBER DONIZETE NANGINO
2271	t	71244697168	LEANDRO.GUERREIRO	LEANDRO GUERREIRO
2272	t	00857519107	FERNANDA.OANTUNES	FERNANDA FERNANDES OLIVEIRA ANTUNES
2273	t	05814245301	JULIANA.RVALE	JULIANNA RAMOS DO VALE
2274	t	03847591142	lucas.osilva	LUCAS MULLER DE OLIVEIRA E SILVA
2275	t	95965033320	maria.pfonseca	ANA MARIA PINHEIRO FONSECA
2276	t	01979530343	NUBIA.FERNANDES	NUBIA MIRIAN LOUZEIRO FERNANDES
2277	t	03211359117	AECIO.MAIA	AECIO MATIAS MAIA
2278	t	04537369124	JESSICA.SILVA	JESSICA FONSECA DA SILVA
2279	t	06516229139	VALERIA.EMARCELINO	VALERIA ESCAMES MARCELINO
2280	t	00984552146	ALAN.MALMEIDA	ALAN JUNIO MARQUES DE ALMEIDA
2281	t	04264796105	LUCAS.DARAUJO	LUCAS DORNELES ARAUJO
2282	t	70997110163	WELLEN.BFARIAS	WELLEN BLOSFELD FARIAS
2283	t	99840642120	IDAIANE.RCUNHA	IDAIANE RODRIGUES DA CUNHA
2284	t	69079358134	RODRIGO.BNASCIMENTO	RODRIGO BINO DO NASCIMENTO
2285	t	01826009124	RENATA.PPEREIRA	RENATA PINHEIRO PEREIRA
2286	t	87525100187	JULIO.CREIS	JULIO CESAR DOS REIS
2287	t	70850849101	sarah.palmeida	SARAH DE PAULA ALMEIDA
2288	t	04645040154	vithor.avitoriano	vithor.avitoriano
2289	t	98731319220	JESSICA.PAMILA	JESSICA PAMILA DOS SANTOS
2290	t	42026717249	WILSON.PSANTOS	WILSON PEREIRA DOS SANTOS
2291	t	62010158172	WILLIAM.CRUZ	WILLIAM DA SILVA CRUZ
2292	t	03382271648	CRISTIANO.BORGES	CRISTIANO DOMINGOS BORGES
2293	t	04626436110	BRUNO.VPIMENTEL	BRUNO.VPIMENTEL
2294	t	02745714120	MIRELY.LMOURA	MIRELY LOPES MOURA
2295	t	03478995108	FERNANDO.GMATOS	FERNANDO GONCALVES DE MATOS
2296	t	06144283196	JOAO.VSSILVA	JOAO.VSSILVA
2297	t	28632192272	PAULO.STAVARES	PAULO SERGIO TAVARES DOS SANTOS
2298	t	04294094192	PRICYLLA.SBORGES	PRICYLLA DE SOUSA OLIVEIRA BORGES
2299	t	71052178120	PIERINA.MZANIOLI	PIERINA MODANEZI ZANIOLI
2300	t	01508274665	JOHN.CREIS	JOHN KENNEDY DA CUNHA REIS
2301	t	91521343349	DANIEL.PAIVA	DANIEL FIALHO ALMEIDA XIMENES PAIVA
2302	t	80036643220	gustavo.mendes	GUSTAVO MENDES
2303	t	75507501149	ARTUR.BMORAES	ARTUR BOVO MORAES
2304	t	01814374140	gabrielle.cribeiro	GABRIELLE CASSIMIRO RIBEIRO
2305	t	00356195112	JOSE.ESANTOS	JOSE EDUARDO DOS SANTOS
2306	t	02495505120	FABIANE.SGARCIA	FABIANE CRISTINA SANTANA GARCIA
2307	t	03019731640	EDUARDO.NOGUEIRA	EDUARDO NOGUEIRA
2308	t	04846595374	DEUSEVAL.BJUNIOR	DEUSEVAL DOS SANTOS BRITO JUNIOR
2309	t	00381231186	wenderson.gcastro	WENDERSON GONCALVES DE CASTRO
2310	t	03186663164	TATIANA.SDIAS	TATIANA SILVA DIAS
2311	t	98814443220	ANDRESSA.TCARVALHO	ANDRESSA THOMAS CARVALHO
2312	t	02867641306	daniella.mbrito	DANIELLA MOREIRA BRITO LUZ
2313	t	01733316108	felipe.ocordao	FELLIPE LEONARDO OLIVEIRA FRANCA CORDAO
2314	t	90032322291	MARIANA.GOEDERT	MARIANA MOURA GOEDERT
2315	t	00471937193	BARBARA.MOTA	BARBARA HAIRA RIBEIRO DA MOTA
2316	t	02481350154	JEFERSON.COSTA	JEFERSON ROBERTO DA COSTA
2317	t	98328140144	CHARLES.ALELIS	CHARLES AUGUSTO LELIS DE SOUZA
2318	t	27685136104	ROBERTO.MACHADO	ROBERTO CANDIDO MACHADO
2319	t	01819826171	UBIRACY.PSILVA	UBIRACY PEREIRA DA SILVA
2320	t	60711981191	JOSE.EOLIVEIRA	JOSE ELISSANDRO DA SILVA OLIVEIRA
2321	t	11152289608	NAYANE.SMENDES	NAYANE PRISCILA SOARES MENDES
2322	t	01223274624	VANDERSON.RDOMICIANO	VANDERSON RAIMUNDO DOMICIANO
2323	t	70066809134	ERNANI.SNETO	ERNANI SOARES GOMES NETO
2324	t	01477653155	ALINE.FCUNHA	ALINE FERREIRA CUNHA
2325	t	03860768123	ULISSES.JBRITO	ULISSES JUNIO RODRIGUES DE BRITO
2326	t	04063092135	CELISMAR.SBARCELLOS	CELISMAR SOUZA BARCELLOS JUNIOR
2327	t	98370740197	reginaldo.calisto	REGINALDO CALISTO FIGUEREDO
2328	t	72763485200	VALTER.JLISBOA	VALTER JOSE NEVES DA SILVA LISBOA
2329	t	03319293176	HEITOR.MFERREIRA	HEITOR MARBEN DIAS FERREIRA
2330	t	02053298103	glaucia.mvieira	GLAUCIA MARTINS VIEIRA MAGALHAES
2331	t	34816577149	SERGIO.MAIA	LUIZ SERGIO DE OLIVEIRA MAIA
2332	t	01174252146	CRISTIANO.ALIMA	CRISTIANO ALVES DE LIMA
2333	t	69833982115	rosangela.bpaula	ROSANGELA BATISTA DE PAULA
2334	t	72311649191	JOSELINA.NSILVA	JOSELINA NASCIMENTO DA SILVA
2335	t	87120615149	MARCIA.PLEITE	MARCIA PATRICIA LEITE FRATINE
2336	t	11241541671	KAMYLLA.REZENDE	KAMYLLA MACHADO REZENDE
2337	t	66180392234	PAULO.AGUIAR	PAULO ADRIANO DE AGUIAR
2338	t	51137410604	EDSON.COSTA	EDSON GONCALVES DA COSTA
2339	t	09172615699	ADRIELE.MOREIRA	ADRIELE APARECIDA DE OLIVEIRA MOREIRA
2340	t	83716823104	ALEX.MOURA	ALEX SOUSA MOURA
2341	t	03629328164	CARULINA.SOLIVEIRA	CARULINA SILVA DE OLIVEIRA
2342	t	01505575150	ALESSANDRA.RSILVA	ALESSANDRA RIBEIRO DA SILVA
2343	t	57417547672	CARLOS.MAIA	CARLOS EDUARDO VIEIRA MAIA
2344	t	04312675104	pamela.lneves	PAMELA LUIZA NEVES ADAMS
2345	t	74739603187	tiago.acamargo	TIAGO ALVES CAMARGO
2346	t	75562626187	FELIPE.MSANTOS	FELIPE MIGUEL DOS SANTOS
2347	t	00174083181	ACKSON.OLIMA	ACKSON DE OLIVEIRA LIMA
2348	t	69561389134	KLEIDYSON.RMENEZES	KLEIDYSON RODRIGUES DE MENEZES
2349	t	01049142110	JHULLY.CARVALHO	JHULLY MAIARA NUNES DE CARVALHO
2350	t	73194077104	THIAGO.MPINHEIRO	THIAGO RIBEIRO MACHADO PINHEIRO
2351	t	00515587184	EDIVAN.PALMEIDA	EDIVAN PINHEIRO DE ALMEIDA
2352	t	00581673158	LUIZ.AMANOEL	LUIZ ANTONIO MANOEL DE ABREU
2353	t	04933074607	PABLO.SCARVALHO	PABLO SPIRANDELLI CORDEIRO CARVALHO
2354	t	66584060144	ADRIANA.SSILVA	ADRIANA DE SOUZA LIMA DA SILVA
2355	t	05319026170	DOUGLAS.RREZER	DOUGLAS DOS REIS REZER
2356	t	06369292133	ISABELLA.CMORAES	ISABELLA CRISTINA DE MORAES
2357	t	04754420322	KEVYLA.FERREIRA	KEVYLA NATHALIA FERREIRA SOUZA
2358	t	51796058149	ALFREDO.PATAIDE	ALFREDO PEREIRA DE ATAIDE
2359	t	05720730389	ANA.BRUZACA	ANA PAULA SOUSA BRUZACA
2360	t	05370255458	KALINE.VCAMPOS	KALINE VANESSA CAMPOS MEDEIROS
2361	t	86316672187	KELSON.LBARBOSA	KELSON LUCIANO TEODORO BARBOSA
2362	t	02660112177	JULIANA.PCARVALHO	JULIANA PEREIRA DE CARVALHO
2363	t	68962355191	andre.lcaceres	ANDRE LUIZ CACERES
2364	t	81790333172	cley.jsilva	CLEY JORGE DE OLIVEIRA SILVA
2365	t	50405284187	SILVIO.GOLIVEIRA	SILVIO GOMES DE OLIVEIRA
2366	t	08068058621	NELSON.SSILVA	NELSON SAMUEL SILVA TEIXEIRA
2367	t	62280759187	DANILO.EFILHO	DANILO ESTEVES FILHO
2368	t	86655523672	MARCOS.FERREIRA	MARCOS ANTONIO FERREIRA
2369	t	72718994134	FABRICIO.SANTOS	FABRICIO RIPARDO DOS SANTOS
2370	t	91186013168	HUGO.MOREIRA	HUGO MAGALHAES MOREIRA
2371	t	38622931811	MICHAEL.CSOUZA	MICHAEL CRISTIANO DE SOUZA
2372	t	02465090151	KATYELLE.AFERREIRA	KATYELLE ALVES FERREIRA
2373	t	04642250190	WESLEY.HSOUSA	WESLEY HENRIQUE SOUSA
2374	t	72388501172	HENNESTHAYANE.AGUIAR	HENNESTHAYNE VIEIRA DE CASTRO AGUIAR
2375	t	03246522162	MYSAEL.AROSA	MYSAEL ALVES ROSA DA SILVA
2376	t	00848008103	ozana.carvalho	OZANA CARVALHO
2377	t	03519116189	MURILO.AROSA	MURILO ANCHIETA ROSA
2378	t	66645409168	LEALDO.BMENEZES	LEALDO BELOTTO MENEZES
2379	t	38097818191	JOSELIA.ADIAS	JOSELIA ALVES DIAS
2380	t	86581163104	ISAMIL.NRONDON	ISAMIL NUNES RONDON
2381	t	05779422125	THIAGO.HCARVALHO	THIAGO HINERY FERREIRA DE CARVALHO
2382	t	02765903174	JAIRO.CBATISTA	JAIRO CESAR BATISTA DE OLIVEIRA JUNIOR
2383	t	06342018677	PATRICIA.NCAMARGOS	PATRICIA NUNES CAMARGOS
2384	t	04105578383	ARILENE.BSILVA	ARILENE BECKMAN DA SILVA
2385	t	02546141163	TARIANA.BUENO	TARIANA FERREIRA BUENO
2386	t	45236062349	claudia.maraujo	CLAUDIA MAISA ARAUJO SANTOS
2387	t	94830185287	FERNANDO.FOLIVEIRA	FERNANDO FURTUNATO DE OLIVEIRA
2388	t	85272345134	JEOVANE.DMACHADO	JEOVANE DIAS MACHADO
2389	t	07999468690	MIRIANE.ASOUZA	MIRIANE ALINE AMARAL SOUZA
2390	t	36872172134	ELAINE.GONCALVEZ	ELAINE GARCEZ GONCALVEZ
2391	t	71069623172	SUELY.RSOLSEN	SUELY ROSA SILVA OLSEN
2392	t	63685442287	GLEDSON.MSOUZA	GLEDSON MAGALHÃES E SOUZA
2393	t	05452070109	PEDRO.HARAUJO	PEDRO HENRIQUE DE ARAUJO ALMEIDA OLIVEIRA
2394	t	04041532167	breno.rodrigues	BRENO DE OLIVEIRA RODRIGUES
2395	t	85184683100	RODRIGO.CSILVA	RODRIGO COSTA E SILVA
2396	t	01054527180	ADEMAR.CJUNIOR	ADEMAR LUIZ COSTA JUNIOR
2397	t	02146719176	DAIANE.ABADIA	DAIANE ABADIA COSTA
2398	t	02709685124	FRANCIEL.OBORGES	FRANCIEL DE OLIVEIRA BORGES
2399	t	08191967626	PAULO.RNUNES	PAULO RICARDO NUNES DA CONCEICAO
2400	t	00517117100	WILLY.SILVA	WILLY SOARES SILVA
2401	t	00812235100	THYAGO.BPINHEIRO	THYAGO BENTO PINHEIRO
2402	t	00148726178	GLEIDSON.MAZEVEDO	GLEIDSON MONTENEGRO DE AZEVEDO
2403	t	72264136120	ALESSANDRO.JOSE	ALESSANDRO CARDOSO LEITE SAO JOSE
2404	t	04425155106	ANDRE.ANEVES	ANDRE ARAUJO NEVES
2405	t	52412601100	ANDRE.SMARQUES	ANDRE DE SOUZA MARQUES
2406	t	02821049170	mariana.pmartins	MARIANA DE PAULA MARTINS
2407	t	00477731384	JOSUE.SSANTANA	JOSUE DA SILVA SANTANA
2408	t	71994807172	ANDERSON.LSANTOS	ADERSON LUIZ DOS SANTOS
2409	t	00562206183	CLEIDILMAR.RSOUSA	CLEIDILMAR RIBEIRO DE SOUSA
2410	t	03415908194	luiz.amaral	LUIZ ANTOYNE AMARAL DE SOUZA
2411	t	26315661149	ANTONIO.BRITO	ANTONIO VIEIRA DE BRITO
2412	t	74587480797	JOSE.DUARTE	JOSE CARLOS DUARTE OLIVEIRA
2413	t	73010308604	rogerio.asilva	ROGERIO ALCANTARA DA SILVA
2414	t	02088261141	JOAO.PRAMOS	JOAO PAULO RAMOS LOPES
2415	t	00115143106	LUCAS.FESILVA	LUCAS FERREIRA DA SILVA
2416	t	94494886149	EDILENE.FMORATO	EDILENE DE AQUINO FERNANDES MORATO
2417	t	77895185500	DERISVALDO.SABACH	DERISVALDO DE SANTANA SABACH
2418	t	00743961102	glauter.bsilva	GLAUTER BARBOZA SILVA
2419	t	04711705341	CLAUDIA.MREIS	CLAUDIA MIRELLY REIS VIEGAS
2420	t	05045989375	CLEIDIANE.GCARVALHO	CLEIDIANE GOMES DE CARVALHO
2421	t	93577869100	LUCIENE.LSOUZA	LUCIENE LIMA DE SOUZA
2422	t	79107095287	CRISTIANE.CLEAL	CRISTIANE COSTA LEAL
2423	t	28882172104	JANUARIO.LJUNIOR	JANUARIO LIMA JUNIOR
2424	t	03403476197	ANDRES.CRUZ	ANDRES MICHAEL DA CRUZ BOMFIM
2425	t	65469097315	RAIMUNDO.MNETO	RAIMUNDO MIGUEL DO NASCIMENTO NETO
2426	t	00675602106	RAPHAEL.MATOS	RAPHAELL BOTELHO MATTOS
2427	t	01593670346	ERYKA.DVIEIRA	ERYKA NATHALIA DE CARVALHO DINIZ VIEIRA
2428	t	01759401188	CARLOS.FJUNIOR	CARLOS LUIZ FABIANO JUNIOR
2429	t	00347237142	PAULA.GSILVA	PAULA GRAZIELA DA SILVA
2430	t	03622018119	MARCOS.VSILVA	MARCOS VINICIUS MORAIS SILVA
2431	t	03359869192	ADILSON.RJUNIOR	ADILSON.RJUNIOR
2432	t	99715333168	FREDERICO.SMOURA	FREDERICO SILVA DE MOURA
2433	t	52973140200	CAMILA.AFONTELES	CAMILA APARECIDA CAO FONTELES
2434	t	04274617157	MAIARA.LARAUJO	MAIARA LUCIO ARAUJO
2435	t	69353808120	KATIANE.DSANTOS	KATIANE DANTAS DOS SANTOS
2436	t	03181721190	RENATO.MARTINS	RENATO MIKHAIL MARTINS ATIE AJI
2437	t	62085468187	ADELIO.OLIVEIRA	ADELIO GONCALVES DE OLIVEIRA
2438	t	01464790175	ALEXANDRO.COLIVEIRA	ALEXANDRO CUNHA DE OLIVEIRA
2439	t	03445660131	DANIEL.FMORAES	DANIEL FELIPE DA SILVA MORAES
2440	t	56370180220	INERI.ALIMA	INERI ALVES DE LIMA
2441	t	04064977106	EDIPO.MASSUNCAO	EDIPO MACIEL DE ASSUNCAO
2442	t	02668923107	cintia.rpedroso	CINTIA RODRIGUES PEDROSO
2443	t	04259496131	CLEIDSON.FLIMA	CLEIDSON MOREIRA DE FARIAS LIMA
2444	t	02609287127	nadia.mteles	NADIA MICHELLE TELES
2445	t	94263400178	SHERLLE.ANES	SHERLLE MACHADO MARQUES ANES
2446	t	62356399291	WALTER.GMONTEIRO	WALTER GONCALVES MONTEIRO
2447	t	04317555158	JENNYFER.BSILVA	JENNYFER BUENO DA SILVA LIMA
2448	t	94264325153	THAINA.SOUSA	THAINA DAMASCENO DE SOUSA
2449	t	00215266218	DIOGO.SSOUSA	DIOGO SIQUEIRA DE SOUSA
2450	t	04174756163	LEILIANE.BRODRIGUES	LEILIANE BATISTA RODRIGUES
2451	t	66642914104	LUCIANO.MROCHA	LUCIANO MACHADO ROCHA
2452	t	01377551121	CRISTIANE.OSILVA	CRISTIANE OLIVEIRA  DA SILVA
2453	t	73191752153	HELLEN.FBARBOSA	HELLEN LOYSE FRANCA BARBOSA
2454	t	11520421656	KEYFANE.FERREIRA	KEYFANE FERREIRA DOURADO
2455	t	02013051190	JOHNATHAN.RDEUS	JOHNATHAN RIBEIRO DE DEUS
2456	t	62944509187	ADILSON.OMOUTINHO	ADILSON OLIVEIRA MOUTINHO
2457	t	71685430163	RENATA.VIEIRA	RENATA DE ASSIS RAMOS E LIMA VIEIRA
2458	t	01206244194	rafael.mbrito	RAFAEL MENDES DE BRITO
2459	t	71764097653	CLAUDIO.ANTONIO	CLAUDIO ANTONIO FONSECA
2460	t	00275480151	LEONARDO.LFAUSTINO	LEONARDO LAY FAUSTINO
2461	t	03334102114	MARCOS.VICENTINI	MARCOS.VICENTINI
2462	t	78183367534	CELINE.ASANTOS	CELINE ALMEIDA DOS SANTOS
2463	t	02072583179	ALICE.GBARRETO	ALICE GONCALVES BARRETO RIBEIRO
2464	t	10089646703	CHARLES.SVALADARES	CHARLES MARCEL DA SILVA VALADARES
2465	t	31040160115	ADRIANA.MLOPES	ADRIANA.MLOPES
2466	t	29194288100	SIMON.CBRITO	SIMON.CBRITO
2467	t	06053732133	BIANCA.CBARBOSA	BIANCA DE CARVALHO BARBOSA
2468	t	48864803149	ENILSON.POURIVES	ENILSON JOSE DE PAULA OURIVES
2469	t	94132097900	PAULO.GFARIAS	PAULO ANTONIO GREGORIO FARIAS
2470	t	05191082133	LORENA.CLOPES	LORENA DA CRUZ LOPES
2471	t	00762500166	EDUARDO.GOLIVEIRA	EDUARDO GONCALVES DE OLIVEIRA
2472	t	81420269100	DANIEL.SANTIAGO	DANIEL SANTIAGO
2473	t	06230841630	LUIZ.CAVILA	LUIZ CARLOS DE AVILA
2474	t	05098152194	EDUARDO.ROLIVEIRA	EDUARDO PATRICK RIBEIRO DE OLIVEIRA
2475	t	70185616127	THAYS.PDUARTE	THAYS ALVES PINHEIRO DUARTE
2476	t	02017986119	RAYLON.MSOUZA	RAYLON MIRANDA DE SOUZA
2477	t	83404570120	ALEXANDRE.FERREIRA	ALEXANDRE FERREIRA DA SILVA
2478	t	52109615249	ROBSON.RMENDONCA	ROBSON ROCHA MENDONCA
2479	t	69688311120	FABIANO.FORTUNATO	FABIANO CEZARIO DA CUNHA FORTUNATO
2480	t	04621073109	diego.rmorais	DIEGO RIBEIRO DE MORAIS
2481	t	07059200703	RENATO.NSILVA	RENATO NOGUEIRA SILVA
2482	t	02907842129	daniela.bsouza	DANIELA BATISTA DOS SANTOS DE SOUZA
2483	t	00850990319	bartolomeu.nfilho	bartolomeu.nfilho
2484	t	97030821149	RONALDO.ASANTOS	RONALDO ALVES DOS SANTOS
2485	t	00361548141	CLAUDIO.MSAQUI	CLAUDIO MACHADO SAQUI
2486	t	00496933108	LEONARDO.TLIMA	LEONARDO TAVARES LIMA
2487	t	02406133176	FABIO.SBARBOSA	FABIO DA SILVA BARBOSA
2488	t	84181524191	ROBERTO.FALMEIDA	ROBERTO FERREIRA DE ALMEIDA
2489	t	03487380307	NATALIA.TARAUJO	NATALIA TORRES DE ARAUJO
2490	t	05238189125	karolayne.assmann	KAROLAYNE ASSMANN
2491	t	03757067126	filipe.mbrito	FILIPE MARQUES BRITO
2492	t	98937758172	ODACYR.PRADO	ODACYR DA SILVA RODRIGUES DO PRADO
2493	t	70231687109	wanessa.asantos	WANESSA ALVES DOS SANTOS
2494	t	49040979120	GIOVANE.FREITAS	GIOVANE DE FREITAS
2495	t	91624622100	ARTHUR.FERREIRA	ARTHUR ALVES FERREIRA
2496	t	52894991215	tayna.csilva	TAYNA CARINE PERES DA SILVA
2497	t	01161841164	VICTOR.SSOARES	VICTOR SILVA SOARES
2498	t	87882809187	WESTHER.RARAUJO	WESTHER RODRIGUES DE ARAUJO
2499	t	04483877194	janaile.bmenezes	JANAILE BATISTA MENEZES MAGALHAES
2500	t		--	--
2501	t	00389849111	ALAN.SOARES	ALAN SOARES DA SILVA
2502	t	04158885140	LIDIANE.PSILVA	LIDIANE PEDRA SILVA
2503	t	71768815291	FRANCISCA.MGOMES	FRANCISCA MARIA GOMES
2504	t	05332626150	ANDRESSA.MPIMENTEL	ANDRESSA MARIA MARQUES PIMENTEL
2505	t	07963702650	THIAGO.FMENDES	THIAGO FRANCO MENDES
2506	t	04110003300	RAIMUNDO.RPEREIRA	RAIMUNDO RAFAEL PEREIRA EVERTON
2507	t	88793109172	ABRAAO.JSILVA	ABRAAO JOSE DA SILVA
2508	t	01535970111	ADRIANA.MSANTANA	ADRIANA MOREIRA SANTANA
2509	t	04491026106	MATHEUS.LLOPES	MATHEUS LIMA LOPES
2510	t	04808249162	LEIDIANE.PCAVALCANTE	LEIDIANE PEREIRA CAVALCANTE
2511	t	00386085102	ANTONIO.CFILHO	ANTONIO JOSE DA COSTA FILHO
2512	t	47915684191	JOAO.FMAGALHAES	JOAO FERREIRA DE MAGALHAES
2513	t	02405475197	diego.osilva	DIEGO OLIVEIRA SILVA
2514	t	01460212150	thiago.psoares	THIAGO PEREIRA SOARES
2515	t	21450417884	ANA.LGUIMARAES	ANA CAROLINA LIMA GUIMARAES
2516	t	02067441108	ANA.GREZENDE	ANA PAULA GOMES REZENDE
2517	t	03103794150	JULIANA.SLEONARDO	JULIANA DA SILVA LEONARDO
2518	t	02380093199	dyanna.hsantos	DYANNA HELENA DOS SANTOS
2519	t	73213748153	ANDRE.LARANTES	ANDRE LOURENCO ARANTES ALBUQUERQUE
2520	t	76597083687	SILVIA.GARCEZ	SILVIA ALVES GARCEZ MEDRADO
2521	t	02185990136	ALESSANDRO.FSANTANA	ALESSANDRO FERREIRA DE SANTANA
2522	t	99005689153	GLAYDSON.FERNANDES	GLAYDSON CAIXETA FERNANDES
2523	t	00444063129	GEILSON.FARIAS	GEILSON LISBOA DE FARIAS
2524	t	07542272195	dheeniffer.bsantos	dheeniffer.bsantos
2525	t	02776625243	KASSANDRA.MAIA	KASSANDRA LEITE MAIA
2526	t	00740374125	JONATHAN.PSILVA	JONATHAN MICHAEL PINHEIRO DA SILVA
2527	t	41294483315	KAREN.MELO	KAREN GONCALVES MELO
2528	t	03877283179	JULIANNA.COSTA	JULIANNA SANTOS COSTA
2529	t	98279386149	LUIZ.CASTRO	LUIZ HENRIQUE DE OLIVEIRA CASTRO
2530	t	02262416109	ERICK.MATIAS	ERICK MATIAS SILVA
2531	t	02660370117	JULIANE.CSANINI	JULIANE CARINE SANINI
2532	t	05194685114	pedro.soliveira	PEDRO HENRIQUE DE SOUZA OLIVEIRA
2533	t	87076608120	ANTONIO.BMOURA	ANTONIO BRAGA DE MOURA
2534	t	00611445174	ANA.PMORAES	ANA PAULA COSTA MORAES
2535	t	01349547182	APOLIANA.SOARES	APOLIANA SOARES RODRIGUES
2536	t	00765656108	DANILO.OMELO	DANILO DE OLIVEIRA MELO
2537	t	00889295310	HAYKEL.PBARBOSA	HAYKEL PINHEIRO BARBOSA
2538	t	64941060725	selma.sferreira	SELMA CRISTINA DOS SANTOS FERREIRA
2539	t	02460779135	FILIPE.JMONTEIRO	FILIPE JOSEPH MONTEIRO AMADO
2540	t	89196066172	MIRIAM.RSA	MIRIAM RIBEIRO DE SA
2541	t	02272165125	WAGNER.MTEODORO	WAGNER MEDEIROS TEODORO
2542	t	00099025116	ELIAS.JOSEPH	ELIAS JOSEPH EL HOMSI
2543	t	01460512111	ALEXANDRE.SERAFIM	ALEXANDRE MARIANO DE ALMEIDA SERAFIM
2544	t	02169656162	ABIMAEL.CJUBE	ABIMAEL CARDOSO JUBE
2545	t	04316523333	ANTONIA.ARAUJO	ANTONIA PEREIRA ARAUJO
2546	t	65971930087	ROSEANE.DALMORO	ROSEANE.DALMORO
2547	t	03687485102	ANA.RCRUZ	ANA KARINNA RIBEIRO DA CRUZ
2548	t	88364496115	FABIOLA.FAGUIAR	FABIOLA FLAVIA DE AGUIAR
2549	t	42541360134	ELPIDIO.RNASCIMENTO	ELPIDIO RODRIGUES DO NASCIMENTO
2550	t	73372145100	THIAGO.ABARBOSA	THIAGO ANTONIO BARBOSA LOPES
2551	t	00737253150	KAROLINE.PSOUSA	KAROLINE PEREIRA DE SOUSA
2552	t	35403683100	EDUARDO.BOFUGI	EDUARDO BORGES OFUGI
2553	t	05300512141	ALAN.SRODRIGUES	ALAN SOARES RODRIGUES DE ALMEIDA
2554	t	04246451142	DANIELLE.ACRUVINEL	DANIELLE LARISSA ARAUJO CRUVINEL
2555	t	81953020178	APARECIDO.GASPARINI	APARECIDO JULIANO GASPARINI
2556	t	70258398175	MILEYDI.MAGALHAES	MILEYDI MARGARIDA MAGALHAES
2557	t	83510915534	FERNANDA.MSAMPAIO	FERNANDA.MSAMPAIO
2558	t	04127275103	ALISSON.BPIRES	ALISSON BRUNO BATISTA PIRES
2559	t	99260646120	elessandra.clima	ELESSANDRA DA COSTA LIMA
2560	t	87564335149	CARLOS.CSILVA	CARLOS EDUARDO CARDOSO DA SILVA
2561	t	04718083346	MICHAEL.BLOPES	MICHAEL BRUNO LOPES SANTOS
2562	t	00278567398	ADRIANA.JAZEVEDO	ADRIANA DE JESUS AZEVEDO
2563	t	71702695387	ADRIANA.BVIEIRA	ADRIANA BONFIM VIEIRA
2564	t	09546114707	MARIO.SOUSA	MARIO DE SOUZA NASCIMENTO
2565	t	45783985368	KAYNE.DREGO	KAYNE DINIZ DO REGO
2566	t	03518075195	VICTOR.PMINAYA	VICTOR ARNIZAUT PINHEIRO MINAYA
2567	t	01555683614	FREDERICO.TABREU	FREDERICO TADEU FONSECA ABREU
2568	t	02699364320	ANTONIO.MACEDO	ANTONIO LEANDRO RODRIGUES DE MACEDO
2569	t	04354541106	ANA.CROCHA	ANA CAROLINA DE LIMA ROCHA
2570	t	03371807177	rafael.lribeiro	RAFAEL DE LIMA RIBEIRO
2571	t	00791432106	CHARLES.BFILHO	CHARLES RODRIGUES BRASIL FILHO
2572	t	02439664154	HELIO.BASSIS	HELIO BERNARDES CORDEIRO DE ASSIS
2573	t	51723379115	CLAUDIA.SALAZAR	CLAUDIA REGINA ANDRADE DE OLIVEIRA SALAZAR
2574	t	70793169100	rafael.laraujo	RAFAEL LIMA ARAUJO
2575	t	83633120106	JULIANO.PRADINES	JULIANO PRADINES MARCANTONIO
2576	t	01080633138	BEATRIZ.OSILVA	BEATRIZ CABRAL DE OLIVEIRA SLVA
2577	t	03767304392	VANDERCLEI.SSANTOS	VANDERCLEI.SSANTOS
2578	t	00552090336	WANDERCLEYSON.LSILVA	WANDERCLEYSON.LSILVA
2579	t	62251813349	JAQUELINE.LRIBEIRO	JAQUELINE.LRIBEIRO
2580	t	00970401183	ALESSANDRA.SILVA	ALESSANDRA RODRIGUES DA SILVA
2581	t	04868283103	BRENNDA.PTEIXEIRA	BRENNDA PEREIRA TEIXEIRA
2582	t	03741383112	ARTHUR.SSANTOS	ARTHUR DE SOUZA SANTOS
2583	t	83943161153	GABIA.CAMPOS	GABIA DA SILVA CAMPOS FRANCISCO
2584	t	03514003386	ana.ksousa	ANA KAREM DE SOUSA OLIVEIRA
2585	t	31802174800	PATRICIA.MADALENA	PATRICIA MADALENA VENANCIO
2586	t	93773412134	Wahtson.rdamacena	WAHTSON RODRIGO BENTO DAMACENA
2587	t	83880305153	JEAN.RPAZ	JEAN RODRIGO PAZ DE CARVALHO
2588	t	06259540175	ana.csousa	ANA CAROLINE DAMACENO DE SOUSA
2589	t	01234976129	adriana.msantos	ADRIANA MARTA DOS SANTOS
2590	t	02889857107	andressa.aoliveira	ANDRESSA CRISTINA AFONSO DE OLIVEIRA
2591	t	03268749178	michely.fpinto	MICHELY EIRO DE FARIA PINTO
2592	t	05160335498	ROMMENNYDJA.SMACEDO	ROMMENNYDJA SALES MACEDO
2593	t	75813610391	RODRIGO.BARBOSA	RODRIGO DA SILVA BARBOSA
2594	t	03136041186	WANESSA.PBARBOSA	WANESSA PEREIRA BARBOSA
2595	t	45508941134	claudinei.slira	CLAUDINEI DOS SANTOS LIRA
2596	t	97734756115	ANDRE.SARIOLI	ANDRE LUIZ DE SOUSA ARIOLI
2597	t	03084935165	ANA.CFALQUETO	ANA CAROLINE LEAO FALQUETO
2598	t	70082713103	MARIA.SLIMA	MARIA REGINA DA SILVA LIMA
2599	t	70391533193	KATELLEN.SSOUSA	KATELLEN SANTANA SOUSA
2600	t	70333204182	DENISE.SABREU	DENISE DE SOUZA ABREU
2601	t	70141639148	KAILLA.HMACHADO	KAILLA HANNA GOMES MACHADO
2602	t	02743237104	LELIANE.SOLIVEIRA	LELIANE SOUSA DE OLIVEIRA
2603	t	01565897102	AMANDA.MARTINS	AMANDA MARTINS FERNANDES
2604	t	04964190641	ALESSANDRO.MRESENDE	ALESSANDRO MENDONCA RESENDE
2605	t	01026769175	FLAVIO.SALVES	FLAVIO DA SILVA ALVES
2606	t	50010743120	JORGE.GFARIA	JORGE GOMES DE FARIA
2607	t	70272024171	luis.hguimaraes	LUIS HENRIQUE SILVA GUIMARAES
2608	t	00920397158	ARCOVERDE.CJUNIOR	ARCOVERDE BARBOSA FRANCO DE CASTRO JUNIOR
2609	t	01275581102	TATIANE.RJESUS	TATIANE RODRIGUES DE JESUS
2610	t	50091743591	VALMIR.MARINHO	VALMIR MOREIRA MARINHO
2611	t	04236660105	GUILHERME.VBUENO	GUILHERME VIEIRA BUENO
2612	t	05081399108	LEOMAR.SCARVALHO	LEOMAR SERGIVALDO DE CARVALHO
2613	t	06136824167	STEFANI.CSANTANA	STEFANI.CSANTANA
2614	t	04445107611	CLEITON.GCOSTA	CLEITON GONCALVES COSTA
2615	t	84381973291	STELLA.CSCHAEFER	STELLA JAQUELINE CASSIANO SCHAEFER
2616	t	02244465179	JULIANE.RSOUSA	JULIANE RIBEIRO DE SOUSA
2617	t	69324620100	TERESINHA.MAIA	TERESINHA MARTINS CARVALHO MAIA
2618	t	02564186327	fabio.aanjos	FABIO ANDRADE DOS ANJOS
2619	t	97386618115	ANGELA.OLIVEIRA	ANGELA CRISTINA SANTOS DE OLIVEIRA
2620	t	05850601180	lucas.oliveira	LUCAS SILVA DE OLIVEIRA
2621	t	26557108867	CONRADO.TJUNIOR	CONRADO TADEU DE GENNARO JUNIOR
2622	t	00260921238	MARCELO.CARNEIRO	MARCELO DE JESUS CARNEIRO
2623	t	01918046140	FRANCIELLE.GOMES	FRANCIELLY GOMES LEITE
2624	t	02301251152	NEUMINAIBI.SSOUZA	NEUMINAIBI SONAI AMADOR DA CRUZ SOUZA
2625	t	71062440153	JANSEN.RALVES	JANSEN WKEPERSON RIBEIRO ALVES
2626	t	04542234126	KETHLYN.LSANTOS	KETHLYN LAYANE GOMES DOS SANTOS
2627	t	71391070178	FABIO.HGUEDES	FABIO HENRIQUE GUEDES DE LIMA
2628	t	73273090120	marcos.rpereira	MARCOS RODRIGUES PEREIRA
2629	t	03783265118	WEULLER.MOREIRA	WEULLER MOREIRA DE OLIVEIRA
2630	t	03767342138	LUCAS.COSTA	LUCAS HENRIQUE NASCIMENTO COSTA
2631	t	03961732175	jessica.ssantos	JESSICA SABRINA DE MELO SANTOS
2632	t	02154037119	MAURO.MSANTOS	MAURO MELO SANTOS
2633	t	03538839131	JESSICA.CSANTOS	JESSICA DA COSTA SANTOS
2634	t	65158156115	VALMIR.DMENEZES	VALMIR DE MENEZES
2635	t	02581816139	JOSE.DSILVA	JOSE CARLOS DA SILVA
2636	t	00530977184	DAISLAN.TSALES	DAISLAN TIAGO LIRA SALES
2637	t	73661651153	WALTER.MJUNIOR	WALTER MORAES DE LIMA JUNIOR
2638	t	00901141160	FLAVIA.AVASCONCELOS	FLAVIA CRISTINA ARANTES VASCONCELOS
2639	t	72929820187	BRUNO.LAPA	BRUNO VIEIRA LAPA
2640	t	96017112104	RODRIGO.BSILVA	RODRIGO BOTELHO DA SILVA
2641	t	95219102168	HELIO.AJUNIOR	HELIO ANTONIO DE MORAIS JUNIOR
2642	t	04203812100	LETICIA.SFERRAO	LETICIA CANDIDA DA SILVA
2643	t	96645130163	PAULINO.JNETO	PAULINO JOSE DA SILVA NETO
2644	t	04062059142	JHONATAN.JALMEIDA	JHONATAN JESUS DE ALMEIDA
2645	t	77087321153	JULIO.RSOUZA	JULIO CESAR RODRIGUES SOUZA
2646	t	00904087131	DANIEL.GDUARTE	DANIEL GONCALVES DUARTE
2647	t	01992377197	ANDRE.PAIM	ANDRE LUIZ PAIM DE SOUZA
2648	t	04811697170	danilo.sgomes	DANILO DE SOUZA GOMES
2649	t	05872128100	lindenberg.moliveira	LINDENBERG MORAIS DE OLIVEIRA
2650	t	03627158660	BIANCA.MARTINS	BIANCA MARTINS DE SOUSA LIMA
2651	t	02800131160	KAMILLA.FSILVA	KAMILLA.FSILVA
2652	t	00331292157	JAMERSON.DIAS	JAMERSON DOS SANTOS DIAS
2653	t	73650609134	MICHELLY.PEREIRA	MICHELLY PEREIRA GONCALVES
2654	t	10396526624	GUSTAVO.CSOUTO	GUSTAVO CANDIDO SOUTO
2655	t	77265106172	ELISVALDO.LSOARES	ELISVALDO LIMA SOARES
2656	t	65011139387	WENDERSON.SOUSA	WENDERSON SOUZA OLIVEIRA
2657	t	73408450110	RAPHAEL.FBORGES	RAPHAEL FELIPE BORGES SALES
2658	t	01636050174	ANDREIA.MRODRIGUES	ANDREIA APARECIDA MATTOS RODRIGUES
2659	t	01929449160	DOGLAS.JUNIOR	DOGLAS JUNIOR LEMES SANTOS
2660	t	96282215234	SUELEN.SRABELO	SUELEN CRISTINA DE SOUSA RABELO
2661	t	02660383529	marcio.ssantana	MARCIO SOARES SANTANA DOS SANTOS
2662	t	64346714153	DELVANIA.RODRIGUES	DELVANIA.RODRIGUES
2663	t	08969144617	LUAN.MRODRIGUES	LUAN MALAQUIAS RODRIGUES
2664	t	69349096153	CRISTIAN.ECORREIA	CRISTIAN ELIAS CORREIA
2665	t	57052174120	WAGNER.RMOREIRA	WAGNER ROJAS MOREIRA
2666	t	12993668641	ARTHUR.CSANTOS	ARTHUR CARVALHO SANTOS
2667	t	03143028165	mikaelle.loliveira	MIKAELLE LORRANE GOMES DE OLIVEIRA
2668	t	02910754103	ADRIANA.ASANTANA	ADRIANA.ASANTANA
2669	t	05000248120	CAIO.VARAUJO	CAIO VIEIRA ARAUJO
2670	t	00001121197	KEICIANE.DSILVA	KEICIANE DIAS DA SILVA
2671	t	91539056104	ELIZETE.BSOUZA	ELIZETE BARBOSA DE SOUZA
2672	t	01086714105	FABRICIO.BMACHADO	FABRICIO BARRETO ALVES MACHADO
2673	t	05113960152	emilly.sluz	EMILLY DA SILVA LUZ
2674	t	00565650386	MARCONE.CSARDINHA	MARCONE CIRO SARDINHA
2675	t	53852699134	FERNANDO.FSILVA	FERNANDO FELIPE DA SILVA
2676	t	78209994387	SANDRA.CAZEVEDO	SANDRA CRISTINA DE JESUS AZEVEDO
2677	t	60277891310	luciene.amatos	LUCIENE DE ALENCAR MATOS
2678	t	51308746172	JANE.MSOBRINHO	JANE MARIA SOBRINHO
2679	t	00426207327	diego.mayres	DIEGO MIRANDA DO NASCIMENTO AYRES
2680	t	91524520691	GILMAR.JSILVA	GILMAR JOSE DA SILVA
2681	t	94843503134	SAHYRRA.FERRAZ	SAHYRRA KARULINA DE SOUZA
2682	t	16721557894	MARCOS.ROLIVEIRA	MARCOS ROBERTO OLIVEIRA PORSSIONATTO
2683	t	02018599127	wamiston.lmartins	WAMISTON LUCAS DE OLIVEIRA MARTINS
2684	t	70863987133	BRENDA.ASILVA	BRENDA AMORIM SILVA
2685	t	01109808160	RENATA.RSOUSA	RENATA RIBEIRO SOUSA
2686	t	69445524187	WESLEY.MBRITO	WESLEY MAGALHAES DE BRITO
2687	t	06137430677	DOUGLAS.SPRADO	DOUGLAS SILVA PRADO
2688	t	05306823386	JOYCE.GLIMA	JOYCE GONCALVES LIMA
2689	t	57566364120	marco.ldias	MARCO AURELIO LOPES DIAS
2690	t	03704672130	AUGUSTO.OLIVEIRA	AUGUSTO CANDIDO SOUSA DE OLIVEIRA
2691	t	70000147176	ANA.TEIXEIRA	ANA BEATRIZ COUTINHO TEIXEIRA
2692	t	45925127153	BENEDITO.CRUZ	BENEDITO DA SILVA CRUZ
2693	t	62906860115	ALEXANDRE.SILVA	ALEXANDRE DA SILVA
2694	t	10906982677	CAROLINA.ATC	CAROLINA SILVA ALVES
2695	t	83944389115	GUILHERME.SPOLI	GUILHERME.SPOLI
2696	t	90443144168	WANDERLEIA.NMARTINS	WANDERLEIA NUNES MARTINS
2697	t	89423852149	ANDRE.LSILVA	ANDRE LUIZ ALVES DA SILVA
2698	t	04705547110	ANDRESSA.CERQUEIRA	ANDRESSA CERQUEIRA
2699	t	03656492107	RENATO.SPENHA	RENATO SILVA CAPISTRANO PENHA
2700	t	73052116134	MOISES.NASCIMENTO	MOISES MAGALHAES DO NASCIMENTO
2701	t	00671324136	MARCIO.LEAL	MARCIO FERNANDES LEAL
2702	t	05105490630	DIEGO.ARODRIGUES	DIEGO ALEXANDER RODRIGUES
2703	t	00078765137	ANDRESSA.SPEREIRA	ANDRESSA THAIS SILVA PEREIRA
2704	t	13022730608	leonardo.casilva	LEONARDO DE CASTRO SILVA
2705	t	01398691151	WENDEL.VROCHA	WENDEL VILAS BOAS ROCHA
2706	t	05780104107	laisa.dnascimento	LAISA DUTRA DO NASCIMENTO
2707	t	99130009553	ANTONIO.CCRUZ	ANTONIO CARLOS NUNES CRUZ
2708	t	12037526601	GABRIELLA.FMOREIRA	GABRIELLA DE FATIMA MOREIRA
2709	t	83141677387	FRANCISCO.ROLIVEIRA	FRANCISCO DE ASSIS OLIVEIRA RODRIGUES
2710	t	65023757315	daniel.gsilva	DANIEL JORGE GONCALVES SILVA
2711	t	00404637370	simone.amartins	SIMONE AMORIM MARTINS
2712	t	06519718106	Katielly.KSoares	KATIELLY KETHEREYN SOARES
2713	t	00732016185	CRISTIANO.MOURA	CRISTIANO SILVA SANTOS DE MOURA
2714	t	02414427108	IARA.NSILVA	IARA NUNES DA SILVA
2715	t	68959494100	ALEX.CSOUZA	ALEX CARDOSO CRUZ DE SOUZA
2716	t	99153173104	thiago.osousa	THIAGO OLIVEIRA SOUSA
2717	t	06412322175	rosa.cmoran	ROSA CRISTINA BERNARDO MORAN
2718	t	05412262155	maycon.gcarvalho	MAYCON DOUGLAS ROSA GONCALVES DE CARVALHO
2719	t	02884784101	AILTON.JSILVA	AILTON DE JESUS SILVA
2720	t	73142581120	rodolfo.agomes	RODOLFO ALBERTO GOMES
2721	t	05376850307	indra.lmendes	INDRA LOPES MENDES
2722	t	01555998283	lucila.bchanevi	LUCILA BUSTAMANTE CHANEVI
2723	t	87357348172	ELAINE.CSANTOS	ELAINE CORREA DOS SANTOS
2724	t	01804244325	LUIZ.ESILVA	LUIZ EDNALDO DA SILVA
2725	t	03540814116	grazielle.slima	GRAZIELLE SANTOS LIMA
2726	t	70290903149	ricardo.ntavares	RICARDO NEUTO TAVARES
2727	t	99968991104	ADRIANA.GONCALVES	ADRIANA DE ALMEIDA GONCALVES
2728	t	56552629168	rinaldo.aleao	RINALDO APARECIDO JULIO LEAO
2729	t	68731167268	ANDREA.ASILVA	ANDREA ALVES DA SILVA
2730	t	02477737198	ALESSANDRA.MATA	ALESSANDRA DA MATA SILVA
2731	t	75576970144	yago.hsilva	YAGO HENRIQUE DA SILVA
2732	t	00757384307	danubia.svieira	DANUBIA SANTOS VIEIRA GONCALVES
2733	t	05734176109	cleyson.dmartins	CLEYSON DENYS MARTINS LARA
2734	t	01894076621	MATHEUS.USILVA	MATHEUS LUCAS UMBERTO DA SILVA
2735	t	84912545368	keila.rrodrigues	keila.rrodrigues
2736	t	06484291136	amanda.msousa	AMANDA DIVINA MACHADO DE SOUSA
2737	t	70013005197	ana.lmendes	ANA LAURA PALMEIRA MENDES
2738	t	21703308875	AGATA.OBRECCIANI	AGATA OLIVEIRA BRECCIANI
2739	t	02910992110	CARLOS.RSOUZA	CARLOS RICHELY DE SOUZA
2740	t	70605360197	ADRIANO.FOLIVEIRA	ADRIANO LINCOLN FERREIRA DE OLIVEIRA
2741	t	01784418137	CARLOS.ABEZERRA	CARLOS ALBERTO BEZERRA
2742	t	01616975342	rayssilene.cvalente	RAYSSINELLE CASSIA DA SILVA VALENTE
2743	t	08371319606	camila.asantos	CAMILA APARECIDA DOS SANTOS
2744	t	02184122157	higor.loliveira	HIGOR LEONARDO TELES DE OLIVEIRA
2745	t	46063200100	GERSON.BSIQUEIRA	GERSON BENEDITO SIQUEIRA
2746	t	00560318154	DIOGO.BPEREIRA	DIOGO BENON CARNAVAL PEREIRA DA ROCHA
2747	t	04849971148	MATHEUS.FSOARES	MATHEUS FERREIRA SOARES
2748	t	03947512139	veronica.amachado	VERONICA ALVES MACHADO
2749	t	02756892122	alinne.cbastos	ALINNE VIANA COSTA BASTOS
2750	t	04321016169	andre.hsilva	ANDRE HENRIQUE SILVA DE ARAUJO
2751	t	70288298128	fabiana.mcavalcante	FABIANA MOURA CAVALCANTE
2752	t	72773006100	tatiane.dcavalcante	TATIANE DUARTE CAVALCANTE
2753	t	01735415189	renan.mribeiro	RENAN MACHADO RIBEIRO
2754	t	02207741109	KAMILLY.SOLIVEIRA	KAMILLY.SOLIVEIRA
2755	t	71014686164	andreina.scarvalho	andreina.scarvalho
2756	t	88433102168	josimar.msilva	JOSIMAR MODESTO DA SILVA
2757	t	33695385120	lucila.jsilva	LUCILA JESUS DA SILVA
2758	t	01361591684	MARCOS.ROCHA	MARCOS AURELIO TAVARES ROCHA
2759	t	63252775204	FABIO.GSOUZA	FABIO GONCALVES DE SOUZA
2760	t	03090178150	luiza.gmarques	LUZIA GALVAO MARQUES
2761	t	60398609365	EDEN.DFIGUEIREDO	EDEN DANILO PAIXAO FIGUEREDO
2762	t	00811472361	adriana.scosta	ADRIANA SILVA COSTA
2763	t	04138186247	cleiza.malmeida	CLEIZA TAIELLE MOTA DE ALMEIDA
2764	t	85450154100	KEMMER.SSILVA	KEMMER SOBREIRA SILVA
2765	t	00790724103	helder.marantes	HELDER MARTINS ARANTES
2766	t	51865670278	cristiane.fsilva	CRISTIANE FEITOZA DA SILVA CHIQUETTO
2767	t	05432169185	NATHALIA.PSOUSA	NATHALIA PEREIRA DE SOUSA VAZ
2768	t	08977221676	guilherme.acarvalho	GUILHERME ALVES DE CARVALHO
2769	t	05201315178	nathalia.bpereira	NATHALIA BRENDA DE SOUSA PEREIRA
2770	t	05112817143	gessyana.mcastro	GESSYANE MARCOS DE CASTRO
2771	t	69253978104	elton.ssantana	ELTON SOUZA SANTANA
2772	t	60518144348	ana.ssilva	ANA NUBIA SANTOS DA SILVA
2773	t	05206853186	erik.asilva	ERIK ALEXANDER DE LIMA SILVA
2774	t	04342990148	MARCIO.VLIMA	MARCIO VIEIRA LIMA
2775	t	03447834137	ANDRE.BSILVA	ANDRE BENJAMIM DOS ANJOS SILVA
2776	t	84394056268	adson.fribeiro	ADSON FRANCO RIBEIRO
2777	t	03852887194	hugo.slima	HUGO DE SOUSA LIMA
2778	t	02450940162	djalma.rocha	DJALMA ANDERSON MOURA ROCHA
2779	t	01272871169	thiago.lsouza	THIAGO LUIZ DE SOUZA SILVEIRA
2780	t	70398132119	karen.mrocha	KAREN MICHELLY SANTOS ROCHA
2781	t	02043269232	alan.fsanto	ALAN FURTADO DO ESPIRITO SANTO
2782	t	04620177105	FABIANA.FMATOS	FABIANA FREIRE MATOS
2783	t	04592076362	diocelho.ssantos	DIOCELHO DOS SANTOS E SANTOS
2784	t	00973759267	ENOCH.SMACEDO	ENOCH SILAS ARAGAO MACEDO
2785	t	95319603134	flavio.rsantos	FLAVIO AURELIO ROSA DOS SANTOS
2786	t	06090619175	EDNA.KATRINA	EDNA KATRINA PISSOLATO MORAIS
2787	t	96302950163	erika.freitas	ERIKA CRISTINA DE FREITAS
2788	t	03921179165	CAROLINA.GOLIVEIRA	CAROLINA GONCALVES OLIVEIRA
2789	t	02810306184	euclides.rnetto	EUCLIDES DOS REIS NETTO
2790	t	22450424334	ZILDA.GMARTINS	ZILDA GAMA MARTINS
2791	t	61994103604	luis.gbenichio	luis.gbenichio
2792	t	70012100137	vanessa.lsilva	VANESSA LOURENCO DA SILVA
2793	t	42490464191	luzimeire.mferreira	LUZIMEIRE MARQUES FERREIRA
2794	t	83128280100	daniel.crocha	DANIEL CABRAL DA ROCHA
2795	t	07019972144	luciana.lsousa	LUCIANA LEANDRO DE SOUSA
2796	t	52948781272	leonardo.dsouza	LEONAM DARLISSON FERREIRA DE SOUZA
2797	t	60952120305	karoline.ssa	KAROLINE SANTOS SA
2798	t	12692047770	maurilio.spereira	MAURILIO CABREIRA SANTOS PEREIRA
2799	t	03498168169	jose.agomes	JOSE ANTONIO GOMES DA SILVA FILHO
2800	t	70393586111	vanessa.mevaristo	VANESSA MARIA MARTINS EVARISTO
2801	t	03380036145	sheynni.falmeida	SHEYNNI FAYFER DE ALMEIDA AYOLFI
2802	t	06927401121	rayssa.fmacedo	RAYSSA FERNANDES MACEDO
2803	t	01318239141	paulo.balves	PAULO VICTOR BERTULINO ALVES
2804	t	00928588130	ALESSANDRA.MSILVA	ALESSANDRA MARIA DA SILVA
2805	t	03050454156	talles.balves	TALLES BRENO MOREIRA ALVES
2806	t	01603461183	deborah.kvieira	DEBORAH KELLY VIEIRA
2807	t	04680573155	FABIO.SBRUNO	FABIO PEREIRA DA SILVA BRUNO
2808	t	04470138150	JOABE.SMELO	JOABE DA SILVA MELO
2809	t	91230101187	RENATA.JMEDEIROS	RENATA JOANA MEDEIROS
2810	t	60183992377	sarah.csantos	SARAH CARNEIRO DOS SANTOS
2811	t	71986251187	lidiane.ssantos	lidiane.ssantos
2812	t	05476807918	rodrigo.pmelo	rodrigo.pmelo
2813	t	72192194153	giselle.acamargo	giselle.acamargo
2814	t	71972196120	marcio.jcosta	marcio.jcosta
2815	t	61063860130	cristiana.soliveira	cristiana.soliveira
2816	t	00161161197	michele.diniz	michele.diniz
2817	t	55177050149	edson.rmartins	EDSON JUNIOR RAIA MARTINS
2818	t	02474261133	paulo.sbagageiro	PAULO SERGIO BAGAGEIRO
2819	t	02637478322	TAYNARA.SSOUSA	TAYNARA SILVA SOUSA
2820	t	03829802129	paula.fmontanari	PAULA FRACISNETI TEIXEIRA MONTANARI
2821	t	00446310140	pablo.gsantos	PABLO GREGORY DOS SANTOS
2822	t	46415768824	lucas.rsantana	LUCAS RENAN COSTA SANTANA
2823	t	39480410168	luciano.lsilveira	LUCIANO LOBO SILVEIRA
2824	t	03105438190	vicente.mfilho	VICENTE MESQUITA MARTINS FILHO
2825	t	02575454174	douglas.loliveira	DOUGLAS LIMA DE OLIVEIRA
2826	t	05479743376	rayssa.jcoutinho	rayssa.jcoutinho
2827	t	69512469120	maria.aoliveira	MARIA HELENA MARQUES ANDRADE DE OLIVEIRA
2828	t	64932877153	PAULO.AMARO	PAULO AMARO DE ARAUJO
2829	t	00097149365	bruno.pereira	BRUNO LEONARDO CONCEICAO PEREIRA
2830	t	90845390104	ANA.FONTANEZI	ANA CLAUDIA FONTANEZI
2831	t	02093725169	ludmila.asilva	LUDMILA AGUIAR DA SILVA
2832	t	01993133186	MARIA.PSOUSA	MARIA DA PENHA BORGES DE SOUSA
2833	t	60182949311	tarcila.gveiga	TARCILA MENEZES GUALHARDO VEIGA
2834	t	05737219399	camilla.pmartins	CAMILLA PEREIRA MARTINS
2835	t	79484760163	RONDINELLY.MSILVA	RONDINELLY MESQUITA DA SILVA
2836	t	25424688349	tatiana.cgomes	TATIANA CARNEIRO GOMES
2837	t	03174701163	AUGUSTO.DNASCIMENTO	AUGUSTO DE DEUS NASCIMENTO
2838	t	28005146809	sergio.mcarval	SERGIO MURILO MATOS DE CARVALHO
2839	t	04694890140	JOAO.NEGREIROS	JOAO LUCAS MARTINS DE NEGREIROS
2840	t	00957578210	cleisse.lsilva	CLEISSE LAIANA SOUZA E SILVA
2841	t	75183927120	SARAH.SOLIVEIRA	SARAH DE SOUZA OLIVEIRA
2842	t	03834378143	jose.bsilva	JOSE INACIO BRITO DA SILVA
2843	t	01770056378	diego.cmeireles	DIEGO COSTA MEIRELES
2844	t	02297822138	thays.msilva	THAYS ALESSANDRA MOREIRA SILVA
2845	t	86502379249	ANDERSON.SMOTA	ANDERSON DOS SANTOS MOTA
2846	t	58890300949	camilo.jcomerlatto	CAMILO JOSE COMERLATTO
2847	t	49517244134	benedito.sbrito	BENEDITO WALDECIR SOARES DE BRITO
2848	t	05164029197	anny.tsouza	ANNY KAROLINNY TAQUES DE SOUZA
2849	t	04256234160	pedro.hsilva	PEDRO HENRIQUE FERREIRA DA SILVA
2850	t	04169814100	anapaula.csilva	ANA PAULA CHAGAS DA SILVA
2851	t	70276057163	elismar.rmarques	ELISMAR REZENDE MARQUES
2852	t	05731420173	carla.smoura	CARLA DE SOUSA MOURA
2853	t	60512303355	milena.smachado	MILENA DE SOUSA SILVA MACHADO
2854	t	60673381307	elisvaldo.costa	ELISVALDO COSTA
2855	t	04874858155	daniel.dsilva	DANIEL DOURADO DA SILVA
2856	t	05193821103	kesia.nrodrigues	KESIA NATHIENIE SILVA RODRIGUES
2857	t	01757866230	elieth.scastro	ELIETH DOS SANTOS DE CASTRO
2858	t	80320996115	adevair.fsilva	ADEVAIR FRANCISCO DA SILVA
2859	t	05052611625	danilo.nsilva	DANILO NICHOLAS DA SILVA
2860	t	00358947170	bruno.asouza	BRUNO ANTUNES DE SOUZA
2861	t	02309542199	JONATHAN.FOLIVEIRA	JONATHAN WALLASON FURTADO DE OLIVEIRA
2862	t	02572212390	GILVANETE.SCOSTA	GILVANETE SANTOS SILVA COSTA
2863	t	95037390206	GEORDANE.HFONSECA	GEORDANE HOLANDA FONSECA
2864	t	02603345109	stephanie.martins	STEPHANIE VIEIRA MARTINS
2865	t	94749159204	uanderson.soliveira	UANDERSON DOS SANTOS OLIVEIRA
2866	t	03325007139	julio.cbrito	JULIO CESAR DA SILVA BARBOSA BRITO
2867	t	86301411153	carlos.tsantos	CARLOS EDUARDO TEIXEIRA DOS SANTOS
2868	t	70592495167	joquebede.mcosta	JOQUEBEDE MARTINS DA COSTA
2869	t	70775073199	vivyen.mneves	VIVYEN MOREIRA NEVES
2870	t	04820572113	TAINARA.FSOUSA	TAINARA FERREIRA DE SOUSA
2871	t	99293633353	MARCIEELA.RESENDE	MARCIEELA RESENDE DE MENESES BEZERRA
2872	t	02782915118	RENATA.SFIDELES	RENATA SERPA FIDELES
2873	t	01405504102	marcos.rchaves	MARCOS RABELO CHAVES
2874	t	05373517100	stefane.lsouza	STEFANE LEMOS SOUZA
2875	t	31128306875	WANDER.TMOREIRA	WANDER TADEU TEODORO MOREIRA
2876	t	01059788411	FRANCISCO.ANETO	FRANCISCO AUGUSTO DE QUEIROZ NETO
2877	t	85387215187	CATIUCIA.SILVA	CATIUCIA FERNANDES CAETANO DA SILVA
2878	t	00789096196	GABRIELA.LSILVA	GABRIELA LUIZ DE CARVALHO SILVA
2879	t	01597317128	bruno.fandrade	BRUNO FAGUNDES DE ANDRADE
2880	t	03485497177	anna.cferreira	ANNA CARLA FERREIRA CAMILO
2881	t	79432042120	orlando.sdespacho	ORLANDO DOS SANTOS BOM DESPACHO
2882	t	75427001115	frederick.aoliveira	FREDERICK ALEXANDER CABRAL DE OLIVEIRA
2883	t	00704010399	MARCOS.FIGUEIREDO	MARCOS ANDRADE FIGUEIREDO
2884	t	03660768170	VAGNER.SRIBEIRO	VAGNER SANTOS RIBEIRO
2885	t	03331762350	joedson.bviana	JOEDSON BORGES VIANA
2886	t	04192774399	BIANCA.PCORREIA	BIANCA PAIVA CORREIA
2887	t	08188154776	RAFAEL.TLEITE	RAFAEL TIAGO DE LELLES LEITE
2888	t	70049523120	alexandre.borba	ALEXANDRE CESAR BORBA BRANDAO
2889	t	01830829157	ricardo.aferreira	RICARDO ARIMURA FERREIRA
2890	t	09248744656	JAQUELINE.NSANTOS	JAQUELINE NUNES SANTOS
2891	t	01853926604	DAMIRES.TLEMES	DAMIRES VIRGINIA TOME LEMES
2892	t	01956188150	CLEMOR.VJUNIOR	CLEMOR VELOSO DE OLIVEIRA JUNIOR
2893	t	94255024120	MARIANO.TAVARES	JOSE MARIANO VIEIRA TAVARES
2894	t	73290122115	DANIEL.PCOSTA	DANIEL PEREIRA DA COSTA
2895	t	61568541341	lucas.smarques	LUCAS DE SOUSA MARQUES
2896	t	70186818122	isadora.soliveira	ISADORA SOUSA DE OLIVEIRA
2897	t	03396197160	ana.crodrigues	ANA CLAUDIA CARMO DOS REIS RODRIGUES
2898	t	96437707291	ROMARIO.RSOUZA	ROMARIO ROBERTO DE SOUZA
2899	t	98561057220	GISELE.RARAUJO	GISELE REGINA SOARES DE ARAUJO
2900	t	01105129101	samantha.bfortunato	SAMANTHA BORGES FORTUNATO
2901	t	03490282302	caroline.aseveriano	CAROLINE AMORIM SEVERIANO
2902	t	06810441107	LUCAS.BSOUZA	LUCAS IRUAN BITENCOURT DE SOUZA
2903	t	61629767344	amanda.efonseca	AMANDA EMILY DO NASCIMENTO FONSECA
2904	t	39631613100	TADEU.SCOSTA	TADEU SANTOS COSTA
2905	t	63188490178	ALFREDO.MONTEIRO	ALFREDO MONTEIRO DA SILVA
2906	t	03274123218	kettellyn.lbezerra	KETTELLYN LOPES BEZERRA
2907	t	04057977104	matheus.marques	MATHEUS ANDRE SOUSA MARQUES
2908	t	00811188213	janayna.foliveira	JANAYNA FORTUNATO DE OLIVEIRA
2909	t	04386375341	thalison.fmuniz	THALISON GABRIEL FONSECA MUNIZ
2910	t	03265721124	rafael.bsantos	rafael.bsantos
2911	t	04087997308	helainne.crodrigues	HELAINNE KAROLINI CONCEICAO RODRIGUES
2912	t	61390981304	ana.mviana	ANA MARIA OLIVEIRA VIANA
2913	t	70022219145	LARISSA.SCARVALHO	LARISSA MARIA SILVA CARVALHO
2914	t	01715515161	karollynee.spassos	KAROLLYNEE STEFANY DE SOUZA PASSOS
2915	t	70684415135	mariana.gnascimento	MARIANA GONCALVES NASCIMENTO
2916	t	11114658430	priscilla.blima	PRISCILLA KELLY BARBOSA LIMA
2917	t	02707517186	fabio.wgomes	fabio.wgomes
2918	t	05909403176	cristiane.vmartins	CRISTIANE VITORIA DOS SANTOS MARTINS
2919	t	01923934139	MARIANA.AMORAIS	MARIANA ALVES DE MORAIS
2920	t	67636039249	ERIVANIA.SPINHEIRO	ERIVANIA DOS SANTOS PINHEIRO
2921	t	03728221180	JESSICA.NSILVA	JESSICA NUNES SILVA
2922	t	57549710287	josymara.msouza	JOSYMARA MONICA AGUIAR DE SOUZA
2923	t	02647228175	dhuliano.acastro	DHULIANO ALVES DE CASTRO
2924	t	05293334301	nilmarques.mgomes	NILMARQUES MUNIZ GOMES
2925	t	01752302346	angelica.rsilva	ANGELICA ROCHA DA SILVA
2926	t	99562960315	alan.viana	alan.viana
2927	t	01962796159	ANA.LINHARES	ANA PAULA LINHARES DA SILVA
2928	t	01010485300	PATRICIA.VBOAS	PATRICIA CRISTINA SILVA VILAS BOAS
2929	t	60699992311	TAYNARA.CARAUJO	TAYNARA CARVALHO DE ARAUJO
2930	t	71958983187	francini.ferreira	FRANCINI FERREIRA
2931	t	04101318140	JULIANA.SDAMASCENO	JULIANA STHEFANY DAMASCENO
2932	t	03863025539	DANIELLI.CDANTAS	DANIELLI CRUZ DANTAS
2933	t	09813826738	vilson.pjunior	VILSON PEREIRA BARCELLOS JUNIOR
2934	t	03996528175	REGIS.SSILVA	REGIS SOUSA SILVA
2935	t	03874241130	micaelly.afranca	MICAELLY ARAUJO FRANCA
2936	t	70088412130	ANNA.CDIAS	ANNA CLAUDIA PEREIRA DIAS
2937	t	28561554134	SIDNEY.CAETANO	SIDNEY RODRIGUES CAETANO
2938	t	74095200197	pablo.ssousa	PABLO SILVA MARTINS DE SOUZA
2939	t	06404317125	joiara.sbadu	JOAIRA DA SILVA ARRAIS BADU
2940	t	02939529140	keyla.cmalheiros	KEYLA CRISTINA MALHEIROS
2941	t	44264941172	EMANUEL.CPINHEIRO	EMANUEL DE CASTRO PINHEIRO
2942	t	01567245374	zilmara.oconceicao	ZILMARA OLIVEIRA DA CONCEICAO
2943	t	61295785390	jessica.kfonseca	JESSICA KEYSSE SERRA FONSECA
2944	t	23058042884	manoella.mlopes	MANOELLA MEDRADO LOPES
2945	t	01831454106	wanielyda.smoura	WANIELYDA DE SOUZA MOURA
2946	t	07060703136	pablo.vcurado	PABLO VINICIUS VARELO CURADO
2947	t	70014915197	lucas.cferreira	LUCAS CARVALHO FERREIRA
2948	t	05856143183	THIAGO.ARODRIGUES	THIAGO HENRIQUE DO ARTE RODRIGUES
2949	t	87458292334	daniel.msobrinho	DANIEL DE JESUS MONTEIRO SOBRINHO
2950	t	70854971149	ROSANGELA.BSANTOS	ROSANGELA BENEDITA DOS SANTOS
2951	t	03125898161	dennis.amesquita	DENNIS DE ABREU SALLES MESQUITA
2952	t	76104125115	sandro.azevedo	SANDRO AZEVEDO AMERICANO DO BRASIL
2953	t	01611897173	BRUNO.CRODRIGUES	BRUNO CESAR SILVA RODRIGUES
2954	t	00982763107	DOMAILE.SDAMASCENO	DOMAILE SILVA DAMASCENO
2955	t	24901636120	armando.crosa	ARMANDO CUSTODIO ROSA
2956	t	05436764126	JHONNY.CFIGUEIREDO	JHONNY CAMPOS FIGUEIREDO
2957	t	04488169627	kennede.assis	KENNEDE DE FARIA ASSIS
2958	t	03034228155	brunno.sambrosio	BRUNNO HENRIQUE SOARES AMBROSIO
2959	t	06886141163	gabriela.sfernandes	GABRIELA SOUZA FERNANDES
2960	t	04474437551	lucas.pcosta	LUCAS VINICIUS PESSOA COSTA
2961	t	04431107193	victor.hsantos	VICTOR HUGO SOUSA SANTOS
2962	t	00238088103	odinalva.pborges	ODINALVA PAULA VIANA BORGES
2963	t	97569704134	klayton.cfernandes	KLAYTON CAMPOS FERNANDES
2964	t	13479366619	karina.obarbosa	KARINA DE OLIVEIRA BARBOSA
2965	t	00399444335	guioberto.apenha	guioberto.apenha
2966	t	02928571108	ARTHUR.FMELO	ARTHUR FABRICIO SPINDOLA DE MELO
2967	t	04442671664	sarah.czaidan	SARAH CAPANEMA ZAIDAN
2968	t	04077653100	FERNANDO.PSANTOS	FERNANDO PERES DOS SANTOS
2969	t	75163217100	gabriel.samorim	GABRIEL DE SOUSA AMORIM
2970	t	04883965147	rosangela.gsousa	ROSANGELA GOMES DE SOUSA
2971	t	03455717110	gabriela.rsampaio	GABRIELA CYNTIA RAMALHO SAMPAIO
2972	t	66223792387	ALEXSANDRO.CSANTOS	ALEXSANDRO COELHO SANTOS
2973	t	04900895180	rodrigo.jsantos	RODRIGO DE JESUS PEREIRA DOS SANTOS
2974	t	03653421144	andre.rmarques	ANDRE REIS MASCARENHA MARQUES
2975	t	53275055100	jose.daraujo	JOSE DIVINO DE ARAUJO
2976	t	11611020603	vitor.mboas	VITOR MENDONCA VILAS BOAS
2977	t	03475364174	gabriela.nmelo	GABRIELA KATIUCIA NUNES DE MELO
2978	t	01083380109	JOAO.CSILVA	JOAO CARLOS PEREIRA DA SILVA
2979	t	05149486124	paulo.darwilee	PAULO DARWILEE DE SOUZA
2980	t	70062335693	daniel.rsilva	DANIEL MARCOS RAMOS SILVA
2981	t	87921200168	ROBSON.GMELO	ROBSON GOMES DE MELO
2982	t	04701706116	CAMILA.ATAIDES	CAMILA ALMEIDA ATAIDES
2983	t	60528295322	KELLY.CGOMES	BRUNA KELLY CUTRIM GOMES
2984	t	00424441101	CYNTIA.SILVA	CYNTIA CUNHA DA SILVA
2985	t	41067946837	tais.bmartins	tais.bmartins
2986	t	60539266396	thiago.rcosta	THIAGO RONYERISSON SILVA COSTA
2987	t	83956743253	keila.ssilva	KEILA DOS SANTOS BRAGA DA SILVA
2988	t	05369774374	wilma.ogoncalves	WILMA DE OLIVEIRA GONCALVES
2989	t	02666194108	rafael.tsilva	RAFAEL TELES SOARES SILVA
2990	t	00967369177	rutiane.loliveira	rutiane.loliveira
2991	t	66559090663	maria.dpacheco	MARIA DEOLINDA PACHECO
2992	t	04091664130	suellen.gomes	SUELLEN MARTINS GOMES
2993	t	02943081302	LEANDRO.FGRACA	LEANDRO CORDEIRO FONTENELLE GRACA
2994	t	06934566122	weverton.msilva	WEVERTON MATHEUS SILVA
2995	t	70681942142	annie.ktavares	ANNIE KAROLINIE OLIVEIRA TAVARES
2996	t	05998995147	monise.rsilva	MONISE RIBEIRO SILVA
2997	t	60159455324	patricia.aleite	PATRICIA ALMEIDA LEITE
2998	t	00268939241	FABRICIO.SCOSTA	FABRICIO SILVA DA COSTA
2999	t	05260084152	ANA.FSILVA	ANA CLAUDIA FERNANDES SILVA
3000	t	04954390109	luiz.hpatury	LUIZ HENRIQUE PEREIRA DA SILVA PATURY
3001	t	11839541660	DENNIS.RCICERO	DENNIS ROGER RIBEIRO CICERO
3002	t	06499470619	pablo.arodrigues	PABLO ALVES RODRIGUES
3003	t	06945714314	lourena.ssantos	LOURENA MARIA SILVA SANTOS
3004	t	04753566323	andressa.osilva	ANDRESSA MAGDA OLIVEIRA SILVA
3005	t	03537780109	gleison.rsilva	GLEISON RENER DA SILVA
3006	t	01232943126	RONDINELE.FRANCA	RONDINELE DE FRANCA QUIXABEIRA
3007	t	04238033361	victor.psantos	VICTOR HUGO PASSOS DOS SANTOS
3008	t	01521010129	danniely.silvestre	DANNIELY DA SILVA CAMPOS SILVESTRE
3009	t	02852223252	vanessa.wreis	VANESSA WITHS DOS REIS
3010	t	86919822253	aline.cmoura	ALINE CRISTINA SILVA DE MOURA ALMEIDA
3011	t	01842247131	menite.asantos	MENITE EMANUELA AZEVEDO DOS SANTOS
3012	t	87943328191	GESIEL.MCOTRIM	GESIEL MARCOS COTRIM
3013	t	81676778268	mario.onofre	MARIO ROMULO MENEZES ONOFRE
3014	t	70183078136	brenno.ssilva	brenno.ssilva
3015	t	04954169160	willian.mmoraes	WILLIAN MARQUES DE MORAES
3016	t	03604083117	THAIS.CSAMPAIO	THAIS COSTA SAMPAIO
3017	t	03901168150	tatiane.balmeida	TATIANE BUENO DE ALMEIDA
3018	t	70375419152	JESSICA.PEREIRA	JESSICA PEREIRA DOS SANTOS
3019	t	81523505168	RENATA.RIBEIRO	RENATA RIBEIRO
3020	t	04451395100	amanda.ataide	amanda.ataide
3021	t	00933051255	ARILTON.RSOARES	ARILTON RIBEIRO SOARES
3023	t	01390957136	michele.lsilva	MICHELE LINO VITURIANO SILVA
3024	t	01489679146	jaldeivid.pgomes	JALDEIVID DOS PRAZERES GOMES
3025	t	03024545190	willian.csantos	WILLIAN CRUZ SANTOS
3026	t	80259588172	jocelia.fsobrinha	JOCELIA FELICIANO SOBRINHA
3027	t	06539466358	robert.rcosta	robert.rcosta
3028	t	77162102387	gilson.spereira	gilson.spereira
3029	t	08229853657	roberta.fsilva	ROBERTA FERREIRA VILELA SILVA
3030	t	02163068170	alessandra.araujo	ALESSANDRA FRANCISCO ARAUJO DAMASCENO
3031	t	04818299111	elica.asilva	ELICA FERNANDA ABREU SILVA
3032	t	70151340129	bruna.scunha	BRUNA SOARES DA CUNHA
3033	t	83717811304	JOSE.SLIMA	JOSE CARLOS DA SILVA LIMA
3034	t	98380834100	LEONARDO.RNETO	LEONARDO RIBEIRO PROTESTATO NETO
3035	t	05919438169	JENNIFFER.MDADALT	JENNIFFER MARQUEZI DADALT
3036	t	73486604104	CRISELEN.QUEIROZ	CRISELEN FERNANDES DE QUEIROZ
3037	t	64521419372	MARCIO.RMATOS	MARCIO ROBERTO MORAES MATOS
3038	t	90503163953	marco.hjaeger	MARCO HENRIQUE JAEGER
3039	t	02132780226	eugenio.paganini	EUGENIO MATEUS PAGANINI
3040	t	61913278115	andre.samaral	ANDRE SILVEIRA DO AMARAL
3041	t	01168620171	margarete.asantos	MARGARETE ALVES DOS SANTOS
3042	t	61040528350	maria.dcosta	MARIA DARLENI ARAUJO COSTA
3043	t	06988207384	marcelo.flira	MARCELO FERREIRA LIRA
3044	t	61298407303	thaynara.correa	THAYNARA COSTA CORREA
3045	t	05576418530	jonathas.rferreira	JONATHAS ROBERIO DIAS FERREIRA
3046	t	51054833168	cledison.ogoncalves	CLEDISON OVIDIO GONCALVES
3047	t	39223156823	vivian.fmar	VIVIAN FIGUEIREDO DO MAR
3048	t	70328860190	sarah.rbarboza	SARAH RODRIGUES BARBOZA
3049	t	07348446620	vanessa.asantos	VANESSA ADRIANA SANTOS
3050	t	06512622162	ana.jsilva	ANA CLARA DE JESUS SILVA ALVES
3051	t	96177489249	NATIELEM.SALVES	NATIELEM DOS SANTOS ALVES
3052	t	44834005844	JULIA.PSOARES	JULIA PORTO MORGADO SOARES
3053	t	52179141149	VLADIMIR.FREITAS	VLADIMIR LOURENCO DE FREITAS
3054	t	99130122104	JACQUELINE.MORETTI	JACQUELINE HELENA MORETTI DA SILVA
3055	t	02390773184	fernanda.gcampos	FERNANDA GODOI DE CAMPOS
3056	t	88039560187	gerusa.jaraujo	GERUSA JULIO ALVES DE ARAUJO
3057	t	00117680109	glaucia.soliveira	GLAUCIA SANTOS OLIVEIRA
3058	t	70541835173	JOSYANE.SFERNANDES	JOSYANE PORTELA DOS SANTOS FERNANDES
3059	t	00269315160	CLEIDIANE.DSILVA	CLEIDIANE DIAS DA SILVA
3060	t	04736890142	JACKELINE.NSILVA	JACKELINE NAYARA SILVA CONCEICAO
3061	t	02556485108	julliane.grangeiro	JULLIANE MARIA DOS SANTOS GRANGEIRO
3062	t	60356585395	gustavo.vmuniz	gustavo.vmuniz
3063	t	03425771310	fabio.hsilva	FABIO HENRIQUE DA CUNHA SILVA
3064	t	02089187280	vinicius.rpessoa	vinicius.rpessoa
3065	t	03404332130	RYLLER.OBARBOSA	RYLLER OLIVEIRA BARBOSA
3066	t	92045006115	janaina.bcosta	JANAINA BARBOSA DA COSTA
3067	t	71738177149	LEILA.CMORAES	LEILA CAMPOS RIBEIRO DE MORAIS
3068	t	03885962101	kamila.mrezende	KAMILA DOS SANTOS MAGALHAES REZENDE
3069	t	03011677107	vitoria.faraujo	VITORIA CAROLINE FELIPE ARAUJO
3070	t	04901200160	kamila.ldrumond	KAMILA LEMOS DRUMOND
3071	t	04720591140	jose.csouza	JOSE JUNIOR CARVALHO DE SOUZA
3072	t	84632569372	IGOR.STOMAZ	IGOR SALGADO TOMAZ
3073	t	01182516190	cristiana.fmarques	CRISTIANA FERREIRA MARQUES
3074	t	03955328171	warley.asantos	WARLEY ALVES SANTOS
3075	t	05275582145	gabriel.msilva	GABRIEL MARQUES DA SILVA
3076	t	07085560193	patrick.olima	PATRICK EDUARDO OLIVEIRA DE LIMA
3077	t	05425738145	marcos.malmeida	MARCOS JUNIO MAGALHAES DE ALMEIDA
3078	t	00422954160	driely.rguimaraes	DRIELY RIBEIRO GUIMARAES
3079	t	51686201168	angelo.nneto	ANGELO PEREIRA NUNES NETO
3080	t	70259179159	ANA.CAROLINE	ANA CAROLINE ARAUJO DA SILVA
3081	t	22518393803	JOAO.ELISEU	JOAO RENATO ELISEU
3082	t	05701505154	graziana.alves	GRAZIANA DE AMORIM ALVES
3083	t	01508084106	LORAYNNE.MSOUSA	LORAYNNE MONIZA DE OLIVEIRA SOUSA
3084	t	00975932144	CARLOS.RCHAGAS	CARLOS ANTONIO DA ROCHA CHAGAS
3085	t	05109228132	joao.rsantos	JOAO PEDRO RODRIGUES SANTOS
3086	t	18096125869	LEANDRO.QUINTILIANO	LEANDRO QUINTILIANO
3087	t	00730348946	ROBERTO.JUNIOR	ROBERTO CARLOS DE SOUSA JUNIOR
3088	t	71895515149	ANDRE.ROGERIO	ANDRE LINO ROGERIO
3089	t	27018148120	LINO.TALVES	LINO TADEU TAVARES ALVES
3090	t	71380221153	JONILSON.SILVA	JONILSON CELESTINO DA SILVA
3091	t	03875618114	ITALO.MIRANDA	ITALO GUSTAVO MIRANDA MELO
3092	t	03861559170	MARIO.HJUNIOR	MARIO HAYASAKI JUNIOR
3093	t	70775958115	ADRIANE.OARAUJO	ADRIANE DE OLIVEIRA ARAUJO
3094	t	70033619140	JOAO.ARAUJO	JOAO MARCOS RODRIGUES DE ARAUJO
3095	t	00888883129	FABIO.PAULO	FABIO SILVA PAULO
3096	t	69605726149	GILVANIO.BRESSAN	GILVANIO BRESSAN
3097	t	94689059268	LUANA.MRIBEIRO	LUANA MACHADO RIBEIRO
3098	t	82963835191	ana.gpinheiro	ANA CLAUDIA GUIMARAES PINHEIRO
3099	t	02053695293	AUGUSTO.PSILVA	AUGUSTO PEREIRA DA SILVA
3100	t	80234623187	edilene.msousa	EDILENE DE MOURA SOUSA
3101	t	92056393115	luziano.asilva	LUZIANO ARRUDA DA SILVA
3102	t	04263193113	tallys.cfigueiredo	TALLYS CAITANO OPENKOSKI FIGUEIREDO
3103	t	85834041191	TSUNEO.MFILHO	TSUNEO MURATA FILHO
3104	t	05598518473	joao.ocavalcanti	JOAO PAULO OLIVEIRA CAVALCANTI
3105	t	01353167151	ANTONIO.BANDEIRA	ANTONIO BANDEIRA LIMA NETO
3106	t	70030264146	ANA.NMONTEIRO	ANA CAROLINA NUNES MONTEIRO
3107	t	50995812187	luciano.csena	LUCIANO CRISPIM DE SENA
3108	t	01920270183	suelen.poliveira	SUELEN PRISCILA ARIAS DE OLIVEIRA
3109	t	05379275141	gabriel.fnascimento	GABRIEL ALVES FONSECA NASCIMENTO
3110	t	84305711249	IONES.LSILVA	IONES LIMA DA SILVA
3111	t	04368481127	mario.mneves	MARIO MARCIO SILVA DAS NEVES
3112	t	03961357145	THIAGO.SFERREIRA	THIAGO SANTOS FERREIRA
3113	t	03191327142	HELLEN.CSANTOS	HELLEN CRYSTHYNY DOS SANTOS
3114	t	92072755115	rogerio.ravanelli	rogerio.ravanelli
3115	t	03995357130	luis.moliveira	LUIS HENRIQUE MARQUES OLIVEIRA
3116	t	46764216134	walter.pereira	WALTER MOTA PEREIRA
3117	t	92406351149	DAYSE.GSILVA	DAYSE GOMES E SILVA
3118	t	04982814180	ana.gsantos	ANA GABRIELA LOPES DOS SANTOS
3119	t	71749691191	noemi.slima	NOEMI ESCORCIO DE SOUSA LIMA
3120	t	02864125196	FRANCISCO.SLUCAS	FRANCISCO DE ASSIS DA SILVA LUCAS
3121	t	60205822134	michel.asilva	MICHEL ANTONIO DA SILVA
3122	t	05875230185	larissa.pferreira	LARISSA PERES FERREIRA
3123	t	04758155135	LEONARDO.PAZEVEDO	LEONARDO THIAGO PEREIRA AZEVEDO
3124	t	70163206180	gabriel.rlima	GABRIEL RODRIGUES DE LIMA
3125	t	05826707135	lucas.dpereira	LUCAS DANIEL GONCALVES PEREIRA
3126	t	02695291108	THASSIA.RSANTOS	THASSIA RHANAYRA SILVA DOS SANTOS
3127	t	00664876170	MOISES.CORTES	MOISES ARAUJO CORTES
3128	t	83599894191	IRAN.JSILVA	IRAN JOSE DA SILVA
3129	t	86775278187	KARINA.FURTADO	KARINA GUIMARAES FURTADO
3130	t	69333734104	RICARDO.ACRISTALDO	RICARDO ADRIANO CRISTALDO
3131	t	82221596153	RENATO.RPEIXOTO	RENATO RODRIGUES PEIXOTO
3132	t	91801753687	ALEXANDER.MAIA	ALEXANDER AUGUSTUS MAIA DE VASCONCELOS
3133	t	02781582182	GLEIBSON.SBORGES	GLEIBSON WEMERSON SILVA BORGES
3134	t	01810441102	REYLLA.GSILVA	REYLLA GOMES SILVA
3135	t	72393629149	JORGE.AREIS	JORGE LUIZ DE ARAUJO REIS
3136	t	05679535130	samuel.sabreu	SAMUEL DA SILVA ABREU
3137	t	00916016145	liana.fsantana	LAINA DE FREITAS SANTANA
3138	t	02943519308	francisco.nluz	FRANCISCO MARIANO NASCIMENTO DA LUZ JUNIOR
3139	t	05042278150	WANDERSON.JESUS	WANDERSON DE JESUS SANTANA
3140	t	05752267145	micaele.ssoares	MICAELE DA SILVA SOARES
3141	t	04502718114	paulo.hcarvalho	PAULO HENRIQUE FERREIRA CARVALHO
3142	t	10169167607	werica.ssouza	WERICA SOARES DE SOUZA
3143	t	02210170117	THAYNARA.MCAMILO	THAYNARA MARIA CAMILO
3144	t	70090434188	laryssa.atomaz	laryssa.atomaz
3145	t	01016092130	RICARDO.CAMELO	RICARDO CAMELO DA SILVA
3146	t	02890040127	JULYENE.DCAMARGO	JULYENE DIAS CAMARGO
3147	t	99958732149	rosineide.ppires	ROSINEIDE SOARES PIRES
3148	t	02583742337	felipe.ssilva	FELIPE DOS SANTOS SILVA
3149	t	04497289354	islany.aoliveira	ISLANY ALBUQUERQUE OLIVEIRA
3150	t	52384853368	raimundo.asousa	RAIMUNDO ALMEIDA DE SOUSA
3151	t	05203611300	ERIKA.RMUNIZ	ERIKA WANESSA RODRIGUES MUNIZ
3152	t	03742499114	LEANDRO.PSOUZA	LEANDRO PAIXAO DE SOUZA
3153	t	03489005384	ellaine.cmartins	ELLAINE CARVALHO MARTINS
3154	t	84421380210	matusael.lsouza	MATUSAEL LINS DE SOUZA
3155	t	92939678120	erica.bsouza	erica.bsouza
3156	t	11095501623	ROSALIA.DFONSECA	ROSALIA DORIZETE GOMES DA FONSECA
3157	t	02269173120	alex.soliveira	ALEX SANDRO GOMES DE OLIVEIRA
3158	t	91361672315	jurandir.junior	JURANDIR RODRIGUES DA SILVA JUNIOR
3159	t	05627952350	jainara.msilva	JAINARA MAYRA SILVA PENHA
3160	t	03503411330	marcelle.asilva	MARCELLE DANTAS ALVES DA SILVA
3161	t	70175987173	thays.ssouza	THAYS DOS SANTOS SOUZA
3162	t	04605073116	VIRGINIA.SPINTO	VIRGINIA SILVEIRA PINTO
3163	t	71664815104	karolina.santos	karolina.santos
3164	t	22445536120	ALBERTINO.COELHO	ALBERTINO COELHO DOS SANTOS
3165	t	00518132110	JULIO.CGRATAO	JULIO CESAR DE OLIVEIRA GRATAO
3166	t	01524172146	FREDERICO.XMARTINS	FREDERICO XAVIER MARTINS
3167	t	86855786120	RONALD.VPIRES	RONALD PATRICK VASCONCELOS PIRES
3168	t	29135700890	CARLOS.EVICENTINI	CARLOS EDUARDO VICENTINI VAZ
3169	t	01429619147	MARIA.HSOUSA	MARIA HELAYNE SOUSA NUNES
3170	t	02693529107	VINICIUS.COSTA	VINICIUS SILVA COSTA
3171	t	00256417270	NAIARA.AOLIVEIRA	NAIARA ALEIXO DE OLIVEIRA
3172	t	07080061598	ALEXANDRE.SBRITO	ALEXANDRE DOS SANTOS DE ARAUJO BRITO
3173	t	05556887390	isabelle.aviana	ISABELLE ARAUJO VIANA
3174	t	02504275161	ALEXANDRE.APASSOS	ALEXANDRE PASSOS LELIS
3175	t	03489216164	luana.tchaves	LUANA DE BRITO TEIXEIRA CHAVES
3176	t	75579553153	PABLO.MANDRADE	PABLO MATOS ANDRADE
3177	t	94338043172	DIEGO.LMELLO	DIEGO LEONARDO AMESTOY MELLO
3178	t	99171465120	warley.sfelix	WARLEY DA SILVA FELIX
3179	t	88956539120	ALEX.RSOARES	ALEX RODRIGO SOARES
3180	t	03543956108	wellen.ssilva	WELLEN GEREMIAS SOUZA DA SILVA
3181	t	73025364149	CAROLINE.FREZENDE	CAROLINE MARIA FELIX REZENDE
3182	t	09791907625	KEVIN.KVIEIRA	KEVIN KARLOS MERCENDES VIEIRA
3183	t	05414641161	LETICIA.PCOTTA	LETICIA PEREIRA DOS SANTOS COTTA
3184	t	70587876115	fabiane.massini	FABIANE DE MAGELA MASSINI
3185	t	70342389106	thiago.rgouveia	THIAGO RIBEIRO GOUVEIA
3186	t	04364135151	luiz.arruda	LUIS FELLIPE ARRUDA DA MATA
3187	t	03449748122	augusto.ribeiro	AUGUSTO CESAR RIBEIRO
3188	t	31918343896	thalita.yumi	thalita.yumi
3189	t	01578886651	rodrigo.lteodoro	RODRIGO LOURENCO TEODORO
3190	t	02577839383	taina.clucena	TAINA CARVALHO LUCENA
3191	t	40813215234	shelly.osouza	SHELLY DE OLIVEIRA SOUZA
3192	t	00821260383	alfredo.ncoimbra	ALFREDO NOVAIS COIMBRA
3193	t	04791314301	marjorie.nsilva	MARJORIE RAYANNE NUNES SILVA
3194	t	02230979345	damaris.tvieira	DAMARIS TAINA MARTINS DE AMORIM VIEIRA
3195	t	21476810168	jovelina.fpedrosa	JOVELINA FERREIRA PEDROSA
3196	t	05576880439	RHODRYGO.CFONSECA	RHODRYGO DE CARVALHO FONSECA
3197	t	29137845349	leila.mlima	LEILA MARIA DOS SANTOS LIMA
3198	t	00007394217	ADALBERTO.PMONTEIRO	ADALBERTO PEREIRA MONTEIRO
3199	t	03954360101	VINICIUS.CANDRADE	VINICIUS CORREIA DE ANDRADE
3200	t	01568347669	TIAGO.KOCHMANN	TIAGO BARBOSA KOCHMANN
3201	t	89837134100	LEIDILENE.VVICENTIN	LEIDILENE VICENTIN
3202	t	05066789111	marcia.pereira	MARCIA PEREIRA SERGIO
3203	t	60240270100	ALEXANDRO.CCARDOSO	ALEXANDRO DA COSTA CARDOSO
3204	t	00117060232	VITOR.HONORIO	VITOR MATEUS GREGORIO HONORIO
3205	t	70083102183	carolina.bbarbosa	CAROLINA BUENO DE LIMA BARBOSA
3206	t	04969497133	beatriz.mnetto	BEATRIZ MAINO HERNANDES NETTO
3207	t	71128980134	THIAGO.SBARRADAS	THIAGO.SBARRADAS (PJ) 
3208	t	02567494189	GLEYCY.CSOUSA	GLEYCY KELLY CUNHA E SOUSA
3209	t	57803625649	HUEDER.MARQUETE	HUEDER MARQUETE
3210	t	71108505104	RHANGEL.CTODA	RHANGEL CARDOSO TODA
3211	t	60299479153	EMERSON.LCOSTA	EMERSON LUIZ DE OLIVEIRA COSTA
3212	t	00878711147	sidney.aoliveira	SIDNEY ALVES OLIVEIRA
3213	t	03421126194	edgar.mlima	edgar.mlima
3214	t	03748345127	THIAGO.SABRAAO	THIAGO DOS SANTOS ABRAO
3215	t	02305238118	giulian.csilva	GIULIAN LENO CUNHA E SILVA
3216	t	00346867207	russiara.vcarlos	RUSSIARA VIRGULINO CARLOS
3217	t	03601655160	jessica.ovasconcelo	JESSICA ORBEN VASCONCELOS
3218	t	02220973107	priscila.nfreitas	PRISCILA LANE NUNES DE FREITAS
3219	t	01730111106	ely.aborges	ELY ALVES BORGES
3220	t	05469072100	nathan.gdias	NATHAN GABRIEL DOS SANTOS DIAS
3221	t	24554988830	carlos.megydio	CARLOS ALBERTO MOURA EGYDIO
3222	t	06324075141	danielly.srocha	DANIELLY DE SOUSA ROCHA
3223	t	26404524287	UACIMA.PSILVA	UACIMA PICANCO DA SILVA
3224	t	04641845174	ELIEZER.JOLIVEIRA	ELIEZER DE JESUS OLIVEIRA
3225	t	70174415168	fernando.lsouza	FERNANDO LUIZ DE SOUZA
3226	t	03543691139	MARICLEIDE.DSANTOS	MARICLEIDE DOS SANTOS
3227	t	00734778694	HAELITON.DELGADO	HAELITON MENDES DELGADO
3228	t	01018369104	maria.cfabiano	MARIA ALICE CAMARGO FABIANO
3229	t	00146023188	tiago.sporfirio	TIAGO SOARES PORFIRIO
3230	t	75630770144	sarah.mfreitas	SARAH MONIQUE SILVA FREITAS
3231	t	01098522141	lara.clima	LARA CARVALHO DE LIMA
3232	t	70301498156	ana.galmeida	ANA PATRICIA GODOI DE ALMEIDA
3233	t	70122906179	EDUARDO.PVIEIRA	EDUARDO PEIXOTO VIEIRA
3234	t	01056199148	GESIEL.SILVA	GESIEL COTRIN DA SILVA
3235	t	03155990147	HELTON.PORTELA	HELTON RODRIGUES PORTELA
3236	t	01104597250	CLAUMIR.JUNIOR	CLAUMIR RIBEIRO MACHADO JUNIOR
3237	t	73529630144	lais.bsorte	LAIS NUNES BOA SORTE
3238	t	04824029384	janio.gsousa	JANIO GONCALVES DE SOUSA
3239	t	70210320648	ellen.bmartins	ellen.bmartins
3240	t	89664914134	WERIKA.GSOUSA	WERIKA GOMES SOUSA
3241	t	70565089102	alynne.vsantos	ALYNNE VIEIRA SANTOS
3242	t	02468736125	luana.rperrud	LUANA RIBEIRO PERRUD
3243	t	01463377193	roberta.rmoura	ROBERTA RODRIGUES DE MOURA
3244	t	01838519190	hugo.sleite	HUGO ALBERTO DE SOUSA LEITE
3245	t	03682238174	lucas.cordeiro	lucas.cordeiro
3246	t	02463702117	samira.lsantos	SAMIRA LOPES DOS SANTOS
3247	t	49643576191	clayton.dsantos	CLAYTON DIAS DOS SANTOS
3248	t	70217793134	ROSANGELA.SOLIVEIRA	ROSANGELA DE SOUZA OLIVEIRA
3249	t	02552897150	JESSYKA.VCARDOSO	JESSYKA VIEIRA CARDOSO
3250	t	02117047173	andressa.rmelo	ANDRESSA RODRIGUES DE MELO
3251	t	70897450159	antonio.nmeireles	ANTONIO THEODORO NEIVA MEIRELES
3252	t	01867402394	luciano.cneto	luciano.cneto
3253	t	04253210104	felipe.fleite	PEDRO FELIPE FERREIRA LEITE
3254	t	93967233120	ADEMIR.RJUNIOR	ADEMIR RODRIGUES JUNIOR
3255	t	01103232118	CARLOS.BOLIVEIRA	CARLOS BARRETO DE OLIVEIRA
3256	t	65874676368	VIVIANNE.SMESQUITA	VIVIANNE DOS SANTOS MESQUITA
3257	t	03284610162	vitor.mviana	VITOR MAIA VIANNA
3258	t	06504565562	lenin.jlima	LENIN JUNIOR MUROS DE LIMA
3259	t	04430165181	johnathan.rsantos	JOHNATHAN RIVIS DOS SANTOS
3260	t	03385417210	dandara.asilva	DANDARA STEFANY ARAUJO DA SILVA
3261	t	03356761110	SIDNEY.SSOUSA	SIDNEY SILVA SOUSA
3262	t	29001102115	edilson.aduarte	EDILSON ALVES DUARTE
3263	t	00858871106	LEIF.MMORAES	LEIF LEDSON MOURA DE MORAES
3264	t	00750681179	thaynne.dsilveira	THAYNNE DANTAS DA SILVEIRA
3265	t	01605563170	ediberto.gsjunior	EDIBERTO GOMES DA SILVA JUNIOR
3266	t	95950630297	FAGNER.SSOUZA	FAGNER SILVA CLEMENTE DE SOUZA
3267	t	06528142122	carina.ssantos	CARINA DA SILVA SANTOS
3268	t	05805933128	deividi.lsantiago	deividi.lsantiago
3269	t	05075235395	maria.bmendes	MARIA JOSE BASTOS MENDES
3270	t	03338909144	jorge.mlima	JORGE MOREIRA LIMA
3271	t	75119307191	kaio.dteixeira	KAIO DIAS TEIXEIRA
3272	t	05614649107	suzana.ssousa	SUZANA CRISTINA SANTANA DE SOUSA
3273	t	00591972263	andrei.flima	ANDREI FERREIRA DE LIMA
3274	t	49895818300	CLESIO.AMACHADO	CLESIO ALVES MACHADO
3275	t	60202302369	yawana.msantos	YAWANA ANITA MOURAO DOS SANTOS
3276	t	01944128140	marlon.msoares	MARLON MENDES SOARES
3277	t	06572416100	amanda.ofernandes	AMANDA DE OLIVEIRA FERNANDES
3278	t	06249057137	marcos.caraujo	marcos.caraujo
3279	t	27679649871	SERGIO.ROLIVEIRA	SERGIO RICARDO DE OLIVEIRA
3280	t	12662830646	bruna.lsousa	BRUNA LEANDRO DE SOUSA
3281	t	70025068113	paulo.hsato	PAULO HENRIQUE YOZO SATO
3282	t	02845987129	ANDRE.NTRAJANO	ANDRE NEPOMUCENO TRAJANO
3283	t	70238046176	tania.bsilva	TANIA BATISTA SILVA
3284	t	01573430145	felipe.gcastro	FELIPE GUILHERME SANTANA E CASTRO
3285	t	00847656314	rafaele.apinto	rafaele.apinto
3286	t	05057975188	leticia.rguimaraes	LETICIA RODRIGUES GUIMARAES
3287	t	57990867249	ozimar.fsilva	OZIMAR FRANCA DA SILVA
3288	t	69776652115	LAURA.VTRINDADE	LAURA GRAZIELLE VITOR TRINDADE
3289	t	02091773140	ALAN.FILHO	ALAN SILVA HONORATO FILHO
3290	t	22070478149	CASTRO.ATEIXEIRA	CASTRO ALVES TEIXEIRA
3291	t	02700470184	RENATA.CRUVINEL	RENATA PEREIRA CRUVINEL
3292	t	65836928215	DOGIVANIA.FSILVA	DOGIVANIA FARIAS SILVA
3293	t	77881192372	idenilson.ssilva	IDENILSON SANTOS SILVA
3294	t	01663564337	FILIPE.ASOUSA	FILIPE AUGUSTO DOS SANTOS SOUSA
3295	t	04041552192	bruno.roliveira	BRUNO RODRIGUES OLIVEIRA
3296	t	84430036187	MARIA.DALENCAR	MARIA ISABEL VALCANAIA DE ALENCAR
3297	t	09888370723	JOYCE.LLOPES	JOYCE.LLOPES
3298	t	99682079187	ALCIMARA.ALMEIDA	ALCIMARA RODRIGUES ALMEIDA
3299	t	04376473154	miria.dsouza	MIRIA DEICYNAN ESTEFANOSKI SOUZA
3300	t	94059446149	MARIANA.VBARBOSA	MARIANA DE REZENDE VILA VERDE BARBOSA
3301	t	03726422170	FERNANDA.DCOELHO	FERNANDA LAYS DUTRA COELHO
3302	t	05888725129	JOICE.SOLIVEIRA	JOICE SILVA OLIVEIRA
3303	t	03229230183	JUYLLA.DSOUZA	JUYLLA DE SOUZA LIMA
3304	t	01892393360	marina.apinheiro	MARINA ADRIANA SANTOS PINHEIRO
3305	t	70026925184	rayane.csousa	RAYANE FERNANDES DA COSTA SOUSA
3306	t	93360746104	IRACEMA.VSANTOS	IRACEMA VAZ DOS SANTOS
3307	t	01592975674	jonathas.mcunha	JONATHAS HUMBERTO MATEUS DA CUNHA
3308	t	88739627268	ANDRE.LMORAIS	ANDRE LUIS SANTOS MORAIS
3309	t	05687642340	isaac.msousa	ISAAC MARTINS SILVEIRA SOUSA
3310	t	00391214292	emileide.cmacena	EMILEIDE GOMIS DA COSTA MACENA
3311	t	01242934170	ODILSON.MOLIVEIRA	ODILSON MIGUEL DE OLIVEIRA FILHO
3312	t	02803044129	eliene.nsantos	ELIENE NASCIMENTO DOS SANTOS
3313	t	04118636301	nathalia.slira	NATHALIA VIRGINIA DA SILVA LIRA
3314	t	02520012145	elias.ssilva	ELIAS DE SOUSA SILVA
3315	t	04328550179	NATALIA.ACASTRO	NATALIA AUXILIADORA LIMA DE CASTRO
3316	t	71283498120	janaina.cmonte	JANAINA COSTA MONTE
3317	t	04650731151	raul.varaujo	raul.varaujo
3318	t	03564155384	jacqueline.pfrazao	JACQUELINE PEREIRA FRAZÃO
3319	t	02091490199	EVERTON.CSANTOS	EVERTON CORREIA DOS SANTOS
3320	t	04763631101	camila.aquadros	CAMILA ARAUJO DE QUADROS
3321	t	44609140349	ALESSANDRO.MELO	ALESSANDRO DE ANDRADE MELO
3322	t	00946624100	felipe.psalgado	FELIPE PENA PINHEIRO SALGADO
3323	t	06136439174	evellin.brios	evellin.brios
3324	t	03321083175	pollyana.xavier	POLLYANA RODRIGUES XAVIER BARBOSA
3325	t	03359217101	dieyle.dsantos	DIEYLE DIAS DOS SANTOS
3326	t	04118395193	wanessa.lsilva	WANESSA LORENA DA SILVA
3327	t	70093048157	bruno.aborges	BRUNO DE ALBUQUERQUE BORGES
3328	t	98368664187	hugo.acoutinho	HUGO ANDRADE COUTINHO
3329	t	04450319133	melquezedeque.coelho	MELQUEZEDEQUE COELHO MARCELINO
3330	t	02591496161	beatriz.aponte	BEATRIZ AZEVEDO PEREIRA DA PONTE
3331	t	05777911170	daniela.csales	DANIELA CORDEIRO SALES
3332	t	06155886113	daniela.tsouza	DANIELA SILVA TEIXEIRA SOUZA
3333	t	48854620378	ana.vsampaio	ANA VERA SAMPAIO
3334	t	88836886353	RONALD.LCOIMBRA	RONALD LIMA COIMBRA
3335	t	06039882450	ALEX.MSILVA	ALEX MANOEL DA SILVA
3336	t	32952045860	paulo.dmunhos	PAULO DIONIZIO MUNHOS ROSSI
3337	t	22114449890	eduardo.cortez	eduardo.cortez
3338	t	45198225304	MARCELO.MAZEVEDO	MARCELO MENDES AZEVEDO
3339	t	00320867170	kenya.snunes	KENYA DOS SANTOS NUNES
3340	t	02504992319	luis.nascimento	LUIS TIAGO OLIVEIRA DO NASCIMENTO
3341	t	03981165128	LUIZ.FPEREIRA	LUIZ FELIPE PEREIRA DA SILVA PATURY
3342	t	05928101198	thalyanna.vieira	THALYANNA SHELYNNE CARNEIRO VIEIRA
3343	t	05138192159	emilly.acampos	emilly.acampos
3344	t	04880177180	JACINEIA.PAIVA	JACINEIA DE PAIVA MOISES
3345	t	85108049153	gleibe.ssilva	GLEIBE DE SOUZA E SILVA
3346	t	92672566100	juliana.gcoutinho	JULIANA GOMES DA COSTA COUTINHO
3347	t	00011845120	daniela.alchaar	DANIELA ALCHAAR SODRE
3348	t	04258605158	vitoria.jcaded	vitoria.jcaded
3349	t	73817570104	marcelo.dias	MARCELO DIAS
3350	t	03624695126	GUILHERME.SGONCALVES	GUILHERME DOS SANTOS GONCALVES
3351	t	03453330129	jaqueline.rsouza	JAQUELINE ROSA DE SOUZA
3352	t	00714977144	romulo.mcaixeta	ROMULO MARTINS CAIXETA
3353	t	53366212187	denise.cmoraes	DENISE CAETANO DE MORAES
3354	t	99280361104	renata.afigueredo	RENATA ALVES FIGUEREDO
3355	t	62627821504	luciano.csilva	LUCIANO CARNEIRO DA SILVA
3356	t	05952708161	rafaella.gmonteiro	RAFAELLA GOMES MONTEIRO
3357	t	70922633100	ISADORA.CNUNES	ISADORA CAVALCANTE NUNES
3358	t	03777542180	thaline.srodrigues	THALINE THAYNA SILVA RODRIGUES
3359	t	70010403124	RICARDO.GDIAS	RICARDO GABRIEL DIAS
3360	t	01303246201	rafael.doliveira	RAFAEL DERIC PAULA DO NASCIMENTO OLIVEIRA
3361	t	92425216120	fabricia.psantos	fabricia.psantos
3362	t	09358347619	MICHELLE.PSILVA	MICHELLE PEREIRA SILVA
3363	t	00273601210	jonathan.psousa	JONATHAN PALMER DE SOUSA
3364	t	04775318144	BRUNA.PEREIRA	BRUNA GOMES PEREIRA
3365	t	70381691144	ANDRESSA.MRIBEIRO	ANDRESSA MAMEDE RIBEIRO
3366	t	01954240341	FRANCISCO.JALCANTARA	FRANCISCO JAIRON DE OLIVEIRA ALCANTARA
3367	t	94789126234	NAIRA.ACOELHO	NAIRA CHELLI ALVES COELHO
3368	t	03961244103	FRANKLIN.FMACHADO	FRANKLIN FIRMIANO MACHADO
3369	t	75239345104	samuel.esilva	samuel.esilva
3370	t	85855880125	elizangela.fcarneiro	ELIZANGELA FREITAS CARNEIRO
3371	t	01810802229	loyane.scunha	LOYANE SANCHES CUNHA
3372	t	96372206234	erique.lsilva	ERIQUE LUCENA SILVA
3373	t	03910063128	gleice.moliveira	GLEICE QUELI MARTINS DE OLIVEIRA
3374	t	04752257165	allyson.dpaz	ALLYSON DOUGLAS ANDRADE PAZ
3375	t	02516264151	allax.hbarbosa	ALLAX HANDELL DUTRA DE CASTRO BARBOSA
3376	t	03549582161	NATALIA.VCARDOSO	NATALIA VASCONCELOS CARDOSO
3377	t	04879143170	PAULO.EOLIVEIRA	PAULO EVANDRO RODRIGUES DE OLIVEIRA
3378	t	04874593186	thaine.scarvalho	THAINE SANTOS EVELIM DE CARVALHO
3379	t	01198051116	divaldo.fantonio	DIVALDO FERREIRA ANTONIO
3380	t	31071485806	ana.pfigueiredo	ANA PAULA ALVES DE FIGUEIREDO
3381	t	41346521115	MARCO.PCOSTA	MARCO POLO RODRIGUES DA COSTA
3382	t	05134709130	carolini.mamorim	CAROLINI DE MELO AMORIM
3383	t	03317295120	bruno.hsilva	BRUNO HENRIQUE MOREIRA DA SILVA
3384	t	26565315191	fatima.arodrigues	FÁTIMA APARECIDA RODRIGUES MICHALSKY
3385	t	05588682157	larissa.vprofeta	LARISSA VIEIRA PROFETA
3386	t	01535627654	joao.gfaria	JOAO VICTOR GUIMARAES FARIA
3387	t	08696926234	ADELSON.SBRAGA	ADELSON SILVA BRAGA
3388	t	70361611161	TAIS.OCALDAS	TAIS OLIVEIRA CALDAS
3389	t	03363797141	kattryel.salcantara	KATTRYEL DE SOUZA ALCANTARA
3390	t	83243380120	girlene.pluz	GIRLENE PINHEIRO DA LUZ
3391	t	60636740390	rebeca.fjesus	REBECA FREITAS DE JESUS
3392	t	60156419327	fernanda.sdiniz	FERNANDA COSTA DA SILVA DINIZ
3393	t	03019400104	bryanne.ccardoso	BRYANNE ANGELICA COSTA CARDOSO
3394	t	01699964173	MURILLO.MSOUSA	MURILLO MACHADO DE SOUSA
3395	t	02531319360	ROMARIO.LSANTOS	ROMARIO LOPES DOS SANTOS
3396	t	01141866188	thayla.mcarvalho	THAYLA MARTINS DE CARVALHO
3397	t	05219138103	evellin.passis	EVELLIN PAULA SOUZA DE ASSIS
3398	t	00893690171	RAFAEL.FSOARES	RAFAEL FERREIRA SOARES
3399	t	71003525172	maria.hsilveira	MARIA HELENA DA SILVEIRA
3400	t	01964589355	ROMULO.PSANTOS	ROMULO DENILSON PEREIRA DOS SANTOS
3401	t	03419899165	angelica.lsantos	ANGELINA LORRANY LOURENCO SANTOS
3402	t	70533680174	brenda.psantana	brenda.psantana
3403	t	04037465230	fernanda.psousa	FERNANDA PEREIRA DE SOUSA
3404	t	01158946163	ELISIO.PNETO	ELISIO PATRICIO DA SILVA NETO
3405	t	06089317136	leonardo.araujo	LEONARDO DE ARAUJO BARBOSA
3406	t	04149302316	HAYANAYARA.LOPES	HAYANAYARA LIMA LOPES
3407	t	04471787179	thays.smiranda	THAYS SOUZA MIRANDA
3408	t	89781104104	renata.roliveira	RENATA GONCALVES ROCHA OLIVEIRA
3409	t	09059196619	raquel.sbatista	RAQUEL SILVA BATISTA
3410	t	02541439385	mariula.arodrigues	MARIULA ABREU RODRIGUES
3411	t	05936297365	adriana.fcastro	ADRIANA FERNANDA DOS SANTOS CASTRO
3412	t	06946463188	rayla.kercia	RAYLA KERCIA DA SILVA LIMA
3413	t	03137377030	ana.pferreira	ANA PAULA BETICO FERREIRA
3414	t	04440127100	josimar.agomes	JOSIMAR ALBERTO SILVA GOMES
3415	t	02073443109	VALERIA.AMARAL	VALERIA ALVES DE AMARAL
3416	t	70055811132	itallo.dsilva	ITALLO DIAS SILVA
3417	t	03731360306	breno.almeida	BRENO ALMEIDA SA
3418	t	02707384100	AMANDA.AWARLEZ	AMANDA ADRIELA WARLEZ VASCONCELOS
3419	t	00913424226	anderson.sdiniz	ANDERSON SANTIAGO DINIZ
3420	t	13843655839	ALESSANDRO.SOLDI	ALESSANDRO SOLDI
3421	t	19371126590	teste.ti	teste.ti
3422	t	17118775819	teste	teste
3423	t	00922543143	FELIPPE.WIST	FELIPPE CINTRA WIST
3424	t	44967730310	CLEDINEIRE.ASOUSA	CLEDINEIRE ALVES DE SOUSA
3425	t	01854665324	wilmara.rpassinho	WILMARA DA ROCHA PASSINHO
3426	t	35220318810	adalberto.dsantos	adalberto.dsantos
3427	t	04345021111	julio.ccosta	JULIO CESAR DA CONCEICAO COSTA
3428	t	02161998161	LUAN.SCARACANHA	LUAN CARLOS SANTANA BACO CARACANHA
3429	t	03212693576	PEDRO.ROLIVEIRA	PEDRO RONAN GOMES DE OLIVEIRA
3430	t	02707067121	thulio.sribeiro	THULIO HELTER SOARES RIBEIRO
3431	t	99746620100	ELISANGELA.ALMEIDA	ELISANGELA FERREIRA DE ALMEIDA
3432	t	76815056287	jonathan.fsouza	JONATHAN FONTINELE DE SOUZA
3433	t	00031621252	JESSICA.SCHAVEZ	JESSICA SUELI CHAVEZ
3434	t	10084841648	LEONARDO.RIBEIRO	LEONARDO RIBEIRO
3435	t	00236147145	FABIO.SNASCIMENTO	FABIO DA SILVA NASCIMENTO
3436	t	04821344378	GIULLIANE.MSILVA	GIULLIANE LENNON DE MORAES SILVA
3437	t	26451280178	joao.rrezende	JOAO ROBERTO NAVES REZENDE
3438	t	04670039121	neiriele.valvarenga	NEIRELE VIEIRA DE ALVARENGA
3439	t	00515910147	WELBER.SMARTINS	WELBER SANTIAGO CANDIDO MARTINS
3440	t	03759056180	sibelhi.msilva	SIBELHY MARTINS DA SILVA
3441	t	98158058272	jessica.omartins	JESSICA DE OLIVEIRA MARTINS
3442	t	84740213672	ALAN.JSILVA	ALAN JOSE DA SILVA
3443	t	02509131192	BRUNA.RROCHA	BRUNA RODRIGUES DA ROCHA
3444	t	02026166145	elizabeth.soliveira	ELIZABETH SAMARA BEZERRA DE OLIVEIRA
3445	t	02726127312	AMANDA.DAMASCENO	AMANDA DAMASCENO DE OLIVEIRA
3446	t	02345179139	DEBORA.SMESSIAS	DEBORA SOUZA DE CAMARGO STIEVANO MESSIAS
3447	t	75073765168	walyson.brosa	WALYSON BRITO ROSA
3448	t	98203630200	arthur.dsilva	ARTHUR DOMINGOS FERREIRA DA SILVA
3449	t	95258817687	fabiano.falves	fabiano.falves
3450	t	05503180380	kairon.ksantos	KAIRON KENER FERREIRA DOS SANTOS
3451	t	86988808191	CARLA.MEIRELES	CARLA MEIRELES
3452	t	03542094340	jorge.spires	jorge.spires
3453	t	05370203300	anderson.calmeida	anderson.calmeida
3454	t	71162712368	vitoria.ssousa	vitoria.ssousa
3455	t	05049844380	raimundo.rjunior	raimundo.rjunior
3456	t	73602825353	francinira.snunes	francinira.snunes
3457	t	75014041353	katiane.vguedes	KATIANE VITORIA LOBAO GUEDES
3458	t	87502860304	antonio.csilva	antonio.csilva
3459	t	06052849118	amanda.arodrigues	amanda.arodrigues
3460	t	04021652175	LUCAS.RODRIGUES	LUCAS RODRIGUES DE MENDONCA
3461	t	01369402260	caique.tpereira	CAIQUE LEONARDO TEOTONIO PEREIRA
3462	t	01847494170	TANIA.VIEIRA	TANIA PIRES VIEIRA
3463	t	02444519159	FILIPE.CLOBO	FILIPE CUSTODIO LOBO
3464	t	70552731102	nathallia.gsantos	nathallia.gsantos
3465	t	06726083165	thaila.mbraga	THAILA MARTINS BRAGA
3466	t	01476561176	neuza.ppinange	NEUZA PABLINY ARAUJO PINANGE
3467	t	01676979190	ANDREIA.CSANTOS	ANDREIA CARDOSO DOS SANTOS
3468	t	01472793137	simone.cnunes	SIMONE DA COSTA NUNES
3469	t	02927050180	RODRIGO.SMENDES	RODRIGO SILVA MENDES
3470	t	83731849372	mauro.rhaydar	MAURO HENRIQUE REGO HAYDAR
3471	t	02737780136	ana.ecandido	ANA ELISA DE CASTRO CANDIDO
3472	t	04305490161	lukas.ggontijo	LUKAS GONCALVES GONTIJO
3473	t	20728530368	MOISES.SNETO	MOISES AMORIM DE SOUZA NETO
3474	t	04871961125	ANDERSON.MENDONCA	ANDERSON RODRIGUES DE MENDONCA
3475	t	02394522160	DAYANE.PFERREIRA	DAYANE PIRES FERREIRA
3476	t	03849526151	daniela.smiranda	DANIELA DE SOUSA MIRANDA
3477	t	56609671249	CARLA.AABREU	CARLA ADRIANA LEMOS DE ABREU
3478	t	02490027107	thalita.sbruno	THALITA ANTONIA SIBIONI BRUNO
3479	t	89437829500	EDILAINE.CASTRO	EDILAINE CERQUEIRA DE CASTRO
3480	t	70289998158	ryllary.jgodinho	RYLLARY DE JESUS RODRIGUES GODINHO
3481	t	05314101106	victor.hbrandao	VICTOR HUGO VIEIRA BRANDAO
3482	t	17330095826	CHRISTIAN.LALMEIDA	CHRISTIAN LUIZ JARDIM DE ALMEIDA
3483	t	17722870163	eliezer.csilva	ELIEZER CECILIO DA SILVA
3484	t	01825718156	caroline.cvilela	CAROLINE CORREA VILELA
3485	t	02293708101	whagynton.rcastro	WHAGYNTON RODRIGUES DE CASTRO
3486	t	01403416176	karina.ssantos	KARINA SILVA DOS SANTOS
3487	t	32166615805	glauber.sportela	GLAUBER SILVA PORTELA
3488	t	04915702103	lucas.hyago	LUCAS HYAGO DIAS MONTEIRO
3489	t	38706111890	manuella.jcosta	MANUELLA JOYCE COSTA DE SOUSA
3490	t	05085180160	kethlen.brito	KETHLEN DAYANE DE BRITO CORDEIRO ALMADA
3491	t	75531631391	christian.cmarques	CHRISTIAN COSTA MARQUES
3492	t	57079188172	ADRIANA.ABRAGA	ADRIANA AQUINO BRAGA
3493	t	01352323176	juely.wjunior	JUELY WELLINGTON MOREIRA JUNIOR
3494	t	02743804122	gabriel.nsilva	GABRIEL NESTALI DA SILVA
3495	t	11462358608	marcus.rsouza	MARCUS VINICIUS RODRIGUES DE SOUZA
3496	t	02105505112	josemar.drodrigues	JOSEMAR DIAS RODRIGUES
3497	t	05544393443	natalia.cgodoy	NATALIA COSTA BRANCO DE GODOY
3498	t	81576358100	ROGERIO.BARBOSA	ROGERIO SANTOS BARBOSA
3499	t	75428334134	FERNANDO.MFERNANDES	FERNANDO AUGUSTO MENDONCA FERNANDES
3500	t	70330668137	jefferson.msouza	JEFFERSON MACIEL DE SOUZA
3501	t	55333168172	ELIZEU.FROTA	ELIZEU MAGALHAES FROTA
3502	t	05875296100	LUCAS.WONSOSCKY	LUCAS LEMOS WONSOSCKY
3503	t	04232293620	GERALDO.RJUNIOR	GERALDO RIBEIRO DE CASTRO JUNIOR
3504	t	01118029119	MARCUS.ASOARES	MARCUS VINICIO DE ALMEIDA SOARES
3505	t	81785585134	alex.sborges	ALEX SANDRO BORGES
3506	t	03884383124	daniela.aoliveira	DANIELA ALVES DE OLIVEIRA
3507	t	99474239134	CALOSMAN.MLUCENA	CALOSMAN MOURA DE LUCENA
3508	t	05124713127	katyellen.lmoura	KATYELLEN LORRAINE NUNES MOURA
3509	t	70927412110	maria.goliveira	maria.goliveira
3510	t	04624822196	katia.rgaleno	KATIA ROSANE DA SILVA GALENO
3511	t	03719322114	jefferson.rsilva	JEFFERSON RODRIGUES DA SILVA SANTOS
3512	t	06236619182	beatriz.msilva	BEATRIZ MELO MAXIMO DA SILVA
3513	t	01019637188	GESIVAM.LSILVA	GESIVAM LOPES DA SILVA
3514	t	06443574109	aliny.olorigiola	ALINY OLIVEIRA LORIGIOLA
3515	t	98963767191	marci.afreitas	MARCI ALISSON CARNEIRO FREITAS
3516	t	05486206100	AMANDA.ACALVES	AMANDA APARECIDA CORDEIRO ALVES
3517	t	04645622102	JENIFFER.CVELOSO	JENIFFER CRISTTIELLE VELOSO BUENO
3518	t	01755023154	alaercio.smatos	ALAERCIO SILVA DE MATOS
3519	t	01462901190	nillor.dfreitas	NILLOR TAYLOR DURAES E FREITAS
3520	t	05332491123	greice.fsilva	GLEICE CAMPOS FERREIRA DA SILVA
3521	t	15363139200	ERLEY.SSANTOS	ERLEY DA SILVA DOS SANTOS
3522	t	11091039640	dacyanni.borba	DACYANNI SILVA BORBA
3523	t	00537337113	talita.mcarvalho	TALITA MENDES CARVALHO
3524	t	04842011319	gracyelle.mlemos	GRACYELLE MACEDO LEMOS
3525	t	02355360162	graziella.laires	GRAZIELLA LEITE AIRES
3526	t	02699782165	elias.srodrigues	ELIAS DA SILVA RODRIGUES
3527	t	73718971100	dayana.vlima	DAYANA VIEIRA SILVA DE LIMA
3528	t	00161549160	rodrigo.soliveira	RODRIGO SABINO DE OLIVEIRA
3529	t	92147097187	marcos.cleite	MARCOS DE CAMPOS LEITE
3530	t	02633014194	gabriela.camorim	GABRIELA COSTA DE AMORIM
3531	t	04334783147	matheus.bcalil	MATHEUS CARNEIRO BORGES CALIL
3532	t	03413382144	daiane.nbarra	daiane.nbarra
3533	t	02110145161	gabriela.aprado	gabriela.aprado
3534	t	01496262158	ana.tartuce	ana.tartuce
3535	t	04251327144	silfarney.fsilva	SILFARNEY FRANCYS DE OLIVEIRA SILVA
3536	t	07200087190	mayke.tsilva	mayke.tsilva
3537	t	06365261195	geimison.asilva	GEIMISON GIZELIO ALVES E SILVA
3538	t	70589914480	francisco.cjunior	francisco.cjunior
3539	t	02945261103	jilvan.fcarvalho	JILVAN FORTES DE CARVALHO
3540	t	04363489189	tanagra.ssantos	TANAGRA ROAN DE SOUZA SANTOS
3541	t	69750122372	lucinete.rcunha	lucinete.rcunha
3542	t	00313375380	herika.sdomingues	herika.sdomingues
3543	t	73251224115	pedro.costa	PEDRO HENRIQUE RIBEIRO COSTA
3544	t	72226625100	daniel.pamaral	DANIEL HENRIQUE PERES DO AMARAL
3545	t	02973639212	erica.msilva	ERICA MENEZES DA SILVA
3546	t	97209228187	ROGERIO.SSILVA	ROGERIO SANTIAGO DA SILVA
3547	t	07731514196	viviane.lsilva	VIVIANE LINO DA SILVA
3548	t	06252552160	natalia.coliveira	NATALIA CORTEZ DE OLIVEIRA
3549	t	02904503145	lazara.osantos	LAZARA DAISY DE OLIVEIRA SANTOS
3550	t	26820258895	RODRIGO.BSANTOS	RODRIGO BORGES SANTOS
3551	t	05443773119	gezica.amoreno	GEZICA ARGEMON MORENO
3552	t	41633914879	mirlane.rfarinha	MIRLANE RAMOS FARINHA
3553	t	01927755107	ingrid.lsantos	INGRID AMANDA LIMA DOS SANTOS
3554	t	03603382161	CAROLINA.PSANTOS	CAROLINA PEREIRA DOS SANTOS
3555	t	06160625160	tifanny.dduarte	tifanny.dduarte
3556	t	39723860104	rivaldo.rlima	RIVALDO RODRIGUES DE LIMA
3557	t	01245551108	edinaldo.psiqueira	EDINALDO PEREIRA SIQUEIRA
3558	t	03715099208	diego.msousa	DIEGO MARCIO PRIVADO SOUSA
3559	t	61357804172	gabriela.dierings	GABRIELA DIERINGS CORTES
3560	t	32011412838	FERNANDO.CIUFFI	FERNANDO CIUFFI
3561	t	86692275687	SILVIO.GFILHO	SILVIO GLADSTONE DE AFONSECA FILHO
3562	t	01791458190	JAILSON.FPEREIRA	JAILSON FAUSTINO PEREIRA
3563	t	89933737104	THIAGO.MDANTAS	THIAGO MARTINS DANTAS DE SOUZA LINS
3564	t	12876792699	camilla.smendes	CAMILLA CRISTINA SOARES MENDES
3565	t	70075792133	andressa.csilva	ANDRESSA CRISTYNA CANDIDO SILVA
3566	t	02211658270	erica.smorais	ERICA SALES DE MORAIS
3567	t	70011789182	carolinne.bcarrijo	CAROLINNE BORGES FIDELIS CARRIJO
3568	t	04425679105	greiciane.guia	GREICIANE OLIVEIRA GUIA
3569	t	70258945168	RENATO.CAMELO	RENATO PEREIRA CAMELO
3570	t	01903607205	KARINY.OLIVEIRA	KARINY FARIAS OLIVEIRA
3571	t	03912056170	isabella.raraujo	ISABELLA RIBEIRO DE ARAUJO
3572	t	95586431149	lindimarcia.vsilva	LINDIMARCIA VIEIRA DA SILVA
3573	t	82666008304	maria.cnascimento	MARIA DA SOLEDADE COSTA SILVA NASCIMENTO
3574	t	43910750249	marinalva.jrabelo	MARINALVA DE JESUS RABELO
3575	t	02766854150	daniela.gfleuri	daniela.gfleuri
3576	t	04118621118	jeiel.psouza	jeiel.psouza
3577	t	03513434154	natalia.figueiredo	natalia.figueiredo
3578	t	02747701131	suzana.flacerda	SUZANA MAMEDE FERREIRA LACERDA
3579	t	00833391160	bruno.ccosta	BRUNO URZEDA CARVALHO COSTA
3580	t	07123229142	jhonatan.jsantos	JHONATAN JOSE LIRA DOS SANTOS
3581	t	03898905101	antonia.ssilva	ANTONIA SAMILE DE CASTRO SILVA
3582	t	70281559198	amanda.rsilva	AMANDA GABRIELA REIS SILVA
3583	t	04769742592	johny.manjos	JOHNY STANLEY MACHADO DOS ANJOS
3584	t	81300158115	ERICSON.SANTOS	ERICSON CICERO DOS SANTOS
3585	t	14328869752	valdir.fneto	VALDIR ANDRADE DA FONSECA NETO
3586	t	03717383183	cristhiane.tcarvalho	CRISTHIANE TAMILLY DE MENEZES CARVALHO
3587	t	12322957607	nara.bcirilo	NARA KAROLINA BARBOSA CIRILO
3588	t	01328058123	rafael.mmoraes	RAFAEL MENDES MORAES
3589	t	96079010100	tatiana.psilva	TATIANA MICHELLI PEREIRA DA SILVA
3590	t	03235035192	klewerson.abezerra	KLEWERSON ALVES BEZERRA
3591	t	03417946190	roney.lmatos	RONEY ROBSON LIRA DE MATOS
3592	t	01423672143	ludmilla.mdaniel	LUDMILLA MEDEIROS DANIEL
3593	t	05695402166	natalia.psantos	NATALIA PEREIRA DOS SANTOS
3594	t	07111644522	iasnaia.calmeida	IASNAIA DA COSTA DE ALMEIDA
3595	t	70637388100	vitoria.emoreira	VITORIA EDUARDA BOTELHO MOREIRA
3596	t	04090247160	francie.bsouza	FRANCIE BATISTA DE SOUZA
3597	t	72377305172	luiz.adriano	LUIZ ADRIANO LEAL DOS SANTOS
3598	t	73651842134	alexandre.syanagui	ALEXANDRE SERRA YANAGUI
3599	t	44016855802	CAIO.SMAIA	CAIO DI CARLO DE SOUZA MAIA
3600	t	01916334377	girlande.souza	GIRLANDE SOUZA
3601	t	26328984120	fernando.reis	FERNANDO ANTONIO DOS REIS
3602	t	70304524107	luana.lcarvalho	LUANA LOPES CARVALHO
3603	t	06255166139	ludmyla.csilva	ludmyla.csilva
3604	t	00196523133	THIAGO.CARNEIRO	THIAGO RODRIGUES CARNEIRO
3605	t	70270754156	LETICIA.RSILVA	LETICIA RODRIGUES DA SILVA
3606	t	04311575106	JESSICA.MARTINS	JESSICA MARTINS SILVA
3607	t	02156604207	erik.lsilveira	ERIK DE LIMA SILVEIRA
3608	t	91584043253	fabio.massuncao	FABIO MONTEIRO SOUZA CORREIA DE ASSUNCAO
3609	t	05386392964	SILVANA.DGARCIA	SILVANA DOBROVOLSKI GARCIA
3610	t	12108086412	marcela.gvieira	MARCELA GREICY DE JESUS VIEIRA
3611	t	70011900121	mayara.smacedo	MAYARA GOMES SAMRA MACEDO
3612	t	00331563100	rudmila.cdias	RUDMILLA CONCEICAO LOPES DIAS
3613	t	70132834103	luiz.tsilva	LUIZ OCTAVIO TOLEDO SILVA
3614	t	99737140168	IZABELA.SLISBOA	IZABELA.SLISBOA
3615	t	02240896256	matheus.bcampos	MATHEUS BRAGA CAMPOS
3616	t	73662720272	valeria.mdourado	VALERIA MARQUES DOURADO LIMA
3617	t	00296764108	mary.hpimenta	MARY HELLEN PIMENTA SPIGOLONI
3618	t	97478121349	CAROLINE.MDIAS	CAROLINE CASTELO DE MESQUITA DIAS
3619	t	99207001187	paula.lrabelo	ANA PAULA LOPES RABELO
3620	t	00612322203	alex.srocha	ALEX SOUSA ROCHA
3621	t	72847379134	johnathan.moraes	JOHNATHAN GOMES DE MORAES
3622	t	98027808120	rosilei.gcidram	ROSILEI GREGORIO CIDRAM
3623	t	70111969190	WILYN.JMESQUITA	WILYN JOPLIN RODRIGUES MESQUITA
3624	t	02857460147	jessica.fcosta	JESSICA FERREIRA DA COSTA
3625	t	95024670191	santiago.mnovo	SANTIAGO MOREIRA DIAS NOVO
3626	t	05429063177	beatriz.bsilva	BEATRIZ BATISTA DA SILVA
3627	t	00251121127	meury.dsilva	MEURY DAIANNY RODRIGUES DA SILVA
3628	t	04315816140	LIVIA.DLACERDA	LIVIA DAMAS DIAS LACERDA
3629	t	05914475171	leticia.lsilva	LETICIA LORRANY GONCALVES SILVA
3630	t	70286072165	laura.ccosta	LAURA CRISTINA DIAS COSTA
3631	t	00365932140	bruno.lbarreto	BRUNO LUCENA DE BARRETO
3632	t	60416763308	jullyana.spinto	JULLYANA PATRICIA SOARES PINTO
3633	t	98947230120	FLAVIO.SPAULO	FLAVIO DE SOUSA PAULO
3634	t	83120718149	ALEXANDRE.EVERS	ALEXANDRE DE SOUSA EVERS
3635	t	83289704149	nilsa.aoliveira	NILSA APARECIDA DE OLIVEIRA
3636	t	00288286154	elber.salves	elber.salves
3637	t	98664190100	andre.aferreira	ANDRE DEUSDET ALVES FERREIRA
3638	t	00789132176	viviane.ssousa	VIVIANE SILVA DE SOUSA
3639	t	70080730116	debora.acarvalho	DEBORA ALVES DIAS BATISTA DE CARVALHO
3640	t	04865929100	adriana.acarvalho	ADRIANA DE ALMEIDA CARVALHO
3641	t	70533447119	lucas.sgoncalves	LUCAS PEREIRA SILVA GONCALVES
3642	t	70232205183	kleber.fluiz	KLEBER MATEUS FERREIRA LUIZ
3643	t	01141645254	crislane.mgaldino	CRISLANE DE MELO GALDINO
3644	t	05238788185	ramon.delgado	RAMON DELGADO
3645	t	01065307209	michele.smatos	MICHELE SOUZA DE MATOS
3646	t	89091574291	leidiane.cbarreto	LEIDIANE CARDOSO CAVALCANTE BARRETO
3647	t	08662736890	marcos.lpenteado	MARCOS ANTONIO LYRA PENTEADO
3648	t	02091988260	priscila.sfreitas	PRISCILA SALES ROLIM DE FREITAS
3649	t	03273718200	rodiney.jmarinho	RODINEY JOHNATHAN COSTA MARINHO
3650	t	02442042258	camila.tmatos	CAMILA TEIXEIRA DE MORAIS
3651	t	00847019276	douglas.snascimento	DOUGLAS SOUZA DO NASCIMENTO
3652	t	01169361269	fabiane.npeixoto	FABIANE NERYS PEIXOTO
3653	t	69893438187	CLEBER.BCOSTA	CLEBER BRANDAO DA COSTA
3654	t	00283434171	julyanne.pmartins	JULYANNE PATRICIA MARTINS
3655	t	07575667903	estevao.soliveira	ESTEVAO DOS SANTOS DE OLIVEIRA
3656	t	05519903174	luca.sfantino	LUCA SAMUEL SANTOS FANTINO
3657	t	00888142110	leticia.afreire	LETICIA ALVES MORAES FREIRE
3658	t	87174006353	karla.rcampos	KARLA REGIA CUNHA MONTEIRO CAMPOS
3659	t	71260870120	cleunice.jsilva	CLEUNICE JUSTINO DA SILVA
3660	t	09882873669	JORDAN.ASOUSA	JORDAN ALVES DE SOUSA
3661	t	05212923107	glaucia.bbandeira	GLAUCIA BATISTA BANDEIRA
3662	t	70048933163	FRANCIELLE.OLIVEIRA	FRANCIELLE OLIVEIRA
3663	t	04291586108	igor.aoliveira	IGOR HENRIQUE ALVES OLIVEIRA
3664	t	01561172154	bruna.bpereira	BRUNA BERNADES PEREIRA
3665	t	04481948132	lucas.malves	LUCAS MAIA ALVES
3666	t	94160481191	FABIO.JSILVA	FABIO JUNIO ALVES DA SILVA
3667	t	70236097172	ALESSANDRA.PBATISTA	ALESSANDRA PRESTES BATISTA
3668	t	73064742191	djanaina.bsampaio	DJANAINA BORGES SAMPAIO
3669	t	02527398170	gesielle.pcarmo	GESIELLE PAIVA RIBEIRO CARMO
3670	t	81678037249	HERIALDO.NETO	HERIALDO BATISTA PAIVA NETO
3671	t	02482352142	josy.apaula	JOSY AZEVEDO DE PAULA
3672	t	04445572101	JULIO.KVERISSIMO	JULIO KALITON VERISSIMO
3673	t	66545110187	carlos.vlima	CARLOS VIEIRA LIMA
3674	t	00581652908	ronaldo.gfaria	RONALDO GOMES BARROSO DE FARIA
3675	t	25994115134	carlos.abelle	CARLOS ANTONIO BELLE
3676	t	69833486568	ADILSON.ASANTOS	ADILSON ARAUJO DOS SANTOS
3677	t	00826836119	ALINE.GOMES	ALINE DE OLIVEIRA GOMES
3678	t	83544160110	GILVAN.LGOMES	GILVAN DE LIRA GOMES
3679	t	03888909163	bruna.psilva	BRUNA PIMENTEL DA SILVA
3680	t	70122097122	mabia.vbarbosa	MABIA VAZ BARBOSA
3681	t	02225201161	ellen.pnascimento	ELLEN ISSIS PEREIRA DO NASCIMENTO
3682	t	02658456964	RAFAEL.AMAZETTO	RAFAEL PEDRO ARAUJO MAZETTO
3683	t	70356561119	alexandre.ctavares	ALEXANDRE DO CARMO TAVARES
3684	t	47384379687	IEDO.PMELO	IEDO PEIXOTO DE MELO
3685	t	71591958253	WALTER.SSANTOS	WALTER DA SILVA SANTOS
3686	t	04557538142	marciana.scosta	MARCIANA DE SOUZA COSTA
3687	t	73638390187	jhennifer.falves	JHENNIFFER FAGUNDES ALVES
3688	t	04140329343	carliene.mneves	CARLIENE MARTINS NEVES
3689	t	01442915170	daiany.lima	DAIANY LIMA BONFIM
3690	t	04969342601	graziele.jnoronha	GRAZIELE JESUS DE NORONHA
3691	t	05142434307	marcelo.rmota	MARCELO BENE REIS MOTA
3692	t	73643122187	ediene.bnardes	EDIENE BATISTA NARDES
3693	t	75721600187	giovanna.sproto	GIOVANNA SIMOES PROTO
3694	t	57737134115	altair.souza	ALTAIR JOSE TEIXEIRA DE SOUZA
3695	t	05757499117	bruna.lsilva	BRUNA LARISSA DA SILVA
3696	t	06376252184	daniel.ljesus	DANIEL LUCAS DE JESUS PEREIRA
3697	t	95726306104	PAULO.WGOMES	PAULO FABIANO WOLKER GOMES
3698	t	70600645169	ISABELLA.CLAET	ISABELLA CRISTINA LAET SEGANTINE E SILVA
3699	t	71270710125	jordana.csilva	JORDANA DO CARMO LISITA SILVA
3700	t	05937592140	matheus.lxavier	MATHEUS LUCAS XAVIER
3701	t	11274028620	danielle.fneves	DANIELLE FERNANDES NEVES
3702	t	71861882149	GUILHERME.SSOUSA	GUILHERME DOS SANTOS SOUSA
3703	t	03203547171	DANILO.AVALE	DANILO ALVES DO VALE
3704	t	61214639291	VALDIRENE.LOPES	VALDIRENE LOPES
3705	t	00192294105	thiago.joliveira	THIAGO JADER MENDONCA DE OLIVEIRA
3706	t	97038148168	henrique.hjesus	HENRIQUE HERMINIO DE JESUS
3707	t	04555188152	leonardo.calazenco	LEONARDO CALAZENCO DA SILVA
3708	t	04428329156	wenderson.rsilva	WENDERSON JORALINO RIBEIRO DA SILVA
3709	t	91085446620	mirian.mribeiro	MIRIAN MERCIA DE OLIVEIRA RIBEIRO
3710	t	12305121601	carolaine.speixoto	CAROLAINE DA SILVA PEIXOTO
3711	t	08683665607	otavio.asilva	OTAVIO ALBINO REZENDE DA SILVA
3712	t	93452764168	patricia.atoledo	PATRICIA ARAUJO DIAS TOLEDO
3713	t	81054459215	ediana.salmeida	EDIANA SILVA ALMEIDA
3714	t	05800370532	FERNANDA.SMOREIRA	FERNANDA DE SOUZA SALES MOREIRA
3715	t	04461608140	claudio.wnogueira	CLAUDIO WENDER TAVARES NOGUEIRA
3716	t	01564564274	jailson.dsilva	JAILSON DAMASCENO DA SILVA
3717	t	02629169162	RARUME.AFIGUEIREDO	RARUME AIALA FIGUEIREDO PINTO
3718	t	05051175171	eduarda.cjesus	EDUARDA NOANE CARDOSO DE JESUS
3719	t	59474106134	bruno.tlima	BRUNO TIMOTEO DE LIMA
3720	t	06469762140	amanda.aferreira	AMANDA DE ABREU FERREIRA
3721	t	04586464151	ANGELA.ARODRIGUES	ANGELA ALVES RODRIGUES
3722	t	83255460263	rosangela.cgomes	ROSANGELA PRADO DA COSTA GOMES
3723	t	02286402299	ranielly.tcutrim	RANIELLY TEIXEIRA CUTRIM
3724	t	03458111190	monyk.falves	MONYK DE FARIA ALVES
3725	t	03511454190	ITAMARA.SBEZERRA	ITAMARA DOS SANTOS BEZERRA
3726	t	81478801204	francisco.ssouza	FRANCISCO SARAIVA DE SOUZA
3727	t	05448835198	LETICIA.ESAMPAIO	LETICIA EMANOELI VARELA SAMPAIO
3728	t	01894526171	DENISE.POLIVEIRA	DENISE MAGALHAES PIMENTEL DE OLIVEIRA
3729	t	32356969896	JOAO.DBARROS	JOAO DYEGO DIAS BARROS
3730	t	77535030149	JEOVANE.PFERNANDES	JEOVANE PEREIRA FERNANDES
3731	t	06529949119	anna.moliveira	anna.moliveira
3732	t	03189992185	rafael.rrmelo	RAFAEL ROSA DE MELO
3733	t	83509836200	daniel.svargas	DANIEL SEVERO VARGAS
3734	t	04318295192	daniel.sfeliciano	DANIEL STIVAL FELICIANO
3735	t	95215131600	altamiro.ojunior	ALTAMIRO DIVINO DE OLIVEIRA JUNIOR
3736	t	01646558162	patricia.grodrigues	PATRICIA GONCALVES SILVA RODRIGUES
3737	t	00346334136	margarete.lscampos	MARGARETE LOURENCO DA SILVA CAMPOS
3738	t	02368751165	jocasta.malves	JOCASTA MARQUES ALVES
3739	t	00448193159	DENIVALDO.ALVES	DENIVALDO SILVA ALVES
3740	t	06973599486	JARDSON.ASOUZA	JARDSON ALVES DE SOUZA
3741	t	32563460816	Dario.sneto	DARIO DOS SANTOS PEREIRA NETO
3742	t	95506209215	jeffiter.njunior	JEFFITER NATIVIDADE FRASAO JUNIOR
3743	t	00856739197	WIRES.SOUSA	WIRES PEREIRA DE SOUSA
3744	t	90437799115	SILVANIA.ASANTOS	SILVANIA LECINDA DE ALVARENGA SANTOS
3745	t	70552095109	erika.ccosta	ERIKA GONCALVES CHAGAS COSTA
3746	t	71068821191	erveton.bferreira	ERVETON BARBOSA FERREIRA
3747	t	03915426130	kamila.csoares	KAMILLA CHRISTYNE ALCIDES SOARES
3748	t	70152960120	marcia.ebarreto	MARCIA EMANUELLY DIAS BARRETO
3749	t	40873331320	SUMARIA.CGASPAR	SUMARIA CRISTINA ALVES GASPAR
3750	t	01334957100	silvani.poliveira	SILVANI PONTES OLIVEIRA
3751	t	73624497120	weuler.alima	WEULER ANTONIO DE LIMA
3752	t	32814399187	otair.rsilva	OTAIR RODRIGUES DA SILVA
3753	t	30750660104	LEZIEL.GALMEIDA	LEZIEL GONCALVES DE ALMEIDA
3754	t	88337600144	marcio.tfelix	MARCIO TOGISAKI  FELIX
3755	t	04521969143	fernanda.gnunes	FERNANDA GESSICA COSTA NUNES
3756	t	53069943187	MARCELO.LSILVA	MARCELO LEONARDO SILVA
3757	t	02298187142	leiridiane.bsoares	LEIRIDIANE BARROS SOARES
3758	t	70739926110	nicole.rjesus	nicole.rjesus
3759	t	70713696150	bruna.aoliveira	bruna.aoliveira
3760	t	06405974162	polyane.sfarias	POLYANE SOUZA FARIAS
3761	t	70728551144	ellen.psoares	ELLEN DE PAULA SOARES
3762	t	80210791187	adalberto.rfilho	ADALBERTO RIBEIRO FILHO
3763	t	83201971120	simoney.rpereira	SIMONEY RODRIGO DE MORAIS PEREIRA
3764	t	03623936138	gabrielle.mfurtado	GABRIELLE DE MELO FURTADO
3765	t	05433561321	gilvan.rsilva	GILVAN RAMOS SILVA
3766	t	03986408312	rafael.fsilva	RAFAEL SALES FERREIRA SILVA
3767	t	05668958116	beatriz.csantos	BEATRIZ CAETANO SANTOS
3768	t	04322119344	ANALYNE.MSAMPAIO	ANALYNE MARCELINO SAMPAIO
3769	t	69469792149	geraldo.junior	GERALDO RODRIGUES JUNIOR
3770	t	91049512120	nalva.moliveira	nalva.moliveira
3771	t	66783178153	fabio.clopes	FABIO CARDOSO LOPES
3772	t	71651349134	ARITALLA.JPARREIRA	ARITALLA DE JESUS PARREIRA
3773	t	04619791105	WILLIAN.SRESENDE	WILLIAN CHIRAIVA DE SOUZA RESENDE
3774	t	70210768193	adriano.vfilho	ADRIANO VIEIRA ROCHA FILHO
3775	t	80487718100	luciana.rsilva	LUCIANA RIOS DE ARAUJO SILVA
3776	t	75324652172	natalia.tborges	NATALIA TALITA BORGES
3777	t	00587711159	evandro.gassis	EVANDRO GOMES CAVALCANTE DE ASSIS
3778	t	00012213101	hugo.aalmeida	HUGO ALVES DE ALMEIDA
3779	t	01670418197	ALEX.ANDRADE	ALEX MOURA ANDRADE
3780	t	73667633149	lucas.asousa	LUCAS ANTONIO MARTINS DE SOUSA
3781	t	08589365425	lucas.soliveira	lucas.soliveira
3782	t	70040657159	IGOR.MDOMINGUES	IGOR MARCOS DE AZEVEDO DOMINGUES
3783	t	96137509168	DENYS.TFERREIRA	DENYS TACITO DE ANDRADE FERREIRA
3784	t	01514595176	danielle.pmelo	DANIELLE PEREIRA DE MELO
3785	t	91793025134	thais.ggodoy	THAIS GONCALVES DE GODOY
3786	t	71478574291	cristiane.alima	CRISTIANE DE MARIA ALVES DE LIMA
3787	t	75447886104	raphael.razevedo	RAPHAEL RODRIGUES DE AZEVEDO
3788	t	70005854121	natalia.trodrigues	NATALIA TAVARES RODRIGUES
3789	t	85143030153	VITOR.LJUNIOR	VITOR LOURENCO JUNIOR
3790	t	04204593160	amanda.onunes	AMANDA DE OLIVEIRA NUNES
3791	t	03322575179	daniela.roliveira	DANIELA ROCHA DE OLIVEIRA
3792	t	04439043160	LANNA.CBORGES	LANNA CARLA SOUSA BORGES
3793	t	05482150143	gustavo.fbarros	GUSTAVO FARIA BARROS
3794	t	41323327134	maria.naraujo	MARIA APARECIDA NERIS ARAUJO
3795	t	00978548167	kelly.gferreira	KELLY GOMES FERREIRA
3796	t	03964958182	juliana.lmoreira	JULIANA LARA MIRANDA MOREIRA
3797	t	51512475149	joao.lmelo	JOAO LUIZ BATISTA DE MELO
3798	t	93676883187	luciana.osousa	LUCIANA OLIVEIRA DE SOUSA
3799	t	89677650106	GEIDSON.JESUS	GEIDSON OLIVEIRA DE JESUS
3800	t	05047623629	ISRAEL.AFURTADO	ISRAEL AUGUSTO FURTADO
3801	t	07021979126	ana.fduarte	ANA FLAVIA DE SOUZA DUARTE
3802	t	69457972104	JULIO.CCOELHO	JULIO CESAR COELHO DO NASCIMENTO
3803	t	05120655190	ARIANE.ASILVA	ARIANNE ALESK RODRIGUES DA SILVA
3804	t	01804003123	DANIEL.BFARIA	DANIEL FRANCO BORGES FARIA
3805	t	02061886507	victor.mcosta	VICTOR MATHEUS COSTA
3806	t	73115479115	daniele.bsantos	DANIELE BENEDITA DOS SANTOS
3807	t	37681222391	evandro.obarbosa	evandro.obarbosa
3808	t	68486154391	carlos.rjunior	carlos.rjunior
3809	t	00586461167	diego.namaral	DIEGO NUNES DO AMARAL
3810	t	04934469150	fernando.cmenezes	FERNANDO HENRIQUE COSTA MENEZES
3811	t	01541938364	francisco.bjunior	FRANCISCO GUIMARAES BEZERRA JUNIOR
3812	t	06256874188	isabella.coliveira	ISABELLA COELHO DE OLIVEIRA
3813	t	34498451104	LEONIDAS.SFILHO	LEONIDAS JOSE DOS SANTOS FILHO
3814	t	00998551198	jonathan.opontes	JONATHAN DE OLIVEIRA PONTES
3815	t	06570627317	enir.jrodrigues	ENIR ANTONIA DE JESUS RODRIGUES
3816	t	73454044200	fernanda.salmeida	FERNANDA SILVA DE ALMEIDA
3817	t	00917967178	kleanne.tcoelho	KLEANNE TELES COELHO
3818	t	03517119105	CARLOS.HSILVA	CARLOS HENRIQUE DE AZEVEDO SILVA
3819	t	70009975160	VICTORIA.FFRANCO	VICTORIA FERESIN FRANCO
3820	t	53837460134	edson.smiranda	EDSON SILVA DE MIRANDA
3821	t	70077967100	gislene.ksousa	GISLENE KOCH DE SOUSA
3822	t	75516993253	felipe.saraiva	FELIPE SANTANA SARAIVA
3823	t	53818792100	gerson.oliveira	GERSON CAMPOS DE OLIVEIRA
3824	t	70086174614	jeferson.vfranco	JEFERSON LUIZ VIEIRA FRANCO
3825	t	01506248160	cassia.mpaula	CASSIA MIGUEL DE PAULA
3826	t	05216549129	isabela.pcassol	ISABELA PEREIRA ALVES CASSOL
3827	t	70450426122	JHENNY.SANTANA	JHENNY.SANTANA
3828	t	06140654963	aline.lazzeri	ALINE LAZZERI
3829	t	62002406120	sergio.adias	SERGIO APARECIDO GOMES DIAS
3830	t	07043174198	tatiane.feitosa	TATIANE FEITOSA VIEIRA
3831	t	01573084212	rodrigo.scandido	RODRIGO SANTOS SOARES CANDIDO
3832	t	92570151149	HUGO.LEONARDO	HUGO LEONARDO VIEIRA DE MEDEIROS
3833	t	03895214183	ronnivaldo.soliveira	RONNIVALDO DA SILVA OLIVEIRA
3834	t	69909318172	erika.ssalim	ERIKA DOS SANTOS SALIM
3835	t	99004232249	gleina.noliveira	GLEINA NARRAYANA SOUZA DE OLIVEIRA
3836	t	03828287123	jessica.sluz	JESSICA DA SILVA LUZ
3837	t	01513957139	alini.gtavares	ALINI GOMES TAVARES
3838	t	25597094300	aldenia.vpires	aldenia.vpires
3839	t	70396434134	boberio.djunior	ROBERIO DIAS DE SOUSA JUNIOR
3840	t	01270269160	roberto.palencar	ROBERTO PEREIRA DE ALENCAR
3841	t	04997154113	kleleober.sbarbosa	KLELEOBER ALEXANDRE DE SOUZA BARBOSA
3842	t	02518381104	shadia.ssalim	shadia.ssalim
3843	t	05739700175	REINALDO.OSOUSA	REINALDO OLIVEIRA SOUSA
3844	t	81553196104	jefferson.bsantos	JEFFERSON BERNARDES DOS SANTOS
3845	t	03377246100	maria.jsousa	MARIA DE JESUS VIANA DE SOUSA
3846	t	64303802204	LEIA.PRESTES	LEIA GARCIA PRESTES
3847	t	71347879668	RENATO.CAMPOS	RENATO NOGUEIRA CAMPOS
3848	t	01425310141	LUIZ.FERNANDO	LUIZ FERNANDO DE SOUZA
3849	t	02819256155	INDIANARA.APEREIRA	INDIANARA ADRIANE PEREIRA
3850	t	02786582105	deborah.pferreira	DEBORAH LIVIA PINTO FERREIRA
3851	t	00095233164	ALLYSON.FMARIANO	ALLYSON FIGUEIREDO MARIANO
3852	t	05991513155	YGOR.RSILVA	YGOR RODRIGUES DA SILVA
3853	t	01852499141	PAULO.PFILHO	PAULO CESAR PARRINI FILHO
3854	t	46981675153	monica.vteixeira	MONICA BEATRIZ VALADAO TEIXEIRA
3855	t	05952152112	jhonatan.fcardoso	JHONATAN FRANKLIN FARIA CARDOSO
3856	t	01863405160	MAURICIO.SALMEIDA	MAURICIO SOUTO DE ALMEIDA
3857	t	72329696353	ana.msilva	ANA FLAVIA MORAES SILVA
3858	t	05133903324	sanndy.blima	SANNDY ARAUJO BRANDAO LIMA
3859	t	02563779103	rayssa.gonzaga	RAYSSA EDUARDO GONZAGA
3860	t	75101467120	michelly.nsilva	MICHELLY NATHANNY LEITE SILVA
3861	t	61689610204	phatricia.vdalmeida	PHATRICIA WIVIANE DALMEIDA
3862	t	01600756107	suzana.msilva	SUZANA MARQUES LEANDRO SILVA
3863	t	05713482626	LUIZ.MCARVALHO	LUIZ MARCIO ALVES CARVALHO
3864	t	03962142150	MARCELO.PJUNIOR	MARCELO OTONIEL PIMENTA JUNIOR
3865	t	03666444164	victor.bcosta	VICTOR DE BARROS DA COSTA
3866	t	04117442163	PATRICIA.AZOMIOTI	PATRICIA DE AQUINO ZOMIOTI
3867	t	01607169347	herika.mpereira	HERIKA CARNEIRO MUNIZ PEREIRA
3868	t	00452902100	SORAIA.CDORNELAS	SORAYA COELHO DORNELAS
3869	t	01252789181	cleiton.cruz	CLEILTON OLIVEIRA CRUZ
3870	t	75774437353	luiz.afilho	LUIZ ANTONIO GUIMARAES CUNHA FILHO
3871	t	09581849416	henrique.abezerra	HENRIQUE ARAUJO BEZERRA
3872	t	09605734966	BRUNO.APEREIRA	BRUNO ARTHUR RIBEIRO GOMES PEREIRA
3873	t	08939036638	GRACIELLY.FERREIRA	GRACIELLY ALVES FERREIRA
3874	t	70555141187	ARIL.JUNIOR	ARIL RIBEIRO JUNIOR
3875	t	65009479087	ORNEI.CIVO	ORNEI CARVALHO IVO
3876	t	00192885146	KATIA.RSILVA	KATIA REGINA DA SILVA LACERDA
3877	t	78107806387	maria.lfranca	MARIA REIJANE LOPES FRANCA
3878	t	02143981104	ISABELLA.CBITTAR	ISABELLA CRISTINE DE CAMARGO BITTAR
3879	t	03126616143	RENATA.ASILVA	RENATA GUIMARAES ALMEIDA DA SILVA
3880	t	00046234101	aline.pmoreira	aline.pmoreira
3881	t	38983265191	vinicius.bvasques	VINICIUS DE BARROS VASQUES
3882	t	04463006163	edimar.ojunior	EDIMAR OLIVEIRA ARAUJO JUNIOR
3883	t	09425506680	ADILEISSON.BFRANCA	ADILEISSON BERNARDINO FRANCA
3884	t	03840933102	MAYSA.CARVALHO	MAYSA DIAS DE CARVALHO
3885	t	00104556129	myrian.abastos	MYRIAN ANDRADE BASTOS
3886	t	70165817135	shirley.asilva	shirley.asilva
3887	t	83387722168	marcelo.scosta	MARCELO SAMPAIO COSTA
3888	t	00283066199	WESLEY.XPENSO	WESLEY XAVIER PENSO
3889	t	00955866103	cleber.jlima	CLEBER DE JESUS LIMA
3890	t	96030755234	fernanda.csantos	FERNANDA CAROLINA DOS SANTOS
3891	t	06890220188	italo.asousa	ITALO ANTUNES DE SOUSA
3892	t	06203089150	sabrina.amorais	SABRINA ARIANE DE MORAIS SILVA
3893	t	03565003146	phatrik.lsilva	PHATRIK LOURENCO DA SILVA
3894	t	69113157191	helisa.ocampos	HELISA EMANUELE DE OLIVEIRA CAMPOS
3895	t	04821745100	pedro.msilva	PEDRO PAULO MACHADO E SILVA
3896	t	01871044685	augusto.cpereira	AUGUSTO CARDOSO PEREIRA
3897	t	01993726080	VINICIUS.MPEREIRA	VINICIUS MARTINS PEREIRA
3898	t	00638903130	JULIANA.DSILVA	JULIANA DAVILA SILVA
3899	t	03868639110	maraisa.lbraga	MARAISA CRISTINA DE LUIZ BRAGA
3900	t	04647199177	geovana.fsilva	GEOVANA FERREIRA DA SILVA
3901	t	01802705163	ANA.PSOUSA	ANA PAULA DE SOUSA NUNES
3902	t	00075114186	sara.vgarcia	SARA VALERIA PEREIRA GARCIA
3903	t	06301516109	joelia.acosta	JOELIA ALCANTARA DA COSTA
3904	t	04479137190	christopher.aoliveir	CHRISTOPHER MARTINS ALVES DE OLIVEIRA
3905	t	00098420127	anilton.junior	ANILTON GERALDO MAGELA NASCIMENTO JUNIOR
3906	t	70402533135	isabella.oabrao	ISABELLA DE OLIVEIRA ABRAO
3907	t	03860762192	karla.lvieira	KARLA LARISSA PINHEIRO VIEIRA
3908	t	76151727304	SIMONE.NEVES	SIMONE CORREIA NEVES
3909	t	01241617155	ricardo.ssantos	RICARDO AUGUSTO DE SOUSA SANTOS
3910	t	06692132177	francielly.cpereira	FRANCIELLY DEL COLLI PEREIRA
3911	t	04294397179	dayane.clima	DAYANE CRISTINA LIMA DOS SANTOS
3912	t	03520651173	gustavo.fbrito	GUSTAVO FERREIRA DE SILVA BRITO
3913	t	04754436598	jefferson.smagalhaes	JEFFERSON SOUZA MAGALHAES
3914	t	91106419120	RICARDO.PFREITAS	RICARDO PRADO FREITAS
3915	t	04309169147	thomas.mcastro	THOMAS ROBSON MANGINI DE CASTRO
3916	t	03361642116	geane.sarvelos	GEANE SILVA ARVELOS
3917	t	02876622173	luzia.gbraga	LUZIA GABRIELA FLOR SANTOS BRAGA
3918	t	02731076151	rafael.nascente	rafael.nascente
3919	t	00949991988	paolo.chiarlone	paolo.chiarlone
3920	t	01922193160	luiz.ssilva	LUIZ FERNANDO DE SOUSA E SILVA
3921	t	72700130120	paulo.rfranca	PAULO RONAI PEREIRA FRANCA
3922	t	02521293180	adriana.fsilva	ADRIANA FRAZAO DA SILVA
3923	t	48751618320	weverson.moliveira	WEVERSON MACHADO DE OLIVEIRA
3924	t	01846947286	maciel.smoraes	MACIEL DE SOUZA MORAES
3925	t	01930950179	vinicius.fsouza	vinicius.fsouza
3926	t	03356041290	jaqueline.rlima	JAQUELINE REIS LIMA
3927	t	04509511205	ludimila.runeda	LUDIMILA MELISSA ROSA UNEDA
3928	t	04233347198	douglas.psena	DOUGLAS CARVALHO PATATAS SENA
3929	t	01834074312	ELEONOR.CBORGES	ELEONOR GERMANO CAMINHA BORGES
3930	t	01895189101	luciene.mnascimento	LUCIENE VIEIRA DE MOURA NASCIMENTO
3931	t	92023029104	camila.nvaz	CAMILA NASSER BAIA VAZ
3932	t	64475239320	INALDO.GROCHA	INALDO GOMES DA ROCHA JUNIOR
3933	t	75315521168	andreia.scarmo	ANDREIA SOUZA DUARTE DO CARMO
3934	t	61023330393	kassia.adias	KASSIA MAYARA ALVES DIAS
3935	t	06420403111	juliana.osilva	JULIANA DE OLIVEIRA E SILVA
3936	t	70218585152	bruna.ncarvalho	BRUNA NASCIMENTO CARVALHO
3937	t	02158203129	gean.gsilva	GEAN CARLO GUIMARAES SILVA
3938	t	70156712121	nathalia.icaetano	NATHALIA INACIO CAETANO
3939	t	44685190459	carlos.aaraujo	CARLOS ANTONIO DE ARAUJO SILVA
3940	t	03432086105	rafael.gsilva	RAFAEL GALVAO SILVA
3941	t	04294690127	emerson.ssouza	emerson.ssouza
3942	t	61703702115	edezio.alima	edezio.alima
3943	t	04603245190	LORRAYNE.PMELO	LORRAYNE PAIVA DE MELO
3944	t	04401286170	jamile.scruz	jamile.scruz
3945	t	75707659153	annanda.atavares	ANNANDA AUGUSTA TAVARES PRESTO DOS ANJOS
3946	t	04169977599	daiane.nferreira	DAIANE NASCIMENTO FERREIRA
3947	t	04352982164	rayanne.acaroba	RAYANNE ESTHER ANTUNES CAROBA
3948	t	99618818187	JANAINA.FERREIRA	JANAINA FERREIRA MARQUES
3949	t	97810487191	sara.fterra	SARA FERREIRA TERRA
3950	t	70199148104	laysa.scamelo	LAYSA DE SOUSA CAMELO
3951	t	00585898138	suzana.rrodrigues	SUZANA RIBEIRO RODRIGUES
3952	t	01699980101	marcos.vinicios	MARCOS VINICIOS RODRIGUES
3953	t	07066026330	davi.dviana	DAVI DELFINO VIANA
3954	t	04600104170	tais.ssantos	TAIS SOUZA OLIVEIRA SANTOS
3955	t	01677984210	Thays.SVieira	THAYS SILVA VIEIRA
3956	t	05331905170	carolina.hsilva	CAROLINA HORRARA SILVA
3957	t	70233685154	lidia.dsilva	LIDIA DE DEUS DA SILVA
3958	t	93578350168	marcia.coliveira	MARCIA CEZARIA DE OLIVEIRA
3959	t	70382822129	maria.vcarvalho	MARIA ERICA VASCONCELOS DE CARVALHO
3960	t	04370756138	mariana.rpires	MARIANA REIS PIRES
3961	t	03184999327	MAYSE.TALVES	MAYSE THIELLE ALVES REIS
3962	t	01430628162	ana.lsilva	ANA LUCIA LEMOS DA SILVA
3963	t	00801649196	marco.abrasil	MARCO AURELIO MOURA BRASIL
3964	t	75258951391	HEITOR.SNETO	HEITOR SAVIO DA NOBREGA NETO
3965	t	72482303168	jussara.vjunqueira	JUSSARA VALERIA MACEDO JUNQUEIRA
3966	t	02365566146	layane.gdias	LAYANE GOMES DIAS
3967	t	03463640198	neytha.soliveira	NEYTHA SILVA DE OLIVEIRA
3968	t	04924153150	hugo.fnoleto	HUGO LEONARDO FERNANDES NOLETO
3969	t	05310975101	VICTOR.HOLIVEIRA	VICTOR HUGO RODRIGUES OLIVEIRA
3970	t	33997624100	sergio.barroso	SERGIO RODRIGUES BARROSO
3971	t	04887459181	walisson.ocoutinho	WALISSON OLIVEIRA COUTINHO
3972	t	03395815145	joilson.rqueiroz	JOILSON RIBEIRO DE QUEIROZ
3973	t	01581169175	rafael.gfeitosa	RAFAEL GUIMARAES FEITOSA
3974	t	47795506291	rosiane.msilva	ROSIANE MARTINS DA SILVA
3975	t	02407107110	FABIO.BJUNIOR	FABIO BARROS DE ALMEIDA JUNIOR
3976	t	05408613160	LETICIA.MSILVA	LETICIA TEODORO MENDANHA SILVA
3977	t	86008544153	LARISSA.FSILVA	LARISSA FRANCISCO MAIA SILVA
3978	t	01483491102	alinne.sbastos	ALINNE SOUZA BASTOS
3979	t	02081069369	CLEDILCE.ASILVA	CLEDILCE ABREU MONDEGO SILVA
3980	t	04975454114	RAFAELA.TSILVA	RAFAELA TAVARES DA SILVA
3981	t	00287811123	vanessa.lmoreira	VANESSA LEITE MOREIRA PASSOS
3982	t	04493587121	Matheus.hsantiago	Matheus.hsantiago
3983	t	69873119191	maria.crosa	MARIA DO CARMO DE OLIVEIRA ROSA
3984	t	05357694150	palloma.drocha	PALLOMA OLIVEIRA DUARTE ROCHA
3985	t	00924513144	WENDER.JCOUTINHO	WENDER JUNQUEIRA COUTINHO
3986	t	06031652145	barbara.rsilva	BARBARA LUCENA RIBEIRO DA SILVA
3987	t	74521365353	cleilton.rsilva	CLEILTON RODRIGUES DA SILVA
3988	t	89520424172	CAMILA.DIAS	CAMILA DIAS FERREIRA DE OLIVEIRA
3989	t	00485186110	bryan.vsilva	BRYAN VALERIO DA SILVA
3990	t	00710705166	LEONARDO.FBARROS	LEONARDO DE FARIA BARROS
3991	t	47590629391	deborah.oliveira	DEBORAH PRAZERES SARAIVA OLIVEIRA NASCIMENTO
3992	t	01920522174	jose.rmatos	JOSE ELPIDIO RODRIGUES DE MATOS
3993	t	99806630149	JOSLEY.DROCHA	JOSLEY DE CRISTO ROCHA
3994	t	01456467182	fernanda.lporto	fernanda.lporto
3995	t	57489300134	MARCOS.AFRANCISCO	MARCOS ANTONIO FRANCISCO
3996	t	53623355100	LEANDRO.MACHADO	LEANDRO VIEIRA MACHADO
3997	t	41018222120	GILDERIA.MENDES	GILDERIA CRISTINA MENDES
3998	t	64890694153	JONH.KENEDY	JONHY KENEDY ALVES DOS SANTOS
3999	t	87042231104	VIVIAN.OBARBOSA	VIVIAN DE OLIVERA BARBOSA
4000	t	00017447127	rosalvo.mcastro	rosalvo.mcastro
4001	t	05520993173	maricon.rsilva	maricon.rsilva
4002	t	69312079115	ricardo.pparreira	ricardo.pparreira
4003	t	71296832104	mario.agoncalves	mario.agoncalves
4004	t	83723714153	CRISTIANE.PFELIPE	CRISTIANE PERNET FELIPE
4005	t	75772841300	LUCIANO.SCOELHO	LUCIANO DA SILVA COELHO
4006	t	92712959191	ana.ppereira	ANA PAULA DE SOUZA PEREIRA
4007	t	04262283160	raphael.farias	RAPHAEL TELES MACIEL FARIAS
4008	t	04019607141	alex.otomaz	ALEX JUNIO OLIVEIRA TOMAZ
4009	t	02466338165	murilo.bmendes	MURILO BARBARESCO MENDES
4010	t	03465272102	agnaldo.vfilho	AGNALDO DA VEIGA JARDIM FILHO
4011	t	15837204644	ingryd.cdias	INGRYD CARVALHO DIAS
4012	t	02390567282	crystian.sstering	CRYSTIAN SOUSA STERING
4013	t	49319191115	itamar.sassuncao	ITAMAR SOUSA DE ASSUNCAO
4014	t	04765243184	VITOR.CSILVA	VITOR DA COSTA SILVA
4015	t	02682090141	bruno.galves	BRUNO GOMES ALVES
4016	t	02495390129	kaine.joliveira	KAINE DE JESUS ROCHA NAVES DE OLIVEIRA
4017	t	01523733144	wendell.jlopes	wendell.jlopes
4018	t	70544929195	shara.eoliveira	SHARA ELIAS DE OLIVEIRA
4019	t	04599310166	NAGELA.TSA	NAGELA TEIXEIRA DE SA
4020	t	74558463200	donovan.mmachado	DONOVAN MC DULLES LIMA MACHADO
4021	t	01719368252	larissa.agadelha	LARISSA ALVES GOMES GADELHA
4022	t	66417732134	KARLA.SAIDAR	KARLA DE SADA AIDAR
4023	t	01026345308	WANESSA.RROCHA	WANESSA RAQUEL ROSA ROCHA
4024	t	05376760154	VINICIUS.PARRUDA	VINICIUS MATEUS PINHO DE ARRUDA
4025	t	03858970140	JESSICA.BDIAS	JESSICA BISPO DIAS
4026	t	60040019373	pedro.gdamasceno	PEDRO GABRIEL DE PAIVA MAIA DAMASCENO
4027	t	08053491626	mayara.moliveira	MAYARA MARJORE DOS SANTOS OLIVEIRA
4028	t	86636073187	julio.rsilva	JULIO GLECIS RIBEIRO DA SILVA
4029	t	04409565109	eveline.pmesquita	EVELINE PEREIRA DE MESQUITA
4030	t	02885734124	taynara.nsilva	TAYNARA DO NASCIMENTO SILVA
4031	t	04486216148	reny.scanedo	RENY DOS SANTOS CANEDO
4032	t	03566732354	joana.ccamelo	JOANA KATIA COSTA CUNHA CAMELO
4033	t	73270873115	leonardo.moliveira	LEONARDO MACHADO DE OLIVEIRA
4034	t	05857768144	MARIA.CMARTINS	MARIA LENITA COSTA MARTINS
4035	t	01724450271	daiara.gcosta	DAIARA GOMES DA COSTA
4036	t	02086308132	juliana.ssouza	JULIANA SAMIRA DE SOUZA
4037	t	12578669660	tayna.lgomes	TAYNA LOBATO BATISTA GOMES
4038	t	78831016172	joao.bpioven	JOAO LENO BUZAHAR PIOVEZAN
4039	t	71554530130	joao.fcouto	JOAO PAULO FIGUEIREDO COUTO
4040	t	29083355810	ALCIMARA.PCOSTA	ALCIMARA PUSSU LIMA COSTA
4041	t	00922371180	tassia.lbarroso	TASSIA LACERDA BARROSO
4042	t	06465809818	JULIO.FCOSTA	JULIO CESAR FERREIRA DA COSTA
4043	t	01321944144	WENDER.APEIXOTO	WENDER ALMEIDA PEIXOTO
4044	t	01542678137	MARCOS.MLIRA	MARCOS MARCIEL LIRA
4045	t	01728354102	osvaldo.ojunior	OSVALDO MARQUES DE OLIVEIRA JUNIOR
4046	t	70403053137	maria.vsilva	MARIA VITORIA VALERIO BRITO SILVA
4047	t	03056700000	morgana.slopes	MORGANA DA SILVA LOPES
4048	t	04544286182	jakeline.bvaranda	JAKELINE BARBOSA CARVALHO VARANDA
4049	t	03662073188	helio.mmaciel	HELIO MENDES MACIEL
4050	t	04160768156	danielle.lfreitas	DANIELLE LIMA SOARES DE FREITAS
4051	t	04385176108	nayane.fsilva	NAYANE LINDICE FERNANDES SILVA
4052	t	04179832127	marissa.mcosta	MARISSA MORAES DA COSTA
4053	t	70343671166	jessica.malves	JESSICA MENDES ALVES
4054	t	00811613100	iracy.cferreira	IRACY CRISTINA DA ROCHA FERREIRA
4055	t	89328957168	MARIANO.MIRANDA	MARIANO DA SILVA MIRANDA
4056	t	70012131105	beatriz.ssouza	BEATRIZ DA SILVA DE SOUZA
4057	t	69768820144	atyla.ssilva	atyla.ssilva
4058	t	07034723133	wanessa.csilva	WANESSA CARLA SILVA CANDIDO
4059	t	03393891170	JOHNATHAN.SSANTOS	JOHNATHAN RODRIGUES SANTANA DOS SANTOS
4060	t	99898446315	yara.dpimentel	YARA DINIZ PIMENTEL
4061	t	04224958678	rosana.msantana	ROSANA MARIA SANTANA
4062	t	70029303117	tatiane.ffreitas	TATIANE FERREIRA FREITAS
4063	t	70029214106	renata.sferreira	RENATA SALVIANO FERREIRA
4064	t	03922578152	gilmar.mjunior	GILMAR CARDOSO DE MELO JUNIOR
4065	t	01120792100	luana.csousa	LUANA CACIANO DE SOUSA
4066	t	03428340108	flaviane.mdias	FLAVIANE MUNIZ DIAS
4067	t	06501740380	jueli.rferreira	JUELI RAMOS DOS SANTOS FERREIRA
4068	t	05352281396	luis.fsantos	LUIS FELIPE COIMBRA DOS SANTOS
4069	t	70823086127	gabriel.caquino	GABRIEL CORTES DE AQUINO
4070	t	90546369200	wadson.rlemos	WADSON ROCHA LEMOS
4071	t	02444668154	GABRIEL.AAIRES	GABRIEL ARAUJO AIRES
4072	t	37060961100	EDSON.FERREIRA	EDSON PORTO FERREIRA
4073	t	02277540340	lucas.plima	LUCAS PAZ LIMA
4074	t	75043297115	KELVEN.RSANTOS	KELVEN IZAQUI RODRIGUES SANTOS
4075	t	00164355162	roberto.ipacheco	ROBERTO INACIO RODRIGUES PACHECO
4076	t	13010816120	jose.mcosta	JOSE MIGUEL COSTA GOMES
4077	t	04042260276	susana.rbezerra	SUSANA RIBEIRO BEZERRA
4078	t	04164739118	leticia.blima	LETICIA JEANNE BARAGEHUM LIMA
4079	t	70542593190	leticia.smarques	LETICIA DOS SANTOS MARQUES
4080	t	75088835191	jessica.fatima	JESSICA DE FATIMA SANTOS
4081	t	63547180325	MARCO.CARVALHO	MARCO AURELIO CARVALHO DA SILVA
4082	t	04799170198	gustavo.aferreira	GUSTAVO ALVES FERREIRA
4083	t	02435617178	DANIELI.DSILVA	DANIELI DA SILVA
4084	t	06908231186	rafhael.otoni	RAFHAEL DOS SANTOS SILVA OTONI
4085	t	06118781192	Vanessa.adias	VANESSA APRINIO DIAS
4086	t	03941372300	LAIZA.GSANCHEZ	LAISA GONCALVES SANCHEZ
4087	t	05827420131	sara.nviana	SARA NASCIMENTO VIANA
4088	t	67724108668	WALDEZ.GUIMARAES	WALDEZ LUIZ GUIMARAES
4089	t	02149934183	wanessa.areis	WANESSA ALVES DOS REIS
4090	t	06938844100	laura.sfarias	LAURA SIQUEIRA DE FARIAS
4091	t	04339615382	claudia.ftavares	CLAUDIA FERNANDA ALVES DE LIMA
4092	t	84768495168	FABIANO.MENDES	FABIANO DE OLIVEIRA MENDES
4093	t	02503246109	MAKLEY.CLAUDINO	MAKLEY GUEDES CLAUDINO
4094	t	06917030600	JOSE.TORRES	JOSE SIRLEI DIAS TORRES
4095	t	02295705185	RENATA.AREDES	RENATA VIEIRA DE ALMEIDA AREDES
4096	t	01767096224	ALEX.NAZEVEDO	ALEX NUNES DE AZEVEDO
4097	t	78208297100	DENIS.SEGANTINE	DENIS SEGANTINE DOS SANTOS
4098	t	03807716165	andressa.silva	ANDRESSA ANGELICA SILVA
4099	t	05954440166	melina.maraschini	MELINA HIDALIA MARASCHINI
4100	t	70082988110	meigna.aribeiro	MEIGNA AYSLA DE CASTRO RIBEIRO
4101	t	01019322195	SUELEN.SOUSA	SUELEN CRISTINA RODRIGUES DE SOUSA
4102	t	78730503191	ADAO.ARAUJO	ADAO PAULO FERREIRA DE ARAUJO
4103	t	08028578683	RONALDO.MZANZARINI	RONALDO MILANI ZANZARINI
4104	t	06328455380	ana.ccosta	ANA MEIRE CANTANHEDE COSTA
4105	t	70541749170	ARTHUR.BSILVA	ARTHUR BRANDAO SILVA
4106	t	94651337368	marcio.rferreira	MARCIO RENE DA SILVA FERREIRA
4107	t	01462041159	santilha.rmenezes	SANTILHA KELLEN REZENDE MENEZES
4108	t	01489454144	RENATO.FSILVA	RENATO FREIRE DA SILVA
4109	t	02421622158	FLAVIA.AGOMES	FLAVIA ALLINE BATISTA DE ABREU LIMA GOMES
4110	t	52922197387	JONATHAN.AOLIVEIRA	JONATHAN ALVES DE OLIVEIRA
4111	t	28862872372	nubia.ccordeiro	MARA NUBIA COSTA CORDEIRO
4112	t	78715296334	ROSA.ACOELHO	ROSA ANDREA PORTELA PESSOA COELHO
4113	t	70103984100	ALYSSON.OLIVEIRA	ALYSSON HENRIQUE LIMA DE OLIVEIRA
4114	t	42305853068	JAMESON.HOEWELL	JAMESON HOEWELL
4116	t	01757820167	ergildo.cdias	JOSE ERGILDO CARVALHO DIAS
4117	t	81447590287	francilene.clima	FRANCILENE CORREIA DE LIMA
4118	t	04443844139	bruno.rabelo	BRUNO RABELO
4119	t	70751723100	clara.dsilva	MARIA CLARA DIAS DA SILVA
4120	t	06330672377	ANA.CSANTOS	ANA PAULA DE CARVALHO DOS SANTOS
4121	t	70307886131	marjhore.tbarreto	MARJHORE RAFAELLA SOUZA TEIXEIRA BARRETO
4122	t	70941407110	emily.bsilva	EMILY BARBOSA DA SILVA
4123	t	04415995160	edvan.csantos	EDVAN CLEITON DA SILVA SANTOS
4124	t	75731223149	JOSEANE.BOSCO	JOSEANE DA SILVA BOSCO
4125	t	71851828168	ANDRE.MSOUZA	ANDRE LUIZ MOREIRA DE SOUZA
4126	t	70365149144	andrielly.aoliveira	ANDRIELLY ALMEIDA DE OLIVEIRA
4127	t	78375754668	edson.fsegatto	EDSON FELIX SEGATTO
4128	t	05115354144	mariana.rsouza	MARIANA RIBEIRO DE SOUZA
4129	t	70566705125	kessia.rvale	KESSIA DALIANA OLIVEIRA BERNARDO
4130	t	39587916115	ANGELA.CMARTINS	ANGELA CRISTINE DINIZ MARTINS
4131	t	75526476120	INGRYD.PSANTOS	INGRYD LORRAYNE PEREIRA DOS SANTOS
4132	t	04605235116	marillia.psantos	MARILLIA DE PAULA SANTOS
4133	t	02657718116	luis.mlemes	LUIS GUILHERME DE MORAIS LEMES
4134	t	04907506139	leandro.lportela	LEANDRO LIMA PORTELA
4135	t	08869074625	THONY.DOMINGOS	THONY BONIFACIO DOMINGOS
4136	t	61851205349	ADRIANA.TGOMES	ADRIANA TAVARES SANTOS GOMES
4137	t	00579198103	ELISA.FOLGIERINI	ELISA PRISCILLA ESPINDOLA FOLGIERINI
4138	t	05029173692	MURILO.PATROCINIO	MURILO PATROCINIO DE OLIVEIRA
4139	t	46288414115	ANDRE.MADEIRA	ANDRE LUIZ GUERRA MADEIRA
4140	t	05295886689	ADRIANA.PIMENTEL	ADRIANA PIMENTEL GOMES
4141	t	70850330106	daiany.ftavares	DAIANY GOMES FERREIRA TAVARES
4142	t	03562687214	ana.pgoes	ANA PAULA PICANCO GOES
4143	t	01207066109	JOAN.MFRANCA	JOAN MENDES DE FRANCA
4144	t	04393839196	KAMYLLA.SANTOS	KAMYLLA EDUARDO DOS SANTOS
4145	t	58847154120	ANDREA.PSILVA	ANDREIA PADUA DA SILVA
4146	t	03085310193	eduardo.rfreitas	EDUARDO RODRIGUES DE FREITAS
4147	t	03570787133	marcos.rcosta	MARCOS WINYCIUS RIBEIRO COSTA
4148	t	93731167115	tatyana.fcosta	TATYANA FREITAS DA COSTA
4149	t	00701138157	LIVIA.MARAUJO	LIVIA MARLLEN AVELAR ARAUJO
4150	t	09920418994	suellen.kjordao	SUELLEN KATHELIN JORDAO
4151	t	73099660159	RENATA.SOUSA	RENATA DE SOUSA SANTOS
4152	t	06687684108	leticia.rvale	LETICIA GRAZZIELLY RODRIGUES DO VALE COELHO
4153	t	05753027121	samuel.rsousa	SAMUEL RODRIGUES DE SOUSA
4154	t	70747054118	lamoncrecio.msantos	LAMONCRECIO SULLYVAN MELO DOS SANTOS
4155	t	05503672178	jordana.mmoura	JORDANA MACHADO DE MOURA
4156	t	03347499131	kleandro.gsilva	KLEANDRO GONCALVES DE MEDEIROS E SILVA
4157	t	05144514324	isabelle.lmendes	ISABELLE LIMA MENDES
4158	t	06476968351	igor.cmoreno	IGOR CRUZ MORENO
4159	t	32293546802	jose.rmelo	JOSE RODRIGO GARCEZ DE MELO
4160	t	04798087165	MATHEUS.VMORAES	MATHEUS VINICIOS GABRIEL DE MORAES
4161	t	03423957182	ANA.SPAULA	ANA PAULA CHAVEIRO DA SILVA
4162	t	05014653162	CEZAR.BCOSTA	CEZAR BATISTA DA COSTA
4163	t	01066336350	NAYARA.VBARROZO	NAYARA FERNANDA VIEIRA BARROZO
4164	t	82729697187	IGNEZ.MSANTOS	IGNEZ MARIA SANTOS REINEHR ALVES
4165	t	02080574124	MAXIMULLER.FERREIRA	MAXIMULER SILVA FERREIRA
4166	t	99923580172	MARCO.ANTONIO	MARCO ANTONIO DE SOUZA MEDRADO FILHO
4167	t	57092125120	ALEXANDRA.RSILVA	ALEXANDRA RIBEIRO DA SILVA DE MAMAN
4168	t	79368042187	LUCIANO.ADUARTE	LUCIANO DE AGUIAR DUARTE
4169	t	43856144153	ALEXANDRE.ROCHA	ALEXANDRE GETINO ROCHA
4170	t	03982191173	hellen.spaz	HELLEN ORRANNA SOUSA PAZ
4171	t	00726021121	leonardo.freire	LEONARDO FREIRE PEREIRA
4172	t	62684230110	edezio.anicesio	EDEZIO AVELINO DE ANICESIO
4173	t	01430120100	andre.asantos	ANDRE APARECIDO DOS SANTOS
4174	t	02277276146	creidomario.pinheiro	CREIDOMARIO PINHEIRO
4175	t	45923507153	LUIZ.CLIMA	LUIZ CARLOS EVANGELISTA LIMA
4176	t	05232106120	DANIELA.PSANTIAGO	DANIELA CRISTINA DE PAULA SANTIAGO
4177	t	03232267106	francisco.tripardo	FRANCISCO TIAGO DE PAULA RIPARDO
4178	t	02754708146	thamires.rpereira	THAMIRES RAIANNE PEREIRA
4179	t	00467860157	ALESSANDRO.GASSIS	ALESSANDRO GOMES DE ASSIS
4180	t	02847992200	joyce.asantos	JOYCE ARAUJO DOS SANTOS
4181	t	04879364185	sabrina.vsilva	SABRINA SANTINA VIEIRA DA SILVA
4182	t	00429661169	RAINEY.TSILVA	RAINEY TONI DA SILVA
4183	t	60170374394	JOSIMARA.RPENHA	JOSIMARA RIBEIRO PENHA
4184	t	00766978192	RODRIGO.PCAMPOS	RODRIGO PEREIRA CAMPOS
4185	t	92924778115	THIAGO.RCARVALHO	THIAGO DA SILVA ROCHA CARVALHO
4186	t	04810659119	JONATHAN.ASILVA	JONATHAN AUGUSTO DA SILVA
4187	t	97898325168	wanderson.dsales	WANDERSON DIVINO AGUIAR SALES
4188	t	04829228180	thayanne.msouza	THAYANNE EMMANUELLY MONTANA DE SOUZA
4189	t	97178381191	eliane.assis	ELIANE CANDIDA ASSIS
4190	t	01206543183	lainna.rondon	LAINNA DE ALMEIDA RONDON
4191	t	92952305153	MARCIA.SAVES	MARCIA PENARIOL SAVES
4192	t	87286670182	diego.scampos	DIEGO SARAIVA DA SILVA CAMPOS
4193	t	00276894170	JOAO.PRODRIGUES	JOAO PAULO RODRIGUES
4194	t	00236116347	LIGIA.LPACHECO	LIGIA MARCELA LOPES PACHECO
4195	t	02964019193	thays.sbrito	THAYS CAROLINE SOUSA DE BRITO
4196	t	70057558124	PAULO.CNASCIMENTO	PAULO CEZAR DO NASCIMENTO
4197	t	03023886130	fernanda.ssantana	FERNANDA GOMES DA SILVA SANTANA
4198	t	65883390110	sandra.aoliveira	SANDRA REGINA ARAUJO DE OLIVEIRA
4199	t	01289329109	deyvid.rmartins	DEYVID RODRIGUES MARTINS
4200	t	01331856132	SUELI.FAMANCIO	SUELI FRANCISCO AMANCIO
4201	t	11027677606	MARCELA.OLIVEIRA	MARCELA FERREIRA OLIVEIRA
4202	t	01200870140	ALEXANDRE.SSILVA	ALEXANDRE SOUSA DA SILVA
4203	t	00981502008	JULIANO.BASSAN	JULIANO COSTA BASSAN
4204	t	77736583668	TULIO.ATABATINGA	TULIO ALBUQUERQUE TABATINGA
4205	t	25777392814	TADEU.FJUNIOR	TADEU ANTONIO FERNANDES JUNIOR
4206	t	04373900124	thiago.rsilva	PEDRO THIAGO RIBEIRO DA SILVA
4207	t	00256479119	FABRICIO.SILVA	FABRICIO HENRIQUE DA SILVA
4208	t	70365078107	marcos.vsousa	MARCOS VINICIUS SOARES DE SOUSA
4209	t	60704335301	adriana.mmatos	ADRIANA MARTINS MATOS
4210	t	03499207109	BRUNO.MSILVA	BRUNO MEDEIROS DA SILVA
4211	t	00990984150	geovane.sbouzada	GEOVANE SILVA BOUZADA
4212	t	01088617166	douglas.mborges	DOUGLAS MARTINS BORGES
4213	t	75232634100	matheus.dsilva	MATHEUS GOMES DA SILVA
4214	t	70076831183	guilherme.csantos	GUILHERME CAMPOS DOS SANTOS
4215	t	02730558101	felipe.acosta	FELIPE AUGUSTO COSTA SOUSA
4216	t	02424613362	fernanda.osilva	FERNANDA OLIVEIRA DA SILVA
4217	t	06132450165	victoria.meneghetti	VICTORIA MENEGHETTI MARQUEZAM
4218	t	70279741189	brunno.rgoncalves	BRUNNO RODRIGUES GONCALVES
4219	t	70139952152	lorena.bsantos	LORENA BARSANULFO DOS SANTOS
4220	t	02564728154	leticia.sferreira	LETICIA SOARES FERREIRA
4221	t	10115643605	ygor.asilva	YGOR ARAUJO SILVA
4222	t	77818121153	ana.bferreira	ANA CELMA BALDOINO FERREIRA
4223	t	83774416168	CLAITON.PSILVA	CLAITON PEREIRA DA SILVA
4224	t	41864473819	thainan.gfranco	THAINAN GUERRA FRANCO
4225	t	36505260854	TAIS.ASILVA	TAIS ALVES DA SILVA
4226	t	01422494160	roberto.eribeiro	ROBERTO ERIC VIEIRA RIBEIRO
4227	t	03378126167	brennda.pcunha	BRENNDA PALMERSTON RODRIGUES DA CUNHA
4228	t	94420157291	karen.ncarvalho	KAREN LYANE NASCIMENTO DE CARVALHO
4229	t	03620994137	thiciane.osilva	THICIANE OLIVEIRA SILVA
4230	t	06467992156	leticia.csilva	LETICIA CRISTINA DA SILVA
4231	t	00493975160	LEANDRO.RLEITE	LEANDRO GUIMARAES RODRIGUES LEITE
4232	t	03619888183	GREYCIELLE.LUANNE	GREYCIELLE LUANNE SANTOS RODRIGUES
4233	t	00991012119	THATHIANA.CABRAL	THATHIANA BORGES CABRAL
4234	t	00238011232	cristiane.randrade	CRISTIANE REGES DE ANDRADE
4235	t	00331499177	maryelle.aoliveira	MARYELLE APARECIDA ARAUJO DE OLIVEIRA
4236	t	02158366265	danieli.mboeri	DANIELI MOURA BOERI
4237	t	89514777115	ANTONIO.SROGERIO	ANTONIO DA SILVA ROGERIO
4238	t	89008758172	neimar.ejesus	NEIMAR ADRIANO MOREIRA FERNANDES ETERNO DE JESUS
4239	t	03117310150	camila.cmarinho	CAMILA CASSIMIRO MARINHO
4240	t	03143962178	milca.canjos	MILCA SEVERINO COELHO ANJOS
4241	t	72417510197	EDUARDO.MONTE	EDUARDO DE ASSIS MONTE
4242	t	96733780615	LILIANE.FERNANDES	LILIANE PEIXOTO BARBOSA FERNANDES
4243	t	55920489120	tony.fsilva	TONY FRANCISCO DA SILVA
4244	t	70849773172	vinicius.pevangelist	VINICIUS PEREIRA EVANGELISTA
4245	t	03609300183	rafael.oliveira	RAFAEL DE OLIVEIRA DA SILVA
4246	t	01564172198	vanusa.roliveira	VANUSA DA SILVA RODRIGUES OLIVEIRA
4247	t	05087019102	amanda.balbuquerque	AMANDA BHARBARA ALBUQUERQUE
4248	t	01696163129	robson.mfernandes	ROBSON MARTINS FERNADES
4249	t	86318233172	luciana.pdias	LUCIANA PEREIRA DIAS
4250	t	70074867113	jordana.cgomes	JORDANA CRHISTHINE GOMES
4251	t	02369871199	bruna.vsalim	BRUNA VICTORIA URBANO SALIM
4252	t	42675487813	rafael.ateixeira	rafael.ateixeira
4253	t	30536340153	GIOVANE.AGARRIDO	GIOVANE ALVES GARRIDO
4254	t	77799917172	peterson.acastro	PETERSON ANDRADE CASTRO
4255	t	08437889766	RENATO.RSOUSA	RENATO RODRIGUES DE SOUSA
4256	t	01205899154	thayane.asantos	THAYANE ALVES DOS SANTOS
4257	t	03490664183	guilherme.rgomes	GUILHERME ROBERTO GOMES
4258	t	73222755191	andre.costa	ANDRE LUIZ DA COSTA DE CASTRO
4259	t	01510182195	THANMILLYN.VSILVA	THANMILLYN VILLANYH CAETANO DA SILVA
4260	t	14287942639	marcus.crocha	MARCUS VINICIUS CORTES DA ROCHA
4261	t	05869852129	CAROLINE.PNUNES	CAROLINE PORTO NUNES
4262	t	70766417123	maria.pfreire	MARIA RAFAELLA PEREIRA FREIRE
3022	f	03989899155	JOSE.MMARINHO	JOSE LUCAS MIRANDA MARINHO
\.


--
-- Data for Name: veiculopatrimonio; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.veiculopatrimonio (id, anofabricacao, anomodelo, marca, modelo, placa, valor) FROM stdin;
\.


--
-- Data for Name: veiculoproposta; Type: TABLE DATA; Schema: public; Owner: dbroot
--

COPY public.veiculoproposta (id, adaptado, anoinformadomanualmente, chassi, codigomodelofipe, codigomodelomarca, codigomodelomolicar, cor, descricaomarca, descricaomodelo, placa, taxi, veiculoanofabricacao, veiculoanomodelo, veiculocodigo, veiculocorcodexterna, veiculoestadocodplaca, veiculoestadonomeplaca, veiculokm, veiculomodeloveiculocod, veiculomunicipionomeplacaibge, veiculonrrenavam, veiculostatus) FROM stdin;
\.


--
-- Name: alternativarespostasvs alternativarespostasvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.alternativarespostasvs
    ADD CONSTRAINT alternativarespostasvs_pkey PRIMARY KEY (id);


--
-- Name: andamentooperacao andamentooperacao_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.andamentooperacao
    ADD CONSTRAINT andamentooperacao_pkey PRIMARY KEY (id);


--
-- Name: ano_mod_comb_santander ano_mod_comb_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ano_mod_comb_santander
    ADD CONSTRAINT ano_mod_comb_santander_pkey PRIMARY KEY (id);


--
-- Name: anunciosvs anunciosvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.anunciosvs
    ADD CONSTRAINT anunciosvs_pkey PRIMARY KEY (id);


--
-- Name: aprovacao_bancaria_motivos aprovacao_bancaria_motivos_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.aprovacao_bancaria_motivos
    ADD CONSTRAINT aprovacao_bancaria_motivos_pkey PRIMARY KEY (aprovacao_id, motivo_id);


--
-- Name: aprovacaobancaria aprovacaobancaria_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.aprovacaobancaria
    ADD CONSTRAINT aprovacaobancaria_pkey PRIMARY KEY (id);


--
-- Name: atividadeeconomicasantander atividadeeconomicasantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.atividadeeconomicasantander
    ADD CONSTRAINT atividadeeconomicasantander_pkey PRIMARY KEY (id);


--
-- Name: atividadeleadsvs atividadeleadsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.atividadeleadsvs
    ADD CONSTRAINT atividadeleadsvs_pkey PRIMARY KEY (id);


--
-- Name: avalista avalista_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista
    ADD CONSTRAINT avalista_pkey PRIMARY KEY (id);


--
-- Name: avalista_svs_imoveis_pat avalista_svs_imoveis_pat_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_imoveis_pat
    ADD CONSTRAINT avalista_svs_imoveis_pat_pkey PRIMARY KEY (avalista_id, imovel_id);


--
-- Name: avalista_svs_outrasrendas avalista_svs_outrasrendas_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_outrasrendas
    ADD CONSTRAINT avalista_svs_outrasrendas_pkey PRIMARY KEY (avalista_id, outrarenda_id);


--
-- Name: avalista_svs_veiculos_pat avalista_svs_veiculos_pat_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_veiculos_pat
    ADD CONSTRAINT avalista_svs_veiculos_pat_pkey PRIMARY KEY (avalista_id, veiculo_id);


--
-- Name: banco banco_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.banco
    ADD CONSTRAINT banco_pkey PRIMARY KEY (id);


--
-- Name: bancosantander bancosantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.bancosantander
    ADD CONSTRAINT bancosantander_pkey PRIMARY KEY (id);


--
-- Name: calculorentabilidade calculorentabilidade_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.calculorentabilidade
    ADD CONSTRAINT calculorentabilidade_pkey PRIMARY KEY (id);


--
-- Name: campoparsesvs campoparsesvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.campoparsesvs
    ADD CONSTRAINT campoparsesvs_pkey PRIMARY KEY (id);


--
-- Name: cidade cidade_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cidade
    ADD CONSTRAINT cidade_pkey PRIMARY KEY (id);


--
-- Name: cliente cliente_cpfcnpj_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_cpfcnpj_key UNIQUE (cpfcnpj);


--
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- Name: cnpj_if_prod_fin_v2 cnpj_if_prod_fin_v2_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cnpj_if_prod_fin_v2
    ADD CONSTRAINT cnpj_if_prod_fin_v2_pkey PRIMARY KEY (cnpj_inst_financeira_id, produto_financeiro_id);


--
-- Name: cnpj_inst_financ cnpj_inst_financ_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cnpj_inst_financ
    ADD CONSTRAINT cnpj_inst_financ_pkey PRIMARY KEY (id);


--
-- Name: conf_par_emails_dom conf_par_emails_dom_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.conf_par_emails_dom
    ADD CONSTRAINT conf_par_emails_dom_pkey PRIMARY KEY (configuracao_id, dominio_email_id);


--
-- Name: configuracaoparsesvs configuracaoparsesvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.configuracaoparsesvs
    ADD CONSTRAINT configuracaoparsesvs_pkey PRIMARY KEY (id);


--
-- Name: conjuge conjuge_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.conjuge
    ADD CONSTRAINT conjuge_pkey PRIMARY KEY (id);


--
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- Name: dominioemailsvs dominioemailsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.dominioemailsvs
    ADD CONSTRAINT dominioemailsvs_pkey PRIMARY KEY (id);


--
-- Name: emailblacklist emailblacklist_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.emailblacklist
    ADD CONSTRAINT emailblacklist_pkey PRIMARY KEY (id);


--
-- Name: endereco endereco_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id);


--
-- Name: entidadeteste entidadeteste_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.entidadeteste
    ADD CONSTRAINT entidadeteste_pkey PRIMARY KEY (id);


--
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- Name: estado_santander estado_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.estado_santander
    ADD CONSTRAINT estado_santander_pkey PRIMARY KEY (id);


--
-- Name: estadocivilsantander estadocivilsantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.estadocivilsantander
    ADD CONSTRAINT estadocivilsantander_pkey PRIMARY KEY (id);


--
-- Name: faturamentoclientesvs faturamentoclientesvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.faturamentoclientesvs
    ADD CONSTRAINT faturamentoclientesvs_pkey PRIMARY KEY (id);


--
-- Name: financiamentoonlineitau financiamentoonlineitau_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlineitau
    ADD CONSTRAINT financiamentoonlineitau_pkey PRIMARY KEY (id);


--
-- Name: financiamentoonlinesantander financiamentoonlinesantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT financiamentoonlinesantander_pkey PRIMARY KEY (id);


--
-- Name: fonteleadsvs fonteleadsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.fonteleadsvs
    ADD CONSTRAINT fonteleadsvs_pkey PRIMARY KEY (id);


--
-- Name: formapagamentosantander formapagamentosantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.formapagamentosantander
    ADD CONSTRAINT formapagamentosantander_pkey PRIMARY KEY (id);


--
-- Name: fornecedorclientesvs fornecedorclientesvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.fornecedorclientesvs
    ADD CONSTRAINT fornecedorclientesvs_pkey PRIMARY KEY (id);


--
-- Name: grauparentescosantander grauparentescosantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.grauparentescosantander
    ADD CONSTRAINT grauparentescosantander_pkey PRIMARY KEY (id);


--
-- Name: hist_int_leads_icarros hist_int_leads_icarros_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.hist_int_leads_icarros
    ADD CONSTRAINT hist_int_leads_icarros_pkey PRIMARY KEY (id);


--
-- Name: historicoalteracao historicoalteracao_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.historicoalteracao
    ADD CONSTRAINT historicoalteracao_pkey PRIMARY KEY (id);


--
-- Name: historicoleadsvs historicoleadsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.historicoleadsvs
    ADD CONSTRAINT historicoleadsvs_pkey PRIMARY KEY (id);


--
-- Name: icarrostoken icarrostoken_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.icarrostoken
    ADD CONSTRAINT icarrostoken_pkey PRIMARY KEY (id);


--
-- Name: icarrosusuario icarrosusuario_clientid_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.icarrosusuario
    ADD CONSTRAINT icarrosusuario_clientid_key UNIQUE (clientid);


--
-- Name: icarrosusuario icarrosusuario_descricao_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.icarrosusuario
    ADD CONSTRAINT icarrosusuario_descricao_key UNIQUE (descricao);


--
-- Name: icarrosusuario icarrosusuario_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.icarrosusuario
    ADD CONSTRAINT icarrosusuario_pkey PRIMARY KEY (id);


--
-- Name: imovelpatrimonio imovelpatrimonio_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.imovelpatrimonio
    ADD CONSTRAINT imovelpatrimonio_pkey PRIMARY KEY (id);


--
-- Name: instituicaofinanceira instituicaofinanceira_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.instituicaofinanceira
    ADD CONSTRAINT instituicaofinanceira_pkey PRIMARY KEY (id);


--
-- Name: itemcbc itemcbc_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.itemcbc
    ADD CONSTRAINT itemcbc_pkey PRIMARY KEY (id);


--
-- Name: json_credit_an_itau json_credit_an_itau_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_credit_an_itau
    ADD CONSTRAINT json_credit_an_itau_pkey PRIMARY KEY (id);


--
-- Name: json_ident_santander json_ident_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_ident_santander
    ADD CONSTRAINT json_ident_santander_pkey PRIMARY KEY (id);


--
-- Name: json_preanalise_santander json_preanalise_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_preanalise_santander
    ADD CONSTRAINT json_preanalise_santander_pkey PRIMARY KEY (id);


--
-- Name: json_req_sim_santander json_req_sim_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_req_sim_santander
    ADD CONSTRAINT json_req_sim_santander_pkey PRIMARY KEY (id);


--
-- Name: json_sim_resp_itau json_sim_resp_itau_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_sim_resp_itau
    ADD CONSTRAINT json_sim_resp_itau_pkey PRIMARY KEY (id);


--
-- Name: json_transac_req_itau json_transac_req_itau_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_transac_req_itau
    ADD CONSTRAINT json_transac_req_itau_pkey PRIMARY KEY (id);


--
-- Name: jsonrequisicaoproposta jsonrequisicaoproposta_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.jsonrequisicaoproposta
    ADD CONSTRAINT jsonrequisicaoproposta_pkey PRIMARY KEY (id);


--
-- Name: jsonrespostaproposta jsonrespostaproposta_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.jsonrespostaproposta
    ADD CONSTRAINT jsonrespostaproposta_pkey PRIMARY KEY (id);


--
-- Name: jsonrespostasimulacaosantander jsonrespostasimulacaosantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.jsonrespostasimulacaosantander
    ADD CONSTRAINT jsonrespostasimulacaosantander_pkey PRIMARY KEY (id);


--
-- Name: leadsvs leadsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT leadsvs_pkey PRIMARY KEY (id);


--
-- Name: leadsvsenriquecido leadsvsenriquecido_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvsenriquecido
    ADD CONSTRAINT leadsvsenriquecido_pkey PRIMARY KEY (id);


--
-- Name: marcasantander marcasantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.marcasantander
    ADD CONSTRAINT marcasantander_pkey PRIMARY KEY (id);


--
-- Name: masc_taxa_tabela_fin masc_taxa_tabela_fin_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.masc_taxa_tabela_fin
    ADD CONSTRAINT masc_taxa_tabela_fin_pkey PRIMARY KEY (id);


--
-- Name: mod_veic_sant_unidades mod_veic_sant_unidades_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.mod_veic_sant_unidades
    ADD CONSTRAINT mod_veic_sant_unidades_pkey PRIMARY KEY (modelo_id, unidade_id);


--
-- Name: modelo_quest_unidades modelo_quest_unidades_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modelo_quest_unidades
    ADD CONSTRAINT modelo_quest_unidades_pkey PRIMARY KEY (modelo_questionario_id, unidade_id);


--
-- Name: modelo_santander modelo_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modelo_santander
    ADD CONSTRAINT modelo_santander_pkey PRIMARY KEY (id);


--
-- Name: modeloquestionariosvs modeloquestionariosvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modeloquestionariosvs
    ADD CONSTRAINT modeloquestionariosvs_pkey PRIMARY KEY (id);


--
-- Name: modeloveiculo modeloveiculo_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modeloveiculo
    ADD CONSTRAINT modeloveiculo_pkey PRIMARY KEY (id);


--
-- Name: motivoaprovacaosvs motivoaprovacaosvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.motivoaprovacaosvs
    ADD CONSTRAINT motivoaprovacaosvs_pkey PRIMARY KEY (id);


--
-- Name: motivodescartesvs motivodescartesvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.motivodescartesvs
    ADD CONSTRAINT motivodescartesvs_pkey PRIMARY KEY (id);


--
-- Name: municipio municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (id);


--
-- Name: municipio_santander municipio_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.municipio_santander
    ADD CONSTRAINT municipio_santander_pkey PRIMARY KEY (id);


--
-- Name: nacionalidadesantander nacionalidadesantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.nacionalidadesantander
    ADD CONSTRAINT nacionalidadesantander_pkey PRIMARY KEY (id);


--
-- Name: naturezajuridicasantander naturezajuridicasantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.naturezajuridicasantander
    ADD CONSTRAINT naturezajuridicasantander_pkey PRIMARY KEY (id);


--
-- Name: notaleadsvs notaleadsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.notaleadsvs
    ADD CONSTRAINT notaleadsvs_pkey PRIMARY KEY (id);


--
-- Name: ocupacao ocupacao_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ocupacao
    ADD CONSTRAINT ocupacao_pkey PRIMARY KEY (id);


--
-- Name: op_finan_online_santander op_finan_online_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.op_finan_online_santander
    ADD CONSTRAINT op_finan_online_santander_pkey PRIMARY KEY (id, financiamento_id);


--
-- Name: opcaoitemcbc opcaoitemcbc_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.opcaoitemcbc
    ADD CONSTRAINT opcaoitemcbc_pkey PRIMARY KEY (id);


--
-- Name: opcionaisveiculoproposta opcionaisveiculoproposta_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.opcionaisveiculoproposta
    ADD CONSTRAINT opcionaisveiculoproposta_pkey PRIMARY KEY (id, opcional_id);


--
-- Name: opcionalveiculo opcionalveiculo_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.opcionalveiculo
    ADD CONSTRAINT opcionalveiculo_pkey PRIMARY KEY (id);


--
-- Name: operacaofinanciada operacaofinanciada_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT operacaofinanciada_pkey PRIMARY KEY (id);


--
-- Name: operacaofinanciadacalcenv operacaofinanciadacalcenv_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciadacalcenv
    ADD CONSTRAINT operacaofinanciadacalcenv_pkey PRIMARY KEY (id, calculo_id);


--
-- Name: operacaofinanciadatodasrent operacaofinanciadatodasrent_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciadatodasrent
    ADD CONSTRAINT operacaofinanciadatodasrent_pkey PRIMARY KEY (id, calculo_id);


--
-- Name: outrarendaclientesvs outrarendaclientesvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.outrarendaclientesvs
    ADD CONSTRAINT outrarendaclientesvs_pkey PRIMARY KEY (id);


--
-- Name: parcelaentrada parcelaentrada_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.parcelaentrada
    ADD CONSTRAINT parcelaentrada_pkey PRIMARY KEY (id);


--
-- Name: perfilsvs perfilsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.perfilsvs
    ADD CONSTRAINT perfilsvs_pkey PRIMARY KEY (id);


--
-- Name: perguntaquestionariosvs perguntaquestionariosvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.perguntaquestionariosvs
    ADD CONSTRAINT perguntaquestionariosvs_pkey PRIMARY KEY (id);


--
-- Name: porteempresasantander porteempresasantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.porteempresasantander
    ADD CONSTRAINT porteempresasantander_pkey PRIMARY KEY (id);


--
-- Name: produtofinanceiro produtofinanceiro_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.produtofinanceiro
    ADD CONSTRAINT produtofinanceiro_pkey PRIMARY KEY (id);


--
-- Name: produtointeresse produtointeresse_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.produtointeresse
    ADD CONSTRAINT produtointeresse_pkey PRIMARY KEY (id);


--
-- Name: profissaosantander profissaosantander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.profissaosantander
    ADD CONSTRAINT profissaosantander_pkey PRIMARY KEY (id);


--
-- Name: proposta proposta_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.proposta
    ADD CONSTRAINT proposta_pkey PRIMARY KEY (id);


--
-- Name: referenciabancaria referenciabancaria_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.referenciabancaria
    ADD CONSTRAINT referenciabancaria_pkey PRIMARY KEY (id);


--
-- Name: referenciapessoal referenciapessoal_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.referenciapessoal
    ADD CONSTRAINT referenciapessoal_pkey PRIMARY KEY (id);


--
-- Name: regra_spam_fontes_lead regra_spam_fontes_lead_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_fontes_lead
    ADD CONSTRAINT regra_spam_fontes_lead_pkey PRIMARY KEY (regra_id, fonte_lead_id);


--
-- Name: regra_spam_unidades regra_spam_unidades_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_unidades
    ADD CONSTRAINT regra_spam_unidades_pkey PRIMARY KEY (regra_id, unidade_id);


--
-- Name: regraspamsvs regraspamsvs_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regraspamsvs
    ADD CONSTRAINT regraspamsvs_pkey PRIMARY KEY (id);


--
-- Name: tabela_financ_unidades_v2 tabela_financ_unidades_v2_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tabela_financ_unidades_v2
    ADD CONSTRAINT tabela_financ_unidades_v2_pkey PRIMARY KEY (tabela_financeira_id, unidade_id);


--
-- Name: tabelafinanceira tabelafinanceira_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tabelafinanceira
    ADD CONSTRAINT tabelafinanceira_pkey PRIMARY KEY (id);


--
-- Name: taxa_perfil_if taxa_perfil_if_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxa_perfil_if
    ADD CONSTRAINT taxa_perfil_if_pkey PRIMARY KEY (id);


--
-- Name: taxa_tabela_financeira taxa_tabela_financeira_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxa_tabela_financeira
    ADD CONSTRAINT taxa_tabela_financeira_pkey PRIMARY KEY (id);


--
-- Name: taxaperfiltabelafinanceira taxaperfiltabelafinanceira_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxaperfiltabelafinanceira
    ADD CONSTRAINT taxaperfiltabelafinanceira_pkey PRIMARY KEY (id);


--
-- Name: telefone telefone_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.telefone
    ADD CONSTRAINT telefone_pkey PRIMARY KEY (id);


--
-- Name: tipo_ativ_ec_santander tipo_ativ_ec_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipo_ativ_ec_santander
    ADD CONSTRAINT tipo_ativ_ec_santander_pkey PRIMARY KEY (id);


--
-- Name: tipo_doc_santander tipo_doc_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipo_doc_santander
    ADD CONSTRAINT tipo_doc_santander_pkey PRIMARY KEY (id);


--
-- Name: tipo_rel_emp_santander tipo_rel_emp_santander_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipo_rel_emp_santander
    ADD CONSTRAINT tipo_rel_emp_santander_pkey PRIMARY KEY (id);


--
-- Name: tipoendereco tipoendereco_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipoendereco
    ADD CONSTRAINT tipoendereco_pkey PRIMARY KEY (id);


--
-- Name: tipopagamento tipopagamento_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipopagamento
    ADD CONSTRAINT tipopagamento_pkey PRIMARY KEY (id);


--
-- Name: tipotabelafinanceira tipotabelafinanceira_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipotabelafinanceira
    ADD CONSTRAINT tipotabelafinanceira_pkey PRIMARY KEY (id);


--
-- Name: tipotelefone tipotelefone_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tipotelefone
    ADD CONSTRAINT tipotelefone_pkey PRIMARY KEY (id);


--
-- Name: unidadeorganizacional unidadeorganizacional_cnpj_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.unidadeorganizacional
    ADD CONSTRAINT unidadeorganizacional_cnpj_key UNIQUE (cnpj);


--
-- Name: unidadeorganizacional unidadeorganizacional_emailintegracao_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.unidadeorganizacional
    ADD CONSTRAINT unidadeorganizacional_emailintegracao_key UNIQUE (emailintegracao);


--
-- Name: unidadeorganizacional unidadeorganizacional_nomefantasia_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.unidadeorganizacional
    ADD CONSTRAINT unidadeorganizacional_nomefantasia_key UNIQUE (nomefantasia);


--
-- Name: unidadeorganizacional unidadeorganizacional_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.unidadeorganizacional
    ADD CONSTRAINT unidadeorganizacional_pkey PRIMARY KEY (id);


--
-- Name: usuario usuario_cpf_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_cpf_key UNIQUE (cpf);


--
-- Name: usuario usuario_login_key; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_login_key UNIQUE (login);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: veiculopatrimonio veiculopatrimonio_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.veiculopatrimonio
    ADD CONSTRAINT veiculopatrimonio_pkey PRIMARY KEY (id);


--
-- Name: veiculoproposta veiculoproposta_pkey; Type: CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.veiculoproposta
    ADD CONSTRAINT veiculoproposta_pkey PRIMARY KEY (id);


--
-- Name: alternativarespostasvs fk_alternativarespostasvs_id_pergunta_questionario; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.alternativarespostasvs
    ADD CONSTRAINT fk_alternativarespostasvs_id_pergunta_questionario FOREIGN KEY (id_pergunta_questionario) REFERENCES public.perguntaquestionariosvs(id);


--
-- Name: andamentooperacao fk_andamentooperacao_id_mot_aprovacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.andamentooperacao
    ADD CONSTRAINT fk_andamentooperacao_id_mot_aprovacao FOREIGN KEY (id_mot_aprovacao) REFERENCES public.motivoaprovacaosvs(id);


--
-- Name: andamentooperacao fk_andamentooperacao_id_operacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.andamentooperacao
    ADD CONSTRAINT fk_andamentooperacao_id_operacao FOREIGN KEY (id_operacao) REFERENCES public.operacaofinanciada(id);


--
-- Name: andamentooperacao fk_andamentooperacao_id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.andamentooperacao
    ADD CONSTRAINT fk_andamentooperacao_id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: ano_mod_comb_santander fk_ano_mod_comb_santander_id_modelo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ano_mod_comb_santander
    ADD CONSTRAINT fk_ano_mod_comb_santander_id_modelo FOREIGN KEY (id_modelo) REFERENCES public.modelo_santander(id);


--
-- Name: anunciosvs fk_anunciosvs_id_lead; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.anunciosvs
    ADD CONSTRAINT fk_anunciosvs_id_lead FOREIGN KEY (id_lead) REFERENCES public.leadsvs(id);


--
-- Name: aprovacao_bancaria_motivos fk_aprovacao_bancaria_motivos_aprovacao_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.aprovacao_bancaria_motivos
    ADD CONSTRAINT fk_aprovacao_bancaria_motivos_aprovacao_id FOREIGN KEY (aprovacao_id) REFERENCES public.aprovacaobancaria(id);


--
-- Name: aprovacao_bancaria_motivos fk_aprovacao_bancaria_motivos_motivo_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.aprovacao_bancaria_motivos
    ADD CONSTRAINT fk_aprovacao_bancaria_motivos_motivo_id FOREIGN KEY (motivo_id) REFERENCES public.motivoaprovacaosvs(id);


--
-- Name: aprovacaobancaria fk_aprovacaobancaria_id_calculo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.aprovacaobancaria
    ADD CONSTRAINT fk_aprovacaobancaria_id_calculo FOREIGN KEY (id_calculo) REFERENCES public.calculorentabilidade(id);


--
-- Name: aprovacaobancaria fk_aprovacaobancaria_id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.aprovacaobancaria
    ADD CONSTRAINT fk_aprovacaobancaria_id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: atividadeeconomicasantander fk_atividadeeconomicasantander_id_tipo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.atividadeeconomicasantander
    ADD CONSTRAINT fk_atividadeeconomicasantander_id_tipo FOREIGN KEY (id_tipo) REFERENCES public.tipo_ativ_ec_santander(id);


--
-- Name: atividadeleadsvs fk_atividadeleadsvs_id_lead; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.atividadeleadsvs
    ADD CONSTRAINT fk_atividadeleadsvs_id_lead FOREIGN KEY (id_lead) REFERENCES public.leadsvs(id);


--
-- Name: atividadeleadsvs fk_atividadeleadsvs_id_usuario_conclusao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.atividadeleadsvs
    ADD CONSTRAINT fk_atividadeleadsvs_id_usuario_conclusao FOREIGN KEY (id_usuario_conclusao) REFERENCES public.usuario(id);


--
-- Name: atividadeleadsvs fk_atividadeleadsvs_id_usuario_criacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.atividadeleadsvs
    ADD CONSTRAINT fk_atividadeleadsvs_id_usuario_criacao FOREIGN KEY (id_usuario_criacao) REFERENCES public.usuario(id);


--
-- Name: avalista fk_avalista_conjuge_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista
    ADD CONSTRAINT fk_avalista_conjuge_id FOREIGN KEY (conjuge_id) REFERENCES public.conjuge(id);


--
-- Name: avalista fk_avalista_endereco_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista
    ADD CONSTRAINT fk_avalista_endereco_id FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- Name: avalista fk_avalista_ocupacao_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista
    ADD CONSTRAINT fk_avalista_ocupacao_id FOREIGN KEY (ocupacao_id) REFERENCES public.ocupacao(id);


--
-- Name: avalista_svs_imoveis_pat fk_avalista_svs_imoveis_pat_avalista_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_imoveis_pat
    ADD CONSTRAINT fk_avalista_svs_imoveis_pat_avalista_id FOREIGN KEY (avalista_id) REFERENCES public.avalista(id);


--
-- Name: avalista_svs_imoveis_pat fk_avalista_svs_imoveis_pat_imovel_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_imoveis_pat
    ADD CONSTRAINT fk_avalista_svs_imoveis_pat_imovel_id FOREIGN KEY (imovel_id) REFERENCES public.imovelpatrimonio(id);


--
-- Name: avalista_svs_outrasrendas fk_avalista_svs_outrasrendas_avalista_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_outrasrendas
    ADD CONSTRAINT fk_avalista_svs_outrasrendas_avalista_id FOREIGN KEY (avalista_id) REFERENCES public.avalista(id);


--
-- Name: avalista_svs_outrasrendas fk_avalista_svs_outrasrendas_outrarenda_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_outrasrendas
    ADD CONSTRAINT fk_avalista_svs_outrasrendas_outrarenda_id FOREIGN KEY (outrarenda_id) REFERENCES public.outrarendaclientesvs(id);


--
-- Name: avalista_svs_veiculos_pat fk_avalista_svs_veiculos_pat_avalista_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_veiculos_pat
    ADD CONSTRAINT fk_avalista_svs_veiculos_pat_avalista_id FOREIGN KEY (avalista_id) REFERENCES public.avalista(id);


--
-- Name: avalista_svs_veiculos_pat fk_avalista_svs_veiculos_pat_veiculo_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.avalista_svs_veiculos_pat
    ADD CONSTRAINT fk_avalista_svs_veiculos_pat_veiculo_id FOREIGN KEY (veiculo_id) REFERENCES public.veiculopatrimonio(id);


--
-- Name: calculorentabilidade fk_calculorentabilidade_id_instituicao_financ; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.calculorentabilidade
    ADD CONSTRAINT fk_calculorentabilidade_id_instituicao_financ FOREIGN KEY (id_instituicao_financ) REFERENCES public.instituicaofinanceira(id);


--
-- Name: calculorentabilidade fk_calculorentabilidade_id_mascara; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.calculorentabilidade
    ADD CONSTRAINT fk_calculorentabilidade_id_mascara FOREIGN KEY (id_mascara) REFERENCES public.masc_taxa_tabela_fin(id);


--
-- Name: calculorentabilidade fk_calculorentabilidade_id_operacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.calculorentabilidade
    ADD CONSTRAINT fk_calculorentabilidade_id_operacao FOREIGN KEY (id_operacao) REFERENCES public.operacaofinanciada(id);


--
-- Name: calculorentabilidade fk_calculorentabilidade_id_tabelafinanceira; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.calculorentabilidade
    ADD CONSTRAINT fk_calculorentabilidade_id_tabelafinanceira FOREIGN KEY (id_tabelafinanceira) REFERENCES public.tabelafinanceira(id);


--
-- Name: campoparsesvs fk_campoparsesvs_id_configuracao_parse; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.campoparsesvs
    ADD CONSTRAINT fk_campoparsesvs_id_configuracao_parse FOREIGN KEY (id_configuracao_parse) REFERENCES public.configuracaoparsesvs(id);


--
-- Name: cidade fk_cidade_estado_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cidade
    ADD CONSTRAINT fk_cidade_estado_id FOREIGN KEY (estado_id) REFERENCES public.estado(id);


--
-- Name: cnpj_if_prod_fin_v2 fk_cnpj_if_prod_fin_v2_cnpj_inst_financeira_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cnpj_if_prod_fin_v2
    ADD CONSTRAINT fk_cnpj_if_prod_fin_v2_cnpj_inst_financeira_id FOREIGN KEY (cnpj_inst_financeira_id) REFERENCES public.cnpj_inst_financ(id);


--
-- Name: cnpj_if_prod_fin_v2 fk_cnpj_if_prod_fin_v2_produto_financeiro_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cnpj_if_prod_fin_v2
    ADD CONSTRAINT fk_cnpj_if_prod_fin_v2_produto_financeiro_id FOREIGN KEY (produto_financeiro_id) REFERENCES public.produtofinanceiro(id);


--
-- Name: cnpj_inst_financ fk_cnpj_inst_financ_instituicao_financeira_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.cnpj_inst_financ
    ADD CONSTRAINT fk_cnpj_inst_financ_instituicao_financeira_id FOREIGN KEY (instituicao_financeira_id) REFERENCES public.instituicaofinanceira(id);


--
-- Name: conf_par_emails_dom fk_conf_par_emails_dom_configuracao_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.conf_par_emails_dom
    ADD CONSTRAINT fk_conf_par_emails_dom_configuracao_id FOREIGN KEY (configuracao_id) REFERENCES public.configuracaoparsesvs(id);


--
-- Name: conf_par_emails_dom fk_conf_par_emails_dom_dominio_email_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.conf_par_emails_dom
    ADD CONSTRAINT fk_conf_par_emails_dom_dominio_email_id FOREIGN KEY (dominio_email_id) REFERENCES public.dominioemailsvs(id);


--
-- Name: configuracaoparsesvs fk_configuracaoparsesvs_id_fonte_lead; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.configuracaoparsesvs
    ADD CONSTRAINT fk_configuracaoparsesvs_id_fonte_lead FOREIGN KEY (id_fonte_lead) REFERENCES public.fonteleadsvs(id);


--
-- Name: conjuge fk_conjuge_ocupacao_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.conjuge
    ADD CONSTRAINT fk_conjuge_ocupacao_id FOREIGN KEY (ocupacao_id) REFERENCES public.ocupacao(id);


--
-- Name: endereco fk_endereco_id_municipio; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT fk_endereco_id_municipio FOREIGN KEY (id_municipio) REFERENCES public.municipio(id);


--
-- Name: endereco fk_endereco_id_tipo_endereco; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT fk_endereco_id_tipo_endereco FOREIGN KEY (id_tipo_endereco) REFERENCES public.tipoendereco(id);


--
-- Name: financiamentoonlineitau fk_financiamentoonlineitau_id_json_credit_an_itau; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlineitau
    ADD CONSTRAINT fk_financiamentoonlineitau_id_json_credit_an_itau FOREIGN KEY (id_json_credit_an_itau) REFERENCES public.json_credit_an_itau(id);


--
-- Name: financiamentoonlineitau fk_financiamentoonlineitau_id_json_sim_resp_itau; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlineitau
    ADD CONSTRAINT fk_financiamentoonlineitau_id_json_sim_resp_itau FOREIGN KEY (id_json_sim_resp_itau) REFERENCES public.json_sim_resp_itau(id);


--
-- Name: financiamentoonlineitau fk_financiamentoonlineitau_id_json_transac_req_itau; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlineitau
    ADD CONSTRAINT fk_financiamentoonlineitau_id_json_transac_req_itau FOREIGN KEY (id_json_transac_req_itau) REFERENCES public.json_transac_req_itau(id);


--
-- Name: financiamentoonlineitau fk_financiamentoonlineitau_id_operacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlineitau
    ADD CONSTRAINT fk_financiamentoonlineitau_id_operacao FOREIGN KEY (id_operacao) REFERENCES public.operacaofinanciada(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_calculo_rentabilidade; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_calculo_rentabilidade FOREIGN KEY (id_calculo_rentabilidade) REFERENCES public.calculorentabilidade(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_json_req_id_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_json_req_id_santander FOREIGN KEY (id_json_req_id_santander) REFERENCES public.json_ident_santander(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_json_req_prop_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_json_req_prop_santander FOREIGN KEY (id_json_req_prop_santander) REFERENCES public.jsonrequisicaoproposta(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_json_resp_prean_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_json_resp_prean_santander FOREIGN KEY (id_json_resp_prean_santander) REFERENCES public.json_preanalise_santander(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_json_resp_prop_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_json_resp_prop_santander FOREIGN KEY (id_json_resp_prop_santander) REFERENCES public.jsonrespostaproposta(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_json_resp_sim_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_json_resp_sim_santander FOREIGN KEY (id_json_resp_sim_santander) REFERENCES public.jsonrespostasimulacaosantander(id);


--
-- Name: financiamentoonlinesantander fk_financiamentoonlinesantander_id_json_simulacao_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.financiamentoonlinesantander
    ADD CONSTRAINT fk_financiamentoonlinesantander_id_json_simulacao_santander FOREIGN KEY (id_json_simulacao_santander) REFERENCES public.json_req_sim_santander(id);


--
-- Name: historicoalteracao fk_historicoalteracao_id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.historicoalteracao
    ADD CONSTRAINT fk_historicoalteracao_id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: historicoleadsvs fk_historicoleadsvs_id_lead; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.historicoleadsvs
    ADD CONSTRAINT fk_historicoleadsvs_id_lead FOREIGN KEY (id_lead) REFERENCES public.leadsvs(id);


--
-- Name: historicoleadsvs fk_historicoleadsvs_id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.historicoleadsvs
    ADD CONSTRAINT fk_historicoleadsvs_id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: icarrostoken fk_icarrostoken_icarrosusuario_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.icarrostoken
    ADD CONSTRAINT fk_icarrostoken_icarrosusuario_id FOREIGN KEY (icarrosusuario_id) REFERENCES public.icarrosusuario(id);


--
-- Name: imovelpatrimonio fk_imovelpatrimonio_endereco_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.imovelpatrimonio
    ADD CONSTRAINT fk_imovelpatrimonio_endereco_id FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- Name: instituicaofinanceira fk_instituicaofinanceira_id_usuario_criacao_alteracao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.instituicaofinanceira
    ADD CONSTRAINT fk_instituicaofinanceira_id_usuario_criacao_alteracao FOREIGN KEY (id_usuario_criacao_alteracao) REFERENCES public.usuario(id);


--
-- Name: itemcbc fk_itemcbc_id_opcao_item_cbc; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.itemcbc
    ADD CONSTRAINT fk_itemcbc_id_opcao_item_cbc FOREIGN KEY (id_opcao_item_cbc) REFERENCES public.opcaoitemcbc(id);


--
-- Name: itemcbc fk_itemcbc_id_operacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.itemcbc
    ADD CONSTRAINT fk_itemcbc_id_operacao FOREIGN KEY (id_operacao) REFERENCES public.operacaofinanciada(id);


--
-- Name: json_ident_santander fk_json_ident_santander_id_ano_modelo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_ident_santander
    ADD CONSTRAINT fk_json_ident_santander_id_ano_modelo FOREIGN KEY (id_ano_modelo) REFERENCES public.ano_mod_comb_santander(id);


--
-- Name: json_ident_santander fk_json_ident_santander_id_modelo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.json_ident_santander
    ADD CONSTRAINT fk_json_ident_santander_id_modelo FOREIGN KEY (id_modelo) REFERENCES public.modelo_santander(id);


--
-- Name: leadsvs fk_leadsvs_id_fonte_lead; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT fk_leadsvs_id_fonte_lead FOREIGN KEY (id_fonte_lead) REFERENCES public.fonteleadsvs(id);


--
-- Name: leadsvs fk_leadsvs_id_leadenriquecido; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT fk_leadsvs_id_leadenriquecido FOREIGN KEY (id_leadenriquecido) REFERENCES public.leadsvsenriquecido(id);


--
-- Name: leadsvs fk_leadsvs_id_motivo_descarte; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT fk_leadsvs_id_motivo_descarte FOREIGN KEY (id_motivo_descarte) REFERENCES public.motivodescartesvs(id);


--
-- Name: leadsvs fk_leadsvs_id_produto_interesse; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT fk_leadsvs_id_produto_interesse FOREIGN KEY (id_produto_interesse) REFERENCES public.produtointeresse(id);


--
-- Name: leadsvs fk_leadsvs_id_responsavel; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT fk_leadsvs_id_responsavel FOREIGN KEY (id_responsavel) REFERENCES public.usuario(id);


--
-- Name: leadsvs fk_leadsvs_id_unidade_organizacional; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.leadsvs
    ADD CONSTRAINT fk_leadsvs_id_unidade_organizacional FOREIGN KEY (id_unidade_organizacional) REFERENCES public.unidadeorganizacional(id);


--
-- Name: masc_taxa_tabela_fin fk_masc_taxa_tabela_fin_id_usuario_criacao_alteracao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.masc_taxa_tabela_fin
    ADD CONSTRAINT fk_masc_taxa_tabela_fin_id_usuario_criacao_alteracao FOREIGN KEY (id_usuario_criacao_alteracao) REFERENCES public.usuario(id);


--
-- Name: mod_veic_sant_unidades fk_mod_veic_sant_unidades_modelo_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.mod_veic_sant_unidades
    ADD CONSTRAINT fk_mod_veic_sant_unidades_modelo_id FOREIGN KEY (modelo_id) REFERENCES public.modelo_santander(id);


--
-- Name: mod_veic_sant_unidades fk_mod_veic_sant_unidades_unidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.mod_veic_sant_unidades
    ADD CONSTRAINT fk_mod_veic_sant_unidades_unidade_id FOREIGN KEY (unidade_id) REFERENCES public.unidadeorganizacional(id);


--
-- Name: modelo_quest_departamentos fk_modelo_quest_departamentos_id_modelo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modelo_quest_departamentos
    ADD CONSTRAINT fk_modelo_quest_departamentos_id_modelo FOREIGN KEY (modelo_questionario_id) REFERENCES public.modeloquestionariosvs(id);


--
-- Name: modelo_quest_unidades fk_modelo_quest_unidades_modelo_questionario_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modelo_quest_unidades
    ADD CONSTRAINT fk_modelo_quest_unidades_modelo_questionario_id FOREIGN KEY (modelo_questionario_id) REFERENCES public.modeloquestionariosvs(id);


--
-- Name: modelo_quest_unidades fk_modelo_quest_unidades_unidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modelo_quest_unidades
    ADD CONSTRAINT fk_modelo_quest_unidades_unidade_id FOREIGN KEY (unidade_id) REFERENCES public.unidadeorganizacional(id);


--
-- Name: modelo_santander fk_modelo_santander_id_marca; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.modelo_santander
    ADD CONSTRAINT fk_modelo_santander_id_marca FOREIGN KEY (id_marca) REFERENCES public.marcasantander(id);


--
-- Name: motivoaprovacaosvs fk_motivoaprovacaosvs_id_usuario_criacao_alteracao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.motivoaprovacaosvs
    ADD CONSTRAINT fk_motivoaprovacaosvs_id_usuario_criacao_alteracao FOREIGN KEY (id_usuario_criacao_alteracao) REFERENCES public.usuario(id);


--
-- Name: motivodescartesvs fk_motivodescartesvs_id_usuario_criacao_alteracao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.motivodescartesvs
    ADD CONSTRAINT fk_motivodescartesvs_id_usuario_criacao_alteracao FOREIGN KEY (id_usuario_criacao_alteracao) REFERENCES public.usuario(id);


--
-- Name: municipio_santander fk_municipio_santander_id_estado; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.municipio_santander
    ADD CONSTRAINT fk_municipio_santander_id_estado FOREIGN KEY (id_estado) REFERENCES public.estado_santander(id);


--
-- Name: notaleadsvs fk_notaleadsvs_id_lead; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.notaleadsvs
    ADD CONSTRAINT fk_notaleadsvs_id_lead FOREIGN KEY (id_lead) REFERENCES public.leadsvs(id);


--
-- Name: ocupacao fk_ocupacao_atividade_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ocupacao
    ADD CONSTRAINT fk_ocupacao_atividade_id FOREIGN KEY (atividade_id) REFERENCES public.atividadeeconomicasantander(id);


--
-- Name: ocupacao fk_ocupacao_endereco_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ocupacao
    ADD CONSTRAINT fk_ocupacao_endereco_id FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- Name: ocupacao fk_ocupacao_profissao_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ocupacao
    ADD CONSTRAINT fk_ocupacao_profissao_id FOREIGN KEY (profissao_id) REFERENCES public.profissaosantander(id);


--
-- Name: ocupacao fk_ocupacao_tipo_atividade_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.ocupacao
    ADD CONSTRAINT fk_ocupacao_tipo_atividade_id FOREIGN KEY (tipo_atividade_id) REFERENCES public.tipo_ativ_ec_santander(id);


--
-- Name: op_finan_online_santander fk_op_finan_online_santander_financiamento_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.op_finan_online_santander
    ADD CONSTRAINT fk_op_finan_online_santander_financiamento_id FOREIGN KEY (financiamento_id) REFERENCES public.financiamentoonlinesantander(id);


--
-- Name: op_finan_online_santander fk_op_finan_online_santander_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.op_finan_online_santander
    ADD CONSTRAINT fk_op_finan_online_santander_id FOREIGN KEY (id) REFERENCES public.operacaofinanciada(id);


--
-- Name: opcaoitemcbc fk_opcaoitemcbc_id_unidade_organizacional; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.opcaoitemcbc
    ADD CONSTRAINT fk_opcaoitemcbc_id_unidade_organizacional FOREIGN KEY (id_unidade_organizacional) REFERENCES public.unidadeorganizacional(id);


--
-- Name: opcionaisveiculoproposta fk_opcionaisveiculoproposta_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.opcionaisveiculoproposta
    ADD CONSTRAINT fk_opcionaisveiculoproposta_id FOREIGN KEY (id) REFERENCES public.veiculoproposta(id);


--
-- Name: opcionaisveiculoproposta fk_opcionaisveiculoproposta_opcional_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.opcionaisveiculoproposta
    ADD CONSTRAINT fk_opcionaisveiculoproposta_opcional_id FOREIGN KEY (opcional_id) REFERENCES public.opcionalveiculo(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_ano_modelo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_ano_modelo FOREIGN KEY (id_ano_modelo) REFERENCES public.ano_mod_comb_santander(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_calculo_faturamento; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_calculo_faturamento FOREIGN KEY (id_calculo_faturamento) REFERENCES public.calculorentabilidade(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_calculo_tabela_instit; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_calculo_tabela_instit FOREIGN KEY (id_calculo_tabela_instit) REFERENCES public.calculorentabilidade(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_instituicao_financ_fat; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_instituicao_financ_fat FOREIGN KEY (id_instituicao_financ_fat) REFERENCES public.instituicaofinanceira(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_marca_santander; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_marca_santander FOREIGN KEY (id_marca_santander) REFERENCES public.marcasantander(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_modelo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_modelo FOREIGN KEY (id_modelo) REFERENCES public.modelo_santander(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_proposta; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_proposta FOREIGN KEY (id_proposta) REFERENCES public.proposta(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_unidade_organizacional; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_unidade_organizacional FOREIGN KEY (id_unidade_organizacional) REFERENCES public.unidadeorganizacional(id);


--
-- Name: operacaofinanciada fk_operacaofinanciada_id_vendedor; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciada
    ADD CONSTRAINT fk_operacaofinanciada_id_vendedor FOREIGN KEY (id_vendedor) REFERENCES public.usuario(id);


--
-- Name: operacaofinanciadacalcenv fk_operacaofinanciadacalcenv_calculo_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciadacalcenv
    ADD CONSTRAINT fk_operacaofinanciadacalcenv_calculo_id FOREIGN KEY (calculo_id) REFERENCES public.calculorentabilidade(id);


--
-- Name: operacaofinanciadacalcenv fk_operacaofinanciadacalcenv_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciadacalcenv
    ADD CONSTRAINT fk_operacaofinanciadacalcenv_id FOREIGN KEY (id) REFERENCES public.operacaofinanciada(id);


--
-- Name: operacaofinanciadatodasrent fk_operacaofinanciadatodasrent_calculo_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciadatodasrent
    ADD CONSTRAINT fk_operacaofinanciadatodasrent_calculo_id FOREIGN KEY (calculo_id) REFERENCES public.calculorentabilidade(id);


--
-- Name: operacaofinanciadatodasrent fk_operacaofinanciadatodasrent_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.operacaofinanciadatodasrent
    ADD CONSTRAINT fk_operacaofinanciadatodasrent_id FOREIGN KEY (id) REFERENCES public.operacaofinanciada(id);


--
-- Name: parcelaentrada fk_parcelaentrada_id_operacao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.parcelaentrada
    ADD CONSTRAINT fk_parcelaentrada_id_operacao FOREIGN KEY (id_operacao) REFERENCES public.operacaofinanciada(id);


--
-- Name: parcelaentrada fk_parcelaentrada_id_parcela_abater; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.parcelaentrada
    ADD CONSTRAINT fk_parcelaentrada_id_parcela_abater FOREIGN KEY (id_parcela_abater) REFERENCES public.parcelaentrada(id);


--
-- Name: parcelaentrada fk_parcelaentrada_id_tipo_pagamento; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.parcelaentrada
    ADD CONSTRAINT fk_parcelaentrada_id_tipo_pagamento FOREIGN KEY (id_tipo_pagamento) REFERENCES public.tipopagamento(id);


--
-- Name: perguntaquestionariosvs fk_perguntaquestionariosvs_id_modelo_questionario; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.perguntaquestionariosvs
    ADD CONSTRAINT fk_perguntaquestionariosvs_id_modelo_questionario FOREIGN KEY (id_modelo_questionario) REFERENCES public.modeloquestionariosvs(id);


--
-- Name: produtofinanceiro fk_produtofinanceiro_id_usuario_criacao_alteracao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.produtofinanceiro
    ADD CONSTRAINT fk_produtofinanceiro_id_usuario_criacao_alteracao FOREIGN KEY (id_usuario_criacao_alteracao) REFERENCES public.usuario(id);


--
-- Name: produtointeresse fk_produtointeresse_id_usuario_criacao_alteracao; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.produtointeresse
    ADD CONSTRAINT fk_produtointeresse_id_usuario_criacao_alteracao FOREIGN KEY (id_usuario_criacao_alteracao) REFERENCES public.usuario(id);


--
-- Name: proposta fk_proposta_id_cliente_svs; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.proposta
    ADD CONSTRAINT fk_proposta_id_cliente_svs FOREIGN KEY (id_cliente_svs) REFERENCES public.cliente(id);


--
-- Name: proposta fk_proposta_id_unidade_organizacional; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.proposta
    ADD CONSTRAINT fk_proposta_id_unidade_organizacional FOREIGN KEY (id_unidade_organizacional) REFERENCES public.unidadeorganizacional(id);


--
-- Name: proposta fk_proposta_id_veiculo; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.proposta
    ADD CONSTRAINT fk_proposta_id_veiculo FOREIGN KEY (id_veiculo) REFERENCES public.veiculoproposta(id);


--
-- Name: referenciabancaria fk_referenciabancaria_id_banco; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.referenciabancaria
    ADD CONSTRAINT fk_referenciabancaria_id_banco FOREIGN KEY (id_banco) REFERENCES public.banco(id);


--
-- Name: referenciabancaria fk_referenciabancaria_id_cidade; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.referenciabancaria
    ADD CONSTRAINT fk_referenciabancaria_id_cidade FOREIGN KEY (id_cidade) REFERENCES public.cidade(id);


--
-- Name: referenciabancaria fk_referenciabancaria_id_estado; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.referenciabancaria
    ADD CONSTRAINT fk_referenciabancaria_id_estado FOREIGN KEY (id_estado) REFERENCES public.estado(id);


--
-- Name: regra_spam_departamentos fk_regra_spam_departamentos_regraspamsvs_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_departamentos
    ADD CONSTRAINT fk_regra_spam_departamentos_regraspamsvs_id FOREIGN KEY (regraspamsvs_id) REFERENCES public.regraspamsvs(id);


--
-- Name: regra_spam_fontes_lead fk_regra_spam_fontes_lead_fonte_lead_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_fontes_lead
    ADD CONSTRAINT fk_regra_spam_fontes_lead_fonte_lead_id FOREIGN KEY (fonte_lead_id) REFERENCES public.fonteleadsvs(id);


--
-- Name: regra_spam_fontes_lead fk_regra_spam_fontes_lead_regra_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_fontes_lead
    ADD CONSTRAINT fk_regra_spam_fontes_lead_regra_id FOREIGN KEY (regra_id) REFERENCES public.regraspamsvs(id);


--
-- Name: regra_spam_tipos_regra fk_regra_spam_tipos_regra_regraspamsvs_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_tipos_regra
    ADD CONSTRAINT fk_regra_spam_tipos_regra_regraspamsvs_id FOREIGN KEY (regraspamsvs_id) REFERENCES public.regraspamsvs(id);


--
-- Name: regra_spam_unidades fk_regra_spam_unidades_regra_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_unidades
    ADD CONSTRAINT fk_regra_spam_unidades_regra_id FOREIGN KEY (regra_id) REFERENCES public.regraspamsvs(id);


--
-- Name: regra_spam_unidades fk_regra_spam_unidades_unidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regra_spam_unidades
    ADD CONSTRAINT fk_regra_spam_unidades_unidade_id FOREIGN KEY (unidade_id) REFERENCES public.unidadeorganizacional(id);


--
-- Name: regraspamsvs fk_regraspamsvs_id_motivo_descarte; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.regraspamsvs
    ADD CONSTRAINT fk_regraspamsvs_id_motivo_descarte FOREIGN KEY (id_motivo_descarte) REFERENCES public.motivodescartesvs(id);


--
-- Name: tabela_financ_unidades_v2 fk_tabela_financ_unidades_v2_tabela_financeira_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tabela_financ_unidades_v2
    ADD CONSTRAINT fk_tabela_financ_unidades_v2_tabela_financeira_id FOREIGN KEY (tabela_financeira_id) REFERENCES public.tabelafinanceira(id);


--
-- Name: tabela_financ_unidades_v2 fk_tabela_financ_unidades_v2_unidade_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tabela_financ_unidades_v2
    ADD CONSTRAINT fk_tabela_financ_unidades_v2_unidade_id FOREIGN KEY (unidade_id) REFERENCES public.unidadeorganizacional(id);


--
-- Name: tabelafinanceira fk_tabelafinanceira_id_instituicao_financeira; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tabelafinanceira
    ADD CONSTRAINT fk_tabelafinanceira_id_instituicao_financeira FOREIGN KEY (id_instituicao_financeira) REFERENCES public.instituicaofinanceira(id);


--
-- Name: tabelafinanceira fk_tabelafinanceira_id_produto_financeiro; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.tabelafinanceira
    ADD CONSTRAINT fk_tabelafinanceira_id_produto_financeiro FOREIGN KEY (id_produto_financeiro) REFERENCES public.produtofinanceiro(id);


--
-- Name: taxa_perfil_if fk_taxa_perfil_if_instituicao_financeira_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxa_perfil_if
    ADD CONSTRAINT fk_taxa_perfil_if_instituicao_financeira_id FOREIGN KEY (instituicao_financeira_id) REFERENCES public.instituicaofinanceira(id);


--
-- Name: taxa_perfil_if fk_taxa_perfil_if_perfil_svs_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxa_perfil_if
    ADD CONSTRAINT fk_taxa_perfil_if_perfil_svs_id FOREIGN KEY (perfil_svs_id) REFERENCES public.perfilsvs(id);


--
-- Name: taxa_tabela_financeira fk_taxa_tabela_financeira_id_mascara; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxa_tabela_financeira
    ADD CONSTRAINT fk_taxa_tabela_financeira_id_mascara FOREIGN KEY (id_mascara) REFERENCES public.masc_taxa_tabela_fin(id);


--
-- Name: taxa_tabela_financeira fk_taxa_tabela_financeira_tabela_financeira_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxa_tabela_financeira
    ADD CONSTRAINT fk_taxa_tabela_financeira_tabela_financeira_id FOREIGN KEY (tabela_financeira_id) REFERENCES public.tabelafinanceira(id);


--
-- Name: taxaperfiltabelafinanceira fk_taxaperfiltabelafinanceira_perfil_svs_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxaperfiltabelafinanceira
    ADD CONSTRAINT fk_taxaperfiltabelafinanceira_perfil_svs_id FOREIGN KEY (perfil_svs_id) REFERENCES public.perfilsvs(id);


--
-- Name: taxaperfiltabelafinanceira fk_taxaperfiltabelafinanceira_tabela_financeira_id; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.taxaperfiltabelafinanceira
    ADD CONSTRAINT fk_taxaperfiltabelafinanceira_tabela_financeira_id FOREIGN KEY (tabela_financeira_id) REFERENCES public.tabelafinanceira(id);


--
-- Name: telefone fk_telefone_id_tipo_endereco; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.telefone
    ADD CONSTRAINT fk_telefone_id_tipo_endereco FOREIGN KEY (id_tipo_endereco) REFERENCES public.tipoendereco(id);


--
-- Name: telefone fk_telefone_id_tipo_telefone; Type: FK CONSTRAINT; Schema: public; Owner: dbroot
--

ALTER TABLE ONLY public.telefone
    ADD CONSTRAINT fk_telefone_id_tipo_telefone FOREIGN KEY (id_tipo_telefone) REFERENCES public.tipotelefone(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: dbroot
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO dbroot;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

