package br.com.gruposaga.cad.svs.fin.ws.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.entities.ClienteSvs;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author italo.miranda
 *
 */
@Api(value = "Clientes SVS-FIN")
@Path(WebServicesSvsFin.CLIENTES)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WsClienteSvs {

	// @POST
	// @Path("listarFiltro")
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces(MediaType.APPLICATION_JSON)
	// @ApiOperation(value = "Consulta os registros da tabela Cliente")
	// @ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION,
	// required = true, dataType = "String", value = "Token", paramType = "header"))
	// List<ClienteSvs> listarFiltro(@NotNull @Valid ListParamsDto listParamsDto)
	// throws Erro400;
	//
	// @POST
	// @Path(value = "contarRegistros")
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces(MediaType.APPLICATION_JSON)
	// @ApiOperation("Contar os clientes de forma filtrada")
	// @ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION,
	// required = true, dataType = "String", value = "Token", paramType = "header"))
	// long contarRegistros(@Valid @NotNull ListParamsDto listParamsDto) throws
	// Erro400;

	@GET
	@ApiOperation(value = "listar")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("cpfCnpj") String cpfCnpj,
			@QueryParam("flagLoadLists") Boolean flagLoadLists);

	@GET
	@Path("/{id}")
	@ApiOperation(value = "obter por id")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response obterPorId(@PathParam("id") Integer id);

	@GET
	@Path("/cpfCnpj/{cpfCnpj}")
	@ApiOperation(value = "obter por cpf cnpj")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response obterPorCpfCnpj(@PathParam("cpfCnpj") String cpfCnpj);

	@PUT
	@ApiOperation(value = "alterar")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response alterar(ClienteSvs clienteSvs);

	@POST
	@ApiOperation(value = "incluir")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response incluir(ClienteSvs clienteSvs);

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "remover")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response remover(@PathParam("id") Long id);

}
