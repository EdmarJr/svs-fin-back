package br.com.gruposaga.cad.svs.fin.ws.api;


import br.com.gruposaga.cad.svs.fin.modelo.pojo.EntidadeTesteConsultaPojo;
import br.com.gruposaga.cad.ws.Erro400;
import com.svs.fin.model.dto.EntidadeTesteDto;
import com.svs.fin.model.dto.EntidadeTesteEdicaoDto;
import com.svs.fin.model.dto.ListParamsDto;
import io.swagger.annotations.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api(value="Cadastro de entidade teste")
@Path(WebServicesSvsFin.ENTIDADE_TESTE)
public interface WsEntidadeTeste {

	@PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Atualiza um entidade teste existente")
	@ApiImplicitParams(
			@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"
	))
    void put(
        @Min(1)
        @PathParam("id")
        @ApiParam("A id do entidade teste")
            Long idEntidadeTeste,
        @Valid
        @NotNull
            EntidadeTesteEdicaoDto produtoInteresseDto
    ) throws Erro400;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Cria uma novo entidade teste")
    @ApiImplicitParams(
			@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"
	))
    Long post(
        @Valid
        @NotNull
            EntidadeTesteEdicaoDto produtoInteresseDto
    ) throws Erro400;

    @POST
    @Path(value="listarFiltro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Listar entidade teste de forma paginada e filtrada")
    @ApiImplicitParams(
        @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"
        ))
    List<EntidadeTesteConsultaPojo> listarFiltro(
        @Valid
        @NotNull
            ListParamsDto pageFilterDto) throws Erro400;

    @POST
    @Path(value="contarRegistros")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Contar entidade teste de forma filtrada")
    @ApiImplicitParams(
        @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"
        ))
    long contarRegistros(
        @Valid
        @NotNull
            ListParamsDto listCountParamsDto) throws Erro400;

    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Consultar entidade teste")
    @ApiImplicitParams(
        @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"
        ))
    EntidadeTesteDto get(
        @Min(1)
        @PathParam("id")
        @ApiParam("A id do entidade teste")
            Long idEntidadeTeste);
}
