/**
 * RetornoCalculoParcelas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class RetornoCalculoParcelas  extends com.svs.fin.integracao.safra.model.RetornoProposta  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model.CalculoParcelas retCalcParc;

    private java.lang.String id_simulacao;

    public RetornoCalculoParcelas() {
    }

    public RetornoCalculoParcelas(
           java.lang.String id_proposta,
           java.lang.String id_retorno,
           com.svs.fin.integracao.safra.model.CalculoParcelas retCalcParc,
           java.lang.String id_simulacao) {
        super(
            id_proposta,
            id_retorno);
        this.retCalcParc = retCalcParc;
        this.id_simulacao = id_simulacao;
    }


    /**
     * Gets the retCalcParc value for this RetornoCalculoParcelas.
     * 
     * @return retCalcParc
     */
    public com.svs.fin.integracao.safra.model.CalculoParcelas getRetCalcParc() {
        return retCalcParc;
    }


    /**
     * Sets the retCalcParc value for this RetornoCalculoParcelas.
     * 
     * @param retCalcParc
     */
    public void setRetCalcParc(com.svs.fin.integracao.safra.model.CalculoParcelas retCalcParc) {
        this.retCalcParc = retCalcParc;
    }


    /**
     * Gets the id_simulacao value for this RetornoCalculoParcelas.
     * 
     * @return id_simulacao
     */
    public java.lang.String getId_simulacao() {
        return id_simulacao;
    }


    /**
     * Sets the id_simulacao value for this RetornoCalculoParcelas.
     * 
     * @param id_simulacao
     */
    public void setId_simulacao(java.lang.String id_simulacao) {
        this.id_simulacao = id_simulacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetornoCalculoParcelas)) return false;
        RetornoCalculoParcelas other = (RetornoCalculoParcelas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.retCalcParc==null && other.getRetCalcParc()==null) || 
             (this.retCalcParc!=null &&
              this.retCalcParc.equals(other.getRetCalcParc()))) &&
            ((this.id_simulacao==null && other.getId_simulacao()==null) || 
             (this.id_simulacao!=null &&
              this.id_simulacao.equals(other.getId_simulacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRetCalcParc() != null) {
            _hashCode += getRetCalcParc().hashCode();
        }
        if (getId_simulacao() != null) {
            _hashCode += getId_simulacao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetornoCalculoParcelas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoCalculoParcelas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCalcParc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetCalcParc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "CalculoParcelas"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_simulacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_simulacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
