package com.svs.fin.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.svs.fin.enums.EnumDepartamentoSvs;
import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumSexo;
import com.svs.fin.enums.EnumTipoOcupacao;
import com.svs.fin.enums.EnumTipoResidenciaSVS;
import com.svs.fin.model.entities.Avalista;
import com.svs.fin.model.entities.ClienteSvs;
import com.svs.fin.model.entities.Conjuge;
import com.svs.fin.model.entities.DeParaBancoZFlow;
import com.svs.fin.model.entities.Endereco;
import com.svs.fin.model.entities.Ocupacao;
import com.svs.fin.model.entities.OperacaoFinanciada;
import com.svs.fin.model.entities.Proposta;
import com.svs.fin.model.entities.ReferenciaBancaria;
import com.svs.fin.model.entities.ReferenciaPessoal;
import com.svs.fin.model.entities.Telefone;
import com.svs.fin.model.entities.VeiculoProposta;

import br.com.gruposaga.sdk.zflow.model.gen.Address;
import br.com.gruposaga.sdk.zflow.model.gen.BankReference;
import br.com.gruposaga.sdk.zflow.model.gen.Client;
import br.com.gruposaga.sdk.zflow.model.gen.ClientAssets;
import br.com.gruposaga.sdk.zflow.model.gen.ClientEstate;
import br.com.gruposaga.sdk.zflow.model.gen.ClientReferences;
import br.com.gruposaga.sdk.zflow.model.gen.ClientVehicle;
import br.com.gruposaga.sdk.zflow.model.gen.FinanceOperationRequest;
import br.com.gruposaga.sdk.zflow.model.gen.IdentityDocument;
import br.com.gruposaga.sdk.zflow.model.gen.MaritalPartner;
import br.com.gruposaga.sdk.zflow.model.gen.PersonalReference;
import br.com.gruposaga.sdk.zflow.model.gen.Phone;
import br.com.gruposaga.sdk.zflow.model.gen.ProfessionalData;
import br.com.gruposaga.sdk.zflow.model.gen.enums.DocumentIssuerEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.DocumentTypeEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.GenderEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.GraduationEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.MaritalStatusEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.OccupationEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.PhoneTypeEnum;
import br.com.gruposaga.sdk.zflow.model.gen.enums.ResidenceTypeEnum;

public class IntegracaoZFlowUtil {
	
	public static Address convertAddress(Endereco endereco) {
		
		if(endereco == null || endereco.getMunicipio() == null) {
			return null;
		}
		
		Address adress = new Address();
		adress.setAddress(endereco.getLogradouro());
		adress.setCity(endereco.getMunicipio().getNome());
		adress.setState(endereco.getMunicipio().getEstado().getSigla());
		adress.setComplement(endereco.getComplemento());
		adress.setCountry("Brasil");
		adress.setNeighborhood(endereco.getBairro());
		adress.setNumber(endereco.getNumero());
		adress.setZipCode(endereco.getCep().replaceAll("\\D+",""));
		
		return adress;
	}
	
	public static ResidenceTypeEnum convertResidenceType(Endereco endereco) {
		
		if(endereco == null)
			return null;
		
		EnumTipoResidenciaSVS tipoResidencia = endereco.getTipoResidencia();
		
		if(tipoResidencia != null) {
			
			if(tipoResidencia.equals(EnumTipoResidenciaSVS.ALUGADA)) {
				return ResidenceTypeEnum.ALUGADA;
			}else if(tipoResidencia.equals(EnumTipoResidenciaSVS.FINANCIADA)) {
				return ResidenceTypeEnum.FINANCIADA;
			}else if(tipoResidencia.equals(EnumTipoResidenciaSVS.PARENTES)) {
				return ResidenceTypeEnum.PAISPARENTES;
			}else if(tipoResidencia.equals(EnumTipoResidenciaSVS.CEDIDA)) {
				return ResidenceTypeEnum.OUTROS;
			}else if(tipoResidencia.equals(EnumTipoResidenciaSVS.FUNCIONAL)) {
				return ResidenceTypeEnum.OUTROS;
			}else if(tipoResidencia.equals(EnumTipoResidenciaSVS.PROPRIA)) {
				return  ResidenceTypeEnum.QUITADA;
			}
		}
		
		return null;
	}
	
	public static GraduationEnum convertGraduation(EnumEscolaridade escolaridade) {
	
		if(escolaridade.equals(EnumEscolaridade.PRIMEIRO_GRAU_COMPLETO)
				|| escolaridade.equals(EnumEscolaridade.PRIMEIRO_GRAU_INCOMPLETO)
				|| escolaridade.equals(EnumEscolaridade.PRIMARIO_COMPLETO)
				|| escolaridade.equals(EnumEscolaridade.PRIMARIO_INCOMPLETO)
				|| escolaridade.equals(EnumEscolaridade.SEGUNDO_GRAU_INCOMPLETO)
				|| escolaridade.equals(EnumEscolaridade.SEGUNDO_GRAU_TECNICO_INCOMPLETO)) {
	
			return GraduationEnum.PRIMEIROGRAU;
			
		}else if(escolaridade.equals(EnumEscolaridade.SEGUNDO_GRAU_COMPLETO)
				|| escolaridade.equals(EnumEscolaridade.SEGUNDO_GRAU_TECNICO_COMPLETO)
				|| escolaridade.equals(EnumEscolaridade.SUPERIOR_INCOMPLETO)){
			
			return GraduationEnum.SEGUNDOGRAU;
			
		}else if(escolaridade.equals(EnumEscolaridade.SUPERIOR_COMPLETO)){
			
			return GraduationEnum.SUPERIOR;
			
		}else if(escolaridade.equals(EnumEscolaridade.POS_GRADUACAO)
				|| escolaridade.equals(EnumEscolaridade.MESTRADO)
				|| escolaridade.equals(EnumEscolaridade.DOUTORADO)){
			
			return GraduationEnum.POSGRADUACAO;
		}
		
		return null;
	}
	
	public static OccupationEnum convertOcupation(EnumTipoOcupacao tipoOcupacao) {
		
		if(tipoOcupacao.equals(EnumTipoOcupacao.ASSALARIADO)) {
			return OccupationEnum.SALARIED;
		}else if(tipoOcupacao.equals(EnumTipoOcupacao.APOSENTADO_PENSIONISTA)) {
			return OccupationEnum.RETIRED_OR_PENSIONER;
		}else if(tipoOcupacao.equals(EnumTipoOcupacao.AUTONOMO)) {
			return OccupationEnum.SELF_EMPLOYED;
		}else if(tipoOcupacao.equals(EnumTipoOcupacao.PROFISSIONAL_LIBERAL)) {
			return OccupationEnum.PROFESSIONAL_PERSON;
		}else if(tipoOcupacao.equals(EnumTipoOcupacao.SOCIO_PROPIETARIO)) {
			return OccupationEnum.SOCIAL_OWNER;
		}
		return null;
	}
	
	public static MaritalStatusEnum convertMaritalStatus(EnumEstadoCivil estadoCivil) {
		if(estadoCivil.equals(EnumEstadoCivil.CASADO)){
			return MaritalStatusEnum.MARRIED;
		}else if(estadoCivil.equals(EnumEstadoCivil.DIVORCIADO)){
			return MaritalStatusEnum.DIVORCED;
		}else if(estadoCivil.equals(EnumEstadoCivil.SOLTEIRO)){
			return MaritalStatusEnum.SINGLE;
		}else if(estadoCivil.equals(EnumEstadoCivil.COMPANHEIRO)){
			return MaritalStatusEnum.LIVING_COMMON_LAW;
		}
		return null;
	}
	
	public static GenderEnum converterSexoSvsParaZFlow(EnumSexo sexo) {
		if(sexo.equals(EnumSexo.MASCULINO)) {
			return GenderEnum.MALE;
		}else if(sexo.equals(EnumSexo.FEMININO)) {
			return GenderEnum.FEMALE;
		}
		return null;
	}
	
	public static ProfessionalData convertProfessionalData(Ocupacao ocupacao) {
		
		ProfessionalData professionalData = new ProfessionalData();

		professionalData.setCompanyName(ocupacao.getNomeEmpregador());
		professionalData.setProfession(ocupacao.getProfissao().getProfessionEquivalenteZFlow());
		professionalData.setIncome(new Integer(ocupacao.getRenda().intValue()));
		professionalData.setOccupation(convertOcupation(ocupacao.getTipoOcupacao()));
		professionalData.setCommercialAddress(convertAddress(ocupacao.getEndereco()));
		
		if(ocupacao.getCnpj() != null && !ocupacao.getCnpj().trim().equals("")) {
			professionalData.setBrazilianCNPJ(new Long(Uteis.removeCaracteresCpfCnpj(ocupacao.getCnpj())));
		}
		
		if(ocupacao.getDataInicialCargo() != null) {
			LocalDate dataAtual = LocalDate.now();
			LocalDate dataInicioCargo = ocupacao.getDataInicialCargo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Period periodo = Period.between(dataInicioCargo, dataAtual);
			professionalData.setCompanyTime(periodo.getMonths());
		}
		
		if(ocupacao.getTelefone() != null) {
			Phone phone = new Phone();
			phone.setAreaCode(extrairDDDTelefoneFormatado(ocupacao.getTelefone()));
			phone.setCountryCode(55);
			phone.setNumber(extrairNumeroTelefoneFormatado(ocupacao.getTelefone()));
			phone.setPhoneType(PhoneTypeEnum.LANDLINE);
			professionalData.setCommercialPhone(phone);
		}
		
		professionalData.setCommercialAddress(convertAddress(ocupacao.getEndereco()));
		
		return professionalData;
	}
	
	public static List<IdentityDocument> getIdentityDocuments(Avalista avalista) {
		
		IdentityDocument documentRgAvalista = new IdentityDocument();
		
		documentRgAvalista.setDocumentType(DocumentTypeEnum.RG);
		documentRgAvalista.setDocumentNumber(avalista.getRg());
		documentRgAvalista.setDocumentIssuer(DocumentIssuerEnum.valueOf(avalista.getOrgaoEmissorDocRg().toString()));
		documentRgAvalista.setDocumentIssuingDate(Uteis.asLocalDate(avalista.getDataEmissaoRg()));
		documentRgAvalista.setDocumentIssuer(DocumentIssuerEnum.valueOf(avalista.getOrgaoEmissorDocRg().toString()));
		
		List<IdentityDocument> identityDocumentsAvalista = new ArrayList<IdentityDocument>();
		identityDocumentsAvalista.add(documentRgAvalista);
		
		return identityDocumentsAvalista;
	}
	
	public static List<IdentityDocument> getIdentityDocuments(ClienteSvs clienteSvs){
		
		IdentityDocument documentRG = new IdentityDocument();
		documentRG.setDocumentType(DocumentTypeEnum.RG);
		documentRG.setDocumentNumber(clienteSvs.getRg());			
		documentRG.setDocumentIssuingDate(Uteis.asLocalDate(clienteSvs.getDataEmissaoRg()));
		
		if (clienteSvs.getOrgaoEmissorDocRg() != null)
			documentRG.setDocumentIssuer(DocumentIssuerEnum.valueOf(clienteSvs.getOrgaoEmissorDocRg().toString()));
		
		List<IdentityDocument> identityDocuments = new ArrayList<IdentityDocument>();
		
		return identityDocuments;
		
	}
	
	public static Phone convertPhone(Telefone telefone) {
		
		Phone phone = new Phone();
		phone.setAreaCode(new Integer(telefone.getDdd().trim()));
		phone.setCountryCode(55);
		phone.setNumber(new Integer(telefone.getTelefone().replaceAll("-", "").trim()));

		if (telefone.getTipoTelefone().getDescricao().contains("CELULAR")) {
			phone.setPhoneType(PhoneTypeEnum.MOBILE);
		} else {
			phone.setPhoneType(PhoneTypeEnum.LANDLINE);
		}
		
		return phone;
	}
	
	public static PersonalReference convertPersonalReference(ReferenciaPessoal referencia) {
		
		PersonalReference personalReference = new PersonalReference();
		personalReference.setName(referencia.getNome());

		if(referencia.getTelefoneCelular() != null && !referencia.getTelefoneCelular().trim().equals("")) {
			personalReference.setPhone(IntegracaoZFlowUtil.convertPhone(referencia.getTelefoneCelular(), PhoneTypeEnum.MOBILE));
			
		}else if(referencia.getTelefoneFixo()!= null && !referencia.getTelefoneFixo().trim().equals("")) {
			personalReference.setPhone(IntegracaoZFlowUtil.convertPhone(referencia.getTelefoneFixo(), PhoneTypeEnum.LANDLINE));
		}
		
		return personalReference;
	}
	
	public static Phone convertPhone(String telefone, PhoneTypeEnum phoneTypeEnum) {
		
		Phone phoneMobile = new Phone();
		phoneMobile.setAreaCode(extrairDDDTelefoneFormatado(telefone));
		phoneMobile.setCountryCode(55);
		phoneMobile.setNumber(extrairNumeroTelefoneFormatado(telefone));
		phoneMobile.setPhoneType(phoneTypeEnum);
		
		return phoneMobile;
	}
	
	public static void createBankReference(ClientReferences clientReferences, ReferenciaBancaria refBancaria2, DeParaBancoZFlow deParaBanco) {
		
		if(deParaBanco != null) {
			BankReference bankReference = new BankReference();
			bankReference.setAgency(refBancaria2.getAgencia() + "-" + refBancaria2.getDigitoAgencia());
			bankReference.setBankCode(deParaBanco.getBankCodeEnum());
			bankReference.setCheckingAccount(refBancaria2.getConta() + "-" + refBancaria2.getDigitoConta());
			clientReferences.setBankReference(bankReference);
		}
	}
	
	public static void popularReferenciaBancaria2(ClienteSvs clienteSvs, Client clientZflow, DeParaBancoZFlow deParaBanco) {
		ReferenciaBancaria refBancaria2 = clienteSvs.getReferenciaBancaria1();
		if(deParaBanco != null && clientZflow.getClientReferences().getBankReference() == null && refBancaria2 != null && refBancaria2.getBanco() != null) {
			IntegracaoZFlowUtil.createBankReference(clientZflow.getClientReferences(), refBancaria2, deParaBanco);
		}
	}

	public static void popularReferenciaBancaria1(ClienteSvs clienteSvs, Client clientZflow, DeParaBancoZFlow deParaBanco) {
		ReferenciaBancaria refBancaria1 = clienteSvs.getReferenciaBancaria1();
		if(deParaBanco != null && refBancaria1 != null && refBancaria1.getBanco() != null) {
			IntegracaoZFlowUtil.createBankReference(clientZflow.getClientReferences(), refBancaria1, deParaBanco);
		}
	}
	
	public static void popularReferenciasPessoais(ClienteSvs clienteSvs, Client clientZflow) {
		for (ReferenciaPessoal referencia : clienteSvs.getReferenciasPessoais()) {
			clientZflow.getClientReferences().addPersonalReferencesItem(IntegracaoZFlowUtil.convertPersonalReference(referencia));
		}
	}
	
	public static void popularTelefones(ClienteSvs clienteSvs, Client clientZflow) {
		for (Telefone telefone : clienteSvs.getTelefones()) {
			clientZflow.addPersonalPhonesItem(IntegracaoZFlowUtil.convertPhone(telefone));
		}
	}
	
	public static void popularDadosPessoais(ClienteSvs clienteSvs, Client clientZflow) {
		clientZflow.setName(clienteSvs.getNome());
		clientZflow.setCpf(new Long(Uteis.removeCaracteresCpfCnpj(clienteSvs.getCpfCnpj())));
		clientZflow.setEmail(clienteSvs.getEmail());
		clientZflow.setBirthDate(clienteSvs.getDataNascimento());
		clientZflow.setBirthCity(clienteSvs.getMunicipioNascimento() != null  ? clienteSvs.getMunicipioNascimento().getDescription() : "");
		clientZflow.setBirthState(clienteSvs.getMunicipioNascimento().getEstado() != null  ? clienteSvs.getMunicipioNascimento().getEstado().getId() : "");
		clientZflow.setNationality(clienteSvs.getNacionalidade() != null  ? clienteSvs.getNacionalidade().getDescription() : "");
		clientZflow.setFatherName(clienteSvs.getNomePai());
		clientZflow.setMotherName(clienteSvs.getNomeMae());
		
		clientZflow.setGender(converterSexoSvsParaZFlow(clienteSvs.getSexo()));
		clientZflow.setMaritalStatus(convertMaritalStatus(clienteSvs.getEstadoCivil()));
		clientZflow.setGraduation(convertGraduation(clienteSvs.getEscolaridade()));
		clientZflow.setIdentityDocuments(getIdentityDocuments(clienteSvs));
		clientZflow.setResidentialAddress(convertAddress(clienteSvs.getEnderecoFichaCliente()));
		clientZflow.residenceType(convertResidenceType(clienteSvs.getEnderecoFichaCliente()));
	}
	
	public static void popularDadosConjuge(ClienteSvs clienteSvs, Client clientZflow) {
		
		if(clienteSvs.getEstadoCivil().equals(EnumEstadoCivil.CASADO)) {
			
			MaritalPartner maritalPartner = new MaritalPartner();
			clientZflow.setMaritalPartner(maritalPartner);
			
			Conjuge conjuge = clienteSvs.getConjuge();
			
			maritalPartner.setBirthDate(conjuge.getDataNascimento().getTime());
			maritalPartner.setCpf(new Long(Uteis.removeCaracteresCpfCnpj(conjuge.getCpf())));
			maritalPartner.setFullName(conjuge.getNome());
			maritalPartner.setIncomeTotalValue(conjuge.getOcupacao().getRenda().intValue());
			maritalPartner.setProfessionalData(IntegracaoZFlowUtil.convertProfessionalData(conjuge.getOcupacao()));
		}
	}
	
	public static void popularDadosConjuge(Avalista avalista, Client clientZflow) {
		
		if(avalista.getEstadoCivil().equals(EnumEstadoCivil.CASADO)) {
			
			MaritalPartner maritalPartner = new MaritalPartner();
			clientZflow.setMaritalPartner(maritalPartner);
			
			Conjuge conjuge = avalista.getConjuge();
			
			maritalPartner.setBirthDate(conjuge.getDataNascimento().getTime());
			maritalPartner.setCpf(new Long(Uteis.removeCaracteresCpfCnpj(conjuge.getCpf())));
			maritalPartner.setFullName(conjuge.getNome());
			maritalPartner.setIncomeTotalValue(conjuge.getOcupacao().getRenda().intValue());
			maritalPartner.setProfessionalData(IntegracaoZFlowUtil.convertProfessionalData(conjuge.getOcupacao()));
		}
	}	
	
	public static void popularDadosPessoais(Avalista avalista, Client avalistaZFlow) {
		
		avalistaZFlow.setName(avalista.getNome());
		
		if(avalista.getDataNascimento() != null) {
			avalistaZFlow.setBirthDate(avalista.getDataNascimento().getTime());
		}
		
		avalistaZFlow.setCpf(new Long(Uteis.removeCaracteresCpfCnpj(avalista.getCpf())));
		avalistaZFlow.setEmail(avalista.getEmail());
		avalistaZFlow.setNationality(avalista.getNacionalidade());
		avalistaZFlow.setFatherName(avalista.getNomePai());
		avalistaZFlow.setMotherName(avalista.getNomeMae());
		avalistaZFlow.setGender(IntegracaoZFlowUtil.converterSexoSvsParaZFlow(avalista.getSexo()));
		avalistaZFlow.setMaritalStatus(IntegracaoZFlowUtil.convertMaritalStatus(avalista.getEstadoCivil()));
		avalistaZFlow.setGraduation(IntegracaoZFlowUtil.convertGraduation(avalista.getEscolaridade()));
		avalistaZFlow.setIdentityDocuments(IntegracaoZFlowUtil.getIdentityDocuments(avalista));
		avalistaZFlow.setResidentialAddress(IntegracaoZFlowUtil.convertAddress(avalista.getEndereco()));
		avalistaZFlow.residenceType(IntegracaoZFlowUtil.convertResidenceType(avalista.getEndereco()));
	}
	
	public static void popularDadosProfissionais(ClienteSvs clienteSvs, Client clientZflow) {
		clientZflow.setProfessionalData(IntegracaoZFlowUtil.convertProfessionalData(clienteSvs.getOcupacao()));
	}
	
	public static void popularDadosProfissionais(Avalista avalista, Client avalistaZFlow) {
		avalistaZFlow.setProfessionalData(IntegracaoZFlowUtil.convertProfessionalData(avalista.getOcupacao()));
	}
	
	public static void popularInformacoesDaProposta(OperacaoFinanciada operacaoFinanciada, FinanceOperationRequest operationRequest) {
		
		Proposta propostaSvs = operacaoFinanciada.getProposta();
		VeiculoProposta veiculoSvs = propostaSvs.getVeiculo();
		
		operationRequest.setCertifiedAgent(70185616127L);
		operationRequest.setUpfrontValue(operacaoFinanciada.getValorEntradaItau() != null ? new Float(operacaoFinanciada.getValorEntradaItau()) : 0);
		operationRequest.setVehicleValue(operacaoFinanciada.getValorVeiculoItau() != null ? new Float(operacaoFinanciada.getValorVeiculoItau()) : 0);
		operationRequest.setAccessoriesValue(operacaoFinanciada.getValorAcessoriosItau() != null ? new Float(operacaoFinanciada.getValorAcessoriosItau()) : 0);
		operationRequest.setVehicleSellerDocument(2215837000183L);
		operationRequest.setVehicleMake(operacaoFinanciada.getMarcaSantander().getDescription());
		operationRequest.setVehicleModel(veiculoSvs.getDescricaoModelo());
		operationRequest.setVehicleModelYear(veiculoSvs.getVeiculoAnoModelo());
		operationRequest.setVehicleManufactureYear(veiculoSvs.getVeiculoAnoFabricacao());
		
		if (operacaoFinanciada.getDepartamento().equals(EnumDepartamentoSvs.NOVOS) || operacaoFinanciada.getDepartamento().equals(EnumDepartamentoSvs.VENDA_DIRETA)) {
			operationRequest.newVehicle(Boolean.TRUE);
		} else {
			operationRequest.newVehicle(Boolean.FALSE);
		}

		if (veiculoSvs.getCodigoModeloMolicar() != null) {
			operationRequest.setVehicleMolicarCode(veiculoSvs.getCodigoModeloMolicar());
		}

		if (veiculoSvs.getCodigoModeloFipe() != null) {
			operationRequest.setVehicleMolicarCode(veiculoSvs.getCodigoModeloFipe());
		}
	}
	
	public static void popularPatrimonios(Avalista avalista, Client avalistaZFlow) {
		
		ClientAssets clientAssets =  new ClientAssets();
		clientAssets.setClientEstate(new ClientEstate());
		clientAssets.getClientEstate().setQuantity(avalista.getImoveisPatrimonio().size());
		clientAssets.getClientEstate().setTotalEstatesValue(avalista.getValorTotalPatrimonioImoveis().floatValue());
		
		clientAssets.setClientVehicle(new ClientVehicle());
		clientAssets.getClientVehicle().setQuantity(avalista.getVeiculosPatrimonio().size());
		
		if(avalista.getVeiculosPatrimonio().size() > 0) {
			
			clientAssets.getClientVehicle().setAvalistCarBrand(avalista.getVeiculosPatrimonio().get(0).getMarca());
			clientAssets.getClientVehicle().setAvalistCarManufactureYear(avalista.getVeiculosPatrimonio().get(0).getAnoFabricacao());
			clientAssets.getClientVehicle().setAvalistCarModel(avalista.getVeiculosPatrimonio().get(0).getModelo());
			clientAssets.getClientVehicle().setAvalistCarModelYear(avalista.getVeiculosPatrimonio().get(0).getAnoModelo());
			clientAssets.getClientVehicle().setAvalistCarValue(avalista.getVeiculosPatrimonio().get(0).getValor().floatValue());
		}
		
		avalistaZFlow.setClientAssets(clientAssets);
	}
	
	public static void popularPatrimonios(ClienteSvs clienteSvs, Client clientZFlow) {
		
		ClientAssets clientAssets =  new ClientAssets();
		clientAssets.setClientEstate(new ClientEstate());
		clientAssets.getClientEstate().setQuantity(clienteSvs.getImoveisPatrimonio().size());
		clientAssets.getClientEstate().setTotalEstatesValue(clienteSvs.getValorTotalPatrimonioImoveis().floatValue());
		
		clientAssets.setClientVehicle(new ClientVehicle());
		clientAssets.getClientVehicle().setQuantity(clienteSvs.getVeiculosPatrimonio().size());
		
		if(clienteSvs.getVeiculosPatrimonio().size() > 0) {
			
			clientAssets.getClientVehicle().setAvalistCarBrand(clienteSvs.getVeiculosPatrimonio().get(0).getMarca());
			clientAssets.getClientVehicle().setAvalistCarManufactureYear(clienteSvs.getVeiculosPatrimonio().get(0).getAnoFabricacao());
			clientAssets.getClientVehicle().setAvalistCarModel(clienteSvs.getVeiculosPatrimonio().get(0).getModelo());
			clientAssets.getClientVehicle().setAvalistCarModelYear(clienteSvs.getVeiculosPatrimonio().get(0).getAnoModelo());
			clientAssets.getClientVehicle().setAvalistCarValue(clienteSvs.getVeiculosPatrimonio().get(0).getValor().floatValue());
		}
		
		clientZFlow.setClientAssets(clientAssets);
	}
	
	private static Integer extrairDDDTelefoneFormatado(String telefone) {
		try
		{
			return new Integer(telefone.substring(1, 3).trim());
		}
		catch (NumberFormatException ex )
		{
			return 0;
		}	
	}
	
	private static Integer extrairNumeroTelefoneFormatado(String telefone) {
		try
		{
			return new Integer(telefone.substring(5, telefone.length()).replaceAll("-","").replaceAll("_","").trim());
		}
		catch (NumberFormatException ex )
		{
			return 0;
		}		
	}
}
