package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Hist�rico de log do Cad por nome do Bem Usu�rio e data de cria��o
 * 
 * @author wesley.mota
 *
 */
@Entity
@Table(name = "LogOperCad", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "classeBean", "dataOperacao", "id" }) })
public class LogOperCad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5826190548391966223L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LOGOPERCARD")
	@SequenceGenerator(name = "SEQ_LOGOPERCARD", sequenceName = "SEQ_LOGOPERCARD", allocationSize = 1)
	private Long id;

	@Column(nullable = false, length = 800)
	private String classeBean;

	@OneToOne
	@JoinColumn(columnDefinition = "id_user", referencedColumnName = "id")
	// @ForeignKey(name="fk_id_user")
	private AcessUser user;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataOperacao;

	@Column(nullable = false, length = 3000)
	private String oQueMudou;

	@Column(nullable = true, length = 87)
	private String ipMaquina;

	@Column(nullable = true, length = 100)
	private String nomeHost;

	@Column(nullable = false)
	private String idObjetoNegocio;

	@Column
	private String tipoOperacao;

	public LogOperCad() {

	}

	/**
	 * 
	 * @param user
	 * @param dataOperacao
	 * @param oQueMudou
	 * @param ipMaquina
	 */
	public LogOperCad(String idObjetoNegocio, AcessUser user, Calendar dataOperacao, String oQueMudou, String ipMaquina,
			String classeBean, String nomeHost, String tipoOperacao) {
		super();
		this.user = user;
		this.dataOperacao = dataOperacao;
		this.oQueMudou = oQueMudou;
		this.ipMaquina = ipMaquina;
		this.classeBean = classeBean;
		this.nomeHost = nomeHost;
		this.idObjetoNegocio = idObjetoNegocio;
		this.tipoOperacao = tipoOperacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AcessUser getUser() {
		return user;
	}

	public void setUser(AcessUser user) {
		this.user = user;
	}

	public Calendar getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(Calendar dataOperacao) {
		dataOperacao.setTime(dataOperacao.getTime());

		this.dataOperacao = dataOperacao;
	}

	public String getoQueMudou() {
		return oQueMudou;
	}

	public void setoQueMudou(String oQueMudou) {
		this.oQueMudou = oQueMudou;
	}

	/**
	 * Nome do Bean que chamou
	 * 
	 * @return
	 */
	public String getClasseBean() {
		return classeBean;
	}

	/**
	 * Nome do Bean que chamou
	 * 
	 * @param classeBean
	 */
	public void setClasseBean(String classeBean) {
		this.classeBean = classeBean;
	}

	public String getIpMaquina() {
		return ipMaquina;
	}

	public void setIpMaquina(String ipMaquina) {
		this.ipMaquina = ipMaquina;
	}

	public String getNomeHost() {
		return nomeHost;
	}

	public void setNomeHost(String nomeHost) {
		this.nomeHost = nomeHost;
	}

	public String getIdObjetoNegocio() {
		return idObjetoNegocio;
	}

	public void setIdObjetoNegocio(String idObjetoNegocio) {
		this.idObjetoNegocio = idObjetoNegocio;
	}

	public String getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(String tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
