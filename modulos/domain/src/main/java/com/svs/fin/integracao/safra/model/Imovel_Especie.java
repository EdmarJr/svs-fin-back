/**
 * Imovel_Especie.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Imovel_Especie")
public class Imovel_Especie implements java.io.Serializable {
	private java.lang.String ds_imovel_especie;

	@Id
	private java.lang.Integer id_imovel_especie;

	private java.lang.String id_pessoa;

	public Imovel_Especie() {
	}

	public Imovel_Especie(java.lang.String ds_imovel_especie, java.lang.Integer id_imovel_especie,
			java.lang.String id_pessoa) {
		this.ds_imovel_especie = ds_imovel_especie;
		this.id_imovel_especie = id_imovel_especie;
		this.id_pessoa = id_pessoa;
	}

	/**
	 * Gets the ds_imovel_especie value for this Imovel_Especie.
	 * 
	 * @return ds_imovel_especie
	 */
	public java.lang.String getDs_imovel_especie() {
		return ds_imovel_especie;
	}

	/**
	 * Sets the ds_imovel_especie value for this Imovel_Especie.
	 * 
	 * @param ds_imovel_especie
	 */
	public void setDs_imovel_especie(java.lang.String ds_imovel_especie) {
		this.ds_imovel_especie = ds_imovel_especie;
	}

	/**
	 * Gets the id_imovel_especie value for this Imovel_Especie.
	 * 
	 * @return id_imovel_especie
	 */
	public java.lang.Integer getId_imovel_especie() {
		return id_imovel_especie;
	}

	/**
	 * Sets the id_imovel_especie value for this Imovel_Especie.
	 * 
	 * @param id_imovel_especie
	 */
	public void setId_imovel_especie(java.lang.Integer id_imovel_especie) {
		this.id_imovel_especie = id_imovel_especie;
	}

	/**
	 * Gets the id_pessoa value for this Imovel_Especie.
	 * 
	 * @return id_pessoa
	 */
	public java.lang.String getId_pessoa() {
		return id_pessoa;
	}

	/**
	 * Sets the id_pessoa value for this Imovel_Especie.
	 * 
	 * @param id_pessoa
	 */
	public void setId_pessoa(java.lang.String id_pessoa) {
		this.id_pessoa = id_pessoa;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Imovel_Especie))
			return false;
		Imovel_Especie other = (Imovel_Especie) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.ds_imovel_especie == null && other.getDs_imovel_especie() == null)
				|| (this.ds_imovel_especie != null && this.ds_imovel_especie.equals(other.getDs_imovel_especie())))
				&& ((this.id_imovel_especie == null && other.getId_imovel_especie() == null)
						|| (this.id_imovel_especie != null
								&& this.id_imovel_especie.equals(other.getId_imovel_especie())))
				&& ((this.id_pessoa == null && other.getId_pessoa() == null)
						|| (this.id_pessoa != null && this.id_pessoa.equals(other.getId_pessoa())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_imovel_especie() != null) {
			_hashCode += getDs_imovel_especie().hashCode();
		}
		if (getId_imovel_especie() != null) {
			_hashCode += getId_imovel_especie().hashCode();
		}
		if (getId_pessoa() != null) {
			_hashCode += getId_pessoa().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Imovel_Especie.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Especie"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_imovel_especie");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_imovel_especie"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_imovel_especie");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_imovel_especie"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_pessoa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_pessoa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
