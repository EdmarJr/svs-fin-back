package com.svs.fin.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

public class RelatorioJasperUtil {
	
	public byte[] gerarRelatorioPdf(HashMap<String, Object> parametrosRelatorio, String nomeRelatorioJasper, Object[] listItens, String reportPath, String filesPath) {
		byte[] retornoByte = null;
		
		try {
			InputStream inputImage = new FileInputStream(filesPath + File.separator + "gruposagamini.png");
			parametrosRelatorio.put("LOGO", inputImage);
			parametrosRelatorio.put("REPORT_LOCALE", new Locale("pt","BR"));

			String caminhoArquivoJasper = "";
			if (reportPath != null) {
				parametrosRelatorio.put("SUBREPORT_DIR", filesPath + File.separator + reportPath + File.separator);
				caminhoArquivoJasper = filesPath + File.separator + reportPath + File.separator
						+ nomeRelatorioJasper + ".jasper";
			} else {
				parametrosRelatorio.put("SUBREPORT_DIR", filesPath + File.separator);
				caminhoArquivoJasper = filesPath + File.separator + nomeRelatorioJasper + ".jasper";
			}

			File jasperFile = new File(caminhoArquivoJasper);

			JRDataSource jasperDataSource = new JRBeanArrayDataSource(listItens);
			JasperReport relatorioJasper;
			relatorioJasper = (JasperReport) JRLoader.loadObject(jasperFile);
			JasperPrint jasperPrint = JasperFillManager.fillReport(relatorioJasper, parametrosRelatorio, jasperDataSource);
			retornoByte = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return retornoByte;
	}
}
