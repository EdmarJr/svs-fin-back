package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQueries({
		@NamedQuery(name = ReferenciaPessoal.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = ReferenciaPessoal.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferenciaPessoal implements Serializable {
	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "ReferenciaPessoal.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from ReferenciaPessoal e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	private static final long serialVersionUID = 3405377150488583707L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REFERENCIAPESSOAL")
	@SequenceGenerator(name = "SEQ_REFERENCIAPESSOAL", sequenceName = "SEQ_REFERENCIAPESSOAL", allocationSize = 1)
	private Long id;

	@Column(length = 200)
	private String nome;

	@Column(length = 200)
	private String relacionamento;

	@Column(length = 200)
	private String telefoneFixo;

	@Column(length = 200)
	private String telefoneCelular;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	private ClienteSvs clienteSvs;

	@Transient
	private Boolean editando;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRelacionamento() {
		return relacionamento;
	}

	public void setRelacionamento(String relacionamento) {
		this.relacionamento = relacionamento;
	}

	public String getTelefoneFixo() {
		return telefoneFixo;
	}

	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReferenciaPessoal other = (ReferenciaPessoal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
