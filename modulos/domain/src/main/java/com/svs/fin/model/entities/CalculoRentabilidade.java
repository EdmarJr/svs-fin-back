package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.enums.EnumBancoIntegracaoOnline;
import com.svs.fin.enums.EnumStatusAprovacaoBancaria;

//import com.cad.enums.EnumBancoIntegracaoOnline;
//import com.cad.enums.EnumStatusAprovacaoBancaria;
//import com.cad.model.AnexosCad;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
	@NamedQuery(name = CalculoRentabilidade.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME, query = CalculoRentabilidade.NQ_OBTER_POR_OPERACAO_FINANCIADA.JPQL) })
public class CalculoRentabilidade implements Serializable {

	public static interface NQ_OBTER_POR_OPERACAO_FINANCIADA {
		public static final String NAME = "CalculoRentabilidade.obterPorOperacaoFinanciada";
		public static final String JPQL = "Select pe from CalculoRentabilidade pe where pe.operacaoFinanciada.id = :"
				+ NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA;
		public static final String NM_PM_ID_OPERACAO_FINANCIADA = "idOperacaoFinanciada";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1485453728645472558L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CALCULORENTABILIDADE")
	@SequenceGenerator(name = "SEQ_CALCULORENTABILIDADE", sequenceName = "SEQ_CALCULORENTABILIDADE", allocationSize = 1)
	private Long id;
	//
	@Enumerated(EnumType.STRING)
	@Column
	private EnumStatusAprovacaoBancaria status;

	@ManyToOne
	@JoinColumn(name = "id_instituicao_financ")
	private InstituicaoFinanceira instituicaoFinanceira;

	@ManyToOne
	@JoinColumn(name = "id_operacao")
	private OperacaoFinanciada operacaoFinanciada;

	@ManyToOne
	@JoinColumn(name = "id_mascara")
	private MascaraTaxaTabelaFinanceira mascaraTaxa;

	@ManyToOne
	@JoinColumn(name = "id_tabelaFinanceira")
	private TabelaFinanceira tabelaFinanceira;
	
	@OneToMany(mappedBy = "calculoRentabilidade", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<AnexoCad> anexos;	

	// @OneToMany(mappedBy = "calculoRentabilidade", targetEntity =
	// AprovacaoBancaria.class, cascade = { CascadeType.MERGE,
	// CascadeType.PERSIST })
	// @OrderBy("data DESC")
	// private List<AprovacaoBancaria> aprovacoesBancarias;
	//
	// @ManyToMany(targetEntity = AnexosCad.class, fetch = FetchType.LAZY, cascade =
	// CascadeType.ALL)
	// @JoinTable(name = "calculo_rentabilidade_anexos", joinColumns =
	// @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name =
	// "anexo_id"))
	// private List<AnexosCad> anexos;

	@Column
	private Boolean enviadoAoBanco;

	@Column
	private Boolean coeficienteAlterado;

	@Column
	private Integer parcelas;

	@Column
	private Double valor;

	@Column
	private Double valorEntrada;

	@Column
	private Double percEntrada;

	@Column
	private Double valorFinanciado;

	@Column
	private Double valorPmt;

	@Column
	private Double valorTac;

	@Column
	private Double percRetorno;

	@Column
	private Double valorRetorno;

	@Column
	private Double percPlus;

	@Column
	private Double valorPlus;

	@Column
	private Double coeficiente;

	@Column
	private Double valorCartorio;

	@Column
	private Double valorGravame;

	@Column
	private Double valorVistoria;

	@Column
	private Double valorOutros;

	@Column
	private Boolean aprovacaoOnline;

	@Column(length = 50)
	private String idFinanciamentoOnline;

	@Enumerated(EnumType.STRING)
	@Column
	private EnumBancoIntegracaoOnline bancoIntegracaoOnline;
	//
	// public CalculoRentabilidade() {
	// }
	//
	// public CalculoRentabilidade(OperacaoFinanciada operacao) {
	// this.operacaoFinanciada = operacao;
	// }

	public Double getValorRentabilidade() {
		Double valorRentabilidade = getValorRetorno() + getValorPlus();
		return valorRentabilidade;
	}

	public String getInstituicaoFinanceiraString() {
		if (instituicaoFinanceira != null) {
			return instituicaoFinanceira.getNome();
		} else {
			return "Institucional";
		}
	}

	// public String getCorStatusAprovacao() {
	//
	// if (getStatus() == null) {
	// return "";
	// }
	//
	// if (getStatus().equals(EnumStatusAprovacaoBancaria.AGUARDANDO_APROVACAO)) {
	// return "#FFFACD";
	// } else if (getStatus().equals(EnumStatusAprovacaoBancaria.APROVADO)) {
	// return "#CCFFCC "; // #98FB98
	// } else if (getStatus().equals(EnumStatusAprovacaoBancaria.REPROVADO)) {
	// return "#F08080";
	// }
	//
	// return "";
	// }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InstituicaoFinanceira getInstituicaoFinanceira() {
		return instituicaoFinanceira;
	}

	public void setInstituicaoFinanceira(InstituicaoFinanceira instituicaoFinanceira) {
		this.instituicaoFinanceira = instituicaoFinanceira;
	}

	public Double getValorFinanciado() {
		return valorFinanciado;
	}

	public void setValorFinanciado(Double valorFinanciado) {
		this.valorFinanciado = valorFinanciado;
	}

	public Double getPercRetorno() {
		return percRetorno;
	}

	public void setPercRetorno(Double percRetorno) {
		this.percRetorno = percRetorno;
	}

	public Double getValorRetorno() {
		if (valorRetorno == null) {
			return 0d;
		}
		return valorRetorno;
	}

	public void setValorRetorno(Double valorRetorno) {
		this.valorRetorno = valorRetorno;
	}

	public Double getPercPlus() {
		return percPlus;
	}

	public void setPercPlus(Double percPlus) {
		this.percPlus = percPlus;
	}

	public Double getValorPlus() {
		if (valorPlus == null) {
			return 0d;
		}
		return valorPlus;
	}

	public void setValorPlus(Double valorPlus) {
		this.valorPlus = valorPlus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	//
	// public OperacaoFinanciada getOperacaoFinanciada() {
	// return operacaoFinanciada;
	// }
	//
	// public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
	// this.operacaoFinanciada = operacaoFinanciada;
	// }

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public Double getValorPmt() {
		return valorPmt;
	}

	public void setValorPmt(Double valorPmt) {
		this.valorPmt = valorPmt;
	}

	public Double getValorTac() {
		return valorTac;
	}

	public void setValorTac(Double valorTac) {
		this.valorTac = valorTac;
	}

	public MascaraTaxaTabelaFinanceira getMascaraTaxa() {
		return mascaraTaxa;
	}

	public void setMascaraTaxa(MascaraTaxaTabelaFinanceira mascaraTaxa) {
		this.mascaraTaxa = mascaraTaxa;
	}

	public TabelaFinanceira getTabelaFinanceira() {
		return tabelaFinanceira;
	}

	public void setTabelaFinanceira(TabelaFinanceira tabelaFinanceira) {
		this.tabelaFinanceira = tabelaFinanceira;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getValorEntrada() {
		return valorEntrada;
	}

	public void setValorEntrada(Double valorEntrada) {
		this.valorEntrada = valorEntrada;
	}

	public Double getPercEntrada() {
		return percEntrada;
	}

	public void setPercEntrada(Double percEntrada) {
		this.percEntrada = percEntrada;
	}

	public Boolean getEnviadoAoBanco() {
		return enviadoAoBanco;
	}

	public void setEnviadoAoBanco(Boolean enviadoAoBanco) {
		this.enviadoAoBanco = enviadoAoBanco;
	}

	// public EnumStatusAprovacaoBancaria getStatus() {
	// return status;
	// }
	//
	// public void setStatus(EnumStatusAprovacaoBancaria status) {
	// this.status = status;
	// }
	//
	// public List<AprovacaoBancaria> getAprovacoesBancarias() {
	// if (aprovacoesBancarias == null) {
	// aprovacoesBancarias = new ArrayList<AprovacaoBancaria>();
	// }
	// return aprovacoesBancarias;
	// }
	//
	// public void setAprovacoesBancarias(List<AprovacaoBancaria>
	// aprovacoesBancarias) {
	// this.aprovacoesBancarias = aprovacoesBancarias;
	// }

	public List<AnexoCad> getAnexos() {
		if (anexos == null) {
			anexos = new ArrayList<AnexoCad>();
		}
		return anexos;
	}

	public void setAnexos(List<AnexoCad> anexos) {
		this.anexos = anexos;
	}	

	public Double getCoeficiente() {
		return coeficiente;
	}

	public void setCoeficiente(Double coeficiente) {
		this.coeficiente = coeficiente;
	}

	public Boolean getCoeficienteAlterado() {
		if (coeficienteAlterado == null) {
			coeficienteAlterado = Boolean.FALSE;
		}
		return coeficienteAlterado;
	}

	public void setCoeficienteAlterado(Boolean coeficienteAlterado) {
		this.coeficienteAlterado = coeficienteAlterado;
	}

	public Double getValorCartorio() {
		if (valorCartorio == null) {
			valorCartorio = 0d;
		}
		return valorCartorio;
	}

	public void setValorCartorio(Double valorCartorio) {
		this.valorCartorio = valorCartorio;
	}

	public Double getValorGravame() {
		if (valorGravame == null) {
			valorGravame = 0d;
		}
		return valorGravame;
	}

	public void setValorGravame(Double valorGravame) {
		this.valorGravame = valorGravame;
	}

	public Double getValorVistoria() {
		if (valorVistoria == null) {
			valorVistoria = 0d;
		}
		return valorVistoria;
	}

	public void setValorVistoria(Double valorVistoria) {
		this.valorVistoria = valorVistoria;
	}

	public Double getValorOutros() {
		if (valorOutros == null) {
			valorOutros = 0d;
		}
		return valorOutros;
	}

	public void setValorOutros(Double valorOutros) {
		this.valorOutros = valorOutros;
	}

	public Double getTotalTributos() {
		return getValorCartorio() + getValorGravame() + getValorVistoria() + getValorOutros();
	}

	public Boolean getAprovacaoOnline() {
		if (aprovacaoOnline == null) {
			aprovacaoOnline = false;
		}
		return aprovacaoOnline;
	}

	public void setAprovacaoOnline(Boolean aprovacaoOnline) {
		this.aprovacaoOnline = aprovacaoOnline;
	}

	public EnumBancoIntegracaoOnline getBancoIntegracaoOnline() {
		return bancoIntegracaoOnline;
	}

	public void setBancoIntegracaoOnline(EnumBancoIntegracaoOnline
			bancoIntegracaoOnline) {
		this.bancoIntegracaoOnline = bancoIntegracaoOnline;
	}

	public String getIdFinanciamentoOnline() {
		return idFinanciamentoOnline;
	}

	public void setIdFinanciamentoOnline(String idFinanciamentoOnline) {
		this.idFinanciamentoOnline = idFinanciamentoOnline;
	}

	public EnumStatusAprovacaoBancaria getStatus() {
		return status;
	}

	public void setStatus(EnumStatusAprovacaoBancaria status) {
		this.status = status;
	}

	public OperacaoFinanciada getOperacaoFinanciada() {
		return operacaoFinanciada;
	}

	public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}


}
