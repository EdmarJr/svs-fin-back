package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.svs.fin.model.entities.interfaces.Identificavel;

@Entity
public class OpcionalVeiculo implements Serializable, Identificavel<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4871984870481482561L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OPCIONALVEICULO")
	@SequenceGenerator(name = "SEQ_OPCIONALVEICULO", sequenceName = "SEQ_OPCIONALVEICULO", allocationSize = 1)
	private Long id;

	@Column
	private Long opcionalCodigo;

	@Column(length = 2000)
	private String opcionalDescricao;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOpcionalCodigo() {
		return opcionalCodigo;
	}

	public void setOpcionalCodigo(Long opcionalCodigo) {
		this.opcionalCodigo = opcionalCodigo;
	}

	public String getOpcionalDescricao() {
		return opcionalDescricao;
	}

	public void setOpcionalDescricao(String opcionalDescricao) {
		this.opcionalDescricao = opcionalDescricao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((opcionalCodigo == null) ? 0 : opcionalCodigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OpcionalVeiculo other = (OpcionalVeiculo) obj;
		if (opcionalCodigo == null) {
			if (other.opcionalCodigo != null)
				return false;
		} else if (!opcionalCodigo.equals(other.opcionalCodigo))
			return false;
		return true;
	}

}
