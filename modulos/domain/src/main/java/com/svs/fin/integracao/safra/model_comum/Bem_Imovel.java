/**
 * Bem_Imovel.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Bem_Imovel  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_imovel_especie;

    private java.lang.String id_imovel_tipo;

    private java.lang.String nm_cidade;

    private java.lang.String nm_endereco;

    private java.lang.String vl_imovel;

    private java.lang.String vl_onus;

    public Bem_Imovel() {
    }

    public Bem_Imovel(
           java.lang.String id_imovel_especie,
           java.lang.String id_imovel_tipo,
           java.lang.String nm_cidade,
           java.lang.String nm_endereco,
           java.lang.String vl_imovel,
           java.lang.String vl_onus) {
        this.id_imovel_especie = id_imovel_especie;
        this.id_imovel_tipo = id_imovel_tipo;
        this.nm_cidade = nm_cidade;
        this.nm_endereco = nm_endereco;
        this.vl_imovel = vl_imovel;
        this.vl_onus = vl_onus;
    }


    /**
     * Gets the id_imovel_especie value for this Bem_Imovel.
     * 
     * @return id_imovel_especie
     */
    public java.lang.String getId_imovel_especie() {
        return id_imovel_especie;
    }


    /**
     * Sets the id_imovel_especie value for this Bem_Imovel.
     * 
     * @param id_imovel_especie
     */
    public void setId_imovel_especie(java.lang.String id_imovel_especie) {
        this.id_imovel_especie = id_imovel_especie;
    }


    /**
     * Gets the id_imovel_tipo value for this Bem_Imovel.
     * 
     * @return id_imovel_tipo
     */
    public java.lang.String getId_imovel_tipo() {
        return id_imovel_tipo;
    }


    /**
     * Sets the id_imovel_tipo value for this Bem_Imovel.
     * 
     * @param id_imovel_tipo
     */
    public void setId_imovel_tipo(java.lang.String id_imovel_tipo) {
        this.id_imovel_tipo = id_imovel_tipo;
    }


    /**
     * Gets the nm_cidade value for this Bem_Imovel.
     * 
     * @return nm_cidade
     */
    public java.lang.String getNm_cidade() {
        return nm_cidade;
    }


    /**
     * Sets the nm_cidade value for this Bem_Imovel.
     * 
     * @param nm_cidade
     */
    public void setNm_cidade(java.lang.String nm_cidade) {
        this.nm_cidade = nm_cidade;
    }


    /**
     * Gets the nm_endereco value for this Bem_Imovel.
     * 
     * @return nm_endereco
     */
    public java.lang.String getNm_endereco() {
        return nm_endereco;
    }


    /**
     * Sets the nm_endereco value for this Bem_Imovel.
     * 
     * @param nm_endereco
     */
    public void setNm_endereco(java.lang.String nm_endereco) {
        this.nm_endereco = nm_endereco;
    }


    /**
     * Gets the vl_imovel value for this Bem_Imovel.
     * 
     * @return vl_imovel
     */
    public java.lang.String getVl_imovel() {
        return vl_imovel;
    }


    /**
     * Sets the vl_imovel value for this Bem_Imovel.
     * 
     * @param vl_imovel
     */
    public void setVl_imovel(java.lang.String vl_imovel) {
        this.vl_imovel = vl_imovel;
    }


    /**
     * Gets the vl_onus value for this Bem_Imovel.
     * 
     * @return vl_onus
     */
    public java.lang.String getVl_onus() {
        return vl_onus;
    }


    /**
     * Sets the vl_onus value for this Bem_Imovel.
     * 
     * @param vl_onus
     */
    public void setVl_onus(java.lang.String vl_onus) {
        this.vl_onus = vl_onus;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bem_Imovel)) return false;
        Bem_Imovel other = (Bem_Imovel) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_imovel_especie==null && other.getId_imovel_especie()==null) || 
             (this.id_imovel_especie!=null &&
              this.id_imovel_especie.equals(other.getId_imovel_especie()))) &&
            ((this.id_imovel_tipo==null && other.getId_imovel_tipo()==null) || 
             (this.id_imovel_tipo!=null &&
              this.id_imovel_tipo.equals(other.getId_imovel_tipo()))) &&
            ((this.nm_cidade==null && other.getNm_cidade()==null) || 
             (this.nm_cidade!=null &&
              this.nm_cidade.equals(other.getNm_cidade()))) &&
            ((this.nm_endereco==null && other.getNm_endereco()==null) || 
             (this.nm_endereco!=null &&
              this.nm_endereco.equals(other.getNm_endereco()))) &&
            ((this.vl_imovel==null && other.getVl_imovel()==null) || 
             (this.vl_imovel!=null &&
              this.vl_imovel.equals(other.getVl_imovel()))) &&
            ((this.vl_onus==null && other.getVl_onus()==null) || 
             (this.vl_onus!=null &&
              this.vl_onus.equals(other.getVl_onus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_imovel_especie() != null) {
            _hashCode += getId_imovel_especie().hashCode();
        }
        if (getId_imovel_tipo() != null) {
            _hashCode += getId_imovel_tipo().hashCode();
        }
        if (getNm_cidade() != null) {
            _hashCode += getNm_cidade().hashCode();
        }
        if (getNm_endereco() != null) {
            _hashCode += getNm_endereco().hashCode();
        }
        if (getVl_imovel() != null) {
            _hashCode += getVl_imovel().hashCode();
        }
        if (getVl_onus() != null) {
            _hashCode += getVl_onus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bem_Imovel.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_imovel_especie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_imovel_especie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_imovel_tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_imovel_tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_cidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_cidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_imovel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_imovel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_onus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_onus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
