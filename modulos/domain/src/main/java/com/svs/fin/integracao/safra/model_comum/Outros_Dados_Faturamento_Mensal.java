/**
 * Outros_Dados_Faturamento_Mensal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Outros_Dados_Faturamento_Mensal  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String vl_fat_anual_medio;

    private java.lang.String vl_fat_msal_maximo;

    private java.lang.String vl_fat_msal_minimo;

    public Outros_Dados_Faturamento_Mensal() {
    }

    public Outros_Dados_Faturamento_Mensal(
           java.lang.String vl_fat_anual_medio,
           java.lang.String vl_fat_msal_maximo,
           java.lang.String vl_fat_msal_minimo) {
        this.vl_fat_anual_medio = vl_fat_anual_medio;
        this.vl_fat_msal_maximo = vl_fat_msal_maximo;
        this.vl_fat_msal_minimo = vl_fat_msal_minimo;
    }


    /**
     * Gets the vl_fat_anual_medio value for this Outros_Dados_Faturamento_Mensal.
     * 
     * @return vl_fat_anual_medio
     */
    public java.lang.String getVl_fat_anual_medio() {
        return vl_fat_anual_medio;
    }


    /**
     * Sets the vl_fat_anual_medio value for this Outros_Dados_Faturamento_Mensal.
     * 
     * @param vl_fat_anual_medio
     */
    public void setVl_fat_anual_medio(java.lang.String vl_fat_anual_medio) {
        this.vl_fat_anual_medio = vl_fat_anual_medio;
    }


    /**
     * Gets the vl_fat_msal_maximo value for this Outros_Dados_Faturamento_Mensal.
     * 
     * @return vl_fat_msal_maximo
     */
    public java.lang.String getVl_fat_msal_maximo() {
        return vl_fat_msal_maximo;
    }


    /**
     * Sets the vl_fat_msal_maximo value for this Outros_Dados_Faturamento_Mensal.
     * 
     * @param vl_fat_msal_maximo
     */
    public void setVl_fat_msal_maximo(java.lang.String vl_fat_msal_maximo) {
        this.vl_fat_msal_maximo = vl_fat_msal_maximo;
    }


    /**
     * Gets the vl_fat_msal_minimo value for this Outros_Dados_Faturamento_Mensal.
     * 
     * @return vl_fat_msal_minimo
     */
    public java.lang.String getVl_fat_msal_minimo() {
        return vl_fat_msal_minimo;
    }


    /**
     * Sets the vl_fat_msal_minimo value for this Outros_Dados_Faturamento_Mensal.
     * 
     * @param vl_fat_msal_minimo
     */
    public void setVl_fat_msal_minimo(java.lang.String vl_fat_msal_minimo) {
        this.vl_fat_msal_minimo = vl_fat_msal_minimo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Outros_Dados_Faturamento_Mensal)) return false;
        Outros_Dados_Faturamento_Mensal other = (Outros_Dados_Faturamento_Mensal) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.vl_fat_anual_medio==null && other.getVl_fat_anual_medio()==null) || 
             (this.vl_fat_anual_medio!=null &&
              this.vl_fat_anual_medio.equals(other.getVl_fat_anual_medio()))) &&
            ((this.vl_fat_msal_maximo==null && other.getVl_fat_msal_maximo()==null) || 
             (this.vl_fat_msal_maximo!=null &&
              this.vl_fat_msal_maximo.equals(other.getVl_fat_msal_maximo()))) &&
            ((this.vl_fat_msal_minimo==null && other.getVl_fat_msal_minimo()==null) || 
             (this.vl_fat_msal_minimo!=null &&
              this.vl_fat_msal_minimo.equals(other.getVl_fat_msal_minimo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getVl_fat_anual_medio() != null) {
            _hashCode += getVl_fat_anual_medio().hashCode();
        }
        if (getVl_fat_msal_maximo() != null) {
            _hashCode += getVl_fat_msal_maximo().hashCode();
        }
        if (getVl_fat_msal_minimo() != null) {
            _hashCode += getVl_fat_msal_minimo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Outros_Dados_Faturamento_Mensal.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Faturamento_Mensal"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_fat_anual_medio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_fat_anual_medio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_fat_msal_maximo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_fat_msal_maximo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_fat_msal_minimo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_fat_msal_minimo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
