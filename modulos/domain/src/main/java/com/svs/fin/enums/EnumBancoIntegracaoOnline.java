package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumBancoIntegracaoOnline {

	@JsonProperty("Itau")
	ITAU("Itau"), @JsonProperty("Banco Safra")
	SAFRA("Banco Safra"), @JsonProperty("Santander")
	SANTANDER("Santander");

	private EnumBancoIntegracaoOnline(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
