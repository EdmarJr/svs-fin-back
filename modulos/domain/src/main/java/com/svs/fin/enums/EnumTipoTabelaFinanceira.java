package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoTabelaFinanceira implements EnumLabeled {
	@JsonProperty("Empresa")
	EMPRESA("Empresa"), @JsonProperty("Instituição financeira")
	INSTITUICAO_FINANCEIRA("Instituição financeira"), @JsonProperty("Promocional")
	PROMOCIONAL("Promocional"), @JsonProperty("Personalizada")
	PERSONALIZADA("Personalizada");

	EnumTipoTabelaFinanceira(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}
}
