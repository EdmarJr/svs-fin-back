package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class Terms implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6227910951848842591L;
	private String times;

	public String getTimes() {
		return times;
	}

	public void setTimes(String times) {
		this.times = times;
	}

	@Override
	public String toString() {
		return "ClassPojo [times = " + times + "]";
	}
}
