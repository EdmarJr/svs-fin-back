package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class Store implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7564731980329787106L;
	
	private String name;
	public String tabId;
	public String tableNumber;

	public String getTabId() {
		return tabId;
	}
	public void setTabId(String tabId) {
		this.tabId = tabId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTableNumber() {
		return tableNumber;
	}
	public void setTableNumber(String tableNumber) {
		this.tableNumber = tableNumber;
	}
	@Override
	public String toString() {
		return "ClassPojo [name = " + name + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((tabId == null) ? 0 : tabId.hashCode());
		result = prime * result + ((tableNumber == null) ? 0 : tableNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Store other = (Store) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (tabId == null) {
			if (other.tabId != null)
				return false;
		} else if (!tabId.equals(other.tabId))
			return false;
		if (tableNumber == null) {
			if (other.tableNumber != null)
				return false;
		} else if (!tableNumber.equals(other.tableNumber))
			return false;
		return true;
	}
}
