/**
 * Referencia.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Referencia  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String nm_endereco;

    private java.lang.String nm_referencia;

    private java.lang.String nr_cnpj;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone;

    public Referencia() {
    }

    public Referencia(
           java.lang.String nm_endereco,
           java.lang.String nm_referencia,
           java.lang.String nr_cnpj,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.nm_endereco = nm_endereco;
        this.nm_referencia = nm_referencia;
        this.nr_cnpj = nr_cnpj;
        this.telefone = telefone;
    }


    /**
     * Gets the nm_endereco value for this Referencia.
     * 
     * @return nm_endereco
     */
    public java.lang.String getNm_endereco() {
        return nm_endereco;
    }


    /**
     * Sets the nm_endereco value for this Referencia.
     * 
     * @param nm_endereco
     */
    public void setNm_endereco(java.lang.String nm_endereco) {
        this.nm_endereco = nm_endereco;
    }


    /**
     * Gets the nm_referencia value for this Referencia.
     * 
     * @return nm_referencia
     */
    public java.lang.String getNm_referencia() {
        return nm_referencia;
    }


    /**
     * Sets the nm_referencia value for this Referencia.
     * 
     * @param nm_referencia
     */
    public void setNm_referencia(java.lang.String nm_referencia) {
        this.nm_referencia = nm_referencia;
    }


    /**
     * Gets the nr_cnpj value for this Referencia.
     * 
     * @return nr_cnpj
     */
    public java.lang.String getNr_cnpj() {
        return nr_cnpj;
    }


    /**
     * Sets the nr_cnpj value for this Referencia.
     * 
     * @param nr_cnpj
     */
    public void setNr_cnpj(java.lang.String nr_cnpj) {
        this.nr_cnpj = nr_cnpj;
    }


    /**
     * Gets the telefone value for this Referencia.
     * 
     * @return telefone
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Referencia.
     * 
     * @param telefone
     */
    public void setTelefone(com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.telefone = telefone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Referencia)) return false;
        Referencia other = (Referencia) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.nm_endereco==null && other.getNm_endereco()==null) || 
             (this.nm_endereco!=null &&
              this.nm_endereco.equals(other.getNm_endereco()))) &&
            ((this.nm_referencia==null && other.getNm_referencia()==null) || 
             (this.nm_referencia!=null &&
              this.nm_referencia.equals(other.getNm_referencia()))) &&
            ((this.nr_cnpj==null && other.getNr_cnpj()==null) || 
             (this.nr_cnpj!=null &&
              this.nr_cnpj.equals(other.getNr_cnpj()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNm_endereco() != null) {
            _hashCode += getNm_endereco().hashCode();
        }
        if (getNm_referencia() != null) {
            _hashCode += getNm_referencia().hashCode();
        }
        if (getNr_cnpj() != null) {
            _hashCode += getNr_cnpj().hashCode();
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Referencia.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_cnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_cnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
