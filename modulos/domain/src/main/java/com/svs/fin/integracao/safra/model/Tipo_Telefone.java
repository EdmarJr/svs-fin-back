/**
 * Tipo_Telefone.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Telefone")
public class Tipo_Telefone implements java.io.Serializable {
	private java.lang.String ds_tipo_telefone;

	@Id
	private java.lang.Integer id_tipo_telefone;

	public Tipo_Telefone() {
	}

	public Tipo_Telefone(java.lang.String ds_tipo_telefone, java.lang.Integer id_tipo_telefone) {
		this.ds_tipo_telefone = ds_tipo_telefone;
		this.id_tipo_telefone = id_tipo_telefone;
	}

	/**
	 * Gets the ds_tipo_telefone value for this Tipo_Telefone.
	 * 
	 * @return ds_tipo_telefone
	 */
	public java.lang.String getDs_tipo_telefone() {
		return ds_tipo_telefone;
	}

	/**
	 * Sets the ds_tipo_telefone value for this Tipo_Telefone.
	 * 
	 * @param ds_tipo_telefone
	 */
	public void setDs_tipo_telefone(java.lang.String ds_tipo_telefone) {
		this.ds_tipo_telefone = ds_tipo_telefone;
	}

	/**
	 * Gets the id_tipo_telefone value for this Tipo_Telefone.
	 * 
	 * @return id_tipo_telefone
	 */
	public java.lang.Integer getId_tipo_telefone() {
		return id_tipo_telefone;
	}

	/**
	 * Sets the id_tipo_telefone value for this Tipo_Telefone.
	 * 
	 * @param id_tipo_telefone
	 */
	public void setId_tipo_telefone(java.lang.Integer id_tipo_telefone) {
		this.id_tipo_telefone = id_tipo_telefone;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Telefone))
			return false;
		Tipo_Telefone other = (Tipo_Telefone) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_tipo_telefone == null && other.getDs_tipo_telefone() == null)
						|| (this.ds_tipo_telefone != null && this.ds_tipo_telefone.equals(other.getDs_tipo_telefone())))
				&& ((this.id_tipo_telefone == null && other.getId_tipo_telefone() == null)
						|| (this.id_tipo_telefone != null
								&& this.id_tipo_telefone.equals(other.getId_tipo_telefone())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_telefone() != null) {
			_hashCode += getDs_tipo_telefone().hashCode();
		}
		if (getId_tipo_telefone() != null) {
			_hashCode += getId_tipo_telefone().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Telefone.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Telefone"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_telefone");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_tipo_telefone"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_telefone");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_tipo_telefone"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
