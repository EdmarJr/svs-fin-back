package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class Fees implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2177566728263572532L;

	private String tabFee;

	private String tcExemption;

	private String tcFee;

	private String tabExemption;
	
	
	public Fees() {}
	
	public Fees(String tabFee, String tcExemption, String tcFee, String tabExemption) {
		this.tabFee = tabFee;
		this.tcExemption = tcExemption;
		this.tcFee = tcFee;
		this.tabExemption = tabExemption;
	}

	public String getTabFee() {
		return tabFee;
	}

	public void setTabFee(String tabFee) {
		this.tabFee = tabFee;
	}

	public String getTcExemption() {
		return tcExemption;
	}

	public void setTcExemption(String tcExemption) {
		this.tcExemption = tcExemption;
	}

	public String getTcFee() {
		return tcFee;
	}

	public void setTcFee(String tcFee) {
		this.tcFee = tcFee;
	}

	public String getTabExemption() {
		return tabExemption;
	}

	public void setTabExemption(String tabExemption) {
		this.tabExemption = tabExemption;
	}

	@Override
	public String toString() {
		return "ClassPojo [tabFee = " + tabFee + ", tcExemption = " + tcExemption + ", tcFee = " + tcFee
				+ ", tabExemption = " + tabExemption + "]";
	}
}
