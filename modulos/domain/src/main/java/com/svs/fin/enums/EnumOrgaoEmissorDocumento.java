package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumOrgaoEmissorDocumento {

//	MTE("MTE"),
//	
//	OAB("OAB"),
//	
//	IIDML("IIDML"),
//
//	IIEP("IIEP"),
//
//	IITP("IITP"),
//
//	IMLC("IMLC"),
//
//	INI("INI"),
//
//	IPF("IPF"),
//
//	IPT("IPT"),
//
//	ITB("ITB"),
//
//	JUIJ("JUIJ"),
//
//	PCIID("PCIID"),
//
//	PDFII("PDFII"),
//
//	PIG("PIG"),
//
//	PJEG("PJEG"),
//
//	PTC("PTC"),
//
//	RBR("RBR"),
//
//	RFBR("RFBR"),
//
//	SDS("SDS"),
//
//	SEDS("SEDS"),
//
//	SEJSP("SEJSP"),
//
//	SEPC("SEPC"),
//
//	SES("SES"),
//
//	SESDC("SESDC"),
//
//	SESP("SESP"),
//
//	SGPJ("SGPJ"),
//
//	SIC("SIC"),
//
//	SJS("SJS"),
//
//	SJSP("SJSP"),
//
//	SJTC("SJTC"),
//
//	SPC("SPC"),
//
//	SPCII("SPCII"),
//
//	SPSP("SPSP"),
//
//	SPTC("SPTC"),
//
//	SSI("SSI"),
//
//	SSP("SSP"),
//
//	SSPDC("SSPDC"),
//
//	MRE("MRE"),
//
//	SEDPF("SEDPF"),
//
//	DETRAN("DETRAN"),
//
//	DESIPE("DESIPE"),
//
//	DESP("DESP"),
//
//	DFSP("DFSP"),
//
//	DGPC("DGPC"),
//
//	DI("DI"),
//
//	DPC("DPC"),
//
//	DPGE("DPGE"),
//
//	DPTC("DPTC"),
//
//	DSG("DSG"),
//
//	EAPJU("EAPJU"),
//
//	FUNAI("FUNAI"),
//
//	GEJ("GEJ"),
//
//	GISI("GISI"),
//
//	IC("IC"),
//
//	IFP("IFP"),
//
//	IIDAMP("IIDAMP"),
//
//	OUTROS("OUTROS");

	@JsonProperty("MTE")
	MTE("MTE"),

	@JsonProperty("OAB")
	OAB("OAB"),

	@JsonProperty("IIDML")
	IIDML("IIDML"),

	@JsonProperty("IIEP")
	IIEP("IIEP"),

	@JsonProperty("IITP")
	IITP("IITP"),

	@JsonProperty("IMLC")
	IMLC("IMLC"),

	@JsonProperty("INI")
	INI("INI"),

	@JsonProperty("IPF")
	IPF("IPF"),

	@JsonProperty("IPT")
	IPT("IPT"),

	@JsonProperty("ITB")
	ITB("ITB"),

	@JsonProperty("JUIJ")
	JUIJ("JUIJ"),

	@JsonProperty("PCIID")
	PCIID("PCIID"),

	@JsonProperty("PDFII")
	PDFII("PDFII"),

	@JsonProperty("PIG")
	PIG("PIG"),

	@JsonProperty("PJEG")
	PJEG("PJEG"),

	@JsonProperty("PTC")
	PTC("PTC"),

	@JsonProperty("RBR")
	RBR("RBR"),

	@JsonProperty("RFBR")
	RFBR("RFBR"),

	@JsonProperty("SDS")
	SDS("SDS"),

	@JsonProperty("SEDS")
	SEDS("SEDS"),

	@JsonProperty("SEJSP")
	SEJSP("SEJSP"),

	@JsonProperty("SEPC")
	SEPC("SEPC"),

	@JsonProperty("SES")
	SES("SES"),

	@JsonProperty("SESDC")
	SESDC("SESDC"),

	@JsonProperty("SESP")
	SESP("SESP"),

	@JsonProperty("SGPJ")
	SGPJ("SGPJ"),

	@JsonProperty("SIC")
	SIC("SIC"),

	@JsonProperty("SJS")
	SJS("SJS"),

	@JsonProperty("SJSP")
	SJSP("SJSP"),

	@JsonProperty("SJTC")
	SJTC("SJTC"),

	@JsonProperty("SPC")
	SPC("SPC"),

	@JsonProperty("SPCII")
	SPCII("SPCII"),

	@JsonProperty("SPSP")
	SPSP("SPSP"),

	@JsonProperty("SPTC")
	SPTC("SPTC"),

	@JsonProperty("SSI")
	SSI("SSI"),

	@JsonProperty("SSP")
	SSP("SSP"),

	@JsonProperty("SSPDC")
	SSPDC("SSPDC"),

	@JsonProperty("MRE")
	MRE("MRE"),

	@JsonProperty("SEDPF")
	SEDPF("SEDPF"),

	@JsonProperty("DETRAN")
	DETRAN("DETRAN"),

	@JsonProperty("DESIPE")
	DESIPE("DESIPE"),

	@JsonProperty("DESP")
	DESP("DESP"),

	@JsonProperty("DFSP")
	DFSP("DFSP"),

	@JsonProperty("DGPC")
	DGPC("DGPC"),

	@JsonProperty("DI")
	DI("DI"),

	@JsonProperty("DPC")
	DPC("DPC"),

	@JsonProperty("DPGE")
	DPGE("DPGE"),

	@JsonProperty("DPTC")
	DPTC("DPTC"),

	@JsonProperty("DSG")
	DSG("DSG"),

	@JsonProperty("EAPJU")
	EAPJU("EAPJU"),

	@JsonProperty("FUNAI")
	FUNAI("FUNAI"),

	@JsonProperty("GEJ")
	GEJ("GEJ"),

	@JsonProperty("GISI")
	GISI("GISI"),

	@JsonProperty("IC")
	IC("IC"),

	@JsonProperty("IFP")
	IFP("IFP"),

	@JsonProperty("IIDAMP")
	IIDAMP("IIDAMP"),
	@JsonProperty("OUTROS")
	OUTROS("OUTROS");

	private String label;

	EnumOrgaoEmissorDocumento(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
