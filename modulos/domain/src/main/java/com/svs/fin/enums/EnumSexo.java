package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumSexo {
	@JsonProperty("Feminino")
	FEMININO("Feminino"), @JsonProperty("Masculino")
	MASCULINO("Masculino");

	private String label;

	private EnumSexo(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
