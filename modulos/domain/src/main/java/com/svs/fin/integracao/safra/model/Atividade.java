/**
 * Atividade.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Atividade")
public class Atividade implements java.io.Serializable {
	private java.lang.String ds_atividade;

	@Id
	private java.lang.Integer id_atividade;

	private java.lang.Integer id_ramo_atividade;

	public Atividade() {
	}

	public Atividade(java.lang.String ds_atividade, java.lang.Integer id_atividade,
			java.lang.Integer id_ramo_atividade) {
		this.ds_atividade = ds_atividade;
		this.id_atividade = id_atividade;
		this.id_ramo_atividade = id_ramo_atividade;
	}

	/**
	 * Gets the ds_atividade value for this Atividade.
	 * 
	 * @return ds_atividade
	 */
	public java.lang.String getDs_atividade() {
		return ds_atividade;
	}

	/**
	 * Sets the ds_atividade value for this Atividade.
	 * 
	 * @param ds_atividade
	 */
	public void setDs_atividade(java.lang.String ds_atividade) {
		this.ds_atividade = ds_atividade;
	}

	/**
	 * Gets the id_atividade value for this Atividade.
	 * 
	 * @return id_atividade
	 */
	public java.lang.Integer getId_atividade() {
		return id_atividade;
	}

	/**
	 * Sets the id_atividade value for this Atividade.
	 * 
	 * @param id_atividade
	 */
	public void setId_atividade(java.lang.Integer id_atividade) {
		this.id_atividade = id_atividade;
	}

	/**
	 * Gets the id_ramo_atividade value for this Atividade.
	 * 
	 * @return id_ramo_atividade
	 */
	public java.lang.Integer getId_ramo_atividade() {
		return id_ramo_atividade;
	}

	/**
	 * Sets the id_ramo_atividade value for this Atividade.
	 * 
	 * @param id_ramo_atividade
	 */
	public void setId_ramo_atividade(java.lang.Integer id_ramo_atividade) {
		this.id_ramo_atividade = id_ramo_atividade;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Atividade))
			return false;
		Atividade other = (Atividade) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_atividade == null && other.getDs_atividade() == null)
						|| (this.ds_atividade != null && this.ds_atividade.equals(other.getDs_atividade())))
				&& ((this.id_atividade == null && other.getId_atividade() == null)
						|| (this.id_atividade != null && this.id_atividade.equals(other.getId_atividade())))
				&& ((this.id_ramo_atividade == null && other.getId_ramo_atividade() == null)
						|| (this.id_ramo_atividade != null
								&& this.id_ramo_atividade.equals(other.getId_ramo_atividade())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_atividade() != null) {
			_hashCode += getDs_atividade().hashCode();
		}
		if (getId_atividade() != null) {
			_hashCode += getId_atividade().hashCode();
		}
		if (getId_ramo_atividade() != null) {
			_hashCode += getId_ramo_atividade().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Atividade.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Atividade"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_atividade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_atividade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_atividade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_atividade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_ramo_atividade");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_ramo_atividade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
