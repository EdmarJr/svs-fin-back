/**
 * Local_Nascimento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Local_Nascimento  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_uf_nascimento;

    private java.lang.String lc_nascimento;

    public Local_Nascimento() {
    }

    public Local_Nascimento(
           java.lang.String id_uf_nascimento,
           java.lang.String lc_nascimento) {
        this.id_uf_nascimento = id_uf_nascimento;
        this.lc_nascimento = lc_nascimento;
    }


    /**
     * Gets the id_uf_nascimento value for this Local_Nascimento.
     * 
     * @return id_uf_nascimento
     */
    public java.lang.String getId_uf_nascimento() {
        return id_uf_nascimento;
    }


    /**
     * Sets the id_uf_nascimento value for this Local_Nascimento.
     * 
     * @param id_uf_nascimento
     */
    public void setId_uf_nascimento(java.lang.String id_uf_nascimento) {
        this.id_uf_nascimento = id_uf_nascimento;
    }


    /**
     * Gets the lc_nascimento value for this Local_Nascimento.
     * 
     * @return lc_nascimento
     */
    public java.lang.String getLc_nascimento() {
        return lc_nascimento;
    }


    /**
     * Sets the lc_nascimento value for this Local_Nascimento.
     * 
     * @param lc_nascimento
     */
    public void setLc_nascimento(java.lang.String lc_nascimento) {
        this.lc_nascimento = lc_nascimento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Local_Nascimento)) return false;
        Local_Nascimento other = (Local_Nascimento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_uf_nascimento==null && other.getId_uf_nascimento()==null) || 
             (this.id_uf_nascimento!=null &&
              this.id_uf_nascimento.equals(other.getId_uf_nascimento()))) &&
            ((this.lc_nascimento==null && other.getLc_nascimento()==null) || 
             (this.lc_nascimento!=null &&
              this.lc_nascimento.equals(other.getLc_nascimento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_uf_nascimento() != null) {
            _hashCode += getId_uf_nascimento().hashCode();
        }
        if (getLc_nascimento() != null) {
            _hashCode += getLc_nascimento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Local_Nascimento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Local_Nascimento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_uf_nascimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_uf_nascimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lc_nascimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "lc_nascimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
