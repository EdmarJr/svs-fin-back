package com.svs.fin.modelo.mapper;


import com.svs.fin.model.dto.EntidadeTesteEdicaoDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import teste.EntidadeTeste;

import java.util.List;

/**
 * @author Ítalo Gustavo
 */
@Mapper
public interface EntidadeTesteEdicaoMapper {

	EntidadeTesteEdicaoMapper INSTANCE = Mappers.getMapper(EntidadeTesteEdicaoMapper.class);

	EntidadeTesteEdicaoDto paraDto(EntidadeTeste source);
	
	EntidadeTeste doDto(EntidadeTesteEdicaoDto source);
	
	List<EntidadeTesteEdicaoDto> paraDto(List<EntidadeTeste> source);

	void atualizar(EntidadeTesteEdicaoDto source, @MappingTarget EntidadeTeste target);
}
