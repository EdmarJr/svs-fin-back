/**
 * Proposta_Avalista.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Proposta_Avalista  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model_comum.Avalista[] lista_avalista;

    public Proposta_Avalista() {
    }

    public Proposta_Avalista(
           com.svs.fin.integracao.safra.model_comum.Avalista[] lista_avalista) {
           this.lista_avalista = lista_avalista;
    }


    /**
     * Gets the lista_avalista value for this Proposta_Avalista.
     * 
     * @return lista_avalista
     */
    public com.svs.fin.integracao.safra.model_comum.Avalista[] getLista_avalista() {
        return lista_avalista;
    }


    /**
     * Sets the lista_avalista value for this Proposta_Avalista.
     * 
     * @param lista_avalista
     */
    public void setLista_avalista(com.svs.fin.integracao.safra.model_comum.Avalista[] lista_avalista) {
        this.lista_avalista = lista_avalista;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Proposta_Avalista)) return false;
        Proposta_Avalista other = (Proposta_Avalista) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.lista_avalista==null && other.getLista_avalista()==null) || 
             (this.lista_avalista!=null &&
              java.util.Arrays.equals(this.lista_avalista, other.getLista_avalista())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLista_avalista() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLista_avalista());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLista_avalista(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Proposta_Avalista.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_Avalista"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lista_avalista");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "lista_avalista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Avalista"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Avalista"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
