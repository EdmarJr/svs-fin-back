/**
 * Dados_Profissionais.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Dados_Profissionais  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String cnpj_empresa;

    private java.lang.String dt_admissao;

    private com.svs.fin.integracao.safra.model_comum.Endereco endereco;

    private java.lang.String id_cargo;

    private java.lang.String id_nat_ocupacao;

    private java.lang.String id_profissao;

    private java.lang.String nm_empresa;

    private java.lang.String nr_beneficio;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone;

    private java.lang.String vl_renda;

    public Dados_Profissionais() {
    }

    public Dados_Profissionais(
           java.lang.String cnpj_empresa,
           java.lang.String dt_admissao,
           com.svs.fin.integracao.safra.model_comum.Endereco endereco,
           java.lang.String id_cargo,
           java.lang.String id_nat_ocupacao,
           java.lang.String id_profissao,
           java.lang.String nm_empresa,
           java.lang.String nr_beneficio,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone,
           java.lang.String vl_renda) {
        this.cnpj_empresa = cnpj_empresa;
        this.dt_admissao = dt_admissao;
        this.endereco = endereco;
        this.id_cargo = id_cargo;
        this.id_nat_ocupacao = id_nat_ocupacao;
        this.id_profissao = id_profissao;
        this.nm_empresa = nm_empresa;
        this.nr_beneficio = nr_beneficio;
        this.telefone = telefone;
        this.vl_renda = vl_renda;
    }


    /**
     * Gets the cnpj_empresa value for this Dados_Profissionais.
     * 
     * @return cnpj_empresa
     */
    public java.lang.String getCnpj_empresa() {
        return cnpj_empresa;
    }


    /**
     * Sets the cnpj_empresa value for this Dados_Profissionais.
     * 
     * @param cnpj_empresa
     */
    public void setCnpj_empresa(java.lang.String cnpj_empresa) {
        this.cnpj_empresa = cnpj_empresa;
    }


    /**
     * Gets the dt_admissao value for this Dados_Profissionais.
     * 
     * @return dt_admissao
     */
    public java.lang.String getDt_admissao() {
        return dt_admissao;
    }


    /**
     * Sets the dt_admissao value for this Dados_Profissionais.
     * 
     * @param dt_admissao
     */
    public void setDt_admissao(java.lang.String dt_admissao) {
        this.dt_admissao = dt_admissao;
    }


    /**
     * Gets the endereco value for this Dados_Profissionais.
     * 
     * @return endereco
     */
    public com.svs.fin.integracao.safra.model_comum.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this Dados_Profissionais.
     * 
     * @param endereco
     */
    public void setEndereco(com.svs.fin.integracao.safra.model_comum.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the id_cargo value for this Dados_Profissionais.
     * 
     * @return id_cargo
     */
    public java.lang.String getId_cargo() {
        return id_cargo;
    }


    /**
     * Sets the id_cargo value for this Dados_Profissionais.
     * 
     * @param id_cargo
     */
    public void setId_cargo(java.lang.String id_cargo) {
        this.id_cargo = id_cargo;
    }


    /**
     * Gets the id_nat_ocupacao value for this Dados_Profissionais.
     * 
     * @return id_nat_ocupacao
     */
    public java.lang.String getId_nat_ocupacao() {
        return id_nat_ocupacao;
    }


    /**
     * Sets the id_nat_ocupacao value for this Dados_Profissionais.
     * 
     * @param id_nat_ocupacao
     */
    public void setId_nat_ocupacao(java.lang.String id_nat_ocupacao) {
        this.id_nat_ocupacao = id_nat_ocupacao;
    }


    /**
     * Gets the id_profissao value for this Dados_Profissionais.
     * 
     * @return id_profissao
     */
    public java.lang.String getId_profissao() {
        return id_profissao;
    }


    /**
     * Sets the id_profissao value for this Dados_Profissionais.
     * 
     * @param id_profissao
     */
    public void setId_profissao(java.lang.String id_profissao) {
        this.id_profissao = id_profissao;
    }


    /**
     * Gets the nm_empresa value for this Dados_Profissionais.
     * 
     * @return nm_empresa
     */
    public java.lang.String getNm_empresa() {
        return nm_empresa;
    }


    /**
     * Sets the nm_empresa value for this Dados_Profissionais.
     * 
     * @param nm_empresa
     */
    public void setNm_empresa(java.lang.String nm_empresa) {
        this.nm_empresa = nm_empresa;
    }


    /**
     * Gets the nr_beneficio value for this Dados_Profissionais.
     * 
     * @return nr_beneficio
     */
    public java.lang.String getNr_beneficio() {
        return nr_beneficio;
    }


    /**
     * Sets the nr_beneficio value for this Dados_Profissionais.
     * 
     * @param nr_beneficio
     */
    public void setNr_beneficio(java.lang.String nr_beneficio) {
        this.nr_beneficio = nr_beneficio;
    }


    /**
     * Gets the telefone value for this Dados_Profissionais.
     * 
     * @return telefone
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Dados_Profissionais.
     * 
     * @param telefone
     */
    public void setTelefone(com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.telefone = telefone;
    }


    /**
     * Gets the vl_renda value for this Dados_Profissionais.
     * 
     * @return vl_renda
     */
    public java.lang.String getVl_renda() {
        return vl_renda;
    }


    /**
     * Sets the vl_renda value for this Dados_Profissionais.
     * 
     * @param vl_renda
     */
    public void setVl_renda(java.lang.String vl_renda) {
        this.vl_renda = vl_renda;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Dados_Profissionais)) return false;
        Dados_Profissionais other = (Dados_Profissionais) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.cnpj_empresa==null && other.getCnpj_empresa()==null) || 
             (this.cnpj_empresa!=null &&
              this.cnpj_empresa.equals(other.getCnpj_empresa()))) &&
            ((this.dt_admissao==null && other.getDt_admissao()==null) || 
             (this.dt_admissao!=null &&
              this.dt_admissao.equals(other.getDt_admissao()))) &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.id_cargo==null && other.getId_cargo()==null) || 
             (this.id_cargo!=null &&
              this.id_cargo.equals(other.getId_cargo()))) &&
            ((this.id_nat_ocupacao==null && other.getId_nat_ocupacao()==null) || 
             (this.id_nat_ocupacao!=null &&
              this.id_nat_ocupacao.equals(other.getId_nat_ocupacao()))) &&
            ((this.id_profissao==null && other.getId_profissao()==null) || 
             (this.id_profissao!=null &&
              this.id_profissao.equals(other.getId_profissao()))) &&
            ((this.nm_empresa==null && other.getNm_empresa()==null) || 
             (this.nm_empresa!=null &&
              this.nm_empresa.equals(other.getNm_empresa()))) &&
            ((this.nr_beneficio==null && other.getNr_beneficio()==null) || 
             (this.nr_beneficio!=null &&
              this.nr_beneficio.equals(other.getNr_beneficio()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone()))) &&
            ((this.vl_renda==null && other.getVl_renda()==null) || 
             (this.vl_renda!=null &&
              this.vl_renda.equals(other.getVl_renda())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCnpj_empresa() != null) {
            _hashCode += getCnpj_empresa().hashCode();
        }
        if (getDt_admissao() != null) {
            _hashCode += getDt_admissao().hashCode();
        }
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getId_cargo() != null) {
            _hashCode += getId_cargo().hashCode();
        }
        if (getId_nat_ocupacao() != null) {
            _hashCode += getId_nat_ocupacao().hashCode();
        }
        if (getId_profissao() != null) {
            _hashCode += getId_profissao().hashCode();
        }
        if (getNm_empresa() != null) {
            _hashCode += getNm_empresa().hashCode();
        }
        if (getNr_beneficio() != null) {
            _hashCode += getNr_beneficio().hashCode();
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        if (getVl_renda() != null) {
            _hashCode += getVl_renda().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Dados_Profissionais.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Dados_Profissionais"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnpj_empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "cnpj_empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_admissao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_admissao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cargo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_cargo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_nat_ocupacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_nat_ocupacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_profissao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_profissao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_beneficio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_beneficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_renda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_renda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
