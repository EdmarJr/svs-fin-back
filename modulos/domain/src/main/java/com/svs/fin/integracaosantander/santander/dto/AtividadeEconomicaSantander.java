package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AtividadeEconomicaSantander implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7577605152376399731L;

	@Id
	private Long id;
	
	@Column(length=100)
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "id_tipo")
	private TipoAtividadeEconomicaSantander tipo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeEconomicaSantander other = (AtividadeEconomicaSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public TipoAtividadeEconomicaSantander getTipo() {
		return tipo;
	}

	public void setTipo(TipoAtividadeEconomicaSantander tipo) {
		this.tipo = tipo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
