package com.svs.fin.integracaoItau;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.model.entities.OperacaoFinanciada;

import br.com.gruposaga.sdk.zflow.model.gen.SimulationOperationResponse;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = FinanciamentoOnlineItau.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME, query = FinanciamentoOnlineItau.NQ_OBTER_POR_OPERACAO_FINANCIADA.JPQL) })
public class FinanciamentoOnlineItau implements Serializable {

	public static interface NQ_OBTER_POR_OPERACAO_FINANCIADA {
		public static final String NAME = "FinanciamentoOnlineItau.obterPorOperacaoFinanciada";
		public static final String JPQL = "Select pe from FinanciamentoOnlineItau pe where pe.operacaoFinanciada.id = :"
				+ NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA;
		public static final String NM_PM_ID_OPERACAO_FINANCIADA = "idOperacaoFinanciada";
	}

	private static final long serialVersionUID = 4221367206612008606L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Column(length = 50)
	private String idOnline;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_operacao")
	@JsonProperty(access = Access.WRITE_ONLY)
	private OperacaoFinanciada operacaoFinanciada;

	@Transient
	private Long idOperacaoFinanciada;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_transac_req_itau")
	private JsonTransactionRequestItau jsonTransactionRequestItau;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_sim_resp_itau")
	private JsonSimulationOperationResponseItau jsonSimulationOperationResponseItau;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_credit_an_itau")
	private JsonCreditAnalysisItau jsonCreditAnalysisItau;

	public SimulationOperationResponse getSimulationOperationResponse() {
		return getJsonSimulationOperationResponseItau().getSimulationOperationResponse();
	}

	public JsonTransactionRequestItau getJsonTransactionRequestItau() {
		if (jsonTransactionRequestItau == null) {
			jsonTransactionRequestItau = new JsonTransactionRequestItau();
		}
		return jsonTransactionRequestItau;
	}

	public void setJsonTransactionRequestItau(JsonTransactionRequestItau jsonTransactionRequestItau) {
		this.jsonTransactionRequestItau = jsonTransactionRequestItau;
	}

	public JsonSimulationOperationResponseItau getJsonSimulationOperationResponseItau() {
		if (jsonSimulationOperationResponseItau == null) {
			jsonSimulationOperationResponseItau = new JsonSimulationOperationResponseItau();
		}
		return jsonSimulationOperationResponseItau;
	}

	public void setJsonSimulationOperationResponseItau(
			JsonSimulationOperationResponseItau jsonSimulationOperationResponseItau) {
		this.jsonSimulationOperationResponseItau = jsonSimulationOperationResponseItau;
	}

	public JsonCreditAnalysisItau getJsonCreditAnalysisItau() {
		if (jsonCreditAnalysisItau == null) {
			jsonCreditAnalysisItau = new JsonCreditAnalysisItau();
		}
		return jsonCreditAnalysisItau;
	}

	public void setJsonCreditAnalysisItau(JsonCreditAnalysisItau jsonCreditAnalysisItau) {
		this.jsonCreditAnalysisItau = jsonCreditAnalysisItau;
	}

	public OperacaoFinanciada getOperacaoFinanciada() {
		return operacaoFinanciada;
	}

	public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getIdOnline() {
		return idOnline;
	}

	public void setIdOnline(String idOnline) {
		this.idOnline = idOnline;
	}

	public Long getIdOperacaoFinanciada() {
		if (idOperacaoFinanciada != null) {
			return idOperacaoFinanciada;
		} else {
			setIdOperacaoFinanciada(this.operacaoFinanciada.getId());
			
			return this.operacaoFinanciada.getId();
		}
	}

	public void setIdOperacaoFinanciada(Long idOperacaoFinanciada) {
		this.idOperacaoFinanciada = idOperacaoFinanciada;
	}

}
