package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.svs.fin.model.entities.interfaces.Identificavel;

@Entity(name = "TAXA_PERFIL_IF")
public class TaxaPerfilInstituicaoFinanceira implements Serializable, Identificavel<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4997065905091518001L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "instituicao_financeira_id")
	private InstituicaoFinanceira instituicaoFinanceira;

	@ManyToOne
	@JoinColumn(name = "perfil_svs_id")
	private Perfil perfil;

	@Column
	private BigDecimal taxaInicial;

	@Column
	private BigDecimal taxaFinal;

	public TaxaPerfilInstituicaoFinanceira() {
	}

	public TaxaPerfilInstituicaoFinanceira(InstituicaoFinanceira instituicaoFinanceira) {
		this.instituicaoFinanceira = instituicaoFinanceira;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InstituicaoFinanceira getInstituicaoFinanceira() {
		return instituicaoFinanceira;
	}

	public void setInstituicaoFinanceira(InstituicaoFinanceira instituicaoFinanceira) {
		this.instituicaoFinanceira = instituicaoFinanceira;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public BigDecimal getTaxaInicial() {
		return taxaInicial;
	}

	public void setTaxaInicial(BigDecimal taxaInicial) {
		this.taxaInicial = taxaInicial;
	}

	public BigDecimal getTaxaFinal() {
		return taxaFinal;
	}

	public void setTaxaFinal(BigDecimal taxaFinal) {
		this.taxaFinal = taxaFinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxaPerfilInstituicaoFinanceira other = (TaxaPerfilInstituicaoFinanceira) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
