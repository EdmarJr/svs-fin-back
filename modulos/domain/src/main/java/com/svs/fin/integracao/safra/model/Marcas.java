/**
 * Marcas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Marcas  implements java.io.Serializable {
    private java.lang.String dsMarca;

    private java.lang.Integer idMarca;

    public Marcas() {
    }

    public Marcas(
           java.lang.String dsMarca,
           java.lang.Integer idMarca) {
           this.dsMarca = dsMarca;
           this.idMarca = idMarca;
    }


    /**
     * Gets the dsMarca value for this Marcas.
     * 
     * @return dsMarca
     */
    public java.lang.String getDsMarca() {
        return dsMarca;
    }


    /**
     * Sets the dsMarca value for this Marcas.
     * 
     * @param dsMarca
     */
    public void setDsMarca(java.lang.String dsMarca) {
        this.dsMarca = dsMarca;
    }


    /**
     * Gets the idMarca value for this Marcas.
     * 
     * @return idMarca
     */
    public java.lang.Integer getIdMarca() {
        return idMarca;
    }


    /**
     * Sets the idMarca value for this Marcas.
     * 
     * @param idMarca
     */
    public void setIdMarca(java.lang.Integer idMarca) {
        this.idMarca = idMarca;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Marcas)) return false;
        Marcas other = (Marcas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dsMarca==null && other.getDsMarca()==null) || 
             (this.dsMarca!=null &&
              this.dsMarca.equals(other.getDsMarca()))) &&
            ((this.idMarca==null && other.getIdMarca()==null) || 
             (this.idMarca!=null &&
              this.idMarca.equals(other.getIdMarca())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDsMarca() != null) {
            _hashCode += getDsMarca().hashCode();
        }
        if (getIdMarca() != null) {
            _hashCode += getIdMarca().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Marcas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsMarca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsMarca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMarca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "IdMarca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
