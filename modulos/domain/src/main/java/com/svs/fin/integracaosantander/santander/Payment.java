package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Payment implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6002677019907717791L;

	private String digit;

	private String installmentAmount;

	private String firstPaymentDate;

	private String downPayment;

	private String agency;

	private String account;

	private String paymentFormId;

	private String modality;

	private String coupon;

	private String totalValue;

	private String packageNumber;
	

	public String getDigit() {
		return digit;
	}

	public void setDigit(String digit) {
		this.digit = digit;
	}

	public String getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public Calendar getFirstPaymentDateCalendar() {
		
		if(firstPaymentDate != null && !firstPaymentDate.trim().equals("")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			try {
				Date data = sdf.parse(firstPaymentDate);
				Calendar dataCalendar = Calendar.getInstance();
				dataCalendar.setTime(data);
				return dataCalendar;
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public String getFirstPaymentDate() {
		return firstPaymentDate;
	}

	public void setFirstPaymentDate(String firstPaymentDate) {
		this.firstPaymentDate = firstPaymentDate;
	}

	public String getDownPayment() {
		return downPayment;
	}

	public void setDownPayment(String downPayment) {
		this.downPayment = downPayment;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPaymentFormId() {
		return paymentFormId;
	}

	public void setPaymentFormId(String paymentFormId) {
		this.paymentFormId = paymentFormId;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public String getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	public String getPackageNumber() {
		return packageNumber;
	}

	public void setPackageNumber(String packageNumber) {
		this.packageNumber = packageNumber;
	}

	@Override
	public String toString() {
		return "ClassPojo [digit = " + digit + ", installmentAmount = " + installmentAmount + ", firstPaymentDate = "
				+ firstPaymentDate + ", downPayment = " + downPayment + ", agency = " + agency + ", account = "
				+ account + ", paymentFormId = " + paymentFormId + ", modality = " + modality + ", coupon = " + coupon
				+ ", totalValue = " + totalValue + ", packageNumber = " + packageNumber + "]";
	}
}
