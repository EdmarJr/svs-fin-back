package com.svs.fin.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.svs.fin.enums.EnumBancoIntegracaoOnline;
import com.svs.fin.model.entities.interfaces.AuditavelResponsabilizacao;
import com.svs.fin.model.entities.interfaces.AuditavelTemporalmente;
import com.svs.fin.model.entities.interfaces.Desativavel;
import com.svs.fin.model.entities.interfaces.Identificavel;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisResponsabilizacao;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisTemporalmente;

@Entity
@Table(name = "masc_taxa_tabela_fin")
@EntityListeners({ ListenerEntidadesAuditaveisResponsabilizacao.class, ListenerEntidadesAuditaveisTemporalmente.class })
@NamedQueries({ @NamedQuery(name = MascaraTaxaTabelaFinanceira.NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NAME, query = MascaraTaxaTabelaFinanceira.NQ_OBTER_POR_MASCARA_RETORNO_BANCO.JPQL) })
public class MascaraTaxaTabelaFinanceira extends Entidade<Long> implements Desativavel, Identificavel<Long>, AuditavelResponsabilizacao, AuditavelTemporalmente {
	
	public static interface NQ_OBTER_POR_MASCARA_RETORNO_BANCO {
		public static final String NAME = "MascaraTaxaTabelaFinanceira.obterPorMascaraRetornoBanco";
		public static final String JPQL = "Select DISTINCT m from MascaraTaxaTabelaFinanceira m where m.mascara = :" + NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NM_PM_MASCARA
				+ " AND m.codigoRetorno = :" + NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NM_PM_CODIGO_RETORNO
				+ " AND m.bancoIntegracaoOnline = :" + NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NM_PM_BANCO;
				
		public static final String NM_PM_MASCARA = "mascara";
		public static final String NM_PM_CODIGO_RETORNO = "codigoRetorno";
		public static final String NM_PM_BANCO = "bancoIntegracaoOnline";
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_masc_taxa_tabela_fin")
	@SequenceGenerator(name = "seq_masc_taxa_tabela_fin", sequenceName = "seq_masc_taxa_tabela_fin", allocationSize = 1)
	private Long id;
	@Column(name = "ativa")
	private Boolean ativo;

	@Column(name = "codigoretorno")
	private String codigoRetorno;

	@Column(name = "bancointegracaoonline")
	@Enumerated(EnumType.STRING)
	private EnumBancoIntegracaoOnline bancoIntegracaoOnline;

	@Column(name = "mascara")
	private String mascara;

	@Column(name = "ordem")
	private Integer ordem;

	@Column(name = "percentualretorno")
	private Double percentualRetorno;

	@Column(name = "alteradoem")
	private LocalDateTime dataHoraDeAtualizacao;
	@Column(name = "criadoem")
	private LocalDateTime dataHoraDeCriacao;

	@ManyToOne
	@JoinColumn(name = "id_usuario_criacao_alteracao")
	private Usuario usuarioResponsavelUltimaAlteracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public LocalDateTime getDataHoraDeAtualizacao() {
		return dataHoraDeAtualizacao;
	}

	public void setDataHoraDeAtualizacao(LocalDateTime dataHoraDeAtualizacao) {
		this.dataHoraDeAtualizacao = dataHoraDeAtualizacao;
	}

	public LocalDateTime getDataHoraDeCriacao() {
		return dataHoraDeCriacao;
	}

	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao) {
		this.dataHoraDeCriacao = dataHoraDeCriacao;
	}

	public Usuario getUsuarioResponsavelUltimaAlteracao() {
		return usuarioResponsavelUltimaAlteracao;
	}

	public void setUsuarioResponsavelUltimaAlteracao(Usuario usuarioResponsavelUltimaAlteracao) {
		this.usuarioResponsavelUltimaAlteracao = usuarioResponsavelUltimaAlteracao;
	}

	public String getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}


	public EnumBancoIntegracaoOnline getBancoIntegracaoOnline() {
		return bancoIntegracaoOnline;
	}

	public void setBancoIntegracaoOnline(EnumBancoIntegracaoOnline bancoIntegracaoOnline) {
		this.bancoIntegracaoOnline = bancoIntegracaoOnline;
	}

	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Double getPercentualRetorno() {
		return percentualRetorno;
	}

	public void setPercentualRetorno(Double percentualRetorno) {
		this.percentualRetorno = percentualRetorno;
	}

}
