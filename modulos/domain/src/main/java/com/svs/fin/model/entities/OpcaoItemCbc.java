package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQueries({
		@NamedQuery(name = OpcaoItemCbc.NQ_OBTER_POR_ID_UNIDADE_ORGANIZACIONAL.NAME, query = OpcaoItemCbc.NQ_OBTER_POR_ID_UNIDADE_ORGANIZACIONAL.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpcaoItemCbc implements Serializable {

	public static interface NQ_OBTER_POR_ID_UNIDADE_ORGANIZACIONAL {
		public static final String NAME = "OpcaoItemCbc.obterPorIdUnidadeOrganizacional";
		public static final String JPQL = "Select o from OpcaoItemCbc o where o.unidadeOrganizacional.id = :"
				+ NQ_OBTER_POR_ID_UNIDADE_ORGANIZACIONAL.NM_PM_ID_UNIDADE_ORGANIZACIONAL;
		public static final String NM_PM_ID_UNIDADE_ORGANIZACIONAL = "idUnidadeOrganizacional";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -268764427111283003L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OPCAOITEMCBC")
	@SequenceGenerator(name = "SEQ_OPCAOITEMCBC", sequenceName = "SEQ_OPCAOITEMCBC", allocationSize = 1)
	private Long id;

	@Column
	private Long codigoDealerWorkflow;

	@Column(length = 500)
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "id_unidade_organizacional")
	private UnidadeOrganizacional unidadeOrganizacional;

	@Column
	private Boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(Long codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public UnidadeOrganizacional getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OpcaoItemCbc other = (OpcaoItemCbc) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
