/**
 * Lojista.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Lojista")
public class Lojista implements java.io.Serializable {
	private java.lang.String ds_abreviatura;

	private java.lang.String ds_cnpj;

	private java.lang.String ds_nome;

	@Id
	private java.lang.String id_codigo;

	private java.lang.Double vl_tc_cdc;

	private java.lang.Double vl_tc_leasing;

	public Lojista() {
	}

	public Lojista(java.lang.String ds_abreviatura, java.lang.String ds_cnpj, java.lang.String ds_nome,
			java.lang.String id_codigo, java.lang.Double vl_tc_cdc, java.lang.Double vl_tc_leasing) {
		this.ds_abreviatura = ds_abreviatura;
		this.ds_cnpj = ds_cnpj;
		this.ds_nome = ds_nome;
		this.id_codigo = id_codigo;
		this.vl_tc_cdc = vl_tc_cdc;
		this.vl_tc_leasing = vl_tc_leasing;
	}

	/**
	 * Gets the ds_abreviatura value for this Lojista.
	 * 
	 * @return ds_abreviatura
	 */
	public java.lang.String getDs_abreviatura() {
		return ds_abreviatura;
	}

	/**
	 * Sets the ds_abreviatura value for this Lojista.
	 * 
	 * @param ds_abreviatura
	 */
	public void setDs_abreviatura(java.lang.String ds_abreviatura) {
		this.ds_abreviatura = ds_abreviatura;
	}

	/**
	 * Gets the ds_cnpj value for this Lojista.
	 * 
	 * @return ds_cnpj
	 */
	public java.lang.String getDs_cnpj() {
		return ds_cnpj;
	}

	/**
	 * Sets the ds_cnpj value for this Lojista.
	 * 
	 * @param ds_cnpj
	 */
	public void setDs_cnpj(java.lang.String ds_cnpj) {
		this.ds_cnpj = ds_cnpj;
	}

	/**
	 * Gets the ds_nome value for this Lojista.
	 * 
	 * @return ds_nome
	 */
	public java.lang.String getDs_nome() {
		return ds_nome;
	}

	/**
	 * Sets the ds_nome value for this Lojista.
	 * 
	 * @param ds_nome
	 */
	public void setDs_nome(java.lang.String ds_nome) {
		this.ds_nome = ds_nome;
	}

	/**
	 * Gets the id_codigo value for this Lojista.
	 * 
	 * @return id_codigo
	 */
	public java.lang.String getId_codigo() {
		return id_codigo;
	}

	/**
	 * Sets the id_codigo value for this Lojista.
	 * 
	 * @param id_codigo
	 */
	public void setId_codigo(java.lang.String id_codigo) {
		this.id_codigo = id_codigo;
	}

	/**
	 * Gets the vl_tc_cdc value for this Lojista.
	 * 
	 * @return vl_tc_cdc
	 */
	public java.lang.Double getVl_tc_cdc() {
		return vl_tc_cdc;
	}

	/**
	 * Sets the vl_tc_cdc value for this Lojista.
	 * 
	 * @param vl_tc_cdc
	 */
	public void setVl_tc_cdc(java.lang.Double vl_tc_cdc) {
		this.vl_tc_cdc = vl_tc_cdc;
	}

	/**
	 * Gets the vl_tc_leasing value for this Lojista.
	 * 
	 * @return vl_tc_leasing
	 */
	public java.lang.Double getVl_tc_leasing() {
		return vl_tc_leasing;
	}

	/**
	 * Sets the vl_tc_leasing value for this Lojista.
	 * 
	 * @param vl_tc_leasing
	 */
	public void setVl_tc_leasing(java.lang.Double vl_tc_leasing) {
		this.vl_tc_leasing = vl_tc_leasing;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Lojista))
			return false;
		Lojista other = (Lojista) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_abreviatura == null && other.getDs_abreviatura() == null)
						|| (this.ds_abreviatura != null && this.ds_abreviatura.equals(other.getDs_abreviatura())))
				&& ((this.ds_cnpj == null && other.getDs_cnpj() == null)
						|| (this.ds_cnpj != null && this.ds_cnpj.equals(other.getDs_cnpj())))
				&& ((this.ds_nome == null && other.getDs_nome() == null)
						|| (this.ds_nome != null && this.ds_nome.equals(other.getDs_nome())))
				&& ((this.id_codigo == null && other.getId_codigo() == null)
						|| (this.id_codigo != null && this.id_codigo.equals(other.getId_codigo())))
				&& ((this.vl_tc_cdc == null && other.getVl_tc_cdc() == null)
						|| (this.vl_tc_cdc != null && this.vl_tc_cdc.equals(other.getVl_tc_cdc())))
				&& ((this.vl_tc_leasing == null && other.getVl_tc_leasing() == null)
						|| (this.vl_tc_leasing != null && this.vl_tc_leasing.equals(other.getVl_tc_leasing())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_abreviatura() != null) {
			_hashCode += getDs_abreviatura().hashCode();
		}
		if (getDs_cnpj() != null) {
			_hashCode += getDs_cnpj().hashCode();
		}
		if (getDs_nome() != null) {
			_hashCode += getDs_nome().hashCode();
		}
		if (getId_codigo() != null) {
			_hashCode += getId_codigo().hashCode();
		}
		if (getVl_tc_cdc() != null) {
			_hashCode += getVl_tc_cdc().hashCode();
		}
		if (getVl_tc_leasing() != null) {
			_hashCode += getVl_tc_leasing().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Lojista.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Lojista"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_abreviatura");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_abreviatura"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_cnpj");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_cnpj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_nome");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_nome"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_codigo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_codigo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_tc_cdc");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tc_cdc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_tc_leasing");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tc_leasing"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
