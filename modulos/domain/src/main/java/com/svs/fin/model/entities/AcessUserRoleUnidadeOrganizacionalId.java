package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AcessUserRoleUnidadeOrganizacionalId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column
	public Long acessUserId;
	@Column
	public Long unidadeOrganizacionalId;
	@Column
	public Long roleId;

	public AcessUserRoleUnidadeOrganizacionalId(Long acessUserId, Long unidadeOrganizacionalId, Long roleId) {
	}

	public AcessUserRoleUnidadeOrganizacionalId() {
	}

	public Long getAcessUserId() {
		return acessUserId;
	}

	public void setAcessUserId(Long acessUserId) {
		this.acessUserId = acessUserId;
	}

	public Long getUnidadeOrganizacionalId() {
		return unidadeOrganizacionalId;
	}

	public void setUnidadeOrganizacionalId(Long unidadeOrganizacionalId) {
		this.unidadeOrganizacionalId = unidadeOrganizacionalId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acessUserId == null) ? 0 : acessUserId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((unidadeOrganizacionalId == null) ? 0 : unidadeOrganizacionalId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcessUserRoleUnidadeOrganizacionalId other = (AcessUserRoleUnidadeOrganizacionalId) obj;
		if (acessUserId == null) {
			if (other.acessUserId != null)
				return false;
		} else if (!acessUserId.equals(other.acessUserId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (unidadeOrganizacionalId == null) {
			if (other.unidadeOrganizacionalId != null)
				return false;
		} else if (!unidadeOrganizacionalId.equals(other.unidadeOrganizacionalId))
			return false;
		return true;
	}

}
