/**
 * Isencao_Fiscal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Isencao_Fiscal")
public class Isencao_Fiscal implements java.io.Serializable {
	private java.lang.String ds_isencao_fiscal;

	@Id
	private java.lang.Integer id_isencao_fiscal;

	public Isencao_Fiscal() {
	}

	public Isencao_Fiscal(java.lang.String ds_isencao_fiscal, java.lang.Integer id_isencao_fiscal) {
		this.ds_isencao_fiscal = ds_isencao_fiscal;
		this.id_isencao_fiscal = id_isencao_fiscal;
	}

	/**
	 * Gets the ds_isencao_fiscal value for this Isencao_Fiscal.
	 * 
	 * @return ds_isencao_fiscal
	 */
	public java.lang.String getDs_isencao_fiscal() {
		return ds_isencao_fiscal;
	}

	/**
	 * Sets the ds_isencao_fiscal value for this Isencao_Fiscal.
	 * 
	 * @param ds_isencao_fiscal
	 */
	public void setDs_isencao_fiscal(java.lang.String ds_isencao_fiscal) {
		this.ds_isencao_fiscal = ds_isencao_fiscal;
	}

	/**
	 * Gets the id_isencao_fiscal value for this Isencao_Fiscal.
	 * 
	 * @return id_isencao_fiscal
	 */
	public java.lang.Integer getId_isencao_fiscal() {
		return id_isencao_fiscal;
	}

	/**
	 * Sets the id_isencao_fiscal value for this Isencao_Fiscal.
	 * 
	 * @param id_isencao_fiscal
	 */
	public void setId_isencao_fiscal(java.lang.Integer id_isencao_fiscal) {
		this.id_isencao_fiscal = id_isencao_fiscal;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Isencao_Fiscal))
			return false;
		Isencao_Fiscal other = (Isencao_Fiscal) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.ds_isencao_fiscal == null && other.getDs_isencao_fiscal() == null)
				|| (this.ds_isencao_fiscal != null && this.ds_isencao_fiscal.equals(other.getDs_isencao_fiscal())))
				&& ((this.id_isencao_fiscal == null && other.getId_isencao_fiscal() == null)
						|| (this.id_isencao_fiscal != null
								&& this.id_isencao_fiscal.equals(other.getId_isencao_fiscal())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_isencao_fiscal() != null) {
			_hashCode += getDs_isencao_fiscal().hashCode();
		}
		if (getId_isencao_fiscal() != null) {
			_hashCode += getId_isencao_fiscal().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Isencao_Fiscal.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Isencao_Fiscal"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_isencao_fiscal");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_isencao_fiscal"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_isencao_fiscal");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_isencao_fiscal"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
