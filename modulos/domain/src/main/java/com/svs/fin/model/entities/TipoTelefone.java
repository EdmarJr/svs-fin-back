package com.svs.fin.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.model.entities.interfaces.Desativavel;
import com.svs.fin.model.entities.interfaces.Identificavel;

@Entity
@Table(name = "tipotelefone")
public class TipoTelefone implements Desativavel, Identificavel<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPO_TELEFONE")
	@SequenceGenerator(name = "SEQ_TIPO_TELEFONE", sequenceName = "SEQ_TIPO_TELEFONE", allocationSize = 1)
	private Long id;
	@Column(name = "ativo")
	private Boolean ativo;
	@Column(name = "codigodealerworkflow")
	private Long codigoDealerWorklow;
	@Column(name = "codigosisdia")
	private Long codigoSISDIA;
	@Column(name = "descricao")
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getCodigoDealerWorklow() {
		return codigoDealerWorklow;
	}

	public void setCodigoDealerWorklow(Long codigoDealerWorklow) {
		this.codigoDealerWorklow = codigoDealerWorklow;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigoSISDIA() {
		return codigoSISDIA;
	}

	public void setCodigoSISDIA(Long codigoSISDIA) {
		this.codigoSISDIA = codigoSISDIA;
	}

}
