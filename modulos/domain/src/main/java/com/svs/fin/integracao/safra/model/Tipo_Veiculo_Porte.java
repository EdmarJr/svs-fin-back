/**
 * Tipo_Veiculo_Porte.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Veiculo_Porte")
public class Tipo_Veiculo_Porte implements java.io.Serializable {
	private java.lang.String ds_cd_porte;

	private java.lang.String ds_veiculo_porte;

	@Id
	private java.lang.String id_veiculo_porte;

	public Tipo_Veiculo_Porte() {
	}

	public Tipo_Veiculo_Porte(java.lang.String ds_cd_porte, java.lang.String ds_veiculo_porte,
			java.lang.String id_veiculo_porte) {
		this.ds_cd_porte = ds_cd_porte;
		this.ds_veiculo_porte = ds_veiculo_porte;
		this.id_veiculo_porte = id_veiculo_porte;
	}

	/**
	 * Gets the ds_cd_porte value for this Tipo_Veiculo_Porte.
	 * 
	 * @return ds_cd_porte
	 */
	public java.lang.String getDs_cd_porte() {
		return ds_cd_porte;
	}

	/**
	 * Sets the ds_cd_porte value for this Tipo_Veiculo_Porte.
	 * 
	 * @param ds_cd_porte
	 */
	public void setDs_cd_porte(java.lang.String ds_cd_porte) {
		this.ds_cd_porte = ds_cd_porte;
	}

	/**
	 * Gets the ds_veiculo_porte value for this Tipo_Veiculo_Porte.
	 * 
	 * @return ds_veiculo_porte
	 */
	public java.lang.String getDs_veiculo_porte() {
		return ds_veiculo_porte;
	}

	/**
	 * Sets the ds_veiculo_porte value for this Tipo_Veiculo_Porte.
	 * 
	 * @param ds_veiculo_porte
	 */
	public void setDs_veiculo_porte(java.lang.String ds_veiculo_porte) {
		this.ds_veiculo_porte = ds_veiculo_porte;
	}

	/**
	 * Gets the id_veiculo_porte value for this Tipo_Veiculo_Porte.
	 * 
	 * @return id_veiculo_porte
	 */
	public java.lang.String getId_veiculo_porte() {
		return id_veiculo_porte;
	}

	/**
	 * Sets the id_veiculo_porte value for this Tipo_Veiculo_Porte.
	 * 
	 * @param id_veiculo_porte
	 */
	public void setId_veiculo_porte(java.lang.String id_veiculo_porte) {
		this.id_veiculo_porte = id_veiculo_porte;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Veiculo_Porte))
			return false;
		Tipo_Veiculo_Porte other = (Tipo_Veiculo_Porte) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_cd_porte == null && other.getDs_cd_porte() == null)
						|| (this.ds_cd_porte != null && this.ds_cd_porte.equals(other.getDs_cd_porte())))
				&& ((this.ds_veiculo_porte == null && other.getDs_veiculo_porte() == null)
						|| (this.ds_veiculo_porte != null && this.ds_veiculo_porte.equals(other.getDs_veiculo_porte())))
				&& ((this.id_veiculo_porte == null && other.getId_veiculo_porte() == null)
						|| (this.id_veiculo_porte != null
								&& this.id_veiculo_porte.equals(other.getId_veiculo_porte())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_cd_porte() != null) {
			_hashCode += getDs_cd_porte().hashCode();
		}
		if (getDs_veiculo_porte() != null) {
			_hashCode += getDs_veiculo_porte().hashCode();
		}
		if (getId_veiculo_porte() != null) {
			_hashCode += getId_veiculo_porte().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Veiculo_Porte.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"Tipo_Veiculo_Porte"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_cd_porte");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_cd_porte"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_veiculo_porte");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_veiculo_porte"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_veiculo_porte");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_veiculo_porte"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
