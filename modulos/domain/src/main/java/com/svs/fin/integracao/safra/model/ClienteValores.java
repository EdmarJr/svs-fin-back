/**
 * ClienteValores.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class ClienteValores  implements java.io.Serializable {
    private java.lang.String fl_isencao_tc;

    private java.lang.String id_cliente;

    private java.lang.Double vl_registro;

    private java.lang.Double vl_tc;

    public ClienteValores() {
    }

    public ClienteValores(
           java.lang.String fl_isencao_tc,
           java.lang.String id_cliente,
           java.lang.Double vl_registro,
           java.lang.Double vl_tc) {
           this.fl_isencao_tc = fl_isencao_tc;
           this.id_cliente = id_cliente;
           this.vl_registro = vl_registro;
           this.vl_tc = vl_tc;
    }


    /**
     * Gets the fl_isencao_tc value for this ClienteValores.
     * 
     * @return fl_isencao_tc
     */
    public java.lang.String getFl_isencao_tc() {
        return fl_isencao_tc;
    }


    /**
     * Sets the fl_isencao_tc value for this ClienteValores.
     * 
     * @param fl_isencao_tc
     */
    public void setFl_isencao_tc(java.lang.String fl_isencao_tc) {
        this.fl_isencao_tc = fl_isencao_tc;
    }


    /**
     * Gets the id_cliente value for this ClienteValores.
     * 
     * @return id_cliente
     */
    public java.lang.String getId_cliente() {
        return id_cliente;
    }


    /**
     * Sets the id_cliente value for this ClienteValores.
     * 
     * @param id_cliente
     */
    public void setId_cliente(java.lang.String id_cliente) {
        this.id_cliente = id_cliente;
    }


    /**
     * Gets the vl_registro value for this ClienteValores.
     * 
     * @return vl_registro
     */
    public java.lang.Double getVl_registro() {
        return vl_registro;
    }


    /**
     * Sets the vl_registro value for this ClienteValores.
     * 
     * @param vl_registro
     */
    public void setVl_registro(java.lang.Double vl_registro) {
        this.vl_registro = vl_registro;
    }


    /**
     * Gets the vl_tc value for this ClienteValores.
     * 
     * @return vl_tc
     */
    public java.lang.Double getVl_tc() {
        return vl_tc;
    }


    /**
     * Sets the vl_tc value for this ClienteValores.
     * 
     * @param vl_tc
     */
    public void setVl_tc(java.lang.Double vl_tc) {
        this.vl_tc = vl_tc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClienteValores)) return false;
        ClienteValores other = (ClienteValores) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fl_isencao_tc==null && other.getFl_isencao_tc()==null) || 
             (this.fl_isencao_tc!=null &&
              this.fl_isencao_tc.equals(other.getFl_isencao_tc()))) &&
            ((this.id_cliente==null && other.getId_cliente()==null) || 
             (this.id_cliente!=null &&
              this.id_cliente.equals(other.getId_cliente()))) &&
            ((this.vl_registro==null && other.getVl_registro()==null) || 
             (this.vl_registro!=null &&
              this.vl_registro.equals(other.getVl_registro()))) &&
            ((this.vl_tc==null && other.getVl_tc()==null) || 
             (this.vl_tc!=null &&
              this.vl_tc.equals(other.getVl_tc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFl_isencao_tc() != null) {
            _hashCode += getFl_isencao_tc().hashCode();
        }
        if (getId_cliente() != null) {
            _hashCode += getId_cliente().hashCode();
        }
        if (getVl_registro() != null) {
            _hashCode += getVl_registro().hashCode();
        }
        if (getVl_tc() != null) {
            _hashCode += getVl_tc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClienteValores.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ClienteValores"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fl_isencao_tc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fl_isencao_tc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_registro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_registro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_tc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
