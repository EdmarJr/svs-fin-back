/**
 * Nacionalidade.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Nacionalidade")
public class Nacionalidade implements java.io.Serializable {
	private java.lang.String ds_nacionalidade;

	private java.lang.String fl_default;

	@Id
	private java.lang.Integer id_nacionalidade;

	public Nacionalidade() {
	}

	public Nacionalidade(java.lang.String ds_nacionalidade, java.lang.String fl_default,
			java.lang.Integer id_nacionalidade) {
		this.ds_nacionalidade = ds_nacionalidade;
		this.fl_default = fl_default;
		this.id_nacionalidade = id_nacionalidade;
	}

	/**
	 * Gets the ds_nacionalidade value for this Nacionalidade.
	 * 
	 * @return ds_nacionalidade
	 */
	public java.lang.String getDs_nacionalidade() {
		return ds_nacionalidade;
	}

	/**
	 * Sets the ds_nacionalidade value for this Nacionalidade.
	 * 
	 * @param ds_nacionalidade
	 */
	public void setDs_nacionalidade(java.lang.String ds_nacionalidade) {
		this.ds_nacionalidade = ds_nacionalidade;
	}

	/**
	 * Gets the fl_default value for this Nacionalidade.
	 * 
	 * @return fl_default
	 */
	public java.lang.String getFl_default() {
		return fl_default;
	}

	/**
	 * Sets the fl_default value for this Nacionalidade.
	 * 
	 * @param fl_default
	 */
	public void setFl_default(java.lang.String fl_default) {
		this.fl_default = fl_default;
	}

	/**
	 * Gets the id_nacionalidade value for this Nacionalidade.
	 * 
	 * @return id_nacionalidade
	 */
	public java.lang.Integer getId_nacionalidade() {
		return id_nacionalidade;
	}

	/**
	 * Sets the id_nacionalidade value for this Nacionalidade.
	 * 
	 * @param id_nacionalidade
	 */
	public void setId_nacionalidade(java.lang.Integer id_nacionalidade) {
		this.id_nacionalidade = id_nacionalidade;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Nacionalidade))
			return false;
		Nacionalidade other = (Nacionalidade) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_nacionalidade == null && other.getDs_nacionalidade() == null)
						|| (this.ds_nacionalidade != null && this.ds_nacionalidade.equals(other.getDs_nacionalidade())))
				&& ((this.fl_default == null && other.getFl_default() == null)
						|| (this.fl_default != null && this.fl_default.equals(other.getFl_default())))
				&& ((this.id_nacionalidade == null && other.getId_nacionalidade() == null)
						|| (this.id_nacionalidade != null
								&& this.id_nacionalidade.equals(other.getId_nacionalidade())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_nacionalidade() != null) {
			_hashCode += getDs_nacionalidade().hashCode();
		}
		if (getFl_default() != null) {
			_hashCode += getFl_default().hashCode();
		}
		if (getId_nacionalidade() != null) {
			_hashCode += getId_nacionalidade().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Nacionalidade.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Nacionalidade"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_nacionalidade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_nacionalidade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("fl_default");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fl_default"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_nacionalidade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_nacionalidade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
