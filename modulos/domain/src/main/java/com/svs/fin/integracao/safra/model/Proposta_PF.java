/**
 * Proposta_PF.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Proposta_PF  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model_comum.Bem_Veiculo[] bem_veiculo;

    private com.svs.fin.integracao.safra.model_comum.Cnh cnh;

    private com.svs.fin.integracao.safra.model_comum.Conjuge_Dados_Basicos conjuge_dados_basicos;

    private com.svs.fin.integracao.safra.model_comum.Dados_Profissionais dados_profissionais;

    private com.svs.fin.integracao.safra.model_comum.Documento documento;

    private com.svs.fin.integracao.safra.model_comum.Empresa_Anterior empresa_anterior;

    private com.svs.fin.integracao.safra.model_comum.Filiacao filiacao;

    private com.svs.fin.integracao.safra.model_dados_financiamento.Isencoes isencao_fiscal;

    private com.svs.fin.integracao.safra.model_comum.Local_Nascimento local_nascimento;

    private com.svs.fin.integracao.safra.model_comum.Outros_Dados outros_dados;

    private com.svs.fin.integracao.safra.model_comum.Outros_Dados_Residencia outros_dados_residencia;

    private java.lang.Boolean permiteRenda;

    private com.svs.fin.integracao.safra.model_comum.Ppe ppe;

    private com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal[] referencia_pessoal;

    private com.svs.fin.integracao.safra.model_comum.Dados_Profissionais renda_adicional;

    public Proposta_PF() {
    }

    public Proposta_PF(
           com.svs.fin.integracao.safra.model_comum.Bem_Veiculo[] bem_veiculo,
           com.svs.fin.integracao.safra.model_comum.Cnh cnh,
           com.svs.fin.integracao.safra.model_comum.Conjuge_Dados_Basicos conjuge_dados_basicos,
           com.svs.fin.integracao.safra.model_comum.Dados_Profissionais dados_profissionais,
           com.svs.fin.integracao.safra.model_comum.Documento documento,
           com.svs.fin.integracao.safra.model_comum.Empresa_Anterior empresa_anterior,
           com.svs.fin.integracao.safra.model_comum.Filiacao filiacao,
           com.svs.fin.integracao.safra.model_dados_financiamento.Isencoes isencao_fiscal,
           com.svs.fin.integracao.safra.model_comum.Local_Nascimento local_nascimento,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados outros_dados,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados_Residencia outros_dados_residencia,
           java.lang.Boolean permiteRenda,
           com.svs.fin.integracao.safra.model_comum.Ppe ppe,
           com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal[] referencia_pessoal,
           com.svs.fin.integracao.safra.model_comum.Dados_Profissionais renda_adicional) {
        this.bem_veiculo = bem_veiculo;
        this.cnh = cnh;
        this.conjuge_dados_basicos = conjuge_dados_basicos;
        this.dados_profissionais = dados_profissionais;
        this.documento = documento;
        this.empresa_anterior = empresa_anterior;
        this.filiacao = filiacao;
        this.isencao_fiscal = isencao_fiscal;
        this.local_nascimento = local_nascimento;
        this.outros_dados = outros_dados;
        this.outros_dados_residencia = outros_dados_residencia;
        this.permiteRenda = permiteRenda;
        this.ppe = ppe;
        this.referencia_pessoal = referencia_pessoal;
        this.renda_adicional = renda_adicional;
    }


    /**
     * Gets the bem_veiculo value for this Proposta_PF.
     * 
     * @return bem_veiculo
     */
    public com.svs.fin.integracao.safra.model_comum.Bem_Veiculo[] getBem_veiculo() {
        return bem_veiculo;
    }


    /**
     * Sets the bem_veiculo value for this Proposta_PF.
     * 
     * @param bem_veiculo
     */
    public void setBem_veiculo(com.svs.fin.integracao.safra.model_comum.Bem_Veiculo[] bem_veiculo) {
        this.bem_veiculo = bem_veiculo;
    }


    /**
     * Gets the cnh value for this Proposta_PF.
     * 
     * @return cnh
     */
    public com.svs.fin.integracao.safra.model_comum.Cnh getCnh() {
        return cnh;
    }


    /**
     * Sets the cnh value for this Proposta_PF.
     * 
     * @param cnh
     */
    public void setCnh(com.svs.fin.integracao.safra.model_comum.Cnh cnh) {
        this.cnh = cnh;
    }


    /**
     * Gets the conjuge_dados_basicos value for this Proposta_PF.
     * 
     * @return conjuge_dados_basicos
     */
    public com.svs.fin.integracao.safra.model_comum.Conjuge_Dados_Basicos getConjuge_dados_basicos() {
        return conjuge_dados_basicos;
    }


    /**
     * Sets the conjuge_dados_basicos value for this Proposta_PF.
     * 
     * @param conjuge_dados_basicos
     */
    public void setConjuge_dados_basicos(com.svs.fin.integracao.safra.model_comum.Conjuge_Dados_Basicos conjuge_dados_basicos) {
        this.conjuge_dados_basicos = conjuge_dados_basicos;
    }


    /**
     * Gets the dados_profissionais value for this Proposta_PF.
     * 
     * @return dados_profissionais
     */
    public com.svs.fin.integracao.safra.model_comum.Dados_Profissionais getDados_profissionais() {
        return dados_profissionais;
    }


    /**
     * Sets the dados_profissionais value for this Proposta_PF.
     * 
     * @param dados_profissionais
     */
    public void setDados_profissionais(com.svs.fin.integracao.safra.model_comum.Dados_Profissionais dados_profissionais) {
        this.dados_profissionais = dados_profissionais;
    }


    /**
     * Gets the documento value for this Proposta_PF.
     * 
     * @return documento
     */
    public com.svs.fin.integracao.safra.model_comum.Documento getDocumento() {
        return documento;
    }


    /**
     * Sets the documento value for this Proposta_PF.
     * 
     * @param documento
     */
    public void setDocumento(com.svs.fin.integracao.safra.model_comum.Documento documento) {
        this.documento = documento;
    }


    /**
     * Gets the empresa_anterior value for this Proposta_PF.
     * 
     * @return empresa_anterior
     */
    public com.svs.fin.integracao.safra.model_comum.Empresa_Anterior getEmpresa_anterior() {
        return empresa_anterior;
    }


    /**
     * Sets the empresa_anterior value for this Proposta_PF.
     * 
     * @param empresa_anterior
     */
    public void setEmpresa_anterior(com.svs.fin.integracao.safra.model_comum.Empresa_Anterior empresa_anterior) {
        this.empresa_anterior = empresa_anterior;
    }


    /**
     * Gets the filiacao value for this Proposta_PF.
     * 
     * @return filiacao
     */
    public com.svs.fin.integracao.safra.model_comum.Filiacao getFiliacao() {
        return filiacao;
    }


    /**
     * Sets the filiacao value for this Proposta_PF.
     * 
     * @param filiacao
     */
    public void setFiliacao(com.svs.fin.integracao.safra.model_comum.Filiacao filiacao) {
        this.filiacao = filiacao;
    }


    /**
     * Gets the isencao_fiscal value for this Proposta_PF.
     * 
     * @return isencao_fiscal
     */
    public com.svs.fin.integracao.safra.model_dados_financiamento.Isencoes getIsencao_fiscal() {
        return isencao_fiscal;
    }


    /**
     * Sets the isencao_fiscal value for this Proposta_PF.
     * 
     * @param isencao_fiscal
     */
    public void setIsencao_fiscal(com.svs.fin.integracao.safra.model_dados_financiamento.Isencoes isencao_fiscal) {
        this.isencao_fiscal = isencao_fiscal;
    }


    /**
     * Gets the local_nascimento value for this Proposta_PF.
     * 
     * @return local_nascimento
     */
    public com.svs.fin.integracao.safra.model_comum.Local_Nascimento getLocal_nascimento() {
        return local_nascimento;
    }


    /**
     * Sets the local_nascimento value for this Proposta_PF.
     * 
     * @param local_nascimento
     */
    public void setLocal_nascimento(com.svs.fin.integracao.safra.model_comum.Local_Nascimento local_nascimento) {
        this.local_nascimento = local_nascimento;
    }


    /**
     * Gets the outros_dados value for this Proposta_PF.
     * 
     * @return outros_dados
     */
    public com.svs.fin.integracao.safra.model_comum.Outros_Dados getOutros_dados() {
        return outros_dados;
    }


    /**
     * Sets the outros_dados value for this Proposta_PF.
     * 
     * @param outros_dados
     */
    public void setOutros_dados(com.svs.fin.integracao.safra.model_comum.Outros_Dados outros_dados) {
        this.outros_dados = outros_dados;
    }


    /**
     * Gets the outros_dados_residencia value for this Proposta_PF.
     * 
     * @return outros_dados_residencia
     */
    public com.svs.fin.integracao.safra.model_comum.Outros_Dados_Residencia getOutros_dados_residencia() {
        return outros_dados_residencia;
    }


    /**
     * Sets the outros_dados_residencia value for this Proposta_PF.
     * 
     * @param outros_dados_residencia
     */
    public void setOutros_dados_residencia(com.svs.fin.integracao.safra.model_comum.Outros_Dados_Residencia outros_dados_residencia) {
        this.outros_dados_residencia = outros_dados_residencia;
    }


    /**
     * Gets the permiteRenda value for this Proposta_PF.
     * 
     * @return permiteRenda
     */
    public java.lang.Boolean getPermiteRenda() {
        return permiteRenda;
    }


    /**
     * Sets the permiteRenda value for this Proposta_PF.
     * 
     * @param permiteRenda
     */
    public void setPermiteRenda(java.lang.Boolean permiteRenda) {
        this.permiteRenda = permiteRenda;
    }


    /**
     * Gets the ppe value for this Proposta_PF.
     * 
     * @return ppe
     */
    public com.svs.fin.integracao.safra.model_comum.Ppe getPpe() {
        return ppe;
    }


    /**
     * Sets the ppe value for this Proposta_PF.
     * 
     * @param ppe
     */
    public void setPpe(com.svs.fin.integracao.safra.model_comum.Ppe ppe) {
        this.ppe = ppe;
    }


    /**
     * Gets the referencia_pessoal value for this Proposta_PF.
     * 
     * @return referencia_pessoal
     */
    public com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal[] getReferencia_pessoal() {
        return referencia_pessoal;
    }


    /**
     * Sets the referencia_pessoal value for this Proposta_PF.
     * 
     * @param referencia_pessoal
     */
    public void setReferencia_pessoal(com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal[] referencia_pessoal) {
        this.referencia_pessoal = referencia_pessoal;
    }


    /**
     * Gets the renda_adicional value for this Proposta_PF.
     * 
     * @return renda_adicional
     */
    public com.svs.fin.integracao.safra.model_comum.Dados_Profissionais getRenda_adicional() {
        return renda_adicional;
    }


    /**
     * Sets the renda_adicional value for this Proposta_PF.
     * 
     * @param renda_adicional
     */
    public void setRenda_adicional(com.svs.fin.integracao.safra.model_comum.Dados_Profissionais renda_adicional) {
        this.renda_adicional = renda_adicional;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Proposta_PF)) return false;
        Proposta_PF other = (Proposta_PF) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.bem_veiculo==null && other.getBem_veiculo()==null) || 
             (this.bem_veiculo!=null &&
              java.util.Arrays.equals(this.bem_veiculo, other.getBem_veiculo()))) &&
            ((this.cnh==null && other.getCnh()==null) || 
             (this.cnh!=null &&
              this.cnh.equals(other.getCnh()))) &&
            ((this.conjuge_dados_basicos==null && other.getConjuge_dados_basicos()==null) || 
             (this.conjuge_dados_basicos!=null &&
              this.conjuge_dados_basicos.equals(other.getConjuge_dados_basicos()))) &&
            ((this.dados_profissionais==null && other.getDados_profissionais()==null) || 
             (this.dados_profissionais!=null &&
              this.dados_profissionais.equals(other.getDados_profissionais()))) &&
            ((this.documento==null && other.getDocumento()==null) || 
             (this.documento!=null &&
              this.documento.equals(other.getDocumento()))) &&
            ((this.empresa_anterior==null && other.getEmpresa_anterior()==null) || 
             (this.empresa_anterior!=null &&
              this.empresa_anterior.equals(other.getEmpresa_anterior()))) &&
            ((this.filiacao==null && other.getFiliacao()==null) || 
             (this.filiacao!=null &&
              this.filiacao.equals(other.getFiliacao()))) &&
            ((this.isencao_fiscal==null && other.getIsencao_fiscal()==null) || 
             (this.isencao_fiscal!=null &&
              this.isencao_fiscal.equals(other.getIsencao_fiscal()))) &&
            ((this.local_nascimento==null && other.getLocal_nascimento()==null) || 
             (this.local_nascimento!=null &&
              this.local_nascimento.equals(other.getLocal_nascimento()))) &&
            ((this.outros_dados==null && other.getOutros_dados()==null) || 
             (this.outros_dados!=null &&
              this.outros_dados.equals(other.getOutros_dados()))) &&
            ((this.outros_dados_residencia==null && other.getOutros_dados_residencia()==null) || 
             (this.outros_dados_residencia!=null &&
              this.outros_dados_residencia.equals(other.getOutros_dados_residencia()))) &&
            ((this.permiteRenda==null && other.getPermiteRenda()==null) || 
             (this.permiteRenda!=null &&
              this.permiteRenda.equals(other.getPermiteRenda()))) &&
            ((this.ppe==null && other.getPpe()==null) || 
             (this.ppe!=null &&
              this.ppe.equals(other.getPpe()))) &&
            ((this.referencia_pessoal==null && other.getReferencia_pessoal()==null) || 
             (this.referencia_pessoal!=null &&
              java.util.Arrays.equals(this.referencia_pessoal, other.getReferencia_pessoal()))) &&
            ((this.renda_adicional==null && other.getRenda_adicional()==null) || 
             (this.renda_adicional!=null &&
              this.renda_adicional.equals(other.getRenda_adicional())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBem_veiculo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBem_veiculo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBem_veiculo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCnh() != null) {
            _hashCode += getCnh().hashCode();
        }
        if (getConjuge_dados_basicos() != null) {
            _hashCode += getConjuge_dados_basicos().hashCode();
        }
        if (getDados_profissionais() != null) {
            _hashCode += getDados_profissionais().hashCode();
        }
        if (getDocumento() != null) {
            _hashCode += getDocumento().hashCode();
        }
        if (getEmpresa_anterior() != null) {
            _hashCode += getEmpresa_anterior().hashCode();
        }
        if (getFiliacao() != null) {
            _hashCode += getFiliacao().hashCode();
        }
        if (getIsencao_fiscal() != null) {
            _hashCode += getIsencao_fiscal().hashCode();
        }
        if (getLocal_nascimento() != null) {
            _hashCode += getLocal_nascimento().hashCode();
        }
        if (getOutros_dados() != null) {
            _hashCode += getOutros_dados().hashCode();
        }
        if (getOutros_dados_residencia() != null) {
            _hashCode += getOutros_dados_residencia().hashCode();
        }
        if (getPermiteRenda() != null) {
            _hashCode += getPermiteRenda().hashCode();
        }
        if (getPpe() != null) {
            _hashCode += getPpe().hashCode();
        }
        if (getReferencia_pessoal() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferencia_pessoal());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferencia_pessoal(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRenda_adicional() != null) {
            _hashCode += getRenda_adicional().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Proposta_PF.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_PF"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bem_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "bem_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Veiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Veiculo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Cnh"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conjuge_dados_basicos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "conjuge_dados_basicos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Conjuge_Dados_Basicos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dados_profissionais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "dados_profissionais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Dados_Profissionais"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Documento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empresa_anterior");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "empresa_anterior"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Empresa_Anterior"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filiacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "filiacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Filiacao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isencao_fiscal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "isencao_fiscal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Isencoes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("local_nascimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "local_nascimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Local_Nascimento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outros_dados");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "outros_dados"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outros_dados_residencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "outros_dados_residencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Residencia"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("permiteRenda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "permiteRenda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ppe");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ppe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Ppe"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencia_pessoal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "referencia_pessoal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Pessoal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Pessoal"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("renda_adicional");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "renda_adicional"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Dados_Profissionais"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
