package com.svs.fin.integracaosantander.santander;

public class Legal {

	private Phone phone;

	private BankReference bankReference;

	private String economicActivity;

	private String companySize;

	private String monthlyInvoicing;

	private Guarantor guarantor;

	private String legalNature;

	private String groupEconomicActivity;

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public BankReference getBankReference() {
		return bankReference;
	}

	public void setBankReference(BankReference bankReference) {
		this.bankReference = bankReference;
	}

	public String getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(String economicActivity) {
		this.economicActivity = economicActivity;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getMonthlyInvoicing() {
		return monthlyInvoicing;
	}

	public void setMonthlyInvoicing(String monthlyInvoicing) {
		this.monthlyInvoicing = monthlyInvoicing;
	}

	public Guarantor getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(Guarantor guarantor) {
		this.guarantor = guarantor;
	}

	public String getLegalNature() {
		return legalNature;
	}

	public void setLegalNature(String legalNature) {
		this.legalNature = legalNature;
	}

	public String getGroupEconomicActivity() {
		return groupEconomicActivity;
	}

	public void setGroupEconomicActivity(String groupEconomicActivity) {
		this.groupEconomicActivity = groupEconomicActivity;
	}

	@Override
	public String toString() {
		return "ClassPojo [phone = " + phone + ", bankReference = " + bankReference + ", economicActivity = "
				+ economicActivity + ", companySize = " + companySize + ", monthlyInvoicing = " + monthlyInvoicing
				+ ", guarantor = " + guarantor + ", legalNature = " + legalNature + ", groupEconomicActivity = "
				+ groupEconomicActivity + "]";
	}

}
