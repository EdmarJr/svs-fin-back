/**
 * Filiacao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Filiacao  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String nm_mae;

    private java.lang.String nm_pai;

    public Filiacao() {
    }

    public Filiacao(
           java.lang.String nm_mae,
           java.lang.String nm_pai) {
        this.nm_mae = nm_mae;
        this.nm_pai = nm_pai;
    }


    /**
     * Gets the nm_mae value for this Filiacao.
     * 
     * @return nm_mae
     */
    public java.lang.String getNm_mae() {
        return nm_mae;
    }


    /**
     * Sets the nm_mae value for this Filiacao.
     * 
     * @param nm_mae
     */
    public void setNm_mae(java.lang.String nm_mae) {
        this.nm_mae = nm_mae;
    }


    /**
     * Gets the nm_pai value for this Filiacao.
     * 
     * @return nm_pai
     */
    public java.lang.String getNm_pai() {
        return nm_pai;
    }


    /**
     * Sets the nm_pai value for this Filiacao.
     * 
     * @param nm_pai
     */
    public void setNm_pai(java.lang.String nm_pai) {
        this.nm_pai = nm_pai;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Filiacao)) return false;
        Filiacao other = (Filiacao) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.nm_mae==null && other.getNm_mae()==null) || 
             (this.nm_mae!=null &&
              this.nm_mae.equals(other.getNm_mae()))) &&
            ((this.nm_pai==null && other.getNm_pai()==null) || 
             (this.nm_pai!=null &&
              this.nm_pai.equals(other.getNm_pai())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNm_mae() != null) {
            _hashCode += getNm_mae().hashCode();
        }
        if (getNm_pai() != null) {
            _hashCode += getNm_pai().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Filiacao.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Filiacao"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_mae");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_mae"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_pai");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_pai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
