package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoPessoa {
	
//	PESSOA_FISICA("Pessoa Física"),
//	PESSOA_JURIDICA("Pessoa Jurídica");
	@JsonProperty("Pessoa Física")
	PESSOA_FISICA("Pessoa Física"),
		@JsonProperty("Pessoa Jurídica")
	PESSOA_JURIDICA("Pessoa Jurídica");
	EnumTipoPessoa(String label){
		this.label = label;
	}
	
	private String label;

	public String getLabel() {
		return label;
	}
	
	public String getDocumento() {
		if(label.equals("Pessoa F�sica")){
			return "CPF";
		}else{
			return "CNPJ";
		}
	}

}
