package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class PropostaCadastroRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2776754535469363910L;

	private String certifieldAgentId;

	private Customer customer;

	private String channel;
	
	private String uuid;

	public String getCertifieldAgentId() {
		return certifieldAgentId;
	}

	public void setCertifieldAgentId(String certifieldAgentId) {
		this.certifieldAgentId = certifieldAgentId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return "ClassPojo [certifieldAgentId = " + certifieldAgentId + ", customer = " + customer + ", channel = "
				+ channel + "]";
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
