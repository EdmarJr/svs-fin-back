package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import com.svs.fin.integracaosantander.santander.PropostaCadastroResponse;

@Entity
public class JsonRespostaProposta implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -587846660333114882L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@Column
	private Calendar data;
	
	@Lob
	private byte[] jsonByte;
	
	@Transient
	private String jsonTexto;
	
	@Transient
	private PropostaCadastroResponse propostaCadastroResponse;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public byte[] getJsonByte() {
		return jsonByte;
	}

	public void setJsonByte(byte[] jsonByte) {
		this.jsonByte = jsonByte;
	}

	public String getJsonTexto() {
		if(jsonByte != null && jsonTexto == null) {
			setJsonTexto(new String(jsonByte, Charset.forName("UTF-8")));
		}
		return jsonTexto;
	}
	
	public String getJsonTextoFormatado() {
		String jsonTexto = getJsonTexto();
//		if(jsonTexto != null && !jsonTexto.trim().equals("")) {
//			PropostaCadastroResponse response = new Gson().fromJson(getJsonTexto(), PropostaCadastroResponse.class);
//			return StringUtils.getObjetoComoJsonFormatado(response);
//		}
		return "";
	}

	public void setJsonTexto(String jsonTexto) {
		this.jsonTexto = jsonTexto;
	}

	public PropostaCadastroResponse getPropostaCadastroResponse() {
		if(propostaCadastroResponse == null) {
			this.propostaCadastroResponse = new PropostaCadastroResponse();
		}
		return propostaCadastroResponse;
	}

	public void setPropostaCadastroResponse(PropostaCadastroResponse propostaCadastroResponse) {
		this.propostaCadastroResponse = propostaCadastroResponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonRespostaProposta other = (JsonRespostaProposta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
