package com.svs.fin.integracaosantander.santander.dto;

public class TabIdDTO {

	public String tabId;
	public String uuid;
	
	public String getTabId() {
		return tabId;
	}
	public void setTabId(String tabId) {
		this.tabId = tabId;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
