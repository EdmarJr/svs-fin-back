package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.svs.fin.enums.EnumTipoRecebimento;

@Entity
@NamedQuery(name=TipoPagamento.NQ_OBTER_POR_TIPO.NAME, query=TipoPagamento.NQ_OBTER_POR_TIPO.JPQL)
public class TipoPagamento implements Serializable {

	public static interface NQ_OBTER_POR_TIPO {
		public static final String NAME = "TipoPagamento.obterPorTipo";
		public static final String JPQL = "Select t from TipoPagamento t where t.tipo = :" + NQ_OBTER_POR_TIPO.NM_PM_TIPO;
		public static final String NM_PM_TIPO = "tipo";
	}
	
	private static final long serialVersionUID = -2793147496738356202L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPOPAGAMENTO")
	@SequenceGenerator(name = "SEQ_TIPOPAGAMENTO", sequenceName = "SEQ_TIPOPAGAMENTO", allocationSize = 1)
	private Long id;

	@Column
	private Long codigoDealerWorkflow;

	@Column(length = 500)
	private String descricao;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoRecebimento tipo;

	@Column
	private Boolean ativo;

	@Transient
	private String tipoRecebimentoString;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(Long codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumTipoRecebimento getTipo() {
		return tipo;
	}

	public void setTipo(EnumTipoRecebimento tipo) {
		this.tipo = tipo;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getTipoRecebimentoString() {
		return tipoRecebimentoString;
	}

	public void setTipoRecebimentoString(String tipoRecebimentoString) {
		this.tipoRecebimentoString = tipoRecebimentoString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPagamento other = (TipoPagamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
