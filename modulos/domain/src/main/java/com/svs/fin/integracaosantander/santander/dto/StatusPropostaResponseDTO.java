package com.svs.fin.integracaosantander.santander.dto;

public class StatusPropostaResponseDTO {
	
	private String status;
	
	private String statusDescription;
	
	private String frozen;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getFrozen() {
		return frozen;
	}

	public void setFrozen(String frozen) {
		this.frozen = frozen;
	}
}
