package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import com.svs.fin.integracaosantander.santander.PreAnaliseResponse;

@Entity(name="JSON_PREANALISE_SANTANDER")
public class JsonRespostaPreAnaliseSantander implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8215749958220294696L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@Column
	private Calendar data;
	
	@Column
	private String nomeCliente;
	
	@Column
	private String cpfCliente;
	
	@Lob
	private byte[] jsonByte;
	
	@Transient
	private String jsonTexto;
	
	@Transient
	private PreAnaliseResponse preAnaliseResponse;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getJsonTexto() {
		if(jsonByte != null && jsonTexto == null) {
			setJsonTexto(new String(jsonByte, Charset.forName("UTF-8")));
		}
		return jsonTexto;
	}

	public String getJsonTextoFormatado() {
		String jsonTexto = getJsonTexto();
//		if(jsonTexto != null && !jsonTexto.trim().equals("")) {
//			PreAnaliseResponse preAnaliseResponse = new Gson().fromJson(getJsonTexto(), PreAnaliseResponse.class);
//			return StringUtils.getObjetoComoJsonFormatado(preAnaliseResponse);
//		}
		return "";
	}
	
	public void setJsonTexto(String jsonTexto) {
		this.jsonTexto = jsonTexto;
	}

	public byte[] getJsonByte() {
		return jsonByte;
	}

	public void setJsonByte(byte[] jsonByte) {
		this.jsonByte = jsonByte;
		this.jsonTexto = null;
	}

	public PreAnaliseResponse getPreAnaliseResponse() {
		return preAnaliseResponse;
	}

	public void setPreAnaliseResponse(PreAnaliseResponse preAnaliseResponse) {
		this.preAnaliseResponse = preAnaliseResponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonRespostaPreAnaliseSantander other = (JsonRespostaPreAnaliseSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
