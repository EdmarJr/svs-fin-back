/**
 * Outros_Dados_Complemento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Outros_Dados_Complemento  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_atividade;

    private java.lang.String id_porte_empresa;

    private java.lang.String id_ramo_atividade;

    private java.lang.String in_exportacao;

    private java.lang.String in_importacao;

    private java.lang.String pc_exporta_vndas;

    private java.lang.String pc_importa_cpras;

    private java.lang.String qt_empregados;

    private java.lang.String vl_cpras_ult_exerc;

    private java.lang.String vl_nvdas_ult_exerc;

    public Outros_Dados_Complemento() {
    }

    public Outros_Dados_Complemento(
           java.lang.String id_atividade,
           java.lang.String id_porte_empresa,
           java.lang.String id_ramo_atividade,
           java.lang.String in_exportacao,
           java.lang.String in_importacao,
           java.lang.String pc_exporta_vndas,
           java.lang.String pc_importa_cpras,
           java.lang.String qt_empregados,
           java.lang.String vl_cpras_ult_exerc,
           java.lang.String vl_nvdas_ult_exerc) {
        this.id_atividade = id_atividade;
        this.id_porte_empresa = id_porte_empresa;
        this.id_ramo_atividade = id_ramo_atividade;
        this.in_exportacao = in_exportacao;
        this.in_importacao = in_importacao;
        this.pc_exporta_vndas = pc_exporta_vndas;
        this.pc_importa_cpras = pc_importa_cpras;
        this.qt_empregados = qt_empregados;
        this.vl_cpras_ult_exerc = vl_cpras_ult_exerc;
        this.vl_nvdas_ult_exerc = vl_nvdas_ult_exerc;
    }


    /**
     * Gets the id_atividade value for this Outros_Dados_Complemento.
     * 
     * @return id_atividade
     */
    public java.lang.String getId_atividade() {
        return id_atividade;
    }


    /**
     * Sets the id_atividade value for this Outros_Dados_Complemento.
     * 
     * @param id_atividade
     */
    public void setId_atividade(java.lang.String id_atividade) {
        this.id_atividade = id_atividade;
    }


    /**
     * Gets the id_porte_empresa value for this Outros_Dados_Complemento.
     * 
     * @return id_porte_empresa
     */
    public java.lang.String getId_porte_empresa() {
        return id_porte_empresa;
    }


    /**
     * Sets the id_porte_empresa value for this Outros_Dados_Complemento.
     * 
     * @param id_porte_empresa
     */
    public void setId_porte_empresa(java.lang.String id_porte_empresa) {
        this.id_porte_empresa = id_porte_empresa;
    }


    /**
     * Gets the id_ramo_atividade value for this Outros_Dados_Complemento.
     * 
     * @return id_ramo_atividade
     */
    public java.lang.String getId_ramo_atividade() {
        return id_ramo_atividade;
    }


    /**
     * Sets the id_ramo_atividade value for this Outros_Dados_Complemento.
     * 
     * @param id_ramo_atividade
     */
    public void setId_ramo_atividade(java.lang.String id_ramo_atividade) {
        this.id_ramo_atividade = id_ramo_atividade;
    }


    /**
     * Gets the in_exportacao value for this Outros_Dados_Complemento.
     * 
     * @return in_exportacao
     */
    public java.lang.String getIn_exportacao() {
        return in_exportacao;
    }


    /**
     * Sets the in_exportacao value for this Outros_Dados_Complemento.
     * 
     * @param in_exportacao
     */
    public void setIn_exportacao(java.lang.String in_exportacao) {
        this.in_exportacao = in_exportacao;
    }


    /**
     * Gets the in_importacao value for this Outros_Dados_Complemento.
     * 
     * @return in_importacao
     */
    public java.lang.String getIn_importacao() {
        return in_importacao;
    }


    /**
     * Sets the in_importacao value for this Outros_Dados_Complemento.
     * 
     * @param in_importacao
     */
    public void setIn_importacao(java.lang.String in_importacao) {
        this.in_importacao = in_importacao;
    }


    /**
     * Gets the pc_exporta_vndas value for this Outros_Dados_Complemento.
     * 
     * @return pc_exporta_vndas
     */
    public java.lang.String getPc_exporta_vndas() {
        return pc_exporta_vndas;
    }


    /**
     * Sets the pc_exporta_vndas value for this Outros_Dados_Complemento.
     * 
     * @param pc_exporta_vndas
     */
    public void setPc_exporta_vndas(java.lang.String pc_exporta_vndas) {
        this.pc_exporta_vndas = pc_exporta_vndas;
    }


    /**
     * Gets the pc_importa_cpras value for this Outros_Dados_Complemento.
     * 
     * @return pc_importa_cpras
     */
    public java.lang.String getPc_importa_cpras() {
        return pc_importa_cpras;
    }


    /**
     * Sets the pc_importa_cpras value for this Outros_Dados_Complemento.
     * 
     * @param pc_importa_cpras
     */
    public void setPc_importa_cpras(java.lang.String pc_importa_cpras) {
        this.pc_importa_cpras = pc_importa_cpras;
    }


    /**
     * Gets the qt_empregados value for this Outros_Dados_Complemento.
     * 
     * @return qt_empregados
     */
    public java.lang.String getQt_empregados() {
        return qt_empregados;
    }


    /**
     * Sets the qt_empregados value for this Outros_Dados_Complemento.
     * 
     * @param qt_empregados
     */
    public void setQt_empregados(java.lang.String qt_empregados) {
        this.qt_empregados = qt_empregados;
    }


    /**
     * Gets the vl_cpras_ult_exerc value for this Outros_Dados_Complemento.
     * 
     * @return vl_cpras_ult_exerc
     */
    public java.lang.String getVl_cpras_ult_exerc() {
        return vl_cpras_ult_exerc;
    }


    /**
     * Sets the vl_cpras_ult_exerc value for this Outros_Dados_Complemento.
     * 
     * @param vl_cpras_ult_exerc
     */
    public void setVl_cpras_ult_exerc(java.lang.String vl_cpras_ult_exerc) {
        this.vl_cpras_ult_exerc = vl_cpras_ult_exerc;
    }


    /**
     * Gets the vl_nvdas_ult_exerc value for this Outros_Dados_Complemento.
     * 
     * @return vl_nvdas_ult_exerc
     */
    public java.lang.String getVl_nvdas_ult_exerc() {
        return vl_nvdas_ult_exerc;
    }


    /**
     * Sets the vl_nvdas_ult_exerc value for this Outros_Dados_Complemento.
     * 
     * @param vl_nvdas_ult_exerc
     */
    public void setVl_nvdas_ult_exerc(java.lang.String vl_nvdas_ult_exerc) {
        this.vl_nvdas_ult_exerc = vl_nvdas_ult_exerc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Outros_Dados_Complemento)) return false;
        Outros_Dados_Complemento other = (Outros_Dados_Complemento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_atividade==null && other.getId_atividade()==null) || 
             (this.id_atividade!=null &&
              this.id_atividade.equals(other.getId_atividade()))) &&
            ((this.id_porte_empresa==null && other.getId_porte_empresa()==null) || 
             (this.id_porte_empresa!=null &&
              this.id_porte_empresa.equals(other.getId_porte_empresa()))) &&
            ((this.id_ramo_atividade==null && other.getId_ramo_atividade()==null) || 
             (this.id_ramo_atividade!=null &&
              this.id_ramo_atividade.equals(other.getId_ramo_atividade()))) &&
            ((this.in_exportacao==null && other.getIn_exportacao()==null) || 
             (this.in_exportacao!=null &&
              this.in_exportacao.equals(other.getIn_exportacao()))) &&
            ((this.in_importacao==null && other.getIn_importacao()==null) || 
             (this.in_importacao!=null &&
              this.in_importacao.equals(other.getIn_importacao()))) &&
            ((this.pc_exporta_vndas==null && other.getPc_exporta_vndas()==null) || 
             (this.pc_exporta_vndas!=null &&
              this.pc_exporta_vndas.equals(other.getPc_exporta_vndas()))) &&
            ((this.pc_importa_cpras==null && other.getPc_importa_cpras()==null) || 
             (this.pc_importa_cpras!=null &&
              this.pc_importa_cpras.equals(other.getPc_importa_cpras()))) &&
            ((this.qt_empregados==null && other.getQt_empregados()==null) || 
             (this.qt_empregados!=null &&
              this.qt_empregados.equals(other.getQt_empregados()))) &&
            ((this.vl_cpras_ult_exerc==null && other.getVl_cpras_ult_exerc()==null) || 
             (this.vl_cpras_ult_exerc!=null &&
              this.vl_cpras_ult_exerc.equals(other.getVl_cpras_ult_exerc()))) &&
            ((this.vl_nvdas_ult_exerc==null && other.getVl_nvdas_ult_exerc()==null) || 
             (this.vl_nvdas_ult_exerc!=null &&
              this.vl_nvdas_ult_exerc.equals(other.getVl_nvdas_ult_exerc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_atividade() != null) {
            _hashCode += getId_atividade().hashCode();
        }
        if (getId_porte_empresa() != null) {
            _hashCode += getId_porte_empresa().hashCode();
        }
        if (getId_ramo_atividade() != null) {
            _hashCode += getId_ramo_atividade().hashCode();
        }
        if (getIn_exportacao() != null) {
            _hashCode += getIn_exportacao().hashCode();
        }
        if (getIn_importacao() != null) {
            _hashCode += getIn_importacao().hashCode();
        }
        if (getPc_exporta_vndas() != null) {
            _hashCode += getPc_exporta_vndas().hashCode();
        }
        if (getPc_importa_cpras() != null) {
            _hashCode += getPc_importa_cpras().hashCode();
        }
        if (getQt_empregados() != null) {
            _hashCode += getQt_empregados().hashCode();
        }
        if (getVl_cpras_ult_exerc() != null) {
            _hashCode += getVl_cpras_ult_exerc().hashCode();
        }
        if (getVl_nvdas_ult_exerc() != null) {
            _hashCode += getVl_nvdas_ult_exerc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Outros_Dados_Complemento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Complemento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_atividade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_atividade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_porte_empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_porte_empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_ramo_atividade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_ramo_atividade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("in_exportacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "in_exportacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("in_importacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "in_importacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_exporta_vndas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "pc_exporta_vndas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_importa_cpras");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "pc_importa_cpras"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qt_empregados");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "qt_empregados"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_cpras_ult_exerc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_cpras_ult_exerc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_nvdas_ult_exerc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_nvdas_ult_exerc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
