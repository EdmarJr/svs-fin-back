/**
 * ParamCalculaParcelas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class ParamCalculaParcelas  extends com.svs.fin.integracao.safra.model.ValidaHASH  implements java.io.Serializable {
    private java.lang.Integer v_ano;

    private java.lang.String v_cd_molicar;

    private java.lang.String v_cidade;

    private java.lang.String v_dt_vecto;

    private java.lang.Integer v_faixa;

    private java.lang.String v_financia_iof;

    private java.lang.String v_financia_iof_adic;

    private java.lang.String v_financia_reg_grav;

    private java.lang.String v_financia_retorno;

    private java.lang.String v_financia_serv_prest;

    private java.lang.String v_financia_ta;

    private java.lang.String v_financia_tc;

    private java.lang.String v_fl_taxi;

    private java.lang.String v_id_cliente;

    private java.lang.String v_id_pessoa;

    private java.lang.Integer v_id_segmento;

    private java.lang.String v_id_tc_paga;

    private java.lang.String v_id_usuario_web;

    private java.math.BigDecimal v_pc_ret;

    private java.lang.Integer v_prazo;

    private java.lang.Integer v_produto;

    private java.lang.Integer v_qtd_veiculo;

    private java.lang.Integer v_seguro;

    private java.lang.Integer v_tabela;

    private java.lang.Integer v_tipo_fluxo;

    private java.lang.String v_uf;

    private java.lang.Double v_vl_entrada;

    private java.lang.Double v_vl_principal;

    public ParamCalculaParcelas() {
    }

    public ParamCalculaParcelas(
           java.lang.Integer idRevenda,
           java.lang.String dsHash,
           java.lang.Integer v_ano,
           java.lang.String v_cd_molicar,
           java.lang.String v_cidade,
           java.lang.String v_dt_vecto,
           java.lang.Integer v_faixa,
           java.lang.String v_financia_iof,
           java.lang.String v_financia_iof_adic,
           java.lang.String v_financia_reg_grav,
           java.lang.String v_financia_retorno,
           java.lang.String v_financia_serv_prest,
           java.lang.String v_financia_ta,
           java.lang.String v_financia_tc,
           java.lang.String v_fl_taxi,
           java.lang.String v_id_cliente,
           java.lang.String v_id_pessoa,
           java.lang.Integer v_id_segmento,
           java.lang.String v_id_tc_paga,
           java.lang.String v_id_usuario_web,
           java.math.BigDecimal v_pc_ret,
           java.lang.Integer v_prazo,
           java.lang.Integer v_produto,
           java.lang.Integer v_qtd_veiculo,
           java.lang.Integer v_seguro,
           java.lang.Integer v_tabela,
           java.lang.Integer v_tipo_fluxo,
           java.lang.String v_uf,
           java.lang.Double v_vl_entrada,
           java.lang.Double v_vl_principal) {
        super(
            idRevenda,
            dsHash);
        this.v_ano = v_ano;
        this.v_cd_molicar = v_cd_molicar;
        this.v_cidade = v_cidade;
        this.v_dt_vecto = v_dt_vecto;
        this.v_faixa = v_faixa;
        this.v_financia_iof = v_financia_iof;
        this.v_financia_iof_adic = v_financia_iof_adic;
        this.v_financia_reg_grav = v_financia_reg_grav;
        this.v_financia_retorno = v_financia_retorno;
        this.v_financia_serv_prest = v_financia_serv_prest;
        this.v_financia_ta = v_financia_ta;
        this.v_financia_tc = v_financia_tc;
        this.v_fl_taxi = v_fl_taxi;
        this.v_id_cliente = v_id_cliente;
        this.v_id_pessoa = v_id_pessoa;
        this.v_id_segmento = v_id_segmento;
        this.v_id_tc_paga = v_id_tc_paga;
        this.v_id_usuario_web = v_id_usuario_web;
        this.v_pc_ret = v_pc_ret;
        this.v_prazo = v_prazo;
        this.v_produto = v_produto;
        this.v_qtd_veiculo = v_qtd_veiculo;
        this.v_seguro = v_seguro;
        this.v_tabela = v_tabela;
        this.v_tipo_fluxo = v_tipo_fluxo;
        this.v_uf = v_uf;
        this.v_vl_entrada = v_vl_entrada;
        this.v_vl_principal = v_vl_principal;
    }


    /**
     * Gets the v_ano value for this ParamCalculaParcelas.
     * 
     * @return v_ano
     */
    public java.lang.Integer getV_ano() {
        return v_ano;
    }


    /**
     * Sets the v_ano value for this ParamCalculaParcelas.
     * 
     * @param v_ano
     */
    public void setV_ano(java.lang.Integer v_ano) {
        this.v_ano = v_ano;
    }


    /**
     * Gets the v_cd_molicar value for this ParamCalculaParcelas.
     * 
     * @return v_cd_molicar
     */
    public java.lang.String getV_cd_molicar() {
        return v_cd_molicar;
    }


    /**
     * Sets the v_cd_molicar value for this ParamCalculaParcelas.
     * 
     * @param v_cd_molicar
     */
    public void setV_cd_molicar(java.lang.String v_cd_molicar) {
        this.v_cd_molicar = v_cd_molicar;
    }


    /**
     * Gets the v_cidade value for this ParamCalculaParcelas.
     * 
     * @return v_cidade
     */
    public java.lang.String getV_cidade() {
        return v_cidade;
    }


    /**
     * Sets the v_cidade value for this ParamCalculaParcelas.
     * 
     * @param v_cidade
     */
    public void setV_cidade(java.lang.String v_cidade) {
        this.v_cidade = v_cidade;
    }


    /**
     * Gets the v_dt_vecto value for this ParamCalculaParcelas.
     * 
     * @return v_dt_vecto
     */
    public java.lang.String getV_dt_vecto() {
        return v_dt_vecto;
    }


    /**
     * Sets the v_dt_vecto value for this ParamCalculaParcelas.
     * 
     * @param v_dt_vecto
     */
    public void setV_dt_vecto(java.lang.String v_dt_vecto) {
        this.v_dt_vecto = v_dt_vecto;
    }


    /**
     * Gets the v_faixa value for this ParamCalculaParcelas.
     * 
     * @return v_faixa
     */
    public java.lang.Integer getV_faixa() {
        return v_faixa;
    }


    /**
     * Sets the v_faixa value for this ParamCalculaParcelas.
     * 
     * @param v_faixa
     */
    public void setV_faixa(java.lang.Integer v_faixa) {
        this.v_faixa = v_faixa;
    }


    /**
     * Gets the v_financia_iof value for this ParamCalculaParcelas.
     * 
     * @return v_financia_iof
     */
    public java.lang.String getV_financia_iof() {
        return v_financia_iof;
    }


    /**
     * Sets the v_financia_iof value for this ParamCalculaParcelas.
     * 
     * @param v_financia_iof
     */
    public void setV_financia_iof(java.lang.String v_financia_iof) {
        this.v_financia_iof = v_financia_iof;
    }


    /**
     * Gets the v_financia_iof_adic value for this ParamCalculaParcelas.
     * 
     * @return v_financia_iof_adic
     */
    public java.lang.String getV_financia_iof_adic() {
        return v_financia_iof_adic;
    }


    /**
     * Sets the v_financia_iof_adic value for this ParamCalculaParcelas.
     * 
     * @param v_financia_iof_adic
     */
    public void setV_financia_iof_adic(java.lang.String v_financia_iof_adic) {
        this.v_financia_iof_adic = v_financia_iof_adic;
    }


    /**
     * Gets the v_financia_reg_grav value for this ParamCalculaParcelas.
     * 
     * @return v_financia_reg_grav
     */
    public java.lang.String getV_financia_reg_grav() {
        return v_financia_reg_grav;
    }


    /**
     * Sets the v_financia_reg_grav value for this ParamCalculaParcelas.
     * 
     * @param v_financia_reg_grav
     */
    public void setV_financia_reg_grav(java.lang.String v_financia_reg_grav) {
        this.v_financia_reg_grav = v_financia_reg_grav;
    }


    /**
     * Gets the v_financia_retorno value for this ParamCalculaParcelas.
     * 
     * @return v_financia_retorno
     */
    public java.lang.String getV_financia_retorno() {
        return v_financia_retorno;
    }


    /**
     * Sets the v_financia_retorno value for this ParamCalculaParcelas.
     * 
     * @param v_financia_retorno
     */
    public void setV_financia_retorno(java.lang.String v_financia_retorno) {
        this.v_financia_retorno = v_financia_retorno;
    }


    /**
     * Gets the v_financia_serv_prest value for this ParamCalculaParcelas.
     * 
     * @return v_financia_serv_prest
     */
    public java.lang.String getV_financia_serv_prest() {
        return v_financia_serv_prest;
    }


    /**
     * Sets the v_financia_serv_prest value for this ParamCalculaParcelas.
     * 
     * @param v_financia_serv_prest
     */
    public void setV_financia_serv_prest(java.lang.String v_financia_serv_prest) {
        this.v_financia_serv_prest = v_financia_serv_prest;
    }


    /**
     * Gets the v_financia_ta value for this ParamCalculaParcelas.
     * 
     * @return v_financia_ta
     */
    public java.lang.String getV_financia_ta() {
        return v_financia_ta;
    }


    /**
     * Sets the v_financia_ta value for this ParamCalculaParcelas.
     * 
     * @param v_financia_ta
     */
    public void setV_financia_ta(java.lang.String v_financia_ta) {
        this.v_financia_ta = v_financia_ta;
    }


    /**
     * Gets the v_financia_tc value for this ParamCalculaParcelas.
     * 
     * @return v_financia_tc
     */
    public java.lang.String getV_financia_tc() {
        return v_financia_tc;
    }


    /**
     * Sets the v_financia_tc value for this ParamCalculaParcelas.
     * 
     * @param v_financia_tc
     */
    public void setV_financia_tc(java.lang.String v_financia_tc) {
        this.v_financia_tc = v_financia_tc;
    }


    /**
     * Gets the v_fl_taxi value for this ParamCalculaParcelas.
     * 
     * @return v_fl_taxi
     */
    public java.lang.String getV_fl_taxi() {
        return v_fl_taxi;
    }


    /**
     * Sets the v_fl_taxi value for this ParamCalculaParcelas.
     * 
     * @param v_fl_taxi
     */
    public void setV_fl_taxi(java.lang.String v_fl_taxi) {
        this.v_fl_taxi = v_fl_taxi;
    }


    /**
     * Gets the v_id_cliente value for this ParamCalculaParcelas.
     * 
     * @return v_id_cliente
     */
    public java.lang.String getV_id_cliente() {
        return v_id_cliente;
    }


    /**
     * Sets the v_id_cliente value for this ParamCalculaParcelas.
     * 
     * @param v_id_cliente
     */
    public void setV_id_cliente(java.lang.String v_id_cliente) {
        this.v_id_cliente = v_id_cliente;
    }


    /**
     * Gets the v_id_pessoa value for this ParamCalculaParcelas.
     * 
     * @return v_id_pessoa
     */
    public java.lang.String getV_id_pessoa() {
        return v_id_pessoa;
    }


    /**
     * Sets the v_id_pessoa value for this ParamCalculaParcelas.
     * 
     * @param v_id_pessoa
     */
    public void setV_id_pessoa(java.lang.String v_id_pessoa) {
        this.v_id_pessoa = v_id_pessoa;
    }


    /**
     * Gets the v_id_segmento value for this ParamCalculaParcelas.
     * 
     * @return v_id_segmento
     */
    public java.lang.Integer getV_id_segmento() {
        return v_id_segmento;
    }


    /**
     * Sets the v_id_segmento value for this ParamCalculaParcelas.
     * 
     * @param v_id_segmento
     */
    public void setV_id_segmento(java.lang.Integer v_id_segmento) {
        this.v_id_segmento = v_id_segmento;
    }


    /**
     * Gets the v_id_tc_paga value for this ParamCalculaParcelas.
     * 
     * @return v_id_tc_paga
     */
    public java.lang.String getV_id_tc_paga() {
        return v_id_tc_paga;
    }


    /**
     * Sets the v_id_tc_paga value for this ParamCalculaParcelas.
     * 
     * @param v_id_tc_paga
     */
    public void setV_id_tc_paga(java.lang.String v_id_tc_paga) {
        this.v_id_tc_paga = v_id_tc_paga;
    }


    /**
     * Gets the v_id_usuario_web value for this ParamCalculaParcelas.
     * 
     * @return v_id_usuario_web
     */
    public java.lang.String getV_id_usuario_web() {
        return v_id_usuario_web;
    }


    /**
     * Sets the v_id_usuario_web value for this ParamCalculaParcelas.
     * 
     * @param v_id_usuario_web
     */
    public void setV_id_usuario_web(java.lang.String v_id_usuario_web) {
        this.v_id_usuario_web = v_id_usuario_web;
    }


    /**
     * Gets the v_pc_ret value for this ParamCalculaParcelas.
     * 
     * @return v_pc_ret
     */
    public java.math.BigDecimal getV_pc_ret() {
        return v_pc_ret;
    }


    /**
     * Sets the v_pc_ret value for this ParamCalculaParcelas.
     * 
     * @param v_pc_ret
     */
    public void setV_pc_ret(java.math.BigDecimal v_pc_ret) {
        this.v_pc_ret = v_pc_ret;
    }


    /**
     * Gets the v_prazo value for this ParamCalculaParcelas.
     * 
     * @return v_prazo
     */
    public java.lang.Integer getV_prazo() {
        return v_prazo;
    }


    /**
     * Sets the v_prazo value for this ParamCalculaParcelas.
     * 
     * @param v_prazo
     */
    public void setV_prazo(java.lang.Integer v_prazo) {
        this.v_prazo = v_prazo;
    }


    /**
     * Gets the v_produto value for this ParamCalculaParcelas.
     * 
     * @return v_produto
     */
    public java.lang.Integer getV_produto() {
        return v_produto;
    }


    /**
     * Sets the v_produto value for this ParamCalculaParcelas.
     * 
     * @param v_produto
     */
    public void setV_produto(java.lang.Integer v_produto) {
        this.v_produto = v_produto;
    }


    /**
     * Gets the v_qtd_veiculo value for this ParamCalculaParcelas.
     * 
     * @return v_qtd_veiculo
     */
    public java.lang.Integer getV_qtd_veiculo() {
        return v_qtd_veiculo;
    }


    /**
     * Sets the v_qtd_veiculo value for this ParamCalculaParcelas.
     * 
     * @param v_qtd_veiculo
     */
    public void setV_qtd_veiculo(java.lang.Integer v_qtd_veiculo) {
        this.v_qtd_veiculo = v_qtd_veiculo;
    }


    /**
     * Gets the v_seguro value for this ParamCalculaParcelas.
     * 
     * @return v_seguro
     */
    public java.lang.Integer getV_seguro() {
        return v_seguro;
    }


    /**
     * Sets the v_seguro value for this ParamCalculaParcelas.
     * 
     * @param v_seguro
     */
    public void setV_seguro(java.lang.Integer v_seguro) {
        this.v_seguro = v_seguro;
    }


    /**
     * Gets the v_tabela value for this ParamCalculaParcelas.
     * 
     * @return v_tabela
     */
    public java.lang.Integer getV_tabela() {
        return v_tabela;
    }


    /**
     * Sets the v_tabela value for this ParamCalculaParcelas.
     * 
     * @param v_tabela
     */
    public void setV_tabela(java.lang.Integer v_tabela) {
        this.v_tabela = v_tabela;
    }


    /**
     * Gets the v_tipo_fluxo value for this ParamCalculaParcelas.
     * 
     * @return v_tipo_fluxo
     */
    public java.lang.Integer getV_tipo_fluxo() {
        return v_tipo_fluxo;
    }


    /**
     * Sets the v_tipo_fluxo value for this ParamCalculaParcelas.
     * 
     * @param v_tipo_fluxo
     */
    public void setV_tipo_fluxo(java.lang.Integer v_tipo_fluxo) {
        this.v_tipo_fluxo = v_tipo_fluxo;
    }


    /**
     * Gets the v_uf value for this ParamCalculaParcelas.
     * 
     * @return v_uf
     */
    public java.lang.String getV_uf() {
        return v_uf;
    }


    /**
     * Sets the v_uf value for this ParamCalculaParcelas.
     * 
     * @param v_uf
     */
    public void setV_uf(java.lang.String v_uf) {
        this.v_uf = v_uf;
    }


    /**
     * Gets the v_vl_entrada value for this ParamCalculaParcelas.
     * 
     * @return v_vl_entrada
     */
    public java.lang.Double getV_vl_entrada() {
        return v_vl_entrada;
    }


    /**
     * Sets the v_vl_entrada value for this ParamCalculaParcelas.
     * 
     * @param v_vl_entrada
     */
    public void setV_vl_entrada(java.lang.Double v_vl_entrada) {
        this.v_vl_entrada = v_vl_entrada;
    }


    /**
     * Gets the v_vl_principal value for this ParamCalculaParcelas.
     * 
     * @return v_vl_principal
     */
    public java.lang.Double getV_vl_principal() {
        return v_vl_principal;
    }


    /**
     * Sets the v_vl_principal value for this ParamCalculaParcelas.
     * 
     * @param v_vl_principal
     */
    public void setV_vl_principal(java.lang.Double v_vl_principal) {
        this.v_vl_principal = v_vl_principal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ParamCalculaParcelas)) return false;
        ParamCalculaParcelas other = (ParamCalculaParcelas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.v_ano==null && other.getV_ano()==null) || 
             (this.v_ano!=null &&
              this.v_ano.equals(other.getV_ano()))) &&
            ((this.v_cd_molicar==null && other.getV_cd_molicar()==null) || 
             (this.v_cd_molicar!=null &&
              this.v_cd_molicar.equals(other.getV_cd_molicar()))) &&
            ((this.v_cidade==null && other.getV_cidade()==null) || 
             (this.v_cidade!=null &&
              this.v_cidade.equals(other.getV_cidade()))) &&
            ((this.v_dt_vecto==null && other.getV_dt_vecto()==null) || 
             (this.v_dt_vecto!=null &&
              this.v_dt_vecto.equals(other.getV_dt_vecto()))) &&
            ((this.v_faixa==null && other.getV_faixa()==null) || 
             (this.v_faixa!=null &&
              this.v_faixa.equals(other.getV_faixa()))) &&
            ((this.v_financia_iof==null && other.getV_financia_iof()==null) || 
             (this.v_financia_iof!=null &&
              this.v_financia_iof.equals(other.getV_financia_iof()))) &&
            ((this.v_financia_iof_adic==null && other.getV_financia_iof_adic()==null) || 
             (this.v_financia_iof_adic!=null &&
              this.v_financia_iof_adic.equals(other.getV_financia_iof_adic()))) &&
            ((this.v_financia_reg_grav==null && other.getV_financia_reg_grav()==null) || 
             (this.v_financia_reg_grav!=null &&
              this.v_financia_reg_grav.equals(other.getV_financia_reg_grav()))) &&
            ((this.v_financia_retorno==null && other.getV_financia_retorno()==null) || 
             (this.v_financia_retorno!=null &&
              this.v_financia_retorno.equals(other.getV_financia_retorno()))) &&
            ((this.v_financia_serv_prest==null && other.getV_financia_serv_prest()==null) || 
             (this.v_financia_serv_prest!=null &&
              this.v_financia_serv_prest.equals(other.getV_financia_serv_prest()))) &&
            ((this.v_financia_ta==null && other.getV_financia_ta()==null) || 
             (this.v_financia_ta!=null &&
              this.v_financia_ta.equals(other.getV_financia_ta()))) &&
            ((this.v_financia_tc==null && other.getV_financia_tc()==null) || 
             (this.v_financia_tc!=null &&
              this.v_financia_tc.equals(other.getV_financia_tc()))) &&
            ((this.v_fl_taxi==null && other.getV_fl_taxi()==null) || 
             (this.v_fl_taxi!=null &&
              this.v_fl_taxi.equals(other.getV_fl_taxi()))) &&
            ((this.v_id_cliente==null && other.getV_id_cliente()==null) || 
             (this.v_id_cliente!=null &&
              this.v_id_cliente.equals(other.getV_id_cliente()))) &&
            ((this.v_id_pessoa==null && other.getV_id_pessoa()==null) || 
             (this.v_id_pessoa!=null &&
              this.v_id_pessoa.equals(other.getV_id_pessoa()))) &&
            ((this.v_id_segmento==null && other.getV_id_segmento()==null) || 
             (this.v_id_segmento!=null &&
              this.v_id_segmento.equals(other.getV_id_segmento()))) &&
            ((this.v_id_tc_paga==null && other.getV_id_tc_paga()==null) || 
             (this.v_id_tc_paga!=null &&
              this.v_id_tc_paga.equals(other.getV_id_tc_paga()))) &&
            ((this.v_id_usuario_web==null && other.getV_id_usuario_web()==null) || 
             (this.v_id_usuario_web!=null &&
              this.v_id_usuario_web.equals(other.getV_id_usuario_web()))) &&
            ((this.v_pc_ret==null && other.getV_pc_ret()==null) || 
             (this.v_pc_ret!=null &&
              this.v_pc_ret.equals(other.getV_pc_ret()))) &&
            ((this.v_prazo==null && other.getV_prazo()==null) || 
             (this.v_prazo!=null &&
              this.v_prazo.equals(other.getV_prazo()))) &&
            ((this.v_produto==null && other.getV_produto()==null) || 
             (this.v_produto!=null &&
              this.v_produto.equals(other.getV_produto()))) &&
            ((this.v_qtd_veiculo==null && other.getV_qtd_veiculo()==null) || 
             (this.v_qtd_veiculo!=null &&
              this.v_qtd_veiculo.equals(other.getV_qtd_veiculo()))) &&
            ((this.v_seguro==null && other.getV_seguro()==null) || 
             (this.v_seguro!=null &&
              this.v_seguro.equals(other.getV_seguro()))) &&
            ((this.v_tabela==null && other.getV_tabela()==null) || 
             (this.v_tabela!=null &&
              this.v_tabela.equals(other.getV_tabela()))) &&
            ((this.v_tipo_fluxo==null && other.getV_tipo_fluxo()==null) || 
             (this.v_tipo_fluxo!=null &&
              this.v_tipo_fluxo.equals(other.getV_tipo_fluxo()))) &&
            ((this.v_uf==null && other.getV_uf()==null) || 
             (this.v_uf!=null &&
              this.v_uf.equals(other.getV_uf()))) &&
            ((this.v_vl_entrada==null && other.getV_vl_entrada()==null) || 
             (this.v_vl_entrada!=null &&
              this.v_vl_entrada.equals(other.getV_vl_entrada()))) &&
            ((this.v_vl_principal==null && other.getV_vl_principal()==null) || 
             (this.v_vl_principal!=null &&
              this.v_vl_principal.equals(other.getV_vl_principal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getV_ano() != null) {
            _hashCode += getV_ano().hashCode();
        }
        if (getV_cd_molicar() != null) {
            _hashCode += getV_cd_molicar().hashCode();
        }
        if (getV_cidade() != null) {
            _hashCode += getV_cidade().hashCode();
        }
        if (getV_dt_vecto() != null) {
            _hashCode += getV_dt_vecto().hashCode();
        }
        if (getV_faixa() != null) {
            _hashCode += getV_faixa().hashCode();
        }
        if (getV_financia_iof() != null) {
            _hashCode += getV_financia_iof().hashCode();
        }
        if (getV_financia_iof_adic() != null) {
            _hashCode += getV_financia_iof_adic().hashCode();
        }
        if (getV_financia_reg_grav() != null) {
            _hashCode += getV_financia_reg_grav().hashCode();
        }
        if (getV_financia_retorno() != null) {
            _hashCode += getV_financia_retorno().hashCode();
        }
        if (getV_financia_serv_prest() != null) {
            _hashCode += getV_financia_serv_prest().hashCode();
        }
        if (getV_financia_ta() != null) {
            _hashCode += getV_financia_ta().hashCode();
        }
        if (getV_financia_tc() != null) {
            _hashCode += getV_financia_tc().hashCode();
        }
        if (getV_fl_taxi() != null) {
            _hashCode += getV_fl_taxi().hashCode();
        }
        if (getV_id_cliente() != null) {
            _hashCode += getV_id_cliente().hashCode();
        }
        if (getV_id_pessoa() != null) {
            _hashCode += getV_id_pessoa().hashCode();
        }
        if (getV_id_segmento() != null) {
            _hashCode += getV_id_segmento().hashCode();
        }
        if (getV_id_tc_paga() != null) {
            _hashCode += getV_id_tc_paga().hashCode();
        }
        if (getV_id_usuario_web() != null) {
            _hashCode += getV_id_usuario_web().hashCode();
        }
        if (getV_pc_ret() != null) {
            _hashCode += getV_pc_ret().hashCode();
        }
        if (getV_prazo() != null) {
            _hashCode += getV_prazo().hashCode();
        }
        if (getV_produto() != null) {
            _hashCode += getV_produto().hashCode();
        }
        if (getV_qtd_veiculo() != null) {
            _hashCode += getV_qtd_veiculo().hashCode();
        }
        if (getV_seguro() != null) {
            _hashCode += getV_seguro().hashCode();
        }
        if (getV_tabela() != null) {
            _hashCode += getV_tabela().hashCode();
        }
        if (getV_tipo_fluxo() != null) {
            _hashCode += getV_tipo_fluxo().hashCode();
        }
        if (getV_uf() != null) {
            _hashCode += getV_uf().hashCode();
        }
        if (getV_vl_entrada() != null) {
            _hashCode += getV_vl_entrada().hashCode();
        }
        if (getV_vl_principal() != null) {
            _hashCode += getV_vl_principal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ParamCalculaParcelas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ParamCalculaParcelas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_ano");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_ano"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_cd_molicar");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_cd_molicar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_cidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_cidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_dt_vecto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_dt_vecto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_faixa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_faixa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_iof");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_iof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_iof_adic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_iof_adic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_reg_grav");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_reg_grav"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_retorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_retorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_serv_prest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_serv_prest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_ta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_ta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_financia_tc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_financia_tc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_fl_taxi");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_fl_taxi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_id_pessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_id_pessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_id_segmento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_id_segmento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_id_tc_paga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_id_tc_paga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_id_usuario_web");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_id_usuario_web"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_pc_ret");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_pc_ret"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_prazo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_prazo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_produto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_produto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_qtd_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_qtd_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_seguro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_seguro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_tabela");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_tabela"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_tipo_fluxo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_tipo_fluxo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_uf");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_uf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_vl_entrada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_vl_entrada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_vl_principal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_vl_principal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
