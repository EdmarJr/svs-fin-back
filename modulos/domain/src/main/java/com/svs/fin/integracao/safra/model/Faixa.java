/**
 * Faixa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Faixa")
public class Faixa implements java.io.Serializable {
	private com.svs.fin.integracao.safra.model.Coeficiente[] coeficientes;

	@Id
	private java.lang.Integer idFaixa;

	private java.math.BigDecimal pcEntradaMinima;

	private java.math.BigDecimal pcPlus;

	private java.math.BigDecimal pcTaxa;

	private java.lang.Integer qtPrazo;

	private java.math.BigDecimal vlFinanciamentoMaximo;

	private java.math.BigDecimal vlFinanciamentoMinimo;

	private java.math.BigDecimal vlTC;

	public Faixa() {
	}

	public Faixa(com.svs.fin.integracao.safra.model.Coeficiente[] coeficientes, java.lang.Integer idFaixa,
			java.math.BigDecimal pcEntradaMinima, java.math.BigDecimal pcPlus, java.math.BigDecimal pcTaxa,
			java.lang.Integer qtPrazo, java.math.BigDecimal vlFinanciamentoMaximo,
			java.math.BigDecimal vlFinanciamentoMinimo, java.math.BigDecimal vlTC) {
		this.coeficientes = coeficientes;
		this.idFaixa = idFaixa;
		this.pcEntradaMinima = pcEntradaMinima;
		this.pcPlus = pcPlus;
		this.pcTaxa = pcTaxa;
		this.qtPrazo = qtPrazo;
		this.vlFinanciamentoMaximo = vlFinanciamentoMaximo;
		this.vlFinanciamentoMinimo = vlFinanciamentoMinimo;
		this.vlTC = vlTC;
	}

	/**
	 * Gets the coeficientes value for this Faixa.
	 * 
	 * @return coeficientes
	 */
	public com.svs.fin.integracao.safra.model.Coeficiente[] getCoeficientes() {
		return coeficientes;
	}

	/**
	 * Sets the coeficientes value for this Faixa.
	 * 
	 * @param coeficientes
	 */
	public void setCoeficientes(com.svs.fin.integracao.safra.model.Coeficiente[] coeficientes) {
		this.coeficientes = coeficientes;
	}

	/**
	 * Gets the idFaixa value for this Faixa.
	 * 
	 * @return idFaixa
	 */
	public java.lang.Integer getIdFaixa() {
		return idFaixa;
	}

	/**
	 * Sets the idFaixa value for this Faixa.
	 * 
	 * @param idFaixa
	 */
	public void setIdFaixa(java.lang.Integer idFaixa) {
		this.idFaixa = idFaixa;
	}

	/**
	 * Gets the pcEntradaMinima value for this Faixa.
	 * 
	 * @return pcEntradaMinima
	 */
	public java.math.BigDecimal getPcEntradaMinima() {
		return pcEntradaMinima;
	}

	/**
	 * Sets the pcEntradaMinima value for this Faixa.
	 * 
	 * @param pcEntradaMinima
	 */
	public void setPcEntradaMinima(java.math.BigDecimal pcEntradaMinima) {
		this.pcEntradaMinima = pcEntradaMinima;
	}

	/**
	 * Gets the pcPlus value for this Faixa.
	 * 
	 * @return pcPlus
	 */
	public java.math.BigDecimal getPcPlus() {
		return pcPlus;
	}

	/**
	 * Sets the pcPlus value for this Faixa.
	 * 
	 * @param pcPlus
	 */
	public void setPcPlus(java.math.BigDecimal pcPlus) {
		this.pcPlus = pcPlus;
	}

	/**
	 * Gets the pcTaxa value for this Faixa.
	 * 
	 * @return pcTaxa
	 */
	public java.math.BigDecimal getPcTaxa() {
		return pcTaxa;
	}

	/**
	 * Sets the pcTaxa value for this Faixa.
	 * 
	 * @param pcTaxa
	 */
	public void setPcTaxa(java.math.BigDecimal pcTaxa) {
		this.pcTaxa = pcTaxa;
	}

	/**
	 * Gets the qtPrazo value for this Faixa.
	 * 
	 * @return qtPrazo
	 */
	public java.lang.Integer getQtPrazo() {
		return qtPrazo;
	}

	/**
	 * Sets the qtPrazo value for this Faixa.
	 * 
	 * @param qtPrazo
	 */
	public void setQtPrazo(java.lang.Integer qtPrazo) {
		this.qtPrazo = qtPrazo;
	}

	/**
	 * Gets the vlFinanciamentoMaximo value for this Faixa.
	 * 
	 * @return vlFinanciamentoMaximo
	 */
	public java.math.BigDecimal getVlFinanciamentoMaximo() {
		return vlFinanciamentoMaximo;
	}

	/**
	 * Sets the vlFinanciamentoMaximo value for this Faixa.
	 * 
	 * @param vlFinanciamentoMaximo
	 */
	public void setVlFinanciamentoMaximo(java.math.BigDecimal vlFinanciamentoMaximo) {
		this.vlFinanciamentoMaximo = vlFinanciamentoMaximo;
	}

	/**
	 * Gets the vlFinanciamentoMinimo value for this Faixa.
	 * 
	 * @return vlFinanciamentoMinimo
	 */
	public java.math.BigDecimal getVlFinanciamentoMinimo() {
		return vlFinanciamentoMinimo;
	}

	/**
	 * Sets the vlFinanciamentoMinimo value for this Faixa.
	 * 
	 * @param vlFinanciamentoMinimo
	 */
	public void setVlFinanciamentoMinimo(java.math.BigDecimal vlFinanciamentoMinimo) {
		this.vlFinanciamentoMinimo = vlFinanciamentoMinimo;
	}

	/**
	 * Gets the vlTC value for this Faixa.
	 * 
	 * @return vlTC
	 */
	public java.math.BigDecimal getVlTC() {
		return vlTC;
	}

	/**
	 * Sets the vlTC value for this Faixa.
	 * 
	 * @param vlTC
	 */
	public void setVlTC(java.math.BigDecimal vlTC) {
		this.vlTC = vlTC;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Faixa))
			return false;
		Faixa other = (Faixa) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.coeficientes == null && other.getCoeficientes() == null)
				|| (this.coeficientes != null && java.util.Arrays.equals(this.coeficientes, other.getCoeficientes())))
				&& ((this.idFaixa == null && other.getIdFaixa() == null)
						|| (this.idFaixa != null && this.idFaixa.equals(other.getIdFaixa())))
				&& ((this.pcEntradaMinima == null && other.getPcEntradaMinima() == null)
						|| (this.pcEntradaMinima != null && this.pcEntradaMinima.equals(other.getPcEntradaMinima())))
				&& ((this.pcPlus == null && other.getPcPlus() == null)
						|| (this.pcPlus != null && this.pcPlus.equals(other.getPcPlus())))
				&& ((this.pcTaxa == null && other.getPcTaxa() == null)
						|| (this.pcTaxa != null && this.pcTaxa.equals(other.getPcTaxa())))
				&& ((this.qtPrazo == null && other.getQtPrazo() == null)
						|| (this.qtPrazo != null && this.qtPrazo.equals(other.getQtPrazo())))
				&& ((this.vlFinanciamentoMaximo == null && other.getVlFinanciamentoMaximo() == null)
						|| (this.vlFinanciamentoMaximo != null
								&& this.vlFinanciamentoMaximo.equals(other.getVlFinanciamentoMaximo())))
				&& ((this.vlFinanciamentoMinimo == null && other.getVlFinanciamentoMinimo() == null)
						|| (this.vlFinanciamentoMinimo != null
								&& this.vlFinanciamentoMinimo.equals(other.getVlFinanciamentoMinimo())))
				&& ((this.vlTC == null && other.getVlTC() == null)
						|| (this.vlTC != null && this.vlTC.equals(other.getVlTC())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getCoeficientes() != null) {
			for (int i = 0; i < java.lang.reflect.Array.getLength(getCoeficientes()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(getCoeficientes(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getIdFaixa() != null) {
			_hashCode += getIdFaixa().hashCode();
		}
		if (getPcEntradaMinima() != null) {
			_hashCode += getPcEntradaMinima().hashCode();
		}
		if (getPcPlus() != null) {
			_hashCode += getPcPlus().hashCode();
		}
		if (getPcTaxa() != null) {
			_hashCode += getPcTaxa().hashCode();
		}
		if (getQtPrazo() != null) {
			_hashCode += getQtPrazo().hashCode();
		}
		if (getVlFinanciamentoMaximo() != null) {
			_hashCode += getVlFinanciamentoMaximo().hashCode();
		}
		if (getVlFinanciamentoMinimo() != null) {
			_hashCode += getVlFinanciamentoMinimo().hashCode();
		}
		if (getVlTC() != null) {
			_hashCode += getVlTC().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(Faixa.class,
			true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixa"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("coeficientes");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficientes"));
		elemField.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficiente"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		elemField.setItemQName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficiente"));
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("idFaixa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "IdFaixa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("pcEntradaMinima");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "PcEntradaMinima"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("pcPlus");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "PcPlus"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("pcTaxa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "PcTaxa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("qtPrazo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "QtPrazo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlFinanciamentoMaximo");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"VlFinanciamentoMaximo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlFinanciamentoMinimo");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"VlFinanciamentoMinimo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlTC");
		elemField
				.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "VlTC"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
