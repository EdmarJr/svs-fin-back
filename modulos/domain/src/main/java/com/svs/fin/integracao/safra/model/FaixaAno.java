/**
 * FaixaAno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class FaixaAno  implements java.io.Serializable {
    private java.lang.Integer anoFinal;

    private java.lang.Integer anoInicial;

    private com.svs.fin.integracao.safra.model.Faixa[] faixas;

    public FaixaAno() {
    }

    public FaixaAno(
           java.lang.Integer anoFinal,
           java.lang.Integer anoInicial,
           com.svs.fin.integracao.safra.model.Faixa[] faixas) {
           this.anoFinal = anoFinal;
           this.anoInicial = anoInicial;
           this.faixas = faixas;
    }


    /**
     * Gets the anoFinal value for this FaixaAno.
     * 
     * @return anoFinal
     */
    public java.lang.Integer getAnoFinal() {
        return anoFinal;
    }


    /**
     * Sets the anoFinal value for this FaixaAno.
     * 
     * @param anoFinal
     */
    public void setAnoFinal(java.lang.Integer anoFinal) {
        this.anoFinal = anoFinal;
    }


    /**
     * Gets the anoInicial value for this FaixaAno.
     * 
     * @return anoInicial
     */
    public java.lang.Integer getAnoInicial() {
        return anoInicial;
    }


    /**
     * Sets the anoInicial value for this FaixaAno.
     * 
     * @param anoInicial
     */
    public void setAnoInicial(java.lang.Integer anoInicial) {
        this.anoInicial = anoInicial;
    }


    /**
     * Gets the faixas value for this FaixaAno.
     * 
     * @return faixas
     */
    public com.svs.fin.integracao.safra.model.Faixa[] getFaixas() {
        return faixas;
    }


    /**
     * Sets the faixas value for this FaixaAno.
     * 
     * @param faixas
     */
    public void setFaixas(com.svs.fin.integracao.safra.model.Faixa[] faixas) {
        this.faixas = faixas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FaixaAno)) return false;
        FaixaAno other = (FaixaAno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.anoFinal==null && other.getAnoFinal()==null) || 
             (this.anoFinal!=null &&
              this.anoFinal.equals(other.getAnoFinal()))) &&
            ((this.anoInicial==null && other.getAnoInicial()==null) || 
             (this.anoInicial!=null &&
              this.anoInicial.equals(other.getAnoInicial()))) &&
            ((this.faixas==null && other.getFaixas()==null) || 
             (this.faixas!=null &&
              java.util.Arrays.equals(this.faixas, other.getFaixas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAnoFinal() != null) {
            _hashCode += getAnoFinal().hashCode();
        }
        if (getAnoInicial() != null) {
            _hashCode += getAnoInicial().hashCode();
        }
        if (getFaixas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFaixas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFaixas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FaixaAno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaAno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anoFinal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "AnoFinal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anoInicial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "AnoInicial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faixas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixa"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
