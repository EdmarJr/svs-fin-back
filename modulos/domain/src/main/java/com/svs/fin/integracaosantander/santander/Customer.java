package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;
import java.util.Calendar;

public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8144765193484512510L;

	private Legal legal;

	private String document;

	private String email;

	private Address address;

	private String dateOfBirthFoundation;

	private String name;

	private String uuid;

	private String storeId;

	private Natural natural;

	private String ip;

	private Calendar dataNascimento;

	public Legal getLegal() {
		return legal;
	}

	public void setLegal(Legal legal) {
		this.legal = legal;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDateOfBirthFoundation() {
		return dateOfBirthFoundation;
	}

	public void setDateOfBirthFoundation(String dateOfBirthFoundation) {
		this.dateOfBirthFoundation = dateOfBirthFoundation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Natural getNatural() {
		return natural;
	}

	public void setNatural(Natural natural) {
		this.natural = natural;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "ClassPojo [legal = " + legal + ", document = " + document + ", email = " + email + ", address = "
				+ address + ", dateOfBirthFoundation = " + dateOfBirthFoundation + ", name = " + name + ", uuid = "
				+ uuid + ", storeId = " + storeId + ", natural = " + natural + ", ip = " + ip + "]";
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
