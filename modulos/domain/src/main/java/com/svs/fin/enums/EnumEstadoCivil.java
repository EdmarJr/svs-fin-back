package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumEstadoCivil {

//	SOLTEIRO("Solteiro(a)", "4"),
//	CASADO("Casado(a)", "1"),
//	DIVORCIADO("Divorciado(a)", "3"),
//	VIUVO("Viúvo(a)", "5"),
//	SEPARADO("Separado(a)", "6"),
//	COMPANHEIRO("Companheiro(a)", "11"),
//	SEPARADO_JUDICIALMENTE("Separado(a) judicialmente", "6");
	
	@JsonProperty("Solteiro(a)")
	SOLTEIRO("Solteiro(a)", "4"),
		@JsonProperty("Casado(a)")
	CASADO("Casado(a)", "1"),
		@JsonProperty("Divorciado(a)")
	DIVORCIADO("Divorciado(a)", "3"),
		@JsonProperty("Viúvo(a)")
	VIUVO("Viúvo(a)", "5"),
		@JsonProperty("Separado(a)")
	SEPARADO("Separado(a)", "6"),
		@JsonProperty("Companheiro(a)")
	COMPANHEIRO("Companheiro(a)", "11"),
		@JsonProperty("Separado(a) judicialmente")
	SEPARADO_JUDICIALMENTE("Separado(a) judicialmente", "6");
	
	private String idSantander;
	private String label;
	
	private EnumEstadoCivil(String label, String idSantander){
		this.label = label;
		this.idSantander = idSantander;
	}
	
	public String getLabel(){
		return label;
	}

	public String getIdSantander() {
		return idSantander;
	}
}
