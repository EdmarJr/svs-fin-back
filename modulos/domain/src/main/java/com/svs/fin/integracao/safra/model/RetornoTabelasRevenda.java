/**
 * RetornoTabelasRevenda.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class RetornoTabelasRevenda  extends com.svs.fin.integracao.safra.model.RetornoProposta  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model.TabelaJuros[] tabelasRevenda;

    public RetornoTabelasRevenda() {
    }

    public RetornoTabelasRevenda(
           java.lang.String id_proposta,
           java.lang.String id_retorno,
           com.svs.fin.integracao.safra.model.TabelaJuros[] tabelasRevenda) {
        super(
            id_proposta,
            id_retorno);
        this.tabelasRevenda = tabelasRevenda;
    }


    /**
     * Gets the tabelasRevenda value for this RetornoTabelasRevenda.
     * 
     * @return tabelasRevenda
     */
    public com.svs.fin.integracao.safra.model.TabelaJuros[] getTabelasRevenda() {
        return tabelasRevenda;
    }


    /**
     * Sets the tabelasRevenda value for this RetornoTabelasRevenda.
     * 
     * @param tabelasRevenda
     */
    public void setTabelasRevenda(com.svs.fin.integracao.safra.model.TabelaJuros[] tabelasRevenda) {
        this.tabelasRevenda = tabelasRevenda;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetornoTabelasRevenda)) return false;
        RetornoTabelasRevenda other = (RetornoTabelasRevenda) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tabelasRevenda==null && other.getTabelasRevenda()==null) || 
             (this.tabelasRevenda!=null &&
              java.util.Arrays.equals(this.tabelasRevenda, other.getTabelasRevenda())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTabelasRevenda() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTabelasRevenda());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTabelasRevenda(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetornoTabelasRevenda.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoTabelasRevenda"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tabelasRevenda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelasRevenda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelaJuros"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelaJuros"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
