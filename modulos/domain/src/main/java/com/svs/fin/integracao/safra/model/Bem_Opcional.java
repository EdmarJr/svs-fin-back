/**
 * Bem_Opcional.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Bem_Opcional")
public class Bem_Opcional implements java.io.Serializable {
	private java.lang.String ds_bem_opcional;

	@Id
	private java.lang.Integer id_bem_opcional;

	public Bem_Opcional() {
	}

	public Bem_Opcional(java.lang.String ds_bem_opcional, java.lang.Integer id_bem_opcional) {
		this.ds_bem_opcional = ds_bem_opcional;
		this.id_bem_opcional = id_bem_opcional;
	}

	/**
	 * Gets the ds_bem_opcional value for this Bem_Opcional.
	 * 
	 * @return ds_bem_opcional
	 */
	public java.lang.String getDs_bem_opcional() {
		return ds_bem_opcional;
	}

	/**
	 * Sets the ds_bem_opcional value for this Bem_Opcional.
	 * 
	 * @param ds_bem_opcional
	 */
	public void setDs_bem_opcional(java.lang.String ds_bem_opcional) {
		this.ds_bem_opcional = ds_bem_opcional;
	}

	/**
	 * Gets the id_bem_opcional value for this Bem_Opcional.
	 * 
	 * @return id_bem_opcional
	 */
	public java.lang.Integer getId_bem_opcional() {
		return id_bem_opcional;
	}

	/**
	 * Sets the id_bem_opcional value for this Bem_Opcional.
	 * 
	 * @param id_bem_opcional
	 */
	public void setId_bem_opcional(java.lang.Integer id_bem_opcional) {
		this.id_bem_opcional = id_bem_opcional;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Bem_Opcional))
			return false;
		Bem_Opcional other = (Bem_Opcional) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_bem_opcional == null && other.getDs_bem_opcional() == null)
						|| (this.ds_bem_opcional != null && this.ds_bem_opcional.equals(other.getDs_bem_opcional())))
				&& ((this.id_bem_opcional == null && other.getId_bem_opcional() == null)
						|| (this.id_bem_opcional != null && this.id_bem_opcional.equals(other.getId_bem_opcional())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_bem_opcional() != null) {
			_hashCode += getDs_bem_opcional().hashCode();
		}
		if (getId_bem_opcional() != null) {
			_hashCode += getId_bem_opcional().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Bem_Opcional.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Bem_Opcional"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_bem_opcional");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_bem_opcional"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_bem_opcional");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_bem_opcional"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
