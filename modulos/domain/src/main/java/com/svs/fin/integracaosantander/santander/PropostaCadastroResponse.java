package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class PropostaCadastroResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7813583302545297074L;

	private String proposalId;
	
	private String proposalExternalId;
	
	private String status;
	
	private String statusAdp;
	
	private String statusDescription;
	
	private String creationDate;
	
	private String[] errors;

	private String code;
	
	public String getProposalId() {
		return proposalId;
	}

	public void setProposalId(String proposalId) {
		this.proposalId = proposalId;
	}

	public String getProposalExternalId() {
		return proposalExternalId;
	}

	public void setProposalExternalId(String proposalExternalId) {
		this.proposalExternalId = proposalExternalId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusAdp() {
		return statusAdp;
	}

	public void setStatusAdp(String statusAdp) {
		this.statusAdp = statusAdp;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}	

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public String[] getErrors() {
		return errors;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
