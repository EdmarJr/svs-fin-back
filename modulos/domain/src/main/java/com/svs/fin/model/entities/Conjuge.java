package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.enums.EnumSexo;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conjuge implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8822404482584140511L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONJUGE")
	@SequenceGenerator(name = "SEQ_CONJUGE", sequenceName = "SEQ_CONJUGE", allocationSize = 1)
	private Long id;

	@Column
	private String nome;

	@Column
	private String cpf;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumSexo sexo;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEscolaridade escolaridade;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEstadoCivil estadoCivil;

	@Column(length = 100)
	private String telefoneResidencial;

	@Column(length = 100)
	private String telefoneCelular;

	@Column(length = 100)
	private String email;

	@Column(length = 100)
	private String rg;

	@Column(length = 100)
	private String orgaoEmissorRg;

	@Column(length = 100)
	private String ufEmissorRg;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEstados ufOrgaoEmissorDocRg;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumOrgaoEmissorDocumento orgaoEmissorDocRg;

	@Column
	private Calendar dataNascimento;

	@Column(length = 100)
	private String nacionalidade;

	@Column(length = 100)
	private String cidadeNascimento;

	@Column(length = 100)
	private String ufNascimento;

	@Column(length = 100)
	private String nomePai;

	@Column(length = 100)
	private String nomeMae;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ocupacao_id")
	private Ocupacao ocupacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public EnumSexo getSexo() {
		return sexo;
	}

	public void setSexo(EnumSexo sexo) {
		this.sexo = sexo;
	}

	public EnumEscolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(EnumEscolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public EnumEstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EnumEstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoEmissorRg() {
		return orgaoEmissorRg;
	}

	public void setOrgaoEmissorRg(String orgaoEmissorRg) {
		this.orgaoEmissorRg = orgaoEmissorRg;
	}

	public String getUfEmissorRg() {
		return ufEmissorRg;
	}

	public void setUfEmissorRg(String ufEmissorRg) {
		this.ufEmissorRg = ufEmissorRg;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getCidadeNascimento() {
		return cidadeNascimento;
	}

	public void setCidadeNascimento(String cidadeNascimento) {
		this.cidadeNascimento = cidadeNascimento;
	}

	public String getUfNascimento() {
		return ufNascimento;
	}

	public void setUfNascimento(String ufNascimento) {
		this.ufNascimento = ufNascimento;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Ocupacao getOcupacao() {
		if (ocupacao == null) {
			this.ocupacao = new Ocupacao();
		}
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumEstados getUfOrgaoEmissorDocRg() {
		return ufOrgaoEmissorDocRg;
	}

	public void setUfOrgaoEmissorDocRg(EnumEstados ufOrgaoEmissorDocRg) {
		this.ufOrgaoEmissorDocRg = ufOrgaoEmissorDocRg;
	}

	public EnumOrgaoEmissorDocumento getOrgaoEmissorDocRg() {
		return orgaoEmissorDocRg;
	}

	public void setOrgaoEmissorDocRg(EnumOrgaoEmissorDocumento orgaoEmissorDocRg) {
		this.orgaoEmissorDocRg = orgaoEmissorDocRg;
	}
}
