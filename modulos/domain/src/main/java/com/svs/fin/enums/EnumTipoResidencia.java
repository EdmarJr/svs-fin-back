package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoResidencia {

//	PROPRIA("Própria"), ALUGADA("Alugada"), CEDIDO("Cedido"), OUTROS("Outros");

	@JsonProperty("Própria")
	PROPRIA("Própria"), @JsonProperty("Alugada")
	ALUGADA("Alugada"), @JsonProperty("Cedido")
	CEDIDO("Cedido"), @JsonProperty("Outros")
	OUTROS("Outros");

	EnumTipoResidencia(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}

}
