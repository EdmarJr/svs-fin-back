/**
 * Imovel_Tipo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Imovel_Tipo")
public class Imovel_Tipo implements java.io.Serializable {
	private java.lang.String ds_imovel_tipo;

	private java.lang.Integer id_imovel_especie;

	@Id
	private java.lang.Integer id_imovel_tipo;

	public Imovel_Tipo() {
	}

	public Imovel_Tipo(java.lang.String ds_imovel_tipo, java.lang.Integer id_imovel_especie,
			java.lang.Integer id_imovel_tipo) {
		this.ds_imovel_tipo = ds_imovel_tipo;
		this.id_imovel_especie = id_imovel_especie;
		this.id_imovel_tipo = id_imovel_tipo;
	}

	/**
	 * Gets the ds_imovel_tipo value for this Imovel_Tipo.
	 * 
	 * @return ds_imovel_tipo
	 */
	public java.lang.String getDs_imovel_tipo() {
		return ds_imovel_tipo;
	}

	/**
	 * Sets the ds_imovel_tipo value for this Imovel_Tipo.
	 * 
	 * @param ds_imovel_tipo
	 */
	public void setDs_imovel_tipo(java.lang.String ds_imovel_tipo) {
		this.ds_imovel_tipo = ds_imovel_tipo;
	}

	/**
	 * Gets the id_imovel_especie value for this Imovel_Tipo.
	 * 
	 * @return id_imovel_especie
	 */
	public java.lang.Integer getId_imovel_especie() {
		return id_imovel_especie;
	}

	/**
	 * Sets the id_imovel_especie value for this Imovel_Tipo.
	 * 
	 * @param id_imovel_especie
	 */
	public void setId_imovel_especie(java.lang.Integer id_imovel_especie) {
		this.id_imovel_especie = id_imovel_especie;
	}

	/**
	 * Gets the id_imovel_tipo value for this Imovel_Tipo.
	 * 
	 * @return id_imovel_tipo
	 */
	public java.lang.Integer getId_imovel_tipo() {
		return id_imovel_tipo;
	}

	/**
	 * Sets the id_imovel_tipo value for this Imovel_Tipo.
	 * 
	 * @param id_imovel_tipo
	 */
	public void setId_imovel_tipo(java.lang.Integer id_imovel_tipo) {
		this.id_imovel_tipo = id_imovel_tipo;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Imovel_Tipo))
			return false;
		Imovel_Tipo other = (Imovel_Tipo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_imovel_tipo == null && other.getDs_imovel_tipo() == null)
						|| (this.ds_imovel_tipo != null && this.ds_imovel_tipo.equals(other.getDs_imovel_tipo())))
				&& ((this.id_imovel_especie == null && other.getId_imovel_especie() == null)
						|| (this.id_imovel_especie != null
								&& this.id_imovel_especie.equals(other.getId_imovel_especie())))
				&& ((this.id_imovel_tipo == null && other.getId_imovel_tipo() == null)
						|| (this.id_imovel_tipo != null && this.id_imovel_tipo.equals(other.getId_imovel_tipo())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_imovel_tipo() != null) {
			_hashCode += getDs_imovel_tipo().hashCode();
		}
		if (getId_imovel_especie() != null) {
			_hashCode += getId_imovel_especie().hashCode();
		}
		if (getId_imovel_tipo() != null) {
			_hashCode += getId_imovel_tipo().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Imovel_Tipo.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Tipo"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_imovel_tipo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_imovel_tipo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_imovel_especie");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_imovel_especie"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_imovel_tipo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_imovel_tipo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
