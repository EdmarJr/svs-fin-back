/**
 * Proposta_PJ.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Proposta_PJ  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model_comum.Contato_Cadastro contato_Cadastro;

    private com.svs.fin.integracao.safra.model_comum.Acionista_Socio[] acionista_socio;

    private com.svs.fin.integracao.safra.model_comum.Endividamento[] endividamento;

    private com.svs.fin.integracao.safra.model_comum.Outros_Dados_Complemento outros_dados_complemento;

    private com.svs.fin.integracao.safra.model_comum.Outros_Dados_Diversos outros_dados_diversos;

    private com.svs.fin.integracao.safra.model_comum.Outros_Dados_Faturamento_Mensal outros_dados_faturamento_mensal;

    private com.svs.fin.integracao.safra.model_comum.Produto_Vendido[] produto_vendido;

    private com.svs.fin.integracao.safra.model_comum.Referencia[] referencia;

    private com.svs.fin.integracao.safra.model_comum.Socio_Participacao[] socio_participacao;

    public Proposta_PJ() {
    }

    public Proposta_PJ(
           com.svs.fin.integracao.safra.model_comum.Contato_Cadastro contato_Cadastro,
           com.svs.fin.integracao.safra.model_comum.Acionista_Socio[] acionista_socio,
           com.svs.fin.integracao.safra.model_comum.Endividamento[] endividamento,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados_Complemento outros_dados_complemento,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados_Diversos outros_dados_diversos,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados_Faturamento_Mensal outros_dados_faturamento_mensal,
           com.svs.fin.integracao.safra.model_comum.Produto_Vendido[] produto_vendido,
           com.svs.fin.integracao.safra.model_comum.Referencia[] referencia,
           com.svs.fin.integracao.safra.model_comum.Socio_Participacao[] socio_participacao) {
           this.contato_Cadastro = contato_Cadastro;
           this.acionista_socio = acionista_socio;
           this.endividamento = endividamento;
           this.outros_dados_complemento = outros_dados_complemento;
           this.outros_dados_diversos = outros_dados_diversos;
           this.outros_dados_faturamento_mensal = outros_dados_faturamento_mensal;
           this.produto_vendido = produto_vendido;
           this.referencia = referencia;
           this.socio_participacao = socio_participacao;
    }


    /**
     * Gets the contato_Cadastro value for this Proposta_PJ.
     * 
     * @return contato_Cadastro
     */
    public com.svs.fin.integracao.safra.model_comum.Contato_Cadastro getContato_Cadastro() {
        return contato_Cadastro;
    }


    /**
     * Sets the contato_Cadastro value for this Proposta_PJ.
     * 
     * @param contato_Cadastro
     */
    public void setContato_Cadastro(com.svs.fin.integracao.safra.model_comum.Contato_Cadastro contato_Cadastro) {
        this.contato_Cadastro = contato_Cadastro;
    }


    /**
     * Gets the acionista_socio value for this Proposta_PJ.
     * 
     * @return acionista_socio
     */
    public com.svs.fin.integracao.safra.model_comum.Acionista_Socio[] getAcionista_socio() {
        return acionista_socio;
    }


    /**
     * Sets the acionista_socio value for this Proposta_PJ.
     * 
     * @param acionista_socio
     */
    public void setAcionista_socio(com.svs.fin.integracao.safra.model_comum.Acionista_Socio[] acionista_socio) {
        this.acionista_socio = acionista_socio;
    }


    /**
     * Gets the endividamento value for this Proposta_PJ.
     * 
     * @return endividamento
     */
    public com.svs.fin.integracao.safra.model_comum.Endividamento[] getEndividamento() {
        return endividamento;
    }


    /**
     * Sets the endividamento value for this Proposta_PJ.
     * 
     * @param endividamento
     */
    public void setEndividamento(com.svs.fin.integracao.safra.model_comum.Endividamento[] endividamento) {
        this.endividamento = endividamento;
    }


    /**
     * Gets the outros_dados_complemento value for this Proposta_PJ.
     * 
     * @return outros_dados_complemento
     */
    public com.svs.fin.integracao.safra.model_comum.Outros_Dados_Complemento getOutros_dados_complemento() {
        return outros_dados_complemento;
    }


    /**
     * Sets the outros_dados_complemento value for this Proposta_PJ.
     * 
     * @param outros_dados_complemento
     */
    public void setOutros_dados_complemento(com.svs.fin.integracao.safra.model_comum.Outros_Dados_Complemento outros_dados_complemento) {
        this.outros_dados_complemento = outros_dados_complemento;
    }


    /**
     * Gets the outros_dados_diversos value for this Proposta_PJ.
     * 
     * @return outros_dados_diversos
     */
    public com.svs.fin.integracao.safra.model_comum.Outros_Dados_Diversos getOutros_dados_diversos() {
        return outros_dados_diversos;
    }


    /**
     * Sets the outros_dados_diversos value for this Proposta_PJ.
     * 
     * @param outros_dados_diversos
     */
    public void setOutros_dados_diversos(com.svs.fin.integracao.safra.model_comum.Outros_Dados_Diversos outros_dados_diversos) {
        this.outros_dados_diversos = outros_dados_diversos;
    }


    /**
     * Gets the outros_dados_faturamento_mensal value for this Proposta_PJ.
     * 
     * @return outros_dados_faturamento_mensal
     */
    public com.svs.fin.integracao.safra.model_comum.Outros_Dados_Faturamento_Mensal getOutros_dados_faturamento_mensal() {
        return outros_dados_faturamento_mensal;
    }


    /**
     * Sets the outros_dados_faturamento_mensal value for this Proposta_PJ.
     * 
     * @param outros_dados_faturamento_mensal
     */
    public void setOutros_dados_faturamento_mensal(com.svs.fin.integracao.safra.model_comum.Outros_Dados_Faturamento_Mensal outros_dados_faturamento_mensal) {
        this.outros_dados_faturamento_mensal = outros_dados_faturamento_mensal;
    }


    /**
     * Gets the produto_vendido value for this Proposta_PJ.
     * 
     * @return produto_vendido
     */
    public com.svs.fin.integracao.safra.model_comum.Produto_Vendido[] getProduto_vendido() {
        return produto_vendido;
    }


    /**
     * Sets the produto_vendido value for this Proposta_PJ.
     * 
     * @param produto_vendido
     */
    public void setProduto_vendido(com.svs.fin.integracao.safra.model_comum.Produto_Vendido[] produto_vendido) {
        this.produto_vendido = produto_vendido;
    }


    /**
     * Gets the referencia value for this Proposta_PJ.
     * 
     * @return referencia
     */
    public com.svs.fin.integracao.safra.model_comum.Referencia[] getReferencia() {
        return referencia;
    }


    /**
     * Sets the referencia value for this Proposta_PJ.
     * 
     * @param referencia
     */
    public void setReferencia(com.svs.fin.integracao.safra.model_comum.Referencia[] referencia) {
        this.referencia = referencia;
    }


    /**
     * Gets the socio_participacao value for this Proposta_PJ.
     * 
     * @return socio_participacao
     */
    public com.svs.fin.integracao.safra.model_comum.Socio_Participacao[] getSocio_participacao() {
        return socio_participacao;
    }


    /**
     * Sets the socio_participacao value for this Proposta_PJ.
     * 
     * @param socio_participacao
     */
    public void setSocio_participacao(com.svs.fin.integracao.safra.model_comum.Socio_Participacao[] socio_participacao) {
        this.socio_participacao = socio_participacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Proposta_PJ)) return false;
        Proposta_PJ other = (Proposta_PJ) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.contato_Cadastro==null && other.getContato_Cadastro()==null) || 
             (this.contato_Cadastro!=null &&
              this.contato_Cadastro.equals(other.getContato_Cadastro()))) &&
            ((this.acionista_socio==null && other.getAcionista_socio()==null) || 
             (this.acionista_socio!=null &&
              java.util.Arrays.equals(this.acionista_socio, other.getAcionista_socio()))) &&
            ((this.endividamento==null && other.getEndividamento()==null) || 
             (this.endividamento!=null &&
              java.util.Arrays.equals(this.endividamento, other.getEndividamento()))) &&
            ((this.outros_dados_complemento==null && other.getOutros_dados_complemento()==null) || 
             (this.outros_dados_complemento!=null &&
              this.outros_dados_complemento.equals(other.getOutros_dados_complemento()))) &&
            ((this.outros_dados_diversos==null && other.getOutros_dados_diversos()==null) || 
             (this.outros_dados_diversos!=null &&
              this.outros_dados_diversos.equals(other.getOutros_dados_diversos()))) &&
            ((this.outros_dados_faturamento_mensal==null && other.getOutros_dados_faturamento_mensal()==null) || 
             (this.outros_dados_faturamento_mensal!=null &&
              this.outros_dados_faturamento_mensal.equals(other.getOutros_dados_faturamento_mensal()))) &&
            ((this.produto_vendido==null && other.getProduto_vendido()==null) || 
             (this.produto_vendido!=null &&
              java.util.Arrays.equals(this.produto_vendido, other.getProduto_vendido()))) &&
            ((this.referencia==null && other.getReferencia()==null) || 
             (this.referencia!=null &&
              java.util.Arrays.equals(this.referencia, other.getReferencia()))) &&
            ((this.socio_participacao==null && other.getSocio_participacao()==null) || 
             (this.socio_participacao!=null &&
              java.util.Arrays.equals(this.socio_participacao, other.getSocio_participacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContato_Cadastro() != null) {
            _hashCode += getContato_Cadastro().hashCode();
        }
        if (getAcionista_socio() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAcionista_socio());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAcionista_socio(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEndividamento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEndividamento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEndividamento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOutros_dados_complemento() != null) {
            _hashCode += getOutros_dados_complemento().hashCode();
        }
        if (getOutros_dados_diversos() != null) {
            _hashCode += getOutros_dados_diversos().hashCode();
        }
        if (getOutros_dados_faturamento_mensal() != null) {
            _hashCode += getOutros_dados_faturamento_mensal().hashCode();
        }
        if (getProduto_vendido() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProduto_vendido());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProduto_vendido(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReferencia() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferencia());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferencia(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSocio_participacao() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSocio_participacao());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSocio_participacao(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Proposta_PJ.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_PJ"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contato_Cadastro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Contato_Cadastro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Contato_Cadastro"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acionista_socio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "acionista_socio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Acionista_Socio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Acionista_Socio"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endividamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "endividamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endividamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endividamento"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outros_dados_complemento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "outros_dados_complemento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Complemento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outros_dados_diversos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "outros_dados_diversos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Diversos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outros_dados_faturamento_mensal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "outros_dados_faturamento_mensal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Faturamento_Mensal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("produto_vendido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "produto_vendido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Produto_Vendido"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Produto_Vendido"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("socio_participacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "socio_participacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Socio_Participacao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Socio_Participacao"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
