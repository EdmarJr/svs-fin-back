/**
 * Financiamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Financiamento  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model_dados_financiamento.Fluxo fluxo;

    private com.svs.fin.integracao.safra.model_dados_financiamento.Veiculo veiculo;

    public Financiamento() {
    }

    public Financiamento(
           com.svs.fin.integracao.safra.model_dados_financiamento.Fluxo fluxo,
           com.svs.fin.integracao.safra.model_dados_financiamento.Veiculo veiculo) {
           this.fluxo = fluxo;
           this.veiculo = veiculo;
    }


    /**
     * Gets the fluxo value for this Financiamento.
     * 
     * @return fluxo
     */
    public com.svs.fin.integracao.safra.model_dados_financiamento.Fluxo getFluxo() {
        return fluxo;
    }


    /**
     * Sets the fluxo value for this Financiamento.
     * 
     * @param fluxo
     */
    public void setFluxo(com.svs.fin.integracao.safra.model_dados_financiamento.Fluxo fluxo) {
        this.fluxo = fluxo;
    }


    /**
     * Gets the veiculo value for this Financiamento.
     * 
     * @return veiculo
     */
    public com.svs.fin.integracao.safra.model_dados_financiamento.Veiculo getVeiculo() {
        return veiculo;
    }


    /**
     * Sets the veiculo value for this Financiamento.
     * 
     * @param veiculo
     */
    public void setVeiculo(com.svs.fin.integracao.safra.model_dados_financiamento.Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Financiamento)) return false;
        Financiamento other = (Financiamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fluxo==null && other.getFluxo()==null) || 
             (this.fluxo!=null &&
              this.fluxo.equals(other.getFluxo()))) &&
            ((this.veiculo==null && other.getVeiculo()==null) || 
             (this.veiculo!=null &&
              this.veiculo.equals(other.getVeiculo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFluxo() != null) {
            _hashCode += getFluxo().hashCode();
        }
        if (getVeiculo() != null) {
            _hashCode += getVeiculo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Financiamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Financiamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fluxo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fluxo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Fluxo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Veiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
