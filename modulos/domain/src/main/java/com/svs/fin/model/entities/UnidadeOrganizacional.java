package com.svs.fin.model.entities;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.utils.CADConstants;

@Entity(name = "UnidadeOrganizacional")
@NamedQueries({
	@NamedQuery(name = UnidadeOrganizacional.NQ_OBTER_POR_CNPJ.NAME, query = UnidadeOrganizacional.NQ_OBTER_POR_CNPJ.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class UnidadeOrganizacional {

	public static interface NQ_OBTER_POR_CNPJ {
		public static final String NAME = "UnidadeOrganizacional.obterPorCnpj";
		public static final String JPQL = "Select u from UnidadeOrganizacional u where u.cnpj = :"
				+ NQ_OBTER_POR_CNPJ.NM_PM_CNPJ;
		public static final String NM_PM_CNPJ = "cnpj";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UNIDADEORGANIZACIONAL")
	@SequenceGenerator(name = "SEQ_UNIDADEORGANIZACIONAL", sequenceName = "SEQ_UNIDADEORGANIZACIONAL", allocationSize = 1)
	private Long id;

	@Column(length = 100, unique = true, nullable = false)
	private String nomeFantasia;

	@Column(length = 100, unique = true, nullable = false)
	private String cnpj;

	@Column
	private Boolean ativo;

	@Column
	private Boolean integra;

	@Column(length = 100, unique = true)
	private String emailIntegracao;

	@Column
	private Calendar dataInicioIntegracao;

	@Column(name = "id_emp_ext")
	private String idEmpExterno;

	@Column(name = "sistema")
	private String sistemaParaIntegracao;

	public String getSistemaParaIntegracao() {
		if (sistemaParaIntegracao == null) {
			setSistemaParaIntegracao("");
		}
		return sistemaParaIntegracao;
	}

	public void setSistemaParaIntegracao(String sistemaParaIntegracao) {
		if (sistemaParaIntegracao != null && (sistemaParaIntegracao.equalsIgnoreCase("FIATNET")
				|| sistemaParaIntegracao.equalsIgnoreCase("SISDIA") || sistemaParaIntegracao.equalsIgnoreCase("NBS")
				|| sistemaParaIntegracao.equalsIgnoreCase("DR") || sistemaParaIntegracao.equalsIgnoreCase("DEALER_WKF")
				|| sistemaParaIntegracao.equalsIgnoreCase("UAU"))) {

			this.sistemaParaIntegracao = sistemaParaIntegracao;

		}
	}

	public String getIdEmpExterno() {
		if ((idEmpExterno == null) && getSistemaParaIntegracao() != null
				&& getSistemaParaIntegracao().equalsIgnoreCase("SISDIA")) {

			if (getCnpj().equalsIgnoreCase(CADConstants.CNPJ_SAGA_MOTORS_GYN)) {

				return "1";

			} else if (getCnpj().equalsIgnoreCase(CADConstants.CNPJ_SAGA_MOTORS_ANA)) {

				return "2";
			} else if (getCnpj().equalsIgnoreCase(CADConstants.CNPJ_SAGA_MOTORS_BSB)) {

				return "3";

			} else {
				return idEmpExterno;
			}

		} else
			return idEmpExterno;
	}

	public void setIdEmpExterno(String idEmpExterno) {
		this.idEmpExterno = idEmpExterno;
	}

	public UnidadeOrganizacional() {
	}

	public UnidadeOrganizacional(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getIntegra() {
		return integra;
	}

	public void setIntegra(Boolean integra) {
		this.integra = integra;
	}

	public String getEmailIntegracao() {
		return emailIntegracao;
	}

	public void setEmailIntegracao(String emailIntegracao) {
		this.emailIntegracao = emailIntegracao;
	}

	public Calendar getDataInicioIntegracao() {
		return dataInicioIntegracao;
	}

	public void setDataInicioIntegracao(Calendar dataInicioIntegracao) {
		this.dataInicioIntegracao = dataInicioIntegracao;
	}
}
