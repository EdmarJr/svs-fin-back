/**
 * Moeda.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Moeda")
public class Moeda implements java.io.Serializable {
	private java.lang.String ds_moeda;

	private java.lang.String fl_default;

	@Id
	private java.lang.Integer id_moeda;

	public Moeda() {
	}

	public Moeda(java.lang.String ds_moeda, java.lang.String fl_default, java.lang.Integer id_moeda) {
		this.ds_moeda = ds_moeda;
		this.fl_default = fl_default;
		this.id_moeda = id_moeda;
	}

	/**
	 * Gets the ds_moeda value for this Moeda.
	 * 
	 * @return ds_moeda
	 */
	public java.lang.String getDs_moeda() {
		return ds_moeda;
	}

	/**
	 * Sets the ds_moeda value for this Moeda.
	 * 
	 * @param ds_moeda
	 */
	public void setDs_moeda(java.lang.String ds_moeda) {
		this.ds_moeda = ds_moeda;
	}

	/**
	 * Gets the fl_default value for this Moeda.
	 * 
	 * @return fl_default
	 */
	public java.lang.String getFl_default() {
		return fl_default;
	}

	/**
	 * Sets the fl_default value for this Moeda.
	 * 
	 * @param fl_default
	 */
	public void setFl_default(java.lang.String fl_default) {
		this.fl_default = fl_default;
	}

	/**
	 * Gets the id_moeda value for this Moeda.
	 * 
	 * @return id_moeda
	 */
	public java.lang.Integer getId_moeda() {
		return id_moeda;
	}

	/**
	 * Sets the id_moeda value for this Moeda.
	 * 
	 * @param id_moeda
	 */
	public void setId_moeda(java.lang.Integer id_moeda) {
		this.id_moeda = id_moeda;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Moeda))
			return false;
		Moeda other = (Moeda) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_moeda == null && other.getDs_moeda() == null)
						|| (this.ds_moeda != null && this.ds_moeda.equals(other.getDs_moeda())))
				&& ((this.fl_default == null && other.getFl_default() == null)
						|| (this.fl_default != null && this.fl_default.equals(other.getFl_default())))
				&& ((this.id_moeda == null && other.getId_moeda() == null)
						|| (this.id_moeda != null && this.id_moeda.equals(other.getId_moeda())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_moeda() != null) {
			_hashCode += getDs_moeda().hashCode();
		}
		if (getFl_default() != null) {
			_hashCode += getFl_default().hashCode();
		}
		if (getId_moeda() != null) {
			_hashCode += getId_moeda().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(Moeda.class,
			true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Moeda"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_moeda");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_moeda"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("fl_default");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fl_default"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_moeda");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_moeda"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
