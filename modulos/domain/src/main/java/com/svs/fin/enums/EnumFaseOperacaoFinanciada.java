package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumFaseOperacaoFinanciada {

	@JsonProperty("Primeira venda")
	PRIMEIRA_VENDA("Primeira venda"), @JsonProperty("Cbc")
	CBC("Cbc"), @JsonProperty("Aprovação de parcela")
	APROVACAO_DE_PARCELA("Aprovação de parcela"), @JsonProperty("Análise mesa")
	ANALISE_MESA("Análise mesa"), @JsonProperty("Informação recusada")
	INFORMACAO_RECUSADA("Informação recusada"), @JsonProperty("Análise crédito")
	ANALISE_CREDITO("Análise crédito"), @JsonProperty("Recusado")
	RECUSADO("Recusado"), @JsonProperty("Autorização faturamento")
	AUTORIZACAO_FATURAMENTO("Autorização faturamento"), @JsonProperty("Faturar")
	FATURAR("Faturar");

	private EnumFaseOperacaoFinanciada(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
