package com.svs.fin.integracaoItau;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gruposaga.sdk.zflow.model.gen.SimulationOperationResponse;

@Entity(name="JSON_SIM_RESP_ITAU")
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonSimulationOperationResponseItau implements Serializable{

	private static final long serialVersionUID = -2115612072355513674L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@Column
	private Calendar data;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	private byte[] jsonByte;
	
	@Transient
	private String jsonTexto;
	
	@Transient
	private SimulationOperationResponse simulationOperationResponse;
	
	public String getJsonTexto() {
		if(jsonByte != null && jsonTexto == null) {
			setJsonTexto(new String(jsonByte, Charset.forName("UTF-8")));
		}
		return jsonTexto;
	}
	
	public String getJsonTextoFormatado() {
		String jsonTexto = getJsonTexto();
//		if(jsonTexto != null && !jsonTexto.trim().equals("")) {
//			SimulationOperationResponse simulationResponse = new Gson().fromJson(getJsonTexto(), SimulationOperationResponse.class);
//			return StringUtils.getObjetoComoJsonFormatado(simulationResponse);
//		}
		return jsonTexto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public byte[] getJsonByte() {
		return jsonByte;
	}

	public void setJsonByte(byte[] jsonByte) {
		this.jsonByte = jsonByte;
	}

	public void setJsonTexto(String jsonTexto) {
		this.jsonTexto = jsonTexto;
	}

	public SimulationOperationResponse getSimulationOperationResponse() {
		return simulationOperationResponse;
	}

	public void setSimulationOperationResponse(SimulationOperationResponse simulationOperationResponse) {
		this.simulationOperationResponse = simulationOperationResponse;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
