/**
 * Profissao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Profissao")
public class Profissao implements java.io.Serializable {
	private java.lang.String ds_profissao;

	private java.lang.Integer id_natureza_ocupacao;

	@Id
	private java.lang.Integer id_profissao;

	public Profissao() {
	}

	public Profissao(java.lang.String ds_profissao, java.lang.Integer id_natureza_ocupacao,
			java.lang.Integer id_profissao) {
		this.ds_profissao = ds_profissao;
		this.id_natureza_ocupacao = id_natureza_ocupacao;
		this.id_profissao = id_profissao;
	}

	/**
	 * Gets the ds_profissao value for this Profissao.
	 * 
	 * @return ds_profissao
	 */
	public java.lang.String getDs_profissao() {
		return ds_profissao;
	}

	/**
	 * Sets the ds_profissao value for this Profissao.
	 * 
	 * @param ds_profissao
	 */
	public void setDs_profissao(java.lang.String ds_profissao) {
		this.ds_profissao = ds_profissao;
	}

	/**
	 * Gets the id_natureza_ocupacao value for this Profissao.
	 * 
	 * @return id_natureza_ocupacao
	 */
	public java.lang.Integer getId_natureza_ocupacao() {
		return id_natureza_ocupacao;
	}

	/**
	 * Sets the id_natureza_ocupacao value for this Profissao.
	 * 
	 * @param id_natureza_ocupacao
	 */
	public void setId_natureza_ocupacao(java.lang.Integer id_natureza_ocupacao) {
		this.id_natureza_ocupacao = id_natureza_ocupacao;
	}

	/**
	 * Gets the id_profissao value for this Profissao.
	 * 
	 * @return id_profissao
	 */
	public java.lang.Integer getId_profissao() {
		return id_profissao;
	}

	/**
	 * Sets the id_profissao value for this Profissao.
	 * 
	 * @param id_profissao
	 */
	public void setId_profissao(java.lang.Integer id_profissao) {
		this.id_profissao = id_profissao;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Profissao))
			return false;
		Profissao other = (Profissao) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_profissao == null && other.getDs_profissao() == null)
						|| (this.ds_profissao != null && this.ds_profissao.equals(other.getDs_profissao())))
				&& ((this.id_natureza_ocupacao == null && other.getId_natureza_ocupacao() == null)
						|| (this.id_natureza_ocupacao != null
								&& this.id_natureza_ocupacao.equals(other.getId_natureza_ocupacao())))
				&& ((this.id_profissao == null && other.getId_profissao() == null)
						|| (this.id_profissao != null && this.id_profissao.equals(other.getId_profissao())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_profissao() != null) {
			_hashCode += getDs_profissao().hashCode();
		}
		if (getId_natureza_ocupacao() != null) {
			_hashCode += getId_natureza_ocupacao().hashCode();
		}
		if (getId_profissao() != null) {
			_hashCode += getId_profissao().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Profissao.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Profissao"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_profissao");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_profissao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_natureza_ocupacao");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_natureza_ocupacao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_profissao");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_profissao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
