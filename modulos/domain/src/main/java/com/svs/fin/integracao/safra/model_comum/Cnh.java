/**
 * Cnh.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Cnh  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_categoria_cnh;

    private java.lang.String dt_emissao_cnh;

    private java.lang.String dt_validade_cnh;

    private java.lang.String id_tem_cnh;

    private java.lang.String id_uf_emissao_cnh;

    private java.lang.String nr_cnh;

    private java.lang.String nr_registro;

    private java.lang.String nr_seguranca;

    public Cnh() {
    }

    public Cnh(
           java.lang.String ds_categoria_cnh,
           java.lang.String dt_emissao_cnh,
           java.lang.String dt_validade_cnh,
           java.lang.String id_tem_cnh,
           java.lang.String id_uf_emissao_cnh,
           java.lang.String nr_cnh,
           java.lang.String nr_registro,
           java.lang.String nr_seguranca) {
        this.ds_categoria_cnh = ds_categoria_cnh;
        this.dt_emissao_cnh = dt_emissao_cnh;
        this.dt_validade_cnh = dt_validade_cnh;
        this.id_tem_cnh = id_tem_cnh;
        this.id_uf_emissao_cnh = id_uf_emissao_cnh;
        this.nr_cnh = nr_cnh;
        this.nr_registro = nr_registro;
        this.nr_seguranca = nr_seguranca;
    }


    /**
     * Gets the ds_categoria_cnh value for this Cnh.
     * 
     * @return ds_categoria_cnh
     */
    public java.lang.String getDs_categoria_cnh() {
        return ds_categoria_cnh;
    }


    /**
     * Sets the ds_categoria_cnh value for this Cnh.
     * 
     * @param ds_categoria_cnh
     */
    public void setDs_categoria_cnh(java.lang.String ds_categoria_cnh) {
        this.ds_categoria_cnh = ds_categoria_cnh;
    }


    /**
     * Gets the dt_emissao_cnh value for this Cnh.
     * 
     * @return dt_emissao_cnh
     */
    public java.lang.String getDt_emissao_cnh() {
        return dt_emissao_cnh;
    }


    /**
     * Sets the dt_emissao_cnh value for this Cnh.
     * 
     * @param dt_emissao_cnh
     */
    public void setDt_emissao_cnh(java.lang.String dt_emissao_cnh) {
        this.dt_emissao_cnh = dt_emissao_cnh;
    }


    /**
     * Gets the dt_validade_cnh value for this Cnh.
     * 
     * @return dt_validade_cnh
     */
    public java.lang.String getDt_validade_cnh() {
        return dt_validade_cnh;
    }


    /**
     * Sets the dt_validade_cnh value for this Cnh.
     * 
     * @param dt_validade_cnh
     */
    public void setDt_validade_cnh(java.lang.String dt_validade_cnh) {
        this.dt_validade_cnh = dt_validade_cnh;
    }


    /**
     * Gets the id_tem_cnh value for this Cnh.
     * 
     * @return id_tem_cnh
     */
    public java.lang.String getId_tem_cnh() {
        return id_tem_cnh;
    }


    /**
     * Sets the id_tem_cnh value for this Cnh.
     * 
     * @param id_tem_cnh
     */
    public void setId_tem_cnh(java.lang.String id_tem_cnh) {
        this.id_tem_cnh = id_tem_cnh;
    }


    /**
     * Gets the id_uf_emissao_cnh value for this Cnh.
     * 
     * @return id_uf_emissao_cnh
     */
    public java.lang.String getId_uf_emissao_cnh() {
        return id_uf_emissao_cnh;
    }


    /**
     * Sets the id_uf_emissao_cnh value for this Cnh.
     * 
     * @param id_uf_emissao_cnh
     */
    public void setId_uf_emissao_cnh(java.lang.String id_uf_emissao_cnh) {
        this.id_uf_emissao_cnh = id_uf_emissao_cnh;
    }


    /**
     * Gets the nr_cnh value for this Cnh.
     * 
     * @return nr_cnh
     */
    public java.lang.String getNr_cnh() {
        return nr_cnh;
    }


    /**
     * Sets the nr_cnh value for this Cnh.
     * 
     * @param nr_cnh
     */
    public void setNr_cnh(java.lang.String nr_cnh) {
        this.nr_cnh = nr_cnh;
    }


    /**
     * Gets the nr_registro value for this Cnh.
     * 
     * @return nr_registro
     */
    public java.lang.String getNr_registro() {
        return nr_registro;
    }


    /**
     * Sets the nr_registro value for this Cnh.
     * 
     * @param nr_registro
     */
    public void setNr_registro(java.lang.String nr_registro) {
        this.nr_registro = nr_registro;
    }


    /**
     * Gets the nr_seguranca value for this Cnh.
     * 
     * @return nr_seguranca
     */
    public java.lang.String getNr_seguranca() {
        return nr_seguranca;
    }


    /**
     * Sets the nr_seguranca value for this Cnh.
     * 
     * @param nr_seguranca
     */
    public void setNr_seguranca(java.lang.String nr_seguranca) {
        this.nr_seguranca = nr_seguranca;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Cnh)) return false;
        Cnh other = (Cnh) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_categoria_cnh==null && other.getDs_categoria_cnh()==null) || 
             (this.ds_categoria_cnh!=null &&
              this.ds_categoria_cnh.equals(other.getDs_categoria_cnh()))) &&
            ((this.dt_emissao_cnh==null && other.getDt_emissao_cnh()==null) || 
             (this.dt_emissao_cnh!=null &&
              this.dt_emissao_cnh.equals(other.getDt_emissao_cnh()))) &&
            ((this.dt_validade_cnh==null && other.getDt_validade_cnh()==null) || 
             (this.dt_validade_cnh!=null &&
              this.dt_validade_cnh.equals(other.getDt_validade_cnh()))) &&
            ((this.id_tem_cnh==null && other.getId_tem_cnh()==null) || 
             (this.id_tem_cnh!=null &&
              this.id_tem_cnh.equals(other.getId_tem_cnh()))) &&
            ((this.id_uf_emissao_cnh==null && other.getId_uf_emissao_cnh()==null) || 
             (this.id_uf_emissao_cnh!=null &&
              this.id_uf_emissao_cnh.equals(other.getId_uf_emissao_cnh()))) &&
            ((this.nr_cnh==null && other.getNr_cnh()==null) || 
             (this.nr_cnh!=null &&
              this.nr_cnh.equals(other.getNr_cnh()))) &&
            ((this.nr_registro==null && other.getNr_registro()==null) || 
             (this.nr_registro!=null &&
              this.nr_registro.equals(other.getNr_registro()))) &&
            ((this.nr_seguranca==null && other.getNr_seguranca()==null) || 
             (this.nr_seguranca!=null &&
              this.nr_seguranca.equals(other.getNr_seguranca())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_categoria_cnh() != null) {
            _hashCode += getDs_categoria_cnh().hashCode();
        }
        if (getDt_emissao_cnh() != null) {
            _hashCode += getDt_emissao_cnh().hashCode();
        }
        if (getDt_validade_cnh() != null) {
            _hashCode += getDt_validade_cnh().hashCode();
        }
        if (getId_tem_cnh() != null) {
            _hashCode += getId_tem_cnh().hashCode();
        }
        if (getId_uf_emissao_cnh() != null) {
            _hashCode += getId_uf_emissao_cnh().hashCode();
        }
        if (getNr_cnh() != null) {
            _hashCode += getNr_cnh().hashCode();
        }
        if (getNr_registro() != null) {
            _hashCode += getNr_registro().hashCode();
        }
        if (getNr_seguranca() != null) {
            _hashCode += getNr_seguranca().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Cnh.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Cnh"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_categoria_cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_categoria_cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_emissao_cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_emissao_cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_validade_cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_validade_cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_tem_cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_tem_cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_uf_emissao_cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_uf_emissao_cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_cnh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_cnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_registro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_registro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_seguranca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_seguranca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
