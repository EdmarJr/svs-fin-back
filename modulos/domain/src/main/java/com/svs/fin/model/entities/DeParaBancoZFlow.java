package com.svs.fin.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.gruposaga.sdk.zflow.model.gen.enums.BankCodeEnum;

@Entity
public class DeParaBancoZFlow {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@ManyToOne
    @JoinColumn(name = "id_banco")
    private Banco banco;
	
	@Column
	@Enumerated(EnumType.STRING)
	private BankCodeEnum bankCodeEnum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public BankCodeEnum getBankCodeEnum() {
		return bankCodeEnum;
	}

	public void setBankCodeEnum(BankCodeEnum bankCodeEnum) {
		this.bankCodeEnum = bankCodeEnum;
	}
}
