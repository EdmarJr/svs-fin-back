package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumTipoTabelaFinanceira;
import com.svs.fin.model.entities.interfaces.Desativavel;
import com.svs.fin.model.entities.interfaces.Identificavel;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NAME, query = TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.JPQL),
		@NamedQuery(name = TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NAME, query = TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.JPQL) })
public class TabelaFinanceira extends Entidade<Long> implements Serializable, Identificavel<Long>, Desativavel {

	public static interface NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA {
		public static final String NAME = "TabelaFinanceira.obterPorEstadoUnidadeOrganizacionalETipoETaxasPorParcela";
		public static final String JPQL = "Select DISTINCT t from TabelaFinanceira t JOIN t.unidades u LEFT JOIN FETCH t.taxas taxas where t.ativo = :"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_ESTADO_ATIVO
				+ " AND t.tipo =:"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_TIPO + " AND u.id =:"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_ID_UNIDADE_ORGANIZACIONAL
				+ " AND taxas.parcelas = :"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_QTD_PARCELAS;
		public static final String NM_PM_ID_UNIDADE_ORGANIZACIONAL = "idUnidadeOrganizacional";
		public static final String NM_PM_ESTADO_ATIVO = "ativo";
		public static final String NM_PM_TIPO = "tipo";
		public static final String NM_PM_QTD_PARCELAS = "qtdParcelas";
	}

	public static interface NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO {
		public static final String NAME = "TabelaFinanceira.obterPorEstadoUnidadeOrganizacionalETipo";
		public static final String JPQL = "Select DISTINCT t from TabelaFinanceira t JOIN t.unidades u LEFT JOIN FETCH t.taxas taxas where t.ativo = :"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NM_PM_ESTADO_ATIVO + " AND t.tipo =:"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NM_PM_TIPO + " AND u.id =:"
				+ NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NM_PM_ID_UNIDADE_ORGANIZACIONAL;
		public static final String NM_PM_ID_UNIDADE_ORGANIZACIONAL = "idUnidadeOrganizacional";
		public static final String NM_PM_ESTADO_ATIVO = "ativo";
		public static final String NM_PM_TIPO = "tipo";
	}

	public TabelaFinanceira() {
		setTaxas(new ArrayList<TaxaTabelaFinanceira>());
		setUnidades(new ArrayList<UnidadeOrganizacional>());
	}

	private static final long serialVersionUID = 8927626277003033652L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TABELAFINANCEIRA")
	@SequenceGenerator(name = "SEQ_TABELAFINANCEIRA", sequenceName = "SEQ_TABELAFINANCEIRA", allocationSize = 1)
	private Long id;

	@Column(length = 300)
	private String nome;

	// @ManyToOne
	// @JoinColumn(name = "id_tipo_tabela")
	// private TipoTabelaFinanceira tipoTabelaFinanceira;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoTabelaFinanceira tipo;

	@Column
	private BigDecimal valorTacCobrada;

	@Column
	private BigDecimal valorTacDevolvida;

	@Column
	private BigDecimal valorPlus;

	@Column
	private BigDecimal valorPercentualPlus;

	@Column
	private BigDecimal valorBonus;

	@Column
	private BigDecimal valorPercentualBonus;

	/**
	 * Tarifas
	 */
	@Column
	private BigDecimal valorCartorio;

	@Column
	private BigDecimal valorGravame;

	@Column
	private BigDecimal valorVistoria;

	@Column
	private BigDecimal valorOutros;

	@Column
	private Calendar dataInicial;

	@Column
	private Calendar dataFinal;

	@ManyToOne
	@JoinColumn(name = "id_produto_financeiro")
	private ProdutoFinanceiro produtoFinanceiro;

	@ManyToOne
	@JoinColumn(name = "id_instituicao_financeira")
	private InstituicaoFinanceira instituicaoFinanceira;
	//
	// @JsonProperty(access = Access.WRITE_ONLY)
	@OneToMany(mappedBy = "tabelaFinanceira", targetEntity = TaxaTabelaFinanceira.class, cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	private List<TaxaTabelaFinanceira> taxas;

	// @OneToMany(mappedBy = "tabelaFinanceira", targetEntity =
	// TaxaPerfilTabelaFinanceira.class, cascade = CascadeType.ALL, orphanRemoval =
	// true)
	// private List<TaxaPerfilTabelaFinanceira> taxasPorPerfil;
	//
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "tabela_financ_unidades_v2", joinColumns = @JoinColumn(name = "tabela_financeira_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
	private List<UnidadeOrganizacional> unidades;
	//
	@Column(name = "ativa")
	private Boolean ativo;
	//
	// @Transient
	// private List<UnidadeOrganizacional> unidadesTransient;

	// public List<TaxaTabelaFinanceira> getTaxasTabelaFinanceira(
	// MascaraTaxaTabelaFinanceira mascaraTaxaTabelaFinanceira) {
	// List<TaxaTabelaFinanceira> taxas = new ArrayList<TaxaTabelaFinanceira>();
	// for (TaxaTabelaFinanceira taxa : getTaxas()) {
	// if (taxa.getMascara() != null &&
	// taxa.getMascara().getId().equals(mascaraTaxaTabelaFinanceira.getId())) {
	// taxas.add(taxa);
	// }
	// }
	// return taxas;
	// }
	//
	// public List<MascaraTaxaTabelaFinanceira> getMascarasDisponiveisPorAno(Integer
	// anoVeiculo) {
	// List<MascaraTaxaTabelaFinanceira> mascaras = new
	// ArrayList<MascaraTaxaTabelaFinanceira>();
	// for (TaxaTabelaFinanceira taxa : getTaxas()) {
	// if (!mascaras.contains(taxa.getMascara()) && taxa.getAnoInicio() <=
	// anoVeiculo
	// && taxa.getAnoFim() <= anoVeiculo) {
	// mascaras.add(taxa.getMascara());
	// }
	// }
	// Collections.sort(mascaras);
	// return mascaras;
	// }
	//
	// public List<MascaraTaxaTabelaFinanceira> getMascarasDisponiveis(Integer
	// anoVeiculo) {
	// List<MascaraTaxaTabelaFinanceira> mascaras = new
	// ArrayList<MascaraTaxaTabelaFinanceira>();
	// if (anoVeiculo != null) {
	// for (TaxaTabelaFinanceira taxa : getTaxas()) {
	// if (taxa.getMascara() != null && !mascaras.contains(taxa.getMascara())
	// && taxa.getAnoInicio() <= anoVeiculo && taxa.getAnoFim() >= anoVeiculo) {
	// mascaras.add(taxa.getMascara());
	// }
	// }
	// Collections.sort(mascaras);
	// }
	// return mascaras;
	// }
	//
	// public List<BigDecimal>
	// getPercentuaisEntradaDisponiveis(MascaraTaxaTabelaFinanceira mascara, Integer
	// qtdParcelas,
	// Boolean zeroKm) {
	//
	// List<BigDecimal> percentuaisDisponiveis = new ArrayList<BigDecimal>();
	//
	// for (TaxaTabelaFinanceira taxa : getTaxas()) {
	// if (mascara != null && taxa.getMascara() != null &&
	// taxa.getMascara().getId().equals(mascara.getId())
	// && !percentuaisDisponiveis.contains(taxa.getPercentualEntrada())
	// && taxa.getParcelas().equals(qtdParcelas)
	// && taxa.getZeroKm().booleanValue() == zeroKm.booleanValue()) {
	// percentuaisDisponiveis.add(taxa.getPercentualEntrada());
	// }
	// }
	//
	// Collections.sort(percentuaisDisponiveis);
	//
	// return percentuaisDisponiveis;
	// }

	// public BigDecimal getFaixaPercentualMascara(MascaraTaxaTabelaFinanceira
	// mascara,
	// BigDecimal percentualInformado,
	// Boolean zeroKm, Integer qtdParcelas) {
	//
	// List<BigDecimal> percentuaisDisponiveis =
	// getPercentuaisEntradaDisponiveis(mascara, qtdParcelas, zeroKm);
	// BigDecimal percentualAdequadoAtual = null;
	//
	// for (BigDecimal percentual : percentuaisDisponiveis) {
	// if (percentualInformado >= percentual) {
	// percentualAdequadoAtual = percentual;
	// } else {
	// break;
	// }
	// }
	// return percentualAdequadoAtual;
	// }

	public TabelaFinanceira(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	// public TipoTabelaFinanceira getTipoTabelaFinanceira() {
	// return tipoTabelaFinanceira;
	// }
	//
	// public void setTipoTabelaFinanceira(TipoTabelaFinanceira
	// tipoTabelaFinanceira) {
	// this.tipoTabelaFinanceira = tipoTabelaFinanceira;
	// }

	public BigDecimal getValorTacCobrada() {
		return valorTacCobrada;
	}

	public void setValorTacCobrada(BigDecimal valorTacCobrada) {
		this.valorTacCobrada = valorTacCobrada;
	}

	public BigDecimal getValorTacDevolvida() {
		return valorTacDevolvida;
	}

	public void setValorTacDevolvida(BigDecimal valorTacDevolvida) {
		this.valorTacDevolvida = valorTacDevolvida;
	}

	public BigDecimal getValorPlus() {
		if (valorPlus == null) {
			valorPlus = new BigDecimal(0);
		}
		return valorPlus;
	}

	public void setValorPlus(BigDecimal valorPlus) {
		this.valorPlus = valorPlus;
	}

	public BigDecimal getValorPercentualPlus() {
		if (valorPercentualPlus == null) {
			return new BigDecimal(0);
		}
		return valorPercentualPlus;
	}

	public void setValorPercentualPlus(BigDecimal valorPercentualPlus) {
		this.valorPercentualPlus = valorPercentualPlus;
	}

	public BigDecimal getValorBonus() {
		return valorBonus;
	}

	public void setValorBonus(BigDecimal valorBonus) {
		this.valorBonus = valorBonus;
	}

	public BigDecimal getValorPercentualBonus() {
		return valorPercentualBonus;
	}

	public void setValorPercentualBonus(BigDecimal valorPercentualBonus) {
		this.valorPercentualBonus = valorPercentualBonus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// public List<TaxaTabelaFinanceira> getTaxas() {
	// if (taxas == null) {
	// taxas = new ArrayList<TaxaTabelaFinanceira>();
	// }
	// return taxas;
	// }
	//
	// public void setTaxas(List<TaxaTabelaFinanceira> taxas) {
	// this.taxas = taxas;
	// }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Calendar getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}

	public ProdutoFinanceiro getProdutoFinanceiro() {
		return produtoFinanceiro;
	}

	public void setProdutoFinanceiro(ProdutoFinanceiro produtoFinanceiro) {
		this.produtoFinanceiro = produtoFinanceiro;
	}

	public InstituicaoFinanceira getInstituicaoFinanceira() {
		return instituicaoFinanceira;
	}

	public void setInstituicaoFinanceira(InstituicaoFinanceira instituicaoFinanceira) {
		this.instituicaoFinanceira = instituicaoFinanceira;
	}
	//
	// public List<TaxaPerfilTabelaFinanceira> getTaxasPorPerfil() {
	// if (taxasPorPerfil == null) {
	// taxasPorPerfil = new ArrayList<TaxaPerfilTabelaFinanceira>();
	// }
	// return taxasPorPerfil;
	// }
	//
	// public void setTaxasPorPerfil(List<TaxaPerfilTabelaFinanceira>
	// taxasPorPerfil) {
	// this.taxasPorPerfil = taxasPorPerfil;
	// }

	public EnumTipoTabelaFinanceira getTipo() {
		return tipo;
	}

	public void setTipo(EnumTipoTabelaFinanceira tipo) {
		this.tipo = tipo;
	}

	// public List<UnidadeOrganizacional> getUnidades() {
	// if (unidades == null) {
	// unidades = new ArrayList<UnidadeOrganizacional>();
	// }
	// return unidades;
	// }
	//
	// public void setUnidades(List<UnidadeOrganizacional> unidades) {
	// this.unidades = unidades;
	// }
	//
	// public List<UnidadeOrganizacional> getUnidadesTransient() {
	// if (unidadesTransient == null) {
	// unidadesTransient = new ArrayList<UnidadeOrganizacional>();
	// }
	// return unidadesTransient;
	// }
	//
	// public void setUnidadesTransient(List<UnidadeOrganizacional>
	// unidadesTransient) {
	// this.unidadesTransient = unidadesTransient;
	// }

	public List<TaxaTabelaFinanceira> getTaxas() {
		if (org.hibernate.Hibernate.isInitialized(taxas)) {
			return taxas;
		}
		return null;
	}

	public void setTaxas(List<TaxaTabelaFinanceira> taxas) {
		this.taxas = taxas;
	}

	public List<UnidadeOrganizacional> getUnidades() {
		return unidades;
	}

	public void setUnidades(List<UnidadeOrganizacional> unidades) {
		this.unidades = unidades;
	}

	public BigDecimal getValorCartorio() {
		if (valorCartorio == null) {
			valorCartorio = new BigDecimal(0);
		}
		return valorCartorio;
	}

	public void setValorCartorio(BigDecimal valorCartorio) {
		this.valorCartorio = valorCartorio;
	}

	public BigDecimal getValorGravame() {
		if (valorGravame == null) {
			valorGravame = new BigDecimal(0);
		}
		return valorGravame;
	}

	public void setValorGravame(BigDecimal valorGravame) {
		this.valorGravame = valorGravame;
	}

	public BigDecimal getValorVistoria() {
		if (valorVistoria == null) {
			valorVistoria = new BigDecimal(0);
		}
		return valorVistoria;
	}

	public void setValorVistoria(BigDecimal valorVistoria) {
		this.valorVistoria = valorVistoria;
	}

	public BigDecimal getValorOutros() {
		if (valorOutros == null) {
			valorOutros = new BigDecimal(0);
		}
		return valorOutros;
	}

	public void setValorOutros(BigDecimal valorOutros) {
		this.valorOutros = valorOutros;
	}

	public BigDecimal getTotalTributos() {
		return getValorCartorio().add(getValorGravame()).add(getValorVistoria()).add(getValorOutros());
	}

}
