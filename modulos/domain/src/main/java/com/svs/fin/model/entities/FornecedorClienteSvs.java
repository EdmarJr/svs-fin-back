package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQueries({
		@NamedQuery(name = FornecedorClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = FornecedorClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class FornecedorClienteSvs implements Serializable {

	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "FornecedorClienteSvs.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from FornecedorClienteSvs e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6237125223533653812L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FORNECEDORCLIENTESVS")
	@SequenceGenerator(name = "SEQ_FORNECEDORCLIENTESVS", sequenceName = "SEQ_FORNECEDORCLIENTESVS", allocationSize = 1)
	private Long id;

	@Column
	private String nome;

	@Column
	private String telefone;

	@Transient
	private Boolean editando;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	private ClienteSvs clienteSvs;

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}
}
