package com.svs.fin.enums;


public enum EnumTipoResidenciaSVS {
	
	ALUGADA("Alugada"),
	CEDIDA("Cedida"),
	FINANCIADA("Financiada"),
	FUNCIONAL("Funcional"),
	PARENTES("Parente"),
	PROPRIA("Pr�pria"),
	OUTROS("Outros");
	
	private String label;
	
	private EnumTipoResidenciaSVS(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}
}
