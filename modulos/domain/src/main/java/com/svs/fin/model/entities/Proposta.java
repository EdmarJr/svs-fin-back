package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQueries({
		@NamedQuery(name = Proposta.NQ_OBTER_PROPOSTA_POR_CODIGO.NAME, query = Proposta.NQ_OBTER_PROPOSTA_POR_CODIGO.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Proposta implements Serializable {

	public static interface NQ_OBTER_PROPOSTA_POR_CODIGO {
		public static final String NAME = "Proposta.obterPropostaPorCodigo";
		public static final String JPQL = "Select p from Proposta p where p.propostaCodigo = :"
				+ NQ_OBTER_PROPOSTA_POR_CODIGO.NM_PM_CODIGO_PROPOSTA;
		public static final String NM_PM_CODIGO_PROPOSTA = "propostaCodigo";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5694653751969157384L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROPOSTA")
	@SequenceGenerator(name = "SEQ_PROPOSTA", sequenceName = "SEQ_PROPOSTA", allocationSize = 1)
	private Long id;

	@Column
	private Long propostaCodigo;

	@Column
	private Double propostaValor;

	@Column
	private String empresaCnpj;

	@Column
	private Long atendimentoCodigo;

	@Column
	private Boolean enviaEmail;

	@Column
	private Boolean entregaPesquisaEmail;

	@Column
	private Boolean aprovacaoCobranca;

	@Column
	private Boolean aprovacaoGerente;

	@Column
	private Boolean aprovacaoEntrega;

	@Column
	private Boolean liberacaoExtra;

	@Column(length = 1000)
	private String propostaObservacao;

	@Column(length = 50)
	private String estoqueTipo;

	@ManyToOne
	@JoinColumn(name = "id_cliente_svs", referencedColumnName = "id")
	private ClienteSvs clienteSvs;

	@ManyToOne
	@JoinColumn(name = "id_unidade_organizacional")
	private UnidadeOrganizacional unidade;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_veiculo")
	private VeiculoProposta veiculo;

	@Transient
	private String cpfVendedor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPropostaCodigo() {
		return propostaCodigo;
	}

	public void setPropostaCodigo(Long propostaCodigo) {
		this.propostaCodigo = propostaCodigo;
	}

	public Double getPropostaValor() {
		return propostaValor;
	}

	public void setPropostaValor(Double propostaValor) {
		this.propostaValor = propostaValor;
	}

	public String getEmpresaCnpj() {
		return empresaCnpj;
	}

	public void setEmpresaCnpj(String empresaCnpj) {
		this.empresaCnpj = empresaCnpj;
	}

	public Long getAtendimentoCodigo() {
		return atendimentoCodigo;
	}

	public void setAtendimentoCodigo(Long atendimentoCodigo) {
		this.atendimentoCodigo = atendimentoCodigo;
	}

	public Boolean getEnviaEmail() {
		return enviaEmail;
	}

	public void setEnviaEmail(Boolean enviaEmail) {
		this.enviaEmail = enviaEmail;
	}

	public Boolean getEntregaPesquisaEmail() {
		return entregaPesquisaEmail;
	}

	public void setEntregaPesquisaEmail(Boolean entregaPesquisaEmail) {
		this.entregaPesquisaEmail = entregaPesquisaEmail;
	}

	public Boolean getAprovacaoCobranca() {
		return aprovacaoCobranca;
	}

	public void setAprovacaoCobranca(Boolean aprovacaoCobranca) {
		this.aprovacaoCobranca = aprovacaoCobranca;
	}

	public Boolean getAprovacaoGerente() {
		return aprovacaoGerente;
	}

	public void setAprovacaoGerente(Boolean aprovacaoGerente) {
		this.aprovacaoGerente = aprovacaoGerente;
	}

	public Boolean getAprovacaoEntrega() {
		return aprovacaoEntrega;
	}

	public void setAprovacaoEntrega(Boolean aprovacaoEntrega) {
		this.aprovacaoEntrega = aprovacaoEntrega;
	}

	public Boolean getLiberacaoExtra() {
		return liberacaoExtra;
	}

	public void setLiberacaoExtra(Boolean liberacaoExtra) {
		this.liberacaoExtra = liberacaoExtra;
	}

	public String getPropostaObservacao() {
		return propostaObservacao;
	}

	public void setPropostaObservacao(String propostaObservacao) {
		this.propostaObservacao = propostaObservacao;
	}

	public String getEstoqueTipo() {
		return estoqueTipo;
	}

	public void setEstoqueTipo(String estoqueTipo) {
		this.estoqueTipo = estoqueTipo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	public UnidadeOrganizacional getUnidade() {
		return unidade;
	}

	public void setUnidade(UnidadeOrganizacional unidade) {
		this.unidade = unidade;
	}

	public VeiculoProposta getVeiculo() {
		if (veiculo == null) {
			veiculo = new VeiculoProposta();
		}
		return veiculo;
	}

	public void setVeiculo(VeiculoProposta veiculo) {
		this.veiculo = veiculo;
	}

	public String getCpfVendedor() {
		return cpfVendedor;
	}

	public void setCpfVendedor(String cpfVendedor) {
		this.cpfVendedor = cpfVendedor;
	}

}
