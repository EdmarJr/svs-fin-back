package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class MotivoAprovacaoSvs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9072453990970075148L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MOTIVOAPROVACAOSVS")
	@SequenceGenerator(name = "SEQ_MOTIVOAPROVACAOSVS", sequenceName = "SEQ_MOTIVOAPROVACAOSVS", allocationSize = 1)
	private Long id;

	@Column(length = 200)
	private String titulo;

	@Column
	private Boolean ativo;

	@Column
	private Calendar criadoEm;

	@Column
	private Calendar alteradoEm;

	// @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED,
	// withModifiedFlag = true)
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_usuario_criacao_alteracao")
	private AcessUser usuarioCriacaoAlteracao;

	public Calendar getCriadoEm() {
		return criadoEm;
	}

	public void setCriadoEm(Calendar criadoEm) {
		this.criadoEm = criadoEm;
	}

	public Calendar getAlteradoEm() {
		return alteradoEm;
	}

	public void setAlteradoEm(Calendar alteradoEm) {
		this.alteradoEm = alteradoEm;
	}

	public AcessUser getUsuarioCriacaoAlteracao() {
		return usuarioCriacaoAlteracao;
	}

	public void setUsuarioCriacaoAlteracao(AcessUser usuarioCriacaoAlteracao) {
		this.usuarioCriacaoAlteracao = usuarioCriacaoAlteracao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MotivoAprovacaoSvs other = (MotivoAprovacaoSvs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
