/**
 * Catalogo_Erro.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Catalogo_Erro")
public class Catalogo_Erro implements java.io.Serializable {
	private java.lang.String ds_erro;

	@Id
	private java.lang.Integer id_erro;

	public Catalogo_Erro() {
	}

	public Catalogo_Erro(java.lang.String ds_erro, java.lang.Integer id_erro) {
		this.ds_erro = ds_erro;
		this.id_erro = id_erro;
	}

	/**
	 * Gets the ds_erro value for this Catalogo_Erro.
	 * 
	 * @return ds_erro
	 */
	public java.lang.String getDs_erro() {
		return ds_erro;
	}

	/**
	 * Sets the ds_erro value for this Catalogo_Erro.
	 * 
	 * @param ds_erro
	 */
	public void setDs_erro(java.lang.String ds_erro) {
		this.ds_erro = ds_erro;
	}

	/**
	 * Gets the id_erro value for this Catalogo_Erro.
	 * 
	 * @return id_erro
	 */
	public java.lang.Integer getId_erro() {
		return id_erro;
	}

	/**
	 * Sets the id_erro value for this Catalogo_Erro.
	 * 
	 * @param id_erro
	 */
	public void setId_erro(java.lang.Integer id_erro) {
		this.id_erro = id_erro;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Catalogo_Erro))
			return false;
		Catalogo_Erro other = (Catalogo_Erro) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_erro == null && other.getDs_erro() == null)
						|| (this.ds_erro != null && this.ds_erro.equals(other.getDs_erro())))
				&& ((this.id_erro == null && other.getId_erro() == null)
						|| (this.id_erro != null && this.id_erro.equals(other.getId_erro())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_erro() != null) {
			_hashCode += getDs_erro().hashCode();
		}
		if (getId_erro() != null) {
			_hashCode += getId_erro().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Catalogo_Erro.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Catalogo_Erro"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_erro");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_erro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_erro");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_erro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
