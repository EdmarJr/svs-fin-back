package com.svs.fin.model.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class AcessUserRoleUnidadeOrganizacional {

	@EmbeddedId
	private AcessUserRoleUnidadeOrganizacionalId id = new AcessUserRoleUnidadeOrganizacionalId();

	@MapsId("acessUserId")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "acessUserId", referencedColumnName = "id")
	@JsonIgnore
	private AcessUser acessUser;
	@MapsId("unidadeOrganizacionalId")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "unidadeOrganizacionalId", referencedColumnName = "id")
	private UnidadeOrganizacional unidadeOrganizacional;
	@MapsId("roleId")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId", referencedColumnName = "id")
	private Role role;

	public AcessUser getAcessUser() {
		return acessUser;
	}

	public void setAcessUser(AcessUser acessUser) {
		this.acessUser = acessUser;
	}

	public UnidadeOrganizacional getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
