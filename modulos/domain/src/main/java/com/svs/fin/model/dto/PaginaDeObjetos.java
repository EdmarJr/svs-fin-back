package com.svs.fin.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

public class PaginaDeObjetos<T> {
	private InformacoesPaginacao informacoesDePaginacao;
	private List<T> objetos;

	public PaginaDeObjetos() {
		setObjetos(new ArrayList<T>());
		setInformacoesDePaginacao(new InformacoesPaginacao());
	}

	public InformacoesPaginacao getInformacoesDePaginacao() {
		return informacoesDePaginacao;
	}

	public void setInformacoesDePaginacao(InformacoesPaginacao informacoesDePaginacao) {
		this.informacoesDePaginacao = informacoesDePaginacao;
	}

	public List<T> getObjetos() {
		return objetos;
	}

	public void setObjetos(List<T> objetos) {
		this.objetos = objetos;
	}

	public static class Builder<T> {
		private PaginaDeObjetos<T> paginaDeObjetos;

		private Builder(PaginaDeObjetos<T> paginaDeObjetos) {
			this.paginaDeObjetos = paginaDeObjetos;
		}

		public static <T> PaginaDeObjetos.Builder<T> novaPagina(Long totalDeObjetosNaTabela,
				List<T> objetos, GrupoDeParametrosParaListagemLazy grupoDeParametros) {
			Builder<T> builder = new Builder<T>(new PaginaDeObjetos<T>());
			builder.setLimitDeObjetosNaPagina(grupoDeParametros.getLimit()).setObjetos(objetos)
					.setOffset(grupoDeParametros.getOffSet()).setTotalDeObjetosNaTabela(totalDeObjetosNaTabela);
			return builder;
		}

		public static <T> PaginaDeObjetos.Builder<T> novaPagina() {
			Builder<T> builder = new Builder<T>(new PaginaDeObjetos<T>());
			return builder;
		}

		public Builder<T> setTotalDeObjetosNaTabela(Long total) {
			this.getPaginaDeObjetos().getInformacoesDePaginacao().setTotal(total);
			return this;
		}

		public Builder<T> setObjetos(List<T> objetos) {
			this.getPaginaDeObjetos().setObjetos(objetos);
			return this;
		}

		public Builder<T> setOffset(Integer offset) {
			this.getPaginaDeObjetos().getInformacoesDePaginacao().setOffset(offset);
			return this;
		}

		public Builder<T> setLimitDeObjetosNaPagina(Integer limit) {
			this.getPaginaDeObjetos().getInformacoesDePaginacao().setLimit(limit);
			return this;
		}

		private PaginaDeObjetos<T> getPaginaDeObjetos() {
			return paginaDeObjetos;
		}

		public PaginaDeObjetos<T> build() {
			return this.getPaginaDeObjetos();

		}

	}

}
