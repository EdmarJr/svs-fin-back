/**
 * Sexo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Sexo")
public class Sexo implements java.io.Serializable {
	private java.lang.String ds_sexo;

	@Id
	private java.lang.String id_sexo;

	public Sexo() {
	}

	public Sexo(java.lang.String ds_sexo, java.lang.String id_sexo) {
		this.ds_sexo = ds_sexo;
		this.id_sexo = id_sexo;
	}

	/**
	 * Gets the ds_sexo value for this Sexo.
	 * 
	 * @return ds_sexo
	 */
	public java.lang.String getDs_sexo() {
		return ds_sexo;
	}

	/**
	 * Sets the ds_sexo value for this Sexo.
	 * 
	 * @param ds_sexo
	 */
	public void setDs_sexo(java.lang.String ds_sexo) {
		this.ds_sexo = ds_sexo;
	}

	/**
	 * Gets the id_sexo value for this Sexo.
	 * 
	 * @return id_sexo
	 */
	public java.lang.String getId_sexo() {
		return id_sexo;
	}

	/**
	 * Sets the id_sexo value for this Sexo.
	 * 
	 * @param id_sexo
	 */
	public void setId_sexo(java.lang.String id_sexo) {
		this.id_sexo = id_sexo;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Sexo))
			return false;
		Sexo other = (Sexo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_sexo == null && other.getDs_sexo() == null)
						|| (this.ds_sexo != null && this.ds_sexo.equals(other.getDs_sexo())))
				&& ((this.id_sexo == null && other.getId_sexo() == null)
						|| (this.id_sexo != null && this.id_sexo.equals(other.getId_sexo())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_sexo() != null) {
			_hashCode += getDs_sexo().hashCode();
		}
		if (getId_sexo() != null) {
			_hashCode += getId_sexo().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(Sexo.class,
			true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sexo"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_sexo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_sexo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_sexo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_sexo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
