package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class VeiculoProposta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4646616765454298817L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_VEICULOPROPOSTA")
	@SequenceGenerator(name = "SEQ_VEICULOPROPOSTA", sequenceName = "SEQ_VEICULOPROPOSTA", allocationSize = 1)
	private Long id;

	@Column(length = 50)
	private String cor;

	private String chassi;

	private String placa;

	private String descricaoModelo;

	private String codigoModeloMarca;

	private String codigoModeloMolicar;

	private String codigoModeloFipe;

	private String veiculoStatus;

	private Integer veiculoKm;

	private Integer veiculoAnoModelo;

	private Integer veiculoAnoFabricacao;

	private String veiculoMunicipioNomePlacaIbge;

	private String veiculoEstadoNomePlaca;

	private String veiculoEstadoCodPlaca;

	private String veiculoCorCodExterna;

	private String veiculoNrRenavam;

	private String veiculoCodigo;

	private String veiculoModeloVeiculoCod;

	@ManyToMany(targetEntity = OpcionalVeiculo.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "opcionaisVeiculoProposta", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "opcional_id"))
	@Fetch(FetchMode.SUBSELECT)
	private List<OpcionalVeiculo> opcionais;

	@Column
	private Boolean anoInformadoManualmente;

	@Column(length = 50)
	private String descricaoMarca;

	@Column
	private Boolean adaptado;

	@Column
	private Boolean taxi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getDescricaoModelo() {
		return descricaoModelo;
	}

	public void setDescricaoModelo(String descricaoModelo) {
		this.descricaoModelo = descricaoModelo;
	}

	public String getCodigoModeloMarca() {
		return codigoModeloMarca;
	}

	public void setCodigoModeloMarca(String codigoModeloMarca) {
		this.codigoModeloMarca = codigoModeloMarca;
	}

	public String getCodigoModeloMolicar() {
		return codigoModeloMolicar;
	}

	public void setCodigoModeloMolicar(String codigoModeloMolicar) {
		this.codigoModeloMolicar = codigoModeloMolicar;
	}

	public String getCodigoModeloFipe() {
		return codigoModeloFipe;
	}

	public void setCodigoModeloFipe(String codigoModeloFipe) {
		this.codigoModeloFipe = codigoModeloFipe;
	}

	public String getVeiculoStatus() {
		return veiculoStatus;
	}

	public void setVeiculoStatus(String veiculoStatus) {
		this.veiculoStatus = veiculoStatus;
	}

	public Integer getVeiculoKm() {
		return veiculoKm;
	}

	public void setVeiculoKm(Integer veiculoKm) {
		this.veiculoKm = veiculoKm;
	}

	public Integer getVeiculoAnoModelo() {
		return veiculoAnoModelo;
	}

	public void setVeiculoAnoModelo(Integer veiculoAnoModelo) {
		this.veiculoAnoModelo = veiculoAnoModelo;
	}

	public Integer getVeiculoAnoFabricacao() {
		return veiculoAnoFabricacao;
	}

	public void setVeiculoAnoFabricacao(Integer veiculoAnoFabricacao) {
		this.veiculoAnoFabricacao = veiculoAnoFabricacao;
	}

	public String getVeiculoMunicipioNomePlacaIbge() {
		return veiculoMunicipioNomePlacaIbge;
	}

	public void setVeiculoMunicipioNomePlacaIbge(String veiculoMunicipioNomePlacaIbge) {
		this.veiculoMunicipioNomePlacaIbge = veiculoMunicipioNomePlacaIbge;
	}

	public String getVeiculoEstadoNomePlaca() {
		return veiculoEstadoNomePlaca;
	}

	public void setVeiculoEstadoNomePlaca(String veiculoEstadoNomePlaca) {
		this.veiculoEstadoNomePlaca = veiculoEstadoNomePlaca;
	}

	public String getVeiculoEstadoCodPlaca() {
		return veiculoEstadoCodPlaca;
	}

	public void setVeiculoEstadoCodPlaca(String veiculoEstadoCodPlaca) {
		this.veiculoEstadoCodPlaca = veiculoEstadoCodPlaca;
	}

	public String getVeiculoCorCodExterna() {
		return veiculoCorCodExterna;
	}

	public void setVeiculoCorCodExterna(String veiculoCorCodExterna) {
		this.veiculoCorCodExterna = veiculoCorCodExterna;
	}

	public String getVeiculoNrRenavam() {
		return veiculoNrRenavam;
	}

	public void setVeiculoNrRenavam(String veiculoNrRenavam) {
		this.veiculoNrRenavam = veiculoNrRenavam;
	}

	public String getVeiculoCodigo() {
		return veiculoCodigo;
	}

	public void setVeiculoCodigo(String veiculoCodigo) {
		this.veiculoCodigo = veiculoCodigo;
	}

	public String getVeiculoModeloVeiculoCod() {
		return veiculoModeloVeiculoCod;
	}

	public void setVeiculoModeloVeiculoCod(String veiculoModeloVeiculoCod) {
		this.veiculoModeloVeiculoCod = veiculoModeloVeiculoCod;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public List<OpcionalVeiculo> getOpcionais() {
		if (opcionais == null) {
			opcionais = new ArrayList<OpcionalVeiculo>();
		}
		return opcionais;
	}

	public void setOpcionais(List<OpcionalVeiculo> opcionais) {
		this.opcionais = opcionais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoProposta other = (VeiculoProposta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean getAnoInformadoManualmente() {
		return anoInformadoManualmente;
	}

	public void setAnoInformadoManualmente(Boolean anoInformadoManualmente) {
		this.anoInformadoManualmente = anoInformadoManualmente;
	}

	public String getDescricaoMarca() {
		return descricaoMarca;
	}

	public void setDescricaoMarca(String descricaoMarca) {
		this.descricaoMarca = descricaoMarca;
	}

	public Boolean getAdaptado() {
		if (adaptado == null) {
			adaptado = false;
		}
		return adaptado;
	}

	public void setAdaptado(Boolean adaptado) {
		this.adaptado = adaptado;
	}

	public Boolean getTaxi() {
		if (taxi == null) {
			taxi = false;
		}
		return taxi;
	}

	public void setTaxi(Boolean taxi) {
		this.taxi = taxi;
	}
}
