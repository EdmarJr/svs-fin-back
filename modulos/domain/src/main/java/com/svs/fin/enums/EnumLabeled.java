package com.svs.fin.enums;

public interface EnumLabeled {
	public String getLabel();
}
