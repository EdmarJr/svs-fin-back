package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.enums.EnumSexo;
import com.svs.fin.enums.EnumTipoAvalista;

@Entity
@NamedQueries({
		@NamedQuery(name = Avalista.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = Avalista.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Avalista implements Serializable {

	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "Avalista.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from Avalista e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3272456999965965390L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AVALISTA")
	@SequenceGenerator(name = "SEQ_AVALISTA", sequenceName = "SEQ_AVALISTA", allocationSize = 1)
	private Long id;

	@Column
	private String nome;

	@Column
	private String cpf;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoAvalista tipoAvalista;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumSexo sexo;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEscolaridade escolaridade;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private ClienteSvs clienteSvs;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEstadoCivil estadoCivil;

	@Column(length = 100)
	private String telefoneResidencial;

	@Column(length = 100)
	private String telefoneCelular;

	@Column(length = 100)
	private String email;

	@Column(length = 100)
	private String rg;

	@Column(length = 100)
	private String orgaoEmissorRg;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEstados ufOrgaoEmissorDocRg;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumOrgaoEmissorDocumento orgaoEmissorDocRg;

	@Column
	private Calendar dataEmissaoRg;

	@Column(length = 100)
	private String ufEmissorRg;

	@Column
	private Calendar dataNascimento;

	@Column(length = 100)
	private String nacionalidade;

	@Column(length = 100)
	private String cidadeNascimento;

	@Column(length = 100)
	private String ufNascimento;

	@Column(length = 100)
	private String nomePai;

	@Column(length = 100)
	private String nomeMae;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ocupacao_id")
	private Ocupacao ocupacao;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "conjuge_id")
	private Conjuge conjuge;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, mappedBy = "avalista", orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	private List<ImovelPatrimonio> imoveisPatrimonio;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, mappedBy = "avalista", orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	private List<VeiculoPatrimonio> veiculosPatrimonio;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, mappedBy = "avalista", orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	private List<OutraRendaClienteSvs> outrasRendas;

	@Transient
	private Boolean editando;

	public Avalista() {
		this.imoveisPatrimonio = new ArrayList<ImovelPatrimonio>();
		this.veiculosPatrimonio = new ArrayList<VeiculoPatrimonio>();
		this.outrasRendas = new ArrayList<OutraRendaClienteSvs>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public EnumSexo getSexo() {
		return sexo;
	}

	public void setSexo(EnumSexo sexo) {
		this.sexo = sexo;
	}

	public EnumEscolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(EnumEscolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public EnumEstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EnumEstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoEmissorRg() {
		return orgaoEmissorRg;
	}

	public void setOrgaoEmissorRg(String orgaoEmissorRg) {
		this.orgaoEmissorRg = orgaoEmissorRg;
	}

	public String getUfEmissorRg() {
		return ufEmissorRg;
	}

	public void setUfEmissorRg(String ufEmissorRg) {
		this.ufEmissorRg = ufEmissorRg;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getCidadeNascimento() {
		return cidadeNascimento;
	}

	public void setCidadeNascimento(String cidadeNascimento) {
		this.cidadeNascimento = cidadeNascimento;
	}

	public String getUfNascimento() {
		return ufNascimento;
	}

	public void setUfNascimento(String ufNascimento) {
		this.ufNascimento = ufNascimento;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Ocupacao getOcupacao() {
		if (ocupacao == null) {
			this.ocupacao = new Ocupacao();
		}
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumTipoAvalista getTipoAvalista() {
		return tipoAvalista;
	}

	public void setTipoAvalista(EnumTipoAvalista tipoAvalista) {
		this.tipoAvalista = tipoAvalista;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Avalista other = (Avalista) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public Endereco getEndereco() {
		if (endereco == null) {
			endereco = new Endereco();
		}
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<ImovelPatrimonio> getImoveisPatrimonio() {
		if (imoveisPatrimonio == null) {
			imoveisPatrimonio = new ArrayList<ImovelPatrimonio>();
		}
		return imoveisPatrimonio;
	}

	public void setImoveisPatrimonio(List<ImovelPatrimonio> imoveisPatrimonio) {
		this.imoveisPatrimonio = imoveisPatrimonio;
	}

	public List<VeiculoPatrimonio> getVeiculosPatrimonio() {
		if (veiculosPatrimonio == null) {
			veiculosPatrimonio = new ArrayList<VeiculoPatrimonio>();
		}
		return veiculosPatrimonio;
	}

	public void setVeiculosPatrimonio(List<VeiculoPatrimonio> veiculosPatrimonio) {
		this.veiculosPatrimonio = veiculosPatrimonio;
	}

	// public Double getValorTotalPatrimonio() {
	// Double valorPatrimonio = 0d;
	// for(VeiculoPatrimonio veiculo : getVeiculosPatrimonio()) {
	// if(veiculo.getValor() != null) {
	// valorPatrimonio += veiculo.getValor();
	// }
	// }
	// for(ImovelPatrimonio imovel : getImoveisPatrimonio()) {
	// if(imovel.getValor() != null) {
	// valorPatrimonio += imovel.getValor();
	// }
	// }
	// return valorPatrimonio;
	// }
	//
	//

	@JsonIgnore
	public Double getValorTotalPatrimonioImoveis() {
		Double valorPatrimonio = 0d;
		for (ImovelPatrimonio imovel : getImoveisPatrimonio()) {
			if (imovel.getValor() != null) {
				valorPatrimonio += imovel.getValor();
			}
		}
		return valorPatrimonio;
	}

	// public Double getValorTotalPatrimonioVeiculos() {
	// Double valorPatrimonio = 0d;
	// for(VeiculoPatrimonio veiculo : getVeiculosPatrimonio()) {
	// if(veiculo.getValor() != null) {
	// valorPatrimonio += veiculo.getValor();
	// }
	// }
	// return valorPatrimonio;
	// }

	public EnumEstados getUfOrgaoEmissorDocRg() {
		return ufOrgaoEmissorDocRg;
	}

	public void setUfOrgaoEmissorDocRg(EnumEstados ufOrgaoEmissorDocRg) {
		this.ufOrgaoEmissorDocRg = ufOrgaoEmissorDocRg;
	}

	public Calendar getDataEmissaoRg() {
		return dataEmissaoRg;
	}

	public void setDataEmissaoRg(Calendar dataEmissaoRg) {
		this.dataEmissaoRg = dataEmissaoRg;
	}

	public EnumOrgaoEmissorDocumento getOrgaoEmissorDocRg() {
		return orgaoEmissorDocRg;
	}

	public void setOrgaoEmissorDocRg(EnumOrgaoEmissorDocumento orgaoEmissorDocRg) {
		this.orgaoEmissorDocRg = orgaoEmissorDocRg;
	}

	public List<OutraRendaClienteSvs> getOutrasRendas() {
		if (outrasRendas == null) {
			outrasRendas = new ArrayList<OutraRendaClienteSvs>();
		}
		return outrasRendas;
	}

	public void setOutrasRendas(List<OutraRendaClienteSvs> outrasRendas) {
		this.outrasRendas = outrasRendas;
	}

	public Conjuge getConjuge() {
		if (conjuge == null) {
			this.conjuge = new Conjuge();
		}
		return conjuge;
	}

	public void setConjuge(Conjuge conjuge) {
		this.conjuge = conjuge;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}
}
