/**
 * Empresa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Empresa")
public class Empresa implements java.io.Serializable {
	private java.lang.String ds_cep;

	private java.lang.String ds_cidade;

	private java.lang.String ds_cnpj;

	private java.lang.String ds_complemento;

	@Id
	private java.lang.Integer id_empresa;

	private java.lang.String id_uf;

	private java.lang.String nm_empresa;

	private java.lang.String nm_empresa_full;

	private java.lang.String nm_logradouro;

	private java.lang.String nr_numero;

	public Empresa() {
	}

	public Empresa(java.lang.String ds_cep, java.lang.String ds_cidade, java.lang.String ds_cnpj,
			java.lang.String ds_complemento, java.lang.Integer id_empresa, java.lang.String id_uf,
			java.lang.String nm_empresa, java.lang.String nm_empresa_full, java.lang.String nm_logradouro,
			java.lang.String nr_numero) {
		this.ds_cep = ds_cep;
		this.ds_cidade = ds_cidade;
		this.ds_cnpj = ds_cnpj;
		this.ds_complemento = ds_complemento;
		this.id_empresa = id_empresa;
		this.id_uf = id_uf;
		this.nm_empresa = nm_empresa;
		this.nm_empresa_full = nm_empresa_full;
		this.nm_logradouro = nm_logradouro;
		this.nr_numero = nr_numero;
	}

	/**
	 * Gets the ds_cep value for this Empresa.
	 * 
	 * @return ds_cep
	 */
	public java.lang.String getDs_cep() {
		return ds_cep;
	}

	/**
	 * Sets the ds_cep value for this Empresa.
	 * 
	 * @param ds_cep
	 */
	public void setDs_cep(java.lang.String ds_cep) {
		this.ds_cep = ds_cep;
	}

	/**
	 * Gets the ds_cidade value for this Empresa.
	 * 
	 * @return ds_cidade
	 */
	public java.lang.String getDs_cidade() {
		return ds_cidade;
	}

	/**
	 * Sets the ds_cidade value for this Empresa.
	 * 
	 * @param ds_cidade
	 */
	public void setDs_cidade(java.lang.String ds_cidade) {
		this.ds_cidade = ds_cidade;
	}

	/**
	 * Gets the ds_cnpj value for this Empresa.
	 * 
	 * @return ds_cnpj
	 */
	public java.lang.String getDs_cnpj() {
		return ds_cnpj;
	}

	/**
	 * Sets the ds_cnpj value for this Empresa.
	 * 
	 * @param ds_cnpj
	 */
	public void setDs_cnpj(java.lang.String ds_cnpj) {
		this.ds_cnpj = ds_cnpj;
	}

	/**
	 * Gets the ds_complemento value for this Empresa.
	 * 
	 * @return ds_complemento
	 */
	public java.lang.String getDs_complemento() {
		return ds_complemento;
	}

	/**
	 * Sets the ds_complemento value for this Empresa.
	 * 
	 * @param ds_complemento
	 */
	public void setDs_complemento(java.lang.String ds_complemento) {
		this.ds_complemento = ds_complemento;
	}

	/**
	 * Gets the id_empresa value for this Empresa.
	 * 
	 * @return id_empresa
	 */
	public java.lang.Integer getId_empresa() {
		return id_empresa;
	}

	/**
	 * Sets the id_empresa value for this Empresa.
	 * 
	 * @param id_empresa
	 */
	public void setId_empresa(java.lang.Integer id_empresa) {
		this.id_empresa = id_empresa;
	}

	/**
	 * Gets the id_uf value for this Empresa.
	 * 
	 * @return id_uf
	 */
	public java.lang.String getId_uf() {
		return id_uf;
	}

	/**
	 * Sets the id_uf value for this Empresa.
	 * 
	 * @param id_uf
	 */
	public void setId_uf(java.lang.String id_uf) {
		this.id_uf = id_uf;
	}

	/**
	 * Gets the nm_empresa value for this Empresa.
	 * 
	 * @return nm_empresa
	 */
	public java.lang.String getNm_empresa() {
		return nm_empresa;
	}

	/**
	 * Sets the nm_empresa value for this Empresa.
	 * 
	 * @param nm_empresa
	 */
	public void setNm_empresa(java.lang.String nm_empresa) {
		this.nm_empresa = nm_empresa;
	}

	/**
	 * Gets the nm_empresa_full value for this Empresa.
	 * 
	 * @return nm_empresa_full
	 */
	public java.lang.String getNm_empresa_full() {
		return nm_empresa_full;
	}

	/**
	 * Sets the nm_empresa_full value for this Empresa.
	 * 
	 * @param nm_empresa_full
	 */
	public void setNm_empresa_full(java.lang.String nm_empresa_full) {
		this.nm_empresa_full = nm_empresa_full;
	}

	/**
	 * Gets the nm_logradouro value for this Empresa.
	 * 
	 * @return nm_logradouro
	 */
	public java.lang.String getNm_logradouro() {
		return nm_logradouro;
	}

	/**
	 * Sets the nm_logradouro value for this Empresa.
	 * 
	 * @param nm_logradouro
	 */
	public void setNm_logradouro(java.lang.String nm_logradouro) {
		this.nm_logradouro = nm_logradouro;
	}

	/**
	 * Gets the nr_numero value for this Empresa.
	 * 
	 * @return nr_numero
	 */
	public java.lang.String getNr_numero() {
		return nr_numero;
	}

	/**
	 * Sets the nr_numero value for this Empresa.
	 * 
	 * @param nr_numero
	 */
	public void setNr_numero(java.lang.String nr_numero) {
		this.nr_numero = nr_numero;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Empresa))
			return false;
		Empresa other = (Empresa) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_cep == null && other.getDs_cep() == null)
						|| (this.ds_cep != null && this.ds_cep.equals(other.getDs_cep())))
				&& ((this.ds_cidade == null && other.getDs_cidade() == null)
						|| (this.ds_cidade != null && this.ds_cidade.equals(other.getDs_cidade())))
				&& ((this.ds_cnpj == null && other.getDs_cnpj() == null)
						|| (this.ds_cnpj != null && this.ds_cnpj.equals(other.getDs_cnpj())))
				&& ((this.ds_complemento == null && other.getDs_complemento() == null)
						|| (this.ds_complemento != null && this.ds_complemento.equals(other.getDs_complemento())))
				&& ((this.id_empresa == null && other.getId_empresa() == null)
						|| (this.id_empresa != null && this.id_empresa.equals(other.getId_empresa())))
				&& ((this.id_uf == null && other.getId_uf() == null)
						|| (this.id_uf != null && this.id_uf.equals(other.getId_uf())))
				&& ((this.nm_empresa == null && other.getNm_empresa() == null)
						|| (this.nm_empresa != null && this.nm_empresa.equals(other.getNm_empresa())))
				&& ((this.nm_empresa_full == null && other.getNm_empresa_full() == null)
						|| (this.nm_empresa_full != null && this.nm_empresa_full.equals(other.getNm_empresa_full())))
				&& ((this.nm_logradouro == null && other.getNm_logradouro() == null)
						|| (this.nm_logradouro != null && this.nm_logradouro.equals(other.getNm_logradouro())))
				&& ((this.nr_numero == null && other.getNr_numero() == null)
						|| (this.nr_numero != null && this.nr_numero.equals(other.getNr_numero())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_cep() != null) {
			_hashCode += getDs_cep().hashCode();
		}
		if (getDs_cidade() != null) {
			_hashCode += getDs_cidade().hashCode();
		}
		if (getDs_cnpj() != null) {
			_hashCode += getDs_cnpj().hashCode();
		}
		if (getDs_complemento() != null) {
			_hashCode += getDs_complemento().hashCode();
		}
		if (getId_empresa() != null) {
			_hashCode += getId_empresa().hashCode();
		}
		if (getId_uf() != null) {
			_hashCode += getId_uf().hashCode();
		}
		if (getNm_empresa() != null) {
			_hashCode += getNm_empresa().hashCode();
		}
		if (getNm_empresa_full() != null) {
			_hashCode += getNm_empresa_full().hashCode();
		}
		if (getNm_logradouro() != null) {
			_hashCode += getNm_logradouro().hashCode();
		}
		if (getNr_numero() != null) {
			_hashCode += getNr_numero().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Empresa.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Empresa"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_cep");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_cep"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_cidade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_cidade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_cnpj");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_cnpj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_complemento");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_complemento"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_empresa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_empresa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_uf");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_uf"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("nm_empresa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "nm_empresa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("nm_empresa_full");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "nm_empresa_full"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("nm_logradouro");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "nm_logradouro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("nr_numero");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "nr_numero"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
