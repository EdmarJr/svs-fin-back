package com.svs.fin.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.enums.EnumFaseOperacaoFinanciada;
import com.svs.fin.model.entities.interfaces.AuditavelDataHoraDeCriacao;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisDataHoraCriacao;
import com.svs.fin.utils.UtilsData;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = AndamentoOperacao.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME, query = AndamentoOperacao.NQ_OBTER_POR_OPERACAO_FINANCIADA.JPQL) })
@EntityListeners({ ListenerEntidadesAuditaveisDataHoraCriacao.class })
public class AndamentoOperacao implements Serializable, AuditavelDataHoraDeCriacao {

	public static interface NQ_OBTER_POR_OPERACAO_FINANCIADA {
		public static final String NAME = "AndamentoOperacao.obterPorOperacaoFinanciada";
		public static final String JPQL = "Select pe from AndamentoOperacao pe where pe.operacaoFinanciada.id = :"
				+ NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA;
		public static final String NM_PM_ID_OPERACAO_FINANCIADA = "idOperacaoFinanciada";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6194559145092890555L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ANDAMENTOOPERACAO")
	@SequenceGenerator(name = "SEQ_ANDAMENTOOPERACAO", sequenceName = "SEQ_ANDAMENTOOPERACAO", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private AcessUser usuario;

	@ManyToOne
	@JoinColumn(name = "id_operacao")
	@JsonProperty(access = Access.WRITE_ONLY)
	private OperacaoFinanciada operacaoFinanciada;

	@Column(name = "data")
	private LocalDateTime dataHoraDeCriacao;

	@Column(length = 3000)
	private String comentario;

	@Column
	private EnumFaseOperacaoFinanciada fase;

	@Column
	private EnumFaseOperacaoFinanciada proximaFase;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_mot_aprovacao")
	private MotivoAprovacaoSvs motivoAprovacao;

	@Transient
	private Boolean alterarFase;

	public AndamentoOperacao() {
	}

	public AndamentoOperacao(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AcessUser getUsuario() {
		return usuario;
	}

	public void setUsuario(AcessUser usuario) {
		this.usuario = usuario;
	}

	public Calendar getData() {
		return UtilsData.toCalendar(this.dataHoraDeCriacao);
	}

	public void setData(Calendar data) {
		this.dataHoraDeCriacao = UtilsData.toLocalDateTime(data);
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AndamentoOperacao other = (AndamentoOperacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public OperacaoFinanciada getOperacaoFinanciada() {
		return operacaoFinanciada;
	}

	public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumFaseOperacaoFinanciada getFase() {
		return fase;
	}

	public void setFase(EnumFaseOperacaoFinanciada fase) {
		this.fase = fase;
	}

	public EnumFaseOperacaoFinanciada getProximaFase() {
		return proximaFase;
	}

	public void setProximaFase(EnumFaseOperacaoFinanciada proximaFase) {
		this.proximaFase = proximaFase;
	}

	public MotivoAprovacaoSvs getMotivoAprovacao() {
		return motivoAprovacao;
	}

	public void setMotivoAprovacao(MotivoAprovacaoSvs motivoAprovacao) {
		this.motivoAprovacao = motivoAprovacao;
	}

	public Boolean getAlterarFase() {
		return alterarFase;
	}

	public void setAlterarFase(Boolean alterarFase) {
		this.alterarFase = alterarFase;
	}

	public LocalDateTime getDataHoraDeCriacao() {
		return dataHoraDeCriacao;
	}

	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao) {
		this.dataHoraDeCriacao = dataHoraDeCriacao;
	}

}
