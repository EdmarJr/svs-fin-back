package com.svs.fin.model.entities.interfaces;

public interface Identificavel<T> {
	T getId();
}
