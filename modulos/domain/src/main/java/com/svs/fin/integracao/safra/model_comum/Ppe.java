/**
 * Ppe.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Ppe  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_ppe_motivo;

    private java.lang.String ds_ppe_motivo_rel;

    private java.lang.String ds_ppe_nome_rel;

    private java.lang.String id_ppe_cpf_rel;

    private java.lang.String mc_ppe;

    private java.lang.String mc_ppe_relacao;

    public Ppe() {
    }

    public Ppe(
           java.lang.String ds_ppe_motivo,
           java.lang.String ds_ppe_motivo_rel,
           java.lang.String ds_ppe_nome_rel,
           java.lang.String id_ppe_cpf_rel,
           java.lang.String mc_ppe,
           java.lang.String mc_ppe_relacao) {
        this.ds_ppe_motivo = ds_ppe_motivo;
        this.ds_ppe_motivo_rel = ds_ppe_motivo_rel;
        this.ds_ppe_nome_rel = ds_ppe_nome_rel;
        this.id_ppe_cpf_rel = id_ppe_cpf_rel;
        this.mc_ppe = mc_ppe;
        this.mc_ppe_relacao = mc_ppe_relacao;
    }


    /**
     * Gets the ds_ppe_motivo value for this Ppe.
     * 
     * @return ds_ppe_motivo
     */
    public java.lang.String getDs_ppe_motivo() {
        return ds_ppe_motivo;
    }


    /**
     * Sets the ds_ppe_motivo value for this Ppe.
     * 
     * @param ds_ppe_motivo
     */
    public void setDs_ppe_motivo(java.lang.String ds_ppe_motivo) {
        this.ds_ppe_motivo = ds_ppe_motivo;
    }


    /**
     * Gets the ds_ppe_motivo_rel value for this Ppe.
     * 
     * @return ds_ppe_motivo_rel
     */
    public java.lang.String getDs_ppe_motivo_rel() {
        return ds_ppe_motivo_rel;
    }


    /**
     * Sets the ds_ppe_motivo_rel value for this Ppe.
     * 
     * @param ds_ppe_motivo_rel
     */
    public void setDs_ppe_motivo_rel(java.lang.String ds_ppe_motivo_rel) {
        this.ds_ppe_motivo_rel = ds_ppe_motivo_rel;
    }


    /**
     * Gets the ds_ppe_nome_rel value for this Ppe.
     * 
     * @return ds_ppe_nome_rel
     */
    public java.lang.String getDs_ppe_nome_rel() {
        return ds_ppe_nome_rel;
    }


    /**
     * Sets the ds_ppe_nome_rel value for this Ppe.
     * 
     * @param ds_ppe_nome_rel
     */
    public void setDs_ppe_nome_rel(java.lang.String ds_ppe_nome_rel) {
        this.ds_ppe_nome_rel = ds_ppe_nome_rel;
    }


    /**
     * Gets the id_ppe_cpf_rel value for this Ppe.
     * 
     * @return id_ppe_cpf_rel
     */
    public java.lang.String getId_ppe_cpf_rel() {
        return id_ppe_cpf_rel;
    }


    /**
     * Sets the id_ppe_cpf_rel value for this Ppe.
     * 
     * @param id_ppe_cpf_rel
     */
    public void setId_ppe_cpf_rel(java.lang.String id_ppe_cpf_rel) {
        this.id_ppe_cpf_rel = id_ppe_cpf_rel;
    }


    /**
     * Gets the mc_ppe value for this Ppe.
     * 
     * @return mc_ppe
     */
    public java.lang.String getMc_ppe() {
        return mc_ppe;
    }


    /**
     * Sets the mc_ppe value for this Ppe.
     * 
     * @param mc_ppe
     */
    public void setMc_ppe(java.lang.String mc_ppe) {
        this.mc_ppe = mc_ppe;
    }


    /**
     * Gets the mc_ppe_relacao value for this Ppe.
     * 
     * @return mc_ppe_relacao
     */
    public java.lang.String getMc_ppe_relacao() {
        return mc_ppe_relacao;
    }


    /**
     * Sets the mc_ppe_relacao value for this Ppe.
     * 
     * @param mc_ppe_relacao
     */
    public void setMc_ppe_relacao(java.lang.String mc_ppe_relacao) {
        this.mc_ppe_relacao = mc_ppe_relacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ppe)) return false;
        Ppe other = (Ppe) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_ppe_motivo==null && other.getDs_ppe_motivo()==null) || 
             (this.ds_ppe_motivo!=null &&
              this.ds_ppe_motivo.equals(other.getDs_ppe_motivo()))) &&
            ((this.ds_ppe_motivo_rel==null && other.getDs_ppe_motivo_rel()==null) || 
             (this.ds_ppe_motivo_rel!=null &&
              this.ds_ppe_motivo_rel.equals(other.getDs_ppe_motivo_rel()))) &&
            ((this.ds_ppe_nome_rel==null && other.getDs_ppe_nome_rel()==null) || 
             (this.ds_ppe_nome_rel!=null &&
              this.ds_ppe_nome_rel.equals(other.getDs_ppe_nome_rel()))) &&
            ((this.id_ppe_cpf_rel==null && other.getId_ppe_cpf_rel()==null) || 
             (this.id_ppe_cpf_rel!=null &&
              this.id_ppe_cpf_rel.equals(other.getId_ppe_cpf_rel()))) &&
            ((this.mc_ppe==null && other.getMc_ppe()==null) || 
             (this.mc_ppe!=null &&
              this.mc_ppe.equals(other.getMc_ppe()))) &&
            ((this.mc_ppe_relacao==null && other.getMc_ppe_relacao()==null) || 
             (this.mc_ppe_relacao!=null &&
              this.mc_ppe_relacao.equals(other.getMc_ppe_relacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_ppe_motivo() != null) {
            _hashCode += getDs_ppe_motivo().hashCode();
        }
        if (getDs_ppe_motivo_rel() != null) {
            _hashCode += getDs_ppe_motivo_rel().hashCode();
        }
        if (getDs_ppe_nome_rel() != null) {
            _hashCode += getDs_ppe_nome_rel().hashCode();
        }
        if (getId_ppe_cpf_rel() != null) {
            _hashCode += getId_ppe_cpf_rel().hashCode();
        }
        if (getMc_ppe() != null) {
            _hashCode += getMc_ppe().hashCode();
        }
        if (getMc_ppe_relacao() != null) {
            _hashCode += getMc_ppe_relacao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ppe.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Ppe"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_ppe_motivo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_ppe_motivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_ppe_motivo_rel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_ppe_motivo_rel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_ppe_nome_rel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_ppe_nome_rel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_ppe_cpf_rel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_ppe_cpf_rel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mc_ppe");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "mc_ppe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mc_ppe_relacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "mc_ppe_relacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
