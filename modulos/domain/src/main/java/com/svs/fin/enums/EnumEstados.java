package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumEstados {

//    RO("11","Rondônia"),
//    AC("12","Acre"),
//    AM("13","Amazonas"),
//    RR("14","Roraima"),
//    PA("15","Pará"),
//    AP("16","Amapá"),
//    TO("17","Tocantins"),
//    MA("21","Maranhão"),
//    PI("22","Piaui"),
//    CE("23","Ceará"),
//    RN("24","Rio Grande do Norte"),
//    PB("25","Paraíba"),
//    PE("26","Pernambuco"),
//    AL("27","Alagoas"),
//    SE("28","Sergipe"),
//    BA("29","Bahia"),
//    MG("31","Minas Gerais"),
//    ES("32","Espírito Santo"),
//    RJ("33","Rio de Janeiro"),
//    SP("35","São Paulo"),
//    PR("41","Paraná"),
//    SC("42","Santa Catarina"),
//    RS("43","Rio Grande do Sul"),
//    MS("50","Mato Grosso do Sul"),
//    MT("51","Mato Grosso"),
//    GO("52","Goiás"),
//    DF("53","Distrito Federal");
	@JsonProperty("Rondônia")
	RO("11", "Rondônia"), @JsonProperty("Acre")
	AC("12", "Acre"), @JsonProperty("Amazonas")
	AM("13", "Amazonas"), @JsonProperty("Roraima")
	RR("14", "Roraima"), @JsonProperty("Pará")
	PA("15", "Pará"), @JsonProperty("Amapá")
	AP("16", "Amapá"), @JsonProperty("Tocantins")
	TO("17", "Tocantins"), @JsonProperty("Maranhão")
	MA("21", "Maranhão"), @JsonProperty("Piaui")
	PI("22", "Piaui"), @JsonProperty("Ceará")
	CE("23", "Ceará"), @JsonProperty("Rio Grande do Norte")
	RN("24", "Rio Grande do Norte"), @JsonProperty("Paraíba")
	PB("25", "Paraíba"), @JsonProperty("Pernambuco")
	PE("26", "Pernambuco"), @JsonProperty("Alagoas")
	AL("27", "Alagoas"), @JsonProperty("Sergipe")
	SE("28", "Sergipe"), @JsonProperty("Bahia")
	BA("29", "Bahia"), @JsonProperty("Minas Gerais")
	MG("31", "Minas Gerais"), @JsonProperty("Espírito Santo")
	ES("32", "Espírito Santo"), @JsonProperty("Rio de Janeiro")
	RJ("33", "Rio de Janeiro"), @JsonProperty("São Paulo")
	SP("35", "São Paulo"), @JsonProperty("Paraná")
	PR("41", "Paraná"), @JsonProperty("Santa Catarina")
	SC("42", "Santa Catarina"), @JsonProperty("Rio Grande do Sul")
	RS("43", "Rio Grande do Sul"), @JsonProperty("Mato Grosso do Sul")
	MS("50", "Mato Grosso do Sul"), @JsonProperty("Mato Grosso")
	MT("51", "Mato Grosso"), @JsonProperty("Goiás")
	GO("52", "Goiás"),  @JsonProperty("Distrito Federal") DF("53", "Distrito Federal");

	private final String codigoIbge;
	private final String nome;

	private EnumEstados(String codigoIbge, String nome) {
		this.codigoIbge = codigoIbge;
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public String getCodigoIbge() {
		return codigoIbge;
	}
}
