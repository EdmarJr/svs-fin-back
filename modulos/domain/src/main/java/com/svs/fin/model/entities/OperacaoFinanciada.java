package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.enums.EnumDepartamentoSvs;
import com.svs.fin.enums.EnumFaseOperacaoFinanciada;
import com.svs.fin.enums.EnumTipoTabelaFinanceira;
import com.svs.fin.enums.EnumTipoVeiculo;
import com.svs.fin.integracaoItau.FinanciamentoOnlineItau;
import com.svs.fin.model.entities.integracaosantander.AnoModeloCombustivelSantander;
import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander;

@Entity
@NamedQueries({
		@NamedQuery(name = OperacaoFinanciada.NQ_OBTER_OPERACOES_VINCULADAS_A_UM_CODIGO_PROPOSTA.NAME, query = OperacaoFinanciada.NQ_OBTER_OPERACOES_VINCULADAS_A_UM_CODIGO_PROPOSTA.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperacaoFinanciada implements Serializable {

	public static interface NQ_OBTER_OPERACOES_VINCULADAS_A_UM_CODIGO_PROPOSTA {
		public static final String NAME = "OperacaoFinanciada.obterPorCodigoProposta";
		public static final String JPQL = "Select o from OperacaoFinanciada o where o.proposta.propostaCodigo = :"
				+ NQ_OBTER_OPERACOES_VINCULADAS_A_UM_CODIGO_PROPOSTA.NM_PM_CODIGO_PROPOSTA;
		public static final String NM_PM_CODIGO_PROPOSTA = "propostaCodigo";
	}

	private static final long serialVersionUID = 5904785350902879680L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OPERACAOFINANCIADA")
	@SequenceGenerator(name = "SEQ_OPERACAOFINANCIADA", sequenceName = "SEQ_OPERACAOFINANCIADA", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_unidade_organizacional")
	private UnidadeOrganizacional unidade;

	@ManyToOne()
	@JoinColumn(name = "id_cliente")
	private ClienteSvs cliente;

	@ManyToOne()
	@JoinColumn(name = "id_vendedor")
	private AcessUser vendedor;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_proposta")
	private Proposta proposta;

	@OneToMany(mappedBy = "operacaoFinanciada", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<AnexoCad> anexos;

	@OneToMany(mappedBy = "operacaoFinanciada", targetEntity = AndamentoOperacao.class, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("data DESC")
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<AndamentoOperacao> andamentos;

	@OneToMany(mappedBy = "operacaoFinanciada", targetEntity = ParcelaEntrada.class, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("codigo ASC")
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<ParcelaEntrada> parcelasEntrada;

	@OneToMany(mappedBy = "operacaoFinanciada", targetEntity = ItemCBC.class, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("codigo ASC")
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<ItemCBC> itensCbc;

	// @ManyToMany(targetEntity = CalculoRentabilidade.class, fetch =
	// FetchType.LAZY, cascade = CascadeType.ALL)
	// @JoinTable(name = "operacaoFinanciadaTodasRent", joinColumns =
	// @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name =
	// "calculo_id"))
	// @JsonProperty(access = Access.WRITE_ONLY)
	// private List<CalculoRentabilidade> todasRentabilidades;

	@OneToMany(mappedBy = "operacaoFinanciada")
	@JsonIgnore
	private List<CalculoRentabilidade> calculosEnviadosIF;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_calculo_tabela_instit")
	private CalculoRentabilidade calculoTabelaInstitucional;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoTabelaFinanceira tipoTabelaFaturamento;

	@OneToOne
	@JoinColumn(name = "id_calculo_faturamento")
	private CalculoRentabilidade calculoFaturamento;

	@ManyToOne
	@JoinColumn(name = "id_instituicao_financ_fat")
	private InstituicaoFinanceira instituicaoFinanceiraFaturamento;

	@ManyToOne
	@JoinColumn(name = "id_marca_santander")
	private MarcaSantander marcaSantander;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoVeiculo tipoVeiculo;

	@ManyToOne
	@JoinColumn(name = "id_modelo")
	private ModeloVeiculoSantander modeloVeiculoSantander;

	@ManyToOne
	@JoinColumn(name = "id_ano_modelo")
	private AnoModeloCombustivelSantander anoModeloCombustivelSantander;

	@Enumerated(EnumType.STRING)
	@Column
	private EnumFaseOperacaoFinanciada fase;

	@Enumerated(EnumType.STRING)
	@Column
	private EnumDepartamentoSvs departamento;

	@Column(length = 100)
	private String cpfVendedor;

	@Column(length = 100)
	private String numeroProposta;

	@Column
	private Calendar dataEntrada;

	@Column
	private Calendar dataUltimaMovimentacao;

	@Column
	private Integer parcelas;

	@Column
	private BigDecimal valor;

	@Column
	private BigDecimal valorCbc;

	@Column
	private BigDecimal valorCbcFinanciamento;

	@Column
	private Double valorEntrada;

	// @Column
	// private BigDecimal percEntrada;

	@Column
	private Double valorFinanciado;

	@Column
	private Double valorVeiculoItau;

	@Column
	private Double valorAcessoriosItau;

	@Column
	private Double valorEntradaItau;

	@Column
	private Double valorFinanciadoItau;

	@Transient
	private Boolean editando;

	@Transient
	private List<SimulacaoPmtOperacaoFinanciada> simulacoesPmt;

	@Transient
	private List<CalculoRentabilidade> opcoesCalculoFaturamento;

	@Transient
	private List<LogOperCad> listLog;

	@Transient
	private Integer qtdParcelasTransient;

	@Transient
	private List<ItemCBC> itensCBCRemover;

	@Transient
	private List<ParcelaEntrada> parcelasEntradaRemover;

	@OneToMany(mappedBy = "operacaoFinanciada", targetEntity = FinanciamentoOnlineItau.class, fetch = FetchType.LAZY)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<FinanciamentoOnlineItau> financiamentosOnlineItau;

	@ManyToMany(targetEntity = CalculoRentabilidade.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonProperty(access = Access.WRITE_ONLY)
	@JoinTable(name = "operacaoFinanciadaTodasRent", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "calculo_id"))
	private List<CalculoRentabilidade> todasRentabilidades;

	// ..
	public CalculoRentabilidade getCalculoContrato() {
		if (getCalculoFaturamento() == null) {
			return getCalculoTabelaInstitucional();
		} else {
			return getCalculoFaturamento();
		}
	}

	// public List<CalculoRentabilidade>
	// getOpcoesCalculosAguardandoAprovacaoDoBanco() {
	// List<CalculoRentabilidade> calculosAgAprovacao = new
	// ArrayList<CalculoRentabilidade>();
	// for (CalculoRentabilidade calculo : getTodasRentabilidades()) {
	// if
	// (calculo.getStatus().equals(EnumStatusAprovacaoBancaria.AGUARDANDO_APROVACAO)
	// || calculo.getStatus().equals(EnumStatusAprovacaoBancaria.REANALISE)) {
	// calculosAgAprovacao.add(calculo);
	// }
	// }
	// return calculosAgAprovacao;
	// }
	//
	// public List<CalculoRentabilidade> getOpcoesCalculosEnviadosAoBanco() {
	// List<CalculoRentabilidade> calculosEnviados = new
	// ArrayList<CalculoRentabilidade>();
	// for (CalculoRentabilidade calculo : getTodasRentabilidades()) {
	// if (!calculo.getStatus().equals(EnumStatusAprovacaoBancaria.NAO_ENVIADO)) {
	// calculosEnviados.add(calculo);
	// }
	// }
	// return calculosEnviados;
	// }
	//
	// public List<Integer> getOpcoesQtdeParcela() {
	// List<Integer> opcoes = new ArrayList<Integer>();
	// for (CalculoRentabilidade calculo : getTodasRentabilidades()) {
	// if (!opcoes.contains(calculo.getParcelas())) {
	// opcoes.add(calculo.getParcelas());
	// }
	// }
	// return opcoes;
	// }
	//
	// public List<InstituicaoFinanceira> getInstituicoesComCalculoAprovado() {
	//
	// List<InstituicaoFinanceira> instituicoes = new
	// ArrayList<InstituicaoFinanceira>();
	//
	// for (CalculoRentabilidade calculo : getTodasRentabilidades()) {
	// if (calculo.getStatus() != null &&
	// calculo.getStatus().equals(EnumStatusAprovacaoBancaria.APROVADO)
	// && calculo.getInstituicaoFinanceira() != null) {
	//
	// if (!instituicoes.contains(calculo.getInstituicaoFinanceira())) {
	// instituicoes.add(calculo.getInstituicaoFinanceira());
	// }
	// }
	// }
	// return instituicoes;
	// }
	//
	// public List<CalculoRentabilidade> getOpcoesCalculoFaturamento() {
	//
	// if (tipoTabelaFaturamento != null &&
	// !tipoTabelaFaturamento.equals(EnumTipoTabelaFinanceira.EMPRESA)
	// && opcoesCalculoFaturamento == null) {
	// opcoesCalculoFaturamento = new ArrayList<CalculoRentabilidade>();
	// if (getInstituicaoFinanceiraFaturamento() == null) {
	// return opcoesCalculoFaturamento;
	// }
	// for (CalculoRentabilidade calculo : getTodasRentabilidades()) {
	// if (calculo.getStatus() != null && calculo.getInstituicaoFinanceira() != null
	// && calculo
	// .getInstituicaoFinanceira().getId().equals(getInstituicaoFinanceiraFaturamento().getId()))
	// {
	// if (!opcoesCalculoFaturamento.contains(calculo)) {
	// opcoesCalculoFaturamento.add(calculo);
	// }
	// }
	// }
	// }
	//
	// if (tipoTabelaFaturamento != null &&
	// tipoTabelaFaturamento.equals(EnumTipoTabelaFinanceira.EMPRESA)) {
	// opcoesCalculoFaturamento = new ArrayList<CalculoRentabilidade>();
	// opcoesCalculoFaturamento.add(getCalculoTabelaInstitucional());
	// }
	//
	// return opcoesCalculoFaturamento;
	// }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UnidadeOrganizacional getUnidade() {
		return unidade;
	}

	public void setUnidade(UnidadeOrganizacional unidade) {
		this.unidade = unidade;
	}

	public ClienteSvs getCliente() {
		return cliente;
	}

	public void setCliente(ClienteSvs cliente) {
		this.cliente = cliente;
	}

	public String getCpfVendedor() {
		return cpfVendedor;
	}

	public void setCpfVendedor(String cpfVendedor) {
		this.cpfVendedor = cpfVendedor;
	}

	public String getNumeroProposta() {
		return numeroProposta;
	}

	public void setNumeroProposta(String numeroProposta) {
		this.numeroProposta = numeroProposta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperacaoFinanciada other = (OperacaoFinanciada) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Proposta getProposta() {
		if (proposta == null) {
			proposta = new Proposta();
		}
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public EnumDepartamentoSvs getDepartamento() {
		return departamento;
	}

	public void setDepartamento(EnumDepartamentoSvs departamento) {
		this.departamento = departamento;
	}

	public Calendar getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Calendar dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	//
	// public BigDecimal getPercEntrada() {
	// return percEntrada;
	// }
	//
	// public void setPercEntrada(BigDecimal percEntrada) {
	// this.percEntrada = percEntrada;
	// }

	public Double getValorFinanciado() {
		return valorFinanciado;
	}

	public void setValorFinanciado(Double valorFinanciado) {
		this.valorFinanciado = valorFinanciado;
	}

	public Double getValorEntrada() {
		return valorEntrada;
	}

	public void setValorEntrada(Double valorEntrada) {
		this.valorEntrada = valorEntrada;
	}

	public List<AnexoCad> getAnexos() {
		if (anexos == null) {
			anexos = new ArrayList<AnexoCad>();
		}
		return anexos;
	}

	public void setAnexos(List<AnexoCad> anexos) {
		this.anexos = anexos;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public List<SimulacaoPmtOperacaoFinanciada> getSimulacoesPmt() {
		if (simulacoesPmt == null) {
			simulacoesPmt = new ArrayList<SimulacaoPmtOperacaoFinanciada>();
		}
		return simulacoesPmt;
	}

	public void setSimulacoesPmt(List<SimulacaoPmtOperacaoFinanciada> simulacoesPmt) {
		this.simulacoesPmt = simulacoesPmt;
	}

	public List<AndamentoOperacao> getAndamentos() {
		return andamentos;
	}

	public void setAndamentos(List<AndamentoOperacao> andamentos) {
		this.andamentos = andamentos;
	}

	public EnumFaseOperacaoFinanciada getFase() {
		return fase;
	}

	public void setFase(EnumFaseOperacaoFinanciada fase) {
		this.fase = fase;
	}

	public CalculoRentabilidade getCalculoTabelaInstitucional() {
		// if (this.calculoTabelaInstitucional == null) {
		// this.calculoTabelaInstitucional = new CalculoRentabilidade(this);
		// }
		return calculoTabelaInstitucional;
	}

	public void setCalculoTabelaInstitucional(CalculoRentabilidade calculoTabelaInstitucional) {
		this.calculoTabelaInstitucional = calculoTabelaInstitucional;
	}

	public List<CalculoRentabilidade> getCalculosEnviadosIF() {
		if (calculosEnviadosIF == null) {
			calculosEnviadosIF = new ArrayList<CalculoRentabilidade>();
		}
		return calculosEnviadosIF;
	}

	public void setCalculosEnviadosIF(List<CalculoRentabilidade> calculosEnviadosIF) {
		this.calculosEnviadosIF = calculosEnviadosIF;
	}

	// public List<CalculoRentabilidade> getTodasRentabilidades() {
	// if (todasRentabilidades == null) {
	// todasRentabilidades = new ArrayList<CalculoRentabilidade>();
	// }
	// return todasRentabilidades;
	// }

	// public void removerCalculosOnline(EnumBancoIntegracaoOnline banco) {
	//
	// List<CalculoRentabilidade> todasRentabilidadesFiltrada =
	// getTodasRentabilidades().stream()
	// .filter(rentabilidade -> !rentabilidade.getAprovacaoOnline()
	// ||
	// !rentabilidade.getBancoIntegracaoOnline().equals(EnumBancoIntegracaoOnline.SANTANDER))
	// .collect(Collectors.toList());
	//
	// this.todasRentabilidades = todasRentabilidadesFiltrada;
	// }

	// public CalculoRentabilidade
	// getCalculoIntegracaoOnline(EnumBancoIntegracaoOnline banco) {
	// for(CalculoRentabilidade calculo : getTodasRentabilidades()) {
	// if(calculo.getAprovacaoOnline() &&
	// calculo.getBancoIntegracaoOnline().equals(EnumBancoIntegracaoOnline.SANTANDER))
	// {
	// return calculo;
	// }
	// }
	// return null;
	// }

	// public void setTodasRentabilidades(List<CalculoRentabilidade>
	// todasRentabilidades) {
	// this.todasRentabilidades = todasRentabilidades;
	// }

	public BigDecimal getValorCbc() {
		if (valorCbc == null) {
			valorCbc = new BigDecimal(0);
		}
		return valorCbc;
	}

	public void setValorCbc(BigDecimal valorCbc) {
		this.valorCbc = valorCbc;
	}

	public List<ParcelaEntrada> getParcelasEntrada() {
		if (parcelasEntrada == null) {
			parcelasEntrada = new ArrayList<ParcelaEntrada>();
		}
		return parcelasEntrada;
	}

	public void setParcelasEntrada(List<ParcelaEntrada> parcelasEntrada) {
		this.parcelasEntrada = parcelasEntrada;
	}

	public Calendar getDataUltimaMovimentacao() {
		return dataUltimaMovimentacao;
	}

	public void setDataUltimaMovimentacao(Calendar dataUltimaMovimentacao) {
		this.dataUltimaMovimentacao = dataUltimaMovimentacao;
	}

	public List<ItemCBC> getItensCbc() {
		if (itensCbc == null) {
			itensCbc = new ArrayList<ItemCBC>();
		}
		return itensCbc;
	}

	public void setItensCbc(List<ItemCBC> itensCbc) {
		this.itensCbc = itensCbc;
	}

	public BigDecimal getValorCbcFinanciamento() {
		if (valorCbcFinanciamento == null) {
			valorCbcFinanciamento = new BigDecimal(0);
		}
		return valorCbcFinanciamento;
	}

	public void setValorCbcFinanciamento(BigDecimal valorCbcFinanciamento) {
		this.valorCbcFinanciamento = valorCbcFinanciamento;
	}

	public CalculoRentabilidade getCalculoFaturamento() {
		return calculoFaturamento;
	}

	public void setCalculoFaturamento(CalculoRentabilidade calculoFaturamento) {
		this.calculoFaturamento = calculoFaturamento;
	}

	public EnumTipoTabelaFinanceira getTipoTabelaFaturamento() {
		return tipoTabelaFaturamento;
	}

	public void setTipoTabelaFaturamento(EnumTipoTabelaFinanceira tipoTabelaFaturamento) {
		this.tipoTabelaFaturamento = tipoTabelaFaturamento;
	}

	public InstituicaoFinanceira getInstituicaoFinanceiraFaturamento() {
		return instituicaoFinanceiraFaturamento;
	}

	public void setInstituicaoFinanceiraFaturamento(InstituicaoFinanceira instituicaoFinanceiraFaturamento) {
		this.instituicaoFinanceiraFaturamento = instituicaoFinanceiraFaturamento;
	}

	public void setOpcoesCalculoFaturamento(List<CalculoRentabilidade> opcoesCalculoFaturamento) {
		this.opcoesCalculoFaturamento = opcoesCalculoFaturamento;
	}

	public List<LogOperCad> getListLog() {
		return listLog;
	}

	public void setListLog(List<LogOperCad> listLog) {
		this.listLog = listLog;
	}

	public Integer getQtdParcelasTransient() {
		if (qtdParcelasTransient == null && getParcelas() != null) {
			qtdParcelasTransient = new Integer(getParcelas().intValue());
		}
		return qtdParcelasTransient;
	}

	public void setQtdParcelasTransient(Integer qtdParcelasTransient) {
		this.qtdParcelasTransient = qtdParcelasTransient;
	}

	public List<ItemCBC> getItensCBCRemover() {
		if (itensCBCRemover == null) {
			itensCBCRemover = new ArrayList<ItemCBC>();
		}
		return itensCBCRemover;
	}

	public void setItensCBCRemover(List<ItemCBC> itensCBCRemover) {
		this.itensCBCRemover = itensCBCRemover;
	}

	public List<ParcelaEntrada> getParcelasEntradaRemover() {
		if (parcelasEntradaRemover == null) {
			parcelasEntradaRemover = new ArrayList<ParcelaEntrada>();
		}
		return parcelasEntradaRemover;
	}

	public void setParcelasEntradaRemover(List<ParcelaEntrada> parcelasEntradaRemover) {
		this.parcelasEntradaRemover = parcelasEntradaRemover;
	}

	public AcessUser getVendedor() {
		return vendedor;
	}

	public void setVendedor(AcessUser vendedor) {
		this.vendedor = vendedor;
	}

	public MarcaSantander getMarcaSantander() {
		return marcaSantander;
	}

	public void setMarcaSantander(MarcaSantander marcaSantander) {
		this.marcaSantander = marcaSantander;
	}
	//
	// public List<FinanciamentoOnlineSantander> getFinanciamentosOnlineSantander()
	// {
	// if (financiamentosOnlineSantander == null) {
	// financiamentosOnlineSantander = new
	// ArrayList<FinanciamentoOnlineSantander>();
	// }
	// return financiamentosOnlineSantander;
	// }
	//
	// public void
	// setFinanciamentosOnlineSantander(List<FinanciamentoOnlineSantander>
	// financiamentosOnlineSantander) {
	// this.financiamentosOnlineSantander = financiamentosOnlineSantander;
	// }

	public ModeloVeiculoSantander getModeloVeiculoSantander() {
		return modeloVeiculoSantander;
	}

	public void setModeloVeiculoSantander(ModeloVeiculoSantander modeloVeiculoSantander) {
		this.modeloVeiculoSantander = modeloVeiculoSantander;
	}

	public AnoModeloCombustivelSantander getAnoModeloCombustivelSantander() {
		return anoModeloCombustivelSantander;
	}

	public void setAnoModeloCombustivelSantander(AnoModeloCombustivelSantander anoModeloCombustivelSantander) {
		this.anoModeloCombustivelSantander = anoModeloCombustivelSantander;
	}

	public EnumTipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(EnumTipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Double getValorVeiculoItau() {
		return valorVeiculoItau;
	}

	public void setValorVeiculoItau(Double valorVeiculoItau) {
		this.valorVeiculoItau = valorVeiculoItau;
	}

	public Double getValorAcessoriosItau() {
		return valorAcessoriosItau;
	}

	public void setValorAcessoriosItau(Double valorAcessoriosItau) {
		this.valorAcessoriosItau = valorAcessoriosItau;
	}

	public Double getValorEntradaItau() {
		return valorEntradaItau;
	}

	public void setValorEntradaItau(Double valorEntradaItau) {
		this.valorEntradaItau = valorEntradaItau;
	}

	public Double getValorFinanciadoItau() {
		return valorFinanciadoItau;
	}

	public void setValorFinanciadoItau(Double valorFinanciadoItau) {
		this.valorFinanciadoItau = valorFinanciadoItau;
	}

	public List<FinanciamentoOnlineItau> getFinanciamentosOnlineItau() {
		if (financiamentosOnlineItau == null) {
			financiamentosOnlineItau = new ArrayList<FinanciamentoOnlineItau>();
		}
		return financiamentosOnlineItau;
	}

	public void setFinanciamentosOnlineItau(List<FinanciamentoOnlineItau> financiamentosOnlineItau) {
		this.financiamentosOnlineItau = financiamentosOnlineItau;
	}

	public List<CalculoRentabilidade> getTodasRentabilidades() {
		if (todasRentabilidades == null) {
			todasRentabilidades = new ArrayList<CalculoRentabilidade>();
		}
		return todasRentabilidades;
	}

	public void setTodasRentabilidades(List<CalculoRentabilidade> todasRentabilidades) {
		this.todasRentabilidades = todasRentabilidades;
	}

	// public List<FinanciamentoOnlineItau> getFinanciamentosOnlineItau() {
	// if (financiamentosOnlineItau == null) {
	// financiamentosOnlineItau = new ArrayList<FinanciamentoOnlineItau>();
	// }
	// return financiamentosOnlineItau;
	// }
	//
	// public void setFinanciamentosOnlineItau(List<FinanciamentoOnlineItau>
	// financiamentosOnlineItau) {
	// this.financiamentosOnlineItau = financiamentosOnlineItau;
	// }
	//
	// public List<BeanOpcaoFinanciamentoItau> getListTodasOpcoesItau() {
	// List<BeanOpcaoFinanciamentoItau> options = new
	// ArrayList<BeanOpcaoFinanciamentoItau>();
	// for (FinanciamentoOnlineItau financiamentoOnline :
	// getFinanciamentosOnlineItau()) {
	// SimulationOperationResponse simulationResponse = financiamentoOnline
	// .getJsonSimulationOperationResponseItau().getSimulationOperationResponse();
	// if (simulationResponse != null &&
	// simulationResponse.getFinanceOperationDetails() != null
	// &&
	// simulationResponse.getFinanceOperationDetails().getFinanceOperationOptions()
	// != null) {
	// for (FinanceOperationOption option :
	// simulationResponse.getFinanceOperationDetails()
	// .getFinanceOperationOptions()) {
	// options.add(new BeanOpcaoFinanciamentoItau(financiamentoOnline, option));
	// }
	// }
	// }
	// return options;
	// }
}
