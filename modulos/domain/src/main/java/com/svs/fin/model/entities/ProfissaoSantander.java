package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.gruposaga.sdk.zflow.model.gen.enums.ProfessionEnum;

@Entity
public class ProfissaoSantander implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -136653191321850284L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROFISSAOSANTANDER")
	@SequenceGenerator(name = "SEQ_PROFISSAOSANTANDER", sequenceName = "SEQ_PROFISSAOSANTANDER", allocationSize = 1)
	private Long id;

	@Column(length = 100)
	private String description;

	@Column
	@Enumerated(EnumType.STRING)
	private ProfessionEnum professionEquivalenteZFlow;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfissaoSantander other = (ProfissaoSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public ProfessionEnum getProfessionEquivalenteZFlow() {
		return professionEquivalenteZFlow;
	}

	public void setProfessionEquivalenteZFlow(ProfessionEnum professionEquivalenteZFlow) {
		this.professionEquivalenteZFlow = professionEquivalenteZFlow;
	}

}
