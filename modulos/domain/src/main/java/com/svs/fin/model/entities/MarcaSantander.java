package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumMarca;
import com.svs.fin.enums.EnumTipoVeiculo;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({ @NamedQuery(name = MarcaSantander.NQ_OBTER_POR_DESCRIPTION.NAME, query = MarcaSantander.NQ_OBTER_POR_DESCRIPTION.JPQL),
	@NamedQuery(name = MarcaSantander.NQ_OBTER_POR_TIPO_VEICULO.NAME, query = MarcaSantander.NQ_OBTER_POR_TIPO_VEICULO.JPQL)})
public class MarcaSantander implements Serializable {
	
	public static interface NQ_OBTER_POR_DESCRIPTION {
		public static final String NAME = "MarcaSantander.obterPorDescription";
		public static final String JPQL = "Select DISTINCT m from MarcaSantander m where m.description = :" + NQ_OBTER_POR_DESCRIPTION.NM_PM_DESCRIPTION;
		public static final String NM_PM_DESCRIPTION = "description";
	}
	
	public static interface NQ_OBTER_POR_TIPO_VEICULO {
		public static final String NAME = "MarcaSantander.obterPorTipoVeiculo";
		public static final String JPQL = "Select DISTINCT m from MarcaSantander m where m.tipoVeiculo = :" + NQ_OBTER_POR_TIPO_VEICULO.NM_PM_TIPO_VEICULO;
		public static final String NM_PM_TIPO_VEICULO = "tipoVeiculo";
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3037149495850343891L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MARCASANTANDER")
	@SequenceGenerator(name = "SEQ_MARCASANTANDER", sequenceName = "SEQ_MARCASANTANDER", allocationSize = 1)
	private Long id;

	@Column(length = 100)
	private String description;

	@Column(length = 100)
	private String integrationCode;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoVeiculo tipoVeiculo;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumMarca marcaMySaga;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntegrationCode() {
		return integrationCode;
	}

	public void setIntegrationCode(String integrationCode) {
		this.integrationCode = integrationCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarcaSantander other = (MarcaSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public EnumTipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(EnumTipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumMarca getMarcaMySaga() {
		return marcaMySaga;
	}

	public void setMarcaMySaga(EnumMarca marcaMySaga) {
		this.marcaMySaga = marcaMySaga;
	}
}
