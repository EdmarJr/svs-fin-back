package com.svs.fin.model.entities.listeners;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.svs.fin.model.entities.interfaces.AuditavelDataHoraDeAtualizacao;
import com.svs.fin.model.entities.interfaces.AuditavelDataHoraDeCriacao;

public class ListenerEntidadesAuditaveisTemporalmente {
	@PreUpdate
	public <T extends AuditavelDataHoraDeAtualizacao> void definirDataHoraAtualizacao(T entidade) {
		entidade.setDataHoraDeAtualizacao(LocalDateTime.now());
	}

	@PrePersist
	public <T extends AuditavelDataHoraDeCriacao> void definirDataHoraCriacao(T entidade) {
		entidade.setDataHoraDeCriacao(LocalDateTime.now());
	}
}
