package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.enums.EnumSexo;
import com.svs.fin.enums.EnumTipoPessoa;
import com.svs.fin.model.entities.interfaces.IClienteCentralCreditoDto;

import br.com.gruposaga.cad.credito.model.dto.ClienteCentralCreditoDto;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({ @NamedQuery(name = ClienteSvs.NQ_OBTER_POR_CPF.NAME, query = ClienteSvs.NQ_OBTER_POR_CPF.JPQL),
		@NamedQuery(name = ClienteSvs.NQ_OBTER_POR_CNPJ.NAME, query = ClienteSvs.NQ_OBTER_POR_CNPJ.JPQL),
		@NamedQuery(name = ClienteSvs.NQ_OBTER_POR_CPF_CNPJ.NAME, query = ClienteSvs.NQ_OBTER_POR_CPF_CNPJ.JPQL),
		// @NamedQuery(name = ClienteSvs.NQ_OBTER_POR_CPF_CNPJ_CARREGADO.NAME, query =
		// ClienteSvs.NQ_OBTER_POR_CPF_CNPJ_CARREGADO.JPQL)
})
public class ClienteSvs implements Serializable, IClienteCentralCreditoDto {
	public static interface NQ_OBTER_POR_CPF {
		public static final String NAME = "ClienteSvs.obterPorCpf";
		public static final String JPQL = "Select c from ClienteSvs c where c.cpf = :" + NQ_OBTER_POR_CPF.NM_PM_CPF;
		public static final String NM_PM_CPF = "cpf";
	}

	public static interface NQ_OBTER_POR_CPF_CNPJ {
		public static final String NAME = "ClienteSvs.obterPorCpfCnpj";
		public static final String JPQL = "Select c from ClienteSvs c where c.cpfCnpj = :"
				+ NQ_OBTER_POR_CPF_CNPJ.NM_PM_CPF_CNPJ;
		public static final String NM_PM_CPF_CNPJ = "cpfCnpj";
	}

	public static interface NQ_OBTER_POR_CNPJ {
		public static final String NAME = "ClienteSvs.obterPorCnpj";
		public static final String JPQL = "Select c from ClienteSvs c where c.cnpj = :" + NQ_OBTER_POR_CNPJ.NM_PM_CNPJ;
		public static final String NM_PM_CNPJ = "cnpj";
	}

	// public static interface NQ_OBTER_POR_CPF_CNPJ_CARREGADO {
	// public static final String NAME = "ClienteSvs.obterPorCpfCnpjCarregado";
	// public static final String JPQL = "Select DISTINCT c from ClienteSvs c " +
	// "LEFT JOIN FETCH c.enderecos e "
	// + "LEFT JOIN FETCH c.outrasRendas or" + "LEFT JOIN FETCH c.faturamentos fa"
	// + "LEFT JOIN FETCH c.fornecedores fo" + "LEFT JOIN FETCH c.avalistas a"
	// + "LEFT JOIN FETCH c.telefones t" + "LEFT JOIN FETCH c.imoveisPatrimonio ip"
	// + "LEFT JOIN FETCH c.veiculosPatrimonio vp" + "LEFT JOIN FETCH
	// c.referenciasPessoais rp"
	// + " where c.cpfCnpj = :" + NQ_OBTER_POR_CPF_CNPJ_CARREGADO.NM_PM_CPF_CNPJ;
	// public static final String NM_PM_CPF_CNPJ = "cpfCnpj";
	// }

	private static final long serialVersionUID = 393996944983548069L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CLIENTESVS")
	@SequenceGenerator(name = "SEQ_CLIENTESVS", sequenceName = "SEQ_CLIENTESVS", allocationSize = 1)
	private Long id;

	@Column
	private Boolean possuiCnh;

	@Column
	private String nome;

	@Column(length = 100)
	private String nomePai;

	@Column(length = 100)
	private String nomeMae;

	@Column
	private String codigoDealerWorkflow;

	@Column
	private String nomeFantasia;

	@Column
	private String cpfCnpj;

	@Column
	private String cpf;

	@Column
	private String cnpj;

	@Column
	private String rg;

	@Column
	private String orgaoEmissorRg;

	@Column
	private String ufOrgaoEmissorRg;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEstados ufOrgaoEmissorDocRg;

	@Column
	private Calendar dataEmissaoRg;

	@Column
	private String email;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumSexo sexo;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEscolaridade escolaridade;

	@Column
	private Date dataNascimento;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoPessoa tipoPessoa;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumEstadoCivil estadoCivil;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumOrgaoEmissorDocumento orgaoEmissorDocRg;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ocupacao_id")
	private Ocupacao ocupacao;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "conjuge_id")
	private Conjuge conjuge;

	@Column(length = 100)
	private String nomeContador;

	@Column(length = 100)
	private String telefoneContador;

	@ManyToOne()
	@JoinColumn(name = "nacionalidade_id")
	private NacionalidadeSantander nacionalidade;

	@ManyToOne()
	@JoinColumn(name = "estado_nascimento_id")
	private EstadoSantander estadoNascimento;

	@ManyToOne()
	@JoinColumn(name = "municipio_nascimento_id")
	private MunicipioSantander municipioNascimento;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<OutraRendaClienteSvs> outrasRendas;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@OrderBy("dataReferencia desc")
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<FaturamentoClienteSvs> faturamentos;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Avalista> avalistas;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<FornecedorClienteSvs> fornecedores;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Telefone> telefones;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Endereco> enderecos;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<ReferenciaPessoal> referenciasPessoais;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "referencia_bancaria_1_id")
	private ReferenciaBancaria referenciaBancaria1;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "referencia_bancaria_2_id")
	private ReferenciaBancaria referenciaBancaria2;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)

	private List<ImovelPatrimonio> imoveisPatrimonio;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "clienteSvs", orphanRemoval = true)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<VeiculoPatrimonio> veiculosPatrimonio;

	@Column
	private Boolean menorIncapaz;

	// @Transient
	// private Boolean verificadoExistenciaNoDealer;
	//
	// @Transient
	// private Boolean existenteNoDealer;
	//
	// @Transient
	// private String sexoString;
	//
	// @Transient
	// private String tipoPessoaString;
	//
	// @Transient
	// private String estadoCivilString;
	//
	// Utilizado na impressao da ficha do cliente (iReport)
	@Transient
	private VeiculoProposta veiculoProposta;

	// Utilizado na impressao da ficha do cliente (iReport)
	@Transient
	private CalculoRentabilidade calculoContrato;

	public ClienteSvs() {
		this.enderecos = new ArrayList<Endereco>();
		this.telefones = new ArrayList<Telefone>();
		this.faturamentos = new ArrayList<FaturamentoClienteSvs>();
		this.avalistas = new ArrayList<Avalista>();
		this.fornecedores = new ArrayList<FornecedorClienteSvs>();
		this.outrasRendas = new ArrayList<OutraRendaClienteSvs>();
		this.imoveisPatrimonio = new ArrayList<ImovelPatrimonio>();
		this.veiculosPatrimonio = new ArrayList<VeiculoPatrimonio>();
		this.referenciasPessoais = new ArrayList<ReferenciaPessoal>();

	}

	public ClienteSvs(Boolean criarObjetosRelacionados) {
		if (criarObjetosRelacionados != null && criarObjetosRelacionados) {
			this.setEndereco(new Endereco());
			this.setOcupacao(new Ocupacao());
			this.ocupacao.setEndereco(new Endereco());
		}
	}

	public ClienteSvs(ClienteCentralCreditoDto clienteCentralCreditoDto) {

	}

	@JsonIgnore
	public Endereco getEnderecoFichaCliente() {
		if (getEnderecoResidencial() != null) {
			return getEnderecoResidencial();
		}
		if (getEnderecos().size() > 0) {
			return getEnderecos().get(0);
		}

		return null;
	}

	@JsonIgnore
	public Endereco getEnderecoResidencial() {
		if (getEnderecos() == null) {
			return null;
		}
		for (Endereco endereco : getEnderecos()) {
			if (endereco.getTipoEndereco() != null && endereco.getTipoEndereco().getDescricao() != null
					&& endereco.getTipoEndereco().getDescricao().equals("RESIDENCIAL")) {
				return endereco;
			}
		}
		return null;
	}

	@JsonIgnore
	public String getTelefoneResidencial() {
		for (Telefone telefone : getTelefones()) {
			if (telefone.getTipoTelefone().getDescricao().equals("RESIDENCIAL")) {
				return telefone.getNumeroFormatado();
			}
		}
		return null;
	}

	@JsonIgnore
	public String getTelefoneComercial() {
		for (Telefone telefone : getTelefones()) {
			if (telefone.getTipoTelefone().getDescricao().equals("COMERCIAL")) {
				return telefone.getNumeroFormatado();
			}
		}
		return null;
	}

	@JsonIgnore
	public String getTelefoneCelular() {
		for (Telefone telefone : getTelefones()) {
			if (telefone.getTipoTelefone().getDescricao().equals("CELULAR")) {
				return telefone.getNumeroFormatado();
			}
		}
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return this.cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public EnumSexo getSexo() {
		return sexo;
	}

	public void setSexo(EnumSexo sexo) {
		this.sexo = sexo;
	}

	public EnumEstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EnumEstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// public String getSexoString() {
	// return sexoString;
	// }
	//
	// public void setSexoString(String sexoString) {
	// this.sexoString = sexoString;
	//
	// if(sexoString != null && sexoString.trim().equals("M")) {
	// this.sexo = EnumSexo.MASCULINO;
	// }else if(sexoString != null && sexoString.trim().equals("F")) {
	// this.sexo = EnumSexo.FEMININO;
	// }
	// }

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public EnumTipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(EnumTipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	// public String getTipoPessoaString() {
	// return tipoPessoaString;
	// }

	// public void setTipoPessoaString(String tipoPessoaString) {
	// if(tipoPessoaString != null & tipoPessoaString.equals("F")) {
	// tipoPessoa = EnumTipoPessoa.PESSOA_FISICA;
	// }else if(tipoPessoaString != null & tipoPessoaString.equals("J")) {
	// tipoPessoa = EnumTipoPessoa.PESSOA_JURIDICA;
	// }
	// this.tipoPessoaString = tipoPessoaString;
	// }
	//
	// public String getEstadoCivilString() {
	// return estadoCivilString;
	// }
	//
	// public void setEstadoCivilString(String estadoCivilString) {
	//
	// this.estadoCivilString = estadoCivilString;
	//
	// if(estadoCivilString != null) {
	//
	// String estadoCivilTratado = estadoCivilString.trim().toUpperCase();
	//
	// if(estadoCivilTratado.contains("SOLTEIRO")) {
	// this.estadoCivil = EnumEstadoCivil.SOLTEIRO;
	// }else if(estadoCivilTratado.contains("CASADO")) {
	// this.estadoCivil = EnumEstadoCivil.CASADO;
	// }else if(estadoCivilTratado.contains("VIUVO")) {
	// this.estadoCivil = EnumEstadoCivil.VIUVO;
	// }else if(estadoCivilTratado.contains("DIVORCIADO")) {
	// this.estadoCivil = EnumEstadoCivil.DIVORCIADO;
	// }else if(estadoCivilTratado.contains("SEPARADO")) {
	// this.estadoCivil = EnumEstadoCivil.SEPARADO;
	// }
	// }
	// }

	public Endereco getEndereco() {
		if (endereco == null) {
			endereco = new Endereco();
		}
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Telefone> getTelefones() {
		if (telefones == null) {
			telefones = new ArrayList<Telefone>();
		}
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	public List<ReferenciaPessoal> getReferenciasPessoais() {
		if (referenciasPessoais == null) {
			referenciasPessoais = new ArrayList<ReferenciaPessoal>();
		}
		return referenciasPessoais;
	}

	public void setReferenciasPessoais(List<ReferenciaPessoal> referenciasPessoais) {
		this.referenciasPessoais = referenciasPessoais;
	}

	@JsonIgnore
	public List<ReferenciaBancaria> getReferenciasBancarias() {
		List<ReferenciaBancaria> referenciasBancarias = new ArrayList<ReferenciaBancaria>();
		referenciasBancarias.add(getReferenciaBancaria1());
		referenciasBancarias.add(getReferenciaBancaria2());
		return referenciasBancarias;
	}

	public ReferenciaBancaria getReferenciaBancaria1() {
		if (referenciaBancaria1 == null) {
			referenciaBancaria1 = new ReferenciaBancaria();
		}
		return referenciaBancaria1;
	}

	public void setReferenciaBancaria1(ReferenciaBancaria referenciaBancaria1) {
		this.referenciaBancaria1 = referenciaBancaria1;
	}

	public ReferenciaBancaria getReferenciaBancaria2() {
		if (referenciaBancaria2 == null) {
			referenciaBancaria2 = new ReferenciaBancaria();
		}
		return referenciaBancaria2;
	}

	public void setReferenciaBancaria2(ReferenciaBancaria referenciaBancaria2) {
		this.referenciaBancaria2 = referenciaBancaria2;
	}

	public Ocupacao getOcupacao() {
		if (ocupacao == null) {
			this.ocupacao = new Ocupacao();
		}
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	public List<ImovelPatrimonio> getImoveisPatrimonio() {
		if (imoveisPatrimonio == null) {
			imoveisPatrimonio = new ArrayList<ImovelPatrimonio>();
		}
		return imoveisPatrimonio;
	}

	public void setImoveisPatrimonio(List<ImovelPatrimonio> imoveisPatrimonio) {
		this.imoveisPatrimonio = imoveisPatrimonio;
	}

	public List<VeiculoPatrimonio> getVeiculosPatrimonio() {
		if (veiculosPatrimonio == null) {
			veiculosPatrimonio = new ArrayList<VeiculoPatrimonio>();
		}
		return veiculosPatrimonio;
	}

	public void setVeiculosPatrimonio(List<VeiculoPatrimonio> veiculosPatrimonio) {
		this.veiculosPatrimonio = veiculosPatrimonio;
	}

	// public Double getValorTotalPatrimonio() {
	// Double valorPatrimonio = 0d;
	// for(VeiculoPatrimonio veiculo : getVeiculosPatrimonio()) {
	// if(veiculo.getValor() != null) {
	// valorPatrimonio += veiculo.getValor();
	// }
	// }
	// for(ImovelPatrimonio imovel : getImoveisPatrimonio()) {
	// if(imovel.getValor() != null) {
	// valorPatrimonio += imovel.getValor();
	// }
	// }
	// return valorPatrimonio;
	// }
	//
	//

	@JsonIgnore
	public Double getValorTotalPatrimonioImoveis() {
		Double valorPatrimonio = 0d;
		for (ImovelPatrimonio imovel : getImoveisPatrimonio()) {
			if (imovel.getValor() != null) {
				valorPatrimonio += imovel.getValor();
			}
		}
		return valorPatrimonio;
	}
	//
	//
	// public Double getValorTotalPatrimonioVeiculos() {
	// Double valorPatrimonio = 0d;
	// for(VeiculoPatrimonio veiculo : getVeiculosPatrimonio()) {
	// if(veiculo.getValor() != null) {
	// valorPatrimonio += veiculo.getValor();
	// }
	// }
	// return valorPatrimonio;
	// }

	public Conjuge getConjuge() {
		if (conjuge == null) {
			this.conjuge = new Conjuge();
		}
		return conjuge;
	}

	public void setConjuge(Conjuge conjuge) {
		this.conjuge = conjuge;
	}

	public List<Endereco> getEnderecos() {
		if (enderecos == null) {
			enderecos = new ArrayList<Endereco>();
		}
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	// public Boolean getVerificadoExistenciaNoDealer() {
	// return verificadoExistenciaNoDealer;
	// }
	//
	// public void setVerificadoExistenciaNoDealer(Boolean
	// verificadoExistenciaNoDealer) {
	// this.verificadoExistenciaNoDealer = verificadoExistenciaNoDealer;
	// }
	//
	// public Boolean getExistenteNoDealer() {
	// return existenteNoDealer;
	// }
	//
	// public void setExistenteNoDealer(Boolean existenteNoDealer) {
	// this.existenteNoDealer = existenteNoDealer;
	// }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteSvs other = (ClienteSvs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoEmissorRg() {
		return orgaoEmissorRg;
	}

	public void setOrgaoEmissorRg(String orgaoEmissorRg) {
		this.orgaoEmissorRg = orgaoEmissorRg;
	}

	public List<Avalista> getAvalistas() {
		if (avalistas == null) {
			avalistas = new ArrayList<Avalista>();
		}
		return avalistas;
	}

	public void setAvalistas(List<Avalista> avalistas) {
		this.avalistas = avalistas;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Calendar getDataEmissaoRg() {
		return dataEmissaoRg;
	}

	public void setDataEmissaoRg(Calendar dataEmissaoRg) {
		this.dataEmissaoRg = dataEmissaoRg;
	}

	public String getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(String codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getUfOrgaoEmissorRg() {
		return ufOrgaoEmissorRg;
	}

	public void setUfOrgaoEmissorRg(String ufOrgaoEmissorRg) {
		this.ufOrgaoEmissorRg = ufOrgaoEmissorRg;
	}

	public NacionalidadeSantander getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(NacionalidadeSantander nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public EstadoSantander getEstadoNascimento() {
		return estadoNascimento;
	}

	public void setEstadoNascimento(EstadoSantander estadoNascimento) {
		this.estadoNascimento = estadoNascimento;
	}

	public MunicipioSantander getMunicipioNascimento() {
		return municipioNascimento;
	}

	public void setMunicipioNascimento(MunicipioSantander municipioNascimento) {
		this.municipioNascimento = municipioNascimento;
	}

	public Boolean getPossuiCnh() {
		if (possuiCnh == null) {
			possuiCnh = false;
		}
		return possuiCnh;
	}

	public void setPossuiCnh(Boolean possuiCnh) {
		this.possuiCnh = possuiCnh;
	}

	public String getTelefoneContador() {
		return telefoneContador;
	}

	public void setTelefoneContador(String telefoneContador) {
		this.telefoneContador = telefoneContador;
	}

	public List<FornecedorClienteSvs> getFornecedores() {
		if (fornecedores == null) {
			fornecedores = new ArrayList<FornecedorClienteSvs>();
		}
		return fornecedores;
	}

	public void setFornecedores(List<FornecedorClienteSvs> fornecedores) {
		this.fornecedores = fornecedores;
	}

	public List<FaturamentoClienteSvs> getFaturamentos() {
		if (faturamentos == null) {
			faturamentos = new ArrayList<FaturamentoClienteSvs>();
		}
		return faturamentos;
	}

	public void setFaturamentos(List<FaturamentoClienteSvs> faturamentos) {
		this.faturamentos = faturamentos;
	}

	// public Double getValorTotalUltimos12Faturamentos() {
	// int i = 1;
	// double total = 0d;
	// for(FaturamentoClienteSvs faturamento : getFaturamentos() ) {
	// if(i <= 12) {
	// total = total + faturamento.getValor().doubleValue();
	// }else {
	// break;
	// }
	// i++;
	// }
	// return total;
	// }

	public List<OutraRendaClienteSvs> getOutrasRendas() {
		if (outrasRendas == null) {
			outrasRendas = new ArrayList<OutraRendaClienteSvs>();
		}
		return outrasRendas;
	}

	public void setOutrasRendas(List<OutraRendaClienteSvs> outrasRendas) {
		this.outrasRendas = outrasRendas;
	}

	public String getNomeContador() {
		return nomeContador;
	}

	public void setNomeContador(String nomeContador) {
		this.nomeContador = nomeContador;
	}

	public VeiculoProposta getVeiculoProposta() {
		return veiculoProposta;
	}

	public void setVeiculoProposta(VeiculoProposta veiculoProposta) {
		this.veiculoProposta = veiculoProposta;
	}

	public CalculoRentabilidade getCalculoContrato() {
		return calculoContrato;
	}

	public void setCalculoContrato(CalculoRentabilidade calculoContrato) {
		this.calculoContrato = calculoContrato;
	}

	public EnumOrgaoEmissorDocumento getOrgaoEmissorDocRg() {
		return orgaoEmissorDocRg;
	}

	public void setOrgaoEmissorDocRg(EnumOrgaoEmissorDocumento orgaoEmissorDocRg) {
		this.orgaoEmissorDocRg = orgaoEmissorDocRg;
	}

	public EnumEstados getUfOrgaoEmissorDocRg() {
		return ufOrgaoEmissorDocRg;
	}

	public void setUfOrgaoEmissorDocRg(EnumEstados ufOrgaoEmissorDocRg) {
		this.ufOrgaoEmissorDocRg = ufOrgaoEmissorDocRg;
	}

	public EnumEscolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(EnumEscolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public Boolean getMenorIncapaz() {
		return menorIncapaz;
	}

	public void setMenorIncapaz(Boolean menorIncapaz) {
		this.menorIncapaz = menorIncapaz;
	}

	// ----------- COMECA AQUI

	@Override
	public Calendar getDataValidadeLimite() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDataValidadeLimite(Date dataValidadeLimite) {
		// TODO Auto-generated method stub

	}

	@Override
	public BigDecimal getValorLimite() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setValorLimite(BigDecimal valorLimite) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setStatus(String status) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getTipoCliente() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTipoCliente(String tipoCliente) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getTipoPessoaClienteStr() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTipoPessoaClienteStr(String tipoPessoaClienteStr) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getBairro() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBairro(String bairro) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getCidade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCidade(String cidade) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getSiglaEstado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSiglaEstado(String siglaEstado) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getCep() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCep(String cep) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getNumeroTelefone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setNumeroTelefone(String numeroTelefone) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getNumeroCelular() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setNumeroCelular(String numeroCelular) {
		// TODO Auto-generated method stub

	}

	@Override
	public Calendar getDataValidadeLimiteDealerWrk() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDataValidadeLimiteDealerWrk(Date dataValidadeLimiteDealerWrk) {
		// TODO Auto-generated method stub

	}

	@Override
	public BigDecimal getValorLimiteDealerWrk() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setValorLimiteDealerWrk(BigDecimal valorLimiteDealerWrk) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getStatusDealerWrk() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setStatusDealerWrk(String statusDealerWrk) {
		// TODO Auto-generated method stub

	}

	@Override
	public Calendar getDataValidadeLimiteNbs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDataValidadeLimiteNbs(Date dataValidadeLimiteNbs) {
		// TODO Auto-generated method stub

	}

	@Override
	public Calendar getDataValidadeLimiteSisdia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDataValidadeLimiteSisdia(Date dataValidadeLimiteSisdia) {
		// TODO Auto-generated method stub

	}

	@Override
	public BigDecimal getValorLimiteNbs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setValorLimiteNbs(BigDecimal valorLimiteNbs) {
		// TODO Auto-generated method stub

	}

	@Override
	public BigDecimal getValorLimiteSisdia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setValorLimiteSisdia(BigDecimal valorLimiteSisdia) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getStatusNbs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setStatusNbs(String statusNbs) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getStatusSisdia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setStatusSisdia(String statusSisdia) {
		// TODO Auto-generated method stub

	}

}
