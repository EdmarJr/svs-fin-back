/**
 * Socio_Participacao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Socio_Participacao  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String cpf_cnpj;

    private java.lang.String ds_sede;

    private java.lang.String nm_empresa_participa;

    private java.lang.String nr_cnpj;

    private java.lang.String pc_participacao;

    private java.lang.String vl_capital_social;

    public Socio_Participacao() {
    }

    public Socio_Participacao(
           java.lang.String cpf_cnpj,
           java.lang.String ds_sede,
           java.lang.String nm_empresa_participa,
           java.lang.String nr_cnpj,
           java.lang.String pc_participacao,
           java.lang.String vl_capital_social) {
        this.cpf_cnpj = cpf_cnpj;
        this.ds_sede = ds_sede;
        this.nm_empresa_participa = nm_empresa_participa;
        this.nr_cnpj = nr_cnpj;
        this.pc_participacao = pc_participacao;
        this.vl_capital_social = vl_capital_social;
    }


    /**
     * Gets the cpf_cnpj value for this Socio_Participacao.
     * 
     * @return cpf_cnpj
     */
    public java.lang.String getCpf_cnpj() {
        return cpf_cnpj;
    }


    /**
     * Sets the cpf_cnpj value for this Socio_Participacao.
     * 
     * @param cpf_cnpj
     */
    public void setCpf_cnpj(java.lang.String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }


    /**
     * Gets the ds_sede value for this Socio_Participacao.
     * 
     * @return ds_sede
     */
    public java.lang.String getDs_sede() {
        return ds_sede;
    }


    /**
     * Sets the ds_sede value for this Socio_Participacao.
     * 
     * @param ds_sede
     */
    public void setDs_sede(java.lang.String ds_sede) {
        this.ds_sede = ds_sede;
    }


    /**
     * Gets the nm_empresa_participa value for this Socio_Participacao.
     * 
     * @return nm_empresa_participa
     */
    public java.lang.String getNm_empresa_participa() {
        return nm_empresa_participa;
    }


    /**
     * Sets the nm_empresa_participa value for this Socio_Participacao.
     * 
     * @param nm_empresa_participa
     */
    public void setNm_empresa_participa(java.lang.String nm_empresa_participa) {
        this.nm_empresa_participa = nm_empresa_participa;
    }


    /**
     * Gets the nr_cnpj value for this Socio_Participacao.
     * 
     * @return nr_cnpj
     */
    public java.lang.String getNr_cnpj() {
        return nr_cnpj;
    }


    /**
     * Sets the nr_cnpj value for this Socio_Participacao.
     * 
     * @param nr_cnpj
     */
    public void setNr_cnpj(java.lang.String nr_cnpj) {
        this.nr_cnpj = nr_cnpj;
    }


    /**
     * Gets the pc_participacao value for this Socio_Participacao.
     * 
     * @return pc_participacao
     */
    public java.lang.String getPc_participacao() {
        return pc_participacao;
    }


    /**
     * Sets the pc_participacao value for this Socio_Participacao.
     * 
     * @param pc_participacao
     */
    public void setPc_participacao(java.lang.String pc_participacao) {
        this.pc_participacao = pc_participacao;
    }


    /**
     * Gets the vl_capital_social value for this Socio_Participacao.
     * 
     * @return vl_capital_social
     */
    public java.lang.String getVl_capital_social() {
        return vl_capital_social;
    }


    /**
     * Sets the vl_capital_social value for this Socio_Participacao.
     * 
     * @param vl_capital_social
     */
    public void setVl_capital_social(java.lang.String vl_capital_social) {
        this.vl_capital_social = vl_capital_social;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Socio_Participacao)) return false;
        Socio_Participacao other = (Socio_Participacao) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.cpf_cnpj==null && other.getCpf_cnpj()==null) || 
             (this.cpf_cnpj!=null &&
              this.cpf_cnpj.equals(other.getCpf_cnpj()))) &&
            ((this.ds_sede==null && other.getDs_sede()==null) || 
             (this.ds_sede!=null &&
              this.ds_sede.equals(other.getDs_sede()))) &&
            ((this.nm_empresa_participa==null && other.getNm_empresa_participa()==null) || 
             (this.nm_empresa_participa!=null &&
              this.nm_empresa_participa.equals(other.getNm_empresa_participa()))) &&
            ((this.nr_cnpj==null && other.getNr_cnpj()==null) || 
             (this.nr_cnpj!=null &&
              this.nr_cnpj.equals(other.getNr_cnpj()))) &&
            ((this.pc_participacao==null && other.getPc_participacao()==null) || 
             (this.pc_participacao!=null &&
              this.pc_participacao.equals(other.getPc_participacao()))) &&
            ((this.vl_capital_social==null && other.getVl_capital_social()==null) || 
             (this.vl_capital_social!=null &&
              this.vl_capital_social.equals(other.getVl_capital_social())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCpf_cnpj() != null) {
            _hashCode += getCpf_cnpj().hashCode();
        }
        if (getDs_sede() != null) {
            _hashCode += getDs_sede().hashCode();
        }
        if (getNm_empresa_participa() != null) {
            _hashCode += getNm_empresa_participa().hashCode();
        }
        if (getNr_cnpj() != null) {
            _hashCode += getNr_cnpj().hashCode();
        }
        if (getPc_participacao() != null) {
            _hashCode += getPc_participacao().hashCode();
        }
        if (getVl_capital_social() != null) {
            _hashCode += getVl_capital_social().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Socio_Participacao.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Socio_Participacao"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpf_cnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "cpf_cnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_sede");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_sede"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_empresa_participa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_empresa_participa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_cnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_cnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_participacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "pc_participacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_capital_social");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_capital_social"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
