package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoIPTU {
	
	@JsonProperty("Cônjuge")
	CONJUGE("Cônjuge"),
		@JsonProperty("Parentes")
	PARENTES("Parentes"),
		@JsonProperty("Próprio")
	PROPRIO("Próprio"),
		@JsonProperty("Terceiros")
	TERCEIROS("Terceiros");
	
	private String label;
	
	private EnumTipoIPTU(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}
}
