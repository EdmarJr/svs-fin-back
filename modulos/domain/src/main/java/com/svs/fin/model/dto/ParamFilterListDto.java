package com.svs.fin.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "Filtros para listagem")
public class ParamFilterListDto {

	@ApiParam(value="Campo a ser filtrado")
    private String field;
    private String value;
    private String minValue;
    private String maxValue;
    private List<String> values;
    private Boolean exact;
    private String dateFormat;
    private String type;
	
	public ParamFilterListDto() {}
	
	public ParamFilterListDto(String field,
                              String value,
                              Boolean exact,
                              String type,
                              String minValue,
                              String maxValue,
                              String dateFormat,
                              List<String> values) {

		this.field = field;
		this.value = value;
		this.exact = exact;
		this.type = type;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.dateFormat = dateFormat;
		this.values = values;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getExact() {
		if(exact == null) {
			exact = false;
		}
		return exact;
	}

	public void setExact(Boolean exact) {
		this.exact = exact;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public List<String> getValues() {
	    if(values == null){
	        values = new ArrayList<>();
        }
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
