/**
 * Tipo_Endereco.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Endereco")
public class Tipo_Endereco implements java.io.Serializable {
	private java.lang.String ds_tipo_endereco;

	@Id
	private java.lang.Integer id_tipo_endereco;

	private java.lang.String id_tipo_pessoa;

	public Tipo_Endereco() {
	}

	public Tipo_Endereco(java.lang.String ds_tipo_endereco, java.lang.Integer id_tipo_endereco,
			java.lang.String id_tipo_pessoa) {
		this.ds_tipo_endereco = ds_tipo_endereco;
		this.id_tipo_endereco = id_tipo_endereco;
		this.id_tipo_pessoa = id_tipo_pessoa;
	}

	/**
	 * Gets the ds_tipo_endereco value for this Tipo_Endereco.
	 * 
	 * @return ds_tipo_endereco
	 */
	public java.lang.String getDs_tipo_endereco() {
		return ds_tipo_endereco;
	}

	/**
	 * Sets the ds_tipo_endereco value for this Tipo_Endereco.
	 * 
	 * @param ds_tipo_endereco
	 */
	public void setDs_tipo_endereco(java.lang.String ds_tipo_endereco) {
		this.ds_tipo_endereco = ds_tipo_endereco;
	}

	/**
	 * Gets the id_tipo_endereco value for this Tipo_Endereco.
	 * 
	 * @return id_tipo_endereco
	 */
	public java.lang.Integer getId_tipo_endereco() {
		return id_tipo_endereco;
	}

	/**
	 * Sets the id_tipo_endereco value for this Tipo_Endereco.
	 * 
	 * @param id_tipo_endereco
	 */
	public void setId_tipo_endereco(java.lang.Integer id_tipo_endereco) {
		this.id_tipo_endereco = id_tipo_endereco;
	}

	/**
	 * Gets the id_tipo_pessoa value for this Tipo_Endereco.
	 * 
	 * @return id_tipo_pessoa
	 */
	public java.lang.String getId_tipo_pessoa() {
		return id_tipo_pessoa;
	}

	/**
	 * Sets the id_tipo_pessoa value for this Tipo_Endereco.
	 * 
	 * @param id_tipo_pessoa
	 */
	public void setId_tipo_pessoa(java.lang.String id_tipo_pessoa) {
		this.id_tipo_pessoa = id_tipo_pessoa;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Endereco))
			return false;
		Tipo_Endereco other = (Tipo_Endereco) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_tipo_endereco == null && other.getDs_tipo_endereco() == null)
						|| (this.ds_tipo_endereco != null && this.ds_tipo_endereco.equals(other.getDs_tipo_endereco())))
				&& ((this.id_tipo_endereco == null && other.getId_tipo_endereco() == null)
						|| (this.id_tipo_endereco != null && this.id_tipo_endereco.equals(other.getId_tipo_endereco())))
				&& ((this.id_tipo_pessoa == null && other.getId_tipo_pessoa() == null)
						|| (this.id_tipo_pessoa != null && this.id_tipo_pessoa.equals(other.getId_tipo_pessoa())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_endereco() != null) {
			_hashCode += getDs_tipo_endereco().hashCode();
		}
		if (getId_tipo_endereco() != null) {
			_hashCode += getId_tipo_endereco().hashCode();
		}
		if (getId_tipo_pessoa() != null) {
			_hashCode += getId_tipo_pessoa().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Endereco.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endereco"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_endereco");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_tipo_endereco"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_endereco");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_tipo_endereco"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_pessoa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_tipo_pessoa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
