package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "cnpj_inst_financ")
@NamedQueries({
		@NamedQuery(name = CnpjInstituicaoFinanceira.NQ_OBTER_POR_INSTITUICAO_FINANCEIRA.NAME, query = CnpjInstituicaoFinanceira.NQ_OBTER_POR_INSTITUICAO_FINANCEIRA.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class CnpjInstituicaoFinanceira implements Serializable {
	public static interface NQ_OBTER_POR_INSTITUICAO_FINANCEIRA {
		public static final String NAME = "CnpjInstituicaoFinanceira.obterPorInstituicaoFinanceira";
		public static final String JPQL = "Select c from CnpjInstituicaoFinanceira c where c.instituicaoFinanceira.id = :"
				+ NQ_OBTER_POR_INSTITUICAO_FINANCEIRA.NM_PM_INSTITUICAO_FINANCEIRA;
		public static final String NM_PM_INSTITUICAO_FINANCEIRA = "idInstituicaoFinanceira";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6472910578828736431L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cnpj_inst_financ")
	@SequenceGenerator(name = "seq_cnpj_inst_financ", sequenceName = "seq_cnpj_inst_financ", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "instituicao_financeira_id")
	private InstituicaoFinanceira instituicaoFinanceira;

	@Column(length = 50)
	private String cnpj;

	@Column(length = 100)
	private String razaoSocial;

	@Column
	private Boolean ativo;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "cnpj_if_prod_fin_v2", joinColumns = @JoinColumn(name = "cnpj_inst_financeira_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "produto_financeiro_id", referencedColumnName = "id"))
	private List<ProdutoFinanceiro> produtosFinanceiros;

	public CnpjInstituicaoFinanceira() {
		setProdutosFinanceiros(new ArrayList<ProdutoFinanceiro>());
	}

	public CnpjInstituicaoFinanceira(InstituicaoFinanceira instituicaoFinanceira) {
		this.instituicaoFinanceira = instituicaoFinanceira;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<ProdutoFinanceiro> getProdutosFinanceiros() {
		if (produtosFinanceiros == null) {
			produtosFinanceiros = new ArrayList<ProdutoFinanceiro>();
		}
		return produtosFinanceiros;
	}

	public void setProdutosFinanceiros(List<ProdutoFinanceiro> produtosFinanceiros) {
		this.produtosFinanceiros = produtosFinanceiros;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public InstituicaoFinanceira getInstituicaoFinanceira() {
		return instituicaoFinanceira;
	}

	public void setInstituicaoFinanceira(InstituicaoFinanceira instituicaoFinanceira) {
		this.instituicaoFinanceira = instituicaoFinanceira;
	}
}
