package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.enums.EnumTipoImovel;
import com.svs.fin.integracao.safra.interfacesuteis.IBemImovelSafra;

@Entity
@NamedQueries({
		@NamedQuery(name = ImovelPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = ImovelPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImovelPatrimonio implements Serializable, IBemImovelSafra {

	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "ImovelPatrimonio.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from ImovelPatrimonio e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4951231723314546883L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_IMOVELPATRIMONIO")
	@SequenceGenerator(name = "SEQ_IMOVELPATRIMONIO", sequenceName = "SEQ_IMOVELPATRIMONIO", allocationSize = 1)
	private Long id;

	@Column(length = 1000)
	private String descricao;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoImovel tipoImovel;

	@Column
	private Double valor;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private ClienteSvs clienteSvs;

	@ManyToOne
	@JoinColumn(name = "avalista_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private Avalista avalista;

	public Avalista getAvalista() {
		return avalista;
	}

	public void setAvalista(Avalista avalista) {
		this.avalista = avalista;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	@Transient
	private Boolean editando;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImovelPatrimonio other = (ImovelPatrimonio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public EnumTipoImovel getTipoImovel() {
		return tipoImovel;
	}

	public void setTipoImovel(EnumTipoImovel tipoImovel) {
		this.tipoImovel = tipoImovel;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Endereco getEndereco() {
		if (endereco == null) {
			this.endereco = new Endereco();
		}
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}
}
