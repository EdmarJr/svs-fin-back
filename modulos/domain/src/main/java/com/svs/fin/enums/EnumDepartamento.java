package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumDepartamento {

	@JsonProperty("Novos")
	NOVOS("Novos", "1000", 1), @JsonProperty("Frotista")
	FROTISTA("Frotista", "2000", 2), @JsonProperty("Seminovos")
	SEMINOVOS("Seminovos", "3000", 3), @JsonProperty("Peças")
	PECAS("Peças", "4000", 4), @JsonProperty("Administração")
	ADMINISTRACAO("Administração", "6000", 6), @JsonProperty("Administração Brasil")
	ADMINISTRACAOBRASIL("Administração", "", 12), @JsonProperty("Assistência Técnica")
	ASSISTENCIATECNICA("Assistência Técnica", "5000", 5), @JsonProperty("Renovação")
	RENOVACAO("Renovação", "", 0), @JsonProperty("Dir. Contabilidade")
	CONTABILIDADE("Dir. Contabilidade", "", 2), @JsonProperty("Dir. Financeira")
	FINANCEIRO("Dir. Financeira", "", 4), @JsonProperty("Dir. Gente & Gestão")
	DIRGENTEGESTAO("Dir. Gente & Gestão", "", 5), @JsonProperty("Departamento Pessoal")
	DP("Departamento Pessoal", "", 6), @JsonProperty("RH")
	RH("RH", "", 7), @JsonProperty("Dir. TI")
	TI("Dir. TI", "", 1), @JsonProperty("Dir. Comercial")
	DIRCOMERCIAL("Dir. Comercial", "", 3), @JsonProperty("Dir. Corporativa")
	DIRCORPORATIVA("Dir. Corporativa", "", 8), @JsonProperty("Jurídico")
	JURIDICO("Jurídico", "", 13), @JsonProperty("Corporativo")
	CORPORATIVO("Corporativo", "", 14), @JsonProperty("Controladoria")
	CONTROLADORIA("Controladoria", "", 11), @JsonProperty("Auditoria")
	AUDITORIA("Auditoria", "", 10), @JsonProperty("Qualidade")
	QUALIDADE("Qualidade", "", 9), @JsonProperty("Centro de Benefícios")
	CENTROBENEFICIO("Centro de Benefícios", "", 0), @JsonProperty("Seminovos Safari")
	SEMINOVOS_SAFARI("Seminovos Safari", "", 6), @JsonProperty("Seminovos ADM")
	SEMINOVOS_ADM("Seminovos ADM", "", 7), @JsonProperty("Total")
	TOTAL("Total", "", 15);

	private EnumDepartamento(String label, String centroCusto, Integer ordem) {
		this.label = label;
		this.centroCusto = centroCusto;
		this.ordem = ordem;
	}

	private String centroCusto;
	private String label;
	private Integer ordem;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

}
