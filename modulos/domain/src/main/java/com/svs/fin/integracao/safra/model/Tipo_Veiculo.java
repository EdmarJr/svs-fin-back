/**
 * Tipo_Veiculo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Veiculo")
public class Tipo_Veiculo implements java.io.Serializable {
	private java.lang.String ds_tipo_veiculo;

	@Id
	private java.lang.String id_tipo_veiculo;

	public Tipo_Veiculo() {
	}

	public Tipo_Veiculo(java.lang.String ds_tipo_veiculo, java.lang.String id_tipo_veiculo) {
		this.ds_tipo_veiculo = ds_tipo_veiculo;
		this.id_tipo_veiculo = id_tipo_veiculo;
	}

	/**
	 * Gets the ds_tipo_veiculo value for this Tipo_Veiculo.
	 * 
	 * @return ds_tipo_veiculo
	 */
	public java.lang.String getDs_tipo_veiculo() {
		return ds_tipo_veiculo;
	}

	/**
	 * Sets the ds_tipo_veiculo value for this Tipo_Veiculo.
	 * 
	 * @param ds_tipo_veiculo
	 */
	public void setDs_tipo_veiculo(java.lang.String ds_tipo_veiculo) {
		this.ds_tipo_veiculo = ds_tipo_veiculo;
	}

	/**
	 * Gets the id_tipo_veiculo value for this Tipo_Veiculo.
	 * 
	 * @return id_tipo_veiculo
	 */
	public java.lang.String getId_tipo_veiculo() {
		return id_tipo_veiculo;
	}

	/**
	 * Sets the id_tipo_veiculo value for this Tipo_Veiculo.
	 * 
	 * @param id_tipo_veiculo
	 */
	public void setId_tipo_veiculo(java.lang.String id_tipo_veiculo) {
		this.id_tipo_veiculo = id_tipo_veiculo;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Veiculo))
			return false;
		Tipo_Veiculo other = (Tipo_Veiculo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_tipo_veiculo == null && other.getDs_tipo_veiculo() == null)
						|| (this.ds_tipo_veiculo != null && this.ds_tipo_veiculo.equals(other.getDs_tipo_veiculo())))
				&& ((this.id_tipo_veiculo == null && other.getId_tipo_veiculo() == null)
						|| (this.id_tipo_veiculo != null && this.id_tipo_veiculo.equals(other.getId_tipo_veiculo())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_veiculo() != null) {
			_hashCode += getDs_tipo_veiculo().hashCode();
		}
		if (getId_tipo_veiculo() != null) {
			_hashCode += getId_tipo_veiculo().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Veiculo.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_veiculo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_tipo_veiculo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_veiculo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_tipo_veiculo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
