package com.svs.fin.model.entities;

import java.io.Serializable;

public class SimulacaoPmtOperacaoFinanciada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1061714411986625991L;

	private MascaraTaxaTabelaFinanceira mascara;

	private Integer qtdParcelas;

	private Double valorPmt;

	private Boolean selecionado;

	private Double coeficiente;

	public MascaraTaxaTabelaFinanceira getMascara() {
		return mascara;
	}

	public void setMascara(MascaraTaxaTabelaFinanceira mascara) {
		this.mascara = mascara;
	}

	public Double getValorPmt() {
		return valorPmt;
	}

	public void setValorPmt(Double valorPmt) {
		this.valorPmt = valorPmt;
	}

	public Integer getQtdParcelas() {
		return qtdParcelas;
	}

	public void setQtdParcelas(Integer qtdParcelas) {
		this.qtdParcelas = qtdParcelas;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public Double getCoeficiente() {
		return coeficiente;
	}

	public void setCoeficiente(Double coeficiente) {
		this.coeficiente = coeficiente;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mascara == null) ? 0 : mascara.hashCode());
		result = prime * result + ((qtdParcelas == null) ? 0 : qtdParcelas.hashCode());
		result = prime * result + ((selecionado == null) ? 0 : selecionado.hashCode());
		result = prime * result + ((valorPmt == null) ? 0 : valorPmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimulacaoPmtOperacaoFinanciada other = (SimulacaoPmtOperacaoFinanciada) obj;
		if (mascara == null) {
			if (other.mascara != null)
				return false;
		} else if (!mascara.equals(other.mascara))
			return false;
		if (qtdParcelas == null) {
			if (other.qtdParcelas != null)
				return false;
		} else if (!qtdParcelas.equals(other.qtdParcelas))
			return false;
		if (selecionado == null) {
			if (other.selecionado != null)
				return false;
		} else if (!selecionado.equals(other.selecionado))
			return false;
		if (valorPmt == null) {
			if (other.valorPmt != null)
				return false;
		} else if (!valorPmt.equals(other.valorPmt))
			return false;
		return true;
	}
}
