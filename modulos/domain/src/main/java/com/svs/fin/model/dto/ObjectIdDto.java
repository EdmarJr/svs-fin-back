package com.svs.fin.model.dto;

import javax.validation.constraints.NotNull;

public class ObjectIdDto {

    public ObjectIdDto(){}

    public ObjectIdDto(Long id){
        this.id = id;
    }

	@NotNull
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
