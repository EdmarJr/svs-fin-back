package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class AtxResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private FleetOptions fleet;

	private Insurances[] insurances;

	private PaymentForms[] paymentForms;

	private Terms[] terms;

	private Entry entry;

	private String priceTable;

	private Fees fees;

	private FirstInstallment firstInstallment;
	
	
	public Fleet getFleetById(String id) {
		for(Fleet fleet : fleet.getFleets()) {
			if(fleet.getId().equals(id)) {
				return fleet;
			}
		}
		return null;
	}
	

	public FleetOptions getFleet() {
		return fleet;
	}

	public void setFleet(FleetOptions fleet) {
		this.fleet = fleet;
	}

	public Insurances[] getInsurances() {
		return insurances;
	}

	public void setInsurances(Insurances[] insurances) {
		this.insurances = insurances;
	}

	public PaymentForms[] getPaymentForms() {
		return paymentForms;
	}

	public void setPaymentForms(PaymentForms[] paymentForms) {
		this.paymentForms = paymentForms;
	}

	public Terms[] getTerms() {
		return terms;
	}

	public void setTerms(Terms[] terms) {
		this.terms = terms;
	}

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

	public String getPriceTable() {
		return priceTable;
	}

	public void setPriceTable(String priceTable) {
		this.priceTable = priceTable;
	}

	public Fees getFees() {
		return fees;
	}

	public void setFees(Fees fees) {
		this.fees = fees;
	}

	public FirstInstallment getFirstInstallment() {
		return firstInstallment;
	}

	public void setFirstInstallment(FirstInstallment firstInstallment) {
		this.firstInstallment = firstInstallment;
	}

	@Override
	public String toString() {
		return "ClassPojo [fleet = " + fleet + ", insurances = " + insurances + ", paymentForms = " + paymentForms
				+ ", terms = " + terms + ", entry = " + entry + ", priceTable = " + priceTable + ", fees = " + fees
				+ ", firstInstallment = " + firstInstallment + "]";
	}
}
