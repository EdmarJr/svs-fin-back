package com.svs.fin.enums;

public enum SimNao {
	NÃO("Não"), 
	SIM("Sim");
	
	private String label;
	
	private SimNao(final String label) {
		this.label = label;
	}

	
	
	public void setLabel(String label) {
		this.label = label;
	}



	public String getLabel() {
		return label;
	}
}
