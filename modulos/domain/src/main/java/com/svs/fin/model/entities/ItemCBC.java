package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
	@NamedQuery(name = ItemCBC.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME, query = ItemCBC.NQ_OBTER_POR_OPERACAO_FINANCIADA.JPQL) })
public class ItemCBC implements Serializable {

	public static interface NQ_OBTER_POR_OPERACAO_FINANCIADA {
		public static final String NAME = "ItemCBC.obterPorOperacaoFinanciada";
		public static final String JPQL = "Select pe from ItemCBC pe where pe.operacaoFinanciada.id = :"
				+ NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA;
		public static final String NM_PM_ID_OPERACAO_FINANCIADA = "idOperacaoFinanciada";
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6140555607944387907L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ITEMCBC")
	@SequenceGenerator(name = "SEQ_ITEMCBC", sequenceName = "SEQ_ITEMCBC", allocationSize = 1)
	private Long id;

	@ManyToOne()
	@JoinColumn(name = "id_operacao")
	private OperacaoFinanciada operacaoFinanciada;

	@Column
	private Double valor;

	@Column
	private Calendar dataPrevisao;

	@Column
	private Integer codigo;

	@ManyToOne
	@JoinColumn(name = "id_opcao_item_cbc")
	private OpcaoItemCbc opcaoItemCbc;

	@Column
	private Boolean adicionarAoFinanciamento;

	@Transient
	private Boolean editando;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OperacaoFinanciada getOperacaoFinanciada() {
		return operacaoFinanciada;
	}

	public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public Boolean getAdicionarAoFinanciamento() {
		return adicionarAoFinanciamento;
	}

	public void setAdicionarAoFinanciamento(Boolean adicionarAoFinanciamento) {
		this.adicionarAoFinanciamento = adicionarAoFinanciamento;
	}

	public OpcaoItemCbc getOpcaoItemCbc() {
		return opcaoItemCbc;
	}

	public void setOpcaoItemCbc(OpcaoItemCbc opcaoItemCbc) {
		this.opcaoItemCbc = opcaoItemCbc;
	}

	public Calendar getDataPrevisao() {
		return dataPrevisao;
	}

	public void setDataPrevisao(Calendar dataPrevisao) {
		this.dataPrevisao = dataPrevisao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
}
