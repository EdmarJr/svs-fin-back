package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQueries({
		@NamedQuery(name = FaturamentoClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = FaturamentoClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class FaturamentoClienteSvs implements Serializable {
	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "FaturamentoClienteSvs.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from FaturamentoClienteSvs e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7100802691241284637L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FATURAMENTOCLIENTESVS")
	@SequenceGenerator(name = "SEQ_FATURAMENTOCLIENTESVS", sequenceName = "SEQ_FATURAMENTOCLIENTESVS", allocationSize = 1)
	private Long id;

	@Column
	private Calendar dataReferencia;

	@Column
	private Double valor;

	@Transient
	private Boolean editando;

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	private ClienteSvs clienteSvs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Calendar dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FaturamentoClienteSvs other = (FaturamentoClienteSvs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}
}
