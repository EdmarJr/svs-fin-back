/**
 * Avalista.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Avalista  extends com.svs.fin.integracao.safra.model.Proposta_PF  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] bens_imoveis;

    private com.svs.fin.integracao.safra.model_comum.Endereco endereco;

    private java.lang.String id_cliente;

    private java.lang.String nm_cliente_rsoc;

    private com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] referencias_bancarias;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone;

    public Avalista() {
    }

    public Avalista(
           com.svs.fin.integracao.safra.model_comum.Bem_Veiculo[] bem_veiculo,
           com.svs.fin.integracao.safra.model_comum.Cnh cnh,
           com.svs.fin.integracao.safra.model_comum.Conjuge_Dados_Basicos conjuge_dados_basicos,
           com.svs.fin.integracao.safra.model_comum.Dados_Profissionais dados_profissionais,
           com.svs.fin.integracao.safra.model_comum.Documento documento,
           com.svs.fin.integracao.safra.model_comum.Empresa_Anterior empresa_anterior,
           com.svs.fin.integracao.safra.model_comum.Filiacao filiacao,
           com.svs.fin.integracao.safra.model_dados_financiamento.Isencoes isencao_fiscal,
           com.svs.fin.integracao.safra.model_comum.Local_Nascimento local_nascimento,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados outros_dados,
           com.svs.fin.integracao.safra.model_comum.Outros_Dados_Residencia outros_dados_residencia,
           java.lang.Boolean permiteRenda,
           com.svs.fin.integracao.safra.model_comum.Ppe ppe,
           com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal[] referencia_pessoal,
           com.svs.fin.integracao.safra.model_comum.Dados_Profissionais renda_adicional,
           com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] bens_imoveis,
           com.svs.fin.integracao.safra.model_comum.Endereco endereco,
           java.lang.String id_cliente,
           java.lang.String nm_cliente_rsoc,
           com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] referencias_bancarias,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        super(
            bem_veiculo,
            cnh,
            conjuge_dados_basicos,
            dados_profissionais,
            documento,
            empresa_anterior,
            filiacao,
            isencao_fiscal,
            local_nascimento,
            outros_dados,
            outros_dados_residencia,
            permiteRenda,
            ppe,
            referencia_pessoal,
            renda_adicional);
        this.bens_imoveis = bens_imoveis;
        this.endereco = endereco;
        this.id_cliente = id_cliente;
        this.nm_cliente_rsoc = nm_cliente_rsoc;
        this.referencias_bancarias = referencias_bancarias;
        this.telefone = telefone;
    }


    /**
     * Gets the bens_imoveis value for this Avalista.
     * 
     * @return bens_imoveis
     */
    public com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] getBens_imoveis() {
        return bens_imoveis;
    }


    /**
     * Sets the bens_imoveis value for this Avalista.
     * 
     * @param bens_imoveis
     */
    public void setBens_imoveis(com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] bens_imoveis) {
        this.bens_imoveis = bens_imoveis;
    }


    /**
     * Gets the endereco value for this Avalista.
     * 
     * @return endereco
     */
    public com.svs.fin.integracao.safra.model_comum.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this Avalista.
     * 
     * @param endereco
     */
    public void setEndereco(com.svs.fin.integracao.safra.model_comum.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the id_cliente value for this Avalista.
     * 
     * @return id_cliente
     */
    public java.lang.String getId_cliente() {
        return id_cliente;
    }


    /**
     * Sets the id_cliente value for this Avalista.
     * 
     * @param id_cliente
     */
    public void setId_cliente(java.lang.String id_cliente) {
        this.id_cliente = id_cliente;
    }


    /**
     * Gets the nm_cliente_rsoc value for this Avalista.
     * 
     * @return nm_cliente_rsoc
     */
    public java.lang.String getNm_cliente_rsoc() {
        return nm_cliente_rsoc;
    }


    /**
     * Sets the nm_cliente_rsoc value for this Avalista.
     * 
     * @param nm_cliente_rsoc
     */
    public void setNm_cliente_rsoc(java.lang.String nm_cliente_rsoc) {
        this.nm_cliente_rsoc = nm_cliente_rsoc;
    }


    /**
     * Gets the referencias_bancarias value for this Avalista.
     * 
     * @return referencias_bancarias
     */
    public com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] getReferencias_bancarias() {
        return referencias_bancarias;
    }


    /**
     * Sets the referencias_bancarias value for this Avalista.
     * 
     * @param referencias_bancarias
     */
    public void setReferencias_bancarias(com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] referencias_bancarias) {
        this.referencias_bancarias = referencias_bancarias;
    }


    /**
     * Gets the telefone value for this Avalista.
     * 
     * @return telefone
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Avalista.
     * 
     * @param telefone
     */
    public void setTelefone(com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.telefone = telefone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Avalista)) return false;
        Avalista other = (Avalista) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.bens_imoveis==null && other.getBens_imoveis()==null) || 
             (this.bens_imoveis!=null &&
              java.util.Arrays.equals(this.bens_imoveis, other.getBens_imoveis()))) &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.id_cliente==null && other.getId_cliente()==null) || 
             (this.id_cliente!=null &&
              this.id_cliente.equals(other.getId_cliente()))) &&
            ((this.nm_cliente_rsoc==null && other.getNm_cliente_rsoc()==null) || 
             (this.nm_cliente_rsoc!=null &&
              this.nm_cliente_rsoc.equals(other.getNm_cliente_rsoc()))) &&
            ((this.referencias_bancarias==null && other.getReferencias_bancarias()==null) || 
             (this.referencias_bancarias!=null &&
              java.util.Arrays.equals(this.referencias_bancarias, other.getReferencias_bancarias()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBens_imoveis() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBens_imoveis());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBens_imoveis(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getId_cliente() != null) {
            _hashCode += getId_cliente().hashCode();
        }
        if (getNm_cliente_rsoc() != null) {
            _hashCode += getNm_cliente_rsoc().hashCode();
        }
        if (getReferencias_bancarias() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferencias_bancarias());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferencias_bancarias(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Avalista.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Avalista"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bens_imoveis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "bens_imoveis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_cliente_rsoc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_cliente_rsoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencias_bancarias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "referencias_bancarias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
