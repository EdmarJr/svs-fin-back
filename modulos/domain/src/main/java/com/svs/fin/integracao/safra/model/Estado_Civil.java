/**
 * Estado_Civil.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Estado_Civil")
public class Estado_Civil implements java.io.Serializable {
	private java.lang.String ds_estado_civil;

	private java.lang.String fl_casado;

	@Id
	private java.lang.Integer id_estado_civil;

	public Estado_Civil() {
	}

	public Estado_Civil(java.lang.String ds_estado_civil, java.lang.String fl_casado,
			java.lang.Integer id_estado_civil) {
		this.ds_estado_civil = ds_estado_civil;
		this.fl_casado = fl_casado;
		this.id_estado_civil = id_estado_civil;
	}

	/**
	 * Gets the ds_estado_civil value for this Estado_Civil.
	 * 
	 * @return ds_estado_civil
	 */
	public java.lang.String getDs_estado_civil() {
		return ds_estado_civil;
	}

	/**
	 * Sets the ds_estado_civil value for this Estado_Civil.
	 * 
	 * @param ds_estado_civil
	 */
	public void setDs_estado_civil(java.lang.String ds_estado_civil) {
		this.ds_estado_civil = ds_estado_civil;
	}

	/**
	 * Gets the fl_casado value for this Estado_Civil.
	 * 
	 * @return fl_casado
	 */
	public java.lang.String getFl_casado() {
		return fl_casado;
	}

	/**
	 * Sets the fl_casado value for this Estado_Civil.
	 * 
	 * @param fl_casado
	 */
	public void setFl_casado(java.lang.String fl_casado) {
		this.fl_casado = fl_casado;
	}

	/**
	 * Gets the id_estado_civil value for this Estado_Civil.
	 * 
	 * @return id_estado_civil
	 */
	public java.lang.Integer getId_estado_civil() {
		return id_estado_civil;
	}

	/**
	 * Sets the id_estado_civil value for this Estado_Civil.
	 * 
	 * @param id_estado_civil
	 */
	public void setId_estado_civil(java.lang.Integer id_estado_civil) {
		this.id_estado_civil = id_estado_civil;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Estado_Civil))
			return false;
		Estado_Civil other = (Estado_Civil) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_estado_civil == null && other.getDs_estado_civil() == null)
						|| (this.ds_estado_civil != null && this.ds_estado_civil.equals(other.getDs_estado_civil())))
				&& ((this.fl_casado == null && other.getFl_casado() == null)
						|| (this.fl_casado != null && this.fl_casado.equals(other.getFl_casado())))
				&& ((this.id_estado_civil == null && other.getId_estado_civil() == null)
						|| (this.id_estado_civil != null && this.id_estado_civil.equals(other.getId_estado_civil())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_estado_civil() != null) {
			_hashCode += getDs_estado_civil().hashCode();
		}
		if (getFl_casado() != null) {
			_hashCode += getFl_casado().hashCode();
		}
		if (getId_estado_civil() != null) {
			_hashCode += getId_estado_civil().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Estado_Civil.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Estado_Civil"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_estado_civil");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_estado_civil"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("fl_casado");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fl_casado"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_estado_civil");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_estado_civil"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
