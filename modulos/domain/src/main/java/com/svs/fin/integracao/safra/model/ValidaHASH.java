/**
 * ValidaHASH.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class ValidaHASH  implements java.io.Serializable {
    private java.lang.Integer idRevenda;

    private java.lang.String dsHash;

    public ValidaHASH() {
    }

    public ValidaHASH(
           java.lang.Integer idRevenda,
           java.lang.String dsHash) {
           this.idRevenda = idRevenda;
           this.dsHash = dsHash;
    }


    /**
     * Gets the idRevenda value for this ValidaHASH.
     * 
     * @return idRevenda
     */
    public java.lang.Integer getIdRevenda() {
        return idRevenda;
    }


    /**
     * Sets the idRevenda value for this ValidaHASH.
     * 
     * @param idRevenda
     */
    public void setIdRevenda(java.lang.Integer idRevenda) {
        this.idRevenda = idRevenda;
    }


    /**
     * Gets the dsHash value for this ValidaHASH.
     * 
     * @return dsHash
     */
    public java.lang.String getDsHash() {
        return dsHash;
    }


    /**
     * Sets the dsHash value for this ValidaHASH.
     * 
     * @param dsHash
     */
    public void setDsHash(java.lang.String dsHash) {
        this.dsHash = dsHash;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidaHASH)) return false;
        ValidaHASH other = (ValidaHASH) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idRevenda==null && other.getIdRevenda()==null) || 
             (this.idRevenda!=null &&
              this.idRevenda.equals(other.getIdRevenda()))) &&
            ((this.dsHash==null && other.getDsHash()==null) || 
             (this.dsHash!=null &&
              this.dsHash.equals(other.getDsHash())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdRevenda() != null) {
            _hashCode += getIdRevenda().hashCode();
        }
        if (getDsHash() != null) {
            _hashCode += getDsHash().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidaHASH.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idRevenda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "IdRevenda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsHash");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "dsHash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
