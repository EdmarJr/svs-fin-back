/**
 * Coeficiente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Coeficiente  implements java.io.Serializable {
    private java.lang.String dsTipoPessoa;

    private java.lang.Integer idTipoRetorno;

    private java.lang.Integer idTipoSeguro;

    private java.math.BigDecimal pcRetorno;

    private java.math.BigDecimal vlCoeficiente;

    public Coeficiente() {
    }

    public Coeficiente(
           java.lang.String dsTipoPessoa,
           java.lang.Integer idTipoRetorno,
           java.lang.Integer idTipoSeguro,
           java.math.BigDecimal pcRetorno,
           java.math.BigDecimal vlCoeficiente) {
           this.dsTipoPessoa = dsTipoPessoa;
           this.idTipoRetorno = idTipoRetorno;
           this.idTipoSeguro = idTipoSeguro;
           this.pcRetorno = pcRetorno;
           this.vlCoeficiente = vlCoeficiente;
    }


    /**
     * Gets the dsTipoPessoa value for this Coeficiente.
     * 
     * @return dsTipoPessoa
     */
    public java.lang.String getDsTipoPessoa() {
        return dsTipoPessoa;
    }


    /**
     * Sets the dsTipoPessoa value for this Coeficiente.
     * 
     * @param dsTipoPessoa
     */
    public void setDsTipoPessoa(java.lang.String dsTipoPessoa) {
        this.dsTipoPessoa = dsTipoPessoa;
    }


    /**
     * Gets the idTipoRetorno value for this Coeficiente.
     * 
     * @return idTipoRetorno
     */
    public java.lang.Integer getIdTipoRetorno() {
        return idTipoRetorno;
    }


    /**
     * Sets the idTipoRetorno value for this Coeficiente.
     * 
     * @param idTipoRetorno
     */
    public void setIdTipoRetorno(java.lang.Integer idTipoRetorno) {
        this.idTipoRetorno = idTipoRetorno;
    }


    /**
     * Gets the idTipoSeguro value for this Coeficiente.
     * 
     * @return idTipoSeguro
     */
    public java.lang.Integer getIdTipoSeguro() {
        return idTipoSeguro;
    }


    /**
     * Sets the idTipoSeguro value for this Coeficiente.
     * 
     * @param idTipoSeguro
     */
    public void setIdTipoSeguro(java.lang.Integer idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }


    /**
     * Gets the pcRetorno value for this Coeficiente.
     * 
     * @return pcRetorno
     */
    public java.math.BigDecimal getPcRetorno() {
        return pcRetorno;
    }


    /**
     * Sets the pcRetorno value for this Coeficiente.
     * 
     * @param pcRetorno
     */
    public void setPcRetorno(java.math.BigDecimal pcRetorno) {
        this.pcRetorno = pcRetorno;
    }


    /**
     * Gets the vlCoeficiente value for this Coeficiente.
     * 
     * @return vlCoeficiente
     */
    public java.math.BigDecimal getVlCoeficiente() {
        return vlCoeficiente;
    }


    /**
     * Sets the vlCoeficiente value for this Coeficiente.
     * 
     * @param vlCoeficiente
     */
    public void setVlCoeficiente(java.math.BigDecimal vlCoeficiente) {
        this.vlCoeficiente = vlCoeficiente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Coeficiente)) return false;
        Coeficiente other = (Coeficiente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dsTipoPessoa==null && other.getDsTipoPessoa()==null) || 
             (this.dsTipoPessoa!=null &&
              this.dsTipoPessoa.equals(other.getDsTipoPessoa()))) &&
            ((this.idTipoRetorno==null && other.getIdTipoRetorno()==null) || 
             (this.idTipoRetorno!=null &&
              this.idTipoRetorno.equals(other.getIdTipoRetorno()))) &&
            ((this.idTipoSeguro==null && other.getIdTipoSeguro()==null) || 
             (this.idTipoSeguro!=null &&
              this.idTipoSeguro.equals(other.getIdTipoSeguro()))) &&
            ((this.pcRetorno==null && other.getPcRetorno()==null) || 
             (this.pcRetorno!=null &&
              this.pcRetorno.equals(other.getPcRetorno()))) &&
            ((this.vlCoeficiente==null && other.getVlCoeficiente()==null) || 
             (this.vlCoeficiente!=null &&
              this.vlCoeficiente.equals(other.getVlCoeficiente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDsTipoPessoa() != null) {
            _hashCode += getDsTipoPessoa().hashCode();
        }
        if (getIdTipoRetorno() != null) {
            _hashCode += getIdTipoRetorno().hashCode();
        }
        if (getIdTipoSeguro() != null) {
            _hashCode += getIdTipoSeguro().hashCode();
        }
        if (getPcRetorno() != null) {
            _hashCode += getPcRetorno().hashCode();
        }
        if (getVlCoeficiente() != null) {
            _hashCode += getVlCoeficiente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Coeficiente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficiente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsTipoPessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsTipoPessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "IdTipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTipoSeguro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "IdTipoSeguro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pcRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "PcRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlCoeficiente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "VlCoeficiente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
