/**
 * CalculoParcelas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class CalculoParcelas  extends com.svs.fin.integracao.safra.model.RetornoProposta  implements java.io.Serializable {
    private java.lang.String ds_hash_id_simulacao;

    private java.lang.String id_cliente;

    private java.lang.Integer id_emp_fei;

    private java.lang.Double pc_entrada_minima;

    private java.lang.Double pc_ret;

    private java.lang.Double pc_spread;

    private java.lang.Double pc_spread_trava;

    private java.lang.Integer v_carencia;

    private java.lang.Double vl_assegurado;

    private java.lang.Double vl_coeficiente;

    private java.lang.Double vl_gravame;

    private java.lang.Double vl_gravame_registro;

    private java.lang.Double vl_iof_ato;

    private java.lang.Double vl_iof_ato_adic;

    private java.lang.Double vl_iof_financ;

    private java.lang.Double vl_iof_financ_adic;

    private java.lang.Double vl_minimo;

    private java.lang.Double vl_pmt;

    private java.lang.Double vl_principal_soma;

    private java.lang.Double vl_reg_ncobrado;

    private java.lang.Double vl_registro;

    private java.lang.Double vl_ret;

    private java.lang.Double vl_seguro;

    private java.lang.Double vl_taxa;

    private java.lang.Double vl_taxa_iof;

    private java.lang.Double vl_tc_ncobrado;

    private java.lang.Double vl_tx_cet;

    private java.lang.Double vl_tx_cet_ano;

    private java.lang.Double vl_tx_net;

    public CalculoParcelas() {
    }

    public CalculoParcelas(
           java.lang.String id_proposta,
           java.lang.String id_retorno,
           java.lang.String ds_hash_id_simulacao,
           java.lang.String id_cliente,
           java.lang.Integer id_emp_fei,
           java.lang.Double pc_entrada_minima,
           java.lang.Double pc_ret,
           java.lang.Double pc_spread,
           java.lang.Double pc_spread_trava,
           java.lang.Integer v_carencia,
           java.lang.Double vl_assegurado,
           java.lang.Double vl_coeficiente,
           java.lang.Double vl_gravame,
           java.lang.Double vl_gravame_registro,
           java.lang.Double vl_iof_ato,
           java.lang.Double vl_iof_ato_adic,
           java.lang.Double vl_iof_financ,
           java.lang.Double vl_iof_financ_adic,
           java.lang.Double vl_minimo,
           java.lang.Double vl_pmt,
           java.lang.Double vl_principal_soma,
           java.lang.Double vl_reg_ncobrado,
           java.lang.Double vl_registro,
           java.lang.Double vl_ret,
           java.lang.Double vl_seguro,
           java.lang.Double vl_taxa,
           java.lang.Double vl_taxa_iof,
           java.lang.Double vl_tc_ncobrado,
           java.lang.Double vl_tx_cet,
           java.lang.Double vl_tx_cet_ano,
           java.lang.Double vl_tx_net) {
        super(
            id_proposta,
            id_retorno);
        this.ds_hash_id_simulacao = ds_hash_id_simulacao;
        this.id_cliente = id_cliente;
        this.id_emp_fei = id_emp_fei;
        this.pc_entrada_minima = pc_entrada_minima;
        this.pc_ret = pc_ret;
        this.pc_spread = pc_spread;
        this.pc_spread_trava = pc_spread_trava;
        this.v_carencia = v_carencia;
        this.vl_assegurado = vl_assegurado;
        this.vl_coeficiente = vl_coeficiente;
        this.vl_gravame = vl_gravame;
        this.vl_gravame_registro = vl_gravame_registro;
        this.vl_iof_ato = vl_iof_ato;
        this.vl_iof_ato_adic = vl_iof_ato_adic;
        this.vl_iof_financ = vl_iof_financ;
        this.vl_iof_financ_adic = vl_iof_financ_adic;
        this.vl_minimo = vl_minimo;
        this.vl_pmt = vl_pmt;
        this.vl_principal_soma = vl_principal_soma;
        this.vl_reg_ncobrado = vl_reg_ncobrado;
        this.vl_registro = vl_registro;
        this.vl_ret = vl_ret;
        this.vl_seguro = vl_seguro;
        this.vl_taxa = vl_taxa;
        this.vl_taxa_iof = vl_taxa_iof;
        this.vl_tc_ncobrado = vl_tc_ncobrado;
        this.vl_tx_cet = vl_tx_cet;
        this.vl_tx_cet_ano = vl_tx_cet_ano;
        this.vl_tx_net = vl_tx_net;
    }


    /**
     * Gets the ds_hash_id_simulacao value for this CalculoParcelas.
     * 
     * @return ds_hash_id_simulacao
     */
    public java.lang.String getDs_hash_id_simulacao() {
        return ds_hash_id_simulacao;
    }


    /**
     * Sets the ds_hash_id_simulacao value for this CalculoParcelas.
     * 
     * @param ds_hash_id_simulacao
     */
    public void setDs_hash_id_simulacao(java.lang.String ds_hash_id_simulacao) {
        this.ds_hash_id_simulacao = ds_hash_id_simulacao;
    }


    /**
     * Gets the id_cliente value for this CalculoParcelas.
     * 
     * @return id_cliente
     */
    public java.lang.String getId_cliente() {
        return id_cliente;
    }


    /**
     * Sets the id_cliente value for this CalculoParcelas.
     * 
     * @param id_cliente
     */
    public void setId_cliente(java.lang.String id_cliente) {
        this.id_cliente = id_cliente;
    }


    /**
     * Gets the id_emp_fei value for this CalculoParcelas.
     * 
     * @return id_emp_fei
     */
    public java.lang.Integer getId_emp_fei() {
        return id_emp_fei;
    }


    /**
     * Sets the id_emp_fei value for this CalculoParcelas.
     * 
     * @param id_emp_fei
     */
    public void setId_emp_fei(java.lang.Integer id_emp_fei) {
        this.id_emp_fei = id_emp_fei;
    }


    /**
     * Gets the pc_entrada_minima value for this CalculoParcelas.
     * 
     * @return pc_entrada_minima
     */
    public java.lang.Double getPc_entrada_minima() {
        return pc_entrada_minima;
    }


    /**
     * Sets the pc_entrada_minima value for this CalculoParcelas.
     * 
     * @param pc_entrada_minima
     */
    public void setPc_entrada_minima(java.lang.Double pc_entrada_minima) {
        this.pc_entrada_minima = pc_entrada_minima;
    }


    /**
     * Gets the pc_ret value for this CalculoParcelas.
     * 
     * @return pc_ret
     */
    public java.lang.Double getPc_ret() {
        return pc_ret;
    }


    /**
     * Sets the pc_ret value for this CalculoParcelas.
     * 
     * @param pc_ret
     */
    public void setPc_ret(java.lang.Double pc_ret) {
        this.pc_ret = pc_ret;
    }


    /**
     * Gets the pc_spread value for this CalculoParcelas.
     * 
     * @return pc_spread
     */
    public java.lang.Double getPc_spread() {
        return pc_spread;
    }


    /**
     * Sets the pc_spread value for this CalculoParcelas.
     * 
     * @param pc_spread
     */
    public void setPc_spread(java.lang.Double pc_spread) {
        this.pc_spread = pc_spread;
    }


    /**
     * Gets the pc_spread_trava value for this CalculoParcelas.
     * 
     * @return pc_spread_trava
     */
    public java.lang.Double getPc_spread_trava() {
        return pc_spread_trava;
    }


    /**
     * Sets the pc_spread_trava value for this CalculoParcelas.
     * 
     * @param pc_spread_trava
     */
    public void setPc_spread_trava(java.lang.Double pc_spread_trava) {
        this.pc_spread_trava = pc_spread_trava;
    }


    /**
     * Gets the v_carencia value for this CalculoParcelas.
     * 
     * @return v_carencia
     */
    public java.lang.Integer getV_carencia() {
        return v_carencia;
    }


    /**
     * Sets the v_carencia value for this CalculoParcelas.
     * 
     * @param v_carencia
     */
    public void setV_carencia(java.lang.Integer v_carencia) {
        this.v_carencia = v_carencia;
    }


    /**
     * Gets the vl_assegurado value for this CalculoParcelas.
     * 
     * @return vl_assegurado
     */
    public java.lang.Double getVl_assegurado() {
        return vl_assegurado;
    }


    /**
     * Sets the vl_assegurado value for this CalculoParcelas.
     * 
     * @param vl_assegurado
     */
    public void setVl_assegurado(java.lang.Double vl_assegurado) {
        this.vl_assegurado = vl_assegurado;
    }


    /**
     * Gets the vl_coeficiente value for this CalculoParcelas.
     * 
     * @return vl_coeficiente
     */
    public java.lang.Double getVl_coeficiente() {
        return vl_coeficiente;
    }


    /**
     * Sets the vl_coeficiente value for this CalculoParcelas.
     * 
     * @param vl_coeficiente
     */
    public void setVl_coeficiente(java.lang.Double vl_coeficiente) {
        this.vl_coeficiente = vl_coeficiente;
    }


    /**
     * Gets the vl_gravame value for this CalculoParcelas.
     * 
     * @return vl_gravame
     */
    public java.lang.Double getVl_gravame() {
        return vl_gravame;
    }


    /**
     * Sets the vl_gravame value for this CalculoParcelas.
     * 
     * @param vl_gravame
     */
    public void setVl_gravame(java.lang.Double vl_gravame) {
        this.vl_gravame = vl_gravame;
    }


    /**
     * Gets the vl_gravame_registro value for this CalculoParcelas.
     * 
     * @return vl_gravame_registro
     */
    public java.lang.Double getVl_gravame_registro() {
        return vl_gravame_registro;
    }


    /**
     * Sets the vl_gravame_registro value for this CalculoParcelas.
     * 
     * @param vl_gravame_registro
     */
    public void setVl_gravame_registro(java.lang.Double vl_gravame_registro) {
        this.vl_gravame_registro = vl_gravame_registro;
    }


    /**
     * Gets the vl_iof_ato value for this CalculoParcelas.
     * 
     * @return vl_iof_ato
     */
    public java.lang.Double getVl_iof_ato() {
        return vl_iof_ato;
    }


    /**
     * Sets the vl_iof_ato value for this CalculoParcelas.
     * 
     * @param vl_iof_ato
     */
    public void setVl_iof_ato(java.lang.Double vl_iof_ato) {
        this.vl_iof_ato = vl_iof_ato;
    }


    /**
     * Gets the vl_iof_ato_adic value for this CalculoParcelas.
     * 
     * @return vl_iof_ato_adic
     */
    public java.lang.Double getVl_iof_ato_adic() {
        return vl_iof_ato_adic;
    }


    /**
     * Sets the vl_iof_ato_adic value for this CalculoParcelas.
     * 
     * @param vl_iof_ato_adic
     */
    public void setVl_iof_ato_adic(java.lang.Double vl_iof_ato_adic) {
        this.vl_iof_ato_adic = vl_iof_ato_adic;
    }


    /**
     * Gets the vl_iof_financ value for this CalculoParcelas.
     * 
     * @return vl_iof_financ
     */
    public java.lang.Double getVl_iof_financ() {
        return vl_iof_financ;
    }


    /**
     * Sets the vl_iof_financ value for this CalculoParcelas.
     * 
     * @param vl_iof_financ
     */
    public void setVl_iof_financ(java.lang.Double vl_iof_financ) {
        this.vl_iof_financ = vl_iof_financ;
    }


    /**
     * Gets the vl_iof_financ_adic value for this CalculoParcelas.
     * 
     * @return vl_iof_financ_adic
     */
    public java.lang.Double getVl_iof_financ_adic() {
        return vl_iof_financ_adic;
    }


    /**
     * Sets the vl_iof_financ_adic value for this CalculoParcelas.
     * 
     * @param vl_iof_financ_adic
     */
    public void setVl_iof_financ_adic(java.lang.Double vl_iof_financ_adic) {
        this.vl_iof_financ_adic = vl_iof_financ_adic;
    }


    /**
     * Gets the vl_minimo value for this CalculoParcelas.
     * 
     * @return vl_minimo
     */
    public java.lang.Double getVl_minimo() {
        return vl_minimo;
    }


    /**
     * Sets the vl_minimo value for this CalculoParcelas.
     * 
     * @param vl_minimo
     */
    public void setVl_minimo(java.lang.Double vl_minimo) {
        this.vl_minimo = vl_minimo;
    }


    /**
     * Gets the vl_pmt value for this CalculoParcelas.
     * 
     * @return vl_pmt
     */
    public java.lang.Double getVl_pmt() {
        return vl_pmt;
    }


    /**
     * Sets the vl_pmt value for this CalculoParcelas.
     * 
     * @param vl_pmt
     */
    public void setVl_pmt(java.lang.Double vl_pmt) {
        this.vl_pmt = vl_pmt;
    }


    /**
     * Gets the vl_principal_soma value for this CalculoParcelas.
     * 
     * @return vl_principal_soma
     */
    public java.lang.Double getVl_principal_soma() {
        return vl_principal_soma;
    }


    /**
     * Sets the vl_principal_soma value for this CalculoParcelas.
     * 
     * @param vl_principal_soma
     */
    public void setVl_principal_soma(java.lang.Double vl_principal_soma) {
        this.vl_principal_soma = vl_principal_soma;
    }


    /**
     * Gets the vl_reg_ncobrado value for this CalculoParcelas.
     * 
     * @return vl_reg_ncobrado
     */
    public java.lang.Double getVl_reg_ncobrado() {
        return vl_reg_ncobrado;
    }


    /**
     * Sets the vl_reg_ncobrado value for this CalculoParcelas.
     * 
     * @param vl_reg_ncobrado
     */
    public void setVl_reg_ncobrado(java.lang.Double vl_reg_ncobrado) {
        this.vl_reg_ncobrado = vl_reg_ncobrado;
    }


    /**
     * Gets the vl_registro value for this CalculoParcelas.
     * 
     * @return vl_registro
     */
    public java.lang.Double getVl_registro() {
        return vl_registro;
    }


    /**
     * Sets the vl_registro value for this CalculoParcelas.
     * 
     * @param vl_registro
     */
    public void setVl_registro(java.lang.Double vl_registro) {
        this.vl_registro = vl_registro;
    }


    /**
     * Gets the vl_ret value for this CalculoParcelas.
     * 
     * @return vl_ret
     */
    public java.lang.Double getVl_ret() {
        return vl_ret;
    }


    /**
     * Sets the vl_ret value for this CalculoParcelas.
     * 
     * @param vl_ret
     */
    public void setVl_ret(java.lang.Double vl_ret) {
        this.vl_ret = vl_ret;
    }


    /**
     * Gets the vl_seguro value for this CalculoParcelas.
     * 
     * @return vl_seguro
     */
    public java.lang.Double getVl_seguro() {
        return vl_seguro;
    }


    /**
     * Sets the vl_seguro value for this CalculoParcelas.
     * 
     * @param vl_seguro
     */
    public void setVl_seguro(java.lang.Double vl_seguro) {
        this.vl_seguro = vl_seguro;
    }


    /**
     * Gets the vl_taxa value for this CalculoParcelas.
     * 
     * @return vl_taxa
     */
    public java.lang.Double getVl_taxa() {
        return vl_taxa;
    }


    /**
     * Sets the vl_taxa value for this CalculoParcelas.
     * 
     * @param vl_taxa
     */
    public void setVl_taxa(java.lang.Double vl_taxa) {
        this.vl_taxa = vl_taxa;
    }


    /**
     * Gets the vl_taxa_iof value for this CalculoParcelas.
     * 
     * @return vl_taxa_iof
     */
    public java.lang.Double getVl_taxa_iof() {
        return vl_taxa_iof;
    }


    /**
     * Sets the vl_taxa_iof value for this CalculoParcelas.
     * 
     * @param vl_taxa_iof
     */
    public void setVl_taxa_iof(java.lang.Double vl_taxa_iof) {
        this.vl_taxa_iof = vl_taxa_iof;
    }


    /**
     * Gets the vl_tc_ncobrado value for this CalculoParcelas.
     * 
     * @return vl_tc_ncobrado
     */
    public java.lang.Double getVl_tc_ncobrado() {
        return vl_tc_ncobrado;
    }


    /**
     * Sets the vl_tc_ncobrado value for this CalculoParcelas.
     * 
     * @param vl_tc_ncobrado
     */
    public void setVl_tc_ncobrado(java.lang.Double vl_tc_ncobrado) {
        this.vl_tc_ncobrado = vl_tc_ncobrado;
    }


    /**
     * Gets the vl_tx_cet value for this CalculoParcelas.
     * 
     * @return vl_tx_cet
     */
    public java.lang.Double getVl_tx_cet() {
        return vl_tx_cet;
    }


    /**
     * Sets the vl_tx_cet value for this CalculoParcelas.
     * 
     * @param vl_tx_cet
     */
    public void setVl_tx_cet(java.lang.Double vl_tx_cet) {
        this.vl_tx_cet = vl_tx_cet;
    }


    /**
     * Gets the vl_tx_cet_ano value for this CalculoParcelas.
     * 
     * @return vl_tx_cet_ano
     */
    public java.lang.Double getVl_tx_cet_ano() {
        return vl_tx_cet_ano;
    }


    /**
     * Sets the vl_tx_cet_ano value for this CalculoParcelas.
     * 
     * @param vl_tx_cet_ano
     */
    public void setVl_tx_cet_ano(java.lang.Double vl_tx_cet_ano) {
        this.vl_tx_cet_ano = vl_tx_cet_ano;
    }


    /**
     * Gets the vl_tx_net value for this CalculoParcelas.
     * 
     * @return vl_tx_net
     */
    public java.lang.Double getVl_tx_net() {
        return vl_tx_net;
    }


    /**
     * Sets the vl_tx_net value for this CalculoParcelas.
     * 
     * @param vl_tx_net
     */
    public void setVl_tx_net(java.lang.Double vl_tx_net) {
        this.vl_tx_net = vl_tx_net;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoParcelas)) return false;
        CalculoParcelas other = (CalculoParcelas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_hash_id_simulacao==null && other.getDs_hash_id_simulacao()==null) || 
             (this.ds_hash_id_simulacao!=null &&
              this.ds_hash_id_simulacao.equals(other.getDs_hash_id_simulacao()))) &&
            ((this.id_cliente==null && other.getId_cliente()==null) || 
             (this.id_cliente!=null &&
              this.id_cliente.equals(other.getId_cliente()))) &&
            ((this.id_emp_fei==null && other.getId_emp_fei()==null) || 
             (this.id_emp_fei!=null &&
              this.id_emp_fei.equals(other.getId_emp_fei()))) &&
            ((this.pc_entrada_minima==null && other.getPc_entrada_minima()==null) || 
             (this.pc_entrada_minima!=null &&
              this.pc_entrada_minima.equals(other.getPc_entrada_minima()))) &&
            ((this.pc_ret==null && other.getPc_ret()==null) || 
             (this.pc_ret!=null &&
              this.pc_ret.equals(other.getPc_ret()))) &&
            ((this.pc_spread==null && other.getPc_spread()==null) || 
             (this.pc_spread!=null &&
              this.pc_spread.equals(other.getPc_spread()))) &&
            ((this.pc_spread_trava==null && other.getPc_spread_trava()==null) || 
             (this.pc_spread_trava!=null &&
              this.pc_spread_trava.equals(other.getPc_spread_trava()))) &&
            ((this.v_carencia==null && other.getV_carencia()==null) || 
             (this.v_carencia!=null &&
              this.v_carencia.equals(other.getV_carencia()))) &&
            ((this.vl_assegurado==null && other.getVl_assegurado()==null) || 
             (this.vl_assegurado!=null &&
              this.vl_assegurado.equals(other.getVl_assegurado()))) &&
            ((this.vl_coeficiente==null && other.getVl_coeficiente()==null) || 
             (this.vl_coeficiente!=null &&
              this.vl_coeficiente.equals(other.getVl_coeficiente()))) &&
            ((this.vl_gravame==null && other.getVl_gravame()==null) || 
             (this.vl_gravame!=null &&
              this.vl_gravame.equals(other.getVl_gravame()))) &&
            ((this.vl_gravame_registro==null && other.getVl_gravame_registro()==null) || 
             (this.vl_gravame_registro!=null &&
              this.vl_gravame_registro.equals(other.getVl_gravame_registro()))) &&
            ((this.vl_iof_ato==null && other.getVl_iof_ato()==null) || 
             (this.vl_iof_ato!=null &&
              this.vl_iof_ato.equals(other.getVl_iof_ato()))) &&
            ((this.vl_iof_ato_adic==null && other.getVl_iof_ato_adic()==null) || 
             (this.vl_iof_ato_adic!=null &&
              this.vl_iof_ato_adic.equals(other.getVl_iof_ato_adic()))) &&
            ((this.vl_iof_financ==null && other.getVl_iof_financ()==null) || 
             (this.vl_iof_financ!=null &&
              this.vl_iof_financ.equals(other.getVl_iof_financ()))) &&
            ((this.vl_iof_financ_adic==null && other.getVl_iof_financ_adic()==null) || 
             (this.vl_iof_financ_adic!=null &&
              this.vl_iof_financ_adic.equals(other.getVl_iof_financ_adic()))) &&
            ((this.vl_minimo==null && other.getVl_minimo()==null) || 
             (this.vl_minimo!=null &&
              this.vl_minimo.equals(other.getVl_minimo()))) &&
            ((this.vl_pmt==null && other.getVl_pmt()==null) || 
             (this.vl_pmt!=null &&
              this.vl_pmt.equals(other.getVl_pmt()))) &&
            ((this.vl_principal_soma==null && other.getVl_principal_soma()==null) || 
             (this.vl_principal_soma!=null &&
              this.vl_principal_soma.equals(other.getVl_principal_soma()))) &&
            ((this.vl_reg_ncobrado==null && other.getVl_reg_ncobrado()==null) || 
             (this.vl_reg_ncobrado!=null &&
              this.vl_reg_ncobrado.equals(other.getVl_reg_ncobrado()))) &&
            ((this.vl_registro==null && other.getVl_registro()==null) || 
             (this.vl_registro!=null &&
              this.vl_registro.equals(other.getVl_registro()))) &&
            ((this.vl_ret==null && other.getVl_ret()==null) || 
             (this.vl_ret!=null &&
              this.vl_ret.equals(other.getVl_ret()))) &&
            ((this.vl_seguro==null && other.getVl_seguro()==null) || 
             (this.vl_seguro!=null &&
              this.vl_seguro.equals(other.getVl_seguro()))) &&
            ((this.vl_taxa==null && other.getVl_taxa()==null) || 
             (this.vl_taxa!=null &&
              this.vl_taxa.equals(other.getVl_taxa()))) &&
            ((this.vl_taxa_iof==null && other.getVl_taxa_iof()==null) || 
             (this.vl_taxa_iof!=null &&
              this.vl_taxa_iof.equals(other.getVl_taxa_iof()))) &&
            ((this.vl_tc_ncobrado==null && other.getVl_tc_ncobrado()==null) || 
             (this.vl_tc_ncobrado!=null &&
              this.vl_tc_ncobrado.equals(other.getVl_tc_ncobrado()))) &&
            ((this.vl_tx_cet==null && other.getVl_tx_cet()==null) || 
             (this.vl_tx_cet!=null &&
              this.vl_tx_cet.equals(other.getVl_tx_cet()))) &&
            ((this.vl_tx_cet_ano==null && other.getVl_tx_cet_ano()==null) || 
             (this.vl_tx_cet_ano!=null &&
              this.vl_tx_cet_ano.equals(other.getVl_tx_cet_ano()))) &&
            ((this.vl_tx_net==null && other.getVl_tx_net()==null) || 
             (this.vl_tx_net!=null &&
              this.vl_tx_net.equals(other.getVl_tx_net())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_hash_id_simulacao() != null) {
            _hashCode += getDs_hash_id_simulacao().hashCode();
        }
        if (getId_cliente() != null) {
            _hashCode += getId_cliente().hashCode();
        }
        if (getId_emp_fei() != null) {
            _hashCode += getId_emp_fei().hashCode();
        }
        if (getPc_entrada_minima() != null) {
            _hashCode += getPc_entrada_minima().hashCode();
        }
        if (getPc_ret() != null) {
            _hashCode += getPc_ret().hashCode();
        }
        if (getPc_spread() != null) {
            _hashCode += getPc_spread().hashCode();
        }
        if (getPc_spread_trava() != null) {
            _hashCode += getPc_spread_trava().hashCode();
        }
        if (getV_carencia() != null) {
            _hashCode += getV_carencia().hashCode();
        }
        if (getVl_assegurado() != null) {
            _hashCode += getVl_assegurado().hashCode();
        }
        if (getVl_coeficiente() != null) {
            _hashCode += getVl_coeficiente().hashCode();
        }
        if (getVl_gravame() != null) {
            _hashCode += getVl_gravame().hashCode();
        }
        if (getVl_gravame_registro() != null) {
            _hashCode += getVl_gravame_registro().hashCode();
        }
        if (getVl_iof_ato() != null) {
            _hashCode += getVl_iof_ato().hashCode();
        }
        if (getVl_iof_ato_adic() != null) {
            _hashCode += getVl_iof_ato_adic().hashCode();
        }
        if (getVl_iof_financ() != null) {
            _hashCode += getVl_iof_financ().hashCode();
        }
        if (getVl_iof_financ_adic() != null) {
            _hashCode += getVl_iof_financ_adic().hashCode();
        }
        if (getVl_minimo() != null) {
            _hashCode += getVl_minimo().hashCode();
        }
        if (getVl_pmt() != null) {
            _hashCode += getVl_pmt().hashCode();
        }
        if (getVl_principal_soma() != null) {
            _hashCode += getVl_principal_soma().hashCode();
        }
        if (getVl_reg_ncobrado() != null) {
            _hashCode += getVl_reg_ncobrado().hashCode();
        }
        if (getVl_registro() != null) {
            _hashCode += getVl_registro().hashCode();
        }
        if (getVl_ret() != null) {
            _hashCode += getVl_ret().hashCode();
        }
        if (getVl_seguro() != null) {
            _hashCode += getVl_seguro().hashCode();
        }
        if (getVl_taxa() != null) {
            _hashCode += getVl_taxa().hashCode();
        }
        if (getVl_taxa_iof() != null) {
            _hashCode += getVl_taxa_iof().hashCode();
        }
        if (getVl_tc_ncobrado() != null) {
            _hashCode += getVl_tc_ncobrado().hashCode();
        }
        if (getVl_tx_cet() != null) {
            _hashCode += getVl_tx_cet().hashCode();
        }
        if (getVl_tx_cet_ano() != null) {
            _hashCode += getVl_tx_cet_ano().hashCode();
        }
        if (getVl_tx_net() != null) {
            _hashCode += getVl_tx_net().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoParcelas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "CalculoParcelas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_hash_id_simulacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_hash_id_simulacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_emp_fei");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_emp_fei"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_entrada_minima");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "pc_entrada_minima"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_ret");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "pc_ret"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_spread");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "pc_spread"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_spread_trava");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "pc_spread_trava"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("v_carencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "v_carencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_assegurado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_assegurado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_coeficiente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_coeficiente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_gravame");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_gravame"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_gravame_registro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_gravame_registro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_iof_ato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_iof_ato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_iof_ato_adic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_iof_ato_adic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_iof_financ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_iof_financ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_iof_financ_adic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_iof_financ_adic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_minimo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_minimo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_pmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_pmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_principal_soma");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_principal_soma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_reg_ncobrado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_reg_ncobrado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_registro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_registro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_ret");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_ret"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_seguro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_seguro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_taxa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_taxa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_taxa_iof");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_taxa_iof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_tc_ncobrado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tc_ncobrado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_tx_cet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tx_cet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_tx_cet_ano");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tx_cet_ano"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_tx_net");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_tx_net"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
