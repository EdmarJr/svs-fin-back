/**
 * Faixas_Patrimonio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "safra_Faixas_Patrimonio")
public class Faixas_Patrimonio implements java.io.Serializable {
	private java.lang.String ds_faixa_patrimonio;

	@Id
	private java.lang.String id_faixa_patrimonio;

	public Faixas_Patrimonio() {
	}

	public Faixas_Patrimonio(java.lang.String ds_faixa_patrimonio, java.lang.String id_faixa_patrimonio) {
		this.ds_faixa_patrimonio = ds_faixa_patrimonio;
		this.id_faixa_patrimonio = id_faixa_patrimonio;
	}

	/**
	 * Gets the ds_faixa_patrimonio value for this Faixas_Patrimonio.
	 * 
	 * @return ds_faixa_patrimonio
	 */
	public java.lang.String getDs_faixa_patrimonio() {
		return ds_faixa_patrimonio;
	}

	/**
	 * Sets the ds_faixa_patrimonio value for this Faixas_Patrimonio.
	 * 
	 * @param ds_faixa_patrimonio
	 */
	public void setDs_faixa_patrimonio(java.lang.String ds_faixa_patrimonio) {
		this.ds_faixa_patrimonio = ds_faixa_patrimonio;
	}

	/**
	 * Gets the id_faixa_patrimonio value for this Faixas_Patrimonio.
	 * 
	 * @return id_faixa_patrimonio
	 */
	public java.lang.String getId_faixa_patrimonio() {
		return id_faixa_patrimonio;
	}

	/**
	 * Sets the id_faixa_patrimonio value for this Faixas_Patrimonio.
	 * 
	 * @param id_faixa_patrimonio
	 */
	public void setId_faixa_patrimonio(java.lang.String id_faixa_patrimonio) {
		this.id_faixa_patrimonio = id_faixa_patrimonio;
	}

	@Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Faixas_Patrimonio))
			return false;
		Faixas_Patrimonio other = (Faixas_Patrimonio) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_faixa_patrimonio == null && other.getDs_faixa_patrimonio() == null)
						|| (this.ds_faixa_patrimonio != null
								&& this.ds_faixa_patrimonio.equals(other.getDs_faixa_patrimonio())))
				&& ((this.id_faixa_patrimonio == null && other.getId_faixa_patrimonio() == null)
						|| (this.id_faixa_patrimonio != null
								&& this.id_faixa_patrimonio.equals(other.getId_faixa_patrimonio())));
		__equalsCalc = null;
		return _equals;
	}

	@Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_faixa_patrimonio() != null) {
			_hashCode += getDs_faixa_patrimonio().hashCode();
		}
		if (getId_faixa_patrimonio() != null) {
			_hashCode += getId_faixa_patrimonio().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Faixas_Patrimonio.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"Faixas_Patrimonio"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_faixa_patrimonio");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_faixa_patrimonio"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_faixa_patrimonio");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_faixa_patrimonio"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
