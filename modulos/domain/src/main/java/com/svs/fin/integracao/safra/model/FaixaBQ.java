/**
 * FaixaBQ.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class FaixaBQ  implements java.io.Serializable {
    private java.math.BigDecimal vlBQ;

    private java.math.BigDecimal vlFaixaFim;

    private java.math.BigDecimal vlFaixaIni;

    public FaixaBQ() {
    }

    public FaixaBQ(
           java.math.BigDecimal vlBQ,
           java.math.BigDecimal vlFaixaFim,
           java.math.BigDecimal vlFaixaIni) {
           this.vlBQ = vlBQ;
           this.vlFaixaFim = vlFaixaFim;
           this.vlFaixaIni = vlFaixaIni;
    }


    /**
     * Gets the vlBQ value for this FaixaBQ.
     * 
     * @return vlBQ
     */
    public java.math.BigDecimal getVlBQ() {
        return vlBQ;
    }


    /**
     * Sets the vlBQ value for this FaixaBQ.
     * 
     * @param vlBQ
     */
    public void setVlBQ(java.math.BigDecimal vlBQ) {
        this.vlBQ = vlBQ;
    }


    /**
     * Gets the vlFaixaFim value for this FaixaBQ.
     * 
     * @return vlFaixaFim
     */
    public java.math.BigDecimal getVlFaixaFim() {
        return vlFaixaFim;
    }


    /**
     * Sets the vlFaixaFim value for this FaixaBQ.
     * 
     * @param vlFaixaFim
     */
    public void setVlFaixaFim(java.math.BigDecimal vlFaixaFim) {
        this.vlFaixaFim = vlFaixaFim;
    }


    /**
     * Gets the vlFaixaIni value for this FaixaBQ.
     * 
     * @return vlFaixaIni
     */
    public java.math.BigDecimal getVlFaixaIni() {
        return vlFaixaIni;
    }


    /**
     * Sets the vlFaixaIni value for this FaixaBQ.
     * 
     * @param vlFaixaIni
     */
    public void setVlFaixaIni(java.math.BigDecimal vlFaixaIni) {
        this.vlFaixaIni = vlFaixaIni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FaixaBQ)) return false;
        FaixaBQ other = (FaixaBQ) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vlBQ==null && other.getVlBQ()==null) || 
             (this.vlBQ!=null &&
              this.vlBQ.equals(other.getVlBQ()))) &&
            ((this.vlFaixaFim==null && other.getVlFaixaFim()==null) || 
             (this.vlFaixaFim!=null &&
              this.vlFaixaFim.equals(other.getVlFaixaFim()))) &&
            ((this.vlFaixaIni==null && other.getVlFaixaIni()==null) || 
             (this.vlFaixaIni!=null &&
              this.vlFaixaIni.equals(other.getVlFaixaIni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVlBQ() != null) {
            _hashCode += getVlBQ().hashCode();
        }
        if (getVlFaixaFim() != null) {
            _hashCode += getVlFaixaFim().hashCode();
        }
        if (getVlFaixaIni() != null) {
            _hashCode += getVlFaixaIni().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FaixaBQ.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaBQ"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlBQ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "VlBQ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlFaixaFim");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "VlFaixaFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlFaixaIni");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "VlFaixaIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
