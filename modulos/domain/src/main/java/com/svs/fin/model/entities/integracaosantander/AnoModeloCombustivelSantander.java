package com.svs.fin.model.entities.integracaosantander;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ANO_MOD_COMB_SANTANDER")
public class AnoModeloCombustivelSantander implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5364296221225124998L;

	@Id
	private Long id;
	
	@Column(length=100)
	private String description;
	
	@Column(length=100)
	private String integrationCode;
	
	@ManyToOne
    @JoinColumn(name = "id_modelo")
	private ModeloVeiculoSantander modelo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntegrationCode() {
		return integrationCode;
	}

	public void setIntegrationCode(String integrationCode) {
		this.integrationCode = integrationCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnoModeloCombustivelSantander other = (AnoModeloCombustivelSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public ModeloVeiculoSantander getModelo() {
		return modelo;
	}

	public void setModelo(ModeloVeiculoSantander modelo) {
		this.modelo = modelo;
	}
}
