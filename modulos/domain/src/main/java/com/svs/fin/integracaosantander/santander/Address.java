package com.svs.fin.integracaosantander.santander;

public class Address {

	private String complement;

	private String zipCode;

	private String street;

	private String state;

	private String neighborhood;

	private String number;

	private String type;

	private String city;

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "ClassPojo [complement = " + complement + ", zipCode = " + zipCode + ", street = " + street
				+ ", state = " + state + ", neighborhood = " + neighborhood + ", number = " + number + ", type = "
				+ type + ", city = " + city + "]";
	}
}
