package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PorteEmpresaSantander implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4524456484470479017L;
	
	@Id
	private Long id;
	
	@Column(length=100)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
