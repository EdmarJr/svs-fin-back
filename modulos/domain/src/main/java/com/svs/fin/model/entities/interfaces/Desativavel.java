package com.svs.fin.model.entities.interfaces;

public interface Desativavel {
	Boolean getAtivo();
	void setAtivo(Boolean ativo);
}
