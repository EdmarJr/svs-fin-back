/**
 * Cargo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Cargo")
public class Cargo implements java.io.Serializable {
	private java.lang.String ds_cargo;

	@Id
	private java.lang.Integer id_cargo;

	private java.lang.Integer id_nat_ocupacao;

	private java.lang.Integer id_profissao;

	public Cargo() {
	}

	public Cargo(java.lang.String ds_cargo, java.lang.Integer id_cargo, java.lang.Integer id_nat_ocupacao,
			java.lang.Integer id_profissao) {
		this.ds_cargo = ds_cargo;
		this.id_cargo = id_cargo;
		this.id_nat_ocupacao = id_nat_ocupacao;
		this.id_profissao = id_profissao;
	}

	/**
	 * Gets the ds_cargo value for this Cargo.
	 * 
	 * @return ds_cargo
	 */
	public java.lang.String getDs_cargo() {
		return ds_cargo;
	}

	/**
	 * Sets the ds_cargo value for this Cargo.
	 * 
	 * @param ds_cargo
	 */
	public void setDs_cargo(java.lang.String ds_cargo) {
		this.ds_cargo = ds_cargo;
	}

	/**
	 * Gets the id_cargo value for this Cargo.
	 * 
	 * @return id_cargo
	 */
	public java.lang.Integer getId_cargo() {
		return id_cargo;
	}

	/**
	 * Sets the id_cargo value for this Cargo.
	 * 
	 * @param id_cargo
	 */
	public void setId_cargo(java.lang.Integer id_cargo) {
		this.id_cargo = id_cargo;
	}

	/**
	 * Gets the id_nat_ocupacao value for this Cargo.
	 * 
	 * @return id_nat_ocupacao
	 */
	public java.lang.Integer getId_nat_ocupacao() {
		return id_nat_ocupacao;
	}

	/**
	 * Sets the id_nat_ocupacao value for this Cargo.
	 * 
	 * @param id_nat_ocupacao
	 */
	public void setId_nat_ocupacao(java.lang.Integer id_nat_ocupacao) {
		this.id_nat_ocupacao = id_nat_ocupacao;
	}

	/**
	 * Gets the id_profissao value for this Cargo.
	 * 
	 * @return id_profissao
	 */
	public java.lang.Integer getId_profissao() {
		return id_profissao;
	}

	/**
	 * Sets the id_profissao value for this Cargo.
	 * 
	 * @param id_profissao
	 */
	public void setId_profissao(java.lang.Integer id_profissao) {
		this.id_profissao = id_profissao;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Cargo))
			return false;
		Cargo other = (Cargo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_cargo == null && other.getDs_cargo() == null)
						|| (this.ds_cargo != null && this.ds_cargo.equals(other.getDs_cargo())))
				&& ((this.id_cargo == null && other.getId_cargo() == null)
						|| (this.id_cargo != null && this.id_cargo.equals(other.getId_cargo())))
				&& ((this.id_nat_ocupacao == null && other.getId_nat_ocupacao() == null)
						|| (this.id_nat_ocupacao != null && this.id_nat_ocupacao.equals(other.getId_nat_ocupacao())))
				&& ((this.id_profissao == null && other.getId_profissao() == null)
						|| (this.id_profissao != null && this.id_profissao.equals(other.getId_profissao())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_cargo() != null) {
			_hashCode += getDs_cargo().hashCode();
		}
		if (getId_cargo() != null) {
			_hashCode += getId_cargo().hashCode();
		}
		if (getId_nat_ocupacao() != null) {
			_hashCode += getId_nat_ocupacao().hashCode();
		}
		if (getId_profissao() != null) {
			_hashCode += getId_profissao().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(Cargo.class,
			true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cargo"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_cargo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_cargo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_cargo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_cargo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_nat_ocupacao");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_nat_ocupacao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_profissao");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_profissao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
