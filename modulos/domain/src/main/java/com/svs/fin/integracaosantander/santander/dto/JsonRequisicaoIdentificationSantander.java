package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.svs.fin.integracaosantander.santander.IdentificationRequest;
import com.svs.fin.model.entities.integracaosantander.AnoModeloCombustivelSantander;
import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander;

@Entity(name="JSON_IDENT_SANTANDER")
public class JsonRequisicaoIdentificationSantander implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6077353015150515973L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@Column
	private Calendar data;
	
	@Column
	private String nomeCliente;
	
	@Column
	private String cpfCliente;
	
	@Column
    @Enumerated(EnumType.STRING)
    private EnumTipoVeiculo tipoVeiculo;
	
	@ManyToOne
	@JoinColumn(name = "id_modelo")
	private ModeloVeiculoSantander modeloVeiculoSantander;
	
	@ManyToOne
	@JoinColumn(name = "id_ano_modelo")
	private AnoModeloCombustivelSantander anoModeloCombustivelSantander;
	
	@Lob
	private byte[] jsonByte;
	
	@Transient
	private String jsonTexto;
	
	@Transient
	private IdentificationRequest identificationRequest;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJsonTexto() {
		if(jsonByte != null && jsonTexto == null) {
			setJsonTexto(new String(jsonByte, Charset.forName("UTF-8")));
		}
		return jsonTexto;
	}
	
	public String getJsonTextoFormatado() {
		String jsonTexto = getJsonTexto();
//		if(jsonTexto != null && !jsonTexto.trim().equals("")) {
//			IdentificationRequest identification = new Gson().fromJson(getJsonTexto(), IdentificationRequest.class);
//			return StringUtils.getObjetoComoJsonFormatado(identification);
//		}
		return "";
	}

	public void setJsonTexto(String jsonTexto) {
		this.jsonTexto = jsonTexto;
	}

	public byte[] getJsonByte() {
		return jsonByte;
	}

	public void setJsonByte(byte[] jsonByte) {
		this.jsonByte = jsonByte;
		this.jsonTexto = null;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public IdentificationRequest getIdentificationRequest() {
		if(identificationRequest == null) {
			this.identificationRequest = new IdentificationRequest();
		}
		return identificationRequest;
	}

	public void setIdentificationRequest(IdentificationRequest identificationRequest) {
		this.identificationRequest = identificationRequest;
	}

	public EnumTipoVeiculo getTipoVeiculo() {
		if(tipoVeiculo == null) {
			tipoVeiculo = EnumTipoVeiculo.CARROS_UTILITARIOS;
		}
		return tipoVeiculo;
	}

	public void setTipoVeiculo(EnumTipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public ModeloVeiculoSantander getModeloVeiculoSantander() {
		return modeloVeiculoSantander;
	}

	public void setModeloVeiculoSantander(ModeloVeiculoSantander modeloVeiculoSantander) {
		this.modeloVeiculoSantander = modeloVeiculoSantander;
	}

	public AnoModeloCombustivelSantander getAnoModeloCombustivelSantander() {
		return anoModeloCombustivelSantander;
	}

	public void setAnoModeloCombustivelSantander(AnoModeloCombustivelSantander anoModeloCombustivelSantander) {
		this.anoModeloCombustivelSantander = anoModeloCombustivelSantander;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonRequisicaoIdentificationSantander other = (JsonRequisicaoIdentificationSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
