/**
 * Tipo_Residencia.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Residencia")
public class Tipo_Residencia implements java.io.Serializable {
	private java.lang.String ds_tipo_residencia;

	private java.lang.String fl_residencia;

	@Id
	private java.lang.Integer id_tipo_residencia;

	public Tipo_Residencia() {
	}

	public Tipo_Residencia(java.lang.String ds_tipo_residencia, java.lang.String fl_residencia,
			java.lang.Integer id_tipo_residencia) {
		this.ds_tipo_residencia = ds_tipo_residencia;
		this.fl_residencia = fl_residencia;
		this.id_tipo_residencia = id_tipo_residencia;
	}

	/**
	 * Gets the ds_tipo_residencia value for this Tipo_Residencia.
	 * 
	 * @return ds_tipo_residencia
	 */
	public java.lang.String getDs_tipo_residencia() {
		return ds_tipo_residencia;
	}

	/**
	 * Sets the ds_tipo_residencia value for this Tipo_Residencia.
	 * 
	 * @param ds_tipo_residencia
	 */
	public void setDs_tipo_residencia(java.lang.String ds_tipo_residencia) {
		this.ds_tipo_residencia = ds_tipo_residencia;
	}

	/**
	 * Gets the fl_residencia value for this Tipo_Residencia.
	 * 
	 * @return fl_residencia
	 */
	public java.lang.String getFl_residencia() {
		return fl_residencia;
	}

	/**
	 * Sets the fl_residencia value for this Tipo_Residencia.
	 * 
	 * @param fl_residencia
	 */
	public void setFl_residencia(java.lang.String fl_residencia) {
		this.fl_residencia = fl_residencia;
	}

	/**
	 * Gets the id_tipo_residencia value for this Tipo_Residencia.
	 * 
	 * @return id_tipo_residencia
	 */
	public java.lang.Integer getId_tipo_residencia() {
		return id_tipo_residencia;
	}

	/**
	 * Sets the id_tipo_residencia value for this Tipo_Residencia.
	 * 
	 * @param id_tipo_residencia
	 */
	public void setId_tipo_residencia(java.lang.Integer id_tipo_residencia) {
		this.id_tipo_residencia = id_tipo_residencia;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Residencia))
			return false;
		Tipo_Residencia other = (Tipo_Residencia) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.ds_tipo_residencia == null && other.getDs_tipo_residencia() == null)
				|| (this.ds_tipo_residencia != null && this.ds_tipo_residencia.equals(other.getDs_tipo_residencia())))
				&& ((this.fl_residencia == null && other.getFl_residencia() == null)
						|| (this.fl_residencia != null && this.fl_residencia.equals(other.getFl_residencia())))
				&& ((this.id_tipo_residencia == null && other.getId_tipo_residencia() == null)
						|| (this.id_tipo_residencia != null
								&& this.id_tipo_residencia.equals(other.getId_tipo_residencia())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_residencia() != null) {
			_hashCode += getDs_tipo_residencia().hashCode();
		}
		if (getFl_residencia() != null) {
			_hashCode += getFl_residencia().hashCode();
		}
		if (getId_tipo_residencia() != null) {
			_hashCode += getId_tipo_residencia().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Residencia.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Residencia"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_residencia");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_tipo_residencia"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("fl_residencia");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fl_residencia"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_residencia");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_tipo_residencia"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
