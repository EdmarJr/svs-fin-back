package com.svs.fin.integracaoItau;

import br.com.gruposaga.sdk.zflow.model.gen.FinanceOperationOption;
import br.com.gruposaga.sdk.zflow.model.gen.SimulationOperationResponse;

public class BeanOpcaoFinanciamentoItau {
	
	private FinanciamentoOnlineItau financiamentoItau;
	private FinanceOperationOption financeOperationOption;
	private SimulationOperationResponse simulationOperationResponse;
	
	public BeanOpcaoFinanciamentoItau() {}
	
	public BeanOpcaoFinanciamentoItau(FinanciamentoOnlineItau financiamentoItau, 
			FinanceOperationOption financeOperationOption,
			SimulationOperationResponse simulationOperationResponse) {
		
		this.financiamentoItau = financiamentoItau;
		this.financeOperationOption = financeOperationOption;
		this.simulationOperationResponse = simulationOperationResponse;
	}
	
	public FinanciamentoOnlineItau getFinanciamentoItau() {
		return financiamentoItau;
	}
	public void setFinanciamentoItau(FinanciamentoOnlineItau financiamentoItau) {
		this.financiamentoItau = financiamentoItau;
	}
	public FinanceOperationOption getFinanceOperationOption() {
		return financeOperationOption;
	}
	public void setFinanceOperationOption(FinanceOperationOption financeOperationOption) {
		this.financeOperationOption = financeOperationOption;
	}

	public SimulationOperationResponse getSimulationOperationResponse() {
		return simulationOperationResponse;
	}

	public void setSimulationOperationResponse(SimulationOperationResponse simulationOperationResponse) {
		this.simulationOperationResponse = simulationOperationResponse;
	}
}
