/**
 * Financiado.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Financiado  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_cliente;

    private java.lang.String id_patrimonio;

    private java.lang.String nm_cliente_nfan;

    private java.lang.String nm_cliente_rsoc;

    private java.lang.String nm_grupo_economico;

    private java.lang.String qt_avalistas;

    public Financiado() {
    }

    public Financiado(
           java.lang.String id_cliente,
           java.lang.String id_patrimonio,
           java.lang.String nm_cliente_nfan,
           java.lang.String nm_cliente_rsoc,
           java.lang.String nm_grupo_economico,
           java.lang.String qt_avalistas) {
        this.id_cliente = id_cliente;
        this.id_patrimonio = id_patrimonio;
        this.nm_cliente_nfan = nm_cliente_nfan;
        this.nm_cliente_rsoc = nm_cliente_rsoc;
        this.nm_grupo_economico = nm_grupo_economico;
        this.qt_avalistas = qt_avalistas;
    }


    /**
     * Gets the id_cliente value for this Financiado.
     * 
     * @return id_cliente
     */
    public java.lang.String getId_cliente() {
        return id_cliente;
    }


    /**
     * Sets the id_cliente value for this Financiado.
     * 
     * @param id_cliente
     */
    public void setId_cliente(java.lang.String id_cliente) {
        this.id_cliente = id_cliente;
    }


    /**
     * Gets the id_patrimonio value for this Financiado.
     * 
     * @return id_patrimonio
     */
    public java.lang.String getId_patrimonio() {
        return id_patrimonio;
    }


    /**
     * Sets the id_patrimonio value for this Financiado.
     * 
     * @param id_patrimonio
     */
    public void setId_patrimonio(java.lang.String id_patrimonio) {
        this.id_patrimonio = id_patrimonio;
    }


    /**
     * Gets the nm_cliente_nfan value for this Financiado.
     * 
     * @return nm_cliente_nfan
     */
    public java.lang.String getNm_cliente_nfan() {
        return nm_cliente_nfan;
    }


    /**
     * Sets the nm_cliente_nfan value for this Financiado.
     * 
     * @param nm_cliente_nfan
     */
    public void setNm_cliente_nfan(java.lang.String nm_cliente_nfan) {
        this.nm_cliente_nfan = nm_cliente_nfan;
    }


    /**
     * Gets the nm_cliente_rsoc value for this Financiado.
     * 
     * @return nm_cliente_rsoc
     */
    public java.lang.String getNm_cliente_rsoc() {
        return nm_cliente_rsoc;
    }


    /**
     * Sets the nm_cliente_rsoc value for this Financiado.
     * 
     * @param nm_cliente_rsoc
     */
    public void setNm_cliente_rsoc(java.lang.String nm_cliente_rsoc) {
        this.nm_cliente_rsoc = nm_cliente_rsoc;
    }


    /**
     * Gets the nm_grupo_economico value for this Financiado.
     * 
     * @return nm_grupo_economico
     */
    public java.lang.String getNm_grupo_economico() {
        return nm_grupo_economico;
    }


    /**
     * Sets the nm_grupo_economico value for this Financiado.
     * 
     * @param nm_grupo_economico
     */
    public void setNm_grupo_economico(java.lang.String nm_grupo_economico) {
        this.nm_grupo_economico = nm_grupo_economico;
    }


    /**
     * Gets the qt_avalistas value for this Financiado.
     * 
     * @return qt_avalistas
     */
    public java.lang.String getQt_avalistas() {
        return qt_avalistas;
    }


    /**
     * Sets the qt_avalistas value for this Financiado.
     * 
     * @param qt_avalistas
     */
    public void setQt_avalistas(java.lang.String qt_avalistas) {
        this.qt_avalistas = qt_avalistas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Financiado)) return false;
        Financiado other = (Financiado) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_cliente==null && other.getId_cliente()==null) || 
             (this.id_cliente!=null &&
              this.id_cliente.equals(other.getId_cliente()))) &&
            ((this.id_patrimonio==null && other.getId_patrimonio()==null) || 
             (this.id_patrimonio!=null &&
              this.id_patrimonio.equals(other.getId_patrimonio()))) &&
            ((this.nm_cliente_nfan==null && other.getNm_cliente_nfan()==null) || 
             (this.nm_cliente_nfan!=null &&
              this.nm_cliente_nfan.equals(other.getNm_cliente_nfan()))) &&
            ((this.nm_cliente_rsoc==null && other.getNm_cliente_rsoc()==null) || 
             (this.nm_cliente_rsoc!=null &&
              this.nm_cliente_rsoc.equals(other.getNm_cliente_rsoc()))) &&
            ((this.nm_grupo_economico==null && other.getNm_grupo_economico()==null) || 
             (this.nm_grupo_economico!=null &&
              this.nm_grupo_economico.equals(other.getNm_grupo_economico()))) &&
            ((this.qt_avalistas==null && other.getQt_avalistas()==null) || 
             (this.qt_avalistas!=null &&
              this.qt_avalistas.equals(other.getQt_avalistas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_cliente() != null) {
            _hashCode += getId_cliente().hashCode();
        }
        if (getId_patrimonio() != null) {
            _hashCode += getId_patrimonio().hashCode();
        }
        if (getNm_cliente_nfan() != null) {
            _hashCode += getNm_cliente_nfan().hashCode();
        }
        if (getNm_cliente_rsoc() != null) {
            _hashCode += getNm_cliente_rsoc().hashCode();
        }
        if (getNm_grupo_economico() != null) {
            _hashCode += getNm_grupo_economico().hashCode();
        }
        if (getQt_avalistas() != null) {
            _hashCode += getQt_avalistas().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Financiado.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Financiado"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_patrimonio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_patrimonio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_cliente_nfan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_cliente_nfan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_cliente_rsoc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_cliente_rsoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_grupo_economico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_grupo_economico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qt_avalistas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "qt_avalistas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
