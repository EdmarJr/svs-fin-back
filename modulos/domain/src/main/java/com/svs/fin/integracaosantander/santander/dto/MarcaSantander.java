package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.svs.fin.enums.EnumMarca;

@Entity
public class MarcaSantander implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3037149495850343891L;

	@Id
	private Long id;

	@Column(length = 100)
	private String description;

	@Column(length = 100)
	private String integrationCode;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoVeiculo tipoVeiculo;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumMarca marcaMySaga;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntegrationCode() {
		return integrationCode;
	}

	public void setIntegrationCode(String integrationCode) {
		this.integrationCode = integrationCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarcaSantander other = (MarcaSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public EnumTipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(EnumTipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumMarca getMarcaMySaga() {
		return marcaMySaga;
	}

	public void setMarcaMySaga(EnumMarca marcaMySaga) {
		this.marcaMySaga = marcaMySaga;
	}
}
