/**
 * RetornoProposta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class RetornoProposta  implements java.io.Serializable {
    private java.lang.String id_proposta;

    private java.lang.String id_retorno;

    public RetornoProposta() {
    }

    public RetornoProposta(
           java.lang.String id_proposta,
           java.lang.String id_retorno) {
           this.id_proposta = id_proposta;
           this.id_retorno = id_retorno;
    }


    /**
     * Gets the id_proposta value for this RetornoProposta.
     * 
     * @return id_proposta
     */
    public java.lang.String getId_proposta() {
        return id_proposta;
    }


    /**
     * Sets the id_proposta value for this RetornoProposta.
     * 
     * @param id_proposta
     */
    public void setId_proposta(java.lang.String id_proposta) {
        this.id_proposta = id_proposta;
    }


    /**
     * Gets the id_retorno value for this RetornoProposta.
     * 
     * @return id_retorno
     */
    public java.lang.String getId_retorno() {
        return id_retorno;
    }


    /**
     * Sets the id_retorno value for this RetornoProposta.
     * 
     * @param id_retorno
     */
    public void setId_retorno(java.lang.String id_retorno) {
        this.id_retorno = id_retorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetornoProposta)) return false;
        RetornoProposta other = (RetornoProposta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id_proposta==null && other.getId_proposta()==null) || 
             (this.id_proposta!=null &&
              this.id_proposta.equals(other.getId_proposta()))) &&
            ((this.id_retorno==null && other.getId_retorno()==null) || 
             (this.id_retorno!=null &&
              this.id_retorno.equals(other.getId_retorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId_proposta() != null) {
            _hashCode += getId_proposta().hashCode();
        }
        if (getId_retorno() != null) {
            _hashCode += getId_retorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetornoProposta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoProposta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_proposta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_proposta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_retorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_retorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
