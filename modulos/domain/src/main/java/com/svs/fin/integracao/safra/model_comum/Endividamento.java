/**
 * Endividamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Endividamento  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String agencia;

    private java.lang.String banco;

    private java.lang.String ds_garantia;

    private java.lang.String dt_contrato;

    private java.lang.String dt_contrato_fim;

    private java.lang.String id_tipo_endividamento;

    private java.lang.String vl_contrato;

    private java.lang.String vl_prestacao;

    public Endividamento() {
    }

    public Endividamento(
           java.lang.String agencia,
           java.lang.String banco,
           java.lang.String ds_garantia,
           java.lang.String dt_contrato,
           java.lang.String dt_contrato_fim,
           java.lang.String id_tipo_endividamento,
           java.lang.String vl_contrato,
           java.lang.String vl_prestacao) {
        this.agencia = agencia;
        this.banco = banco;
        this.ds_garantia = ds_garantia;
        this.dt_contrato = dt_contrato;
        this.dt_contrato_fim = dt_contrato_fim;
        this.id_tipo_endividamento = id_tipo_endividamento;
        this.vl_contrato = vl_contrato;
        this.vl_prestacao = vl_prestacao;
    }


    /**
     * Gets the agencia value for this Endividamento.
     * 
     * @return agencia
     */
    public java.lang.String getAgencia() {
        return agencia;
    }


    /**
     * Sets the agencia value for this Endividamento.
     * 
     * @param agencia
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }


    /**
     * Gets the banco value for this Endividamento.
     * 
     * @return banco
     */
    public java.lang.String getBanco() {
        return banco;
    }


    /**
     * Sets the banco value for this Endividamento.
     * 
     * @param banco
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }


    /**
     * Gets the ds_garantia value for this Endividamento.
     * 
     * @return ds_garantia
     */
    public java.lang.String getDs_garantia() {
        return ds_garantia;
    }


    /**
     * Sets the ds_garantia value for this Endividamento.
     * 
     * @param ds_garantia
     */
    public void setDs_garantia(java.lang.String ds_garantia) {
        this.ds_garantia = ds_garantia;
    }


    /**
     * Gets the dt_contrato value for this Endividamento.
     * 
     * @return dt_contrato
     */
    public java.lang.String getDt_contrato() {
        return dt_contrato;
    }


    /**
     * Sets the dt_contrato value for this Endividamento.
     * 
     * @param dt_contrato
     */
    public void setDt_contrato(java.lang.String dt_contrato) {
        this.dt_contrato = dt_contrato;
    }


    /**
     * Gets the dt_contrato_fim value for this Endividamento.
     * 
     * @return dt_contrato_fim
     */
    public java.lang.String getDt_contrato_fim() {
        return dt_contrato_fim;
    }


    /**
     * Sets the dt_contrato_fim value for this Endividamento.
     * 
     * @param dt_contrato_fim
     */
    public void setDt_contrato_fim(java.lang.String dt_contrato_fim) {
        this.dt_contrato_fim = dt_contrato_fim;
    }


    /**
     * Gets the id_tipo_endividamento value for this Endividamento.
     * 
     * @return id_tipo_endividamento
     */
    public java.lang.String getId_tipo_endividamento() {
        return id_tipo_endividamento;
    }


    /**
     * Sets the id_tipo_endividamento value for this Endividamento.
     * 
     * @param id_tipo_endividamento
     */
    public void setId_tipo_endividamento(java.lang.String id_tipo_endividamento) {
        this.id_tipo_endividamento = id_tipo_endividamento;
    }


    /**
     * Gets the vl_contrato value for this Endividamento.
     * 
     * @return vl_contrato
     */
    public java.lang.String getVl_contrato() {
        return vl_contrato;
    }


    /**
     * Sets the vl_contrato value for this Endividamento.
     * 
     * @param vl_contrato
     */
    public void setVl_contrato(java.lang.String vl_contrato) {
        this.vl_contrato = vl_contrato;
    }


    /**
     * Gets the vl_prestacao value for this Endividamento.
     * 
     * @return vl_prestacao
     */
    public java.lang.String getVl_prestacao() {
        return vl_prestacao;
    }


    /**
     * Sets the vl_prestacao value for this Endividamento.
     * 
     * @param vl_prestacao
     */
    public void setVl_prestacao(java.lang.String vl_prestacao) {
        this.vl_prestacao = vl_prestacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Endividamento)) return false;
        Endividamento other = (Endividamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.agencia==null && other.getAgencia()==null) || 
             (this.agencia!=null &&
              this.agencia.equals(other.getAgencia()))) &&
            ((this.banco==null && other.getBanco()==null) || 
             (this.banco!=null &&
              this.banco.equals(other.getBanco()))) &&
            ((this.ds_garantia==null && other.getDs_garantia()==null) || 
             (this.ds_garantia!=null &&
              this.ds_garantia.equals(other.getDs_garantia()))) &&
            ((this.dt_contrato==null && other.getDt_contrato()==null) || 
             (this.dt_contrato!=null &&
              this.dt_contrato.equals(other.getDt_contrato()))) &&
            ((this.dt_contrato_fim==null && other.getDt_contrato_fim()==null) || 
             (this.dt_contrato_fim!=null &&
              this.dt_contrato_fim.equals(other.getDt_contrato_fim()))) &&
            ((this.id_tipo_endividamento==null && other.getId_tipo_endividamento()==null) || 
             (this.id_tipo_endividamento!=null &&
              this.id_tipo_endividamento.equals(other.getId_tipo_endividamento()))) &&
            ((this.vl_contrato==null && other.getVl_contrato()==null) || 
             (this.vl_contrato!=null &&
              this.vl_contrato.equals(other.getVl_contrato()))) &&
            ((this.vl_prestacao==null && other.getVl_prestacao()==null) || 
             (this.vl_prestacao!=null &&
              this.vl_prestacao.equals(other.getVl_prestacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAgencia() != null) {
            _hashCode += getAgencia().hashCode();
        }
        if (getBanco() != null) {
            _hashCode += getBanco().hashCode();
        }
        if (getDs_garantia() != null) {
            _hashCode += getDs_garantia().hashCode();
        }
        if (getDt_contrato() != null) {
            _hashCode += getDt_contrato().hashCode();
        }
        if (getDt_contrato_fim() != null) {
            _hashCode += getDt_contrato_fim().hashCode();
        }
        if (getId_tipo_endividamento() != null) {
            _hashCode += getId_tipo_endividamento().hashCode();
        }
        if (getVl_contrato() != null) {
            _hashCode += getVl_contrato().hashCode();
        }
        if (getVl_prestacao() != null) {
            _hashCode += getVl_prestacao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Endividamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endividamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "agencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("banco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "banco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_garantia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_garantia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_contrato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_contrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_contrato_fim");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_contrato_fim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_tipo_endividamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_tipo_endividamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_contrato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_contrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_prestacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_prestacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
