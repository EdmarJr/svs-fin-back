/**
 * Cadastro_Comum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Cadastro_Comum  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] bens_imoveis;

    private com.svs.fin.integracao.safra.model_comum.Endereco endereco;

    private com.svs.fin.integracao.safra.model_comum.Financiado financiado;

    private com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] referencias_bancarias;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone_celular;

    public Cadastro_Comum() {
    }

    public Cadastro_Comum(
           com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] bens_imoveis,
           com.svs.fin.integracao.safra.model_comum.Endereco endereco,
           com.svs.fin.integracao.safra.model_comum.Financiado financiado,
           com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] referencias_bancarias,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone_celular) {
           this.bens_imoveis = bens_imoveis;
           this.endereco = endereco;
           this.financiado = financiado;
           this.referencias_bancarias = referencias_bancarias;
           this.telefone = telefone;
           this.telefone_celular = telefone_celular;
    }


    /**
     * Gets the bens_imoveis value for this Cadastro_Comum.
     * 
     * @return bens_imoveis
     */
    public com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] getBens_imoveis() {
        return bens_imoveis;
    }


    /**
     * Sets the bens_imoveis value for this Cadastro_Comum.
     * 
     * @param bens_imoveis
     */
    public void setBens_imoveis(com.svs.fin.integracao.safra.model_comum.Bem_Imovel[] bens_imoveis) {
        this.bens_imoveis = bens_imoveis;
    }


    /**
     * Gets the endereco value for this Cadastro_Comum.
     * 
     * @return endereco
     */
    public com.svs.fin.integracao.safra.model_comum.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this Cadastro_Comum.
     * 
     * @param endereco
     */
    public void setEndereco(com.svs.fin.integracao.safra.model_comum.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the financiado value for this Cadastro_Comum.
     * 
     * @return financiado
     */
    public com.svs.fin.integracao.safra.model_comum.Financiado getFinanciado() {
        return financiado;
    }


    /**
     * Sets the financiado value for this Cadastro_Comum.
     * 
     * @param financiado
     */
    public void setFinanciado(com.svs.fin.integracao.safra.model_comum.Financiado financiado) {
        this.financiado = financiado;
    }


    /**
     * Gets the referencias_bancarias value for this Cadastro_Comum.
     * 
     * @return referencias_bancarias
     */
    public com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] getReferencias_bancarias() {
        return referencias_bancarias;
    }


    /**
     * Sets the referencias_bancarias value for this Cadastro_Comum.
     * 
     * @param referencias_bancarias
     */
    public void setReferencias_bancarias(com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[] referencias_bancarias) {
        this.referencias_bancarias = referencias_bancarias;
    }


    /**
     * Gets the telefone value for this Cadastro_Comum.
     * 
     * @return telefone
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Cadastro_Comum.
     * 
     * @param telefone
     */
    public void setTelefone(com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.telefone = telefone;
    }


    /**
     * Gets the telefone_celular value for this Cadastro_Comum.
     * 
     * @return telefone_celular
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone_celular() {
        return telefone_celular;
    }


    /**
     * Sets the telefone_celular value for this Cadastro_Comum.
     * 
     * @param telefone_celular
     */
    public void setTelefone_celular(com.svs.fin.integracao.safra.model_comum.Telefone telefone_celular) {
        this.telefone_celular = telefone_celular;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Cadastro_Comum)) return false;
        Cadastro_Comum other = (Cadastro_Comum) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bens_imoveis==null && other.getBens_imoveis()==null) || 
             (this.bens_imoveis!=null &&
              java.util.Arrays.equals(this.bens_imoveis, other.getBens_imoveis()))) &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.financiado==null && other.getFinanciado()==null) || 
             (this.financiado!=null &&
              this.financiado.equals(other.getFinanciado()))) &&
            ((this.referencias_bancarias==null && other.getReferencias_bancarias()==null) || 
             (this.referencias_bancarias!=null &&
              java.util.Arrays.equals(this.referencias_bancarias, other.getReferencias_bancarias()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone()))) &&
            ((this.telefone_celular==null && other.getTelefone_celular()==null) || 
             (this.telefone_celular!=null &&
              this.telefone_celular.equals(other.getTelefone_celular())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBens_imoveis() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBens_imoveis());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBens_imoveis(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getFinanciado() != null) {
            _hashCode += getFinanciado().hashCode();
        }
        if (getReferencias_bancarias() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferencias_bancarias());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferencias_bancarias(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        if (getTelefone_celular() != null) {
            _hashCode += getTelefone_celular().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Cadastro_Comum.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cadastro_Comum"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bens_imoveis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "bens_imoveis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("financiado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "financiado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Financiado"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencias_bancarias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "referencias_bancarias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone_celular");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "telefone_celular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
