package com.svs.fin.model.dto.listagemtabelalazy;

public class ParametroBusca {
	private String nomeCampo;
	private String textoBusca;
	private String tipoComparacao;

	public ParametroBusca(String nomeCampo, String textoBusca) {
		this.nomeCampo = nomeCampo;
		this.textoBusca = textoBusca;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}

	public String getTextoBusca() {
		return textoBusca;
	}

	public void setTextoBusca(String textoBusca) {
		this.textoBusca = textoBusca;
	}

	
	
}
