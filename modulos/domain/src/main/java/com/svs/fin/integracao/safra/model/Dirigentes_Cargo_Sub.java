/**
 * Dirigentes_Cargo_Sub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Dirigentes_Cargo_Sub")
public class Dirigentes_Cargo_Sub implements java.io.Serializable {
	private java.lang.String ds_dirigente_cargo_sub;

	private java.lang.Integer id_dirigente_cargo;

	@Id
	private java.lang.Integer id_dirigente_cargo_sub;

	public Dirigentes_Cargo_Sub() {
	}

	public Dirigentes_Cargo_Sub(java.lang.String ds_dirigente_cargo_sub, java.lang.Integer id_dirigente_cargo,
			java.lang.Integer id_dirigente_cargo_sub) {
		this.ds_dirigente_cargo_sub = ds_dirigente_cargo_sub;
		this.id_dirigente_cargo = id_dirigente_cargo;
		this.id_dirigente_cargo_sub = id_dirigente_cargo_sub;
	}

	/**
	 * Gets the ds_dirigente_cargo_sub value for this Dirigentes_Cargo_Sub.
	 * 
	 * @return ds_dirigente_cargo_sub
	 */
	public java.lang.String getDs_dirigente_cargo_sub() {
		return ds_dirigente_cargo_sub;
	}

	/**
	 * Sets the ds_dirigente_cargo_sub value for this Dirigentes_Cargo_Sub.
	 * 
	 * @param ds_dirigente_cargo_sub
	 */
	public void setDs_dirigente_cargo_sub(java.lang.String ds_dirigente_cargo_sub) {
		this.ds_dirigente_cargo_sub = ds_dirigente_cargo_sub;
	}

	/**
	 * Gets the id_dirigente_cargo value for this Dirigentes_Cargo_Sub.
	 * 
	 * @return id_dirigente_cargo
	 */
	public java.lang.Integer getId_dirigente_cargo() {
		return id_dirigente_cargo;
	}

	/**
	 * Sets the id_dirigente_cargo value for this Dirigentes_Cargo_Sub.
	 * 
	 * @param id_dirigente_cargo
	 */
	public void setId_dirigente_cargo(java.lang.Integer id_dirigente_cargo) {
		this.id_dirigente_cargo = id_dirigente_cargo;
	}

	/**
	 * Gets the id_dirigente_cargo_sub value for this Dirigentes_Cargo_Sub.
	 * 
	 * @return id_dirigente_cargo_sub
	 */
	public java.lang.Integer getId_dirigente_cargo_sub() {
		return id_dirigente_cargo_sub;
	}

	/**
	 * Sets the id_dirigente_cargo_sub value for this Dirigentes_Cargo_Sub.
	 * 
	 * @param id_dirigente_cargo_sub
	 */
	public void setId_dirigente_cargo_sub(java.lang.Integer id_dirigente_cargo_sub) {
		this.id_dirigente_cargo_sub = id_dirigente_cargo_sub;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Dirigentes_Cargo_Sub))
			return false;
		Dirigentes_Cargo_Sub other = (Dirigentes_Cargo_Sub) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_dirigente_cargo_sub == null && other.getDs_dirigente_cargo_sub() == null)
						|| (this.ds_dirigente_cargo_sub != null
								&& this.ds_dirigente_cargo_sub.equals(other.getDs_dirigente_cargo_sub())))
				&& ((this.id_dirigente_cargo == null && other.getId_dirigente_cargo() == null)
						|| (this.id_dirigente_cargo != null
								&& this.id_dirigente_cargo.equals(other.getId_dirigente_cargo())))
				&& ((this.id_dirigente_cargo_sub == null && other.getId_dirigente_cargo_sub() == null)
						|| (this.id_dirigente_cargo_sub != null
								&& this.id_dirigente_cargo_sub.equals(other.getId_dirigente_cargo_sub())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_dirigente_cargo_sub() != null) {
			_hashCode += getDs_dirigente_cargo_sub().hashCode();
		}
		if (getId_dirigente_cargo() != null) {
			_hashCode += getId_dirigente_cargo().hashCode();
		}
		if (getId_dirigente_cargo_sub() != null) {
			_hashCode += getId_dirigente_cargo_sub().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Dirigentes_Cargo_Sub.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"Dirigentes_Cargo_Sub"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_dirigente_cargo_sub");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_dirigente_cargo_sub"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_dirigente_cargo");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_dirigente_cargo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_dirigente_cargo_sub");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_dirigente_cargo_sub"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
