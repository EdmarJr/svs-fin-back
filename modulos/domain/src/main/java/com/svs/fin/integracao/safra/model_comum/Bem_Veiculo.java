/**
 * Bem_Veiculo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Bem_Veiculo  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ano_veiculo;

    private java.lang.String nm_placa;

    private java.lang.String nm_veiculo;

    private java.lang.String vl_onus;

    private java.lang.String vl_veiculo;

    public Bem_Veiculo() {
    }

    public Bem_Veiculo(
           java.lang.String ano_veiculo,
           java.lang.String nm_placa,
           java.lang.String nm_veiculo,
           java.lang.String vl_onus,
           java.lang.String vl_veiculo) {
        this.ano_veiculo = ano_veiculo;
        this.nm_placa = nm_placa;
        this.nm_veiculo = nm_veiculo;
        this.vl_onus = vl_onus;
        this.vl_veiculo = vl_veiculo;
    }


    /**
     * Gets the ano_veiculo value for this Bem_Veiculo.
     * 
     * @return ano_veiculo
     */
    public java.lang.String getAno_veiculo() {
        return ano_veiculo;
    }


    /**
     * Sets the ano_veiculo value for this Bem_Veiculo.
     * 
     * @param ano_veiculo
     */
    public void setAno_veiculo(java.lang.String ano_veiculo) {
        this.ano_veiculo = ano_veiculo;
    }


    /**
     * Gets the nm_placa value for this Bem_Veiculo.
     * 
     * @return nm_placa
     */
    public java.lang.String getNm_placa() {
        return nm_placa;
    }


    /**
     * Sets the nm_placa value for this Bem_Veiculo.
     * 
     * @param nm_placa
     */
    public void setNm_placa(java.lang.String nm_placa) {
        this.nm_placa = nm_placa;
    }


    /**
     * Gets the nm_veiculo value for this Bem_Veiculo.
     * 
     * @return nm_veiculo
     */
    public java.lang.String getNm_veiculo() {
        return nm_veiculo;
    }


    /**
     * Sets the nm_veiculo value for this Bem_Veiculo.
     * 
     * @param nm_veiculo
     */
    public void setNm_veiculo(java.lang.String nm_veiculo) {
        this.nm_veiculo = nm_veiculo;
    }


    /**
     * Gets the vl_onus value for this Bem_Veiculo.
     * 
     * @return vl_onus
     */
    public java.lang.String getVl_onus() {
        return vl_onus;
    }


    /**
     * Sets the vl_onus value for this Bem_Veiculo.
     * 
     * @param vl_onus
     */
    public void setVl_onus(java.lang.String vl_onus) {
        this.vl_onus = vl_onus;
    }


    /**
     * Gets the vl_veiculo value for this Bem_Veiculo.
     * 
     * @return vl_veiculo
     */
    public java.lang.String getVl_veiculo() {
        return vl_veiculo;
    }


    /**
     * Sets the vl_veiculo value for this Bem_Veiculo.
     * 
     * @param vl_veiculo
     */
    public void setVl_veiculo(java.lang.String vl_veiculo) {
        this.vl_veiculo = vl_veiculo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bem_Veiculo)) return false;
        Bem_Veiculo other = (Bem_Veiculo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ano_veiculo==null && other.getAno_veiculo()==null) || 
             (this.ano_veiculo!=null &&
              this.ano_veiculo.equals(other.getAno_veiculo()))) &&
            ((this.nm_placa==null && other.getNm_placa()==null) || 
             (this.nm_placa!=null &&
              this.nm_placa.equals(other.getNm_placa()))) &&
            ((this.nm_veiculo==null && other.getNm_veiculo()==null) || 
             (this.nm_veiculo!=null &&
              this.nm_veiculo.equals(other.getNm_veiculo()))) &&
            ((this.vl_onus==null && other.getVl_onus()==null) || 
             (this.vl_onus!=null &&
              this.vl_onus.equals(other.getVl_onus()))) &&
            ((this.vl_veiculo==null && other.getVl_veiculo()==null) || 
             (this.vl_veiculo!=null &&
              this.vl_veiculo.equals(other.getVl_veiculo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAno_veiculo() != null) {
            _hashCode += getAno_veiculo().hashCode();
        }
        if (getNm_placa() != null) {
            _hashCode += getNm_placa().hashCode();
        }
        if (getNm_veiculo() != null) {
            _hashCode += getNm_veiculo().hashCode();
        }
        if (getVl_onus() != null) {
            _hashCode += getVl_onus().hashCode();
        }
        if (getVl_veiculo() != null) {
            _hashCode += getVl_veiculo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bem_Veiculo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Veiculo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ano_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ano_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_placa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_placa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_onus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_onus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
