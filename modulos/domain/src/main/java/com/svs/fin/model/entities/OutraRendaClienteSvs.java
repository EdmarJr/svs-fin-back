package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = OutraRendaClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = OutraRendaClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
public class OutraRendaClienteSvs implements Serializable {

	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "OutraRendaClienteSvs.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from OutraRendaClienteSvs e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 480111585606088799L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OUTRARENDACLIENTESVS")
	@SequenceGenerator(name = "SEQ_OUTRARENDACLIENTESVS", sequenceName = "SEQ_OUTRARENDACLIENTESVS", allocationSize = 1)
	private Long id;

	@Column(length = 500)
	private String descricao;

	@Column(length = 500)
	private String telefone;

	@Column
	private BigDecimal valor;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private ClienteSvs clienteSvs;

	@ManyToOne
	@JoinColumn(name = "avalista_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private Avalista avalista;

	@Transient
	private Boolean editando;

	public Avalista getAvalista() {
		return avalista;
	}

	public void setAvalista(Avalista avalista) {
		this.avalista = avalista;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OutraRendaClienteSvs other = (OutraRendaClienteSvs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

}
