/**
 * Servico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Servico")
public class Servico implements java.io.Serializable {
	private java.lang.String ds_servico;

	@Id
	private java.lang.Integer id_servico;

	public Servico() {
	}

	public Servico(java.lang.String ds_servico, java.lang.Integer id_servico) {
		this.ds_servico = ds_servico;
		this.id_servico = id_servico;
	}

	/**
	 * Gets the ds_servico value for this Servico.
	 * 
	 * @return ds_servico
	 */
	public java.lang.String getDs_servico() {
		return ds_servico;
	}

	/**
	 * Sets the ds_servico value for this Servico.
	 * 
	 * @param ds_servico
	 */
	public void setDs_servico(java.lang.String ds_servico) {
		this.ds_servico = ds_servico;
	}

	/**
	 * Gets the id_servico value for this Servico.
	 * 
	 * @return id_servico
	 */
	public java.lang.Integer getId_servico() {
		return id_servico;
	}

	/**
	 * Sets the id_servico value for this Servico.
	 * 
	 * @param id_servico
	 */
	public void setId_servico(java.lang.Integer id_servico) {
		this.id_servico = id_servico;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Servico))
			return false;
		Servico other = (Servico) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_servico == null && other.getDs_servico() == null)
						|| (this.ds_servico != null && this.ds_servico.equals(other.getDs_servico())))
				&& ((this.id_servico == null && other.getId_servico() == null)
						|| (this.id_servico != null && this.id_servico.equals(other.getId_servico())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_servico() != null) {
			_hashCode += getDs_servico().hashCode();
		}
		if (getId_servico() != null) {
			_hashCode += getId_servico().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Servico.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Servico"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_servico");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_servico"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_servico");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_servico"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
