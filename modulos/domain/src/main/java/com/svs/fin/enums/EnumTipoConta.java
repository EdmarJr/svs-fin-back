package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoConta {
	@JsonProperty("Corrente")
	CORRENTE("Corrente"),
	@JsonProperty("Poupança")
	POUPANCA("Poupança"),
	@JsonProperty("Salário")
	SALARIO("Salário");

	private String label;
	
	private EnumTipoConta(String label){
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	

}
