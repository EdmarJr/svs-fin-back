package com.svs.fin.integracaosantander.santander;

public class Spouse {

	private String document;

	private String name;

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ClassPojo [document = " + document + ", name = " + name + "]";
	}

}
