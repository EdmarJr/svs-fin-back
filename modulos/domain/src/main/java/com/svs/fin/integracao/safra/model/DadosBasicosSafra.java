/**
 * DadosBasicosSafra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class DadosBasicosSafra  implements java.io.Serializable {
    private com.svs.fin.integracao.safra.model.Afinidade[] afinidades;

    private com.svs.fin.integracao.safra.model.Atividade[] atividades;

    private com.svs.fin.integracao.safra.model.Bem_Opcional[] bem_opcionais;

    private com.svs.fin.integracao.safra.model.Cargo[] cargos;

    private com.svs.fin.integracao.safra.model.Catalogo_Erro[] catalogo_erros;

    private com.svs.fin.integracao.safra.model.Dirigentes_Cargo_Sub[] dirigentes_cargos_sub;

    private com.svs.fin.integracao.safra.model.Empresa[] empresas;

    private com.svs.fin.integracao.safra.model.Escolaridade[] escolaridades;

    private com.svs.fin.integracao.safra.model.Estado_Civil[] estados_civil;

    private com.svs.fin.integracao.safra.model.Faixas_Patrimonio[] faixas_patrimonio;

    private com.svs.fin.integracao.safra.model.Imovel_Especie[] imovel_especies;

    private com.svs.fin.integracao.safra.model.Imovel_Tipo[] imovel_tipos;

    private com.svs.fin.integracao.safra.model.Isencao_Fiscal[] isencao_fiscal;

    private com.svs.fin.integracao.safra.model.Lojista[] lojistas;

    private com.svs.fin.integracao.safra.model.Moeda[] moedas;

    private com.svs.fin.integracao.safra.model.Nacionalidade[] nacionalidades;

    private com.svs.fin.integracao.safra.model.Natureza_Ocupacao[] naturezas_ocupacao;

    private com.svs.fin.integracao.safra.model.Pessoa[] pessoas;

    private com.svs.fin.integracao.safra.model.Planos[] planos;

    private com.svs.fin.integracao.safra.model.Porte_Empresa[] portes_empresa;

    private com.svs.fin.integracao.safra.model.Produto[] produtos;

    private com.svs.fin.integracao.safra.model.Profissao[] profissoes;

    private com.svs.fin.integracao.safra.model.Ramo_Atividade[] ramos_atividade;

    private com.svs.fin.integracao.safra.model.Registro_Gravame[] registros_gravames;

    private com.svs.fin.integracao.safra.model.Sede_Social[] sedes_social;

    private com.svs.fin.integracao.safra.model.Segmento_Veiculo[] segmentos_veiculo;

    private com.svs.fin.integracao.safra.model.Servico[] servicos;

    private com.svs.fin.integracao.safra.model.Sexo[] sexos;

    private com.svs.fin.integracao.safra.model.StatusProposta[] status_proposta;

    private com.svs.fin.integracao.safra.model.Tarifa_Avaliacao[] tarifa_avaliacao;

    private com.svs.fin.integracao.safra.model.Tipo_Categoria[] tipos_categoria;

    private com.svs.fin.integracao.safra.model.Tipo_Combustivel[] tipos_combustivel;

    private com.svs.fin.integracao.safra.model.Tipo_Compromisso[] tipos_compromisso;

    private com.svs.fin.integracao.safra.model.Tipo_Documento[] tipos_documento;

    private com.svs.fin.integracao.safra.model.Tipo_Emprego[] tipos_emprego;

    private com.svs.fin.integracao.safra.model.Tipo_Endereco[] tipos_endereco;

    private com.svs.fin.integracao.safra.model.Tipo_Endividamento[] tipos_endividamento;

    private com.svs.fin.integracao.safra.model.Tipo_Fluxo[] tipos_fluxo;

    private com.svs.fin.integracao.safra.model.Tipo_Residencia[] tipos_residencia;

    private com.svs.fin.integracao.safra.model.Tipo_Seguro[] tipos_seguro;

    private com.svs.fin.integracao.safra.model.Tipo_Sub_Telefone[] tipos_sub_telefone;

    private com.svs.fin.integracao.safra.model.Tipo_Telefone[] tipos_telefone;

    private com.svs.fin.integracao.safra.model.Tipo_Veiculo[] tipos_veiculo;

    private com.svs.fin.integracao.safra.model.Tipo_Veiculo_Porte[] tipos_veiculo_porte;

    public DadosBasicosSafra() {
    }

    public DadosBasicosSafra(
           com.svs.fin.integracao.safra.model.Afinidade[] afinidades,
           com.svs.fin.integracao.safra.model.Atividade[] atividades,
           com.svs.fin.integracao.safra.model.Bem_Opcional[] bem_opcionais,
           com.svs.fin.integracao.safra.model.Cargo[] cargos,
           com.svs.fin.integracao.safra.model.Catalogo_Erro[] catalogo_erros,
           com.svs.fin.integracao.safra.model.Dirigentes_Cargo_Sub[] dirigentes_cargos_sub,
           com.svs.fin.integracao.safra.model.Empresa[] empresas,
           com.svs.fin.integracao.safra.model.Escolaridade[] escolaridades,
           com.svs.fin.integracao.safra.model.Estado_Civil[] estados_civil,
           com.svs.fin.integracao.safra.model.Faixas_Patrimonio[] faixas_patrimonio,
           com.svs.fin.integracao.safra.model.Imovel_Especie[] imovel_especies,
           com.svs.fin.integracao.safra.model.Imovel_Tipo[] imovel_tipos,
           com.svs.fin.integracao.safra.model.Isencao_Fiscal[] isencao_fiscal,
           com.svs.fin.integracao.safra.model.Lojista[] lojistas,
           com.svs.fin.integracao.safra.model.Moeda[] moedas,
           com.svs.fin.integracao.safra.model.Nacionalidade[] nacionalidades,
           com.svs.fin.integracao.safra.model.Natureza_Ocupacao[] naturezas_ocupacao,
           com.svs.fin.integracao.safra.model.Pessoa[] pessoas,
           com.svs.fin.integracao.safra.model.Planos[] planos,
           com.svs.fin.integracao.safra.model.Porte_Empresa[] portes_empresa,
           com.svs.fin.integracao.safra.model.Produto[] produtos,
           com.svs.fin.integracao.safra.model.Profissao[] profissoes,
           com.svs.fin.integracao.safra.model.Ramo_Atividade[] ramos_atividade,
           com.svs.fin.integracao.safra.model.Registro_Gravame[] registros_gravames,
           com.svs.fin.integracao.safra.model.Sede_Social[] sedes_social,
           com.svs.fin.integracao.safra.model.Segmento_Veiculo[] segmentos_veiculo,
           com.svs.fin.integracao.safra.model.Servico[] servicos,
           com.svs.fin.integracao.safra.model.Sexo[] sexos,
           com.svs.fin.integracao.safra.model.StatusProposta[] status_proposta,
           com.svs.fin.integracao.safra.model.Tarifa_Avaliacao[] tarifa_avaliacao,
           com.svs.fin.integracao.safra.model.Tipo_Categoria[] tipos_categoria,
           com.svs.fin.integracao.safra.model.Tipo_Combustivel[] tipos_combustivel,
           com.svs.fin.integracao.safra.model.Tipo_Compromisso[] tipos_compromisso,
           com.svs.fin.integracao.safra.model.Tipo_Documento[] tipos_documento,
           com.svs.fin.integracao.safra.model.Tipo_Emprego[] tipos_emprego,
           com.svs.fin.integracao.safra.model.Tipo_Endereco[] tipos_endereco,
           com.svs.fin.integracao.safra.model.Tipo_Endividamento[] tipos_endividamento,
           com.svs.fin.integracao.safra.model.Tipo_Fluxo[] tipos_fluxo,
           com.svs.fin.integracao.safra.model.Tipo_Residencia[] tipos_residencia,
           com.svs.fin.integracao.safra.model.Tipo_Seguro[] tipos_seguro,
           com.svs.fin.integracao.safra.model.Tipo_Sub_Telefone[] tipos_sub_telefone,
           com.svs.fin.integracao.safra.model.Tipo_Telefone[] tipos_telefone,
           com.svs.fin.integracao.safra.model.Tipo_Veiculo[] tipos_veiculo,
           com.svs.fin.integracao.safra.model.Tipo_Veiculo_Porte[] tipos_veiculo_porte) {
           this.afinidades = afinidades;
           this.atividades = atividades;
           this.bem_opcionais = bem_opcionais;
           this.cargos = cargos;
           this.catalogo_erros = catalogo_erros;
           this.dirigentes_cargos_sub = dirigentes_cargos_sub;
           this.empresas = empresas;
           this.escolaridades = escolaridades;
           this.estados_civil = estados_civil;
           this.faixas_patrimonio = faixas_patrimonio;
           this.imovel_especies = imovel_especies;
           this.imovel_tipos = imovel_tipos;
           this.isencao_fiscal = isencao_fiscal;
           this.lojistas = lojistas;
           this.moedas = moedas;
           this.nacionalidades = nacionalidades;
           this.naturezas_ocupacao = naturezas_ocupacao;
           this.pessoas = pessoas;
           this.planos = planos;
           this.portes_empresa = portes_empresa;
           this.produtos = produtos;
           this.profissoes = profissoes;
           this.ramos_atividade = ramos_atividade;
           this.registros_gravames = registros_gravames;
           this.sedes_social = sedes_social;
           this.segmentos_veiculo = segmentos_veiculo;
           this.servicos = servicos;
           this.sexos = sexos;
           this.status_proposta = status_proposta;
           this.tarifa_avaliacao = tarifa_avaliacao;
           this.tipos_categoria = tipos_categoria;
           this.tipos_combustivel = tipos_combustivel;
           this.tipos_compromisso = tipos_compromisso;
           this.tipos_documento = tipos_documento;
           this.tipos_emprego = tipos_emprego;
           this.tipos_endereco = tipos_endereco;
           this.tipos_endividamento = tipos_endividamento;
           this.tipos_fluxo = tipos_fluxo;
           this.tipos_residencia = tipos_residencia;
           this.tipos_seguro = tipos_seguro;
           this.tipos_sub_telefone = tipos_sub_telefone;
           this.tipos_telefone = tipos_telefone;
           this.tipos_veiculo = tipos_veiculo;
           this.tipos_veiculo_porte = tipos_veiculo_porte;
    }


    /**
     * Gets the afinidades value for this DadosBasicosSafra.
     * 
     * @return afinidades
     */
    public com.svs.fin.integracao.safra.model.Afinidade[] getAfinidades() {
        return afinidades;
    }


    /**
     * Sets the afinidades value for this DadosBasicosSafra.
     * 
     * @param afinidades
     */
    public void setAfinidades(com.svs.fin.integracao.safra.model.Afinidade[] afinidades) {
        this.afinidades = afinidades;
    }


    /**
     * Gets the atividades value for this DadosBasicosSafra.
     * 
     * @return atividades
     */
    public com.svs.fin.integracao.safra.model.Atividade[] getAtividades() {
        return atividades;
    }


    /**
     * Sets the atividades value for this DadosBasicosSafra.
     * 
     * @param atividades
     */
    public void setAtividades(com.svs.fin.integracao.safra.model.Atividade[] atividades) {
        this.atividades = atividades;
    }


    /**
     * Gets the bem_opcionais value for this DadosBasicosSafra.
     * 
     * @return bem_opcionais
     */
    public com.svs.fin.integracao.safra.model.Bem_Opcional[] getBem_opcionais() {
        return bem_opcionais;
    }


    /**
     * Sets the bem_opcionais value for this DadosBasicosSafra.
     * 
     * @param bem_opcionais
     */
    public void setBem_opcionais(com.svs.fin.integracao.safra.model.Bem_Opcional[] bem_opcionais) {
        this.bem_opcionais = bem_opcionais;
    }


    /**
     * Gets the cargos value for this DadosBasicosSafra.
     * 
     * @return cargos
     */
    public com.svs.fin.integracao.safra.model.Cargo[] getCargos() {
        return cargos;
    }


    /**
     * Sets the cargos value for this DadosBasicosSafra.
     * 
     * @param cargos
     */
    public void setCargos(com.svs.fin.integracao.safra.model.Cargo[] cargos) {
        this.cargos = cargos;
    }


    /**
     * Gets the catalogo_erros value for this DadosBasicosSafra.
     * 
     * @return catalogo_erros
     */
    public com.svs.fin.integracao.safra.model.Catalogo_Erro[] getCatalogo_erros() {
        return catalogo_erros;
    }


    /**
     * Sets the catalogo_erros value for this DadosBasicosSafra.
     * 
     * @param catalogo_erros
     */
    public void setCatalogo_erros(com.svs.fin.integracao.safra.model.Catalogo_Erro[] catalogo_erros) {
        this.catalogo_erros = catalogo_erros;
    }


    /**
     * Gets the dirigentes_cargos_sub value for this DadosBasicosSafra.
     * 
     * @return dirigentes_cargos_sub
     */
    public com.svs.fin.integracao.safra.model.Dirigentes_Cargo_Sub[] getDirigentes_cargos_sub() {
        return dirigentes_cargos_sub;
    }


    /**
     * Sets the dirigentes_cargos_sub value for this DadosBasicosSafra.
     * 
     * @param dirigentes_cargos_sub
     */
    public void setDirigentes_cargos_sub(com.svs.fin.integracao.safra.model.Dirigentes_Cargo_Sub[] dirigentes_cargos_sub) {
        this.dirigentes_cargos_sub = dirigentes_cargos_sub;
    }


    /**
     * Gets the empresas value for this DadosBasicosSafra.
     * 
     * @return empresas
     */
    public com.svs.fin.integracao.safra.model.Empresa[] getEmpresas() {
        return empresas;
    }


    /**
     * Sets the empresas value for this DadosBasicosSafra.
     * 
     * @param empresas
     */
    public void setEmpresas(com.svs.fin.integracao.safra.model.Empresa[] empresas) {
        this.empresas = empresas;
    }


    /**
     * Gets the escolaridades value for this DadosBasicosSafra.
     * 
     * @return escolaridades
     */
    public com.svs.fin.integracao.safra.model.Escolaridade[] getEscolaridades() {
        return escolaridades;
    }


    /**
     * Sets the escolaridades value for this DadosBasicosSafra.
     * 
     * @param escolaridades
     */
    public void setEscolaridades(com.svs.fin.integracao.safra.model.Escolaridade[] escolaridades) {
        this.escolaridades = escolaridades;
    }


    /**
     * Gets the estados_civil value for this DadosBasicosSafra.
     * 
     * @return estados_civil
     */
    public com.svs.fin.integracao.safra.model.Estado_Civil[] getEstados_civil() {
        return estados_civil;
    }


    /**
     * Sets the estados_civil value for this DadosBasicosSafra.
     * 
     * @param estados_civil
     */
    public void setEstados_civil(com.svs.fin.integracao.safra.model.Estado_Civil[] estados_civil) {
        this.estados_civil = estados_civil;
    }


    /**
     * Gets the faixas_patrimonio value for this DadosBasicosSafra.
     * 
     * @return faixas_patrimonio
     */
    public com.svs.fin.integracao.safra.model.Faixas_Patrimonio[] getFaixas_patrimonio() {
        return faixas_patrimonio;
    }


    /**
     * Sets the faixas_patrimonio value for this DadosBasicosSafra.
     * 
     * @param faixas_patrimonio
     */
    public void setFaixas_patrimonio(com.svs.fin.integracao.safra.model.Faixas_Patrimonio[] faixas_patrimonio) {
        this.faixas_patrimonio = faixas_patrimonio;
    }


    /**
     * Gets the imovel_especies value for this DadosBasicosSafra.
     * 
     * @return imovel_especies
     */
    public com.svs.fin.integracao.safra.model.Imovel_Especie[] getImovel_especies() {
        return imovel_especies;
    }


    /**
     * Sets the imovel_especies value for this DadosBasicosSafra.
     * 
     * @param imovel_especies
     */
    public void setImovel_especies(com.svs.fin.integracao.safra.model.Imovel_Especie[] imovel_especies) {
        this.imovel_especies = imovel_especies;
    }


    /**
     * Gets the imovel_tipos value for this DadosBasicosSafra.
     * 
     * @return imovel_tipos
     */
    public com.svs.fin.integracao.safra.model.Imovel_Tipo[] getImovel_tipos() {
        return imovel_tipos;
    }


    /**
     * Sets the imovel_tipos value for this DadosBasicosSafra.
     * 
     * @param imovel_tipos
     */
    public void setImovel_tipos(com.svs.fin.integracao.safra.model.Imovel_Tipo[] imovel_tipos) {
        this.imovel_tipos = imovel_tipos;
    }


    /**
     * Gets the isencao_fiscal value for this DadosBasicosSafra.
     * 
     * @return isencao_fiscal
     */
    public com.svs.fin.integracao.safra.model.Isencao_Fiscal[] getIsencao_fiscal() {
        return isencao_fiscal;
    }


    /**
     * Sets the isencao_fiscal value for this DadosBasicosSafra.
     * 
     * @param isencao_fiscal
     */
    public void setIsencao_fiscal(com.svs.fin.integracao.safra.model.Isencao_Fiscal[] isencao_fiscal) {
        this.isencao_fiscal = isencao_fiscal;
    }


    /**
     * Gets the lojistas value for this DadosBasicosSafra.
     * 
     * @return lojistas
     */
    public com.svs.fin.integracao.safra.model.Lojista[] getLojistas() {
        return lojistas;
    }


    /**
     * Sets the lojistas value for this DadosBasicosSafra.
     * 
     * @param lojistas
     */
    public void setLojistas(com.svs.fin.integracao.safra.model.Lojista[] lojistas) {
        this.lojistas = lojistas;
    }


    /**
     * Gets the moedas value for this DadosBasicosSafra.
     * 
     * @return moedas
     */
    public com.svs.fin.integracao.safra.model.Moeda[] getMoedas() {
        return moedas;
    }


    /**
     * Sets the moedas value for this DadosBasicosSafra.
     * 
     * @param moedas
     */
    public void setMoedas(com.svs.fin.integracao.safra.model.Moeda[] moedas) {
        this.moedas = moedas;
    }


    /**
     * Gets the nacionalidades value for this DadosBasicosSafra.
     * 
     * @return nacionalidades
     */
    public com.svs.fin.integracao.safra.model.Nacionalidade[] getNacionalidades() {
        return nacionalidades;
    }


    /**
     * Sets the nacionalidades value for this DadosBasicosSafra.
     * 
     * @param nacionalidades
     */
    public void setNacionalidades(com.svs.fin.integracao.safra.model.Nacionalidade[] nacionalidades) {
        this.nacionalidades = nacionalidades;
    }


    /**
     * Gets the naturezas_ocupacao value for this DadosBasicosSafra.
     * 
     * @return naturezas_ocupacao
     */
    public com.svs.fin.integracao.safra.model.Natureza_Ocupacao[] getNaturezas_ocupacao() {
        return naturezas_ocupacao;
    }


    /**
     * Sets the naturezas_ocupacao value for this DadosBasicosSafra.
     * 
     * @param naturezas_ocupacao
     */
    public void setNaturezas_ocupacao(com.svs.fin.integracao.safra.model.Natureza_Ocupacao[] naturezas_ocupacao) {
        this.naturezas_ocupacao = naturezas_ocupacao;
    }


    /**
     * Gets the pessoas value for this DadosBasicosSafra.
     * 
     * @return pessoas
     */
    public com.svs.fin.integracao.safra.model.Pessoa[] getPessoas() {
        return pessoas;
    }


    /**
     * Sets the pessoas value for this DadosBasicosSafra.
     * 
     * @param pessoas
     */
    public void setPessoas(com.svs.fin.integracao.safra.model.Pessoa[] pessoas) {
        this.pessoas = pessoas;
    }


    /**
     * Gets the planos value for this DadosBasicosSafra.
     * 
     * @return planos
     */
    public com.svs.fin.integracao.safra.model.Planos[] getPlanos() {
        return planos;
    }


    /**
     * Sets the planos value for this DadosBasicosSafra.
     * 
     * @param planos
     */
    public void setPlanos(com.svs.fin.integracao.safra.model.Planos[] planos) {
        this.planos = planos;
    }


    /**
     * Gets the portes_empresa value for this DadosBasicosSafra.
     * 
     * @return portes_empresa
     */
    public com.svs.fin.integracao.safra.model.Porte_Empresa[] getPortes_empresa() {
        return portes_empresa;
    }


    /**
     * Sets the portes_empresa value for this DadosBasicosSafra.
     * 
     * @param portes_empresa
     */
    public void setPortes_empresa(com.svs.fin.integracao.safra.model.Porte_Empresa[] portes_empresa) {
        this.portes_empresa = portes_empresa;
    }


    /**
     * Gets the produtos value for this DadosBasicosSafra.
     * 
     * @return produtos
     */
    public com.svs.fin.integracao.safra.model.Produto[] getProdutos() {
        return produtos;
    }


    /**
     * Sets the produtos value for this DadosBasicosSafra.
     * 
     * @param produtos
     */
    public void setProdutos(com.svs.fin.integracao.safra.model.Produto[] produtos) {
        this.produtos = produtos;
    }


    /**
     * Gets the profissoes value for this DadosBasicosSafra.
     * 
     * @return profissoes
     */
    public com.svs.fin.integracao.safra.model.Profissao[] getProfissoes() {
        return profissoes;
    }


    /**
     * Sets the profissoes value for this DadosBasicosSafra.
     * 
     * @param profissoes
     */
    public void setProfissoes(com.svs.fin.integracao.safra.model.Profissao[] profissoes) {
        this.profissoes = profissoes;
    }


    /**
     * Gets the ramos_atividade value for this DadosBasicosSafra.
     * 
     * @return ramos_atividade
     */
    public com.svs.fin.integracao.safra.model.Ramo_Atividade[] getRamos_atividade() {
        return ramos_atividade;
    }


    /**
     * Sets the ramos_atividade value for this DadosBasicosSafra.
     * 
     * @param ramos_atividade
     */
    public void setRamos_atividade(com.svs.fin.integracao.safra.model.Ramo_Atividade[] ramos_atividade) {
        this.ramos_atividade = ramos_atividade;
    }


    /**
     * Gets the registros_gravames value for this DadosBasicosSafra.
     * 
     * @return registros_gravames
     */
    public com.svs.fin.integracao.safra.model.Registro_Gravame[] getRegistros_gravames() {
        return registros_gravames;
    }


    /**
     * Sets the registros_gravames value for this DadosBasicosSafra.
     * 
     * @param registros_gravames
     */
    public void setRegistros_gravames(com.svs.fin.integracao.safra.model.Registro_Gravame[] registros_gravames) {
        this.registros_gravames = registros_gravames;
    }


    /**
     * Gets the sedes_social value for this DadosBasicosSafra.
     * 
     * @return sedes_social
     */
    public com.svs.fin.integracao.safra.model.Sede_Social[] getSedes_social() {
        return sedes_social;
    }


    /**
     * Sets the sedes_social value for this DadosBasicosSafra.
     * 
     * @param sedes_social
     */
    public void setSedes_social(com.svs.fin.integracao.safra.model.Sede_Social[] sedes_social) {
        this.sedes_social = sedes_social;
    }


    /**
     * Gets the segmentos_veiculo value for this DadosBasicosSafra.
     * 
     * @return segmentos_veiculo
     */
    public com.svs.fin.integracao.safra.model.Segmento_Veiculo[] getSegmentos_veiculo() {
        return segmentos_veiculo;
    }


    /**
     * Sets the segmentos_veiculo value for this DadosBasicosSafra.
     * 
     * @param segmentos_veiculo
     */
    public void setSegmentos_veiculo(com.svs.fin.integracao.safra.model.Segmento_Veiculo[] segmentos_veiculo) {
        this.segmentos_veiculo = segmentos_veiculo;
    }


    /**
     * Gets the servicos value for this DadosBasicosSafra.
     * 
     * @return servicos
     */
    public com.svs.fin.integracao.safra.model.Servico[] getServicos() {
        return servicos;
    }


    /**
     * Sets the servicos value for this DadosBasicosSafra.
     * 
     * @param servicos
     */
    public void setServicos(com.svs.fin.integracao.safra.model.Servico[] servicos) {
        this.servicos = servicos;
    }


    /**
     * Gets the sexos value for this DadosBasicosSafra.
     * 
     * @return sexos
     */
    public com.svs.fin.integracao.safra.model.Sexo[] getSexos() {
        return sexos;
    }


    /**
     * Sets the sexos value for this DadosBasicosSafra.
     * 
     * @param sexos
     */
    public void setSexos(com.svs.fin.integracao.safra.model.Sexo[] sexos) {
        this.sexos = sexos;
    }


    /**
     * Gets the status_proposta value for this DadosBasicosSafra.
     * 
     * @return status_proposta
     */
    public com.svs.fin.integracao.safra.model.StatusProposta[] getStatus_proposta() {
        return status_proposta;
    }


    /**
     * Sets the status_proposta value for this DadosBasicosSafra.
     * 
     * @param status_proposta
     */
    public void setStatus_proposta(com.svs.fin.integracao.safra.model.StatusProposta[] status_proposta) {
        this.status_proposta = status_proposta;
    }


    /**
     * Gets the tarifa_avaliacao value for this DadosBasicosSafra.
     * 
     * @return tarifa_avaliacao
     */
    public com.svs.fin.integracao.safra.model.Tarifa_Avaliacao[] getTarifa_avaliacao() {
        return tarifa_avaliacao;
    }


    /**
     * Sets the tarifa_avaliacao value for this DadosBasicosSafra.
     * 
     * @param tarifa_avaliacao
     */
    public void setTarifa_avaliacao(com.svs.fin.integracao.safra.model.Tarifa_Avaliacao[] tarifa_avaliacao) {
        this.tarifa_avaliacao = tarifa_avaliacao;
    }


    /**
     * Gets the tipos_categoria value for this DadosBasicosSafra.
     * 
     * @return tipos_categoria
     */
    public com.svs.fin.integracao.safra.model.Tipo_Categoria[] getTipos_categoria() {
        return tipos_categoria;
    }


    /**
     * Sets the tipos_categoria value for this DadosBasicosSafra.
     * 
     * @param tipos_categoria
     */
    public void setTipos_categoria(com.svs.fin.integracao.safra.model.Tipo_Categoria[] tipos_categoria) {
        this.tipos_categoria = tipos_categoria;
    }


    /**
     * Gets the tipos_combustivel value for this DadosBasicosSafra.
     * 
     * @return tipos_combustivel
     */
    public com.svs.fin.integracao.safra.model.Tipo_Combustivel[] getTipos_combustivel() {
        return tipos_combustivel;
    }


    /**
     * Sets the tipos_combustivel value for this DadosBasicosSafra.
     * 
     * @param tipos_combustivel
     */
    public void setTipos_combustivel(com.svs.fin.integracao.safra.model.Tipo_Combustivel[] tipos_combustivel) {
        this.tipos_combustivel = tipos_combustivel;
    }


    /**
     * Gets the tipos_compromisso value for this DadosBasicosSafra.
     * 
     * @return tipos_compromisso
     */
    public com.svs.fin.integracao.safra.model.Tipo_Compromisso[] getTipos_compromisso() {
        return tipos_compromisso;
    }


    /**
     * Sets the tipos_compromisso value for this DadosBasicosSafra.
     * 
     * @param tipos_compromisso
     */
    public void setTipos_compromisso(com.svs.fin.integracao.safra.model.Tipo_Compromisso[] tipos_compromisso) {
        this.tipos_compromisso = tipos_compromisso;
    }


    /**
     * Gets the tipos_documento value for this DadosBasicosSafra.
     * 
     * @return tipos_documento
     */
    public com.svs.fin.integracao.safra.model.Tipo_Documento[] getTipos_documento() {
        return tipos_documento;
    }


    /**
     * Sets the tipos_documento value for this DadosBasicosSafra.
     * 
     * @param tipos_documento
     */
    public void setTipos_documento(com.svs.fin.integracao.safra.model.Tipo_Documento[] tipos_documento) {
        this.tipos_documento = tipos_documento;
    }


    /**
     * Gets the tipos_emprego value for this DadosBasicosSafra.
     * 
     * @return tipos_emprego
     */
    public com.svs.fin.integracao.safra.model.Tipo_Emprego[] getTipos_emprego() {
        return tipos_emprego;
    }


    /**
     * Sets the tipos_emprego value for this DadosBasicosSafra.
     * 
     * @param tipos_emprego
     */
    public void setTipos_emprego(com.svs.fin.integracao.safra.model.Tipo_Emprego[] tipos_emprego) {
        this.tipos_emprego = tipos_emprego;
    }


    /**
     * Gets the tipos_endereco value for this DadosBasicosSafra.
     * 
     * @return tipos_endereco
     */
    public com.svs.fin.integracao.safra.model.Tipo_Endereco[] getTipos_endereco() {
        return tipos_endereco;
    }


    /**
     * Sets the tipos_endereco value for this DadosBasicosSafra.
     * 
     * @param tipos_endereco
     */
    public void setTipos_endereco(com.svs.fin.integracao.safra.model.Tipo_Endereco[] tipos_endereco) {
        this.tipos_endereco = tipos_endereco;
    }


    /**
     * Gets the tipos_endividamento value for this DadosBasicosSafra.
     * 
     * @return tipos_endividamento
     */
    public com.svs.fin.integracao.safra.model.Tipo_Endividamento[] getTipos_endividamento() {
        return tipos_endividamento;
    }


    /**
     * Sets the tipos_endividamento value for this DadosBasicosSafra.
     * 
     * @param tipos_endividamento
     */
    public void setTipos_endividamento(com.svs.fin.integracao.safra.model.Tipo_Endividamento[] tipos_endividamento) {
        this.tipos_endividamento = tipos_endividamento;
    }


    /**
     * Gets the tipos_fluxo value for this DadosBasicosSafra.
     * 
     * @return tipos_fluxo
     */
    public com.svs.fin.integracao.safra.model.Tipo_Fluxo[] getTipos_fluxo() {
        return tipos_fluxo;
    }


    /**
     * Sets the tipos_fluxo value for this DadosBasicosSafra.
     * 
     * @param tipos_fluxo
     */
    public void setTipos_fluxo(com.svs.fin.integracao.safra.model.Tipo_Fluxo[] tipos_fluxo) {
        this.tipos_fluxo = tipos_fluxo;
    }


    /**
     * Gets the tipos_residencia value for this DadosBasicosSafra.
     * 
     * @return tipos_residencia
     */
    public com.svs.fin.integracao.safra.model.Tipo_Residencia[] getTipos_residencia() {
        return tipos_residencia;
    }


    /**
     * Sets the tipos_residencia value for this DadosBasicosSafra.
     * 
     * @param tipos_residencia
     */
    public void setTipos_residencia(com.svs.fin.integracao.safra.model.Tipo_Residencia[] tipos_residencia) {
        this.tipos_residencia = tipos_residencia;
    }


    /**
     * Gets the tipos_seguro value for this DadosBasicosSafra.
     * 
     * @return tipos_seguro
     */
    public com.svs.fin.integracao.safra.model.Tipo_Seguro[] getTipos_seguro() {
        return tipos_seguro;
    }


    /**
     * Sets the tipos_seguro value for this DadosBasicosSafra.
     * 
     * @param tipos_seguro
     */
    public void setTipos_seguro(com.svs.fin.integracao.safra.model.Tipo_Seguro[] tipos_seguro) {
        this.tipos_seguro = tipos_seguro;
    }


    /**
     * Gets the tipos_sub_telefone value for this DadosBasicosSafra.
     * 
     * @return tipos_sub_telefone
     */
    public com.svs.fin.integracao.safra.model.Tipo_Sub_Telefone[] getTipos_sub_telefone() {
        return tipos_sub_telefone;
    }


    /**
     * Sets the tipos_sub_telefone value for this DadosBasicosSafra.
     * 
     * @param tipos_sub_telefone
     */
    public void setTipos_sub_telefone(com.svs.fin.integracao.safra.model.Tipo_Sub_Telefone[] tipos_sub_telefone) {
        this.tipos_sub_telefone = tipos_sub_telefone;
    }


    /**
     * Gets the tipos_telefone value for this DadosBasicosSafra.
     * 
     * @return tipos_telefone
     */
    public com.svs.fin.integracao.safra.model.Tipo_Telefone[] getTipos_telefone() {
        return tipos_telefone;
    }


    /**
     * Sets the tipos_telefone value for this DadosBasicosSafra.
     * 
     * @param tipos_telefone
     */
    public void setTipos_telefone(com.svs.fin.integracao.safra.model.Tipo_Telefone[] tipos_telefone) {
        this.tipos_telefone = tipos_telefone;
    }


    /**
     * Gets the tipos_veiculo value for this DadosBasicosSafra.
     * 
     * @return tipos_veiculo
     */
    public com.svs.fin.integracao.safra.model.Tipo_Veiculo[] getTipos_veiculo() {
        return tipos_veiculo;
    }


    /**
     * Sets the tipos_veiculo value for this DadosBasicosSafra.
     * 
     * @param tipos_veiculo
     */
    public void setTipos_veiculo(com.svs.fin.integracao.safra.model.Tipo_Veiculo[] tipos_veiculo) {
        this.tipos_veiculo = tipos_veiculo;
    }


    /**
     * Gets the tipos_veiculo_porte value for this DadosBasicosSafra.
     * 
     * @return tipos_veiculo_porte
     */
    public com.svs.fin.integracao.safra.model.Tipo_Veiculo_Porte[] getTipos_veiculo_porte() {
        return tipos_veiculo_porte;
    }


    /**
     * Sets the tipos_veiculo_porte value for this DadosBasicosSafra.
     * 
     * @param tipos_veiculo_porte
     */
    public void setTipos_veiculo_porte(com.svs.fin.integracao.safra.model.Tipo_Veiculo_Porte[] tipos_veiculo_porte) {
        this.tipos_veiculo_porte = tipos_veiculo_porte;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DadosBasicosSafra)) return false;
        DadosBasicosSafra other = (DadosBasicosSafra) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.afinidades==null && other.getAfinidades()==null) || 
             (this.afinidades!=null &&
              java.util.Arrays.equals(this.afinidades, other.getAfinidades()))) &&
            ((this.atividades==null && other.getAtividades()==null) || 
             (this.atividades!=null &&
              java.util.Arrays.equals(this.atividades, other.getAtividades()))) &&
            ((this.bem_opcionais==null && other.getBem_opcionais()==null) || 
             (this.bem_opcionais!=null &&
              java.util.Arrays.equals(this.bem_opcionais, other.getBem_opcionais()))) &&
            ((this.cargos==null && other.getCargos()==null) || 
             (this.cargos!=null &&
              java.util.Arrays.equals(this.cargos, other.getCargos()))) &&
            ((this.catalogo_erros==null && other.getCatalogo_erros()==null) || 
             (this.catalogo_erros!=null &&
              java.util.Arrays.equals(this.catalogo_erros, other.getCatalogo_erros()))) &&
            ((this.dirigentes_cargos_sub==null && other.getDirigentes_cargos_sub()==null) || 
             (this.dirigentes_cargos_sub!=null &&
              java.util.Arrays.equals(this.dirigentes_cargos_sub, other.getDirigentes_cargos_sub()))) &&
            ((this.empresas==null && other.getEmpresas()==null) || 
             (this.empresas!=null &&
              java.util.Arrays.equals(this.empresas, other.getEmpresas()))) &&
            ((this.escolaridades==null && other.getEscolaridades()==null) || 
             (this.escolaridades!=null &&
              java.util.Arrays.equals(this.escolaridades, other.getEscolaridades()))) &&
            ((this.estados_civil==null && other.getEstados_civil()==null) || 
             (this.estados_civil!=null &&
              java.util.Arrays.equals(this.estados_civil, other.getEstados_civil()))) &&
            ((this.faixas_patrimonio==null && other.getFaixas_patrimonio()==null) || 
             (this.faixas_patrimonio!=null &&
              java.util.Arrays.equals(this.faixas_patrimonio, other.getFaixas_patrimonio()))) &&
            ((this.imovel_especies==null && other.getImovel_especies()==null) || 
             (this.imovel_especies!=null &&
              java.util.Arrays.equals(this.imovel_especies, other.getImovel_especies()))) &&
            ((this.imovel_tipos==null && other.getImovel_tipos()==null) || 
             (this.imovel_tipos!=null &&
              java.util.Arrays.equals(this.imovel_tipos, other.getImovel_tipos()))) &&
            ((this.isencao_fiscal==null && other.getIsencao_fiscal()==null) || 
             (this.isencao_fiscal!=null &&
              java.util.Arrays.equals(this.isencao_fiscal, other.getIsencao_fiscal()))) &&
            ((this.lojistas==null && other.getLojistas()==null) || 
             (this.lojistas!=null &&
              java.util.Arrays.equals(this.lojistas, other.getLojistas()))) &&
            ((this.moedas==null && other.getMoedas()==null) || 
             (this.moedas!=null &&
              java.util.Arrays.equals(this.moedas, other.getMoedas()))) &&
            ((this.nacionalidades==null && other.getNacionalidades()==null) || 
             (this.nacionalidades!=null &&
              java.util.Arrays.equals(this.nacionalidades, other.getNacionalidades()))) &&
            ((this.naturezas_ocupacao==null && other.getNaturezas_ocupacao()==null) || 
             (this.naturezas_ocupacao!=null &&
              java.util.Arrays.equals(this.naturezas_ocupacao, other.getNaturezas_ocupacao()))) &&
            ((this.pessoas==null && other.getPessoas()==null) || 
             (this.pessoas!=null &&
              java.util.Arrays.equals(this.pessoas, other.getPessoas()))) &&
            ((this.planos==null && other.getPlanos()==null) || 
             (this.planos!=null &&
              java.util.Arrays.equals(this.planos, other.getPlanos()))) &&
            ((this.portes_empresa==null && other.getPortes_empresa()==null) || 
             (this.portes_empresa!=null &&
              java.util.Arrays.equals(this.portes_empresa, other.getPortes_empresa()))) &&
            ((this.produtos==null && other.getProdutos()==null) || 
             (this.produtos!=null &&
              java.util.Arrays.equals(this.produtos, other.getProdutos()))) &&
            ((this.profissoes==null && other.getProfissoes()==null) || 
             (this.profissoes!=null &&
              java.util.Arrays.equals(this.profissoes, other.getProfissoes()))) &&
            ((this.ramos_atividade==null && other.getRamos_atividade()==null) || 
             (this.ramos_atividade!=null &&
              java.util.Arrays.equals(this.ramos_atividade, other.getRamos_atividade()))) &&
            ((this.registros_gravames==null && other.getRegistros_gravames()==null) || 
             (this.registros_gravames!=null &&
              java.util.Arrays.equals(this.registros_gravames, other.getRegistros_gravames()))) &&
            ((this.sedes_social==null && other.getSedes_social()==null) || 
             (this.sedes_social!=null &&
              java.util.Arrays.equals(this.sedes_social, other.getSedes_social()))) &&
            ((this.segmentos_veiculo==null && other.getSegmentos_veiculo()==null) || 
             (this.segmentos_veiculo!=null &&
              java.util.Arrays.equals(this.segmentos_veiculo, other.getSegmentos_veiculo()))) &&
            ((this.servicos==null && other.getServicos()==null) || 
             (this.servicos!=null &&
              java.util.Arrays.equals(this.servicos, other.getServicos()))) &&
            ((this.sexos==null && other.getSexos()==null) || 
             (this.sexos!=null &&
              java.util.Arrays.equals(this.sexos, other.getSexos()))) &&
            ((this.status_proposta==null && other.getStatus_proposta()==null) || 
             (this.status_proposta!=null &&
              java.util.Arrays.equals(this.status_proposta, other.getStatus_proposta()))) &&
            ((this.tarifa_avaliacao==null && other.getTarifa_avaliacao()==null) || 
             (this.tarifa_avaliacao!=null &&
              java.util.Arrays.equals(this.tarifa_avaliacao, other.getTarifa_avaliacao()))) &&
            ((this.tipos_categoria==null && other.getTipos_categoria()==null) || 
             (this.tipos_categoria!=null &&
              java.util.Arrays.equals(this.tipos_categoria, other.getTipos_categoria()))) &&
            ((this.tipos_combustivel==null && other.getTipos_combustivel()==null) || 
             (this.tipos_combustivel!=null &&
              java.util.Arrays.equals(this.tipos_combustivel, other.getTipos_combustivel()))) &&
            ((this.tipos_compromisso==null && other.getTipos_compromisso()==null) || 
             (this.tipos_compromisso!=null &&
              java.util.Arrays.equals(this.tipos_compromisso, other.getTipos_compromisso()))) &&
            ((this.tipos_documento==null && other.getTipos_documento()==null) || 
             (this.tipos_documento!=null &&
              java.util.Arrays.equals(this.tipos_documento, other.getTipos_documento()))) &&
            ((this.tipos_emprego==null && other.getTipos_emprego()==null) || 
             (this.tipos_emprego!=null &&
              java.util.Arrays.equals(this.tipos_emprego, other.getTipos_emprego()))) &&
            ((this.tipos_endereco==null && other.getTipos_endereco()==null) || 
             (this.tipos_endereco!=null &&
              java.util.Arrays.equals(this.tipos_endereco, other.getTipos_endereco()))) &&
            ((this.tipos_endividamento==null && other.getTipos_endividamento()==null) || 
             (this.tipos_endividamento!=null &&
              java.util.Arrays.equals(this.tipos_endividamento, other.getTipos_endividamento()))) &&
            ((this.tipos_fluxo==null && other.getTipos_fluxo()==null) || 
             (this.tipos_fluxo!=null &&
              java.util.Arrays.equals(this.tipos_fluxo, other.getTipos_fluxo()))) &&
            ((this.tipos_residencia==null && other.getTipos_residencia()==null) || 
             (this.tipos_residencia!=null &&
              java.util.Arrays.equals(this.tipos_residencia, other.getTipos_residencia()))) &&
            ((this.tipos_seguro==null && other.getTipos_seguro()==null) || 
             (this.tipos_seguro!=null &&
              java.util.Arrays.equals(this.tipos_seguro, other.getTipos_seguro()))) &&
            ((this.tipos_sub_telefone==null && other.getTipos_sub_telefone()==null) || 
             (this.tipos_sub_telefone!=null &&
              java.util.Arrays.equals(this.tipos_sub_telefone, other.getTipos_sub_telefone()))) &&
            ((this.tipos_telefone==null && other.getTipos_telefone()==null) || 
             (this.tipos_telefone!=null &&
              java.util.Arrays.equals(this.tipos_telefone, other.getTipos_telefone()))) &&
            ((this.tipos_veiculo==null && other.getTipos_veiculo()==null) || 
             (this.tipos_veiculo!=null &&
              java.util.Arrays.equals(this.tipos_veiculo, other.getTipos_veiculo()))) &&
            ((this.tipos_veiculo_porte==null && other.getTipos_veiculo_porte()==null) || 
             (this.tipos_veiculo_porte!=null &&
              java.util.Arrays.equals(this.tipos_veiculo_porte, other.getTipos_veiculo_porte())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAfinidades() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAfinidades());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAfinidades(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAtividades() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAtividades());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAtividades(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBem_opcionais() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBem_opcionais());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBem_opcionais(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCargos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCargos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCargos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCatalogo_erros() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCatalogo_erros());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCatalogo_erros(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDirigentes_cargos_sub() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDirigentes_cargos_sub());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDirigentes_cargos_sub(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEmpresas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEmpresas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEmpresas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEscolaridades() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEscolaridades());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEscolaridades(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEstados_civil() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEstados_civil());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEstados_civil(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFaixas_patrimonio() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFaixas_patrimonio());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFaixas_patrimonio(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getImovel_especies() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getImovel_especies());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getImovel_especies(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getImovel_tipos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getImovel_tipos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getImovel_tipos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIsencao_fiscal() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIsencao_fiscal());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIsencao_fiscal(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLojistas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLojistas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLojistas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMoedas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMoedas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMoedas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNacionalidades() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNacionalidades());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNacionalidades(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNaturezas_ocupacao() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNaturezas_ocupacao());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNaturezas_ocupacao(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPessoas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPessoas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPessoas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPlanos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPlanos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPlanos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPortes_empresa() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPortes_empresa());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPortes_empresa(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProdutos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProdutos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProdutos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProfissoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProfissoes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProfissoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRamos_atividade() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRamos_atividade());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRamos_atividade(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRegistros_gravames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRegistros_gravames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRegistros_gravames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSedes_social() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSedes_social());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSedes_social(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSegmentos_veiculo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegmentos_veiculo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegmentos_veiculo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getServicos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServicos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServicos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSexos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSexos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSexos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStatus_proposta() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatus_proposta());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatus_proposta(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTarifa_avaliacao() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTarifa_avaliacao());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTarifa_avaliacao(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_categoria() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_categoria());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_categoria(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_combustivel() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_combustivel());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_combustivel(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_compromisso() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_compromisso());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_compromisso(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_documento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_documento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_documento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_emprego() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_emprego());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_emprego(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_endereco() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_endereco());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_endereco(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_endividamento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_endividamento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_endividamento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_fluxo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_fluxo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_fluxo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_residencia() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_residencia());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_residencia(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_seguro() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_seguro());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_seguro(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_sub_telefone() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_sub_telefone());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_sub_telefone(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_telefone() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_telefone());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_telefone(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_veiculo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_veiculo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_veiculo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipos_veiculo_porte() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipos_veiculo_porte());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipos_veiculo_porte(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DadosBasicosSafra.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DadosBasicosSafra"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("afinidades");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "afinidades"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Afinidade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Afinidade"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("atividades");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "atividades"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Atividade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Atividade"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bem_opcionais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "bem_opcionais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Bem_Opcional"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Bem_Opcional"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cargos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "cargos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cargo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cargo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catalogo_erros");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "catalogo_erros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Catalogo_Erro"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Catalogo_Erro"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dirigentes_cargos_sub");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "dirigentes_cargos_sub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Dirigentes_Cargo_Sub"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Dirigentes_Cargo_Sub"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empresas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "empresas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Empresa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Empresa"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("escolaridades");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "escolaridades"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Escolaridade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Escolaridade"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estados_civil");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "estados_civil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Estado_Civil"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Estado_Civil"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faixas_patrimonio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "faixas_patrimonio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixas_Patrimonio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixas_Patrimonio"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imovel_especies");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "imovel_especies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Especie"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Especie"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imovel_tipos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "imovel_tipos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Tipo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Tipo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isencao_fiscal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "isencao_fiscal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Isencao_Fiscal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Isencao_Fiscal"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lojistas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "lojistas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Lojista"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Lojista"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moedas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "moedas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Moeda"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Moeda"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nacionalidades");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "nacionalidades"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Nacionalidade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Nacionalidade"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("naturezas_ocupacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "naturezas_ocupacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Natureza_Ocupacao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Natureza_Ocupacao"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pessoas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "pessoas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Pessoa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Pessoa"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "planos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Planos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Planos"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portes_empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "portes_empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Porte_Empresa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Porte_Empresa"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("produtos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "produtos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Produto"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Produto"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profissoes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "profissoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Profissao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Profissao"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramos_atividade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ramos_atividade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Ramo_Atividade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Ramo_Atividade"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registros_gravames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "registros_gravames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Registro_Gravame"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Registro_Gravame"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sedes_social");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "sedes_social"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sede_Social"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sede_Social"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentos_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "segmentos_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Segmento_Veiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Segmento_Veiculo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "servicos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Servico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Servico"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sexos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "sexos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sexo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sexo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_proposta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "status_proposta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "StatusProposta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "StatusProposta"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tarifa_avaliacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tarifa_avaliacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tarifa_Avaliacao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tarifa_Avaliacao"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_categoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_categoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Categoria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Categoria"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_combustivel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_combustivel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Combustivel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Combustivel"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_compromisso");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_compromisso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Compromisso"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Compromisso"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Documento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Documento"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_emprego");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_emprego"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Emprego"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Emprego"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endereco"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_endividamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_endividamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endividamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endividamento"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_fluxo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_fluxo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Fluxo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Fluxo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_residencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_residencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Residencia"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Residencia"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_seguro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_seguro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Seguro"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Seguro"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_sub_telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_sub_telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Sub_Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Sub_Telefone"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Telefone"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipos_veiculo_porte");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipos_veiculo_porte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo_Porte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo_Porte"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
