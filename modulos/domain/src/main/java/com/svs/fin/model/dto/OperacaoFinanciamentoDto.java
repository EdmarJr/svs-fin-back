package com.svs.fin.model.dto;

import com.svs.fin.integracaoItau.FinanciamentoOnlineItau;

import br.com.gruposaga.sdk.zflow.model.gen.FinanceOperationOption;

public class OperacaoFinanciamentoDto {

	private FinanceOperationOption opcaoFinanciamento;
	private FinanciamentoOnlineItau financiamentoItau;
	private String cpfCnpjCliente;

	public FinanceOperationOption getOpcaoFinanciamento() {
		return opcaoFinanciamento;
	}

	public void setOpcaoFinanciamento(FinanceOperationOption opcaoFinanciamento) {
		this.opcaoFinanciamento = opcaoFinanciamento;
	}

	public FinanciamentoOnlineItau getFinanciamentoItau() {
		return financiamentoItau;
	}

	public void setFinanciamentoItau(FinanciamentoOnlineItau financiamentoItau) {
		this.financiamentoItau = financiamentoItau;
	}

	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}

	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}

}
