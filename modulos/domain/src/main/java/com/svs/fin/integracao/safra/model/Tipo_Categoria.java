/**
 * Tipo_Categoria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Categoria")
public class Tipo_Categoria implements java.io.Serializable {
	private java.lang.String ds_tipo_categoria;

	@Id
	private java.lang.Integer id_tipo_categoria;

	public Tipo_Categoria() {
	}

	public Tipo_Categoria(java.lang.String ds_tipo_categoria, java.lang.Integer id_tipo_categoria) {
		this.ds_tipo_categoria = ds_tipo_categoria;
		this.id_tipo_categoria = id_tipo_categoria;
	}

	/**
	 * Gets the ds_tipo_categoria value for this Tipo_Categoria.
	 * 
	 * @return ds_tipo_categoria
	 */
	public java.lang.String getDs_tipo_categoria() {
		return ds_tipo_categoria;
	}

	/**
	 * Sets the ds_tipo_categoria value for this Tipo_Categoria.
	 * 
	 * @param ds_tipo_categoria
	 */
	public void setDs_tipo_categoria(java.lang.String ds_tipo_categoria) {
		this.ds_tipo_categoria = ds_tipo_categoria;
	}

	/**
	 * Gets the id_tipo_categoria value for this Tipo_Categoria.
	 * 
	 * @return id_tipo_categoria
	 */
	public java.lang.Integer getId_tipo_categoria() {
		return id_tipo_categoria;
	}

	/**
	 * Sets the id_tipo_categoria value for this Tipo_Categoria.
	 * 
	 * @param id_tipo_categoria
	 */
	public void setId_tipo_categoria(java.lang.Integer id_tipo_categoria) {
		this.id_tipo_categoria = id_tipo_categoria;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Categoria))
			return false;
		Tipo_Categoria other = (Tipo_Categoria) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.ds_tipo_categoria == null && other.getDs_tipo_categoria() == null)
				|| (this.ds_tipo_categoria != null && this.ds_tipo_categoria.equals(other.getDs_tipo_categoria())))
				&& ((this.id_tipo_categoria == null && other.getId_tipo_categoria() == null)
						|| (this.id_tipo_categoria != null
								&& this.id_tipo_categoria.equals(other.getId_tipo_categoria())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_categoria() != null) {
			_hashCode += getDs_tipo_categoria().hashCode();
		}
		if (getId_tipo_categoria() != null) {
			_hashCode += getId_tipo_categoria().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Categoria.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Categoria"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_categoria");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_tipo_categoria"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_categoria");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_tipo_categoria"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
