/**
 * Documento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Documento  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_orgao_exp;

    private java.lang.String dt_expedicao;

    private java.lang.String id_tipo_documento;

    private java.lang.String id_uf_expedidor;

    private java.lang.String nr_documento;

    public Documento() {
    }

    public Documento(
           java.lang.String ds_orgao_exp,
           java.lang.String dt_expedicao,
           java.lang.String id_tipo_documento,
           java.lang.String id_uf_expedidor,
           java.lang.String nr_documento) {
        this.ds_orgao_exp = ds_orgao_exp;
        this.dt_expedicao = dt_expedicao;
        this.id_tipo_documento = id_tipo_documento;
        this.id_uf_expedidor = id_uf_expedidor;
        this.nr_documento = nr_documento;
    }


    /**
     * Gets the ds_orgao_exp value for this Documento.
     * 
     * @return ds_orgao_exp
     */
    public java.lang.String getDs_orgao_exp() {
        return ds_orgao_exp;
    }


    /**
     * Sets the ds_orgao_exp value for this Documento.
     * 
     * @param ds_orgao_exp
     */
    public void setDs_orgao_exp(java.lang.String ds_orgao_exp) {
        this.ds_orgao_exp = ds_orgao_exp;
    }


    /**
     * Gets the dt_expedicao value for this Documento.
     * 
     * @return dt_expedicao
     */
    public java.lang.String getDt_expedicao() {
        return dt_expedicao;
    }


    /**
     * Sets the dt_expedicao value for this Documento.
     * 
     * @param dt_expedicao
     */
    public void setDt_expedicao(java.lang.String dt_expedicao) {
        this.dt_expedicao = dt_expedicao;
    }


    /**
     * Gets the id_tipo_documento value for this Documento.
     * 
     * @return id_tipo_documento
     */
    public java.lang.String getId_tipo_documento() {
        return id_tipo_documento;
    }


    /**
     * Sets the id_tipo_documento value for this Documento.
     * 
     * @param id_tipo_documento
     */
    public void setId_tipo_documento(java.lang.String id_tipo_documento) {
        this.id_tipo_documento = id_tipo_documento;
    }


    /**
     * Gets the id_uf_expedidor value for this Documento.
     * 
     * @return id_uf_expedidor
     */
    public java.lang.String getId_uf_expedidor() {
        return id_uf_expedidor;
    }


    /**
     * Sets the id_uf_expedidor value for this Documento.
     * 
     * @param id_uf_expedidor
     */
    public void setId_uf_expedidor(java.lang.String id_uf_expedidor) {
        this.id_uf_expedidor = id_uf_expedidor;
    }


    /**
     * Gets the nr_documento value for this Documento.
     * 
     * @return nr_documento
     */
    public java.lang.String getNr_documento() {
        return nr_documento;
    }


    /**
     * Sets the nr_documento value for this Documento.
     * 
     * @param nr_documento
     */
    public void setNr_documento(java.lang.String nr_documento) {
        this.nr_documento = nr_documento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Documento)) return false;
        Documento other = (Documento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_orgao_exp==null && other.getDs_orgao_exp()==null) || 
             (this.ds_orgao_exp!=null &&
              this.ds_orgao_exp.equals(other.getDs_orgao_exp()))) &&
            ((this.dt_expedicao==null && other.getDt_expedicao()==null) || 
             (this.dt_expedicao!=null &&
              this.dt_expedicao.equals(other.getDt_expedicao()))) &&
            ((this.id_tipo_documento==null && other.getId_tipo_documento()==null) || 
             (this.id_tipo_documento!=null &&
              this.id_tipo_documento.equals(other.getId_tipo_documento()))) &&
            ((this.id_uf_expedidor==null && other.getId_uf_expedidor()==null) || 
             (this.id_uf_expedidor!=null &&
              this.id_uf_expedidor.equals(other.getId_uf_expedidor()))) &&
            ((this.nr_documento==null && other.getNr_documento()==null) || 
             (this.nr_documento!=null &&
              this.nr_documento.equals(other.getNr_documento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_orgao_exp() != null) {
            _hashCode += getDs_orgao_exp().hashCode();
        }
        if (getDt_expedicao() != null) {
            _hashCode += getDt_expedicao().hashCode();
        }
        if (getId_tipo_documento() != null) {
            _hashCode += getId_tipo_documento().hashCode();
        }
        if (getId_uf_expedidor() != null) {
            _hashCode += getId_uf_expedidor().hashCode();
        }
        if (getNr_documento() != null) {
            _hashCode += getNr_documento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Documento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Documento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_orgao_exp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_orgao_exp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_expedicao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_expedicao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_tipo_documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_tipo_documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_uf_expedidor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_uf_expedidor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
