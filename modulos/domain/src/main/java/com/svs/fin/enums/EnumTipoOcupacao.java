package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoOcupacao {

	// AUTONOMO("Autônomo"),
	// ASSALARIADO("Assalariado"),
	// APOSENTADO_PENSIONISTA("Aposentado/pensionista"),
	// PROFISSIONAL_LIBERAL("Profissional liberal"),
	// SOCIO_PROPIETARIO("Sócio-propietario"),
	// SEM_ATIVIDADE_REMUNERADA("Sem atividade remunerada"),
	// FUNCIONARIO_PUBLICO("Funcionário publico");

	@JsonProperty("Autônomo")
	AUTONOMO("Autônomo"), @JsonProperty("Assalariado")
	ASSALARIADO("Assalariado"), @JsonProperty("Aposentado/pensionista")
	APOSENTADO_PENSIONISTA("Aposentado/pensionista"), @JsonProperty("Profissional liberal")
	PROFISSIONAL_LIBERAL("Profissional liberal"), @JsonProperty("Sócio-propietario")
	SOCIO_PROPIETARIO("Sócio-propietario"), @JsonProperty("Sem atividade remunerada")
	SEM_ATIVIDADE_REMUNERADA("Sem atividade remunerada"), @JsonProperty("Funcionário publico")
	FUNCIONARIO_PUBLICO("Funcionário publico");

	private String label;

	private EnumTipoOcupacao(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
