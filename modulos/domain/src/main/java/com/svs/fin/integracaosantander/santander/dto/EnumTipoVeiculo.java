package com.svs.fin.integracaosantander.santander.dto;

public enum EnumTipoVeiculo {
	
	CARROS_UTILITARIOS("Carros e Utilit�rios", "C"), MOTOS("Motos", "M");
	
	private EnumTipoVeiculo(String label, String id) {
		this.label = label;
		this.id = id;
	}
	
	private String label;
	private String id;
	
	public String getLabel() {
		return label;
	}
	
	public String getId() {
		return id;
	}
}
