package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = Telefone.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = Telefone.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
public class Telefone implements Serializable {

	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "Telefone.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from Telefone e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	private static final long serialVersionUID = -8372842756795291451L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TELEFONE")
	@SequenceGenerator(name = "SEQ_TELEFONE", sequenceName = "SEQ_TELEFONE", allocationSize = 1)
	private Long id;

	@Column(length = 10)
	private String ddd;

	@Column(length = 200)
	private String telefone;

	@ManyToOne
	@JoinColumn(name = "id_tipo_telefone")
	private TipoTelefone tipoTelefone;

	@ManyToOne
	@JoinColumn(name = "id_tipo_endereco")
	private TipoEndereco tipoEndereco;

	@Column
	private Long codigoDealerWorkflow;

	@Transient
	private Boolean editando;

	@Transient
	private Long tipoTelefoneCodigoDealerWorkflow;

	@Transient
	private Long tipoEnderecoCodigoDealerWorkflow;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	private ClienteSvs clienteSvs;

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	public String getNumeroFormatado() {
		return "(" + getDdd() + ") " + getTelefone();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefone other = (Telefone) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public TipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(TipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public Long getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(Long codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public Long getTipoTelefoneCodigoDealerWorkflow() {
		return tipoTelefoneCodigoDealerWorkflow;
	}

	public void setTipoTelefoneCodigoDealerWorkflow(Long tipoTelefoneCodigoDealerWorkflow) {
		this.tipoTelefoneCodigoDealerWorkflow = tipoTelefoneCodigoDealerWorkflow;
	}

	public Long getTipoEnderecoCodigoDealerWorkflow() {
		return tipoEnderecoCodigoDealerWorkflow;
	}

	public void setTipoEnderecoCodigoDealerWorkflow(Long tipoEnderecoCodigoDealerWorkflow) {
		this.tipoEnderecoCodigoDealerWorkflow = tipoEnderecoCodigoDealerWorkflow;
	}

}
