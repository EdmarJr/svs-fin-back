package com.svs.fin.integracaosantander.santander.dto;

public class StatusPropostaRequestDTO {

	private String channel;
	
	private String storeId;
	
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
}
