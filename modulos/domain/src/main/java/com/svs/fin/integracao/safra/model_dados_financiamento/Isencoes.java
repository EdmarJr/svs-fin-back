/**
 * Isencoes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_dados_financiamento;

public class Isencoes  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_isencao_fiscal;

    private java.lang.String isencao_fiscal;

    public Isencoes() {
    }

    public Isencoes(
           java.lang.String ds_isencao_fiscal,
           java.lang.String isencao_fiscal) {
        this.ds_isencao_fiscal = ds_isencao_fiscal;
        this.isencao_fiscal = isencao_fiscal;
    }


    /**
     * Gets the ds_isencao_fiscal value for this Isencoes.
     * 
     * @return ds_isencao_fiscal
     */
    public java.lang.String getDs_isencao_fiscal() {
        return ds_isencao_fiscal;
    }


    /**
     * Sets the ds_isencao_fiscal value for this Isencoes.
     * 
     * @param ds_isencao_fiscal
     */
    public void setDs_isencao_fiscal(java.lang.String ds_isencao_fiscal) {
        this.ds_isencao_fiscal = ds_isencao_fiscal;
    }


    /**
     * Gets the isencao_fiscal value for this Isencoes.
     * 
     * @return isencao_fiscal
     */
    public java.lang.String getIsencao_fiscal() {
        return isencao_fiscal;
    }


    /**
     * Sets the isencao_fiscal value for this Isencoes.
     * 
     * @param isencao_fiscal
     */
    public void setIsencao_fiscal(java.lang.String isencao_fiscal) {
        this.isencao_fiscal = isencao_fiscal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Isencoes)) return false;
        Isencoes other = (Isencoes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_isencao_fiscal==null && other.getDs_isencao_fiscal()==null) || 
             (this.ds_isencao_fiscal!=null &&
              this.ds_isencao_fiscal.equals(other.getDs_isencao_fiscal()))) &&
            ((this.isencao_fiscal==null && other.getIsencao_fiscal()==null) || 
             (this.isencao_fiscal!=null &&
              this.isencao_fiscal.equals(other.getIsencao_fiscal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_isencao_fiscal() != null) {
            _hashCode += getDs_isencao_fiscal().hashCode();
        }
        if (getIsencao_fiscal() != null) {
            _hashCode += getIsencao_fiscal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Isencoes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Isencoes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_isencao_fiscal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "ds_isencao_fiscal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isencao_fiscal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "isencao_fiscal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
