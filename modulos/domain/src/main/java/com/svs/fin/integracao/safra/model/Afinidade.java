/**
 * Afinidade.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Afinidade")
public class Afinidade implements java.io.Serializable {
	private java.lang.String ds_afinidade;

	@Id
	private java.lang.Integer id_afinidade;

	public Afinidade() {
	}

	public Afinidade(java.lang.String ds_afinidade, java.lang.Integer id_afinidade) {
		this.ds_afinidade = ds_afinidade;
		this.id_afinidade = id_afinidade;
	}

	/**
	 * Gets the ds_afinidade value for this Afinidade.
	 * 
	 * @return ds_afinidade
	 */
	public java.lang.String getDs_afinidade() {
		return ds_afinidade;
	}

	/**
	 * Sets the ds_afinidade value for this Afinidade.
	 * 
	 * @param ds_afinidade
	 */
	public void setDs_afinidade(java.lang.String ds_afinidade) {
		this.ds_afinidade = ds_afinidade;
	}

	/**
	 * Gets the id_afinidade value for this Afinidade.
	 * 
	 * @return id_afinidade
	 */
	public java.lang.Integer getId_afinidade() {
		return id_afinidade;
	}

	/**
	 * Sets the id_afinidade value for this Afinidade.
	 * 
	 * @param id_afinidade
	 */
	public void setId_afinidade(java.lang.Integer id_afinidade) {
		this.id_afinidade = id_afinidade;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Afinidade))
			return false;
		Afinidade other = (Afinidade) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_afinidade == null && other.getDs_afinidade() == null)
						|| (this.ds_afinidade != null && this.ds_afinidade.equals(other.getDs_afinidade())))
				&& ((this.id_afinidade == null && other.getId_afinidade() == null)
						|| (this.id_afinidade != null && this.id_afinidade.equals(other.getId_afinidade())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_afinidade() != null) {
			_hashCode += getDs_afinidade().hashCode();
		}
		if (getId_afinidade() != null) {
			_hashCode += getId_afinidade().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Afinidade.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Afinidade"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_afinidade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_afinidade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_afinidade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_afinidade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
