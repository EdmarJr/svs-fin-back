package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.SimNao;

@Entity(name = "BANCOS")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Banco implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7480563371579372445L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BANCO")
	@SequenceGenerator(name = "SEQ_BANCO", sequenceName = "SEQ_BANCO", allocationSize = 1)
	private Long id;

	@Column(nullable = false, length = 250)
	private String descBanco;

	@Column(nullable = true, length = 50)
	private String numero;

	@Column(nullable = true, length = 250)
	private String site;

	@Enumerated(EnumType.STRING)
	private SimNao ativo;

	public Long getId() {
		return id;
	}

	public String getDescBanco() {
		return descBanco;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDescBanco(String descBanco) {
		this.descBanco = descBanco;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof Banco) {
			Banco bc = (Banco) obj;
			if (Long.valueOf(bc.getId()).longValue() == Long.valueOf(getId()).longValue()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public SimNao getAtivo() {
		return ativo;
	}

	public void setAtivo(SimNao ativo) {
		this.ativo = ativo;
	}
}
