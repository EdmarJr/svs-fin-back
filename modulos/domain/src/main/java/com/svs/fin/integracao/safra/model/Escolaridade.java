/**
 * Escolaridade.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Escolaridade")
public class Escolaridade implements java.io.Serializable {
	private java.lang.String ds_escolaridade;

	@Id
	private java.lang.Integer id_escolaridade;

	public Escolaridade() {
	}

	public Escolaridade(java.lang.String ds_escolaridade, java.lang.Integer id_escolaridade) {
		this.ds_escolaridade = ds_escolaridade;
		this.id_escolaridade = id_escolaridade;
	}

	/**
	 * Gets the ds_escolaridade value for this Escolaridade.
	 * 
	 * @return ds_escolaridade
	 */
	public java.lang.String getDs_escolaridade() {
		return ds_escolaridade;
	}

	/**
	 * Sets the ds_escolaridade value for this Escolaridade.
	 * 
	 * @param ds_escolaridade
	 */
	public void setDs_escolaridade(java.lang.String ds_escolaridade) {
		this.ds_escolaridade = ds_escolaridade;
	}

	/**
	 * Gets the id_escolaridade value for this Escolaridade.
	 * 
	 * @return id_escolaridade
	 */
	public java.lang.Integer getId_escolaridade() {
		return id_escolaridade;
	}

	/**
	 * Sets the id_escolaridade value for this Escolaridade.
	 * 
	 * @param id_escolaridade
	 */
	public void setId_escolaridade(java.lang.Integer id_escolaridade) {
		this.id_escolaridade = id_escolaridade;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Escolaridade))
			return false;
		Escolaridade other = (Escolaridade) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_escolaridade == null && other.getDs_escolaridade() == null)
						|| (this.ds_escolaridade != null && this.ds_escolaridade.equals(other.getDs_escolaridade())))
				&& ((this.id_escolaridade == null && other.getId_escolaridade() == null)
						|| (this.id_escolaridade != null && this.id_escolaridade.equals(other.getId_escolaridade())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_escolaridade() != null) {
			_hashCode += getDs_escolaridade().hashCode();
		}
		if (getId_escolaridade() != null) {
			_hashCode += getId_escolaridade().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Escolaridade.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Escolaridade"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_escolaridade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_escolaridade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_escolaridade");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_escolaridade"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
