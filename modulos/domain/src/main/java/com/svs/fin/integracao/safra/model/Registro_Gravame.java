/**
 * Registro_Gravame.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Registro_Gravame")
public class Registro_Gravame implements java.io.Serializable {
	@Id
	private java.lang.String id_uf;

	private java.lang.Double vl_reg_ncobrado;

	private java.lang.Double vlr_registro;

	public Registro_Gravame() {
	}

	public Registro_Gravame(java.lang.String id_uf, java.lang.Double vl_reg_ncobrado, java.lang.Double vlr_registro) {
		this.id_uf = id_uf;
		this.vl_reg_ncobrado = vl_reg_ncobrado;
		this.vlr_registro = vlr_registro;
	}

	/**
	 * Gets the id_uf value for this Registro_Gravame.
	 * 
	 * @return id_uf
	 */
	public java.lang.String getId_uf() {
		return id_uf;
	}

	/**
	 * Sets the id_uf value for this Registro_Gravame.
	 * 
	 * @param id_uf
	 */
	public void setId_uf(java.lang.String id_uf) {
		this.id_uf = id_uf;
	}

	/**
	 * Gets the vl_reg_ncobrado value for this Registro_Gravame.
	 * 
	 * @return vl_reg_ncobrado
	 */
	public java.lang.Double getVl_reg_ncobrado() {
		return vl_reg_ncobrado;
	}

	/**
	 * Sets the vl_reg_ncobrado value for this Registro_Gravame.
	 * 
	 * @param vl_reg_ncobrado
	 */
	public void setVl_reg_ncobrado(java.lang.Double vl_reg_ncobrado) {
		this.vl_reg_ncobrado = vl_reg_ncobrado;
	}

	/**
	 * Gets the vlr_registro value for this Registro_Gravame.
	 * 
	 * @return vlr_registro
	 */
	public java.lang.Double getVlr_registro() {
		return vlr_registro;
	}

	/**
	 * Sets the vlr_registro value for this Registro_Gravame.
	 * 
	 * @param vlr_registro
	 */
	public void setVlr_registro(java.lang.Double vlr_registro) {
		this.vlr_registro = vlr_registro;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Registro_Gravame))
			return false;
		Registro_Gravame other = (Registro_Gravame) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.id_uf == null && other.getId_uf() == null)
						|| (this.id_uf != null && this.id_uf.equals(other.getId_uf())))
				&& ((this.vl_reg_ncobrado == null && other.getVl_reg_ncobrado() == null)
						|| (this.vl_reg_ncobrado != null && this.vl_reg_ncobrado.equals(other.getVl_reg_ncobrado())))
				&& ((this.vlr_registro == null && other.getVlr_registro() == null)
						|| (this.vlr_registro != null && this.vlr_registro.equals(other.getVlr_registro())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getId_uf() != null) {
			_hashCode += getId_uf().hashCode();
		}
		if (getVl_reg_ncobrado() != null) {
			_hashCode += getVl_reg_ncobrado().hashCode();
		}
		if (getVlr_registro() != null) {
			_hashCode += getVlr_registro().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Registro_Gravame.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Registro_Gravame"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_uf");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_uf"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_reg_ncobrado");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_reg_ncobrado"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlr_registro");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vlr_registro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
