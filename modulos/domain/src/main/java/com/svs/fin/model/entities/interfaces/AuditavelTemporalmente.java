package com.svs.fin.model.entities.interfaces;

public interface AuditavelTemporalmente extends AuditavelDataHoraDeAtualizacao, AuditavelDataHoraDeCriacao {

}
