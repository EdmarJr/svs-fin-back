package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;
import java.util.Calendar;

import com.svs.fin.integracaosantander.santander.dto.MarcaDTO;

public class IdentificationRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -107474773573418380L;

	private Store store;
	
	private MarcaDTO marca;

	private Vehicle vehicle;

	private String uuid;

	private Customer customer;

	private Calendar dataPrimeiraParcela;
	
	public Store getStore() {
		if(store == null) {
			store = new Store();
		}
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Vehicle getVehicle() {
		if(vehicle == null) {
			vehicle = new Vehicle();
		}
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Customer getCustomer() {
		if(customer == null) {
			customer = new Customer();
		}
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "ClassPojo [store = " + store + ", vehicle = " + vehicle + ", uuid = " + uuid + ", customer = "
				+ customer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentificationRequest other = (IdentificationRequest) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	public MarcaDTO getMarca() {
		return marca;
	}

	public void setMarca(MarcaDTO marca) {
		this.marca = marca;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Calendar getDataPrimeiraParcela() {
		return dataPrimeiraParcela;
	}

	public void setDataPrimeiraParcela(Calendar dataPrimeiraParcela) {
		this.dataPrimeiraParcela = dataPrimeiraParcela;
	}
}
