/**
 * Endereco.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Endereco  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_complemento;

    private java.lang.String id_uf_endereco;

    private java.lang.String nm_bairro;

    private java.lang.String nm_cidade;

    private java.lang.String nm_logradouro;

    private java.lang.String nr_numero;

    private java.lang.String sq_cep;

    public Endereco() {
    }

    public Endereco(
           java.lang.String ds_complemento,
           java.lang.String id_uf_endereco,
           java.lang.String nm_bairro,
           java.lang.String nm_cidade,
           java.lang.String nm_logradouro,
           java.lang.String nr_numero,
           java.lang.String sq_cep) {
        this.ds_complemento = ds_complemento;
        this.id_uf_endereco = id_uf_endereco;
        this.nm_bairro = nm_bairro;
        this.nm_cidade = nm_cidade;
        this.nm_logradouro = nm_logradouro;
        this.nr_numero = nr_numero;
        this.sq_cep = sq_cep;
    }


    /**
     * Gets the ds_complemento value for this Endereco.
     * 
     * @return ds_complemento
     */
    public java.lang.String getDs_complemento() {
        return ds_complemento;
    }


    /**
     * Sets the ds_complemento value for this Endereco.
     * 
     * @param ds_complemento
     */
    public void setDs_complemento(java.lang.String ds_complemento) {
        this.ds_complemento = ds_complemento;
    }


    /**
     * Gets the id_uf_endereco value for this Endereco.
     * 
     * @return id_uf_endereco
     */
    public java.lang.String getId_uf_endereco() {
        return id_uf_endereco;
    }


    /**
     * Sets the id_uf_endereco value for this Endereco.
     * 
     * @param id_uf_endereco
     */
    public void setId_uf_endereco(java.lang.String id_uf_endereco) {
        this.id_uf_endereco = id_uf_endereco;
    }


    /**
     * Gets the nm_bairro value for this Endereco.
     * 
     * @return nm_bairro
     */
    public java.lang.String getNm_bairro() {
        return nm_bairro;
    }


    /**
     * Sets the nm_bairro value for this Endereco.
     * 
     * @param nm_bairro
     */
    public void setNm_bairro(java.lang.String nm_bairro) {
        this.nm_bairro = nm_bairro;
    }


    /**
     * Gets the nm_cidade value for this Endereco.
     * 
     * @return nm_cidade
     */
    public java.lang.String getNm_cidade() {
        return nm_cidade;
    }


    /**
     * Sets the nm_cidade value for this Endereco.
     * 
     * @param nm_cidade
     */
    public void setNm_cidade(java.lang.String nm_cidade) {
        this.nm_cidade = nm_cidade;
    }


    /**
     * Gets the nm_logradouro value for this Endereco.
     * 
     * @return nm_logradouro
     */
    public java.lang.String getNm_logradouro() {
        return nm_logradouro;
    }


    /**
     * Sets the nm_logradouro value for this Endereco.
     * 
     * @param nm_logradouro
     */
    public void setNm_logradouro(java.lang.String nm_logradouro) {
        this.nm_logradouro = nm_logradouro;
    }


    /**
     * Gets the nr_numero value for this Endereco.
     * 
     * @return nr_numero
     */
    public java.lang.String getNr_numero() {
        return nr_numero;
    }


    /**
     * Sets the nr_numero value for this Endereco.
     * 
     * @param nr_numero
     */
    public void setNr_numero(java.lang.String nr_numero) {
        this.nr_numero = nr_numero;
    }


    /**
     * Gets the sq_cep value for this Endereco.
     * 
     * @return sq_cep
     */
    public java.lang.String getSq_cep() {
        return sq_cep;
    }


    /**
     * Sets the sq_cep value for this Endereco.
     * 
     * @param sq_cep
     */
    public void setSq_cep(java.lang.String sq_cep) {
        this.sq_cep = sq_cep;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Endereco)) return false;
        Endereco other = (Endereco) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_complemento==null && other.getDs_complemento()==null) || 
             (this.ds_complemento!=null &&
              this.ds_complemento.equals(other.getDs_complemento()))) &&
            ((this.id_uf_endereco==null && other.getId_uf_endereco()==null) || 
             (this.id_uf_endereco!=null &&
              this.id_uf_endereco.equals(other.getId_uf_endereco()))) &&
            ((this.nm_bairro==null && other.getNm_bairro()==null) || 
             (this.nm_bairro!=null &&
              this.nm_bairro.equals(other.getNm_bairro()))) &&
            ((this.nm_cidade==null && other.getNm_cidade()==null) || 
             (this.nm_cidade!=null &&
              this.nm_cidade.equals(other.getNm_cidade()))) &&
            ((this.nm_logradouro==null && other.getNm_logradouro()==null) || 
             (this.nm_logradouro!=null &&
              this.nm_logradouro.equals(other.getNm_logradouro()))) &&
            ((this.nr_numero==null && other.getNr_numero()==null) || 
             (this.nr_numero!=null &&
              this.nr_numero.equals(other.getNr_numero()))) &&
            ((this.sq_cep==null && other.getSq_cep()==null) || 
             (this.sq_cep!=null &&
              this.sq_cep.equals(other.getSq_cep())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_complemento() != null) {
            _hashCode += getDs_complemento().hashCode();
        }
        if (getId_uf_endereco() != null) {
            _hashCode += getId_uf_endereco().hashCode();
        }
        if (getNm_bairro() != null) {
            _hashCode += getNm_bairro().hashCode();
        }
        if (getNm_cidade() != null) {
            _hashCode += getNm_cidade().hashCode();
        }
        if (getNm_logradouro() != null) {
            _hashCode += getNm_logradouro().hashCode();
        }
        if (getNr_numero() != null) {
            _hashCode += getNr_numero().hashCode();
        }
        if (getSq_cep() != null) {
            _hashCode += getSq_cep().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Endereco.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endereco"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_complemento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_complemento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_uf_endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_uf_endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_bairro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_bairro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_cidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_cidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_logradouro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_logradouro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_numero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sq_cep");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "sq_cep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
