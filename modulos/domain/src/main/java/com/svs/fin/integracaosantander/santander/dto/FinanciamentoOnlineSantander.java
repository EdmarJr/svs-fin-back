package com.svs.fin.integracaosantander.santander.dto;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.svs.fin.model.entities.CalculoRentabilidade;

@Entity
public class FinanciamentoOnlineSantander implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7158687109473905378L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_calculo_rentabilidade", referencedColumnName = "id")
	private CalculoRentabilidade calculoRentabilidade;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_req_id_santander")
	private JsonRequisicaoIdentificationSantander jsonRequisicaoIdentificationSantander;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_resp_prean_santander")
	private JsonRespostaPreAnaliseSantander jsonRespostaPreAnaliseSantander;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_simulacao_santander")
	private JsonRequisicaoSimulacaoSantander jsonRequisicaoSimulacaoSantander;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_resp_sim_santander")
	private JsonRespostaSimulacaoSantander jsonRespostaSimulacaoSantander;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_req_prop_santander")
	private JsonRequisicaoProposta jsonRequisicaoProposta;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_json_resp_prop_santander")
	private JsonRespostaProposta jsonRespostaProposta;

	@Column(length = 100)
	private String status;

	@Lob
	private byte[] pdfCet;

	public JsonRequisicaoProposta getJsonRequisicaoProposta() {
		if (jsonRequisicaoProposta == null) {
			jsonRequisicaoProposta = new JsonRequisicaoProposta();
		}
		return jsonRequisicaoProposta;
	}

	public void setJsonRequisicaoProposta(JsonRequisicaoProposta jsonRequisicaoProposta) {
		this.jsonRequisicaoProposta = jsonRequisicaoProposta;
	}

	public JsonRespostaProposta getJsonRespostaProposta() {
		if (jsonRespostaProposta == null) {
			jsonRespostaProposta = new JsonRespostaProposta();
		}
		return jsonRespostaProposta;
	}

	public void setJsonRespostaProposta(JsonRespostaProposta jsonRespostaProposta) {
		this.jsonRespostaProposta = jsonRespostaProposta;
	}

	public JsonRequisicaoIdentificationSantander getJsonRequisicaoIdentificationSantander() {
		if (jsonRequisicaoIdentificationSantander == null) {
			jsonRequisicaoIdentificationSantander = new JsonRequisicaoIdentificationSantander();
		}
		return jsonRequisicaoIdentificationSantander;
	}

	public void setJsonRequisicaoIdentificationSantander(
			JsonRequisicaoIdentificationSantander jsonRequisicaoIdentificationSantander) {
		this.jsonRequisicaoIdentificationSantander = jsonRequisicaoIdentificationSantander;
	}

	public JsonRespostaPreAnaliseSantander getJsonRespostaPreAnaliseSantander() {
		return jsonRespostaPreAnaliseSantander;
	}

	public void setJsonRespostaPreAnaliseSantander(JsonRespostaPreAnaliseSantander jsonRespostaPreAnaliseSantander) {
		this.jsonRespostaPreAnaliseSantander = jsonRespostaPreAnaliseSantander;
	}

	public JsonRequisicaoSimulacaoSantander getJsonRequisicaoSimulacaoSantander() {
		if (jsonRequisicaoSimulacaoSantander == null) {
			jsonRequisicaoSimulacaoSantander = new JsonRequisicaoSimulacaoSantander();
		}
		return jsonRequisicaoSimulacaoSantander;
	}

	public void setJsonRequisicaoSimulacaoSantander(JsonRequisicaoSimulacaoSantander jsonRequisicaoSimulacaoSantander) {
		this.jsonRequisicaoSimulacaoSantander = jsonRequisicaoSimulacaoSantander;
	}

	public JsonRespostaSimulacaoSantander getJsonRespostaSimulacaoSantander() {
		if (jsonRespostaSimulacaoSantander == null) {
			jsonRespostaSimulacaoSantander = new JsonRespostaSimulacaoSantander();
		}
		return jsonRespostaSimulacaoSantander;
	}

	public void setJsonRespostaSimulacaoSantander(JsonRespostaSimulacaoSantander jsonRespostaSimulacaoSantander) {
		this.jsonRespostaSimulacaoSantander = jsonRespostaSimulacaoSantander;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getPdfCet() {
		return pdfCet;
	}

	public void setPdfCet(byte[] pdfCet) {
		this.pdfCet = pdfCet;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CalculoRentabilidade getCalculoRentabilidade() {
		return calculoRentabilidade;
	}

	public void setCalculoRentabilidade(CalculoRentabilidade calculoRentabilidade) {
		this.calculoRentabilidade = calculoRentabilidade;
	}
}
