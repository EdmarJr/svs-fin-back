package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoAvalista {
	
//	AVALISTA_DE_CONFORTO("Avalista de conforto"),
//	AVALISTA_DE_PORTE("Avalista de porte");
	
	@JsonProperty("Avalista de conforto")
	AVALISTA_DE_CONFORTO("Avalista de conforto"),
		@JsonProperty("Avalista de porte")
	AVALISTA_DE_PORTE("Avalista de porte");
	private String label;
	
	EnumTipoAvalista(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
