package com.svs.fin.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.svs.fin.model.entities.interfaces.AuditavelResponsabilizacao;
import com.svs.fin.model.entities.interfaces.AuditavelTemporalmente;
import com.svs.fin.model.entities.interfaces.Desativavel;
import com.svs.fin.model.entities.interfaces.Identificavel;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisResponsabilizacao;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisTemporalmente;

@Entity
@Table(name = "produtofinanceiro")
@EntityListeners({ ListenerEntidadesAuditaveisResponsabilizacao.class, ListenerEntidadesAuditaveisTemporalmente.class })
public class ProdutoFinanceiro
		implements Desativavel, Identificavel<Long>, AuditavelTemporalmente, AuditavelResponsabilizacao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PRODUTO_FINANCEIRO")
	@SequenceGenerator(name = "SEQ_PRODUTO_FINANCEIRO", sequenceName = "SEQ_PRODUTO_FINANCEIRO", allocationSize = 1)
	private Long id;
	@Column(name = "ativo")
	private Boolean ativo;
	@Column(name = "nome")
	private String nome;
	@Column(name = "alteradoem")
	private LocalDateTime dataHoraDeAtualizacao;
	@Column(name = "criadoem")
	private LocalDateTime dataHoraDeCriacao;

	@ManyToOne
	@JoinColumn(name = "id_usuario_criacao_alteracao")
	private Usuario usuarioResponsavelUltimaAlteracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDateTime getDataHoraDeAtualizacao() {
		return dataHoraDeAtualizacao;
	}

	@JsonIgnore
	public void setDataHoraDeAtualizacao(LocalDateTime dataHoraDeAtualizacao) {
		this.dataHoraDeAtualizacao = dataHoraDeAtualizacao;
	}

	public LocalDateTime getDataHoraDeCriacao() {
		return dataHoraDeCriacao;
	}

	@JsonIgnore
	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao) {
		this.dataHoraDeCriacao = dataHoraDeCriacao;
	}

	public Usuario getUsuarioResponsavelUltimaAlteracao() {
		return usuarioResponsavelUltimaAlteracao;
	}

	public void setUsuarioResponsavelUltimaAlteracao(Usuario usuarioResponsavelUltimaAlteracao) {
		this.usuarioResponsavelUltimaAlteracao = usuarioResponsavelUltimaAlteracao;
	}

}
