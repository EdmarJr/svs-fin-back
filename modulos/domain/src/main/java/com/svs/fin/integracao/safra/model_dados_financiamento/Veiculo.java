/**
 * Veiculo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_dados_financiamento;

public class Veiculo  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ano_veiculo;

    private java.lang.String cd_renavam;

    private java.lang.String ds_cor;

    private java.lang.String ds_placa;

    private java.lang.String ls_opcionais;

    private java.lang.String nm_municipio;

    private java.lang.String nr_chassi;

    public Veiculo() {
    }

    public Veiculo(
           java.lang.String ano_veiculo,
           java.lang.String cd_renavam,
           java.lang.String ds_cor,
           java.lang.String ds_placa,
           java.lang.String ls_opcionais,
           java.lang.String nm_municipio,
           java.lang.String nr_chassi) {
        this.ano_veiculo = ano_veiculo;
        this.cd_renavam = cd_renavam;
        this.ds_cor = ds_cor;
        this.ds_placa = ds_placa;
        this.ls_opcionais = ls_opcionais;
        this.nm_municipio = nm_municipio;
        this.nr_chassi = nr_chassi;
    }


    /**
     * Gets the ano_veiculo value for this Veiculo.
     * 
     * @return ano_veiculo
     */
    public java.lang.String getAno_veiculo() {
        return ano_veiculo;
    }


    /**
     * Sets the ano_veiculo value for this Veiculo.
     * 
     * @param ano_veiculo
     */
    public void setAno_veiculo(java.lang.String ano_veiculo) {
        this.ano_veiculo = ano_veiculo;
    }


    /**
     * Gets the cd_renavam value for this Veiculo.
     * 
     * @return cd_renavam
     */
    public java.lang.String getCd_renavam() {
        return cd_renavam;
    }


    /**
     * Sets the cd_renavam value for this Veiculo.
     * 
     * @param cd_renavam
     */
    public void setCd_renavam(java.lang.String cd_renavam) {
        this.cd_renavam = cd_renavam;
    }


    /**
     * Gets the ds_cor value for this Veiculo.
     * 
     * @return ds_cor
     */
    public java.lang.String getDs_cor() {
        return ds_cor;
    }


    /**
     * Sets the ds_cor value for this Veiculo.
     * 
     * @param ds_cor
     */
    public void setDs_cor(java.lang.String ds_cor) {
        this.ds_cor = ds_cor;
    }


    /**
     * Gets the ds_placa value for this Veiculo.
     * 
     * @return ds_placa
     */
    public java.lang.String getDs_placa() {
        return ds_placa;
    }


    /**
     * Sets the ds_placa value for this Veiculo.
     * 
     * @param ds_placa
     */
    public void setDs_placa(java.lang.String ds_placa) {
        this.ds_placa = ds_placa;
    }


    /**
     * Gets the ls_opcionais value for this Veiculo.
     * 
     * @return ls_opcionais
     */
    public java.lang.String getLs_opcionais() {
        return ls_opcionais;
    }


    /**
     * Sets the ls_opcionais value for this Veiculo.
     * 
     * @param ls_opcionais
     */
    public void setLs_opcionais(java.lang.String ls_opcionais) {
        this.ls_opcionais = ls_opcionais;
    }


    /**
     * Gets the nm_municipio value for this Veiculo.
     * 
     * @return nm_municipio
     */
    public java.lang.String getNm_municipio() {
        return nm_municipio;
    }


    /**
     * Sets the nm_municipio value for this Veiculo.
     * 
     * @param nm_municipio
     */
    public void setNm_municipio(java.lang.String nm_municipio) {
        this.nm_municipio = nm_municipio;
    }


    /**
     * Gets the nr_chassi value for this Veiculo.
     * 
     * @return nr_chassi
     */
    public java.lang.String getNr_chassi() {
        return nr_chassi;
    }


    /**
     * Sets the nr_chassi value for this Veiculo.
     * 
     * @param nr_chassi
     */
    public void setNr_chassi(java.lang.String nr_chassi) {
        this.nr_chassi = nr_chassi;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Veiculo)) return false;
        Veiculo other = (Veiculo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ano_veiculo==null && other.getAno_veiculo()==null) || 
             (this.ano_veiculo!=null &&
              this.ano_veiculo.equals(other.getAno_veiculo()))) &&
            ((this.cd_renavam==null && other.getCd_renavam()==null) || 
             (this.cd_renavam!=null &&
              this.cd_renavam.equals(other.getCd_renavam()))) &&
            ((this.ds_cor==null && other.getDs_cor()==null) || 
             (this.ds_cor!=null &&
              this.ds_cor.equals(other.getDs_cor()))) &&
            ((this.ds_placa==null && other.getDs_placa()==null) || 
             (this.ds_placa!=null &&
              this.ds_placa.equals(other.getDs_placa()))) &&
            ((this.ls_opcionais==null && other.getLs_opcionais()==null) || 
             (this.ls_opcionais!=null &&
              this.ls_opcionais.equals(other.getLs_opcionais()))) &&
            ((this.nm_municipio==null && other.getNm_municipio()==null) || 
             (this.nm_municipio!=null &&
              this.nm_municipio.equals(other.getNm_municipio()))) &&
            ((this.nr_chassi==null && other.getNr_chassi()==null) || 
             (this.nr_chassi!=null &&
              this.nr_chassi.equals(other.getNr_chassi())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAno_veiculo() != null) {
            _hashCode += getAno_veiculo().hashCode();
        }
        if (getCd_renavam() != null) {
            _hashCode += getCd_renavam().hashCode();
        }
        if (getDs_cor() != null) {
            _hashCode += getDs_cor().hashCode();
        }
        if (getDs_placa() != null) {
            _hashCode += getDs_placa().hashCode();
        }
        if (getLs_opcionais() != null) {
            _hashCode += getLs_opcionais().hashCode();
        }
        if (getNm_municipio() != null) {
            _hashCode += getNm_municipio().hashCode();
        }
        if (getNr_chassi() != null) {
            _hashCode += getNr_chassi().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Veiculo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Veiculo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ano_veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "ano_veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cd_renavam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "cd_renavam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_cor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "ds_cor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_placa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "ds_placa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ls_opcionais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "ls_opcionais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_municipio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "nm_municipio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_chassi");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "nr_chassi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
