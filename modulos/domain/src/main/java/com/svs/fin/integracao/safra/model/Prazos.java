/**
 * Prazos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class Prazos  implements java.io.Serializable {
    private java.lang.Integer id_prazo;

    private java.lang.Float vl_taxa;

    public Prazos() {
    }

    public Prazos(
           java.lang.Integer id_prazo,
           java.lang.Float vl_taxa) {
           this.id_prazo = id_prazo;
           this.vl_taxa = vl_taxa;
    }


    /**
     * Gets the id_prazo value for this Prazos.
     * 
     * @return id_prazo
     */
    public java.lang.Integer getId_prazo() {
        return id_prazo;
    }


    /**
     * Sets the id_prazo value for this Prazos.
     * 
     * @param id_prazo
     */
    public void setId_prazo(java.lang.Integer id_prazo) {
        this.id_prazo = id_prazo;
    }


    /**
     * Gets the vl_taxa value for this Prazos.
     * 
     * @return vl_taxa
     */
    public java.lang.Float getVl_taxa() {
        return vl_taxa;
    }


    /**
     * Sets the vl_taxa value for this Prazos.
     * 
     * @param vl_taxa
     */
    public void setVl_taxa(java.lang.Float vl_taxa) {
        this.vl_taxa = vl_taxa;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Prazos)) return false;
        Prazos other = (Prazos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id_prazo==null && other.getId_prazo()==null) || 
             (this.id_prazo!=null &&
              this.id_prazo.equals(other.getId_prazo()))) &&
            ((this.vl_taxa==null && other.getVl_taxa()==null) || 
             (this.vl_taxa!=null &&
              this.vl_taxa.equals(other.getVl_taxa())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId_prazo() != null) {
            _hashCode += getId_prazo().hashCode();
        }
        if (getVl_taxa() != null) {
            _hashCode += getVl_taxa().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Prazos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Prazos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_prazo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_prazo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_taxa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_taxa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
