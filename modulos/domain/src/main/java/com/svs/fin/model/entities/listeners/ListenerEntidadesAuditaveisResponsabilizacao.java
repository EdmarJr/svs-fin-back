package com.svs.fin.model.entities.listeners;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.svs.fin.model.entities.interfaces.AuditavelTemporalmente;

public class ListenerEntidadesAuditaveisResponsabilizacao {

	@PrePersist
	@PreUpdate
	public void definirUsuarioLogado(AuditavelTemporalmente entidade) {

	}
}
