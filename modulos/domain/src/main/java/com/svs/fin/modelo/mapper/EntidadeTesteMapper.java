package com.svs.fin.modelo.mapper;


import com.svs.fin.model.dto.EntidadeTesteDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import teste.EntidadeTeste;

import java.util.List;

/**
 * @author Ítalo Gustavo
 */
@Mapper
public interface EntidadeTesteMapper {

	EntidadeTesteMapper INSTANCE = Mappers.getMapper(EntidadeTesteMapper.class);

	EntidadeTesteDto paraDto(EntidadeTeste source);
	
	EntidadeTeste doDto(EntidadeTesteDto source);
	
	List<EntidadeTesteDto> paraDto(List<EntidadeTeste> source);

	void atualizar(EntidadeTesteDto source, @MappingTarget EntidadeTeste target);
}
