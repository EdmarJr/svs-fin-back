/**
 * Tipo_Compromisso.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Compromisso")
public class Tipo_Compromisso implements java.io.Serializable {
	private java.lang.String ds_compromisso;

	@Id
	private java.lang.Integer id_compromisso;

	public Tipo_Compromisso() {
	}

	public Tipo_Compromisso(java.lang.String ds_compromisso, java.lang.Integer id_compromisso) {
		this.ds_compromisso = ds_compromisso;
		this.id_compromisso = id_compromisso;
	}

	/**
	 * Gets the ds_compromisso value for this Tipo_Compromisso.
	 * 
	 * @return ds_compromisso
	 */
	public java.lang.String getDs_compromisso() {
		return ds_compromisso;
	}

	/**
	 * Sets the ds_compromisso value for this Tipo_Compromisso.
	 * 
	 * @param ds_compromisso
	 */
	public void setDs_compromisso(java.lang.String ds_compromisso) {
		this.ds_compromisso = ds_compromisso;
	}

	/**
	 * Gets the id_compromisso value for this Tipo_Compromisso.
	 * 
	 * @return id_compromisso
	 */
	public java.lang.Integer getId_compromisso() {
		return id_compromisso;
	}

	/**
	 * Sets the id_compromisso value for this Tipo_Compromisso.
	 * 
	 * @param id_compromisso
	 */
	public void setId_compromisso(java.lang.Integer id_compromisso) {
		this.id_compromisso = id_compromisso;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Compromisso))
			return false;
		Tipo_Compromisso other = (Tipo_Compromisso) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_compromisso == null && other.getDs_compromisso() == null)
						|| (this.ds_compromisso != null && this.ds_compromisso.equals(other.getDs_compromisso())))
				&& ((this.id_compromisso == null && other.getId_compromisso() == null)
						|| (this.id_compromisso != null && this.id_compromisso.equals(other.getId_compromisso())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_compromisso() != null) {
			_hashCode += getDs_compromisso().hashCode();
		}
		if (getId_compromisso() != null) {
			_hashCode += getId_compromisso().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Compromisso.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Compromisso"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_compromisso");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_compromisso"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_compromisso");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_compromisso"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
