/**
 * Segmento_Veiculo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Segmento_Veiculo")
public class Segmento_Veiculo implements java.io.Serializable {
	private java.lang.String ds_segmento_veiculo;

	@Id
	private java.lang.Integer id_segmento_veiculo;

	public Segmento_Veiculo() {
	}

	public Segmento_Veiculo(java.lang.String ds_segmento_veiculo, java.lang.Integer id_segmento_veiculo) {
		this.ds_segmento_veiculo = ds_segmento_veiculo;
		this.id_segmento_veiculo = id_segmento_veiculo;
	}

	/**
	 * Gets the ds_segmento_veiculo value for this Segmento_Veiculo.
	 * 
	 * @return ds_segmento_veiculo
	 */
	public java.lang.String getDs_segmento_veiculo() {
		return ds_segmento_veiculo;
	}

	/**
	 * Sets the ds_segmento_veiculo value for this Segmento_Veiculo.
	 * 
	 * @param ds_segmento_veiculo
	 */
	public void setDs_segmento_veiculo(java.lang.String ds_segmento_veiculo) {
		this.ds_segmento_veiculo = ds_segmento_veiculo;
	}

	/**
	 * Gets the id_segmento_veiculo value for this Segmento_Veiculo.
	 * 
	 * @return id_segmento_veiculo
	 */
	public java.lang.Integer getId_segmento_veiculo() {
		return id_segmento_veiculo;
	}

	/**
	 * Sets the id_segmento_veiculo value for this Segmento_Veiculo.
	 * 
	 * @param id_segmento_veiculo
	 */
	public void setId_segmento_veiculo(java.lang.Integer id_segmento_veiculo) {
		this.id_segmento_veiculo = id_segmento_veiculo;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Segmento_Veiculo))
			return false;
		Segmento_Veiculo other = (Segmento_Veiculo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_segmento_veiculo == null && other.getDs_segmento_veiculo() == null)
						|| (this.ds_segmento_veiculo != null
								&& this.ds_segmento_veiculo.equals(other.getDs_segmento_veiculo())))
				&& ((this.id_segmento_veiculo == null && other.getId_segmento_veiculo() == null)
						|| (this.id_segmento_veiculo != null
								&& this.id_segmento_veiculo.equals(other.getId_segmento_veiculo())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_segmento_veiculo() != null) {
			_hashCode += getDs_segmento_veiculo().hashCode();
		}
		if (getId_segmento_veiculo() != null) {
			_hashCode += getId_segmento_veiculo().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Segmento_Veiculo.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Segmento_Veiculo"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_segmento_veiculo");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_segmento_veiculo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_segmento_veiculo");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_segmento_veiculo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
