/**
 * StatusProposta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_StatusProposta")
public class StatusProposta implements java.io.Serializable {
	private java.lang.String ds_status;

	@Id
	private java.lang.String id_status;

	public StatusProposta() {
	}

	public StatusProposta(java.lang.String ds_status, java.lang.String id_status) {
		this.ds_status = ds_status;
		this.id_status = id_status;
	}

	/**
	 * Gets the ds_status value for this StatusProposta.
	 * 
	 * @return ds_status
	 */
	public java.lang.String getDs_status() {
		return ds_status;
	}

	/**
	 * Sets the ds_status value for this StatusProposta.
	 * 
	 * @param ds_status
	 */
	public void setDs_status(java.lang.String ds_status) {
		this.ds_status = ds_status;
	}

	/**
	 * Gets the id_status value for this StatusProposta.
	 * 
	 * @return id_status
	 */
	public java.lang.String getId_status() {
		return id_status;
	}

	/**
	 * Sets the id_status value for this StatusProposta.
	 * 
	 * @param id_status
	 */
	public void setId_status(java.lang.String id_status) {
		this.id_status = id_status;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof StatusProposta))
			return false;
		StatusProposta other = (StatusProposta) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_status == null && other.getDs_status() == null)
						|| (this.ds_status != null && this.ds_status.equals(other.getDs_status())))
				&& ((this.id_status == null && other.getId_status() == null)
						|| (this.id_status != null && this.id_status.equals(other.getId_status())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_status() != null) {
			_hashCode += getDs_status().hashCode();
		}
		if (getId_status() != null) {
			_hashCode += getId_status().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			StatusProposta.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "StatusProposta"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_status");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_status"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_status");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_status"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
