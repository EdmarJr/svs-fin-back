package com.svs.fin.integracaosantander.santander;

public class Cellphone {

	private String ddd;

	private String number;
	
	public Cellphone() {}
	
	public Cellphone(String ddd, String number) {
		this.ddd = ddd;
		this.number = number;
	}
	
	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "ClassPojo [ddd = " + ddd + ", number = " + number + "]";
	}
}
