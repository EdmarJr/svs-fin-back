package com.svs.fin.model.dto;

import io.swagger.annotations.ApiModel;

import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "Parâmetros para contagem de registros")
public class ListCountParamsDto {
	
	List<ParamFilterListDto> filters;
	
	public List<ParamFilterListDto> getFilters() {
		if(filters == null) {
			filters = new ArrayList<ParamFilterListDto>();
		}
		return filters;
	}

	public void setFilters(List<ParamFilterListDto> filters) {
		this.filters = filters;
	}

}
