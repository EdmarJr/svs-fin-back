package com.svs.fin.integracaosantander.santander;

public class Guarantor {

	private String dateOfBirth;

	private String occupation;

	private String documentState;

	private String patrimony;

	private String monthlyIncome;

	private String maritalStatus;

	private String birthCity;

	private String birthState;

	private String issuingBodyDocument;

	private String document;

	private String nationality;

	private String participationType;

	private String mother;

	private Address address;

	private String email;

	private Spouse spouse;

	private Cellphone cellphone;

	private String name;

	private String documentNumber;

	private String gender;

	private String dateOfIssue;

	private String documentId;

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getDocumentState() {
		return documentState;
	}

	public void setDocumentState(String documentState) {
		this.documentState = documentState;
	}

	public String getPatrimony() {
		return patrimony;
	}

	public void setPatrimony(String patrimony) {
		this.patrimony = patrimony;
	}

	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getBirthCity() {
		return birthCity;
	}

	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}

	public String getBirthState() {
		return birthState;
	}

	public void setBirthState(String birthState) {
		this.birthState = birthState;
	}

	public String getIssuingBodyDocument() {
		return issuingBodyDocument;
	}

	public void setIssuingBodyDocument(String issuingBodyDocument) {
		this.issuingBodyDocument = issuingBodyDocument;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getParticipationType() {
		return participationType;
	}

	public void setParticipationType(String participationType) {
		this.participationType = participationType;
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Spouse getSpouse() {
		return spouse;
	}

	public void setSpouse(Spouse spouse) {
		this.spouse = spouse;
	}

	public Cellphone getCellphone() {
		return cellphone;
	}

	public void setCellphone(Cellphone cellphone) {
		this.cellphone = cellphone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	@Override
	public String toString() {
		return "ClassPojo [dateOfBirth = " + dateOfBirth + ", occupation = " + occupation + ", documentState = "
				+ documentState + ", patrimony = " + patrimony + ", monthlyIncome = " + monthlyIncome
				+ ", maritalStatus = " + maritalStatus + ", birthCity = " + birthCity + ", birthState = " + birthState
				+ ", issuingBodyDocument = " + issuingBodyDocument + ", document = " + document + ", nationality = "
				+ nationality + ", participationType = " + participationType + ", mother = " + mother + ", address = "
				+ address + ", email = " + email + ", spouse = " + spouse + ", cellphone = " + cellphone + ", name = "
				+ name + ", documentNumber = " + documentNumber + ", gender = " + gender + ", dateOfIssue = "
				+ dateOfIssue + ", documentId = " + documentId + "]";
	}

}
