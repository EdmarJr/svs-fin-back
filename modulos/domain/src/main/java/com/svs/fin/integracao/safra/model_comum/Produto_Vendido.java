/**
 * Produto_Vendido.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Produto_Vendido  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_produto;

    private java.lang.String pc_produto_vendas;

    public Produto_Vendido() {
    }

    public Produto_Vendido(
           java.lang.String ds_produto,
           java.lang.String pc_produto_vendas) {
        this.ds_produto = ds_produto;
        this.pc_produto_vendas = pc_produto_vendas;
    }


    /**
     * Gets the ds_produto value for this Produto_Vendido.
     * 
     * @return ds_produto
     */
    public java.lang.String getDs_produto() {
        return ds_produto;
    }


    /**
     * Sets the ds_produto value for this Produto_Vendido.
     * 
     * @param ds_produto
     */
    public void setDs_produto(java.lang.String ds_produto) {
        this.ds_produto = ds_produto;
    }


    /**
     * Gets the pc_produto_vendas value for this Produto_Vendido.
     * 
     * @return pc_produto_vendas
     */
    public java.lang.String getPc_produto_vendas() {
        return pc_produto_vendas;
    }


    /**
     * Sets the pc_produto_vendas value for this Produto_Vendido.
     * 
     * @param pc_produto_vendas
     */
    public void setPc_produto_vendas(java.lang.String pc_produto_vendas) {
        this.pc_produto_vendas = pc_produto_vendas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Produto_Vendido)) return false;
        Produto_Vendido other = (Produto_Vendido) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_produto==null && other.getDs_produto()==null) || 
             (this.ds_produto!=null &&
              this.ds_produto.equals(other.getDs_produto()))) &&
            ((this.pc_produto_vendas==null && other.getPc_produto_vendas()==null) || 
             (this.pc_produto_vendas!=null &&
              this.pc_produto_vendas.equals(other.getPc_produto_vendas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_produto() != null) {
            _hashCode += getDs_produto().hashCode();
        }
        if (getPc_produto_vendas() != null) {
            _hashCode += getPc_produto_vendas().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Produto_Vendido.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Produto_Vendido"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_produto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_produto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_produto_vendas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "pc_produto_vendas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
