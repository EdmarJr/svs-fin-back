package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class FleetOptions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1483203602104999992L;

	private Fleet[] fleets;

	private String recommendedFleet;

	public Fleet[] getFleets() {
		return fleets;
	}

	public void setFleets(Fleet[] fleets) {
		this.fleets = fleets;
	}

	public String getRecommendedFleet() {
		return recommendedFleet;
	}

	public void setRecommendedFleet(String recommendedFleet) {
		this.recommendedFleet = recommendedFleet;
	}

	@Override
	public String toString() {
		return "ClassPojo [fleets = " + fleets + ", recommendedFleet = " + recommendedFleet + "]";
	}
}
