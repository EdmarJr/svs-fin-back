package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({ @NamedQuery(name = AprovacaoBancaria.NQ_OBTER_POR_CALCULO_RENTABILIDADE.NAME, query = AprovacaoBancaria.NQ_OBTER_POR_CALCULO_RENTABILIDADE.JPQL) })
public class AprovacaoBancaria implements Serializable {

	private static final long serialVersionUID = -7692768526779335073L;
	
	public static interface NQ_OBTER_POR_CALCULO_RENTABILIDADE {
		public static final String NAME = "AprovacaoBancaria.obterPorCalculoRentabilidade";
		public static final String JPQL = "Select pe from AprovacaoBancaria pe where pe.calculoRentabilidade.id = :"
				+ NQ_OBTER_POR_CALCULO_RENTABILIDADE.NM_PM_ID_CALCULO_RENTABILIDADE;
		public static final String NM_PM_ID_CALCULO_RENTABILIDADE = "idCalculoRentabilidade";
	}	

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Column
	private Calendar data;

	@Column
	private Boolean aprovado;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private AcessUser usuario;

	@ManyToOne
	@JoinColumn(name = "id_calculo")
	private CalculoRentabilidade calculoRentabilidade;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "aprovacao_bancaria_motivos", joinColumns = @JoinColumn(name = "aprovacao_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "motivo_id", referencedColumnName = "id"))
	@Fetch(FetchMode.SUBSELECT)
	private List<MotivoAprovacaoSvs> motivosAprovacao;

	@Column(length=2000)
	private String comentario;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AcessUser getUsuario() {
		return usuario;
	}

	public void setUsuario(AcessUser usuario) {
		this.usuario = usuario;
	}

	public CalculoRentabilidade getCalculoRentabilidade() {
		return calculoRentabilidade;
	}

	public void setCalculoRentabilidade(CalculoRentabilidade calculoRentabilidade) {
		this.calculoRentabilidade = calculoRentabilidade;
	}

	public List<MotivoAprovacaoSvs> getMotivosAprovacao() {
		if(motivosAprovacao == null) {
			motivosAprovacao = new ArrayList<MotivoAprovacaoSvs>();
		}
		return motivosAprovacao;
	}

	public void setMotivosAprovacao(List<MotivoAprovacaoSvs> motivosAprovacao) {
		this.motivosAprovacao = motivosAprovacao;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public Boolean getAprovado() {
		return aprovado;
	}

	public void setAprovado(Boolean aprovado) {
		this.aprovado = aprovado;
	}
	
	
}
