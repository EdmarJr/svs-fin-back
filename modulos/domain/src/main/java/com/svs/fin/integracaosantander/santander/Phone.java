package com.svs.fin.integracaosantander.santander;

public class Phone {

	private String relationshipType;

	private String ddd;

	private String name;

	private String number;

	public String getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(String relationshipType) {
		this.relationshipType = relationshipType;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "ClassPojo [relationshipType = " + relationshipType + ", ddd = " + ddd + ", name = " + name
				+ ", number = " + number + "]";
	}
}
