package com.svs.fin.modelo.mapper.jackson;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class LocalDateFromJsonMilis extends StdDeserializer<LocalDate> {

    private static final long serialVersionUID = 1L;

    public LocalDateFromJsonMilis() {
        super(LocalDate.class);
    }

	@Override
	public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		LocalDate date =
				jp.readValueAs(Long.class) != null && jp.readValueAs(Long.class) != 0?
			    Instant.ofEpochMilli(jp.readValueAs(Long.class)).atZone(ZoneId.systemDefault()).toLocalDate() : null;
		
		return date;
	}

}