package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class SimulationRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5602771295937087327L;

	private String tableNumber;

	private Insurance insurance;

	private Fleet fleet;

	private Store store;

	private Payment payment;

	private Fees fees;

	private String uuid;

	private Customer customer;

	public String getTableNumber() {
		return tableNumber;
	}

	public void setTableNumber(String tableNumber) {
		this.tableNumber = tableNumber;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public Fleet getFleet() {
		if(fleet == null) {
			fleet = new Fleet();
		}
		return fleet;
	}

	public void setFleet(Fleet fleet) {
		this.fleet = fleet;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Payment getPayment() {
		if(payment == null) {
			payment = new Payment();
		}
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Fees getFees() {
		return fees;
	}

	public void setFees(Fees fees) {
		this.fees = fees;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "ClassPojo [tableNumber = " + tableNumber + ", insurance = " + insurance + ", fleet = " + fleet
				+ ", store = " + store + ", payment = " + payment + ", fees = " + fees + ", uuid = " + uuid
				+ ", customer = " + customer + "]";
	}

}
