package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class Insurances implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1538372787845041058L;

	private String id;

	private String indicator;

	private String aliquot;

	private String description;

	private String value;

	private String justification;

	private String characteristic;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}

	public String getAliquot() {
		return aliquot;
	}

	public void setAliquot(String aliquot) {
		this.aliquot = aliquot;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public String getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", indicator = " + indicator + ", aliquot = " + aliquot + ", description = "
				+ description + ", value = " + value + ", justification = " + justification + ", characteristic = "
				+ characteristic + "]";
	}
}
