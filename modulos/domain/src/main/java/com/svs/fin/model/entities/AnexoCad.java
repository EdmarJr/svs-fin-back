package com.svs.fin.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.model.entities.interfaces.AuditavelDataHoraDeCriacao;
import com.svs.fin.utils.UtilsData;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
	@NamedQuery(name = AnexoCad.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME, query = AnexoCad.NQ_OBTER_POR_OPERACAO_FINANCIADA.JPQL),
	@NamedQuery(name = AnexoCad.NQ_OBTER_POR_CALCULO_RENTABILIDADE.NAME, query = AnexoCad.NQ_OBTER_POR_CALCULO_RENTABILIDADE.JPQL)
})
public class AnexoCad implements Serializable, AuditavelDataHoraDeCriacao {

	private static final long serialVersionUID = -1002059401075270299L;

	public static interface NQ_OBTER_POR_OPERACAO_FINANCIADA {
		public static final String NAME = "AnexoCad.obterPorOperacaoFinanciada";
		public static final String JPQL = "Select pe from AnexoCad pe where pe.operacaoFinanciada.id = :"
				+ NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA;
		public static final String NM_PM_ID_OPERACAO_FINANCIADA = "idOperacaoFinanciada";
	}

	public static interface NQ_OBTER_POR_CALCULO_RENTABILIDADE {
		public static final String NAME = "AnexoCad.obterPorCalculoRentabilidade";
		public static final String JPQL = "Select pe from AnexoCad pe where pe.calculoRentabilidade.id = :"
				+ NQ_OBTER_POR_CALCULO_RENTABILIDADE.NM_PM_ID_CALCULO_RENTABILIDADE;
		public static final String NM_PM_ID_CALCULO_RENTABILIDADE = "idCalculoRentabilidade";
	}	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ANEXOSCAD")
	@SequenceGenerator(name = "SEQ_ANEXOSCAD", sequenceName = "SEQ_ANEXOSCAD", allocationSize = 1)
	private Long id;

	private String randomId;

	@Column
	private String nomeArquivo;

	@Column
	private String extensao;

	@Column
	@JsonProperty(access = Access.WRITE_ONLY)
	@Basic(fetch = FetchType.LAZY, optional = false)
	private byte[] anexo;

	// @Transient
	// private StreamedContent file;
	//
	// @Transient
	// private StreamedContent fileUpload;

	private Integer projeto;

	private Calendar dataUpload;

	@ManyToOne
	@JoinColumn(name = "id_operacao_financiada", referencedColumnName = "id")
	@JsonIgnore
	private OperacaoFinanciada operacaoFinanciada;

	@ManyToOne
	@JoinColumn(name = "id_calculo_rentabilidade", referencedColumnName = "id")
	@JsonIgnore
	private CalculoRentabilidade calculoRentabilidade;	

	// @Enumerated(EnumType.STRING)
	// private EnumTesteProducao testeProducao;

	@ManyToOne
	@JoinColumn(name = "usuario_id")
	private AcessUser usuario;

	@Column
	private String tipoDocumento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public String getExtensao() {
		return extensao;
	}

	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}

	public byte[] getAnexo() {
		return anexo;
	}

	public void setAnexo(byte[] anexo) {
		this.anexo = anexo;
	}

	public Integer getProjeto() {
		return projeto;
	}

	public void setProjeto(Integer projeto) {
		this.projeto = projeto;
	}

	public Calendar getDataUpload() {
		return dataUpload;
	}

	public void setDataUpload(Calendar dataUpload) {
		this.dataUpload = dataUpload;
	}

	// public StreamedContent getFile() {
	// if(anexo!= null){
	// InputStream itStream = new ByteArrayInputStream(anexo);
	// file = new DefaultStreamedContent(itStream, extensao, nomeArquivo);
	// }
	// return file;
	// }
	//
	// public void setFile(StreamedContent file) {
	// this.file = file;
	// }
	//
	// public StreamedContent getFileUpload() {
	// return fileUpload;
	// }
	//
	// public void setFileUpload(StreamedContent fileUpload) {
	// this.fileUpload = fileUpload;
	// }
	//
	// public EnumTesteProducao getTesteProducao() {
	// return testeProducao;
	// }
	//
	// public void setTesteProducao(EnumTesteProducao testeProducao) {
	// this.testeProducao = testeProducao;
	// }

	public AcessUser getUsuario() {
		return usuario;
	}

	public void setUsuario(AcessUser usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((randomId == null) ? 0 : randomId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AnexoCad))
			return false;
		AnexoCad other = (AnexoCad) obj;
		if (getId() != null && other.getId() != null && getId().equals(other.getId())) {
			return true;
		} else if (getRandomId() != null && other.getRandomId() != null && getRandomId().equals(other.getRandomId())) {
			return true;
		}
		return false;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public LocalDateTime getDataHoraDeCriacao() {
		return UtilsData.toLocalDateTime(this.getDataUpload());
	}

	@Override
	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao) {
		setDataUpload(UtilsData.toCalendar(dataHoraDeCriacao));

	}

	public OperacaoFinanciada getOperacaoFinanciada() {
		return operacaoFinanciada;
	}

	public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}

	public CalculoRentabilidade getCalculoRentabilidade() {
		return calculoRentabilidade;
	}

	public void setCalculoRentabilidade(CalculoRentabilidade calculoRentabilidade) {
		this.calculoRentabilidade = calculoRentabilidade;
	}

}
