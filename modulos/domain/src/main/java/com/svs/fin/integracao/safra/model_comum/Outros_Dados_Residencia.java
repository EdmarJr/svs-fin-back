/**
 * Outros_Dados_Residencia.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Outros_Dados_Residencia  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_residencia;

    private java.lang.String qt_anos_resid;

    private java.lang.String qt_meses_resid;

    private java.lang.String vl_onus_resid;

    public Outros_Dados_Residencia() {
    }

    public Outros_Dados_Residencia(
           java.lang.String id_residencia,
           java.lang.String qt_anos_resid,
           java.lang.String qt_meses_resid,
           java.lang.String vl_onus_resid) {
        this.id_residencia = id_residencia;
        this.qt_anos_resid = qt_anos_resid;
        this.qt_meses_resid = qt_meses_resid;
        this.vl_onus_resid = vl_onus_resid;
    }


    /**
     * Gets the id_residencia value for this Outros_Dados_Residencia.
     * 
     * @return id_residencia
     */
    public java.lang.String getId_residencia() {
        return id_residencia;
    }


    /**
     * Sets the id_residencia value for this Outros_Dados_Residencia.
     * 
     * @param id_residencia
     */
    public void setId_residencia(java.lang.String id_residencia) {
        this.id_residencia = id_residencia;
    }


    /**
     * Gets the qt_anos_resid value for this Outros_Dados_Residencia.
     * 
     * @return qt_anos_resid
     */
    public java.lang.String getQt_anos_resid() {
        return qt_anos_resid;
    }


    /**
     * Sets the qt_anos_resid value for this Outros_Dados_Residencia.
     * 
     * @param qt_anos_resid
     */
    public void setQt_anos_resid(java.lang.String qt_anos_resid) {
        this.qt_anos_resid = qt_anos_resid;
    }


    /**
     * Gets the qt_meses_resid value for this Outros_Dados_Residencia.
     * 
     * @return qt_meses_resid
     */
    public java.lang.String getQt_meses_resid() {
        return qt_meses_resid;
    }


    /**
     * Sets the qt_meses_resid value for this Outros_Dados_Residencia.
     * 
     * @param qt_meses_resid
     */
    public void setQt_meses_resid(java.lang.String qt_meses_resid) {
        this.qt_meses_resid = qt_meses_resid;
    }


    /**
     * Gets the vl_onus_resid value for this Outros_Dados_Residencia.
     * 
     * @return vl_onus_resid
     */
    public java.lang.String getVl_onus_resid() {
        return vl_onus_resid;
    }


    /**
     * Sets the vl_onus_resid value for this Outros_Dados_Residencia.
     * 
     * @param vl_onus_resid
     */
    public void setVl_onus_resid(java.lang.String vl_onus_resid) {
        this.vl_onus_resid = vl_onus_resid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Outros_Dados_Residencia)) return false;
        Outros_Dados_Residencia other = (Outros_Dados_Residencia) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_residencia==null && other.getId_residencia()==null) || 
             (this.id_residencia!=null &&
              this.id_residencia.equals(other.getId_residencia()))) &&
            ((this.qt_anos_resid==null && other.getQt_anos_resid()==null) || 
             (this.qt_anos_resid!=null &&
              this.qt_anos_resid.equals(other.getQt_anos_resid()))) &&
            ((this.qt_meses_resid==null && other.getQt_meses_resid()==null) || 
             (this.qt_meses_resid!=null &&
              this.qt_meses_resid.equals(other.getQt_meses_resid()))) &&
            ((this.vl_onus_resid==null && other.getVl_onus_resid()==null) || 
             (this.vl_onus_resid!=null &&
              this.vl_onus_resid.equals(other.getVl_onus_resid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_residencia() != null) {
            _hashCode += getId_residencia().hashCode();
        }
        if (getQt_anos_resid() != null) {
            _hashCode += getQt_anos_resid().hashCode();
        }
        if (getQt_meses_resid() != null) {
            _hashCode += getQt_meses_resid().hashCode();
        }
        if (getVl_onus_resid() != null) {
            _hashCode += getVl_onus_resid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Outros_Dados_Residencia.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Residencia"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_residencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_residencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qt_anos_resid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "qt_anos_resid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qt_meses_resid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "qt_meses_resid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_onus_resid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_onus_resid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
