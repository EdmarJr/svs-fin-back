package com.svs.fin.integracaosantander.santander;

public class Natural {

	private String occupation;

	private String documentState;

	private String patrimony;

	private String maritalStatus;

	private String monthlyIncome;

	private String birthCity;

	private String birthState;

	private String issuingBodyDocument;

	private String nationality;

	private String mother;

	private Spouse spouse;

	private Cellphone cellphone;

	private String documentNumber;

	private String gender;

	private String dateOfIssue;

	private String documentId;

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getDocumentState() {
		return documentState;
	}

	public void setDocumentState(String documentState) {
		this.documentState = documentState;
	}

	public String getPatrimony() {
		return patrimony;
	}

	public void setPatrimony(String patrimony) {
		this.patrimony = patrimony;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public String getBirthCity() {
		return birthCity;
	}

	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}

	public String getBirthState() {
		return birthState;
	}

	public void setBirthState(String birthState) {
		this.birthState = birthState;
	}

	public String getIssuingBodyDocument() {
		return issuingBodyDocument;
	}

	public void setIssuingBodyDocument(String issuingBodyDocument) {
		this.issuingBodyDocument = issuingBodyDocument;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}

	public Spouse getSpouse() {
		return spouse;
	}

	public void setSpouse(Spouse spouse) {
		this.spouse = spouse;
	}

	public Cellphone getCellphone() {
		return cellphone;
	}

	public void setCellphone(Cellphone cellphone) {
		this.cellphone = cellphone;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	@Override
	public String toString() {
		return "ClassPojo [occupation = " + occupation + ", documentState = " + documentState + ", patrimony = "
				+ patrimony + ", maritalStatus = " + maritalStatus + ", monthlyIncome = " + monthlyIncome
				+ ", birthCity = " + birthCity + ", birthState = " + birthState + ", issuingBodyDocument = "
				+ issuingBodyDocument + ", nationality = " + nationality + ", mother = " + mother + ", spouse = "
				+ spouse + ", cellphone = " + cellphone + ", documentNumber = " + documentNumber + ", gender = "
				+ gender + ", dateOfIssue = " + dateOfIssue + ", documentId = " + documentId + "]";
	}

}
