package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class PreAnaliseResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7230751605712113682L;

	private String accountHolder;

	private AtxResponse atxResponse;

	private String uuid;
	
	private String json;
	
	private String[] errors;

	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public AtxResponse getAtxResponse() {
		return atxResponse;
	}

	public void setAtxResponse(AtxResponse atxResponse) {
		this.atxResponse = atxResponse;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return "ClassPojo [accountHolder = " + accountHolder + ", atxResponse = " + atxResponse + ", uuid = " + uuid
				+ "]";
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String[] getErrors() {
		return errors;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}
}
