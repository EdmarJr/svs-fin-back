/**
 * Acionista_Socio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Acionista_Socio  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String cpf_cnpj;

    private java.lang.String id_dirigente_cargo_sub;

    private java.lang.String id_nacionalidade;

    private java.lang.String id_tipo_pessoa;

    private java.lang.String in_avalista;

    private java.lang.String nm_acionista_socio_titular;

    private java.lang.String pc_participacao;

    public Acionista_Socio() {
    }

    public Acionista_Socio(
           java.lang.String cpf_cnpj,
           java.lang.String id_dirigente_cargo_sub,
           java.lang.String id_nacionalidade,
           java.lang.String id_tipo_pessoa,
           java.lang.String in_avalista,
           java.lang.String nm_acionista_socio_titular,
           java.lang.String pc_participacao) {
        this.cpf_cnpj = cpf_cnpj;
        this.id_dirigente_cargo_sub = id_dirigente_cargo_sub;
        this.id_nacionalidade = id_nacionalidade;
        this.id_tipo_pessoa = id_tipo_pessoa;
        this.in_avalista = in_avalista;
        this.nm_acionista_socio_titular = nm_acionista_socio_titular;
        this.pc_participacao = pc_participacao;
    }


    /**
     * Gets the cpf_cnpj value for this Acionista_Socio.
     * 
     * @return cpf_cnpj
     */
    public java.lang.String getCpf_cnpj() {
        return cpf_cnpj;
    }


    /**
     * Sets the cpf_cnpj value for this Acionista_Socio.
     * 
     * @param cpf_cnpj
     */
    public void setCpf_cnpj(java.lang.String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }


    /**
     * Gets the id_dirigente_cargo_sub value for this Acionista_Socio.
     * 
     * @return id_dirigente_cargo_sub
     */
    public java.lang.String getId_dirigente_cargo_sub() {
        return id_dirigente_cargo_sub;
    }


    /**
     * Sets the id_dirigente_cargo_sub value for this Acionista_Socio.
     * 
     * @param id_dirigente_cargo_sub
     */
    public void setId_dirigente_cargo_sub(java.lang.String id_dirigente_cargo_sub) {
        this.id_dirigente_cargo_sub = id_dirigente_cargo_sub;
    }


    /**
     * Gets the id_nacionalidade value for this Acionista_Socio.
     * 
     * @return id_nacionalidade
     */
    public java.lang.String getId_nacionalidade() {
        return id_nacionalidade;
    }


    /**
     * Sets the id_nacionalidade value for this Acionista_Socio.
     * 
     * @param id_nacionalidade
     */
    public void setId_nacionalidade(java.lang.String id_nacionalidade) {
        this.id_nacionalidade = id_nacionalidade;
    }


    /**
     * Gets the id_tipo_pessoa value for this Acionista_Socio.
     * 
     * @return id_tipo_pessoa
     */
    public java.lang.String getId_tipo_pessoa() {
        return id_tipo_pessoa;
    }


    /**
     * Sets the id_tipo_pessoa value for this Acionista_Socio.
     * 
     * @param id_tipo_pessoa
     */
    public void setId_tipo_pessoa(java.lang.String id_tipo_pessoa) {
        this.id_tipo_pessoa = id_tipo_pessoa;
    }


    /**
     * Gets the in_avalista value for this Acionista_Socio.
     * 
     * @return in_avalista
     */
    public java.lang.String getIn_avalista() {
        return in_avalista;
    }


    /**
     * Sets the in_avalista value for this Acionista_Socio.
     * 
     * @param in_avalista
     */
    public void setIn_avalista(java.lang.String in_avalista) {
        this.in_avalista = in_avalista;
    }


    /**
     * Gets the nm_acionista_socio_titular value for this Acionista_Socio.
     * 
     * @return nm_acionista_socio_titular
     */
    public java.lang.String getNm_acionista_socio_titular() {
        return nm_acionista_socio_titular;
    }


    /**
     * Sets the nm_acionista_socio_titular value for this Acionista_Socio.
     * 
     * @param nm_acionista_socio_titular
     */
    public void setNm_acionista_socio_titular(java.lang.String nm_acionista_socio_titular) {
        this.nm_acionista_socio_titular = nm_acionista_socio_titular;
    }


    /**
     * Gets the pc_participacao value for this Acionista_Socio.
     * 
     * @return pc_participacao
     */
    public java.lang.String getPc_participacao() {
        return pc_participacao;
    }


    /**
     * Sets the pc_participacao value for this Acionista_Socio.
     * 
     * @param pc_participacao
     */
    public void setPc_participacao(java.lang.String pc_participacao) {
        this.pc_participacao = pc_participacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Acionista_Socio)) return false;
        Acionista_Socio other = (Acionista_Socio) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.cpf_cnpj==null && other.getCpf_cnpj()==null) || 
             (this.cpf_cnpj!=null &&
              this.cpf_cnpj.equals(other.getCpf_cnpj()))) &&
            ((this.id_dirigente_cargo_sub==null && other.getId_dirigente_cargo_sub()==null) || 
             (this.id_dirigente_cargo_sub!=null &&
              this.id_dirigente_cargo_sub.equals(other.getId_dirigente_cargo_sub()))) &&
            ((this.id_nacionalidade==null && other.getId_nacionalidade()==null) || 
             (this.id_nacionalidade!=null &&
              this.id_nacionalidade.equals(other.getId_nacionalidade()))) &&
            ((this.id_tipo_pessoa==null && other.getId_tipo_pessoa()==null) || 
             (this.id_tipo_pessoa!=null &&
              this.id_tipo_pessoa.equals(other.getId_tipo_pessoa()))) &&
            ((this.in_avalista==null && other.getIn_avalista()==null) || 
             (this.in_avalista!=null &&
              this.in_avalista.equals(other.getIn_avalista()))) &&
            ((this.nm_acionista_socio_titular==null && other.getNm_acionista_socio_titular()==null) || 
             (this.nm_acionista_socio_titular!=null &&
              this.nm_acionista_socio_titular.equals(other.getNm_acionista_socio_titular()))) &&
            ((this.pc_participacao==null && other.getPc_participacao()==null) || 
             (this.pc_participacao!=null &&
              this.pc_participacao.equals(other.getPc_participacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCpf_cnpj() != null) {
            _hashCode += getCpf_cnpj().hashCode();
        }
        if (getId_dirigente_cargo_sub() != null) {
            _hashCode += getId_dirigente_cargo_sub().hashCode();
        }
        if (getId_nacionalidade() != null) {
            _hashCode += getId_nacionalidade().hashCode();
        }
        if (getId_tipo_pessoa() != null) {
            _hashCode += getId_tipo_pessoa().hashCode();
        }
        if (getIn_avalista() != null) {
            _hashCode += getIn_avalista().hashCode();
        }
        if (getNm_acionista_socio_titular() != null) {
            _hashCode += getNm_acionista_socio_titular().hashCode();
        }
        if (getPc_participacao() != null) {
            _hashCode += getPc_participacao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Acionista_Socio.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Acionista_Socio"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpf_cnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "cpf_cnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_dirigente_cargo_sub");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_dirigente_cargo_sub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_nacionalidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_nacionalidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_tipo_pessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_tipo_pessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("in_avalista");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "in_avalista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_acionista_socio_titular");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_acionista_socio_titular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pc_participacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "pc_participacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
