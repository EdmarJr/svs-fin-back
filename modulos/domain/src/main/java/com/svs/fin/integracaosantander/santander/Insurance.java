package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class Insurance implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6164834009098829995L;

	private String id;

	private String insurancerCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInsurancerCode() {
		return insurancerCode;
	}

	public void setInsurancerCode(String insurancerCode) {
		this.insurancerCode = insurancerCode;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", insurancerCode = " + insurancerCode + "]";
	}
}
