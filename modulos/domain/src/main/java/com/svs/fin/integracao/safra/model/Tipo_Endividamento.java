/**
 * Tipo_Endividamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Endividamento")
public class Tipo_Endividamento implements java.io.Serializable {
	private java.lang.String ds_tipo_endividamento;

	@Id
	private java.lang.Integer id_tipo_endividamento;

	public Tipo_Endividamento() {
	}

	public Tipo_Endividamento(java.lang.String ds_tipo_endividamento, java.lang.Integer id_tipo_endividamento) {
		this.ds_tipo_endividamento = ds_tipo_endividamento;
		this.id_tipo_endividamento = id_tipo_endividamento;
	}

	/**
	 * Gets the ds_tipo_endividamento value for this Tipo_Endividamento.
	 * 
	 * @return ds_tipo_endividamento
	 */
	public java.lang.String getDs_tipo_endividamento() {
		return ds_tipo_endividamento;
	}

	/**
	 * Sets the ds_tipo_endividamento value for this Tipo_Endividamento.
	 * 
	 * @param ds_tipo_endividamento
	 */
	public void setDs_tipo_endividamento(java.lang.String ds_tipo_endividamento) {
		this.ds_tipo_endividamento = ds_tipo_endividamento;
	}

	/**
	 * Gets the id_tipo_endividamento value for this Tipo_Endividamento.
	 * 
	 * @return id_tipo_endividamento
	 */
	public java.lang.Integer getId_tipo_endividamento() {
		return id_tipo_endividamento;
	}

	/**
	 * Sets the id_tipo_endividamento value for this Tipo_Endividamento.
	 * 
	 * @param id_tipo_endividamento
	 */
	public void setId_tipo_endividamento(java.lang.Integer id_tipo_endividamento) {
		this.id_tipo_endividamento = id_tipo_endividamento;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Endividamento))
			return false;
		Tipo_Endividamento other = (Tipo_Endividamento) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_tipo_endividamento == null && other.getDs_tipo_endividamento() == null)
						|| (this.ds_tipo_endividamento != null
								&& this.ds_tipo_endividamento.equals(other.getDs_tipo_endividamento())))
				&& ((this.id_tipo_endividamento == null && other.getId_tipo_endividamento() == null)
						|| (this.id_tipo_endividamento != null
								&& this.id_tipo_endividamento.equals(other.getId_tipo_endividamento())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_endividamento() != null) {
			_hashCode += getDs_tipo_endividamento().hashCode();
		}
		if (getId_tipo_endividamento() != null) {
			_hashCode += getId_tipo_endividamento().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Endividamento.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"Tipo_Endividamento"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_endividamento");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_tipo_endividamento"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_endividamento");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_tipo_endividamento"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
