package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class Entry implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3884784812972105352L;

	private String maximumValue;

	private String recommendedValue;

	private String minimumValue;

	public String getMaximumValue() {
		return maximumValue;
	}

	public void setMaximumValue(String maximumValue) {
		this.maximumValue = maximumValue;
	}

	public String getRecommendedValue() {
		return recommendedValue;
	}

	public void setRecommendedValue(String recommendedValue) {
		this.recommendedValue = recommendedValue;
	}

	public String getMinimumValue() {
		return minimumValue;
	}

	public void setMinimumValue(String minimumValue) {
		this.minimumValue = minimumValue;
	}
	
	public Double getMinimumValueDouble() {
		if(minimumValue != null && !minimumValue.equals("")) {
			return new Double(minimumValue);
		}
		return null;
	}
	
	public Double getMaximumValueDouble() {
		if(maximumValue != null && !maximumValue.equals("")) {
			return new Double(maximumValue);
		}
		return null;
	}

	public Double getRecommendedValueDouble() {
		if(recommendedValue != null && !recommendedValue.equals("")) {
			return new Double(recommendedValue);
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "ClassPojo [maximumValue = " + maximumValue + ", recommendedValue = " + recommendedValue
				+ ", minimumValue = " + minimumValue + "]";
	}
}
