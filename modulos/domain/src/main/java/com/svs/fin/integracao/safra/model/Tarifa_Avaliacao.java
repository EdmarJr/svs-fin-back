/**
 * Tarifa_Avaliacao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tarifa_Avaliacao")
public class Tarifa_Avaliacao implements java.io.Serializable {
	@Id
	private java.lang.String tipo_tarifa;

	private java.lang.String vl_maximo;

	public Tarifa_Avaliacao() {
	}

	public Tarifa_Avaliacao(java.lang.String tipo_tarifa, java.lang.String vl_maximo) {
		this.tipo_tarifa = tipo_tarifa;
		this.vl_maximo = vl_maximo;
	}

	/**
	 * Gets the tipo_tarifa value for this Tarifa_Avaliacao.
	 * 
	 * @return tipo_tarifa
	 */
	public java.lang.String getTipo_tarifa() {
		return tipo_tarifa;
	}

	/**
	 * Sets the tipo_tarifa value for this Tarifa_Avaliacao.
	 * 
	 * @param tipo_tarifa
	 */
	public void setTipo_tarifa(java.lang.String tipo_tarifa) {
		this.tipo_tarifa = tipo_tarifa;
	}

	/**
	 * Gets the vl_maximo value for this Tarifa_Avaliacao.
	 * 
	 * @return vl_maximo
	 */
	public java.lang.String getVl_maximo() {
		return vl_maximo;
	}

	/**
	 * Sets the vl_maximo value for this Tarifa_Avaliacao.
	 * 
	 * @param vl_maximo
	 */
	public void setVl_maximo(java.lang.String vl_maximo) {
		this.vl_maximo = vl_maximo;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tarifa_Avaliacao))
			return false;
		Tarifa_Avaliacao other = (Tarifa_Avaliacao) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.tipo_tarifa == null && other.getTipo_tarifa() == null)
						|| (this.tipo_tarifa != null && this.tipo_tarifa.equals(other.getTipo_tarifa())))
				&& ((this.vl_maximo == null && other.getVl_maximo() == null)
						|| (this.vl_maximo != null && this.vl_maximo.equals(other.getVl_maximo())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getTipo_tarifa() != null) {
			_hashCode += getTipo_tarifa().hashCode();
		}
		if (getVl_maximo() != null) {
			_hashCode += getVl_maximo().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tarifa_Avaliacao.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tarifa_Avaliacao"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("tipo_tarifa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "tipo_tarifa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_maximo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_maximo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
