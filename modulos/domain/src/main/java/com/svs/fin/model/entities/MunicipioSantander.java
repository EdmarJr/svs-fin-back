package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name = "MUNICIPIO_SANTANDER")
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = MunicipioSantander.NQ_OBTER_POR_SIGLA_ESTADO.NAME, query = MunicipioSantander.NQ_OBTER_POR_SIGLA_ESTADO.JPQL) })
public class MunicipioSantander implements Serializable {

	public static interface NQ_OBTER_POR_SIGLA_ESTADO {
		public static final String NAME = "MunicipioSantander.obterPorSiglaEstado";
		public static final String JPQL = "Select ms from MunicipioSantander ms where ms.estado.id = :"
				+ NQ_OBTER_POR_SIGLA_ESTADO.NM_PM_SIGLA_ESTADO;
		public static final String NM_PM_SIGLA_ESTADO = "siglaEstado";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3746111170814950865L;

	@Id
	private Long id;

	@Column(length = 100)
	private String description;

	@ManyToOne
	@JoinColumn(name = "id_estado")
	private EstadoSantander estado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MunicipioSantander other = (MunicipioSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public EstadoSantander getEstado() {
		return estado;
	}

	public void setEstado(EstadoSantander estado) {
		this.estado = estado;
	}
}
