package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumEscolaridade {

//	ANALFABETO("1 - Analfabeto"), 
	@JsonProperty("Primário Incompleto")
	PRIMARIO_INCOMPLETO("Primário Incompleto"),
	@JsonProperty("Primário Completo")
	PRIMARIO_COMPLETO("Primário Completo"),
	@JsonProperty("Primeiro grau Incompleto")
	PRIMEIRO_GRAU_INCOMPLETO("Primeiro grau Incompleto"),
	@JsonProperty("Primeiro grau Completo")
	PRIMEIRO_GRAU_COMPLETO("Primeiro grau Completo"),
	@JsonProperty("Segundo grau Incompleto")
	SEGUNDO_GRAU_INCOMPLETO("Segundo grau Incompleto"),
	@JsonProperty("Segundo grau Completo")
	SEGUNDO_GRAU_COMPLETO("Segundo grau Completo"),
	@JsonProperty("Superior Incompleto")
	SUPERIOR_INCOMPLETO("Superior Incompleto"),
	@JsonProperty("Superior Completo")
	SUPERIOR_COMPLETO("Superior Completo"),
	@JsonProperty("Pós-Graduação")
	POS_GRADUACAO("Pós-Graduação"),
	@JsonProperty("Doutorado")
	DOUTORADO("Doutorado"),
	@JsonProperty("Segundo grau técnico incompleto")
	SEGUNDO_GRAU_TECNICO_INCOMPLETO("Segundo grau técnico incompleto"),
	@JsonProperty("Segundo grau técnico completo")
	SEGUNDO_GRAU_TECNICO_COMPLETO("Segundo grau técnico completo"),
	@JsonProperty("Mestrado")
	MESTRADO("Mestrado");

	EnumEscolaridade(String label) {
		this.label = label;
	}

	private String label;



	public String getLabel() {
		return label;
	}
}
