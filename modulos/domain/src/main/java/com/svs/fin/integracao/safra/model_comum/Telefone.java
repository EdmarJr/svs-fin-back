/**
 * Telefone.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Telefone  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String nr_ddd;

    private java.lang.String nr_ramal;

    private java.lang.String nr_telefone;

    public Telefone() {
    }

    public Telefone(
           java.lang.String nr_ddd,
           java.lang.String nr_ramal,
           java.lang.String nr_telefone) {
        this.nr_ddd = nr_ddd;
        this.nr_ramal = nr_ramal;
        this.nr_telefone = nr_telefone;
    }


    /**
     * Gets the nr_ddd value for this Telefone.
     * 
     * @return nr_ddd
     */
    public java.lang.String getNr_ddd() {
        return nr_ddd;
    }


    /**
     * Sets the nr_ddd value for this Telefone.
     * 
     * @param nr_ddd
     */
    public void setNr_ddd(java.lang.String nr_ddd) {
        this.nr_ddd = nr_ddd;
    }


    /**
     * Gets the nr_ramal value for this Telefone.
     * 
     * @return nr_ramal
     */
    public java.lang.String getNr_ramal() {
        return nr_ramal;
    }


    /**
     * Sets the nr_ramal value for this Telefone.
     * 
     * @param nr_ramal
     */
    public void setNr_ramal(java.lang.String nr_ramal) {
        this.nr_ramal = nr_ramal;
    }


    /**
     * Gets the nr_telefone value for this Telefone.
     * 
     * @return nr_telefone
     */
    public java.lang.String getNr_telefone() {
        return nr_telefone;
    }


    /**
     * Sets the nr_telefone value for this Telefone.
     * 
     * @param nr_telefone
     */
    public void setNr_telefone(java.lang.String nr_telefone) {
        this.nr_telefone = nr_telefone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Telefone)) return false;
        Telefone other = (Telefone) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.nr_ddd==null && other.getNr_ddd()==null) || 
             (this.nr_ddd!=null &&
              this.nr_ddd.equals(other.getNr_ddd()))) &&
            ((this.nr_ramal==null && other.getNr_ramal()==null) || 
             (this.nr_ramal!=null &&
              this.nr_ramal.equals(other.getNr_ramal()))) &&
            ((this.nr_telefone==null && other.getNr_telefone()==null) || 
             (this.nr_telefone!=null &&
              this.nr_telefone.equals(other.getNr_telefone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNr_ddd() != null) {
            _hashCode += getNr_ddd().hashCode();
        }
        if (getNr_ramal() != null) {
            _hashCode += getNr_ramal().hashCode();
        }
        if (getNr_telefone() != null) {
            _hashCode += getNr_telefone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Telefone.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_ddd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_ddd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_ramal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_ramal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
