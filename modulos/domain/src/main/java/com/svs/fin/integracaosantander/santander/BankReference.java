package com.svs.fin.integracaosantander.santander;

public class BankReference {

	private String accountCode;

	private String agencyCode;

	private String accountDigit;

	private String bankCode;

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getAccountDigit() {
		return accountDigit;
	}

	public void setAccountDigit(String accountDigit) {
		this.accountDigit = accountDigit;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	@Override
	public String toString() {
		return "ClassPojo [accountCode = " + accountCode + ", agencyCode = " + agencyCode + ", accountDigit = "
				+ accountDigit + ", bankCode = " + bankCode + "]";
	}
}
