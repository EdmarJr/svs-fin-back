package com.svs.fin.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "TAXA_TABELA_FINANCEIRA")
@NamedQueries({
		@NamedQuery(name = TaxaTabelaFinanceira.NQ_OBTER_POR_TABELA_FINANCEIRA.NAME, query = TaxaTabelaFinanceira.NQ_OBTER_POR_TABELA_FINANCEIRA.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxaTabelaFinanceira implements Serializable {
	public static interface NQ_OBTER_POR_TABELA_FINANCEIRA {
		public static final String NAME = "TaxaTabelaFinanceira.obterPorTabelaFinanceira";
		public static final String JPQL = "Select t from TaxaTabelaFinanceira t where t.tabelaFinanceira.id = :"
				+ NQ_OBTER_POR_TABELA_FINANCEIRA.NM_PM_TABELA_FINANCEIRA;
		public static final String NM_PM_TABELA_FINANCEIRA = "idTabelaFinanceira";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -270720759024771807L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TAXA_TABELA_FINANCEIRA")
	@SequenceGenerator(name = "SEQ_TAXA_TABELA_FINANCEIRA", sequenceName = "SEQ_TAXA_TABELA_FINANCEIRA", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "tabela_financeira_id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private TabelaFinanceira tabelaFinanceira;

	@Column
	private Integer carencia;

	@Column
	private Integer parcelas;

	@Column
	private Double percentualEntrada;

	@Column
	private Integer anoInicio;

	@Column
	private Integer anoFim;

	@Column
	private Double valorRebate;

	@Column
	private Double percentualRebate;

	@Column
	private Double percentualTaxaRetorno;

	@Column
	private Double coeficiente;

	@Column
	private Double taxaMes;

	@ManyToOne
	@JoinColumn(name = "id_mascara")
	private MascaraTaxaTabelaFinanceira mascara;

	@Column
	private Boolean zeroKm;

	@Transient
	private Boolean editando;

	public TaxaTabelaFinanceira() {
	}

	public TaxaTabelaFinanceira(TabelaFinanceira tabelaFinanceira) {
		this.tabelaFinanceira = tabelaFinanceira;
	}

	public Integer getCarencia() {
		return carencia;
	}

	public void setCarencia(Integer carencia) {
		this.carencia = carencia;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public Double getPercentualEntrada() {
		if (percentualEntrada == null) {
			percentualEntrada = 0d;
		}
		return percentualEntrada;
	}

	public void setPercentualEntrada(Double percentualEntrada) {
		this.percentualEntrada = percentualEntrada;
	}

	public Integer getAnoInicio() {
		return anoInicio;
	}

	public void setAnoInicio(Integer anoInicio) {
		this.anoInicio = anoInicio;
	}

	public Integer getAnoFim() {
		return anoFim;
	}

	public void setAnoFim(Integer anoFim) {
		this.anoFim = anoFim;
	}

	public Double getValorRebate() {
		return valorRebate;
	}

	public void setValorRebate(Double valorRebate) {
		this.valorRebate = valorRebate;
	}

	public Double getPercentualRebate() {
		return percentualRebate;
	}

	public void setPercentualRebate(Double percentualRebate) {
		this.percentualRebate = percentualRebate;
	}

	public Double getPercentualTaxaRetorno() {
		if (percentualTaxaRetorno == null) {
			percentualTaxaRetorno = 0d;
		}
		return percentualTaxaRetorno;
	}

	public void setPercentualTaxaRetorno(Double percentualTaxaRetorno) {
		this.percentualTaxaRetorno = percentualTaxaRetorno;
	}

	public Double getCoeficiente() {
		return coeficiente;
	}

	public void setCoeficiente(Double coeficiente) {
		this.coeficiente = coeficiente;
	}

	public Double getTaxaMes() {
		return taxaMes;
	}

	public void setTaxaMes(Double taxaMes) {
		this.taxaMes = taxaMes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TabelaFinanceira getTabelaFinanceira() {
		return tabelaFinanceira;
	}

	public void setTabelaFinanceira(TabelaFinanceira tabelaFinanceira) {
		this.tabelaFinanceira = tabelaFinanceira;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxaTabelaFinanceira other = (TaxaTabelaFinanceira) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setZeroKm(Boolean zeroKm) {
		this.zeroKm = zeroKm;
	}

	public Boolean getZeroKm() {
		return zeroKm;
	}

	public MascaraTaxaTabelaFinanceira getMascara() {
		return mascara;
	}

	public void setMascara(MascaraTaxaTabelaFinanceira mascara) {
		this.mascara = mascara;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public Boolean isValidaAnoCarro(Integer anoFabricacaoCarro) {
		return getAnoInicio() != null && getAnoInicio().intValue() <= anoFabricacaoCarro && getAnoFim() != null
				&& getAnoFim().intValue() >= anoFabricacaoCarro;
	}
}
