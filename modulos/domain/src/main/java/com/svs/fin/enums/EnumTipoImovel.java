package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoImovel {
	
//	TERRENO("Terreno"),
//	FAZENDA("Fazenda"),
//	CHACARA("Chácara"),
//	OUTROS_URBANOS("Outros urbanos"),
//	CASA("Casa"),
//	APARTAMENTO("Apartamento");
	
	@JsonProperty("Terreno")
	TERRENO("Terreno"),
		@JsonProperty("Fazenda")
	FAZENDA("Fazenda"),
		@JsonProperty("Chácara")
	CHACARA("Chácara"),
		@JsonProperty("Outros urbanos")
	OUTROS_URBANOS("Outros urbanos"),
		@JsonProperty("Casa")
	CASA("Casa"),
		@JsonProperty("Apartamento")
	APARTAMENTO("Apartamento");
	
	private String label;
	
	private EnumTipoImovel(String label){
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
