package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumTipoRecebimento;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
	@NamedQuery(name = ParcelaEntrada.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME, query = ParcelaEntrada.NQ_OBTER_POR_OPERACAO_FINANCIADA.JPQL) })
public class ParcelaEntrada implements Serializable {

	public static interface NQ_OBTER_POR_OPERACAO_FINANCIADA {
		public static final String NAME = "ParcelaEntrada.obterPorOperacaoFinanciada";
		public static final String JPQL = "Select pe from ParcelaEntrada pe where pe.operacaoFinanciada.id = :"
				+ NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA;
		public static final String NM_PM_ID_OPERACAO_FINANCIADA = "idOperacaoFinanciada";
	}
	
	private static final long serialVersionUID = 383357174418920535L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PARCELAENTRADA")
	@SequenceGenerator(name = "SEQ_PARCELAENTRADA", sequenceName = "SEQ_PARCELAENTRADA", allocationSize = 1)
	private Long id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_operacao")
	private OperacaoFinanciada operacaoFinanciada;

	@OneToOne()
	@JoinColumn(name = "id_parcela_abater")
	private ParcelaEntrada parcelaAbater;
	
	@Column(name="FLAG_ABATER_FINANCIMENTO")
	private Boolean flagAbaterFinanciamento;
	
	@Column
	private Calendar data;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoRecebimento tipo;

	@Column
	private Integer codigo;

	@ManyToOne
	@JoinColumn(name = "id_tipo_pagamento")
	private TipoPagamento tipoPagamento;

	@Column(length = 1000)
	private String observacao;

	@Column
	private Double valor;

	@Transient
	private Boolean editando;

	@Transient
	private Integer codigoParcelaAbater;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OperacaoFinanciada getOperacaoFinanciada() {
		return operacaoFinanciada;
	}

	public void setOperacaoFinanciada(OperacaoFinanciada operacaoFinanciada) {
		this.operacaoFinanciada = operacaoFinanciada;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EnumTipoRecebimento getTipo() {
		return tipo;
	}

	public void setTipo(EnumTipoRecebimento tipo) {
		this.tipo = tipo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public ParcelaEntrada getParcelaAbater() {
		return parcelaAbater;
	}

	public void setParcelaAbater(ParcelaEntrada parcelaAbater) {
		this.parcelaAbater = parcelaAbater;
	}

	public Integer getCodigoParcelaAbater() {
		if (getParcelaAbater() != null) {
			codigoParcelaAbater = getParcelaAbater().getCodigo();
		}
		return codigoParcelaAbater;
	}

	public void setCodigoParcelaAbater(Integer codigoParcelaAbater) {
		this.codigoParcelaAbater = codigoParcelaAbater;
	}

	public Boolean getFlagAbaterFinanciamento() {
		return flagAbaterFinanciamento;
	}

	public void setFlagAbaterFinanciamento(Boolean flagAbaterFinanciamento) {
		this.flagAbaterFinanciamento = flagAbaterFinanciamento;
	}
	
	
}
