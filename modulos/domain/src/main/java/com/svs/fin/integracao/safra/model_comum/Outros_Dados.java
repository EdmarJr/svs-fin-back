/**
 * Outros_Dados.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Outros_Dados  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String dt_nascimento;

    private java.lang.String email_website;

    private java.lang.String id_escolaridade;

    private java.lang.String id_estado_civil;

    private java.lang.String id_nacionalidade;

    private java.lang.String id_sexo;

    private java.lang.String nr_dependentes;

    private java.lang.String sq_sub_telefone;

    public Outros_Dados() {
    }

    public Outros_Dados(
           java.lang.String dt_nascimento,
           java.lang.String email_website,
           java.lang.String id_escolaridade,
           java.lang.String id_estado_civil,
           java.lang.String id_nacionalidade,
           java.lang.String id_sexo,
           java.lang.String nr_dependentes,
           java.lang.String sq_sub_telefone) {
        this.dt_nascimento = dt_nascimento;
        this.email_website = email_website;
        this.id_escolaridade = id_escolaridade;
        this.id_estado_civil = id_estado_civil;
        this.id_nacionalidade = id_nacionalidade;
        this.id_sexo = id_sexo;
        this.nr_dependentes = nr_dependentes;
        this.sq_sub_telefone = sq_sub_telefone;
    }


    /**
     * Gets the dt_nascimento value for this Outros_Dados.
     * 
     * @return dt_nascimento
     */
    public java.lang.String getDt_nascimento() {
        return dt_nascimento;
    }


    /**
     * Sets the dt_nascimento value for this Outros_Dados.
     * 
     * @param dt_nascimento
     */
    public void setDt_nascimento(java.lang.String dt_nascimento) {
        this.dt_nascimento = dt_nascimento;
    }


    /**
     * Gets the email_website value for this Outros_Dados.
     * 
     * @return email_website
     */
    public java.lang.String getEmail_website() {
        return email_website;
    }


    /**
     * Sets the email_website value for this Outros_Dados.
     * 
     * @param email_website
     */
    public void setEmail_website(java.lang.String email_website) {
        this.email_website = email_website;
    }


    /**
     * Gets the id_escolaridade value for this Outros_Dados.
     * 
     * @return id_escolaridade
     */
    public java.lang.String getId_escolaridade() {
        return id_escolaridade;
    }


    /**
     * Sets the id_escolaridade value for this Outros_Dados.
     * 
     * @param id_escolaridade
     */
    public void setId_escolaridade(java.lang.String id_escolaridade) {
        this.id_escolaridade = id_escolaridade;
    }


    /**
     * Gets the id_estado_civil value for this Outros_Dados.
     * 
     * @return id_estado_civil
     */
    public java.lang.String getId_estado_civil() {
        return id_estado_civil;
    }


    /**
     * Sets the id_estado_civil value for this Outros_Dados.
     * 
     * @param id_estado_civil
     */
    public void setId_estado_civil(java.lang.String id_estado_civil) {
        this.id_estado_civil = id_estado_civil;
    }


    /**
     * Gets the id_nacionalidade value for this Outros_Dados.
     * 
     * @return id_nacionalidade
     */
    public java.lang.String getId_nacionalidade() {
        return id_nacionalidade;
    }


    /**
     * Sets the id_nacionalidade value for this Outros_Dados.
     * 
     * @param id_nacionalidade
     */
    public void setId_nacionalidade(java.lang.String id_nacionalidade) {
        this.id_nacionalidade = id_nacionalidade;
    }


    /**
     * Gets the id_sexo value for this Outros_Dados.
     * 
     * @return id_sexo
     */
    public java.lang.String getId_sexo() {
        return id_sexo;
    }


    /**
     * Sets the id_sexo value for this Outros_Dados.
     * 
     * @param id_sexo
     */
    public void setId_sexo(java.lang.String id_sexo) {
        this.id_sexo = id_sexo;
    }


    /**
     * Gets the nr_dependentes value for this Outros_Dados.
     * 
     * @return nr_dependentes
     */
    public java.lang.String getNr_dependentes() {
        return nr_dependentes;
    }


    /**
     * Sets the nr_dependentes value for this Outros_Dados.
     * 
     * @param nr_dependentes
     */
    public void setNr_dependentes(java.lang.String nr_dependentes) {
        this.nr_dependentes = nr_dependentes;
    }


    /**
     * Gets the sq_sub_telefone value for this Outros_Dados.
     * 
     * @return sq_sub_telefone
     */
    public java.lang.String getSq_sub_telefone() {
        return sq_sub_telefone;
    }


    /**
     * Sets the sq_sub_telefone value for this Outros_Dados.
     * 
     * @param sq_sub_telefone
     */
    public void setSq_sub_telefone(java.lang.String sq_sub_telefone) {
        this.sq_sub_telefone = sq_sub_telefone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Outros_Dados)) return false;
        Outros_Dados other = (Outros_Dados) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dt_nascimento==null && other.getDt_nascimento()==null) || 
             (this.dt_nascimento!=null &&
              this.dt_nascimento.equals(other.getDt_nascimento()))) &&
            ((this.email_website==null && other.getEmail_website()==null) || 
             (this.email_website!=null &&
              this.email_website.equals(other.getEmail_website()))) &&
            ((this.id_escolaridade==null && other.getId_escolaridade()==null) || 
             (this.id_escolaridade!=null &&
              this.id_escolaridade.equals(other.getId_escolaridade()))) &&
            ((this.id_estado_civil==null && other.getId_estado_civil()==null) || 
             (this.id_estado_civil!=null &&
              this.id_estado_civil.equals(other.getId_estado_civil()))) &&
            ((this.id_nacionalidade==null && other.getId_nacionalidade()==null) || 
             (this.id_nacionalidade!=null &&
              this.id_nacionalidade.equals(other.getId_nacionalidade()))) &&
            ((this.id_sexo==null && other.getId_sexo()==null) || 
             (this.id_sexo!=null &&
              this.id_sexo.equals(other.getId_sexo()))) &&
            ((this.nr_dependentes==null && other.getNr_dependentes()==null) || 
             (this.nr_dependentes!=null &&
              this.nr_dependentes.equals(other.getNr_dependentes()))) &&
            ((this.sq_sub_telefone==null && other.getSq_sub_telefone()==null) || 
             (this.sq_sub_telefone!=null &&
              this.sq_sub_telefone.equals(other.getSq_sub_telefone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDt_nascimento() != null) {
            _hashCode += getDt_nascimento().hashCode();
        }
        if (getEmail_website() != null) {
            _hashCode += getEmail_website().hashCode();
        }
        if (getId_escolaridade() != null) {
            _hashCode += getId_escolaridade().hashCode();
        }
        if (getId_estado_civil() != null) {
            _hashCode += getId_estado_civil().hashCode();
        }
        if (getId_nacionalidade() != null) {
            _hashCode += getId_nacionalidade().hashCode();
        }
        if (getId_sexo() != null) {
            _hashCode += getId_sexo().hashCode();
        }
        if (getNr_dependentes() != null) {
            _hashCode += getNr_dependentes().hashCode();
        }
        if (getSq_sub_telefone() != null) {
            _hashCode += getSq_sub_telefone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Outros_Dados.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_nascimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_nascimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email_website");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "email_website"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_escolaridade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_escolaridade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_estado_civil");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_estado_civil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_nacionalidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_nacionalidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_sexo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_sexo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_dependentes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_dependentes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sq_sub_telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "sq_sub_telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
