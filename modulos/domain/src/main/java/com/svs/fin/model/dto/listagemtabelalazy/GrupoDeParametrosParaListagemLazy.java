package com.svs.fin.model.dto.listagemtabelalazy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GrupoDeParametrosParaListagemLazy {
	private List<ParametroOrdenacao> parametrosDeOrdenacao;
	private List<ParametroBusca> parametrosDeBusca;
	// private String buscaGlobal;
	private Integer offSet;
	private Integer limit;

	private GrupoDeParametrosParaListagemLazy() {
		setParametrosDeOrdenacao(new ArrayList<ParametroOrdenacao>());
		setParametrosDeBusca(new ArrayList<ParametroBusca>());
	}

	public List<ParametroOrdenacao> getParametrosDeOrdenacao() {
		return parametrosDeOrdenacao;
	}

	public void setParametrosDeOrdenacao(List<ParametroOrdenacao> parametrosDeOrdenacao) {
		this.parametrosDeOrdenacao = parametrosDeOrdenacao;
	}

	public List<ParametroBusca> getParametrosDeBusca() {
		return parametrosDeBusca;
	}

	public void setParametrosDeBusca(List<ParametroBusca> parametrosDeBusca) {
		this.parametrosDeBusca = parametrosDeBusca;
	}

	// public String getBuscaGlobal() {
	// return buscaGlobal;
	// }
	//
	// public void setBuscaGlobal(String buscaGlobal) {
	// this.buscaGlobal = buscaGlobal;
	// }

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public static class Builder {
		private GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy;

		private Builder(GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy) {
			this.grupoDeParametrosParaListagemLazy = grupoDeParametrosParaListagemLazy;
		}

		public static Builder novoGrupo() {
			return new GrupoDeParametrosParaListagemLazy.Builder(new GrupoDeParametrosParaListagemLazy());
		}
		
		public static Builder novoGrupo(String[] parametrosDeOrdenacao, String[] parametrosDeBusca,
				Integer offSet, Integer limit) {
			return new GrupoDeParametrosParaListagemLazy.Builder(new GrupoDeParametrosParaListagemLazy())
					.adicionarParametrosBusca(Arrays.asList(parametrosDeBusca)).adicionarParametrosOrdenacao(Arrays.asList(parametrosDeOrdenacao))
					.setLimit(limit).setOffSet(offSet);
			// .setBuscaGlobal(buscaGlobal);
		}

		public static Builder novoGrupo(List<String> parametrosDeOrdenacao, List<String> parametrosDeBusca,
				Integer offSet, Integer limit) {
			return new GrupoDeParametrosParaListagemLazy.Builder(new GrupoDeParametrosParaListagemLazy())
					.adicionarParametrosBusca(parametrosDeBusca).adicionarParametrosOrdenacao(parametrosDeOrdenacao)
					.setLimit(limit).setOffSet(offSet);
			// .setBuscaGlobal(buscaGlobal);
		}

		public Builder adicionarParametroOrdenacao(String parametroOrdenacao) {
			if (seStringTemVirgula(parametroOrdenacao)) {
				String[] parametroOrdenacaoArray = parametroOrdenacao.split(",");
				return this.adicionarParametroOrdenacao(
						new ParametroOrdenacao(parametroOrdenacaoArray[0], parametroOrdenacaoArray[1]));
			}
			return this;
		}

		public Builder adicionarParametrosBusca(List<String> parametros) {
			if (parametros == null) {
				return this;
			}
			parametros.forEach(p -> this.adicionarParametroBusca(p));
			return this;
		}

		public Builder adicionarParametrosOrdenacao(List<String> parametros) {
			if (parametros == null) {
				return this;
			}
			parametros.forEach(p -> this.adicionarParametroOrdenacao(p));
			return this;
		}

		public Builder adicionarParametroOrdenacao(ParametroOrdenacao parametroOrdenacao) {
			getGrupoDeParametrosParaListagemLazy().getParametrosDeOrdenacao().add(parametroOrdenacao);
			return this;
		}

		public Builder adicionarParametroBusca(ParametroBusca parametroBusca) {
			getGrupoDeParametrosParaListagemLazy().getParametrosDeBusca().add(parametroBusca);
			return this;
		}

		public Builder adicionarParametroBusca(String parametroBusca) {
			if (seStringTemVirgula(parametroBusca)) {
				String[] parametroBuscaArray = parametroBusca.split(",");
				return this.adicionarParametroBusca(new ParametroBusca(parametroBuscaArray[0], parametroBuscaArray[1]));
			}
			return this;
		}

		private boolean seStringTemVirgula(String parametroBusca) {
			return parametroBusca != null && parametroBusca.indexOf(",") != -1;
		}

		private GrupoDeParametrosParaListagemLazy getGrupoDeParametrosParaListagemLazy() {
			return grupoDeParametrosParaListagemLazy;
		}

		// public Builder setBuscaGlobal(String buscaGlobal) {
		// getGrupoDeParametrosParaListagemLazy().setBuscaGlobal(buscaGlobal);
		// return this;
		// }

		public Builder setOffSet(Integer offSet) {
			getGrupoDeParametrosParaListagemLazy().setOffSet(offSet);
			return this;
		}

		public Builder setLimit(Integer limit) {
			getGrupoDeParametrosParaListagemLazy().setLimit(limit);
			return this;
		}

		public GrupoDeParametrosParaListagemLazy build() {
			return this.getGrupoDeParametrosParaListagemLazy();
		}

	}

}
