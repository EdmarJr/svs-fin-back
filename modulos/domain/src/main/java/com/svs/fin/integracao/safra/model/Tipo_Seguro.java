/**
 * Tipo_Seguro.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Seguro")
public class Tipo_Seguro implements java.io.Serializable {
	private java.lang.String ds_empresa;

	private java.lang.String ds_tipo_seguro;

	@Id
	private java.lang.Integer id_tipo_seguro;

	private java.math.BigDecimal vl_percentual;

	public Tipo_Seguro() {
	}

	public Tipo_Seguro(java.lang.String ds_empresa, java.lang.String ds_tipo_seguro, java.lang.Integer id_tipo_seguro,
			java.math.BigDecimal vl_percentual) {
		this.ds_empresa = ds_empresa;
		this.ds_tipo_seguro = ds_tipo_seguro;
		this.id_tipo_seguro = id_tipo_seguro;
		this.vl_percentual = vl_percentual;
	}

	/**
	 * Gets the ds_empresa value for this Tipo_Seguro.
	 * 
	 * @return ds_empresa
	 */
	public java.lang.String getDs_empresa() {
		return ds_empresa;
	}

	/**
	 * Sets the ds_empresa value for this Tipo_Seguro.
	 * 
	 * @param ds_empresa
	 */
	public void setDs_empresa(java.lang.String ds_empresa) {
		this.ds_empresa = ds_empresa;
	}

	/**
	 * Gets the ds_tipo_seguro value for this Tipo_Seguro.
	 * 
	 * @return ds_tipo_seguro
	 */
	public java.lang.String getDs_tipo_seguro() {
		return ds_tipo_seguro;
	}

	/**
	 * Sets the ds_tipo_seguro value for this Tipo_Seguro.
	 * 
	 * @param ds_tipo_seguro
	 */
	public void setDs_tipo_seguro(java.lang.String ds_tipo_seguro) {
		this.ds_tipo_seguro = ds_tipo_seguro;
	}

	/**
	 * Gets the id_tipo_seguro value for this Tipo_Seguro.
	 * 
	 * @return id_tipo_seguro
	 */
	public java.lang.Integer getId_tipo_seguro() {
		return id_tipo_seguro;
	}

	/**
	 * Sets the id_tipo_seguro value for this Tipo_Seguro.
	 * 
	 * @param id_tipo_seguro
	 */
	public void setId_tipo_seguro(java.lang.Integer id_tipo_seguro) {
		this.id_tipo_seguro = id_tipo_seguro;
	}

	/**
	 * Gets the vl_percentual value for this Tipo_Seguro.
	 * 
	 * @return vl_percentual
	 */
	public java.math.BigDecimal getVl_percentual() {
		return vl_percentual;
	}

	/**
	 * Sets the vl_percentual value for this Tipo_Seguro.
	 * 
	 * @param vl_percentual
	 */
	public void setVl_percentual(java.math.BigDecimal vl_percentual) {
		this.vl_percentual = vl_percentual;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Seguro))
			return false;
		Tipo_Seguro other = (Tipo_Seguro) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_empresa == null && other.getDs_empresa() == null)
						|| (this.ds_empresa != null && this.ds_empresa.equals(other.getDs_empresa())))
				&& ((this.ds_tipo_seguro == null && other.getDs_tipo_seguro() == null)
						|| (this.ds_tipo_seguro != null && this.ds_tipo_seguro.equals(other.getDs_tipo_seguro())))
				&& ((this.id_tipo_seguro == null && other.getId_tipo_seguro() == null)
						|| (this.id_tipo_seguro != null && this.id_tipo_seguro.equals(other.getId_tipo_seguro())))
				&& ((this.vl_percentual == null && other.getVl_percentual() == null)
						|| (this.vl_percentual != null && this.vl_percentual.equals(other.getVl_percentual())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_empresa() != null) {
			_hashCode += getDs_empresa().hashCode();
		}
		if (getDs_tipo_seguro() != null) {
			_hashCode += getDs_tipo_seguro().hashCode();
		}
		if (getId_tipo_seguro() != null) {
			_hashCode += getId_tipo_seguro().hashCode();
		}
		if (getVl_percentual() != null) {
			_hashCode += getVl_percentual().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Seguro.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Seguro"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_empresa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_empresa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_seguro");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_tipo_seguro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_seguro");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_tipo_seguro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_percentual");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_percentual"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
