/**
 * TabelaJuros.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class TabelaJuros  implements java.io.Serializable {
    private java.lang.String dsContato;

    private java.lang.String dsProduto;

    private java.lang.String dsTelefoneContato;

    private java.lang.String dsTipoPessoa;

    private java.lang.String dsTipoVeiculo;

    private java.util.Calendar dtValidadeFim;

    private java.util.Calendar dtValidadeInicio;

    private com.svs.fin.integracao.safra.model.FaixaAno[] faixasAnos;

    private com.svs.fin.integracao.safra.model.FaixaBQ[] faixasBQ;

    private java.lang.Integer idTabela;

    private com.svs.fin.integracao.safra.model.Marcas[] marcas;

    private java.lang.String nomeTabela;

    private java.lang.String[] revendas;

    public TabelaJuros() {
    }

    public TabelaJuros(
           java.lang.String dsContato,
           java.lang.String dsProduto,
           java.lang.String dsTelefoneContato,
           java.lang.String dsTipoPessoa,
           java.lang.String dsTipoVeiculo,
           java.util.Calendar dtValidadeFim,
           java.util.Calendar dtValidadeInicio,
           com.svs.fin.integracao.safra.model.FaixaAno[] faixasAnos,
           com.svs.fin.integracao.safra.model.FaixaBQ[] faixasBQ,
           java.lang.Integer idTabela,
           com.svs.fin.integracao.safra.model.Marcas[] marcas,
           java.lang.String nomeTabela,
           java.lang.String[] revendas) {
           this.dsContato = dsContato;
           this.dsProduto = dsProduto;
           this.dsTelefoneContato = dsTelefoneContato;
           this.dsTipoPessoa = dsTipoPessoa;
           this.dsTipoVeiculo = dsTipoVeiculo;
           this.dtValidadeFim = dtValidadeFim;
           this.dtValidadeInicio = dtValidadeInicio;
           this.faixasAnos = faixasAnos;
           this.faixasBQ = faixasBQ;
           this.idTabela = idTabela;
           this.marcas = marcas;
           this.nomeTabela = nomeTabela;
           this.revendas = revendas;
    }


    /**
     * Gets the dsContato value for this TabelaJuros.
     * 
     * @return dsContato
     */
    public java.lang.String getDsContato() {
        return dsContato;
    }


    /**
     * Sets the dsContato value for this TabelaJuros.
     * 
     * @param dsContato
     */
    public void setDsContato(java.lang.String dsContato) {
        this.dsContato = dsContato;
    }


    /**
     * Gets the dsProduto value for this TabelaJuros.
     * 
     * @return dsProduto
     */
    public java.lang.String getDsProduto() {
        return dsProduto;
    }


    /**
     * Sets the dsProduto value for this TabelaJuros.
     * 
     * @param dsProduto
     */
    public void setDsProduto(java.lang.String dsProduto) {
        this.dsProduto = dsProduto;
    }


    /**
     * Gets the dsTelefoneContato value for this TabelaJuros.
     * 
     * @return dsTelefoneContato
     */
    public java.lang.String getDsTelefoneContato() {
        return dsTelefoneContato;
    }


    /**
     * Sets the dsTelefoneContato value for this TabelaJuros.
     * 
     * @param dsTelefoneContato
     */
    public void setDsTelefoneContato(java.lang.String dsTelefoneContato) {
        this.dsTelefoneContato = dsTelefoneContato;
    }


    /**
     * Gets the dsTipoPessoa value for this TabelaJuros.
     * 
     * @return dsTipoPessoa
     */
    public java.lang.String getDsTipoPessoa() {
        return dsTipoPessoa;
    }


    /**
     * Sets the dsTipoPessoa value for this TabelaJuros.
     * 
     * @param dsTipoPessoa
     */
    public void setDsTipoPessoa(java.lang.String dsTipoPessoa) {
        this.dsTipoPessoa = dsTipoPessoa;
    }


    /**
     * Gets the dsTipoVeiculo value for this TabelaJuros.
     * 
     * @return dsTipoVeiculo
     */
    public java.lang.String getDsTipoVeiculo() {
        return dsTipoVeiculo;
    }


    /**
     * Sets the dsTipoVeiculo value for this TabelaJuros.
     * 
     * @param dsTipoVeiculo
     */
    public void setDsTipoVeiculo(java.lang.String dsTipoVeiculo) {
        this.dsTipoVeiculo = dsTipoVeiculo;
    }


    /**
     * Gets the dtValidadeFim value for this TabelaJuros.
     * 
     * @return dtValidadeFim
     */
    public java.util.Calendar getDtValidadeFim() {
        return dtValidadeFim;
    }


    /**
     * Sets the dtValidadeFim value for this TabelaJuros.
     * 
     * @param dtValidadeFim
     */
    public void setDtValidadeFim(java.util.Calendar dtValidadeFim) {
        this.dtValidadeFim = dtValidadeFim;
    }


    /**
     * Gets the dtValidadeInicio value for this TabelaJuros.
     * 
     * @return dtValidadeInicio
     */
    public java.util.Calendar getDtValidadeInicio() {
        return dtValidadeInicio;
    }


    /**
     * Sets the dtValidadeInicio value for this TabelaJuros.
     * 
     * @param dtValidadeInicio
     */
    public void setDtValidadeInicio(java.util.Calendar dtValidadeInicio) {
        this.dtValidadeInicio = dtValidadeInicio;
    }


    /**
     * Gets the faixasAnos value for this TabelaJuros.
     * 
     * @return faixasAnos
     */
    public com.svs.fin.integracao.safra.model.FaixaAno[] getFaixasAnos() {
        return faixasAnos;
    }


    /**
     * Sets the faixasAnos value for this TabelaJuros.
     * 
     * @param faixasAnos
     */
    public void setFaixasAnos(com.svs.fin.integracao.safra.model.FaixaAno[] faixasAnos) {
        this.faixasAnos = faixasAnos;
    }


    /**
     * Gets the faixasBQ value for this TabelaJuros.
     * 
     * @return faixasBQ
     */
    public com.svs.fin.integracao.safra.model.FaixaBQ[] getFaixasBQ() {
        return faixasBQ;
    }


    /**
     * Sets the faixasBQ value for this TabelaJuros.
     * 
     * @param faixasBQ
     */
    public void setFaixasBQ(com.svs.fin.integracao.safra.model.FaixaBQ[] faixasBQ) {
        this.faixasBQ = faixasBQ;
    }


    /**
     * Gets the idTabela value for this TabelaJuros.
     * 
     * @return idTabela
     */
    public java.lang.Integer getIdTabela() {
        return idTabela;
    }


    /**
     * Sets the idTabela value for this TabelaJuros.
     * 
     * @param idTabela
     */
    public void setIdTabela(java.lang.Integer idTabela) {
        this.idTabela = idTabela;
    }


    /**
     * Gets the marcas value for this TabelaJuros.
     * 
     * @return marcas
     */
    public com.svs.fin.integracao.safra.model.Marcas[] getMarcas() {
        return marcas;
    }


    /**
     * Sets the marcas value for this TabelaJuros.
     * 
     * @param marcas
     */
    public void setMarcas(com.svs.fin.integracao.safra.model.Marcas[] marcas) {
        this.marcas = marcas;
    }


    /**
     * Gets the nomeTabela value for this TabelaJuros.
     * 
     * @return nomeTabela
     */
    public java.lang.String getNomeTabela() {
        return nomeTabela;
    }


    /**
     * Sets the nomeTabela value for this TabelaJuros.
     * 
     * @param nomeTabela
     */
    public void setNomeTabela(java.lang.String nomeTabela) {
        this.nomeTabela = nomeTabela;
    }


    /**
     * Gets the revendas value for this TabelaJuros.
     * 
     * @return revendas
     */
    public java.lang.String[] getRevendas() {
        return revendas;
    }


    /**
     * Sets the revendas value for this TabelaJuros.
     * 
     * @param revendas
     */
    public void setRevendas(java.lang.String[] revendas) {
        this.revendas = revendas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TabelaJuros)) return false;
        TabelaJuros other = (TabelaJuros) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dsContato==null && other.getDsContato()==null) || 
             (this.dsContato!=null &&
              this.dsContato.equals(other.getDsContato()))) &&
            ((this.dsProduto==null && other.getDsProduto()==null) || 
             (this.dsProduto!=null &&
              this.dsProduto.equals(other.getDsProduto()))) &&
            ((this.dsTelefoneContato==null && other.getDsTelefoneContato()==null) || 
             (this.dsTelefoneContato!=null &&
              this.dsTelefoneContato.equals(other.getDsTelefoneContato()))) &&
            ((this.dsTipoPessoa==null && other.getDsTipoPessoa()==null) || 
             (this.dsTipoPessoa!=null &&
              this.dsTipoPessoa.equals(other.getDsTipoPessoa()))) &&
            ((this.dsTipoVeiculo==null && other.getDsTipoVeiculo()==null) || 
             (this.dsTipoVeiculo!=null &&
              this.dsTipoVeiculo.equals(other.getDsTipoVeiculo()))) &&
            ((this.dtValidadeFim==null && other.getDtValidadeFim()==null) || 
             (this.dtValidadeFim!=null &&
              this.dtValidadeFim.equals(other.getDtValidadeFim()))) &&
            ((this.dtValidadeInicio==null && other.getDtValidadeInicio()==null) || 
             (this.dtValidadeInicio!=null &&
              this.dtValidadeInicio.equals(other.getDtValidadeInicio()))) &&
            ((this.faixasAnos==null && other.getFaixasAnos()==null) || 
             (this.faixasAnos!=null &&
              java.util.Arrays.equals(this.faixasAnos, other.getFaixasAnos()))) &&
            ((this.faixasBQ==null && other.getFaixasBQ()==null) || 
             (this.faixasBQ!=null &&
              java.util.Arrays.equals(this.faixasBQ, other.getFaixasBQ()))) &&
            ((this.idTabela==null && other.getIdTabela()==null) || 
             (this.idTabela!=null &&
              this.idTabela.equals(other.getIdTabela()))) &&
            ((this.marcas==null && other.getMarcas()==null) || 
             (this.marcas!=null &&
              java.util.Arrays.equals(this.marcas, other.getMarcas()))) &&
            ((this.nomeTabela==null && other.getNomeTabela()==null) || 
             (this.nomeTabela!=null &&
              this.nomeTabela.equals(other.getNomeTabela()))) &&
            ((this.revendas==null && other.getRevendas()==null) || 
             (this.revendas!=null &&
              java.util.Arrays.equals(this.revendas, other.getRevendas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDsContato() != null) {
            _hashCode += getDsContato().hashCode();
        }
        if (getDsProduto() != null) {
            _hashCode += getDsProduto().hashCode();
        }
        if (getDsTelefoneContato() != null) {
            _hashCode += getDsTelefoneContato().hashCode();
        }
        if (getDsTipoPessoa() != null) {
            _hashCode += getDsTipoPessoa().hashCode();
        }
        if (getDsTipoVeiculo() != null) {
            _hashCode += getDsTipoVeiculo().hashCode();
        }
        if (getDtValidadeFim() != null) {
            _hashCode += getDtValidadeFim().hashCode();
        }
        if (getDtValidadeInicio() != null) {
            _hashCode += getDtValidadeInicio().hashCode();
        }
        if (getFaixasAnos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFaixasAnos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFaixasAnos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFaixasBQ() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFaixasBQ());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFaixasBQ(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdTabela() != null) {
            _hashCode += getIdTabela().hashCode();
        }
        if (getMarcas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMarcas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMarcas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNomeTabela() != null) {
            _hashCode += getNomeTabela().hashCode();
        }
        if (getRevendas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRevendas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRevendas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TabelaJuros.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelaJuros"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsContato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsContato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsProduto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsProduto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsTelefoneContato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsTelefoneContato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsTipoPessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsTipoPessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dsTipoVeiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DsTipoVeiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dtValidadeFim");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DtValidadeFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dtValidadeInicio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DtValidadeInicio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faixasAnos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixasAnos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaAno"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaAno"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faixasBQ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixasBQ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaBQ"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaBQ"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTabela");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "IdTabela"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marcas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeTabela");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "NomeTabela"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revendas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Revendas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
