/**
 * Contato_Cadastro.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Contato_Cadastro  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String email;

    private java.lang.String nm_referencia;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone;

    public Contato_Cadastro() {
    }

    public Contato_Cadastro(
           java.lang.String email,
           java.lang.String nm_referencia,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.email = email;
        this.nm_referencia = nm_referencia;
        this.telefone = telefone;
    }


    /**
     * Gets the email value for this Contato_Cadastro.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this Contato_Cadastro.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the nm_referencia value for this Contato_Cadastro.
     * 
     * @return nm_referencia
     */
    public java.lang.String getNm_referencia() {
        return nm_referencia;
    }


    /**
     * Sets the nm_referencia value for this Contato_Cadastro.
     * 
     * @param nm_referencia
     */
    public void setNm_referencia(java.lang.String nm_referencia) {
        this.nm_referencia = nm_referencia;
    }


    /**
     * Gets the telefone value for this Contato_Cadastro.
     * 
     * @return telefone
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Contato_Cadastro.
     * 
     * @param telefone
     */
    public void setTelefone(com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.telefone = telefone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Contato_Cadastro)) return false;
        Contato_Cadastro other = (Contato_Cadastro) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.nm_referencia==null && other.getNm_referencia()==null) || 
             (this.nm_referencia!=null &&
              this.nm_referencia.equals(other.getNm_referencia()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getNm_referencia() != null) {
            _hashCode += getNm_referencia().hashCode();
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Contato_Cadastro.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Contato_Cadastro"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
