package com.svs.fin.model.entities.interfaces;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface AuditavelDataHoraDeCriacao {

	@JsonIgnore
	public LocalDateTime getDataHoraDeCriacao();

	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao);
}
