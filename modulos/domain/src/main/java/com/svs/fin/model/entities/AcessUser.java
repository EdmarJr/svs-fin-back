package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Controle de Acesso a Usuers (CAD x LDAP)
 * 
 * @author wesley.mota
 *
 */
@Entity
@Table(name = "AcessUser", uniqueConstraints = { @UniqueConstraint(columnNames = "cpf"),
		@UniqueConstraint(columnNames = "login") })
@NamedQuery(name = AcessUser.NQ_OBTER_POR_CPF.NAME, query = AcessUser.NQ_OBTER_POR_CPF.JPQL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AcessUser implements Serializable {

	public static interface NQ_OBTER_POR_CPF {
		public static final String NAME = "AcessUser.obterPorCpf";
		public static final String JPQL = "Select a from AcessUser a where a.cpf = :" + NQ_OBTER_POR_CPF.NM_PM_CPF;
		public static final String NM_PM_CPF = "cpf";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ACESSUSER")
	@SequenceGenerator(name = "SEQ_ACESSUSER", sequenceName = "SEQ_ACESSUSER", allocationSize = 1)
	@Id
	private Long id;

	@Column(length = 100)
	private String nome;

	@OneToMany(mappedBy = "acessUser", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JsonProperty(access = Access.READ_ONLY)
	private List<AcessUserRoleUnidadeOrganizacional> acessos = new ArrayList<AcessUserRoleUnidadeOrganizacional>();

	@Column(length = 45)
	private String lotacao;

	@Column(length = 20)
	private String matricula;

	@Column(unique = true, length = 14, nullable = false)
	private String cpf;

	@Column(unique = true, length = 25, nullable = false)
	private String login;

	@Column(length = 50, nullable = false)
	private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLotacao() {
		return lotacao;
	}

	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<AcessUserRoleUnidadeOrganizacional> getAcessos() {
		return acessos;
	}

	public void setAcessos(List<AcessUserRoleUnidadeOrganizacional> acessos) {
		this.acessos = acessos;
	}

}

// @ManyToMany(targetEntity = Role.class, fetch = FetchType.LAZY)
// @JoinTable(name = "AcessUser_Roles", joinColumns = @JoinColumn(name = "id"),
// inverseJoinColumns = @JoinColumn(name = "id_role"))
// private List<Role> listRoles;
//
// @ManyToMany(targetEntity = GrupoPermissaoMenus.class, fetch = FetchType.LAZY)
// @JoinTable(name = "acessuser_grupos_menus", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "grupo_id", referencedColumnName = "id"))
// private List<GrupoPermissaoMenus> gruposPermissaoMenus;
//
// @ManyToMany(targetEntity = PerfilAcessoMenus.class, fetch = FetchType.LAZY)
// @JoinTable(name = "acessuser_perfis_menus", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "perfil_id", referencedColumnName = "id"))
// private List<PerfilAcessoMenus> perfisAcessoMenus;
//
// @Transient
// private List<Role> listRolesTransient;
//
// /**
// * Associa��o do usu�rio do Cad com o Grupo do Ad
// */
// @ManyToMany(targetEntity = Role.class, fetch = FetchType.LAZY, cascade =
// CascadeType.ALL)
// @JoinTable(name = "AcessUser_Roles_Ad", joinColumns = {
// @JoinColumn(referencedColumnName = "id", nullable = false, name = "id_user")
// }, inverseJoinColumns = {
// @JoinColumn(name = "id_role", nullable = false) })
// private List<Role> listRolesAd;
//
// /**
// * Associa��o do usu�rio do Cad com o Grupo do Ad. Suporte Laz
// */
// @Transient
// private List<Role> listRolesAdTransient;
//
// @ManyToOne
// @JoinColumn(referencedColumnName = "id_role")
// private Role role;
//
// @ManyToOne
// @JoinColumn(referencedColumnName = "id")
// private UnidadeOrganizacional unidadeOrganizacional;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_UnidadesRespFx", joinColumns = @JoinColumn(name
// = "id"), inverseJoinColumns = @JoinColumn(name = "id_unid_respfx"))
// private List<UnidadeOrganizacional> listOrganizRespFluxo;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_UniRespChamadosOtrs", joinColumns =
// @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name =
// "id_unid_respOtrs"))
// private List<UnidadeOrganizacional> listOrganizRespChamadosOtrs;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_UnidadesRH", joinColumns = @JoinColumn(name =
// "id_acessuser"), inverseJoinColumns = @JoinColumn(name = "id_unidaderh"))
// private List<UnidadeOrganizacional> listUnidadesRH;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_Unidadespesquisa", joinColumns =
// @JoinColumn(name = "id_acessuser"), inverseJoinColumns = @JoinColumn(name =
// "id_unidadepesquisa"))
// private List<UnidadeOrganizacional> listUnidadesPesquisaSatisfacao;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_UnidadesRespEgv", joinColumns = @JoinColumn(name
// = "id"), inverseJoinColumns = @JoinColumn(name = "id_unid_respegv"))
// private List<UnidadeOrganizacional> listOrganizEgv;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_UnidadesRespAud", joinColumns = @JoinColumn(name
// = "id"), inverseJoinColumns = @JoinColumn(name = "id_unid_respaud"))
// private List<UnidadeOrganizacional> listOrganizAud;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "AcessUser_FiliaisConsor", joinColumns = @JoinColumn(name =
// "id"), inverseJoinColumns = @JoinColumn(name = "id_filial_consor"))
// private List<UnidadeOrganizacional> listFiliaisConsor;
//
// @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
// @JoinTable(name = "AcessUser_FiliasAnuncio", inverseJoinColumns =
// @JoinColumn(name = "filial_id"))
// private List<UnidadeOrganizacional> listFiliaisAnuncio;
//
// @Column(nullable = false)
// private SimNao primeiroAcesso;
//
// private SimNao bloqueado;
//
// private SimNao userterceiro;
//
// private SimNao gestorpacote;
//
// @Enumerated(EnumType.STRING)
// private SimNao responsavelCentroCusto;
//
// /*
// * @OneToOne(targetEntity = RespCCusto.class)
// *
// * @JoinColumn(name = "respCCusto_id_user", referencedColumnName = "id_user")
// *
// * @ForeignKey(name = "fk_id_repcc") private RespCCusto respCCusto;
// */
//
// @OneToOne(targetEntity = Gerente.class)
// // @ForeignKey(name = "fk_id_ger")
// private Gerente gerente;
//
// @OneToOne(targetEntity = Diretor.class)
// @JoinColumn(foreignKey = @ForeignKey(name = "FK_BGOQH3MY1UMPEGQQD61MRUL92"))
// // @ForeignKey(name = "fk_id_dir")
// private Diretor diretor;
//
// private AcessoPastaQlikView acessoPastaQlikView;
//
// private GrupoCentroCustos departamentoDoacesso;
//
// private Date dtExpiracaoSenha;
//
// private String skinPadrao;
//
// @ManyToOne(fetch = FetchType.LAZY)
// @JoinColumn
// private SetorTrabalho setorTrabalho;
//
// // private EnumAcessoColunasEgv acessoColunasEgv;
//
// private SimNao colPassagemLoja;
//
// private SimNao colDespachante;
//
// private SimNao colSiqQcp;
//
// @OneToOne
// @JoinColumn(name = "id_carg", referencedColumnName = "id_cargo", foreignKey =
// @ForeignKey(name = "FK_Q5EPOP4988HENTV7C88UMOUSI"))
// // @ForeignKey(name = "fk_cargo")
// private Cargo cargo_Hierarquia;
//
// private String eMailWkf;
//
// private SimNao recebeWkf;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidadesRecbWkf", joinColumns = @JoinColumn(name =
// "user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> unidadesRecbWkf;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "departamentosUserWkf", joinColumns = @JoinColumn(name =
// "user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "id_dpto", referencedColumnName = "id"))
// private List<DepartamentosAcessuser> departamentosUserWkf;
//
// @ElementCollection(targetClass = EnumDepartamentoIQS.class)
// @JoinTable(name = "ACESSUSER_DPTO_IQS", joinColumns = @JoinColumn(name =
// "user_id"))
// @Column(name = "departamento_iqs", nullable = false)
// @Enumerated(EnumType.STRING)
// private List<EnumDepartamentoIQS> departamentosIqsWkf;
//
// @ManyToMany(fetch = FetchType.LAZY, cascade =
// javax.persistence.CascadeType.ALL)
// @JoinTable(name = "lotacoesUserWkf", joinColumns = @JoinColumn(name =
// "user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "id_lot_wkf_usuario", referencedColumnName = "id"))
// private List<LotacaoWorkflowUsuario> lotacoesUserWkf;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "workflowsUserWkf", joinColumns = @JoinColumn(name =
// "user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "id_workflow", referencedColumnName = "id"))
// private List<TarefasWorkFlow> tarefasUserWkf;
//
// @Transient
// private List<TarefasWorkFlow> tarefasUserWkTransient;
//
// private Calendar dthUltEnvWkf;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidadesCbc", joinColumns = @JoinColumn(name = "user_id",
// referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name =
// "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> unidadesCbc;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidadesFi", joinColumns = @JoinColumn(name = "user_id",
// referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name =
// "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> unidadesFi;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidsRespFpw", joinColumns = @JoinColumn(name = "user_id",
// referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name =
// "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> unidadesRespFpw;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "perfilAcessoUser", joinColumns = @JoinColumn(name =
// "user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "perfil_id", referencedColumnName = "id"))
// private List<PerfilAcesso> perfisAcesso;
//
// @Transient
// private List<UnidadeOrganizacional> unidadesRespFpwTransient;
//
// // para receber apenas workflow negativos
// private SimNao negativosWkf;
//
// @Transient
// private List<Role> listRolesCpo;
//
// @Transient
// private SimNao mostraTrocaSenha;
//
// @Transient
// private SimNao mostraTrocaUnidade;
//
// @Transient
// private String mensagem;
//
// @Transient
// Boolean mostraCompDiversos;
//
// @Transient
// private String veryfiPassword;
//
// @Transient
// private Boolean credValida;
//
// @Transient
// private List<UnidadeOrganizacional> listUnidadesRespUser;
//
// private boolean senhaExpirou;
//
// private String envioWkfErro;
//
// @Temporal(TemporalType.DATE)
// private Calendar dataDemissao;
//
// @Temporal(TemporalType.TIMESTAMP)
// private Calendar datUltimoAcesso;
//
// @Column(length = 75)
// private String userNameAd;
//
// @Column(length = 25)
// private String ipLogon;
//
// @Column(length = 125)
// private String departamentoAd;
//
// @Column(length = 3000)
// private String membroDe;
//
// private String emailAd;
//
// @Enumerated(EnumType.STRING)
// private SimNao acessaPeloAd;
//
// private String situacaoAtual;
//
// @Transient
// private EnumTipoRole enumTipoRoleAtual;
//
// /**
// * Controle de gerentes para autoriza��o de notas enviadas ao CSC
// */
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidsGpNfe", joinColumns = @JoinColumn(name = "user_id",
// referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name =
// "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> gerenteAutNfe;
//
// @Transient
// private List<UnidadeOrganizacional> gerenteAutNfeTransient;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_lancadores_nfe", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> lancadoresErpNfe;
//
// // @Transient
// // private userdetails userdetails;
//
// @ElementCollection(targetClass = EnumDepartamento.class)
// @CollectionTable(name = "departamentos_user")
// @Column(name = "departamento")
// @Enumerated(EnumType.STRING)
// private List<EnumDepartamento> listDepartamentosOrcamento;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidades_orcamento", joinColumns = @JoinColumn(name =
// "user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> listUnidadesOrcamento;
//
// // Valor limite para aprova��o de solicita��o de compra
// private Double valorLimiteAprovSolicitacao;
//
// @OneToOne(fetch = FetchType.LAZY)
// @JoinColumn(foreignKey = @ForeignKey(name = "FK_I7D31LJY4A5ODIHXDYOJI82HH"))
// private Faturista faturista;
//
// @OneToOne(mappedBy = "acessUser")
// private PermissaoIqs permissaoIqs;
//
// @OneToOne(mappedBy = "acessUser")
// private PermissaoCrm permissaoCrm;
//
// /*
// * Unidade bi-direcional aprovadores e gerentes
// */
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_gerentes_comerciais", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private List<UnidadeOrganizacional> gerentesComerciais;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_gerentes_financeiros", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private List<UnidadeOrganizacional> gerentesFinanceiros;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_ap_pag_contab", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> aprovadoresPagamentoContabilidade;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_pag_dp", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> aprovadoresPagamentoDP;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_pag_jur", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> aprovadoresPagamentoJuridico;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_pag_ri", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> aprovadoresPagamentoRI;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_pag_cp", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> aprovadoresPagamentoCP;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_gestor_emprestimo", joinColumns = @JoinColumn(name
// = "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> gestoresEmprestimo;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_aprovadores_emprestimo", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private List<UnidadeOrganizacional> aprovadoresEmprestimo;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_diret_aprov_gerais", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private List<UnidadeOrganizacional> diretoresAprovadoresAdmissaoGerais;
//
// @ManyToMany(fetch = FetchType.LAZY)
// @JoinTable(name = "unidade_diret_aprov_espec", joinColumns = @JoinColumn(name
// = "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> diretoresAprovadoresAdmissaoEspecificos;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "planejamentoPrimeiroAprovador", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> primeiroAprovadorPlanejamento;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "planejamentoSegundoAprovador", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> segundoAprovadorPlanejamento;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "planejamentoTerceiroAprovador", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> terceiroAprovadorPlanejamento;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "digNfLancadoresErps", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private Set<UnidadeOrganizacional> lancadoresErpsDigitalizacao;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "digNfAprovadoresImobilizado", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> aprovadoresImobilizado;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "digNfAprovMasterImobilizado", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> aprovadoresMasterImobilizado;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "preAprovDigi", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private Set<UnidadeOrganizacional> preAprovadoresDigitalizacaoNf;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "visualizadoresDigi", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private Set<UnidadeOrganizacional> visualizadoresDigitalizacao;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "diretorGeralDigitalizacao", joinColumns = @JoinColumn(name
// = "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private Set<UnidadeOrganizacional> diretorGeralDigitalizacao;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "visualizadoresSp", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private List<UnidadeOrganizacional> visualizadoresSolicitacaoPagamento;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "solCompraDiretorComercial", joinColumns = @JoinColumn(name
// = "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private Set<UnidadeOrganizacional> diretorComercialSolicitacaoCompra;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "solCompraDiretorGeral", joinColumns = @JoinColumn(name =
// "acess_user_id", referencedColumnName = "id"), inverseJoinColumns =
// @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
// private Set<UnidadeOrganizacional> diretorGeralSolicitacaoCompra;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_com_despachante", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> aprovadoresComercialDespachante;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_exec_despachante", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> aprovadoresExecucaoDespachante;
//
// @ManyToMany(targetEntity = UnidadeOrganizacional.class, fetch =
// FetchType.LAZY)
// @JoinTable(name = "unidade_aprov_fin_despachante", joinColumns =
// @JoinColumn(name = "acess_user_id", referencedColumnName = "id"),
// inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName =
// "id"))
// private Set<UnidadeOrganizacional> aprovadoresFinanceiroDespachante;
// /*
// * Fim itens bi-direcional unidades
// */
//
// @Column
// private Double valorInicioDigiWrk;
//
// @Column
// private Double valorFimDigiWrk;
//
// @Column
// private Boolean usuarioBloqueadoErpDigi;
//
// @Transient
// private String servidorNote;
//
// @Transient
// private List<LogOperCad> listaLogsAlteracao;
//
// // Aprova��o digitaliza��o SAGA BRASIL
//
// public AcessUser() {
//
// setBloqueado(SimNao.N�O);
// setRecebeWkf(SimNao.N�O);
//
// }
//
// /*
// * public AcessUser(String cpf, String login, String password, SimNao
// bloqueado,
// * SimNao userterceiro, SimNao gestorpacote, RespCCusto respCCusto, Role role,
// * AcessoPastaQlikView pastaQlikView) {
// */
// public AcessUser(String cpf, String login, String password, SimNao bloqueado,
// SimNao userterceiro,
// SimNao gestorpacote, Role role, AcessoPastaQlikView pastaQlikView) {
// super();
// this.cpf = cpf;
// this.login = login;
// this.password = password;
// this.bloqueado = bloqueado;
// this.userterceiro = userterceiro;
// this.gestorpacote = gestorpacote;
//
// this.role = role;
// this.acessoPastaQlikView = pastaQlikView;
//
// }
//
// public AcessUser(String cpf) {
// super();
// this.cpf = cpf;
// }
//
// @Override
// public int hashCode() {
// final int prime = 31;
// int result = 1;
// result = prime * result + ((id == null) ? 0 : id.hashCode());
// return result;
// }
//
// @Override
// public boolean equals(Object obj) {
// if (this == obj)
// return true;
// if (obj == null)
// return false;
// if (!(obj instanceof AcessUser))
// return false;
// final AcessUser other = (AcessUser) obj;
// if (getId() == null) {
// if (other.getId() != null)
// return false;
// } else if (!getId().equals(other.getId()))
// return false;
// return true;
// }
//
// /**
// *
// * @return Se o usu�rio est� desbloqueado ou � PJ. (Utilizado para
// * envio de workflows)
// *
// */
// public boolean isDesbloqueadoOuPJ() {
//
// if (this.getBloqueado().equals(SimNao.N�O)
// || (this.getBloqueado().equals(SimNao.SIM) &&
// this.getUserterceiro().equals(SimNao.SIM))) {
//
// return true;
// }
//
// return false;
// }
//
// public String getVeryfiPassword() {
// return veryfiPassword;
// }
//
// public void setVeryfiPassword(String veryfiPassword) {
// this.veryfiPassword = veryfiPassword;
// }
//
// public Diretor getDiretor() {
//
// if (getId() != null && diretor == null) {
// diretor = new Diretor();
//
// diretor.setCargo(getCargo_Hierarquia());
// setDiretor(diretor);
// }
//
// if (diretor != null) {
// diretor.setNomeDiretor(getNome());
// }
//
// return diretor;
// }
//
// public void setDiretor(Diretor diretor) {
// this.diretor = diretor;
// }
//
// public Role getRole() {
// return role;
// }
//
// public void setRole(Role role) {
// this.role = role;
// }
//
// public SimNao getGestorpacote() {
// return gestorpacote;
// }
//
// public void setGestorpacote(SimNao gestorpacote) {
// this.gestorpacote = gestorpacote;
// }
//
// public Long getId() {
// return id;
// }
//
// public void setId(Long id) {
// this.id = id;
// }
//
// public String getCpf() {
// return cpf;
// }
//
// public String getCpfSonumeros() {
//
// if (cpf != null) {
// return cpf.replace(".", "").replace("-", "");
// }
// return null;
// }
//
// public void setCpf(String cpf) {
// this.cpf = cpf;
// }
//
// public String getLogin() {
// return login;
// }
//
// public void setLogin(String login) {
// this.login = login;
// }
//
// public String getUsername() {
// return getLogin();
// }
//
// /*
// * @Override public void setUsername(String username) {
// * super.setUsername(username); login = username; }
// */
//
// public String getPassword() {
//
// return password;
// }
//
// public void setPassword(String password) {
// // super.setPassword(password);
// this.password = password;
// }
//
// public List<Role> getListRoles() {
//
// if (listRoles == null) {
// setListRoles(new ArrayList<Role>());
// }
// return listRoles;
// }
//
// public void setListRoles(List<Role> listRoles) {
// this.listRoles = listRoles;
// }
//
// public SimNao getPrimeiroAcesso() {
//
// if (primeiroAcesso == null) {
// primeiroAcesso = SimNao.N�O;
// }
// return primeiroAcesso;
// }
//
// public void setPrimeiroAcesso(SimNao primeiroAcesso) {
// this.primeiroAcesso = primeiroAcesso;
// }
//
// /**
// * Bloqueado por demiss�o ou por confirma��o no cadastro de PJ.
// *
// * @return
// */
// public SimNao getBloqueado() {
//
// if (getDataDemissao() != null) {
// bloqueado = SimNao.SIM;
// }
//
// return bloqueado;
// }
//
// public void setBloqueado(SimNao bloqueado) {
// this.bloqueado = bloqueado;
// }
//
// /*
// * public RespCCusto getRespCCusto() { return respCCusto; }
// *
// *
// *
// * public void setRespCCusto(RespCCusto respCCusto) { this.respCCusto =
// * respCCusto; }
// */
//
// public Gerente getGerente() {
// return gerente;
// }
//
// public void setGerente(Gerente gerente) {
// this.gerente = gerente;
// }
//
// /**
// * Funcion�rio PJ. Liberado no cadastro do CAD
// *
// * @return
// */
// public SimNao getUserterceiro() {
//
// if (userterceiro == null) {
// userterceiro = SimNao.N�O;
// }
//
// return userterceiro;
// }
//
// public void setUserterceiro(SimNao userterceiro) {
// this.userterceiro = userterceiro;
// }
//
// /*
// * @Override public boolean isInvalid() { return credValida == true ? false :
// * true; }
// */
//
// public Boolean getCredValida() {
// return credValida;
// }
//
// public void setCredValida(Boolean credValida) {
// this.credValida = credValida;
// }
//
// public AcessoPastaQlikView getAcessoPastaQlikView() {
// return acessoPastaQlikView;
// }
//
// public void setAcessoPastaQlikView(AcessoPastaQlikView acessoPastaQlikView) {
// this.acessoPastaQlikView = acessoPastaQlikView;
// }
//
// public SimNao getMostraTrocaSenha() {
// return mostraTrocaSenha;
// }
//
// public void setMostraTrocaSenha(SimNao mostraTrocaSenha) {
// this.mostraTrocaSenha = mostraTrocaSenha;
// }
//
// public String getMensagem() {
// return mensagem;
// }
//
// public void setMensagem(String mensagem) {
// this.mensagem = mensagem;
// }
//
// public Boolean getMostraCompDiversos() {
// return mostraCompDiversos;
// }
//
// public void setMostraCompDiversos(Boolean mostraCompDiversos) {
// this.mostraCompDiversos = mostraCompDiversos;
// }
//
// public UnidadeOrganizacional getUnidadeOrganizacional() {
// return unidadeOrganizacional;
// }
//
// public void setUnidadeOrganizacional(UnidadeOrganizacional
// unidadeOrganizacional) {
// this.unidadeOrganizacional = unidadeOrganizacional;
// }
//
// public SimNao getMostraTrocaUnidade() {
// return mostraTrocaUnidade;
// }
//
// public void setMostraTrocaUnidade(SimNao mostraTrocaUnidade) {
// this.mostraTrocaUnidade = mostraTrocaUnidade;
// }
//
// public GrupoCentroCustos getDepartamentoDoacesso() {
// return departamentoDoacesso;
// }
//
// public void setDepartamentoDoacesso(GrupoCentroCustos departamentoDoacesso) {
// this.departamentoDoacesso = departamentoDoacesso;
// }
//
// public Date getDtExpiracaoSenha() {
// return dtExpiracaoSenha;
// }
//
// public void setDtExpiracaoSenha(Date dtExpiracaoSenha) {
// this.dtExpiracaoSenha = dtExpiracaoSenha;
// }
//
// public List<Role> getListRolesCpo() {
// return listRolesCpo;
// }
//
// public void setListRolesCpo(List<Role> listRolesCpo) {
// this.listRolesCpo = listRolesCpo;
// }
//
// public String getSkinPadrao() {
// return skinPadrao;
// }
//
// public void setSkinPadrao(String skinPadrao) {
// this.skinPadrao = skinPadrao;
// }
//
// public List<UnidadeOrganizacional> getListOrganizRespFluxo() {
// return listOrganizRespFluxo;
// }
//
// public void setListOrganizRespFluxo(List<UnidadeOrganizacional>
// listOrganizRespFluxo) {
// this.listOrganizRespFluxo = listOrganizRespFluxo;
// }
//
// public List<UnidadeOrganizacional> getListOrganizRespChamadosOtrs() {
// return listOrganizRespChamadosOtrs;
// }
//
// public void setListOrganizRespChamadosOtrs(List<UnidadeOrganizacional>
// listOrganizRespChamadosOtrs) {
// this.listOrganizRespChamadosOtrs = listOrganizRespChamadosOtrs;
// }
//
// public List<UnidadeOrganizacional> getListUnidadesRespUser() {
// return listUnidadesRespUser;
// }
//
// public void setListUnidadesRespUser(List<UnidadeOrganizacional>
// listUnidadesRespUser) {
// this.listUnidadesRespUser = listUnidadesRespUser;
// }
//
// public List<UnidadeOrganizacional> getListOrganizEgv() {
//
// if (listOrganizEgv == null) {
// setListOrganizEgv(new ArrayList<UnidadeOrganizacional>());
// }
// return listOrganizEgv;
// }
//
// public void setListOrganizEgv(List<UnidadeOrganizacional> listOrganizEgv) {
// this.listOrganizEgv = listOrganizEgv;
// }
//
// public List<UnidadeOrganizacional> getListOrganizAud() {
// return listOrganizAud;
// }
//
// public void setListOrganizAud(List<UnidadeOrganizacional> listOrganizAud) {
// this.listOrganizAud = listOrganizAud;
// }
//
// /*
// * public EnumAcessoColunasEgv getAcessoColunasEgv() { return
// acessoColunasEgv;
// * }
// *
// *
// *
// * public void setAcessoColunasEgv(EnumAcessoColunasEgv acessoColunasEgv) {
// * this.acessoColunasEgv = acessoColunasEgv; }
// */
//
// public SimNao getColPassagemLoja() {
// return colPassagemLoja;
// }
//
// public SimNao getColDespachante() {
// return colDespachante;
// }
//
// public SimNao getColSiqQcp() {
// return colSiqQcp;
// }
//
// public void setColPassagemLoja(SimNao colPassagemLoja) {
// this.colPassagemLoja = colPassagemLoja;
// }
//
// public void setColDespachante(SimNao colDespachante) {
// this.colDespachante = colDespachante;
// }
//
// public void setColSiqQcp(SimNao colSiqQcp) {
// this.colSiqQcp = colSiqQcp;
// }
//
// /**
// * Cargo Atual no Fpw
// *
// * @return
// */
// public Cargo getCargo_Hierarquia() {
//
// return cargo_Hierarquia;
// }
//
// /**
// * Cargo Atual no Fpw
// *
// * @param cargo_Hierarquia
// */
// public void setCargo_Hierarquia(Cargo cargo_Hierarquia) {
// this.cargo_Hierarquia = cargo_Hierarquia;
// }
//
// public String getEMailWkf() {
// if (eMailWkf == null) {
// setEMailWkf("");
// }
//
// return eMailWkf;
// }
//
// public void setEMailWkf(String mailWkf) {
// eMailWkf = mailWkf;
// }
//
// public SimNao getRecebeWkf() {
//
// if (getUnidadesRecbWkf().isEmpty() == false) {
// recebeWkf = SimNao.SIM;
// } else {
// recebeWkf = SimNao.N�O;
// }
//
// return recebeWkf;
// }
//
// public void setRecebeWkf(SimNao recebeWkf) {
// this.recebeWkf = recebeWkf;
// }
//
// public List<UnidadeOrganizacional> getUnidadesRecbWkf() {
//
// if (unidadesRecbWkf == null) {
// setUnidadesRecbWkf(new ArrayList<UnidadeOrganizacional>());
// }
// return unidadesRecbWkf;
// }
//
// public void setUnidadesRecbWkf(List<UnidadeOrganizacional> unidadesRecbWkf) {
// this.unidadesRecbWkf = unidadesRecbWkf;
// }
//
// public List<DepartamentosAcessuser> getDepartamentosUserWkf() {
// return departamentosUserWkf;
// }
//
// public void setDepartamentosUserWkf(List<DepartamentosAcessuser>
// departamentosUserWkf) {
// this.departamentosUserWkf = departamentosUserWkf;
// }
//
// public List<TarefasWorkFlow> getTarefasUserWkf() {
//
// if (tarefasUserWkf == null) {
// setTarefasUserWkf(new ArrayList<TarefasWorkFlow>());
// }
// return tarefasUserWkf;
// }
//
// public void setTarefasUserWkf(List<TarefasWorkFlow> tarefasUserWkf) {
// this.tarefasUserWkf = tarefasUserWkf;
// }
//
// public Calendar getDthUltEnvWkf() {
// return dthUltEnvWkf;
// }
//
// public void setDthUltEnvWkf(Calendar dthUltEnvWkf) {
// this.dthUltEnvWkf = dthUltEnvWkf;
// }
//
// public SimNao getNegativosWkf() {
// return negativosWkf;
// }
//
// public void setNegativosWkf(SimNao negativosWkf) {
// this.negativosWkf = negativosWkf;
// }
//
// public void setListUnidadesRH(List<UnidadeOrganizacional> listUnidadesRH) {
// this.listUnidadesRH = listUnidadesRH;
// }
//
// public List<UnidadeOrganizacional> getListUnidadesRH() {
//
// if (listUnidadesRH == null) {
// listUnidadesRH = new ArrayList<UnidadeOrganizacional>();
// }
// return listUnidadesRH;
// }
//
// public void setResponsavelCentroCusto(SimNao responsavelCentroCusto) {
//
// if (responsavelCentroCusto == null) {
// responsavelCentroCusto = SimNao.N�O;
// }
// this.responsavelCentroCusto = responsavelCentroCusto;
// }
//
// public SimNao getResponsavelCentroCusto() {
// return responsavelCentroCusto;
// }
//
// public void setListUnidadesPesquisaSatisfacao(List<UnidadeOrganizacional>
// listUnidadesPesquisaSatisfacao) {
// this.listUnidadesPesquisaSatisfacao = listUnidadesPesquisaSatisfacao;
// }
//
// public List<UnidadeOrganizacional> getListUnidadesPesquisaSatisfacao() {
// return listUnidadesPesquisaSatisfacao;
// }
//
// public void setSetorTrabalho(SetorTrabalho setorTrabalho) {
// this.setorTrabalho = setorTrabalho;
// }
//
// public SetorTrabalho getSetorTrabalho() {
// return setorTrabalho;
// }
//
// public boolean isSenhaExpirou() {
// return senhaExpirou;
// }
//
// public void setSenhaExpirou(boolean senhaExpirou) {
// this.senhaExpirou = senhaExpirou;
// }
//
// public List<UnidadeOrganizacional> getListFiliaisConsor() {
// return listFiliaisConsor;
// }
//
// public void setListFiliaisConsor(List<UnidadeOrganizacional>
// listFiliaisConsor) {
// this.listFiliaisConsor = listFiliaisConsor;
// }
//
// public List<UnidadeOrganizacional> getUnidadesCbc() {
// return unidadesCbc;
// }
//
// public void setUnidadesCbc(List<UnidadeOrganizacional> unidadesCbc) {
// this.unidadesCbc = unidadesCbc;
// }
//
// public List<UnidadeOrganizacional> getUnidadesFi() {
// return unidadesFi;
// }
//
// public void setUnidadesFi(List<UnidadeOrganizacional> unidadesFi) {
// this.unidadesFi = unidadesFi;
// }
//
// public String geteMailWkf() {
// return eMailWkf;
// }
//
// public void seteMailWkf(String eMailWkf) {
// this.eMailWkf = eMailWkf;
// }
//
// /**
// * �ltima mensagem de envio de e-mail no workflow
// *
// * @return Mensagem
// */
// public String getEnvioWkfErro() {
// return envioWkfErro;
// }
//
// /**
// * Seta mensagem de envio de e-mial no usu�rio
// *
// * @param envioWkfErro
// */
// public void setEnvioWkfErro(String envioWkfErro) {
// this.envioWkfErro = envioWkfErro;
// }
//
// /**
// *
// * @return
// */
// public Calendar getDataDemissao() {
// return dataDemissao;
// }
//
// public void setDataDemissao(Calendar dataDemissao) {
// this.dataDemissao = dataDemissao;
// }
//
// public String getIpMaquina() {
//
// return "";
//
// }
//
// public String getNome() {
//
// if (nome == null) {
// return getLogin();
// }
// return nome;
// }
//
// public void setNome(String nome) {
// this.nome = nome;
// }
//
// public String getLotacao() {
//
// if (lotacao == null && getId() != null) {
// setLotacao((getUserterceiro() == SimNao.SIM ? "Terceiros" : "n/f"));
// }
// return lotacao;
// }
//
// public void setLotacao(String lotacao) {
// this.lotacao = lotacao;
// }
//
// public String getMatricula() {
// return matricula;
// }
//
// public void setMatricula(String matricula) {
// this.matricula = matricula;
// }
//
// public List<TarefasWorkFlow> getTarefasUserWkTransient() {
//
// if (tarefasUserWkTransient == null) {
// setTarefasUserWkTransient(new ArrayList<TarefasWorkFlow>());
// }
// return tarefasUserWkTransient;
// }
//
// public void setTarefasUserWkTransient(List<TarefasWorkFlow>
// tarefasUserWkTransient) {
// this.tarefasUserWkTransient = tarefasUserWkTransient;
// }
//
// public List<UnidadeOrganizacional> getUnidadesRespFpw() {
//
// return unidadesRespFpw;
// }
//
// public void setUnidadesRespFpw(List<UnidadeOrganizacional> unidadesRespFpw) {
// this.unidadesRespFpw = unidadesRespFpw;
// }
//
// public List<UnidadeOrganizacional> getUnidadesRespFpwTransient() {
//
// if (unidadesRespFpwTransient == null) {
// setUnidadesRespFpwTransient(getUnidadesRespFpw());
// }
// return unidadesRespFpwTransient;
// }
//
// public void setUnidadesRespFpwTransient(List<UnidadeOrganizacional>
// unidadesRespFpwTransient) {
//
// if (this.unidadesRespFpwTransient == null) {
// this.unidadesRespFpwTransient = getUnidadesRespFpw();
// } else {
//
// for (UnidadeOrganizacional unidNova : unidadesRespFpwTransient) {
//
// if (getUnidadesRespFpw().contains(unidNova) == false) {
// getUnidadesRespFpw().add(unidNova);
// }
// }
//
// }
//
// }
//
// public Calendar getDatUltimoAcesso() {
// return datUltimoAcesso;
// }
//
// public void setDatUltimoAcesso(Calendar datUltimoAcesso) {
// this.datUltimoAcesso = datUltimoAcesso;
// }
//
// public String getUserNameAd() {
// if (userNameAd == null) {
// setUserNameAd("");
// }
// return userNameAd;
// }
//
// public void setUserNameAd(String userNameAd) {
//
// if (userNameAd != null && userNameAd.contains("sAMAccountName:")) {
// this.userNameAd = (userNameAd.replace("sAMAccountName:", "")).trim();
// } else
// this.userNameAd = userNameAd;
// }
//
// public String getIpLogon() {
// return ipLogon;
// }
//
// public void setIpLogon(String ipLogon) {
// this.ipLogon = ipLogon;
// }
//
// public String getDepartamentoAd() {
//
// if (departamentoAd == null) {
// setDepartamentoAd("");
// }
// return departamentoAd;
// }
//
// public void setDepartamentoAd(String departamentoAd) {
// if (departamentoAd != null && departamentoAd.contains("department:")) {
// this.departamentoAd = departamentoAd.replace("department:", "");
// }
// this.departamentoAd = departamentoAd;
// }
//
// public String getMembroDe() {
//
// if (membroDe == null) {
// setMembroDe("");
// }
// return membroDe;
// }
//
// public void setMembroDe(String membroDe) {
// if (membroDe != null && membroDe.contains("distinguishedName:")) {
//
// this.membroDe = membroDe.replace("distinguishedName:", "");
// } else
// this.membroDe = membroDe;
// }
//
// public String getEmailAd() {
//
// if (emailAd == null) {
// setEmailAd("");
// }
// return emailAd;
// }
//
// public void setEmailAd(String emailAd) {
// this.emailAd = emailAd;
// }
//
// /**
// * Buscar grupo principal do Ad
// *
// * @return
// */
// public String getGrupoAcessoContaAd() {
// if (getMembroDe() != null && getMembroDe().toUpperCase().contains("OU=")) {
// return
// (getMembroDe().toUpperCase().split(",")[1].split(",OU=")[0]).replace("OU=",
// "");
// } else
// return "";
// }
//
// public List<LotacaoWorkflowUsuario> getLotacoesUserWkf() {
//
// if (lotacoesUserWkf == null) {
// setLotacoesUserWkf(new ArrayList<LotacaoWorkflowUsuario>());
// }
//
// return lotacoesUserWkf;
// }
//
// public void setLotacoesUserWkf(List<LotacaoWorkflowUsuario> lotacoesUserWkf)
// {
// this.lotacoesUserWkf = lotacoesUserWkf;
// }
//
// /**
// * Associa��o do usu�rio do Cad com o Grupo do Ad
// *
// * @return
// */
// public List<Role> getListRolesAd() {
//
// if (listRolesAd == null) {
// setListRolesAd(new ArrayList<Role>());
// }
// return listRolesAd;
// }
//
// /**
// * Associa��o do usu�rio do Cad com o Grupo do Ad. Sincronia
// *
// * @param listRolesAd
// */
// public void setListRolesAd(List<Role> listRolesAd) {
// this.listRolesAd = listRolesAd;
// }
//
// public List<Role> getListRolesAdTransient() {
//
// if (listRolesAdTransient == null) {
// // setListRolesAdTransient(getListRolesAd());
// this.listRolesAdTransient = getListRolesAd();
// }
// return listRolesAdTransient;
// }
//
// @Transient
// private List<Role> listTodasRolesCad;
//
// public List<Role> getListTodasRolesCad() {
// if (this.listTodasRolesCad == null) {
// this.listTodasRolesCad = new ArrayList<Role>();
// // Events.instance().raiseEvent("buscarListaRoles",
// // this.listTodasRolesCad);
// }
// return this.listTodasRolesCad;
// }
//
// /**
// * Sincronizar Roles Cad X Ad X Usu�rio
// *
// * @param listRolesAdTransient
// */
// public void setListRolesAdTransient(List<Role> listRolesAdTransient) {
//
// for (Role roleNova : listRolesAdTransient) {
//
// if (getListTodasRolesCad().contains(roleNova) == false) {
// getListRolesAdTransient().add(roleNova);
//
// } else {
// Role roleCad =
// getListTodasRolesCad().get(getListTodasRolesCad().indexOf(roleNova));
// if (getListRolesAdTransient().contains(roleCad) == false) {
// getListRolesAdTransient().add(roleCad);
// }
//
// }
//
// }
//
// for (Role roleUser : getListRolesAdTransient()) {
//
// if (listRolesAdTransient.contains(roleUser) == false) {
// roleUser.setRemoverAcessoUsuario(true);
// }
//
// }
//
// }
//
// public List<Role> getListRolesTransient() {
//
// if (listRolesTransient == null) {
// this.listRolesTransient = getListRoles();
// }
//
// return listRolesTransient;
// }
//
// public void setListRolesTransientNull() {
//
// this.listRolesTransient = null;
//
// }
//
// public void setListRolesTransient(List<Role> listRolesTransient) {
//
// for (Role roleNova : listRolesTransient) {
//
// if (this.listRolesTransient.contains(roleNova) == false) {
// this.listRolesTransient.add(roleNova);
//
// }
//
// }
//
// List<Role> roles = new ArrayList<Role>();
// for (Role roleAtual : this.listRolesTransient) {
//
// if (roleAtual.getTipoRole().equals(getEnumTipoRoleAtual())
// && listRolesTransient.contains(roleAtual) == false) {
// roles.add(roleAtual);
// }
//
// }
//
// this.listRolesTransient.removeAll(roles);
//
// }
//
// /**
// * Anteriormente j� acessou pelo Ad?
// *
// * @return SIM/NAO
// */
// public SimNao getAcessouPeloAd() {
//
// if (acessaPeloAd == null) {
// setAcessaPeloAd(SimNao.N�O);
// }
// return acessaPeloAd;
// }
//
// /**
// * Anteriormente j� acessou pelo Ad?
// *
// * @param acessaPeloAd
// */
// public void setAcessaPeloAd(SimNao acessaPeloAd) {
// this.acessaPeloAd = acessaPeloAd;
// }
//
// /**
// * Situa��o do colaborador junto ao FPW (ADMITIDO/DEMITIDO/NULL)
// *
// * @return
// */
// public String getSituacaoAtual() {
//
// if (situacaoAtual == null) {
// situacaoAtual = "";
// }
// return situacaoAtual;
// }
//
// /**
// * Situa��o do colaborador junto ao FPW
// *
// * @param situacaoAtual
// */
// public void setSituacaoAtual(String situacaoAtual) {
// this.situacaoAtual = situacaoAtual;
// }
//
// /**
// * Caso possua o nome do usu�rio no banco, retorna seu nome, caso contr�rio,
// * retorna o login do usu�rio.
// */
// public String getIdentificacao() {
//
// if (nome != null && !nome.equals("")) {
// return nome;
// }
//
// return login;
// }
//
// /**
// * Setar que tipo Role ser� tratado para exclus�o ou inclus�o
// *
// * @return
// */
// public EnumTipoRole getEnumTipoRoleAtual() {
// return enumTipoRoleAtual;
// }
//
// public void setEnumTipoRoleAtual(EnumTipoRole enumTipoRoleAtual) {
// this.enumTipoRoleAtual = enumTipoRoleAtual;
// }
//
// /**
// * Controle de gerentes para autoriza��o de notas enviadas ao CSC
// *
// * @return
// */
// public List<UnidadeOrganizacional> getGerenteAutNfe() {
// if (gerenteAutNfe == null) {
// setGerenteAutNfe(new ArrayList<UnidadeOrganizacional>());
// } else {
// setGerenteAutNfeTransient(gerenteAutNfe);
// }
// return gerenteAutNfe;
// }
//
// /**
// * Controle de gerentes para autoriza��o de notas enviadas ao CSC
// *
// * @param gerenteAutNfe
// */
// public void setGerenteAutNfe(List<UnidadeOrganizacional> gerenteAutNfe) {
// this.gerenteAutNfe = gerenteAutNfe;
// }
//
// /**
// * Controle de gerentes para autoriza��o de notas enviadas ao CSC
// *
// * @return
// */
// public List<UnidadeOrganizacional> getGerenteAutNfeTransient() {
//
// if (gerenteAutNfeTransient == null) {
// setGerenteAutNfeTransient(getGerenteAutNfe());
// }
// return gerenteAutNfeTransient;
// }
//
// /**
// * Controle de gerentes para autoriza��o de notas enviadas ao CSC
// *
// * @param gerenteAutNfeTransient
// */
// public void setGerenteAutNfeTransient(List<UnidadeOrganizacional>
// gerenteAutNfeTransient) {
//
// if (this.gerenteAutNfeTransient == null) {
// this.gerenteAutNfeTransient = gerenteAutNfeTransient;
// } else {
//
// for (UnidadeOrganizacional unid : gerenteAutNfeTransient) {
//
// if (this.gerenteAutNfeTransient.contains(unid) == false) {
//
// this.gerenteAutNfeTransient.add(unid);
//
// }
//
// }
//
// List<UnidadeOrganizacional> unidadedes = new
// ArrayList<UnidadeOrganizacional>();
// for (UnidadeOrganizacional unidAtual : gerenteAutNfeTransient) {
//
// if (gerenteAutNfeTransient.contains(unidAtual) == false) {
//
// unidadedes.add(unidAtual);
//
// }
//
// }
//
// if (unidadedes.isEmpty() == false) {
// this.gerenteAutNfeTransient.removeAll(unidadedes);
// }
//
// }
//
// // this.gerenteAutNfeTransient = gerenteAutNfeTransient;
// }
//
// /**
// * Session de login do usuário
// *
// * @return //
// */
// // public UserDetails getUserDetails() {
// // return userDetails;
// // }
// //
// // /**
// // * Session de login do usuário
// // *
// // * @param userDetails
// // */
// // public void setUserDetails(UserDetails userDetails) {
// // this.userDetails = userDetails;
// // }
//
// @Override
// public String toString() {
//
// if (nome != null) {
// return getNome() == null ? getLogin() : getNome().split(" ")[0] + " " +
// getNome().split(" ")[1];
// } else {
// return getLogin();
// }
//
// }
//
// public List<EnumDepartamento> getListDepartamentosOrcamento() {
// if (listDepartamentosOrcamento == null) {
// listDepartamentosOrcamento = new ArrayList<EnumDepartamento>();
// }
// return listDepartamentosOrcamento;
// }
//
// public void setListDepartamentosOrcamento(List<EnumDepartamento>
// listDepartamentosOrcamento) {
// this.listDepartamentosOrcamento = listDepartamentosOrcamento;
// }
//
// public List<UnidadeOrganizacional> getListUnidadesOrcamento() {
// if (listUnidadesOrcamento == null) {
// listUnidadesOrcamento = new ArrayList<UnidadeOrganizacional>();
// }
// return listUnidadesOrcamento;
// }
//
// public void setListUnidadesOrcamento(List<UnidadeOrganizacional>
// listUnidadesOrcamento) {
// this.listUnidadesOrcamento = listUnidadesOrcamento;
// }
//
// public Double getValorLimiteAprovSolicitacao() {
// return valorLimiteAprovSolicitacao;
// }
//
// public void setValorLimiteAprovSolicitacao(Double
// valorLimiteAprovSolicitacao) {
// this.valorLimiteAprovSolicitacao = valorLimiteAprovSolicitacao;
// }
//
// public Faturista getFaturista() {
//
// if (getId() != null && faturista == null) {
// faturista = new Faturista();
// faturista.setAtivo_inativo(Status.ATIVO);
// faturista.setUser(this);
// setFaturista(faturista);
// }
//
// faturista.setCargo(getCargo_Hierarquia());
//
// return faturista;
// }
//
// public void setFaturista(Faturista faturista) {
// this.faturista = faturista;
// }
//
// public Set<UnidadeOrganizacional> getPrimeiroAprovadorPlanejamento() {
// return primeiroAprovadorPlanejamento;
// }
//
// public void setPrimeiroAprovadorPlanejamento(Set<UnidadeOrganizacional>
// primeiroAprovadorPlanejamento) {
// this.primeiroAprovadorPlanejamento = primeiroAprovadorPlanejamento;
// }
//
// public List<UnidadeOrganizacional> getGerentesComerciais() {
// return gerentesComerciais;
// }
//
// public void setGerentesComerciais(List<UnidadeOrganizacional>
// gerentesComerciais) {
// this.gerentesComerciais = gerentesComerciais;
// }
//
// public List<UnidadeOrganizacional> getGerentesFinanceiros() {
// return gerentesFinanceiros;
// }
//
// public void setGerentesFinanceiros(List<UnidadeOrganizacional>
// gerentesFinanceiros) {
// this.gerentesFinanceiros = gerentesFinanceiros;
// }
//
// public List<UnidadeOrganizacional> getAprovadoresPagamentoContabilidade() {
// return aprovadoresPagamentoContabilidade;
// }
//
// public void setAprovadoresPagamentoContabilidade(List<UnidadeOrganizacional>
// aprovadoresPagamentoContabilidade) {
// this.aprovadoresPagamentoContabilidade = aprovadoresPagamentoContabilidade;
// }
//
// public List<UnidadeOrganizacional> getAprovadoresPagamentoDP() {
// return aprovadoresPagamentoDP;
// }
//
// public void setAprovadoresPagamentoDP(List<UnidadeOrganizacional>
// aprovadoresPagamentoDP) {
// this.aprovadoresPagamentoDP = aprovadoresPagamentoDP;
// }
//
// public List<UnidadeOrganizacional> getAprovadoresPagamentoJuridico() {
// return aprovadoresPagamentoJuridico;
// }
//
// public void setAprovadoresPagamentoJuridico(List<UnidadeOrganizacional>
// aprovadoresPagamentoJuridico) {
// this.aprovadoresPagamentoJuridico = aprovadoresPagamentoJuridico;
// }
//
// public List<UnidadeOrganizacional> getAprovadoresPagamentoRI() {
// return aprovadoresPagamentoRI;
// }
//
// public void setAprovadoresPagamentoRI(List<UnidadeOrganizacional>
// aprovadoresPagamentoRI) {
// this.aprovadoresPagamentoRI = aprovadoresPagamentoRI;
// }
//
// public List<UnidadeOrganizacional> getAprovadoresPagamentoCP() {
// return aprovadoresPagamentoCP;
// }
//
// public void setAprovadoresPagamentoCP(List<UnidadeOrganizacional>
// aprovadoresPagamentoCP) {
// this.aprovadoresPagamentoCP = aprovadoresPagamentoCP;
// }
//
// public List<UnidadeOrganizacional> getGestoresEmprestimo() {
// return gestoresEmprestimo;
// }
//
// public void setGestoresEmprestimo(List<UnidadeOrganizacional>
// gestoresEmprestimo) {
// this.gestoresEmprestimo = gestoresEmprestimo;
// }
//
// public List<UnidadeOrganizacional> getAprovadoresEmprestimo() {
// return aprovadoresEmprestimo;
// }
//
// public void setAprovadoresEmprestimo(List<UnidadeOrganizacional>
// aprovadoresEmprestimo) {
// this.aprovadoresEmprestimo = aprovadoresEmprestimo;
// }
//
// public List<UnidadeOrganizacional> getDiretoresAprovadoresAdmissaoGerais() {
// return diretoresAprovadoresAdmissaoGerais;
// }
//
// public void setDiretoresAprovadoresAdmissaoGerais(List<UnidadeOrganizacional>
// diretoresAprovadoresAdmissaoGerais) {
// this.diretoresAprovadoresAdmissaoGerais = diretoresAprovadoresAdmissaoGerais;
// }
//
// public List<UnidadeOrganizacional>
// getDiretoresAprovadoresAdmissaoEspecificos() {
// return diretoresAprovadoresAdmissaoEspecificos;
// }
//
// public void setDiretoresAprovadoresAdmissaoEspecificos(
// List<UnidadeOrganizacional> diretoresAprovadoresAdmissaoEspecificos) {
// this.diretoresAprovadoresAdmissaoEspecificos =
// diretoresAprovadoresAdmissaoEspecificos;
// }
//
// public Set<UnidadeOrganizacional> getSegundoAprovadorPlanejamento() {
// return segundoAprovadorPlanejamento;
// }
//
// public void setSegundoAprovadorPlanejamento(Set<UnidadeOrganizacional>
// segundoAprovadorPlanejamento) {
// this.segundoAprovadorPlanejamento = segundoAprovadorPlanejamento;
// }
//
// public Set<UnidadeOrganizacional> getTerceiroAprovadorPlanejamento() {
// return terceiroAprovadorPlanejamento;
// }
//
// public void setTerceiroAprovadorPlanejamento(Set<UnidadeOrganizacional>
// terceiroAprovadorPlanejamento) {
// this.terceiroAprovadorPlanejamento = terceiroAprovadorPlanejamento;
// }
//
// public Set<UnidadeOrganizacional> getLancadoresErpsDigitalizacao() {
// return lancadoresErpsDigitalizacao;
// }
//
// public void setLancadoresErpsDigitalizacao(Set<UnidadeOrganizacional>
// lancadoresErpsDigitalizacao) {
// this.lancadoresErpsDigitalizacao = lancadoresErpsDigitalizacao;
// }
//
// public Set<UnidadeOrganizacional> getAprovadoresImobilizado() {
// return aprovadoresImobilizado;
// }
//
// public void setAprovadoresImobilizado(Set<UnidadeOrganizacional>
// aprovadoresImobilizado) {
// this.aprovadoresImobilizado = aprovadoresImobilizado;
// }
//
// public Set<UnidadeOrganizacional> getAprovadoresMasterImobilizado() {
// return aprovadoresMasterImobilizado;
// }
//
// public void setAprovadoresMasterImobilizado(Set<UnidadeOrganizacional>
// aprovadoresMasterImobilizado) {
// this.aprovadoresMasterImobilizado = aprovadoresMasterImobilizado;
// }
//
// public Set<UnidadeOrganizacional> getPreAprovadoresDigitalizacaoNf() {
// return preAprovadoresDigitalizacaoNf;
// }
//
// public void setPreAprovadoresDigitalizacaoNf(Set<UnidadeOrganizacional>
// preAprovadoresDigitalizacaoNf) {
// this.preAprovadoresDigitalizacaoNf = preAprovadoresDigitalizacaoNf;
// }
//
// public Set<UnidadeOrganizacional> getVisualizadoresDigitalizacao() {
// return visualizadoresDigitalizacao;
// }
//
// public void setVisualizadoresDigitalizacao(Set<UnidadeOrganizacional>
// visualizadoresDigitalizacao) {
// this.visualizadoresDigitalizacao = visualizadoresDigitalizacao;
// }
//
// public Set<UnidadeOrganizacional> getDiretorGeralDigitalizacao() {
// return diretorGeralDigitalizacao;
// }
//
// public void setDiretorGeralDigitalizacao(Set<UnidadeOrganizacional>
// diretorGeralDigitalizacao) {
// this.diretorGeralDigitalizacao = diretorGeralDigitalizacao;
// }
//
// public Set<UnidadeOrganizacional> getDiretorComercialSolicitacaoCompra() {
// return diretorComercialSolicitacaoCompra;
// }
//
// public void setDiretorComercialSolicitacaoCompra(Set<UnidadeOrganizacional>
// diretorComercialSolicitacaoCompra) {
// this.diretorComercialSolicitacaoCompra = diretorComercialSolicitacaoCompra;
// }
//
// public Set<UnidadeOrganizacional> getDiretorGeralSolicitacaoCompra() {
// return diretorGeralSolicitacaoCompra;
// }
//
// public void setDiretorGeralSolicitacaoCompra(Set<UnidadeOrganizacional>
// diretorGeralSolicitacaoCompra) {
// this.diretorGeralSolicitacaoCompra = diretorGeralSolicitacaoCompra;
// }
//
// public String getServidorNote() {
// return servidorNote;
// }
//
// public void setServidorNote(String servidorNote) {
// this.servidorNote = servidorNote;
// }
//
// public List<EnumDepartamentoIQS> getDepartamentosIqsWkf() {
// if (departamentosIqsWkf == null) {
// departamentosIqsWkf = new ArrayList<EnumDepartamentoIQS>();
// }
// return departamentosIqsWkf;
// }
//
// public void setDepartamentosIqsWkf(List<EnumDepartamentoIQS>
// departamentosIqsWkf) {
// this.departamentosIqsWkf = departamentosIqsWkf;
// }
//
// public PermissaoIqs getPermissaoIqs() {
// return permissaoIqs;
// }
//
// public void setPermissaoIqs(PermissaoIqs permissaoIqs) {
// this.permissaoIqs = permissaoIqs;
// }
//
// public List<PerfilAcesso> getPerfisAcesso() {
// if (perfisAcesso == null) {
// perfisAcesso = new ArrayList<PerfilAcesso>();
// }
// return perfisAcesso;
// }
//
// public void setPerfisAcesso(List<PerfilAcesso> perfisAcesso) {
// this.perfisAcesso = perfisAcesso;
// }
//
// public List<UnidadeOrganizacional> getVisualizadoresSolicitacaoPagamento() {
// return visualizadoresSolicitacaoPagamento;
// }
//
// public void setVisualizadoresSolicitacaoPagamento(List<UnidadeOrganizacional>
// visualizadoresSolicitacaoPagamento) {
// this.visualizadoresSolicitacaoPagamento = visualizadoresSolicitacaoPagamento;
// }
//
// public List<UnidadeOrganizacional> getLancadoresErpNfe() {
// if (lancadoresErpNfe == null) {
// lancadoresErpNfe = new ArrayList<UnidadeOrganizacional>();
// }
// return lancadoresErpNfe;
// }
//
// public void setLancadoresErpNfe(List<UnidadeOrganizacional> lancadoresErpNfe)
// {
// this.lancadoresErpNfe = lancadoresErpNfe;
// }
//
// public Set<UnidadeOrganizacional> getAprovadoresComercialDespachante() {
// return aprovadoresComercialDespachante;
// }
//
// public void setAprovadoresComercialDespachante(Set<UnidadeOrganizacional>
// aprovadoresComercialDespachante) {
// this.aprovadoresComercialDespachante = aprovadoresComercialDespachante;
// }
//
// public Set<UnidadeOrganizacional> getAprovadoresExecucaoDespachante() {
// return aprovadoresExecucaoDespachante;
// }
//
// public void setAprovadoresExecucaoDespachante(Set<UnidadeOrganizacional>
// aprovadoresExecucaoDespachante) {
// this.aprovadoresExecucaoDespachante = aprovadoresExecucaoDespachante;
// }
//
// public Set<UnidadeOrganizacional> getAprovadoresFinanceiroDespachante() {
// return aprovadoresFinanceiroDespachante;
// }
//
// public void setAprovadoresFinanceiroDespachante(Set<UnidadeOrganizacional>
// aprovadoresFinanceiroDespachante) {
// this.aprovadoresFinanceiroDespachante = aprovadoresFinanceiroDespachante;
// }
//
// public Double getValorInicioDigiWrk() {
// return valorInicioDigiWrk;
// }
//
// public void setValorInicioDigiWrk(Double valorInicioDigiWrk) {
// this.valorInicioDigiWrk = valorInicioDigiWrk;
// }
//
// public Double getValorFimDigiWrk() {
// return valorFimDigiWrk;
// }
//
// public void setValorFimDigiWrk(Double valorFimDigiWrk) {
// this.valorFimDigiWrk = valorFimDigiWrk;
// }
//
// public Boolean getUsuarioBloqueadoErpDigi() {
// if (usuarioBloqueadoErpDigi == null) {
// usuarioBloqueadoErpDigi = Boolean.FALSE;
// }
// return usuarioBloqueadoErpDigi;
// }
//
// public void setUsuarioBloqueadoErpDigi(Boolean usuarioBloqueadoErpDigi) {
// this.usuarioBloqueadoErpDigi = usuarioBloqueadoErpDigi;
// }
//
// public PermissaoCrm getPermissaoCrm() {
// return permissaoCrm;
// }
//
// public void setPermissaoCrm(PermissaoCrm permissaoCrm) {
// this.permissaoCrm = permissaoCrm;
// }
//
// public List<UnidadeOrganizacional> getListFiliaisAnuncio() {
// return listFiliaisAnuncio;
// }
//
// public void setListFiliaisAnuncio(List<UnidadeOrganizacional>
// listFiliaisAnuncio) {
// this.listFiliaisAnuncio = listFiliaisAnuncio;
// }
//
// public List<GrupoPermissaoMenus> getGruposPermissaoMenus() {
// if (gruposPermissaoMenus == null) {
// gruposPermissaoMenus = new ArrayList<GrupoPermissaoMenus>();
// }
// return gruposPermissaoMenus;
// }
//
// public void setGruposPermissaoMenus(List<GrupoPermissaoMenus>
// gruposPermissaoMenus) {
// this.gruposPermissaoMenus = gruposPermissaoMenus;
// }
//
// public List<PerfilAcessoMenus> getPerfisAcessoMenus() {
// if (perfisAcessoMenus == null) {
// perfisAcessoMenus = new ArrayList<PerfilAcessoMenus>();
// }
// return perfisAcessoMenus;
// }
//
// public void setPerfisAcessoMenus(List<PerfilAcessoMenus> perfisAcessoMenus) {
// this.perfisAcessoMenus = perfisAcessoMenus;
// }
//
// public List<LogOperCad> getListaLogsAlteracao() {
// if (listaLogsAlteracao == null) {
// setListaLogsAlteracao(new ArrayList<LogOperCad>());
// }
// return listaLogsAlteracao;
// }
//
// public void setListaLogsAlteracao(List<LogOperCad> listaLogsAlteracao) {
// this.listaLogsAlteracao = listaLogsAlteracao;
// }
// }
