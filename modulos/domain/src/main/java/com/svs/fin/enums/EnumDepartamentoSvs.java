package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumDepartamentoSvs {

	@JsonProperty("Novos")
	NOVOS("Novos"), @JsonProperty("Seminovos")
	SEMINOVOS("Seminovos"), @JsonProperty("Venda Direta")
	VENDA_DIRETA("Venda Direta");

	private String label;

	EnumDepartamentoSvs(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}
