package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

import com.svs.fin.integracaosantander.santander.dto.EnumTipoVeiculo;

public class Vehicle implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7256169932183304963L;

	private String vehicleTypeId;

	private String modelId;

	private String purchaseValue;

	private String adapted;

	private String taxi;

	private String fuelYearId;

	private String driverLicense;

	private String stateId;

	private String brandId;
	
	private EnumTipoVeiculo tipoVeiculo;
	
	
	public Vehicle() {}
	
	public Vehicle(EnumTipoVeiculo tipo) {
		this.tipoVeiculo = tipo;
	}
	
	
	public String getVehicleTypeId() {
		return vehicleTypeId;
	}

	public void setVehicleTypeId(String vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getPurchaseValue() {
		return purchaseValue;
	}

	public void setPurchaseValue(String purchaseValue) {
		this.purchaseValue = purchaseValue;
	}

	public String getAdapted() {
		return adapted;
	}

	public void setAdapted(String adapted) {
		this.adapted = adapted;
	}

	public String getTaxi() {
		return taxi;
	}

	public void setTaxi(String taxi) {
		this.taxi = taxi;
	}

	public String getFuelYearId() {
		return fuelYearId;
	}

	public void setFuelYearId(String fuelYearId) {
		this.fuelYearId = fuelYearId;
	}

	public String getDriverLicense() {
		return driverLicense;
	}

	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	@Override
	public String toString() {
		return "ClassPojo [vehicleTypeId = " + vehicleTypeId + ", modelId = " + modelId + ", purchaseValue = "
				+ purchaseValue + ", adapted = " + adapted + ", taxi = " + taxi + ", fuelYearId = " + fuelYearId
				+ ", driverLicense = " + driverLicense + ", stateId = " + stateId + ", brandId = " + brandId + "]";
	}

	public EnumTipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(EnumTipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adapted == null) ? 0 : adapted.hashCode());
		result = prime * result + ((brandId == null) ? 0 : brandId.hashCode());
		result = prime * result + ((driverLicense == null) ? 0 : driverLicense.hashCode());
		result = prime * result + ((fuelYearId == null) ? 0 : fuelYearId.hashCode());
		result = prime * result + ((modelId == null) ? 0 : modelId.hashCode());
		result = prime * result + ((purchaseValue == null) ? 0 : purchaseValue.hashCode());
		result = prime * result + ((stateId == null) ? 0 : stateId.hashCode());
		result = prime * result + ((taxi == null) ? 0 : taxi.hashCode());
		result = prime * result + ((tipoVeiculo == null) ? 0 : tipoVeiculo.hashCode());
		result = prime * result + ((vehicleTypeId == null) ? 0 : vehicleTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (adapted == null) {
			if (other.adapted != null)
				return false;
		} else if (!adapted.equals(other.adapted))
			return false;
		if (brandId == null) {
			if (other.brandId != null)
				return false;
		} else if (!brandId.equals(other.brandId))
			return false;
		if (driverLicense == null) {
			if (other.driverLicense != null)
				return false;
		} else if (!driverLicense.equals(other.driverLicense))
			return false;
		if (fuelYearId == null) {
			if (other.fuelYearId != null)
				return false;
		} else if (!fuelYearId.equals(other.fuelYearId))
			return false;
		if (modelId == null) {
			if (other.modelId != null)
				return false;
		} else if (!modelId.equals(other.modelId))
			return false;
		if (purchaseValue == null) {
			if (other.purchaseValue != null)
				return false;
		} else if (!purchaseValue.equals(other.purchaseValue))
			return false;
		if (stateId == null) {
			if (other.stateId != null)
				return false;
		} else if (!stateId.equals(other.stateId))
			return false;
		if (taxi == null) {
			if (other.taxi != null)
				return false;
		} else if (!taxi.equals(other.taxi))
			return false;
		if (tipoVeiculo != other.tipoVeiculo)
			return false;
		if (vehicleTypeId == null) {
			if (other.vehicleTypeId != null)
				return false;
		} else if (!vehicleTypeId.equals(other.vehicleTypeId))
			return false;
		return true;
	}
}
