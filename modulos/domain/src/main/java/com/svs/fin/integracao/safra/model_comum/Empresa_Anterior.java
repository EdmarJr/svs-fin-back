/**
 * Empresa_Anterior.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Empresa_Anterior  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String nm_empresa;

    private java.lang.String qt_anos;

    private java.lang.String qt_meses;

    public Empresa_Anterior() {
    }

    public Empresa_Anterior(
           java.lang.String nm_empresa,
           java.lang.String qt_anos,
           java.lang.String qt_meses) {
        this.nm_empresa = nm_empresa;
        this.qt_anos = qt_anos;
        this.qt_meses = qt_meses;
    }


    /**
     * Gets the nm_empresa value for this Empresa_Anterior.
     * 
     * @return nm_empresa
     */
    public java.lang.String getNm_empresa() {
        return nm_empresa;
    }


    /**
     * Sets the nm_empresa value for this Empresa_Anterior.
     * 
     * @param nm_empresa
     */
    public void setNm_empresa(java.lang.String nm_empresa) {
        this.nm_empresa = nm_empresa;
    }


    /**
     * Gets the qt_anos value for this Empresa_Anterior.
     * 
     * @return qt_anos
     */
    public java.lang.String getQt_anos() {
        return qt_anos;
    }


    /**
     * Sets the qt_anos value for this Empresa_Anterior.
     * 
     * @param qt_anos
     */
    public void setQt_anos(java.lang.String qt_anos) {
        this.qt_anos = qt_anos;
    }


    /**
     * Gets the qt_meses value for this Empresa_Anterior.
     * 
     * @return qt_meses
     */
    public java.lang.String getQt_meses() {
        return qt_meses;
    }


    /**
     * Sets the qt_meses value for this Empresa_Anterior.
     * 
     * @param qt_meses
     */
    public void setQt_meses(java.lang.String qt_meses) {
        this.qt_meses = qt_meses;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Empresa_Anterior)) return false;
        Empresa_Anterior other = (Empresa_Anterior) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.nm_empresa==null && other.getNm_empresa()==null) || 
             (this.nm_empresa!=null &&
              this.nm_empresa.equals(other.getNm_empresa()))) &&
            ((this.qt_anos==null && other.getQt_anos()==null) || 
             (this.qt_anos!=null &&
              this.qt_anos.equals(other.getQt_anos()))) &&
            ((this.qt_meses==null && other.getQt_meses()==null) || 
             (this.qt_meses!=null &&
              this.qt_meses.equals(other.getQt_meses())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNm_empresa() != null) {
            _hashCode += getNm_empresa().hashCode();
        }
        if (getQt_anos() != null) {
            _hashCode += getQt_anos().hashCode();
        }
        if (getQt_meses() != null) {
            _hashCode += getQt_meses().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Empresa_Anterior.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Empresa_Anterior"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qt_anos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "qt_anos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qt_meses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "qt_meses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
