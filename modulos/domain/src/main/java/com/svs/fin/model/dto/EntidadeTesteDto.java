package com.svs.fin.model.dto;

import io.swagger.annotations.ApiModel;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;	

@ApiModel(description = "Entidade teste")
public class EntidadeTesteDto {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ENTIDADE_TESTE")
	@SequenceGenerator(name = "SEQ_ENTIDADE_TESTE", sequenceName = "SEQ_ENTIDADE_TESTE", allocationSize = 1)
	private Long id;

	@NotNull
	@Size(max = 255, min = 3)
	private String descricao;

	public EntidadeTesteDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(@NotNull String descricao) {
		this.descricao = descricao;
	}
}
