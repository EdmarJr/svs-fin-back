/**
 * Sede_Social.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Sede_Social")
public class Sede_Social implements java.io.Serializable {
	private java.lang.String ds_sede_social;

	@Id
	private java.lang.Integer id_sede_social;

	public Sede_Social() {
	}

	public Sede_Social(java.lang.String ds_sede_social, java.lang.Integer id_sede_social) {
		this.ds_sede_social = ds_sede_social;
		this.id_sede_social = id_sede_social;
	}

	/**
	 * Gets the ds_sede_social value for this Sede_Social.
	 * 
	 * @return ds_sede_social
	 */
	public java.lang.String getDs_sede_social() {
		return ds_sede_social;
	}

	/**
	 * Sets the ds_sede_social value for this Sede_Social.
	 * 
	 * @param ds_sede_social
	 */
	public void setDs_sede_social(java.lang.String ds_sede_social) {
		this.ds_sede_social = ds_sede_social;
	}

	/**
	 * Gets the id_sede_social value for this Sede_Social.
	 * 
	 * @return id_sede_social
	 */
	public java.lang.Integer getId_sede_social() {
		return id_sede_social;
	}

	/**
	 * Sets the id_sede_social value for this Sede_Social.
	 * 
	 * @param id_sede_social
	 */
	public void setId_sede_social(java.lang.Integer id_sede_social) {
		this.id_sede_social = id_sede_social;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Sede_Social))
			return false;
		Sede_Social other = (Sede_Social) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_sede_social == null && other.getDs_sede_social() == null)
						|| (this.ds_sede_social != null && this.ds_sede_social.equals(other.getDs_sede_social())))
				&& ((this.id_sede_social == null && other.getId_sede_social() == null)
						|| (this.id_sede_social != null && this.id_sede_social.equals(other.getId_sede_social())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_sede_social() != null) {
			_hashCode += getDs_sede_social().hashCode();
		}
		if (getId_sede_social() != null) {
			_hashCode += getId_sede_social().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Sede_Social.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sede_Social"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_sede_social");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_sede_social"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_sede_social");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_sede_social"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
