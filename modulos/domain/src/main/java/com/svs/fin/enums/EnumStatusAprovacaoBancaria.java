package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumStatusAprovacaoBancaria {

	@JsonProperty("Não enviado")
	NAO_ENVIADO("Não enviado"), @JsonProperty("Aguardando aprovação")
	AGUARDANDO_APROVACAO("Aguardando aprovação"), @JsonProperty("Aprovado")
	APROVADO("Aprovado"), @JsonProperty("Reprovado")
	REPROVADO("Reprovado"), @JsonProperty("Não enviado (reanálise)")
	NAO_ENVIADO_REANALISE("Não enviado (reanálise)"), @JsonProperty("Reanálise")
	REANALISE("Reanálise");

	private EnumStatusAprovacaoBancaria(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
