package com.svs.fin.model.entities.listeners;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;

import com.svs.fin.model.entities.interfaces.AuditavelDataHoraDeCriacao;

public class ListenerEntidadesAuditaveisDataHoraCriacao {

	@PrePersist
	public <T extends AuditavelDataHoraDeCriacao> void definirDataHoraCriacao(T entidade) {
		entidade.setDataHoraDeCriacao(LocalDateTime.now());
	}
}
