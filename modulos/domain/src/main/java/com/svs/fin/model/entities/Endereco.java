package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumTipoIPTU;
import com.svs.fin.enums.EnumTipoLogradouro;
import com.svs.fin.enums.EnumTipoResidenciaSVS;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = Endereco.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = Endereco.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
public class Endereco implements Serializable {

	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "Endereco.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from Endereco e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	private static final long serialVersionUID = 393996944983548069L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ENDERECO")
	@SequenceGenerator(name = "SEQ_ENDERECO", sequenceName = "SEQ_ENDERECO", allocationSize = 1)
	private Long id;

	@Column
	private Long codigoDealerWorkflow;

	@ManyToOne
	@JoinColumn(name = "id_tipo_endereco")
	private TipoEndereco tipoEndereco;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoLogradouro tipoLogradouro;

	@ManyToOne()
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	private ClienteSvs clienteSvs;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoResidenciaSVS tipoResidencia;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoIPTU enumTipoIPTU;

	@Column(length = 100)
	private String cep;

	@Column(length = 2000)
	private String logradouro;

	@Column(length = 50)
	private String numero;

	@Column(length = 2000)
	private String complemento;

	@ManyToOne
	@JoinColumn(name = "id_municipio")
	private Municipio municipio;

	@Column(length = 200)
	private String bairro;

	@Column
	private Calendar naCidadeDesde;

	@Column
	private Calendar noImovelDesde;

	@Transient
	private Long tipoLogradouroCodigoDealerWorkflow;

	@Transient
	private Long tipoEnderecoCodigoDealerWorkflow;

	@Transient
	private Long codigoMunicipioIbge;

	@Transient
	private String codigoEstado;

	@Transient
	private Boolean editando;

	@Transient
	private String enderecoString;

	public String getEnderecoString() {

		enderecoString = getLogradouro();

		if (getNumero() != null) {
			enderecoString += " n� " + getNumero();
		}
		if (getBairro() != null) {
			enderecoString += " - " + getBairro();
		}
		if (getMunicipio() != null) {
			enderecoString += " - " + getMunicipio().getNome();
			enderecoString += " - " + getMunicipio().getEstado().getSigla();
		}
		if (getCep() != null) {
			enderecoString += " - Cep " + getCep();
		}
		if (getComplemento() != null && !getComplemento().trim().equals("")) {
			enderecoString += " - Compl.: " + getComplemento();
		}
		return enderecoString;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(TipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public EnumTipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(EnumTipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Calendar getNaCidadeDesde() {
		return naCidadeDesde;
	}

	public void setNaCidadeDesde(Calendar naCidadeDesde) {
		this.naCidadeDesde = naCidadeDesde;
	}

	public Calendar getNoImovelDesde() {
		return noImovelDesde;
	}

	public void setNoImovelDesde(Calendar noImovelDesde) {
		this.noImovelDesde = noImovelDesde;
	}

	public EnumTipoResidenciaSVS getTipoResidencia() {
		return tipoResidencia;
	}

	public void setTipoResidencia(EnumTipoResidenciaSVS tipoResidencia) {
		this.tipoResidencia = tipoResidencia;
	}

	public EnumTipoIPTU getEnumTipoIPTU() {
		return enumTipoIPTU;
	}

	public void setEnumTipoIPTU(EnumTipoIPTU enumTipoIPTU) {
		this.enumTipoIPTU = enumTipoIPTU;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(Long codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public Long getTipoLogradouroCodigoDealerWorkflow() {
		return tipoLogradouroCodigoDealerWorkflow;
	}

	public void setTipoLogradouroCodigoDealerWorkflow(Long tipoLogradouroCodigoDealerWorkflow) {
		this.tipoLogradouroCodigoDealerWorkflow = tipoLogradouroCodigoDealerWorkflow;
	}

	public Long getTipoEnderecoCodigoDealerWorkflow() {
		return tipoEnderecoCodigoDealerWorkflow;
	}

	public void setTipoEnderecoCodigoDealerWorkflow(Long tipoEnderecoCodigoDealerWorkflow) {
		this.tipoEnderecoCodigoDealerWorkflow = tipoEnderecoCodigoDealerWorkflow;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getCodigoMunicipioIbge() {
		return codigoMunicipioIbge;
	}

	public void setCodigoMunicipioIbge(Long codigoMunicipioIbge) {
		this.codigoMunicipioIbge = codigoMunicipioIbge;
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public void setEnderecoString(String enderecoString) {
		this.enderecoString = enderecoString;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}
}
