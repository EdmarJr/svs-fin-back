/**
 * Referencia_Bancaria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Referencia_Bancaria  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_banco;

    private java.lang.String nr_agencia;

    private java.lang.String nr_ano;

    private java.lang.String nr_conta;

    private java.lang.String nr_mes;

    private com.svs.fin.integracao.safra.model_comum.Telefone telefone;

    private java.lang.String vl_limite;

    public Referencia_Bancaria() {
    }

    public Referencia_Bancaria(
           java.lang.String id_banco,
           java.lang.String nr_agencia,
           java.lang.String nr_ano,
           java.lang.String nr_conta,
           java.lang.String nr_mes,
           com.svs.fin.integracao.safra.model_comum.Telefone telefone,
           java.lang.String vl_limite) {
        this.id_banco = id_banco;
        this.nr_agencia = nr_agencia;
        this.nr_ano = nr_ano;
        this.nr_conta = nr_conta;
        this.nr_mes = nr_mes;
        this.telefone = telefone;
        this.vl_limite = vl_limite;
    }


    /**
     * Gets the id_banco value for this Referencia_Bancaria.
     * 
     * @return id_banco
     */
    public java.lang.String getId_banco() {
        return id_banco;
    }


    /**
     * Sets the id_banco value for this Referencia_Bancaria.
     * 
     * @param id_banco
     */
    public void setId_banco(java.lang.String id_banco) {
        this.id_banco = id_banco;
    }


    /**
     * Gets the nr_agencia value for this Referencia_Bancaria.
     * 
     * @return nr_agencia
     */
    public java.lang.String getNr_agencia() {
        return nr_agencia;
    }


    /**
     * Sets the nr_agencia value for this Referencia_Bancaria.
     * 
     * @param nr_agencia
     */
    public void setNr_agencia(java.lang.String nr_agencia) {
        this.nr_agencia = nr_agencia;
    }


    /**
     * Gets the nr_ano value for this Referencia_Bancaria.
     * 
     * @return nr_ano
     */
    public java.lang.String getNr_ano() {
        return nr_ano;
    }


    /**
     * Sets the nr_ano value for this Referencia_Bancaria.
     * 
     * @param nr_ano
     */
    public void setNr_ano(java.lang.String nr_ano) {
        this.nr_ano = nr_ano;
    }


    /**
     * Gets the nr_conta value for this Referencia_Bancaria.
     * 
     * @return nr_conta
     */
    public java.lang.String getNr_conta() {
        return nr_conta;
    }


    /**
     * Sets the nr_conta value for this Referencia_Bancaria.
     * 
     * @param nr_conta
     */
    public void setNr_conta(java.lang.String nr_conta) {
        this.nr_conta = nr_conta;
    }


    /**
     * Gets the nr_mes value for this Referencia_Bancaria.
     * 
     * @return nr_mes
     */
    public java.lang.String getNr_mes() {
        return nr_mes;
    }


    /**
     * Sets the nr_mes value for this Referencia_Bancaria.
     * 
     * @param nr_mes
     */
    public void setNr_mes(java.lang.String nr_mes) {
        this.nr_mes = nr_mes;
    }


    /**
     * Gets the telefone value for this Referencia_Bancaria.
     * 
     * @return telefone
     */
    public com.svs.fin.integracao.safra.model_comum.Telefone getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Referencia_Bancaria.
     * 
     * @param telefone
     */
    public void setTelefone(com.svs.fin.integracao.safra.model_comum.Telefone telefone) {
        this.telefone = telefone;
    }


    /**
     * Gets the vl_limite value for this Referencia_Bancaria.
     * 
     * @return vl_limite
     */
    public java.lang.String getVl_limite() {
        return vl_limite;
    }


    /**
     * Sets the vl_limite value for this Referencia_Bancaria.
     * 
     * @param vl_limite
     */
    public void setVl_limite(java.lang.String vl_limite) {
        this.vl_limite = vl_limite;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Referencia_Bancaria)) return false;
        Referencia_Bancaria other = (Referencia_Bancaria) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_banco==null && other.getId_banco()==null) || 
             (this.id_banco!=null &&
              this.id_banco.equals(other.getId_banco()))) &&
            ((this.nr_agencia==null && other.getNr_agencia()==null) || 
             (this.nr_agencia!=null &&
              this.nr_agencia.equals(other.getNr_agencia()))) &&
            ((this.nr_ano==null && other.getNr_ano()==null) || 
             (this.nr_ano!=null &&
              this.nr_ano.equals(other.getNr_ano()))) &&
            ((this.nr_conta==null && other.getNr_conta()==null) || 
             (this.nr_conta!=null &&
              this.nr_conta.equals(other.getNr_conta()))) &&
            ((this.nr_mes==null && other.getNr_mes()==null) || 
             (this.nr_mes!=null &&
              this.nr_mes.equals(other.getNr_mes()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone()))) &&
            ((this.vl_limite==null && other.getVl_limite()==null) || 
             (this.vl_limite!=null &&
              this.vl_limite.equals(other.getVl_limite())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_banco() != null) {
            _hashCode += getId_banco().hashCode();
        }
        if (getNr_agencia() != null) {
            _hashCode += getNr_agencia().hashCode();
        }
        if (getNr_ano() != null) {
            _hashCode += getNr_ano().hashCode();
        }
        if (getNr_conta() != null) {
            _hashCode += getNr_conta().hashCode();
        }
        if (getNr_mes() != null) {
            _hashCode += getNr_mes().hashCode();
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        if (getVl_limite() != null) {
            _hashCode += getVl_limite().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Referencia_Bancaria.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_banco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_banco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_agencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_agencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_ano");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_ano"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_conta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_conta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nr_mes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nr_mes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_limite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_limite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
