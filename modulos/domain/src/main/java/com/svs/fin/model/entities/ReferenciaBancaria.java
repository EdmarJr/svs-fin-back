package com.svs.fin.model.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumTipoConta;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferenciaBancaria implements Serializable {

	private static final long serialVersionUID = 8959028467685083269L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REFERENCIABANCARIA")
	@SequenceGenerator(name = "SEQ_REFERENCIABANCARIA", sequenceName = "SEQ_REFERENCIABANCARIA", allocationSize = 1)
	private Long id;

	@Column(length = 200)
	private String nome;

	@ManyToOne
	@JoinColumn(name = "id_banco")
	private Banco banco;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoConta tipoConta;

	@Column(length = 200)
	private String agencia;

	@Column(length = 200)
	private String digitoAgencia;

	@Column(length = 200)
	private String conta;

	@Column(length = 200)
	private String digitoConta;

	@ManyToOne
	@JoinColumn(name = "id_estado")
	private Estado estado;

	@ManyToOne
	@JoinColumn(name = "id_cidade")
	private Cidade cidade;

	@Column(length = 200)
	private String contato;

	@Column(length = 200)
	private String telefoneContato;

	@Column
	private Calendar clienteDesde;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public EnumTipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(EnumTipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getDigitoAgencia() {
		return digitoAgencia;
	}

	public void setDigitoAgencia(String digitoAgencia) {
		this.digitoAgencia = digitoAgencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getDigitoConta() {
		return digitoConta;
	}

	public void setDigitoConta(String digitoConta) {
		this.digitoConta = digitoConta;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getTelefoneContato() {
		return telefoneContato;
	}

	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Calendar getClienteDesde() {
		return clienteDesde;
	}

	public void setClienteDesde(Calendar clienteDesde) {
		this.clienteDesde = clienteDesde;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReferenciaBancaria other = (ReferenciaBancaria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
