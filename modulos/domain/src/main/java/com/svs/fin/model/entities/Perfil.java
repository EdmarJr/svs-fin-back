package com.svs.fin.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.svs.fin.model.entities.interfaces.Desativavel;
import com.svs.fin.model.entities.interfaces.Identificavel;

@Entity
@Table(name = "perfilsvs")
public class Perfil implements Desativavel, Identificavel<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PERFILSVS")
	@SequenceGenerator(name = "SEQ_PERFILSVS", sequenceName = "SEQ_PERFILSVS", allocationSize = 1)
	private Long id;
	@Column(name = "ativo")
	private Boolean ativo;
	@Column(name = "nome")
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
