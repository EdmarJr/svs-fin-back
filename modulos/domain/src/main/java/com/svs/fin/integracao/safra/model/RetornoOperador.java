/**
 * RetornoOperador.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class RetornoOperador  implements java.io.Serializable {
    private java.lang.String ds_nome;

    private java.lang.Integer id_operador;

    public RetornoOperador() {
    }

    public RetornoOperador(
           java.lang.String ds_nome,
           java.lang.Integer id_operador) {
           this.ds_nome = ds_nome;
           this.id_operador = id_operador;
    }


    /**
     * Gets the ds_nome value for this RetornoOperador.
     * 
     * @return ds_nome
     */
    public java.lang.String getDs_nome() {
        return ds_nome;
    }


    /**
     * Sets the ds_nome value for this RetornoOperador.
     * 
     * @param ds_nome
     */
    public void setDs_nome(java.lang.String ds_nome) {
        this.ds_nome = ds_nome;
    }


    /**
     * Gets the id_operador value for this RetornoOperador.
     * 
     * @return id_operador
     */
    public java.lang.Integer getId_operador() {
        return id_operador;
    }


    /**
     * Sets the id_operador value for this RetornoOperador.
     * 
     * @param id_operador
     */
    public void setId_operador(java.lang.Integer id_operador) {
        this.id_operador = id_operador;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetornoOperador)) return false;
        RetornoOperador other = (RetornoOperador) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ds_nome==null && other.getDs_nome()==null) || 
             (this.ds_nome!=null &&
              this.ds_nome.equals(other.getDs_nome()))) &&
            ((this.id_operador==null && other.getId_operador()==null) || 
             (this.id_operador!=null &&
              this.id_operador.equals(other.getId_operador())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDs_nome() != null) {
            _hashCode += getDs_nome().hashCode();
        }
        if (getId_operador() != null) {
            _hashCode += getId_operador().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetornoOperador.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoOperador"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_nome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_nome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_operador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_operador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
