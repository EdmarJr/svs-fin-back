package com.svs.fin.model.dto.listagemtabelalazy;

public class ParametroOrdenacao {
	private String tipoOrdenacao;
	private String nomeCampo;

	public ParametroOrdenacao(String nomeCampo, String tipoOrdenacao) {
		this.nomeCampo = nomeCampo;
		this.tipoOrdenacao = tipoOrdenacao;
		// TODO Auto-generated constructor stub
	}

	public String getTipoOrdenacao() {
		return tipoOrdenacao;
	}

	public void setTipoOrdenacao(String tipoOrdenacao) {
		this.tipoOrdenacao = tipoOrdenacao;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}
	
	
}
