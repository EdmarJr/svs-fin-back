package com.svs.fin.integracaosantander.santander.dto;

public class ModeloDTO {

	private String id;
	private String description;
	private String integrationCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIntegrationCode() {
		return integrationCode;
	}
	public void setIntegrationCode(String integrationCode) {
		this.integrationCode = integrationCode;
	}
}
