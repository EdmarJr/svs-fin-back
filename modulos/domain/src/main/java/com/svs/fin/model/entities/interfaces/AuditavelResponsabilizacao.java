package com.svs.fin.model.entities.interfaces;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.svs.fin.model.entities.Usuario;

public interface AuditavelResponsabilizacao {

	@JsonIgnore
	public Usuario getUsuarioResponsavelUltimaAlteracao();

	public void setUsuarioResponsavelUltimaAlteracao(Usuario usuarioResponsavelUltimaAlteracao);
}
