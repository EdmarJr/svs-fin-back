package com.svs.fin.integracaoItau;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gruposaga.sdk.zflow.model.gen.FinanceTransaction;

@Entity(name="JSON_TRANSAC_REQ_ITAU")
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonTransactionRequestItau implements Serializable {

	private static final long serialVersionUID = 2275200152625510352L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@Column
	private Calendar data;
	
	@Column
	private String nomeCliente;
	
	@Column
	private String cpfCliente;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	private byte[] jsonByte;
	
	@Transient
	private String jsonTexto;
	
	@Transient
	private FinanceTransaction financeTransaction;

	public String getJsonTexto() {
		if(jsonByte != null && jsonTexto == null) {
			setJsonTexto(new String(jsonByte, Charset.forName("UTF-8")));
		}
		return jsonTexto;
	}
	
	public String getJsonTextoFormatado() {
		String jsonTexto = getJsonTexto();
//		if(jsonTexto != null && !jsonTexto.trim().equals("")) {
//			FinanceTransaction financeTransaction = new Gson().fromJson(getJsonTexto(), FinanceTransaction.class);
//			return StringUtils.getObjetoComoJsonFormatado(financeTransaction);
//		}
		return jsonTexto;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public byte[] getJsonByte() {
		return jsonByte;
	}

	public void setJsonByte(byte[] jsonByte) {
		this.jsonByte = jsonByte;
		this.jsonTexto = null;
	}

	public void setJsonTexto(String jsonTexto) {
		this.jsonTexto = jsonTexto;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public FinanceTransaction getFinanceTransaction() {
		if(financeTransaction == null) {
			this.financeTransaction = new FinanceTransaction();
		}
		return financeTransaction;
	}

	public void setFinanceTransaction(FinanceTransaction financeTransaction) {
		this.financeTransaction = financeTransaction;
	}
}

