package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class PaymentForms implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6583894241787791041L;

	private String id;

	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", description = " + description + "]";
	}
}
