package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class FirstInstallment implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -87688082429394256L;

	private String maximumDate;

	private String minimumDate;

	private String recommendedDate;

	public String getMaximumDate() {
		return maximumDate;
	}

	public void setMaximumDate(String maximumDate) {
		this.maximumDate = maximumDate;
	}

	public String getMinimumDate() {
		return minimumDate;
	}

	public void setMinimumDate(String minimumDate) {
		this.minimumDate = minimumDate;
	}

	public String getRecommendedDate() {
		return recommendedDate;
	}

	public void setRecommendedDate(String recommendedDate) {
		this.recommendedDate = recommendedDate;
	}

	@Override
	public String toString() {
		return "ClassPojo [maximumDate = " + maximumDate + ", minimumDate = " + minimumDate + ", recommendedDate = "
				+ recommendedDate + "]";
	}
}
