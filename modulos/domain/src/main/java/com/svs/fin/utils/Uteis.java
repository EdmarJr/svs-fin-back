package com.svs.fin.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

public class Uteis {
	
	public static String BANCO_DADOS_EXECUCAO;
	
	public static LocalDate asLocalDate(Calendar calendar) {
        return asLocalDate(calendar.getTime(), ZoneId.systemDefault());
    }
	
	public static LocalDate asLocalDate(java.util.Date date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Date)
            return ((java.sql.Date) date).toLocalDate();
        else
            return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDate();
    }

	public static String removeCaracteresCpfCnpj(String cpf) {
        if (cpf != null) {
            cpf = cpf.replace(".", "").replace("-", "").replace("/", "");
            return cpf.trim();
        }
        return "";
    }

}
