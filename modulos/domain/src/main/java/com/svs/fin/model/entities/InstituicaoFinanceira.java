package com.svs.fin.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.svs.fin.model.entities.interfaces.AuditavelResponsabilizacao;
import com.svs.fin.model.entities.interfaces.AuditavelTemporalmente;
import com.svs.fin.model.entities.interfaces.Identificavel;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisResponsabilizacao;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisTemporalmente;

@Entity
@EntityListeners({ ListenerEntidadesAuditaveisResponsabilizacao.class, ListenerEntidadesAuditaveisTemporalmente.class })
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = InstituicaoFinanceira.NQ_OBTER_POR_NOME_INSTITUICAO.NAME, query = InstituicaoFinanceira.NQ_OBTER_POR_NOME_INSTITUICAO.JPQL) })
public class InstituicaoFinanceira
		implements Serializable, AuditavelResponsabilizacao, AuditavelTemporalmente, Identificavel<Long> {

	public static interface NQ_OBTER_POR_NOME_INSTITUICAO {
		public static final String NAME = "InstituicaoFinanceira.obterPorNome";
		public static final String JPQL = "Select DISTINCT t from InstituicaoFinanceira t where t.nome = :"
				+ NQ_OBTER_POR_NOME_INSTITUICAO.NM_PM_NOME;
		public static final String NM_PM_NOME = "nome";
	}

	private static final long serialVersionUID = -8496054790535798220L;

	public InstituicaoFinanceira() {
		setCnpjsAssociados(new ArrayList<>());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_INSTITUICAOFINANCEIRA")
	@SequenceGenerator(name = "SEQ_INSTITUICAOFINANCEIRA", sequenceName = "SEQ_INSTITUICAOFINANCEIRA", allocationSize = 1)
	private Long id;

	@Column(name = "criadoem")
	private LocalDateTime dataHoraDeCriacao;

	@Column(name = "alteradoem")
	private LocalDateTime dataHoraDeAtualizacao;

	@ManyToOne
	@JoinColumn(name = "id_usuario_criacao_alteracao")
	private Usuario usuarioResponsavelUltimaAlteracao;

	@Column(length = 50)
	private String codigo;

	@Column(length = 200)
	private String nome;

	@Column(length = 200)
	private String razaoSocial;

	@Column
	private Long codigoDealerWorkflow;

	@Column(name = "ativa")
	private Boolean ativo;

	@JsonProperty(access = Access.WRITE_ONLY)
	@OneToMany(mappedBy = "instituicaoFinanceira", targetEntity = CnpjInstituicaoFinanceira.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<CnpjInstituicaoFinanceira> cnpjsAssociados;

	// @JsonIgnore
	// @OneToMany(mappedBy = "instituicaoFinanceira", targetEntity =
	// TaxaPerfilInstituicaoFinanceira.class, cascade = CascadeType.ALL,
	// orphanRemoval = true, fetch = FetchType.LAZY)
	// private List<TaxaPerfilInstituicaoFinanceira> taxasPorPerfil;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public List<CnpjInstituicaoFinanceira> getCnpjsAssociados() {
		if (cnpjsAssociados == null) {
			cnpjsAssociados = new ArrayList<CnpjInstituicaoFinanceira>();
		}
		return cnpjsAssociados;
	}

	public void setCnpjsAssociados(List<CnpjInstituicaoFinanceira> cnpjsAssociados) {
		this.cnpjsAssociados = cnpjsAssociados;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	// public List<TaxaPerfilInstituicaoFinanceira> getTaxasPorPerfil() {
	// if (taxasPorPerfil == null) {
	// taxasPorPerfil = new ArrayList<TaxaPerfilInstituicaoFinanceira>();
	// }
	// return taxasPorPerfil;
	// }
	//
	// public void setTaxasPorPerfil(List<TaxaPerfilInstituicaoFinanceira>
	// taxasPorPerfil) {
	// this.taxasPorPerfil = taxasPorPerfil;
	// }

	public Long getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(Long codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public LocalDateTime getDataHoraDeCriacao() {
		return dataHoraDeCriacao;
	}

	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao) {
		this.dataHoraDeCriacao = dataHoraDeCriacao;
	}

	public LocalDateTime getDataHoraDeAtualizacao() {
		return dataHoraDeAtualizacao;
	}

	public void setDataHoraDeAtualizacao(LocalDateTime dataHoraDeAtualizacao) {
		this.dataHoraDeAtualizacao = dataHoraDeAtualizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstituicaoFinanceira other = (InstituicaoFinanceira) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Usuario getUsuarioResponsavelUltimaAlteracao() {
		return usuarioResponsavelUltimaAlteracao;
	}

	public void setUsuarioResponsavelUltimaAlteracao(Usuario usuarioResponsavelUltimaAlteracao) {
		this.usuarioResponsavelUltimaAlteracao = usuarioResponsavelUltimaAlteracao;
	}

}
