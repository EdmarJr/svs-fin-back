/**
 * Conjuge_Dados_Basicos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Conjuge_Dados_Basicos  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String dt_nascimento;

    private java.lang.String id_cliente;

    private java.lang.String id_compromisso;

    private java.lang.String id_nacionalidade;

    private java.lang.String id_sexo;

    private java.lang.String nm_cliente_rsoc;

    public Conjuge_Dados_Basicos() {
    }

    public Conjuge_Dados_Basicos(
           java.lang.String dt_nascimento,
           java.lang.String id_cliente,
           java.lang.String id_compromisso,
           java.lang.String id_nacionalidade,
           java.lang.String id_sexo,
           java.lang.String nm_cliente_rsoc) {
        this.dt_nascimento = dt_nascimento;
        this.id_cliente = id_cliente;
        this.id_compromisso = id_compromisso;
        this.id_nacionalidade = id_nacionalidade;
        this.id_sexo = id_sexo;
        this.nm_cliente_rsoc = nm_cliente_rsoc;
    }


    /**
     * Gets the dt_nascimento value for this Conjuge_Dados_Basicos.
     * 
     * @return dt_nascimento
     */
    public java.lang.String getDt_nascimento() {
        return dt_nascimento;
    }


    /**
     * Sets the dt_nascimento value for this Conjuge_Dados_Basicos.
     * 
     * @param dt_nascimento
     */
    public void setDt_nascimento(java.lang.String dt_nascimento) {
        this.dt_nascimento = dt_nascimento;
    }


    /**
     * Gets the id_cliente value for this Conjuge_Dados_Basicos.
     * 
     * @return id_cliente
     */
    public java.lang.String getId_cliente() {
        return id_cliente;
    }


    /**
     * Sets the id_cliente value for this Conjuge_Dados_Basicos.
     * 
     * @param id_cliente
     */
    public void setId_cliente(java.lang.String id_cliente) {
        this.id_cliente = id_cliente;
    }


    /**
     * Gets the id_compromisso value for this Conjuge_Dados_Basicos.
     * 
     * @return id_compromisso
     */
    public java.lang.String getId_compromisso() {
        return id_compromisso;
    }


    /**
     * Sets the id_compromisso value for this Conjuge_Dados_Basicos.
     * 
     * @param id_compromisso
     */
    public void setId_compromisso(java.lang.String id_compromisso) {
        this.id_compromisso = id_compromisso;
    }


    /**
     * Gets the id_nacionalidade value for this Conjuge_Dados_Basicos.
     * 
     * @return id_nacionalidade
     */
    public java.lang.String getId_nacionalidade() {
        return id_nacionalidade;
    }


    /**
     * Sets the id_nacionalidade value for this Conjuge_Dados_Basicos.
     * 
     * @param id_nacionalidade
     */
    public void setId_nacionalidade(java.lang.String id_nacionalidade) {
        this.id_nacionalidade = id_nacionalidade;
    }


    /**
     * Gets the id_sexo value for this Conjuge_Dados_Basicos.
     * 
     * @return id_sexo
     */
    public java.lang.String getId_sexo() {
        return id_sexo;
    }


    /**
     * Sets the id_sexo value for this Conjuge_Dados_Basicos.
     * 
     * @param id_sexo
     */
    public void setId_sexo(java.lang.String id_sexo) {
        this.id_sexo = id_sexo;
    }


    /**
     * Gets the nm_cliente_rsoc value for this Conjuge_Dados_Basicos.
     * 
     * @return nm_cliente_rsoc
     */
    public java.lang.String getNm_cliente_rsoc() {
        return nm_cliente_rsoc;
    }


    /**
     * Sets the nm_cliente_rsoc value for this Conjuge_Dados_Basicos.
     * 
     * @param nm_cliente_rsoc
     */
    public void setNm_cliente_rsoc(java.lang.String nm_cliente_rsoc) {
        this.nm_cliente_rsoc = nm_cliente_rsoc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Conjuge_Dados_Basicos)) return false;
        Conjuge_Dados_Basicos other = (Conjuge_Dados_Basicos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dt_nascimento==null && other.getDt_nascimento()==null) || 
             (this.dt_nascimento!=null &&
              this.dt_nascimento.equals(other.getDt_nascimento()))) &&
            ((this.id_cliente==null && other.getId_cliente()==null) || 
             (this.id_cliente!=null &&
              this.id_cliente.equals(other.getId_cliente()))) &&
            ((this.id_compromisso==null && other.getId_compromisso()==null) || 
             (this.id_compromisso!=null &&
              this.id_compromisso.equals(other.getId_compromisso()))) &&
            ((this.id_nacionalidade==null && other.getId_nacionalidade()==null) || 
             (this.id_nacionalidade!=null &&
              this.id_nacionalidade.equals(other.getId_nacionalidade()))) &&
            ((this.id_sexo==null && other.getId_sexo()==null) || 
             (this.id_sexo!=null &&
              this.id_sexo.equals(other.getId_sexo()))) &&
            ((this.nm_cliente_rsoc==null && other.getNm_cliente_rsoc()==null) || 
             (this.nm_cliente_rsoc!=null &&
              this.nm_cliente_rsoc.equals(other.getNm_cliente_rsoc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDt_nascimento() != null) {
            _hashCode += getDt_nascimento().hashCode();
        }
        if (getId_cliente() != null) {
            _hashCode += getId_cliente().hashCode();
        }
        if (getId_compromisso() != null) {
            _hashCode += getId_compromisso().hashCode();
        }
        if (getId_nacionalidade() != null) {
            _hashCode += getId_nacionalidade().hashCode();
        }
        if (getId_sexo() != null) {
            _hashCode += getId_sexo().hashCode();
        }
        if (getNm_cliente_rsoc() != null) {
            _hashCode += getNm_cliente_rsoc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Conjuge_Dados_Basicos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Conjuge_Dados_Basicos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_nascimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_nascimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_compromisso");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_compromisso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_nacionalidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_nacionalidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_sexo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "id_sexo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nm_cliente_rsoc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "nm_cliente_rsoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
