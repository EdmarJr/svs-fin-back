package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumTipoOcupacao;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ocupacao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -393693307230589421L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OCUPACAO")
	@SequenceGenerator(name = "SEQ_OCUPACAO", sequenceName = "SEQ_OCUPACAO", allocationSize = 1)
	private Long id;

	@Column(length = 200)
	private String ocupacao;

	@Column(length = 200)
	private String nomeEmpregador;

	@Column(length = 100)
	private String numeroBeneficioAposentadoria;

	@Column(length = 200)
	private String nomeContador;

	@Column(length = 200)
	private String telefone;

	@Column(length = 200)
	private String cnpj;

	@Column(length = 200)
	private String contador;

	@Column(length = 200)
	private String telefoneContador;

	@Column
	private BigDecimal capitalSocial;

	@Column
	private BigDecimal percParticipacao;

	@Column
	private Calendar socioDesde;

	@Column
	private String cargo;

	@Column(length = 200)
	private Calendar dataInicialCargo;

	@Column
	private Double renda;

	@Column
	@Enumerated(EnumType.STRING)
	private EnumTipoOcupacao tipoOcupacao;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@OneToOne
	@JoinColumn(name = "profissao_id")
	private ProfissaoSantander profissao;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "tipo_atividade_id")
	private TipoAtividadeEconomicaSantander tipoAtividade;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "atividade_id")
	private AtividadeEconomicaSantander atividade;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ocupacao other = (Ocupacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(String ocupacao) {
		this.ocupacao = ocupacao;
	}

	public String getNomeEmpregador() {
		return nomeEmpregador;
	}

	public void setNomeEmpregador(String nomeEmpregador) {
		this.nomeEmpregador = nomeEmpregador;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getContador() {
		return contador;
	}

	public void setContador(String contador) {
		this.contador = contador;
	}

	public String getTelefoneContador() {
		return telefoneContador;
	}

	public void setTelefoneContador(String telefoneContador) {
		this.telefoneContador = telefoneContador;
	}

	public BigDecimal getCapitalSocial() {
		return capitalSocial;
	}

	public void setCapitalSocial(BigDecimal capitalSocial) {
		this.capitalSocial = capitalSocial;
	}

	public BigDecimal getPercParticipacao() {
		return percParticipacao;
	}

	public void setPercParticipacao(BigDecimal percParticipacao) {
		this.percParticipacao = percParticipacao;
	}

	public Calendar getSocioDesde() {
		return socioDesde;
	}

	public void setSocioDesde(Calendar socioDesde) {
		this.socioDesde = socioDesde;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Double getRenda() {
		return renda;
	}

	public void setRenda(Double renda) {
		this.renda = renda;
	}

	public Endereco getEndereco() {
		if (endereco == null) {
			this.endereco = new Endereco();
		}
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Calendar getDataInicialCargo() {
		return dataInicialCargo;
	}

	public void setDataInicialCargo(Calendar dataInicialCargo) {
		this.dataInicialCargo = dataInicialCargo;
	}

	public TipoAtividadeEconomicaSantander getTipoAtividade() {
		return tipoAtividade;
	}

	public void setTipoAtividade(TipoAtividadeEconomicaSantander tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}

	public AtividadeEconomicaSantander getAtividade() {
		return atividade;
	}

	public void setAtividade(AtividadeEconomicaSantander atividade) {
		this.atividade = atividade;
	}

	public void setProfissao(ProfissaoSantander profissao) {
		this.profissao = profissao;
	}

	public ProfissaoSantander getProfissao() {
		return profissao;
	}

	public EnumTipoOcupacao getTipoOcupacao() {
		return tipoOcupacao;
	}

	public void setTipoOcupacao(EnumTipoOcupacao tipoOcupacao) {
		this.tipoOcupacao = tipoOcupacao;
	}

	public String getNomeContador() {
		return nomeContador;
	}

	public void setNomeContador(String nomeContador) {
		this.nomeContador = nomeContador;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getNumeroBeneficioAposentadoria() {
		return numeroBeneficioAposentadoria;
	}

	public void setNumeroBeneficioAposentadoria(String numeroBeneficioAposentadoria) {
		this.numeroBeneficioAposentadoria = numeroBeneficioAposentadoria;
	}
}
