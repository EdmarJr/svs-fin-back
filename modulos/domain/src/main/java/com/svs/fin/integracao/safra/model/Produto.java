/**
 * Produto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Produto")
public class Produto implements java.io.Serializable {
	private java.lang.String ds_produto;

	@Id
	private java.lang.Integer id_produto;

	public Produto() {
	}

	public Produto(java.lang.String ds_produto, java.lang.Integer id_produto) {
		this.ds_produto = ds_produto;
		this.id_produto = id_produto;
	}

	/**
	 * Gets the ds_produto value for this Produto.
	 * 
	 * @return ds_produto
	 */
	public java.lang.String getDs_produto() {
		return ds_produto;
	}

	/**
	 * Sets the ds_produto value for this Produto.
	 * 
	 * @param ds_produto
	 */
	public void setDs_produto(java.lang.String ds_produto) {
		this.ds_produto = ds_produto;
	}

	/**
	 * Gets the id_produto value for this Produto.
	 * 
	 * @return id_produto
	 */
	public java.lang.Integer getId_produto() {
		return id_produto;
	}

	/**
	 * Sets the id_produto value for this Produto.
	 * 
	 * @param id_produto
	 */
	public void setId_produto(java.lang.Integer id_produto) {
		this.id_produto = id_produto;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Produto))
			return false;
		Produto other = (Produto) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_produto == null && other.getDs_produto() == null)
						|| (this.ds_produto != null && this.ds_produto.equals(other.getDs_produto())))
				&& ((this.id_produto == null && other.getId_produto() == null)
						|| (this.id_produto != null && this.id_produto.equals(other.getId_produto())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_produto() != null) {
			_hashCode += getDs_produto().hashCode();
		}
		if (getId_produto() != null) {
			_hashCode += getId_produto().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Produto.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Produto"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_produto");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_produto"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_produto");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_produto"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
