/**
 * Natureza_Ocupacao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Natureza_Ocupacao")
public class Natureza_Ocupacao implements java.io.Serializable {
	private java.lang.String ds_natureza_ocupacao;

	private java.lang.String fl_obriga_beneficio;

	private java.lang.String fl_obriga_cnpj;

	@Id
	private java.lang.Integer id_natureza_ocupacao;

	public Natureza_Ocupacao() {
	}

	public Natureza_Ocupacao(java.lang.String ds_natureza_ocupacao, java.lang.String fl_obriga_beneficio,
			java.lang.String fl_obriga_cnpj, java.lang.Integer id_natureza_ocupacao) {
		this.ds_natureza_ocupacao = ds_natureza_ocupacao;
		this.fl_obriga_beneficio = fl_obriga_beneficio;
		this.fl_obriga_cnpj = fl_obriga_cnpj;
		this.id_natureza_ocupacao = id_natureza_ocupacao;
	}

	/**
	 * Gets the ds_natureza_ocupacao value for this Natureza_Ocupacao.
	 * 
	 * @return ds_natureza_ocupacao
	 */
	public java.lang.String getDs_natureza_ocupacao() {
		return ds_natureza_ocupacao;
	}

	/**
	 * Sets the ds_natureza_ocupacao value for this Natureza_Ocupacao.
	 * 
	 * @param ds_natureza_ocupacao
	 */
	public void setDs_natureza_ocupacao(java.lang.String ds_natureza_ocupacao) {
		this.ds_natureza_ocupacao = ds_natureza_ocupacao;
	}

	/**
	 * Gets the fl_obriga_beneficio value for this Natureza_Ocupacao.
	 * 
	 * @return fl_obriga_beneficio
	 */
	public java.lang.String getFl_obriga_beneficio() {
		return fl_obriga_beneficio;
	}

	/**
	 * Sets the fl_obriga_beneficio value for this Natureza_Ocupacao.
	 * 
	 * @param fl_obriga_beneficio
	 */
	public void setFl_obriga_beneficio(java.lang.String fl_obriga_beneficio) {
		this.fl_obriga_beneficio = fl_obriga_beneficio;
	}

	/**
	 * Gets the fl_obriga_cnpj value for this Natureza_Ocupacao.
	 * 
	 * @return fl_obriga_cnpj
	 */
	public java.lang.String getFl_obriga_cnpj() {
		return fl_obriga_cnpj;
	}

	/**
	 * Sets the fl_obriga_cnpj value for this Natureza_Ocupacao.
	 * 
	 * @param fl_obriga_cnpj
	 */
	public void setFl_obriga_cnpj(java.lang.String fl_obriga_cnpj) {
		this.fl_obriga_cnpj = fl_obriga_cnpj;
	}

	/**
	 * Gets the id_natureza_ocupacao value for this Natureza_Ocupacao.
	 * 
	 * @return id_natureza_ocupacao
	 */
	public java.lang.Integer getId_natureza_ocupacao() {
		return id_natureza_ocupacao;
	}

	/**
	 * Sets the id_natureza_ocupacao value for this Natureza_Ocupacao.
	 * 
	 * @param id_natureza_ocupacao
	 */
	public void setId_natureza_ocupacao(java.lang.Integer id_natureza_ocupacao) {
		this.id_natureza_ocupacao = id_natureza_ocupacao;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Natureza_Ocupacao))
			return false;
		Natureza_Ocupacao other = (Natureza_Ocupacao) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_natureza_ocupacao == null && other.getDs_natureza_ocupacao() == null)
						|| (this.ds_natureza_ocupacao != null
								&& this.ds_natureza_ocupacao.equals(other.getDs_natureza_ocupacao())))
				&& ((this.fl_obriga_beneficio == null && other.getFl_obriga_beneficio() == null)
						|| (this.fl_obriga_beneficio != null
								&& this.fl_obriga_beneficio.equals(other.getFl_obriga_beneficio())))
				&& ((this.fl_obriga_cnpj == null && other.getFl_obriga_cnpj() == null)
						|| (this.fl_obriga_cnpj != null && this.fl_obriga_cnpj.equals(other.getFl_obriga_cnpj())))
				&& ((this.id_natureza_ocupacao == null && other.getId_natureza_ocupacao() == null)
						|| (this.id_natureza_ocupacao != null
								&& this.id_natureza_ocupacao.equals(other.getId_natureza_ocupacao())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_natureza_ocupacao() != null) {
			_hashCode += getDs_natureza_ocupacao().hashCode();
		}
		if (getFl_obriga_beneficio() != null) {
			_hashCode += getFl_obriga_beneficio().hashCode();
		}
		if (getFl_obriga_cnpj() != null) {
			_hashCode += getFl_obriga_cnpj().hashCode();
		}
		if (getId_natureza_ocupacao() != null) {
			_hashCode += getId_natureza_ocupacao().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Natureza_Ocupacao.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"Natureza_Ocupacao"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_natureza_ocupacao");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"ds_natureza_ocupacao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("fl_obriga_beneficio");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"fl_obriga_beneficio"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("fl_obriga_cnpj");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "fl_obriga_cnpj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_natureza_ocupacao");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_natureza_ocupacao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
