package com.svs.fin.model.dto;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "Parâmetros para paginação")
public class ListParamsDto {
	
	private String sortField;
	
	private String sortOrder;
    
	private int firstResult;
    
	@Min(0)
	@Max(200)
	@NotNull
	private int maxResult;
    
	private List<ParamFilterListDto> filters;

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
	}

	public int getMaxResult() {
		return maxResult;
	}

	public void setMaxResult(int maxResult) {
		this.maxResult = maxResult;
	}

	public List<ParamFilterListDto> getFilters() {
		if(filters == null) {
			filters = new ArrayList<ParamFilterListDto>();
		}
		return filters;
	}

	public void setFilters(List<ParamFilterListDto> filters) {
		this.filters = filters;
	}
}
