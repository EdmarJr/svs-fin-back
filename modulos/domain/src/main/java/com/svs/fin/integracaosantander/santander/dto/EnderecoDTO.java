package com.svs.fin.integracaosantander.santander.dto;

public class EnderecoDTO {

	private String prefix;

	private String street;

	private String state;

	private String district;

	private String countyId;

	private String city;

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCountyId() {
		return countyId;
	}

	public void setCountyId(String countyId) {
		this.countyId = countyId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "ClassPojo [prefix = " + prefix + ", street = " + street + ", state = " + state + ", district = "
				+ district + ", countyId = " + countyId + ", city = " + city + "]";
	}
}
