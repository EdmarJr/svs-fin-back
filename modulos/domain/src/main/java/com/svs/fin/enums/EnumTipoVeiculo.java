package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoVeiculo implements EnumLabeled{

	@JsonProperty("Carros e Utilitários")
	CARROS_UTILITARIOS("Carros e Utilitários", "C"), @JsonProperty("Motos")
	MOTOS("Motos", "M");

	private EnumTipoVeiculo(String label, String id) {
		this.label = label;
		this.id = id;
	}

	private String label;
	private String id;

	public String getLabel() {
		return label;
	}

	public String getId() {
		return id;
	}
}
