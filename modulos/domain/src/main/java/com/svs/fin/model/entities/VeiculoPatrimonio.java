package com.svs.fin.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@NamedQueries({
		@NamedQuery(name = VeiculoPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.NAME, query = VeiculoPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.JPQL) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class VeiculoPatrimonio implements Serializable {
	public static interface NQ_OBTER_POR_CLIENTE_SVS {
		public static final String NAME = "VeiculoPatrimonio.obterPorIdClienteSvs";
		public static final String JPQL = "Select e from VeiculoPatrimonio e where e.clienteSvs.id = :"
				+ NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS;
		public static final String NM_PM_ID_CLIENTE_SVS = "idClienteSvs";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5251896249255332291L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_VEICULOPATRIMONIO")
	@SequenceGenerator(name = "SEQ_VEICULOPATRIMONIO", sequenceName = "SEQ_VEICULOPATRIMONIO", allocationSize = 1)
	private Long id;

	@Column(length = 100)
	private String marca;

	@Column(length = 100)
	private String modelo;

	@Column
	private Integer anoModelo;

	@Column
	private Integer anoFabricacao;

	@Column
	private BigDecimal valor;

	@Column(length = 100)
	private String placa;

	@Transient
	private Boolean editando;

	@ManyToOne
	@JoinColumn(name = "clientesvs_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private ClienteSvs clienteSvs;

	@ManyToOne
	@JoinColumn(name = "avalista_id", referencedColumnName = "id")
	@JsonProperty(access = Access.WRITE_ONLY)
	private Avalista avalista;

	public Avalista getAvalista() {
		return avalista;
	}

	public void setAvalista(Avalista avalista) {
		this.avalista = avalista;
	}

	public ClienteSvs getClienteSvs() {
		return clienteSvs;
	}

	public void setClienteSvs(ClienteSvs clienteSvs) {
		this.clienteSvs = clienteSvs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAnoModelo() {
		return anoModelo;
	}

	public void setAnoModelo(Integer anoModelo) {
		this.anoModelo = anoModelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoPatrimonio other = (VeiculoPatrimonio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean getEditando() {
		return editando;
	}

	public void setEditando(Boolean editando) {
		this.editando = editando;
	}
}
