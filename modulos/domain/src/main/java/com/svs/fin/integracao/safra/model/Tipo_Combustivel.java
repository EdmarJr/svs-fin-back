/**
 * Tipo_Combustivel.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Combustivel")
public class Tipo_Combustivel implements java.io.Serializable {
	private java.lang.String ds_combustivel;

	@Id
	private java.lang.String id_combustivel;

	public Tipo_Combustivel() {
	}

	public Tipo_Combustivel(java.lang.String ds_combustivel, java.lang.String id_combustivel) {
		this.ds_combustivel = ds_combustivel;
		this.id_combustivel = id_combustivel;
	}

	/**
	 * Gets the ds_combustivel value for this Tipo_Combustivel.
	 * 
	 * @return ds_combustivel
	 */
	public java.lang.String getDs_combustivel() {
		return ds_combustivel;
	}

	/**
	 * Sets the ds_combustivel value for this Tipo_Combustivel.
	 * 
	 * @param ds_combustivel
	 */
	public void setDs_combustivel(java.lang.String ds_combustivel) {
		this.ds_combustivel = ds_combustivel;
	}

	/**
	 * Gets the id_combustivel value for this Tipo_Combustivel.
	 * 
	 * @return id_combustivel
	 */
	public java.lang.String getId_combustivel() {
		return id_combustivel;
	}

	/**
	 * Sets the id_combustivel value for this Tipo_Combustivel.
	 * 
	 * @param id_combustivel
	 */
	public void setId_combustivel(java.lang.String id_combustivel) {
		this.id_combustivel = id_combustivel;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Combustivel))
			return false;
		Tipo_Combustivel other = (Tipo_Combustivel) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_combustivel == null && other.getDs_combustivel() == null)
						|| (this.ds_combustivel != null && this.ds_combustivel.equals(other.getDs_combustivel())))
				&& ((this.id_combustivel == null && other.getId_combustivel() == null)
						|| (this.id_combustivel != null && this.id_combustivel.equals(other.getId_combustivel())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_combustivel() != null) {
			_hashCode += getDs_combustivel().hashCode();
		}
		if (getId_combustivel() != null) {
			_hashCode += getId_combustivel().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Combustivel.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Combustivel"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_combustivel");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_combustivel"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_combustivel");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_combustivel"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
