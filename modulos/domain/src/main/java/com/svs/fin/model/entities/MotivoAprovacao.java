package com.svs.fin.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.svs.fin.model.entities.interfaces.AuditavelResponsabilizacao;
import com.svs.fin.model.entities.interfaces.AuditavelTemporalmente;
import com.svs.fin.model.entities.interfaces.Desativavel;
import com.svs.fin.model.entities.interfaces.Identificavel;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisResponsabilizacao;
import com.svs.fin.model.entities.listeners.ListenerEntidadesAuditaveisTemporalmente;

@Entity
@Table(name = "motivoaprovacaosvs")
@EntityListeners({ ListenerEntidadesAuditaveisResponsabilizacao.class, ListenerEntidadesAuditaveisTemporalmente.class })
public class MotivoAprovacao
		implements Desativavel, Identificavel<Long>, AuditavelResponsabilizacao, AuditavelTemporalmente {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MOTIVOAPROVACAOSVS")
	@SequenceGenerator(name = "SEQ_MOTIVOAPROVACAOSVS", sequenceName = "SEQ_MOTIVOAPROVACAOSVS", allocationSize = 1)
	private Long id;
	@Column(name = "ativo")
	private Boolean ativo;
	@Column(name = "titulo")
	private String titulo;

	@Column(name = "alteradoem")
	private LocalDateTime dataHoraDeAtualizacao;
	@Column(name = "criadoem")
	private LocalDateTime dataHoraDeCriacao;

	@ManyToOne
	@JoinColumn(name = "id_usuario_criacao_alteracao")
	private Usuario usuarioResponsavelUltimaAlteracao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public LocalDateTime getDataHoraDeAtualizacao() {
		return dataHoraDeAtualizacao;
	}

	public void setDataHoraDeAtualizacao(LocalDateTime dataHoraDeAtualizacao) {
		this.dataHoraDeAtualizacao = dataHoraDeAtualizacao;
	}

	public LocalDateTime getDataHoraDeCriacao() {
		return dataHoraDeCriacao;
	}

	public void setDataHoraDeCriacao(LocalDateTime dataHoraDeCriacao) {
		this.dataHoraDeCriacao = dataHoraDeCriacao;
	}

	public Usuario getUsuarioResponsavelUltimaAlteracao() {
		return usuarioResponsavelUltimaAlteracao;
	}

	public void setUsuarioResponsavelUltimaAlteracao(Usuario usuarioResponsavelUltimaAlteracao) {
		this.usuarioResponsavelUltimaAlteracao = usuarioResponsavelUltimaAlteracao;
	}

}
