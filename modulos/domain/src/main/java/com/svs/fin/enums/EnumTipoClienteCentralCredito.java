package com.svs.fin.enums;

public enum EnumTipoClienteCentralCredito {

	CLIENTE_REGULAR("Cliente Regular"),
	CLIENTE_VIP("Cliente Vip"),
	CLIENTE_BLOQUEADOERP("Cliente Bloqueado ERPs"),
	CLIENTE_LISTANEGRA("Cliente Lista Negra");
	
	private String label;
	
	private EnumTipoClienteCentralCredito(String label){
		this.label = label;
	}
	
	public String getLabe(){
		return label;
	}
}
