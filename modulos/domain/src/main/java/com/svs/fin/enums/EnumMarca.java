package com.svs.fin.enums;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Marcas Definidas no Portal Saga
 * 
 * @author wesley.mota
 *
 */
public enum EnumMarca {

	@JsonProperty("VolksWagen")
	VOLKSWAGEN("VolksWagen", "/resources/img/montadoras/vw.png", 36), @JsonProperty("Fiat")
	FIAT("Fiat", "/resources/img/montadoras/fiat.png", 1), // 1
	@JsonProperty("Ford")
	FORD("Ford", "/resources/img/montadoras/ford.png", 14), // 2
	@JsonProperty("Hyundai")
	HYUNDAI("Hyundai", "/resources/img/montadoras/hyundai.png", 18), // 3
	@JsonProperty("Toyota")
	TOYOTA("Toyota", "/resources/img/montadoras/toyota.png", 34), // 4
	@JsonProperty("Citroen")
	CITROEN("Citroen", "/resources/img/montadoras/citroen.png", 10), // 5
	@JsonProperty("Nissan")
	NISSAN("Nissan", "/resources/img/montadoras/nissan.png", 27), // 6
	@JsonProperty("Peugeot")
	PEUGEOT("Peugeot", "/resources/img/montadoras/peugeot.png", 28), // 7
	@JsonProperty("Audi")
	AUDI("Audi", "/resources/img/montadoras/audi.png", 7), // 8
	@JsonProperty("International")
	INTERNATIONAL("International", "", 50), // 9
	@JsonProperty("GM")
	GM("GM", "/resources/img/montadoras/gm.png", 16), // 10
	@JsonProperty("Renault")
	RENAULT("Renault", "/resources/img/montadoras/renault.png", 30), // 11
	@JsonProperty("Sem Marca")
	SEM_MARCA("Sem Marca", "", null), // 12
	@JsonProperty("Seminovos")
	SUPER_CENTER("Seminovos", "/resources/img/montadoras/superCenter.png", null), // 13
	@JsonProperty("Hyundai Caminhões")
	HYUNDAI_CAMINHOES("Hyundai Caminhões", "/resources/img/montadoras/hyundai_caminhoes.png", 18), // 14
	@JsonProperty("Triumph Motos")
	TRIUMPH("Triumph Motos", "/resources/img/montadoras/triumph.png", 147), // 15
	@JsonProperty("Autotech")
	AUTOTECH("Autotech", "", null), // 16
	@JsonProperty("Bmw")
	BMW("Bmw", "/resources/img/montadoras/bmw.png", null), // 17
	@JsonProperty("Chrysler")
	CHRYSLER("Chrysler", "/resources/img/montadoras/chrysler.png", 9), // 18
	@JsonProperty("Jeep")
	JEEP("Jeep", "/resources/img/montadoras/jeep.png", 2), // 19
	@JsonProperty("Land Rover")
	LAND_ROVER("Land Rover", "/resources/img/montadoras/landRover.png", 22), // 20
	@JsonProperty("Bmw Motos")
	BMW_MOTOS("Bmw Motos", "/resources/img/montadoras/bmw.png", 8), // SUB MARCAS DA BANDEIRA BMW //21
	@JsonProperty("Mini")
	MINI("Mini", "/resources/img/montadoras/mini.png", 130), // SUB MARCAS DA BANDEIRA BMW //22
	@JsonProperty("Indian")
	INDIAN("Indian", "/resources/img/montadoras/indian.png", null), // 23
	@JsonProperty("Kia")
	KIA("Kia", "/resources/img/montadoras/kia.png", 20);

	private String label;

	private String gif;

	private Integer codigoDealerWorkflow;

	private EnumMarca(String label, String pthGif, Integer codigoDealerWorkflow) {
		this.label = label;
		this.gif = pthGif;
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	public String getGif() {
		return gif;
	}

	public String getLabel() {
		return label;
	}

	public Integer getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}

	public void setCodigoDealerWorkflow(Integer codigoDealerWorkflow) {
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}

	private static final HashMap<String, EnumMarca> MAP = new HashMap<String, EnumMarca>();

	public static EnumMarca getByLabel(String label) {
		return MAP.get(label);
	}

	static {
		for (EnumMarca field : EnumMarca.values()) {
			MAP.put(field.getLabel(), field);
		}
	}
}
