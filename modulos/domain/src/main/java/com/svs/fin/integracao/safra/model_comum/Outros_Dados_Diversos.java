/**
 * Outros_Dados_Diversos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_comum;

public class Outros_Dados_Diversos  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String ds_pto_referencia;

    private java.lang.String dt_constituicao;

    private java.lang.String dt_ult_alt_contrat;

    private java.lang.String email_website;

    private java.lang.String vl_capital_realiz;

    private java.lang.String vl_capital_social;

    public Outros_Dados_Diversos() {
    }

    public Outros_Dados_Diversos(
           java.lang.String ds_pto_referencia,
           java.lang.String dt_constituicao,
           java.lang.String dt_ult_alt_contrat,
           java.lang.String email_website,
           java.lang.String vl_capital_realiz,
           java.lang.String vl_capital_social) {
        this.ds_pto_referencia = ds_pto_referencia;
        this.dt_constituicao = dt_constituicao;
        this.dt_ult_alt_contrat = dt_ult_alt_contrat;
        this.email_website = email_website;
        this.vl_capital_realiz = vl_capital_realiz;
        this.vl_capital_social = vl_capital_social;
    }


    /**
     * Gets the ds_pto_referencia value for this Outros_Dados_Diversos.
     * 
     * @return ds_pto_referencia
     */
    public java.lang.String getDs_pto_referencia() {
        return ds_pto_referencia;
    }


    /**
     * Sets the ds_pto_referencia value for this Outros_Dados_Diversos.
     * 
     * @param ds_pto_referencia
     */
    public void setDs_pto_referencia(java.lang.String ds_pto_referencia) {
        this.ds_pto_referencia = ds_pto_referencia;
    }


    /**
     * Gets the dt_constituicao value for this Outros_Dados_Diversos.
     * 
     * @return dt_constituicao
     */
    public java.lang.String getDt_constituicao() {
        return dt_constituicao;
    }


    /**
     * Sets the dt_constituicao value for this Outros_Dados_Diversos.
     * 
     * @param dt_constituicao
     */
    public void setDt_constituicao(java.lang.String dt_constituicao) {
        this.dt_constituicao = dt_constituicao;
    }


    /**
     * Gets the dt_ult_alt_contrat value for this Outros_Dados_Diversos.
     * 
     * @return dt_ult_alt_contrat
     */
    public java.lang.String getDt_ult_alt_contrat() {
        return dt_ult_alt_contrat;
    }


    /**
     * Sets the dt_ult_alt_contrat value for this Outros_Dados_Diversos.
     * 
     * @param dt_ult_alt_contrat
     */
    public void setDt_ult_alt_contrat(java.lang.String dt_ult_alt_contrat) {
        this.dt_ult_alt_contrat = dt_ult_alt_contrat;
    }


    /**
     * Gets the email_website value for this Outros_Dados_Diversos.
     * 
     * @return email_website
     */
    public java.lang.String getEmail_website() {
        return email_website;
    }


    /**
     * Sets the email_website value for this Outros_Dados_Diversos.
     * 
     * @param email_website
     */
    public void setEmail_website(java.lang.String email_website) {
        this.email_website = email_website;
    }


    /**
     * Gets the vl_capital_realiz value for this Outros_Dados_Diversos.
     * 
     * @return vl_capital_realiz
     */
    public java.lang.String getVl_capital_realiz() {
        return vl_capital_realiz;
    }


    /**
     * Sets the vl_capital_realiz value for this Outros_Dados_Diversos.
     * 
     * @param vl_capital_realiz
     */
    public void setVl_capital_realiz(java.lang.String vl_capital_realiz) {
        this.vl_capital_realiz = vl_capital_realiz;
    }


    /**
     * Gets the vl_capital_social value for this Outros_Dados_Diversos.
     * 
     * @return vl_capital_social
     */
    public java.lang.String getVl_capital_social() {
        return vl_capital_social;
    }


    /**
     * Sets the vl_capital_social value for this Outros_Dados_Diversos.
     * 
     * @param vl_capital_social
     */
    public void setVl_capital_social(java.lang.String vl_capital_social) {
        this.vl_capital_social = vl_capital_social;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Outros_Dados_Diversos)) return false;
        Outros_Dados_Diversos other = (Outros_Dados_Diversos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ds_pto_referencia==null && other.getDs_pto_referencia()==null) || 
             (this.ds_pto_referencia!=null &&
              this.ds_pto_referencia.equals(other.getDs_pto_referencia()))) &&
            ((this.dt_constituicao==null && other.getDt_constituicao()==null) || 
             (this.dt_constituicao!=null &&
              this.dt_constituicao.equals(other.getDt_constituicao()))) &&
            ((this.dt_ult_alt_contrat==null && other.getDt_ult_alt_contrat()==null) || 
             (this.dt_ult_alt_contrat!=null &&
              this.dt_ult_alt_contrat.equals(other.getDt_ult_alt_contrat()))) &&
            ((this.email_website==null && other.getEmail_website()==null) || 
             (this.email_website!=null &&
              this.email_website.equals(other.getEmail_website()))) &&
            ((this.vl_capital_realiz==null && other.getVl_capital_realiz()==null) || 
             (this.vl_capital_realiz!=null &&
              this.vl_capital_realiz.equals(other.getVl_capital_realiz()))) &&
            ((this.vl_capital_social==null && other.getVl_capital_social()==null) || 
             (this.vl_capital_social!=null &&
              this.vl_capital_social.equals(other.getVl_capital_social())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDs_pto_referencia() != null) {
            _hashCode += getDs_pto_referencia().hashCode();
        }
        if (getDt_constituicao() != null) {
            _hashCode += getDt_constituicao().hashCode();
        }
        if (getDt_ult_alt_contrat() != null) {
            _hashCode += getDt_ult_alt_contrat().hashCode();
        }
        if (getEmail_website() != null) {
            _hashCode += getEmail_website().hashCode();
        }
        if (getVl_capital_realiz() != null) {
            _hashCode += getVl_capital_realiz().hashCode();
        }
        if (getVl_capital_social() != null) {
            _hashCode += getVl_capital_social().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Outros_Dados_Diversos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Diversos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_pto_referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ds_pto_referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_constituicao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_constituicao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dt_ult_alt_contrat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "dt_ult_alt_contrat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email_website");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "email_website"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_capital_realiz");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_capital_realiz"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vl_capital_social");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "vl_capital_social"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
