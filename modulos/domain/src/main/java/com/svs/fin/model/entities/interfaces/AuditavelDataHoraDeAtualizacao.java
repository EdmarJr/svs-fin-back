package com.svs.fin.model.entities.interfaces;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface AuditavelDataHoraDeAtualizacao {
	@JsonIgnore
	public LocalDateTime getDataHoraDeAtualizacao();

	public void setDataHoraDeAtualizacao(LocalDateTime dataHoraDeAtualizacao);

}
