package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoLogradouro {
	
//	RUA("Rua", 1),
//	AVENIDA("Avenida", 2),
//	TRAVESSA("Travessa", 3),
//	ALAMEDA("Alameda", 4),
//	PRACA("Praça", 5),
//	LOTEAMENTO("Loteamento", 6),
//	VILA("Vila", 7),
//	BECO("Beco", 8),
//	RODOVIA("Rodovia", 9),
//	CAMINHO("Caminho", 10),
//	POVOADO("Povoado", 11),
//	FAZENDA("Fazenda", 12),
//	ESTRADA("Estrada", 13),
//	SETOR("Setor", 14),
//	QUADRA("Quadra", 15),
//	TRECHO("Trecho", 16),
//	CONJUNTO("Conjunto", 17);
	
	@JsonProperty("Rua")
	RUA("Rua", 1),
		@JsonProperty("Avenida")
	AVENIDA("Avenida", 2),
		@JsonProperty("Travessa")
	TRAVESSA("Travessa", 3),
		@JsonProperty("Alameda")
	ALAMEDA("Alameda", 4),
		@JsonProperty("Praça")
	PRACA("Praça", 5),
		@JsonProperty("Loteamento")
	LOTEAMENTO("Loteamento", 6),
		@JsonProperty("Vila")
	VILA("Vila", 7),
		@JsonProperty("Beco")
	BECO("Beco", 8),
		@JsonProperty("Rodovia")
	RODOVIA("Rodovia", 9),
		@JsonProperty("Caminho")
	CAMINHO("Caminho", 10),
		@JsonProperty("Povoado")
	POVOADO("Povoado", 11),
		@JsonProperty("Fazenda")
	FAZENDA("Fazenda", 12),
		@JsonProperty("Estrada")
	ESTRADA("Estrada", 13),
		@JsonProperty("Setor")
	SETOR("Setor", 14),
		@JsonProperty("Quadra")
	QUADRA("Quadra", 15),
		@JsonProperty("Trecho")
	TRECHO("Trecho", 16),
		@JsonProperty("Conjunto")
	CONJUNTO("Conjunto", 17);
	
	private String label;
	private Integer codigoDealerWorkflow;
	
	private EnumTipoLogradouro(String label, Integer codigoDealerWorkflow){
		this.label = label;
		this.codigoDealerWorkflow = codigoDealerWorkflow;
	}
	
	public String getLabel(){
		return label;
	}

	public Integer getCodigoDealerWorkflow() {
		return codigoDealerWorkflow;
	}
}
