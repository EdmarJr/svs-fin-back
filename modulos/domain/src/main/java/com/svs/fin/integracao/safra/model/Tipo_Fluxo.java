/**
 * Tipo_Fluxo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Tipo_Fluxo")
public class Tipo_Fluxo implements java.io.Serializable {
	private java.lang.String ds_tipo_fluxo;

	@Id
	private java.lang.Integer id_tipo_fluxo;

	public Tipo_Fluxo() {
	}

	public Tipo_Fluxo(java.lang.String ds_tipo_fluxo, java.lang.Integer id_tipo_fluxo) {
		this.ds_tipo_fluxo = ds_tipo_fluxo;
		this.id_tipo_fluxo = id_tipo_fluxo;
	}

	/**
	 * Gets the ds_tipo_fluxo value for this Tipo_Fluxo.
	 * 
	 * @return ds_tipo_fluxo
	 */
	public java.lang.String getDs_tipo_fluxo() {
		return ds_tipo_fluxo;
	}

	/**
	 * Sets the ds_tipo_fluxo value for this Tipo_Fluxo.
	 * 
	 * @param ds_tipo_fluxo
	 */
	public void setDs_tipo_fluxo(java.lang.String ds_tipo_fluxo) {
		this.ds_tipo_fluxo = ds_tipo_fluxo;
	}

	/**
	 * Gets the id_tipo_fluxo value for this Tipo_Fluxo.
	 * 
	 * @return id_tipo_fluxo
	 */
	public java.lang.Integer getId_tipo_fluxo() {
		return id_tipo_fluxo;
	}

	/**
	 * Sets the id_tipo_fluxo value for this Tipo_Fluxo.
	 * 
	 * @param id_tipo_fluxo
	 */
	public void setId_tipo_fluxo(java.lang.Integer id_tipo_fluxo) {
		this.id_tipo_fluxo = id_tipo_fluxo;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Tipo_Fluxo))
			return false;
		Tipo_Fluxo other = (Tipo_Fluxo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_tipo_fluxo == null && other.getDs_tipo_fluxo() == null)
						|| (this.ds_tipo_fluxo != null && this.ds_tipo_fluxo.equals(other.getDs_tipo_fluxo())))
				&& ((this.id_tipo_fluxo == null && other.getId_tipo_fluxo() == null)
						|| (this.id_tipo_fluxo != null && this.id_tipo_fluxo.equals(other.getId_tipo_fluxo())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_tipo_fluxo() != null) {
			_hashCode += getDs_tipo_fluxo().hashCode();
		}
		if (getId_tipo_fluxo() != null) {
			_hashCode += getId_tipo_fluxo().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Tipo_Fluxo.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Fluxo"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_tipo_fluxo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_tipo_fluxo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_fluxo");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_tipo_fluxo"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
