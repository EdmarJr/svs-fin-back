/**
 * RetornoConsultaProposta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

public class RetornoConsultaProposta  implements java.io.Serializable {
    private java.lang.String ds_status_proposta;

    private java.lang.String id_status;

    public RetornoConsultaProposta() {
    }

    public RetornoConsultaProposta(
           java.lang.String ds_status_proposta,
           java.lang.String id_status) {
           this.ds_status_proposta = ds_status_proposta;
           this.id_status = id_status;
    }


    /**
     * Gets the ds_status_proposta value for this RetornoConsultaProposta.
     * 
     * @return ds_status_proposta
     */
    public java.lang.String getDs_status_proposta() {
        return ds_status_proposta;
    }


    /**
     * Sets the ds_status_proposta value for this RetornoConsultaProposta.
     * 
     * @param ds_status_proposta
     */
    public void setDs_status_proposta(java.lang.String ds_status_proposta) {
        this.ds_status_proposta = ds_status_proposta;
    }


    /**
     * Gets the id_status value for this RetornoConsultaProposta.
     * 
     * @return id_status
     */
    public java.lang.String getId_status() {
        return id_status;
    }


    /**
     * Sets the id_status value for this RetornoConsultaProposta.
     * 
     * @param id_status
     */
    public void setId_status(java.lang.String id_status) {
        this.id_status = id_status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetornoConsultaProposta)) return false;
        RetornoConsultaProposta other = (RetornoConsultaProposta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ds_status_proposta==null && other.getDs_status_proposta()==null) || 
             (this.ds_status_proposta!=null &&
              this.ds_status_proposta.equals(other.getDs_status_proposta()))) &&
            ((this.id_status==null && other.getId_status()==null) || 
             (this.id_status!=null &&
              this.id_status.equals(other.getId_status())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDs_status_proposta() != null) {
            _hashCode += getDs_status_proposta().hashCode();
        }
        if (getId_status() != null) {
            _hashCode += getId_status().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetornoConsultaProposta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoConsultaProposta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_status_proposta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_status_proposta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
