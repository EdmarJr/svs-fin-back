/**
 * Porte_Empresa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Porte_Empresa")
public class Porte_Empresa implements java.io.Serializable {
	private java.lang.String ds_porte_empresa;

	@Id
	private java.lang.Integer id_porte_empresa;

	private java.lang.Double vl_final;

	private java.lang.Double vl_inicial;

	public Porte_Empresa() {
	}

	public Porte_Empresa(java.lang.String ds_porte_empresa, java.lang.Integer id_porte_empresa,
			java.lang.Double vl_final, java.lang.Double vl_inicial) {
		this.ds_porte_empresa = ds_porte_empresa;
		this.id_porte_empresa = id_porte_empresa;
		this.vl_final = vl_final;
		this.vl_inicial = vl_inicial;
	}

	/**
	 * Gets the ds_porte_empresa value for this Porte_Empresa.
	 * 
	 * @return ds_porte_empresa
	 */
	public java.lang.String getDs_porte_empresa() {
		return ds_porte_empresa;
	}

	/**
	 * Sets the ds_porte_empresa value for this Porte_Empresa.
	 * 
	 * @param ds_porte_empresa
	 */
	public void setDs_porte_empresa(java.lang.String ds_porte_empresa) {
		this.ds_porte_empresa = ds_porte_empresa;
	}

	/**
	 * Gets the id_porte_empresa value for this Porte_Empresa.
	 * 
	 * @return id_porte_empresa
	 */
	public java.lang.Integer getId_porte_empresa() {
		return id_porte_empresa;
	}

	/**
	 * Sets the id_porte_empresa value for this Porte_Empresa.
	 * 
	 * @param id_porte_empresa
	 */
	public void setId_porte_empresa(java.lang.Integer id_porte_empresa) {
		this.id_porte_empresa = id_porte_empresa;
	}

	/**
	 * Gets the vl_final value for this Porte_Empresa.
	 * 
	 * @return vl_final
	 */
	public java.lang.Double getVl_final() {
		return vl_final;
	}

	/**
	 * Sets the vl_final value for this Porte_Empresa.
	 * 
	 * @param vl_final
	 */
	public void setVl_final(java.lang.Double vl_final) {
		this.vl_final = vl_final;
	}

	/**
	 * Gets the vl_inicial value for this Porte_Empresa.
	 * 
	 * @return vl_inicial
	 */
	public java.lang.Double getVl_inicial() {
		return vl_inicial;
	}

	/**
	 * Sets the vl_inicial value for this Porte_Empresa.
	 * 
	 * @param vl_inicial
	 */
	public void setVl_inicial(java.lang.Double vl_inicial) {
		this.vl_inicial = vl_inicial;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Porte_Empresa))
			return false;
		Porte_Empresa other = (Porte_Empresa) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ds_porte_empresa == null && other.getDs_porte_empresa() == null)
						|| (this.ds_porte_empresa != null && this.ds_porte_empresa.equals(other.getDs_porte_empresa())))
				&& ((this.id_porte_empresa == null && other.getId_porte_empresa() == null)
						|| (this.id_porte_empresa != null && this.id_porte_empresa.equals(other.getId_porte_empresa())))
				&& ((this.vl_final == null && other.getVl_final() == null)
						|| (this.vl_final != null && this.vl_final.equals(other.getVl_final())))
				&& ((this.vl_inicial == null && other.getVl_inicial() == null)
						|| (this.vl_inicial != null && this.vl_inicial.equals(other.getVl_inicial())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDs_porte_empresa() != null) {
			_hashCode += getDs_porte_empresa().hashCode();
		}
		if (getId_porte_empresa() != null) {
			_hashCode += getId_porte_empresa().hashCode();
		}
		if (getVl_final() != null) {
			_hashCode += getVl_final().hashCode();
		}
		if (getVl_inicial() != null) {
			_hashCode += getVl_inicial().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Porte_Empresa.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Porte_Empresa"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ds_porte_empresa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ds_porte_empresa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_porte_empresa");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_porte_empresa"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_final");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_final"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_inicial");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_inicial"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
