/**
 * Planos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Table;

@Entity
@Table(name = "safra_Planos")
public class Planos implements java.io.Serializable {
	private java.lang.String dt_fim;

	private java.lang.String dt_inicio;

	@Id
	private java.lang.String id_plano;

	private java.lang.Integer id_tipo_categoria;

	private java.lang.String[] lojistas_autorizados;

	private com.svs.fin.integracao.safra.model.Prazos[] prazos;

	private java.lang.Integer vl_ano_final;

	private java.lang.Integer vl_ano_inicial;

	public Planos() {
	}

	public Planos(java.lang.String dt_fim, java.lang.String dt_inicio, java.lang.String id_plano,
			java.lang.Integer id_tipo_categoria, java.lang.String[] lojistas_autorizados,
			com.svs.fin.integracao.safra.model.Prazos[] prazos, java.lang.Integer vl_ano_final,
			java.lang.Integer vl_ano_inicial) {
		this.dt_fim = dt_fim;
		this.dt_inicio = dt_inicio;
		this.id_plano = id_plano;
		this.id_tipo_categoria = id_tipo_categoria;
		this.lojistas_autorizados = lojistas_autorizados;
		this.prazos = prazos;
		this.vl_ano_final = vl_ano_final;
		this.vl_ano_inicial = vl_ano_inicial;
	}

	/**
	 * Gets the dt_fim value for this Planos.
	 * 
	 * @return dt_fim
	 */
	public java.lang.String getDt_fim() {
		return dt_fim;
	}

	/**
	 * Sets the dt_fim value for this Planos.
	 * 
	 * @param dt_fim
	 */
	public void setDt_fim(java.lang.String dt_fim) {
		this.dt_fim = dt_fim;
	}

	/**
	 * Gets the dt_inicio value for this Planos.
	 * 
	 * @return dt_inicio
	 */
	public java.lang.String getDt_inicio() {
		return dt_inicio;
	}

	/**
	 * Sets the dt_inicio value for this Planos.
	 * 
	 * @param dt_inicio
	 */
	public void setDt_inicio(java.lang.String dt_inicio) {
		this.dt_inicio = dt_inicio;
	}

	/**
	 * Gets the id_plano value for this Planos.
	 * 
	 * @return id_plano
	 */
	public java.lang.String getId_plano() {
		return id_plano;
	}

	/**
	 * Sets the id_plano value for this Planos.
	 * 
	 * @param id_plano
	 */
	public void setId_plano(java.lang.String id_plano) {
		this.id_plano = id_plano;
	}

	/**
	 * Gets the id_tipo_categoria value for this Planos.
	 * 
	 * @return id_tipo_categoria
	 */
	public java.lang.Integer getId_tipo_categoria() {
		return id_tipo_categoria;
	}

	/**
	 * Sets the id_tipo_categoria value for this Planos.
	 * 
	 * @param id_tipo_categoria
	 */
	public void setId_tipo_categoria(java.lang.Integer id_tipo_categoria) {
		this.id_tipo_categoria = id_tipo_categoria;
	}

	/**
	 * Gets the lojistas_autorizados value for this Planos.
	 * 
	 * @return lojistas_autorizados
	 */
	public java.lang.String[] getLojistas_autorizados() {
		return lojistas_autorizados;
	}

	/**
	 * Sets the lojistas_autorizados value for this Planos.
	 * 
	 * @param lojistas_autorizados
	 */
	public void setLojistas_autorizados(java.lang.String[] lojistas_autorizados) {
		this.lojistas_autorizados = lojistas_autorizados;
	}

	/**
	 * Gets the prazos value for this Planos.
	 * 
	 * @return prazos
	 */
	public com.svs.fin.integracao.safra.model.Prazos[] getPrazos() {
		return prazos;
	}

	/**
	 * Sets the prazos value for this Planos.
	 * 
	 * @param prazos
	 */
	public void setPrazos(com.svs.fin.integracao.safra.model.Prazos[] prazos) {
		this.prazos = prazos;
	}

	/**
	 * Gets the vl_ano_final value for this Planos.
	 * 
	 * @return vl_ano_final
	 */
	public java.lang.Integer getVl_ano_final() {
		return vl_ano_final;
	}

	/**
	 * Sets the vl_ano_final value for this Planos.
	 * 
	 * @param vl_ano_final
	 */
	public void setVl_ano_final(java.lang.Integer vl_ano_final) {
		this.vl_ano_final = vl_ano_final;
	}

	/**
	 * Gets the vl_ano_inicial value for this Planos.
	 * 
	 * @return vl_ano_inicial
	 */
	public java.lang.Integer getVl_ano_inicial() {
		return vl_ano_inicial;
	}

	/**
	 * Sets the vl_ano_inicial value for this Planos.
	 * 
	 * @param vl_ano_inicial
	 */
	public void setVl_ano_inicial(java.lang.Integer vl_ano_inicial) {
		this.vl_ano_inicial = vl_ano_inicial;
	}

    @Transient
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Planos))
			return false;
		Planos other = (Planos) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.dt_fim == null && other.getDt_fim() == null)
						|| (this.dt_fim != null && this.dt_fim.equals(other.getDt_fim())))
				&& ((this.dt_inicio == null && other.getDt_inicio() == null)
						|| (this.dt_inicio != null && this.dt_inicio.equals(other.getDt_inicio())))
				&& ((this.id_plano == null && other.getId_plano() == null)
						|| (this.id_plano != null && this.id_plano.equals(other.getId_plano())))
				&& ((this.id_tipo_categoria == null && other.getId_tipo_categoria() == null)
						|| (this.id_tipo_categoria != null
								&& this.id_tipo_categoria.equals(other.getId_tipo_categoria())))
				&& ((this.lojistas_autorizados == null && other.getLojistas_autorizados() == null)
						|| (this.lojistas_autorizados != null
								&& java.util.Arrays.equals(this.lojistas_autorizados, other.getLojistas_autorizados())))
				&& ((this.prazos == null && other.getPrazos() == null)
						|| (this.prazos != null && java.util.Arrays.equals(this.prazos, other.getPrazos())))
				&& ((this.vl_ano_final == null && other.getVl_ano_final() == null)
						|| (this.vl_ano_final != null && this.vl_ano_final.equals(other.getVl_ano_final())))
				&& ((this.vl_ano_inicial == null && other.getVl_ano_inicial() == null)
						|| (this.vl_ano_inicial != null && this.vl_ano_inicial.equals(other.getVl_ano_inicial())));
		__equalsCalc = null;
		return _equals;
	}

    @Transient
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDt_fim() != null) {
			_hashCode += getDt_fim().hashCode();
		}
		if (getDt_inicio() != null) {
			_hashCode += getDt_inicio().hashCode();
		}
		if (getId_plano() != null) {
			_hashCode += getId_plano().hashCode();
		}
		if (getId_tipo_categoria() != null) {
			_hashCode += getId_tipo_categoria().hashCode();
		}
		if (getLojistas_autorizados() != null) {
			for (int i = 0; i < java.lang.reflect.Array.getLength(getLojistas_autorizados()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(getLojistas_autorizados(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getPrazos() != null) {
			for (int i = 0; i < java.lang.reflect.Array.getLength(getPrazos()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(getPrazos(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getVl_ano_final() != null) {
			_hashCode += getVl_ano_final().hashCode();
		}
		if (getVl_ano_inicial() != null) {
			_hashCode += getVl_ano_inicial().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Planos.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Planos"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("dt_fim");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "dt_fim"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("dt_inicio");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "dt_inicio"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_plano");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "id_plano"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("id_tipo_categoria");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"id_tipo_categoria"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("lojistas_autorizados");
		elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model",
				"lojistas_autorizados"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		elemField.setItemQName(
				new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("prazos");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "prazos"));
		elemField.setXmlType(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Prazos"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		elemField.setItemQName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Prazos"));
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_ano_final");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_ano_final"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vl_ano_inicial");
		elemField.setXmlName(
				new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "vl_ano_inicial"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
