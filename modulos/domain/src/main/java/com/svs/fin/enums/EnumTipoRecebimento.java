package com.svs.fin.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnumTipoRecebimento implements EnumLabeled {

	@JsonProperty("Outros")
	OUTROS("Outros"), @JsonProperty("Consórcio")
	CONSORCIO("Consórcio"), @JsonProperty("Financeira")
	FINANCEIRA("Financeira"), @JsonProperty("Veículo usado")
	VEICULO_USADO("Veículo usado"), @JsonProperty("Cheque")
	CHEQUE("Cheque");

	private EnumTipoRecebimento(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
