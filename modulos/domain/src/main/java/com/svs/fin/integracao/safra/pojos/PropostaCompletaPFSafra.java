package com.svs.fin.integracao.safra.pojos;

public class PropostaCompletaPFSafra {
	private com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum; 
	private com.svs.fin.integracao.safra.model.Proposta_PF propostaPf;
	private com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas;
	private com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento;
	
	public com.svs.fin.integracao.safra.model.Cadastro_Comum getPropostaComum() {
		return propostaComum;
	}
	public void setPropostaComum(com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum) {
		this.propostaComum = propostaComum;
	}
	public com.svs.fin.integracao.safra.model.Proposta_PF getPropostaPf() {
		return propostaPf;
	}
	public void setPropostaPf(com.svs.fin.integracao.safra.model.Proposta_PF propostaPf) {
		this.propostaPf = propostaPf;
	}
	public com.svs.fin.integracao.safra.model.Proposta_Avalista getAvalistas() {
		return avalistas;
	}
	public void setAvalistas(com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas) {
		this.avalistas = avalistas;
	}
	public com.svs.fin.integracao.safra.model.Financiamento getPropostaFinanciamento() {
		return propostaFinanciamento;
	}
	public void setPropostaFinanciamento(com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) {
		this.propostaFinanciamento = propostaFinanciamento;
	}
}
