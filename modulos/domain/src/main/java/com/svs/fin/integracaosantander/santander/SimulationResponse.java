package com.svs.fin.integracaosantander.santander;

import java.io.Serializable;

public class SimulationResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6295932729171049911L;

	private String lastInstallmentValue;

	private String releasedValue;

	private String downPayment;

	private String initialInstallmentValue;

	private String vehicleValue;

	private String installmentValue;

	private String totalValueFinanced;

	private String installmentAmount;

	private String paymentFormId;

	private String uuid;	

	private String fleetId;

	private String insuranceId;

	private String insuranceValue;
	
	
	
	public String getLastInstallmentValue() {
		return lastInstallmentValue;
	}

	public void setLastInstallmentValue(String lastInstallmentValue) {
		this.lastInstallmentValue = lastInstallmentValue;
	}

	public String getReleasedValue() {
		return releasedValue;
	}

	public void setReleasedValue(String releasedValue) {
		this.releasedValue = releasedValue;
	}

	public String getDownPayment() {
		return downPayment;
	}

	public void setDownPayment(String downPayment) {
		this.downPayment = downPayment;
	}

	public String getInitialInstallmentValue() {
		return initialInstallmentValue;
	}

	public void setInitialInstallmentValue(String initialInstallmentValue) {
		this.initialInstallmentValue = initialInstallmentValue;
	}

	public String getVehicleValue() {
		return vehicleValue;
	}

	public void setVehicleValue(String vehicleValue) {
		this.vehicleValue = vehicleValue;
	}

	public String getInstallmentValue() {
		return installmentValue;
	}

	public void setInstallmentValue(String installmentValue) {
		this.installmentValue = installmentValue;
	}

	public String getTotalValueFinanced() {
		return totalValueFinanced;
	}

	public void setTotalValueFinanced(String totalValueFinanced) {
		this.totalValueFinanced = totalValueFinanced;
	}

	public String getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public String getPaymentFormId() {
		return paymentFormId;
	}

	public void setPaymentFormId(String paymentFormId) {
		this.paymentFormId = paymentFormId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getFleetId() {
		return fleetId;
	}

	public void setFleetId(String fleetId) {
		this.fleetId = fleetId;
	}

	public String getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(String insuranceId) {
		this.insuranceId = insuranceId;
	}

	public String getInsuranceValue() {
		return insuranceValue;
	}

	public void setInsuranceValue(String insuranceValue) {
		this.insuranceValue = insuranceValue;
	}

	@Override
	public String toString() {
		return "ClassPojo [lastInstallmentValue = " + lastInstallmentValue + ", releasedValue = " + releasedValue
				+ ", downPayment = " + downPayment + ", initialInstallmentValue = " + initialInstallmentValue
				+ ", vehicleValue = " + vehicleValue + ", installmentValue = " + installmentValue
				+ ", totalValueFinanced = " + totalValueFinanced + ", installmentAmount = " + installmentAmount
				+ ", paymentFormId = " + paymentFormId + ", uuid = " + uuid + ", fleetId = " + fleetId
				+ ", insuranceId = " + insuranceId + ", insuranceValue = " + insuranceValue + "]";
	}

	public Double getTotalValueFinancedDouble() {
		if(totalValueFinanced != null && !totalValueFinanced.equals("")) {
			return new Double(totalValueFinanced);
		}
		return null;
	}
	
	public Double getDownPaymentDouble() {
		if(downPayment != null && !downPayment.equals("")) {
			return new Double(downPayment);
		}
		return null;
	}
	
	public Integer getInstallmentAmountInteger() {
		if(installmentAmount != null && !installmentAmount.equals("")) {
			return new Integer(installmentAmount);
		}
		return null;
	}
	
	public Double getInstallmentValueDouble() {
		if(installmentValue != null && !installmentValue.equals("")) {
			return new Double(installmentValue);
		}
		return null;
	}
	
	public Double getReleasedValueDouble() {
		if(releasedValue != null && !releasedValue.equals("")) {
			return new Double(releasedValue);
		}
		return null;
	}
	
	public Double getVehicleValueDouble() {
		if(vehicleValue != null && !vehicleValue.equals("")) {
			return new Double(vehicleValue);
		}
		return null;
	}
	
	public Double getInitialInstallmentValueDouble() {
		if(initialInstallmentValue != null && !initialInstallmentValue.equals("")) {
			return new Double(initialInstallmentValue);
		}
		return null;
	}
}
