/**
 * Fluxo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.svs.fin.integracao.safra.model_dados_financiamento;

public class Fluxo  extends com.svs.fin.integracao.safra.model.Padrao  implements java.io.Serializable {
    private java.lang.String id_cpf_age_cert;

    private java.lang.String id_operador;

    private java.lang.String id_servico;

    private java.lang.String qtd_bens;

    public Fluxo() {
    }

    public Fluxo(
           java.lang.String id_cpf_age_cert,
           java.lang.String id_operador,
           java.lang.String id_servico,
           java.lang.String qtd_bens) {
        this.id_cpf_age_cert = id_cpf_age_cert;
        this.id_operador = id_operador;
        this.id_servico = id_servico;
        this.qtd_bens = qtd_bens;
    }


    /**
     * Gets the id_cpf_age_cert value for this Fluxo.
     * 
     * @return id_cpf_age_cert
     */
    public java.lang.String getId_cpf_age_cert() {
        return id_cpf_age_cert;
    }


    /**
     * Sets the id_cpf_age_cert value for this Fluxo.
     * 
     * @param id_cpf_age_cert
     */
    public void setId_cpf_age_cert(java.lang.String id_cpf_age_cert) {
        this.id_cpf_age_cert = id_cpf_age_cert;
    }


    /**
     * Gets the id_operador value for this Fluxo.
     * 
     * @return id_operador
     */
    public java.lang.String getId_operador() {
        return id_operador;
    }


    /**
     * Sets the id_operador value for this Fluxo.
     * 
     * @param id_operador
     */
    public void setId_operador(java.lang.String id_operador) {
        this.id_operador = id_operador;
    }


    /**
     * Gets the id_servico value for this Fluxo.
     * 
     * @return id_servico
     */
    public java.lang.String getId_servico() {
        return id_servico;
    }


    /**
     * Sets the id_servico value for this Fluxo.
     * 
     * @param id_servico
     */
    public void setId_servico(java.lang.String id_servico) {
        this.id_servico = id_servico;
    }


    /**
     * Gets the qtd_bens value for this Fluxo.
     * 
     * @return qtd_bens
     */
    public java.lang.String getQtd_bens() {
        return qtd_bens;
    }


    /**
     * Sets the qtd_bens value for this Fluxo.
     * 
     * @param qtd_bens
     */
    public void setQtd_bens(java.lang.String qtd_bens) {
        this.qtd_bens = qtd_bens;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Fluxo)) return false;
        Fluxo other = (Fluxo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.id_cpf_age_cert==null && other.getId_cpf_age_cert()==null) || 
             (this.id_cpf_age_cert!=null &&
              this.id_cpf_age_cert.equals(other.getId_cpf_age_cert()))) &&
            ((this.id_operador==null && other.getId_operador()==null) || 
             (this.id_operador!=null &&
              this.id_operador.equals(other.getId_operador()))) &&
            ((this.id_servico==null && other.getId_servico()==null) || 
             (this.id_servico!=null &&
              this.id_servico.equals(other.getId_servico()))) &&
            ((this.qtd_bens==null && other.getQtd_bens()==null) || 
             (this.qtd_bens!=null &&
              this.qtd_bens.equals(other.getQtd_bens())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getId_cpf_age_cert() != null) {
            _hashCode += getId_cpf_age_cert().hashCode();
        }
        if (getId_operador() != null) {
            _hashCode += getId_operador().hashCode();
        }
        if (getId_servico() != null) {
            _hashCode += getId_servico().hashCode();
        }
        if (getQtd_bens() != null) {
            _hashCode += getQtd_bens().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Fluxo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Fluxo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_cpf_age_cert");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "id_cpf_age_cert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_operador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "id_operador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_servico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "id_servico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtd_bens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "qtd_bens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
