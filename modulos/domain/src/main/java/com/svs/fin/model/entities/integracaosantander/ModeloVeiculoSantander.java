package com.svs.fin.model.entities.integracaosantander;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.enums.EnumDepartamento;
import com.svs.fin.model.entities.MarcaSantander;
import com.svs.fin.model.entities.UnidadeOrganizacional;
import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander.NQ_OBTER_POR_ID_MARCA;

@Table(name = "MODELO_SANTANDER")
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({ @NamedQuery(name = NQ_OBTER_POR_ID_MARCA.NAME, query = NQ_OBTER_POR_ID_MARCA.JPQL) })
public class ModeloVeiculoSantander implements Serializable {

	public static interface NQ_OBTER_POR_ID_MARCA {
		public static final String NAME = "ModeloVeiculoSantander.obterPorIdMarca";
		public static final String JPQL = "Select DISTINCT m from ModeloVeiculoSantander m where m.marca.id = :"
				+ NQ_OBTER_POR_ID_MARCA.NM_PM_ID_MARCA;
		public static final String NM_PM_ID_MARCA = "idMarca";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7994872801170120309L;

	@Id
	private Long id;

	@Column(length = 100)
	private String description;

	@Column(length = 100)
	private String integrationCode;

	@ManyToOne
	@JoinColumn(name = "id_marca")
	private MarcaSantander marca;

	@Column
	private Boolean ativo;

	@OneToMany(mappedBy = "modelo")
	private List<AnoModeloCombustivelSantander> anosModelo;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "mod_veic_sant_unidades", joinColumns = @JoinColumn(name = "modelo_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "unidade_id", referencedColumnName = "id"))
	private List<UnidadeOrganizacional> unidades;

	@ElementCollection(targetClass = EnumDepartamento.class)
	@CollectionTable(name = "modelo_departamentos")
	@Column(name = "departamento")
	@Enumerated(EnumType.STRING)
	private List<EnumDepartamento> departamentos;

	@Transient
	private List<EnumDepartamento> departamentosTransient;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntegrationCode() {
		return integrationCode;
	}

	public void setIntegrationCode(String integrationCode) {
		this.integrationCode = integrationCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModeloVeiculoSantander other = (ModeloVeiculoSantander) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public MarcaSantander getMarca() {
		return marca;
	}

	public void setMarca(MarcaSantander marca) {
		this.marca = marca;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<AnoModeloCombustivelSantander> getAnosModelo() {
		if (anosModelo == null) {
			anosModelo = new ArrayList<AnoModeloCombustivelSantander>();
		}
		return anosModelo;
	}

	public void setAnosModelo(List<AnoModeloCombustivelSantander> anosModelo) {
		this.anosModelo = anosModelo;
	}

	public List<EnumDepartamento> getDepartamentos() {
		if (departamentos == null) {
			departamentos = new ArrayList<EnumDepartamento>();
		}
		return departamentos;
	}

	public void setDepartamentos(List<EnumDepartamento> departamentos) {
		this.departamentos = departamentos;
	}

	public List<EnumDepartamento> getDepartamentosTransient() {
		if (departamentosTransient == null) {
			departamentosTransient = new ArrayList<EnumDepartamento>();
		}
		return departamentosTransient;
	}

	public void setDepartamentosTransient(List<EnumDepartamento> departamentosTransient) {
		this.departamentosTransient = departamentosTransient;
	}

	public List<UnidadeOrganizacional> getUnidades() {
		if (unidades == null) {
			unidades = new ArrayList<UnidadeOrganizacional>();
		}
		return unidades;
	}

	public void setUnidades(List<UnidadeOrganizacional> unidades) {
		this.unidades = unidades;
	}
}
