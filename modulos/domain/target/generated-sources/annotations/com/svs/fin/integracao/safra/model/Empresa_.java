package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Empresa.class)
public abstract class Empresa_ {

	public static volatile SingularAttribute<Empresa, String> ds_cnpj;
	public static volatile SingularAttribute<Empresa, String> nr_numero;
	public static volatile SingularAttribute<Empresa, String> id_uf;
	public static volatile SingularAttribute<Empresa, String> nm_logradouro;
	public static volatile SingularAttribute<Empresa, Integer> id_empresa;
	public static volatile SingularAttribute<Empresa, String> ds_cep;
	public static volatile SingularAttribute<Empresa, String> ds_cidade;
	public static volatile SingularAttribute<Empresa, String> ds_complemento;
	public static volatile SingularAttribute<Empresa, String> nm_empresa_full;
	public static volatile SingularAttribute<Empresa, String> nm_empresa;

}

