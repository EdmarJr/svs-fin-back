package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Segmento_Veiculo.class)
public abstract class Segmento_Veiculo_ {

	public static volatile SingularAttribute<Segmento_Veiculo, Integer> id_segmento_veiculo;
	public static volatile SingularAttribute<Segmento_Veiculo, String> ds_segmento_veiculo;

}

