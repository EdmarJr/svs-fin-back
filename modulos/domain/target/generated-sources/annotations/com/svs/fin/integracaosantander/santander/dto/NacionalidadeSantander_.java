package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NacionalidadeSantander.class)
public abstract class NacionalidadeSantander_ {

	public static volatile SingularAttribute<NacionalidadeSantander, String> description;
	public static volatile SingularAttribute<NacionalidadeSantander, Long> id;

}

