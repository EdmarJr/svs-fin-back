package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Dirigentes_Cargo_Sub.class)
public abstract class Dirigentes_Cargo_Sub_ {

	public static volatile SingularAttribute<Dirigentes_Cargo_Sub, Integer> id_dirigente_cargo_sub;
	public static volatile SingularAttribute<Dirigentes_Cargo_Sub, String> ds_dirigente_cargo_sub;
	public static volatile SingularAttribute<Dirigentes_Cargo_Sub, Integer> id_dirigente_cargo;

}

