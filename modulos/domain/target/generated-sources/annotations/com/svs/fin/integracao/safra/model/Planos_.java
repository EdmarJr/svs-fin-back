package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Planos.class)
public abstract class Planos_ {

	public static volatile SingularAttribute<Planos, String> id_plano;
	public static volatile SingularAttribute<Planos, String[]> lojistas_autorizados;
	public static volatile SingularAttribute<Planos, Integer> vl_ano_inicial;
	public static volatile SingularAttribute<Planos, String> dt_fim;
	public static volatile SingularAttribute<Planos, String> dt_inicio;
	public static volatile SingularAttribute<Planos, Integer> id_tipo_categoria;
	public static volatile SingularAttribute<Planos, Prazos[]> prazos;
	public static volatile SingularAttribute<Planos, Integer> vl_ano_final;

}

