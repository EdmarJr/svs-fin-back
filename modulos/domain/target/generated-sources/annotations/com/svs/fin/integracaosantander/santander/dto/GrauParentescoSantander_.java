package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GrauParentescoSantander.class)
public abstract class GrauParentescoSantander_ {

	public static volatile SingularAttribute<GrauParentescoSantander, String> integrationCode;
	public static volatile SingularAttribute<GrauParentescoSantander, String> description;
	public static volatile SingularAttribute<GrauParentescoSantander, Long> id;

}

