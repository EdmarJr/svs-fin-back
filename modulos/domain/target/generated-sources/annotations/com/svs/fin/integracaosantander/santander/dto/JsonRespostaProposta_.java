package com.svs.fin.integracaosantander.santander.dto;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonRespostaProposta.class)
public abstract class JsonRespostaProposta_ {

	public static volatile SingularAttribute<JsonRespostaProposta, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonRespostaProposta, Calendar> data;
	public static volatile SingularAttribute<JsonRespostaProposta, Long> id;

}

