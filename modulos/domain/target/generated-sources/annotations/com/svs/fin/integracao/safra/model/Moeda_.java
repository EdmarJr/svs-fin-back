package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Moeda.class)
public abstract class Moeda_ {

	public static volatile SingularAttribute<Moeda, String> ds_moeda;
	public static volatile SingularAttribute<Moeda, Integer> id_moeda;
	public static volatile SingularAttribute<Moeda, String> fl_default;

}

