package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AtividadeEconomicaSantander.class)
public abstract class AtividadeEconomicaSantander_ {

	public static volatile SingularAttribute<AtividadeEconomicaSantander, TipoAtividadeEconomicaSantander> tipo;
	public static volatile SingularAttribute<AtividadeEconomicaSantander, String> description;
	public static volatile SingularAttribute<AtividadeEconomicaSantander, Long> id;

}

