package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Categoria.class)
public abstract class Tipo_Categoria_ {

	public static volatile SingularAttribute<Tipo_Categoria, String> ds_tipo_categoria;
	public static volatile SingularAttribute<Tipo_Categoria, Integer> id_tipo_categoria;

}

