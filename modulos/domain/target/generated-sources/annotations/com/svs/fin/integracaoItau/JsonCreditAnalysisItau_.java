package com.svs.fin.integracaoItau;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonCreditAnalysisItau.class)
public abstract class JsonCreditAnalysisItau_ {

	public static volatile SingularAttribute<JsonCreditAnalysisItau, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonCreditAnalysisItau, Calendar> data;
	public static volatile SingularAttribute<JsonCreditAnalysisItau, Long> id;

}

