package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OpcionalVeiculo.class)
public abstract class OpcionalVeiculo_ {

	public static volatile SingularAttribute<OpcionalVeiculo, String> opcionalDescricao;
	public static volatile SingularAttribute<OpcionalVeiculo, Long> opcionalCodigo;
	public static volatile SingularAttribute<OpcionalVeiculo, Long> id;

}

