package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Natureza_Ocupacao.class)
public abstract class Natureza_Ocupacao_ {

	public static volatile SingularAttribute<Natureza_Ocupacao, Integer> id_natureza_ocupacao;
	public static volatile SingularAttribute<Natureza_Ocupacao, String> fl_obriga_beneficio;
	public static volatile SingularAttribute<Natureza_Ocupacao, String> fl_obriga_cnpj;
	public static volatile SingularAttribute<Natureza_Ocupacao, String> ds_natureza_ocupacao;

}

