package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoConta;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ReferenciaBancaria.class)
public abstract class ReferenciaBancaria_ {

	public static volatile SingularAttribute<ReferenciaBancaria, Estado> estado;
	public static volatile SingularAttribute<ReferenciaBancaria, Cidade> cidade;
	public static volatile SingularAttribute<ReferenciaBancaria, String> digitoAgencia;
	public static volatile SingularAttribute<ReferenciaBancaria, String> conta;
	public static volatile SingularAttribute<ReferenciaBancaria, Banco> banco;
	public static volatile SingularAttribute<ReferenciaBancaria, String> nome;
	public static volatile SingularAttribute<ReferenciaBancaria, String> agencia;
	public static volatile SingularAttribute<ReferenciaBancaria, String> telefoneContato;
	public static volatile SingularAttribute<ReferenciaBancaria, Calendar> clienteDesde;
	public static volatile SingularAttribute<ReferenciaBancaria, String> digitoConta;
	public static volatile SingularAttribute<ReferenciaBancaria, Long> id;
	public static volatile SingularAttribute<ReferenciaBancaria, String> contato;
	public static volatile SingularAttribute<ReferenciaBancaria, EnumTipoConta> tipoConta;

}

