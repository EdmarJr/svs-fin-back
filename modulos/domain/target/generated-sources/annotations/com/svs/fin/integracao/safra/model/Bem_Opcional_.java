package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bem_Opcional.class)
public abstract class Bem_Opcional_ {

	public static volatile SingularAttribute<Bem_Opcional, String> ds_bem_opcional;
	public static volatile SingularAttribute<Bem_Opcional, Integer> id_bem_opcional;

}

