package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Documento.class)
public abstract class Tipo_Documento_ {

	public static volatile SingularAttribute<Tipo_Documento, String> ds_tipo_documento;
	public static volatile SingularAttribute<Tipo_Documento, Integer> id_tipo_documento;

}

