package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OpcaoItemCbc.class)
public abstract class OpcaoItemCbc_ {

	public static volatile SingularAttribute<OpcaoItemCbc, Long> codigoDealerWorkflow;
	public static volatile SingularAttribute<OpcaoItemCbc, UnidadeOrganizacional> unidadeOrganizacional;
	public static volatile SingularAttribute<OpcaoItemCbc, Boolean> ativo;
	public static volatile SingularAttribute<OpcaoItemCbc, Long> id;
	public static volatile SingularAttribute<OpcaoItemCbc, String> descricao;

}

