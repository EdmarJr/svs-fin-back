package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AcessUser.class)
public abstract class AcessUser_ {

	public static volatile SingularAttribute<AcessUser, String> password;
	public static volatile ListAttribute<AcessUser, AcessUserRoleUnidadeOrganizacional> acessos;
	public static volatile SingularAttribute<AcessUser, String> matricula;
	public static volatile SingularAttribute<AcessUser, String> cpf;
	public static volatile SingularAttribute<AcessUser, String> nome;
	public static volatile SingularAttribute<AcessUser, String> lotacao;
	public static volatile SingularAttribute<AcessUser, Long> id;
	public static volatile SingularAttribute<AcessUser, String> login;

}

