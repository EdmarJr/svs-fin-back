package com.svs.fin.model.entities.integracaosantander;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AnoModeloCombustivelSantander.class)
public abstract class AnoModeloCombustivelSantander_ {

	public static volatile SingularAttribute<AnoModeloCombustivelSantander, String> integrationCode;
	public static volatile SingularAttribute<AnoModeloCombustivelSantander, String> description;
	public static volatile SingularAttribute<AnoModeloCombustivelSantander, Long> id;
	public static volatile SingularAttribute<AnoModeloCombustivelSantander, ModeloVeiculoSantander> modelo;

}

