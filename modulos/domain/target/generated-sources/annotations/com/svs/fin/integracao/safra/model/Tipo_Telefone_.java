package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Telefone.class)
public abstract class Tipo_Telefone_ {

	public static volatile SingularAttribute<Tipo_Telefone, Integer> id_tipo_telefone;
	public static volatile SingularAttribute<Tipo_Telefone, String> ds_tipo_telefone;

}

