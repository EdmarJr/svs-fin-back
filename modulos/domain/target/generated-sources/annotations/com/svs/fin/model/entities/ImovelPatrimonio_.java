package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoImovel;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImovelPatrimonio.class)
public abstract class ImovelPatrimonio_ {

	public static volatile SingularAttribute<ImovelPatrimonio, EnumTipoImovel> tipoImovel;
	public static volatile SingularAttribute<ImovelPatrimonio, Endereco> endereco;
	public static volatile SingularAttribute<ImovelPatrimonio, Double> valor;
	public static volatile SingularAttribute<ImovelPatrimonio, Long> id;
	public static volatile SingularAttribute<ImovelPatrimonio, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<ImovelPatrimonio, Avalista> avalista;
	public static volatile SingularAttribute<ImovelPatrimonio, String> descricao;

}

