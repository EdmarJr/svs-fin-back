package com.svs.fin.model.entities;

import br.com.gruposaga.sdk.zflow.model.gen.enums.BankCodeEnum;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DeParaBancoZFlow.class)
public abstract class DeParaBancoZFlow_ {

	public static volatile SingularAttribute<DeParaBancoZFlow, BankCodeEnum> bankCodeEnum;
	public static volatile SingularAttribute<DeParaBancoZFlow, Banco> banco;
	public static volatile SingularAttribute<DeParaBancoZFlow, Long> id;

}

