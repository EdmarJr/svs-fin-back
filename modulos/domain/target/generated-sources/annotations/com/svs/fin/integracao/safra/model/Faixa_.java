package com.svs.fin.integracao.safra.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Faixa.class)
public abstract class Faixa_ {

	public static volatile SingularAttribute<Faixa, BigDecimal> pcEntradaMinima;
	public static volatile SingularAttribute<Faixa, BigDecimal> pcTaxa;
	public static volatile SingularAttribute<Faixa, BigDecimal> vlTC;
	public static volatile SingularAttribute<Faixa, Integer> idFaixa;
	public static volatile SingularAttribute<Faixa, Coeficiente[]> coeficientes;
	public static volatile SingularAttribute<Faixa, BigDecimal> pcPlus;
	public static volatile SingularAttribute<Faixa, BigDecimal> vlFinanciamentoMaximo;
	public static volatile SingularAttribute<Faixa, Integer> qtPrazo;
	public static volatile SingularAttribute<Faixa, BigDecimal> vlFinanciamentoMinimo;

}

