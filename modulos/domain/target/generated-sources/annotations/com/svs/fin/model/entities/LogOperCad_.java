package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LogOperCad.class)
public abstract class LogOperCad_ {

	public static volatile SingularAttribute<LogOperCad, String> oQueMudou;
	public static volatile SingularAttribute<LogOperCad, String> idObjetoNegocio;
	public static volatile SingularAttribute<LogOperCad, String> ipMaquina;
	public static volatile SingularAttribute<LogOperCad, String> nomeHost;
	public static volatile SingularAttribute<LogOperCad, Long> id;
	public static volatile SingularAttribute<LogOperCad, String> tipoOperacao;
	public static volatile SingularAttribute<LogOperCad, String> classeBean;
	public static volatile SingularAttribute<LogOperCad, AcessUser> user;
	public static volatile SingularAttribute<LogOperCad, Calendar> dataOperacao;

}

