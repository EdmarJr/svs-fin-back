package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ReferenciaPessoal.class)
public abstract class ReferenciaPessoal_ {

	public static volatile SingularAttribute<ReferenciaPessoal, String> relacionamento;
	public static volatile SingularAttribute<ReferenciaPessoal, String> telefoneFixo;
	public static volatile SingularAttribute<ReferenciaPessoal, String> nome;
	public static volatile SingularAttribute<ReferenciaPessoal, Long> id;
	public static volatile SingularAttribute<ReferenciaPessoal, String> telefoneCelular;
	public static volatile SingularAttribute<ReferenciaPessoal, ClienteSvs> clienteSvs;

}

