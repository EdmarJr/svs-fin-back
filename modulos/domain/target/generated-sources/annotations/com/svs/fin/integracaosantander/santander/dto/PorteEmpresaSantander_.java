package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PorteEmpresaSantander.class)
public abstract class PorteEmpresaSantander_ {

	public static volatile SingularAttribute<PorteEmpresaSantander, String> description;
	public static volatile SingularAttribute<PorteEmpresaSantander, Long> id;

}

