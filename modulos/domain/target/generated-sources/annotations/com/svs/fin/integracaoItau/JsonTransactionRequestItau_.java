package com.svs.fin.integracaoItau;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonTransactionRequestItau.class)
public abstract class JsonTransactionRequestItau_ {

	public static volatile SingularAttribute<JsonTransactionRequestItau, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonTransactionRequestItau, String> cpfCliente;
	public static volatile SingularAttribute<JsonTransactionRequestItau, Calendar> data;
	public static volatile SingularAttribute<JsonTransactionRequestItau, Long> id;
	public static volatile SingularAttribute<JsonTransactionRequestItau, String> nomeCliente;

}

