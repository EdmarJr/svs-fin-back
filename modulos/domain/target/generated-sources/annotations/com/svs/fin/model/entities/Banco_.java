package com.svs.fin.model.entities;

import com.svs.fin.enums.SimNao;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Banco.class)
public abstract class Banco_ {

	public static volatile SingularAttribute<Banco, String> site;
	public static volatile SingularAttribute<Banco, SimNao> ativo;
	public static volatile SingularAttribute<Banco, String> descBanco;
	public static volatile SingularAttribute<Banco, String> numero;
	public static volatile SingularAttribute<Banco, Long> id;

}

