package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Profissao.class)
public abstract class Profissao_ {

	public static volatile SingularAttribute<Profissao, Integer> id_natureza_ocupacao;
	public static volatile SingularAttribute<Profissao, Integer> id_profissao;
	public static volatile SingularAttribute<Profissao, String> ds_profissao;

}

