package com.svs.fin.integracaosantander.santander.dto;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonRequisicaoProposta.class)
public abstract class JsonRequisicaoProposta_ {

	public static volatile SingularAttribute<JsonRequisicaoProposta, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonRequisicaoProposta, Calendar> data;
	public static volatile SingularAttribute<JsonRequisicaoProposta, Long> id;

}

