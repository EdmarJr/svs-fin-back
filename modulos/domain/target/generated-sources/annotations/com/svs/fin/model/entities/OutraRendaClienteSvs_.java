package com.svs.fin.model.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OutraRendaClienteSvs.class)
public abstract class OutraRendaClienteSvs_ {

	public static volatile SingularAttribute<OutraRendaClienteSvs, String> telefone;
	public static volatile SingularAttribute<OutraRendaClienteSvs, BigDecimal> valor;
	public static volatile SingularAttribute<OutraRendaClienteSvs, Long> id;
	public static volatile SingularAttribute<OutraRendaClienteSvs, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<OutraRendaClienteSvs, Avalista> avalista;
	public static volatile SingularAttribute<OutraRendaClienteSvs, String> descricao;

}

