package com.svs.fin.model.entities;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProdutoFinanceiro.class)
public abstract class ProdutoFinanceiro_ {

	public static volatile SingularAttribute<ProdutoFinanceiro, Boolean> ativo;
	public static volatile SingularAttribute<ProdutoFinanceiro, LocalDateTime> dataHoraDeAtualizacao;
	public static volatile SingularAttribute<ProdutoFinanceiro, Usuario> usuarioResponsavelUltimaAlteracao;
	public static volatile SingularAttribute<ProdutoFinanceiro, String> nome;
	public static volatile SingularAttribute<ProdutoFinanceiro, Long> id;
	public static volatile SingularAttribute<ProdutoFinanceiro, LocalDateTime> dataHoraDeCriacao;

}

