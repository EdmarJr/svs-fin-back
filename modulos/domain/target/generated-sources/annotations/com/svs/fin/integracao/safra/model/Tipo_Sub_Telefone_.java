package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Sub_Telefone.class)
public abstract class Tipo_Sub_Telefone_ {

	public static volatile SingularAttribute<Tipo_Sub_Telefone, String> ds_tipo_sub_telefone;
	public static volatile SingularAttribute<Tipo_Sub_Telefone, Integer> id_tipo_sub_telefone;
	public static volatile SingularAttribute<Tipo_Sub_Telefone, Integer> id_tipo_telefone;

}

