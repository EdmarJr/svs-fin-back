package com.svs.fin.integracaosantander.santander.dto;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonRequisicaoSimulacaoSantander.class)
public abstract class JsonRequisicaoSimulacaoSantander_ {

	public static volatile SingularAttribute<JsonRequisicaoSimulacaoSantander, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonRequisicaoSimulacaoSantander, Calendar> data;
	public static volatile SingularAttribute<JsonRequisicaoSimulacaoSantander, Long> id;

}

