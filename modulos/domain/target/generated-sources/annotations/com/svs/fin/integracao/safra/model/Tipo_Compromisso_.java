package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Compromisso.class)
public abstract class Tipo_Compromisso_ {

	public static volatile SingularAttribute<Tipo_Compromisso, Integer> id_compromisso;
	public static volatile SingularAttribute<Tipo_Compromisso, String> ds_compromisso;

}

