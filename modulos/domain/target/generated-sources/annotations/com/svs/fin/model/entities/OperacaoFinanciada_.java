package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumDepartamentoSvs;
import com.svs.fin.enums.EnumFaseOperacaoFinanciada;
import com.svs.fin.enums.EnumTipoTabelaFinanceira;
import com.svs.fin.enums.EnumTipoVeiculo;
import com.svs.fin.integracaoItau.FinanciamentoOnlineItau;
import com.svs.fin.model.entities.integracaosantander.AnoModeloCombustivelSantander;
import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OperacaoFinanciada.class)
public abstract class OperacaoFinanciada_ {

	public static volatile SingularAttribute<OperacaoFinanciada, EnumTipoVeiculo> tipoVeiculo;
	public static volatile SingularAttribute<OperacaoFinanciada, Double> valorEntradaItau;
	public static volatile SingularAttribute<OperacaoFinanciada, InstituicaoFinanceira> instituicaoFinanceiraFaturamento;
	public static volatile ListAttribute<OperacaoFinanciada, AnexoCad> anexos;
	public static volatile ListAttribute<OperacaoFinanciada, CalculoRentabilidade> calculosEnviadosIF;
	public static volatile SingularAttribute<OperacaoFinanciada, UnidadeOrganizacional> unidade;
	public static volatile ListAttribute<OperacaoFinanciada, ItemCBC> itensCbc;
	public static volatile SingularAttribute<OperacaoFinanciada, Integer> parcelas;
	public static volatile SingularAttribute<OperacaoFinanciada, Double> valorAcessoriosItau;
	public static volatile SingularAttribute<OperacaoFinanciada, Calendar> dataUltimaMovimentacao;
	public static volatile SingularAttribute<OperacaoFinanciada, EnumTipoTabelaFinanceira> tipoTabelaFaturamento;
	public static volatile SingularAttribute<OperacaoFinanciada, Calendar> dataEntrada;
	public static volatile SingularAttribute<OperacaoFinanciada, String> cpfVendedor;
	public static volatile SingularAttribute<OperacaoFinanciada, CalculoRentabilidade> calculoFaturamento;
	public static volatile SingularAttribute<OperacaoFinanciada, Long> id;
	public static volatile ListAttribute<OperacaoFinanciada, AndamentoOperacao> andamentos;
	public static volatile SingularAttribute<OperacaoFinanciada, Double> valorEntrada;
	public static volatile SingularAttribute<OperacaoFinanciada, AcessUser> vendedor;
	public static volatile SingularAttribute<OperacaoFinanciada, EnumFaseOperacaoFinanciada> fase;
	public static volatile SingularAttribute<OperacaoFinanciada, BigDecimal> valorCbc;
	public static volatile SingularAttribute<OperacaoFinanciada, Proposta> proposta;
	public static volatile ListAttribute<OperacaoFinanciada, ParcelaEntrada> parcelasEntrada;
	public static volatile SingularAttribute<OperacaoFinanciada, BigDecimal> valor;
	public static volatile SingularAttribute<OperacaoFinanciada, String> numeroProposta;
	public static volatile SingularAttribute<OperacaoFinanciada, ModeloVeiculoSantander> modeloVeiculoSantander;
	public static volatile SingularAttribute<OperacaoFinanciada, ClienteSvs> cliente;
	public static volatile ListAttribute<OperacaoFinanciada, CalculoRentabilidade> todasRentabilidades;
	public static volatile SingularAttribute<OperacaoFinanciada, MarcaSantander> marcaSantander;
	public static volatile ListAttribute<OperacaoFinanciada, FinanciamentoOnlineItau> financiamentosOnlineItau;
	public static volatile SingularAttribute<OperacaoFinanciada, EnumDepartamentoSvs> departamento;
	public static volatile SingularAttribute<OperacaoFinanciada, Double> valorVeiculoItau;
	public static volatile SingularAttribute<OperacaoFinanciada, BigDecimal> valorCbcFinanciamento;
	public static volatile SingularAttribute<OperacaoFinanciada, AnoModeloCombustivelSantander> anoModeloCombustivelSantander;
	public static volatile SingularAttribute<OperacaoFinanciada, Double> valorFinanciado;
	public static volatile SingularAttribute<OperacaoFinanciada, CalculoRentabilidade> calculoTabelaInstitucional;
	public static volatile SingularAttribute<OperacaoFinanciada, Double> valorFinanciadoItau;

}

