package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoIPTU;
import com.svs.fin.enums.EnumTipoLogradouro;
import com.svs.fin.enums.EnumTipoResidenciaSVS;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Endereco.class)
public abstract class Endereco_ {

	public static volatile SingularAttribute<Endereco, EnumTipoIPTU> enumTipoIPTU;
	public static volatile SingularAttribute<Endereco, String> numero;
	public static volatile SingularAttribute<Endereco, Municipio> municipio;
	public static volatile SingularAttribute<Endereco, String> bairro;
	public static volatile SingularAttribute<Endereco, Calendar> naCidadeDesde;
	public static volatile SingularAttribute<Endereco, EnumTipoResidenciaSVS> tipoResidencia;
	public static volatile SingularAttribute<Endereco, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<Endereco, String> cep;
	public static volatile SingularAttribute<Endereco, Long> codigoDealerWorkflow;
	public static volatile SingularAttribute<Endereco, String> complemento;
	public static volatile SingularAttribute<Endereco, Calendar> noImovelDesde;
	public static volatile SingularAttribute<Endereco, String> logradouro;
	public static volatile SingularAttribute<Endereco, Long> id;
	public static volatile SingularAttribute<Endereco, EnumTipoLogradouro> tipoLogradouro;
	public static volatile SingularAttribute<Endereco, TipoEndereco> tipoEndereco;

}

