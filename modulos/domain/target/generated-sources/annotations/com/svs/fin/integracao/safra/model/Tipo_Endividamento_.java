package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Endividamento.class)
public abstract class Tipo_Endividamento_ {

	public static volatile SingularAttribute<Tipo_Endividamento, Integer> id_tipo_endividamento;
	public static volatile SingularAttribute<Tipo_Endividamento, String> ds_tipo_endividamento;

}

