package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoRecebimento;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ParcelaEntrada.class)
public abstract class ParcelaEntrada_ {

	public static volatile SingularAttribute<ParcelaEntrada, ParcelaEntrada> parcelaAbater;
	public static volatile SingularAttribute<ParcelaEntrada, OperacaoFinanciada> operacaoFinanciada;
	public static volatile SingularAttribute<ParcelaEntrada, EnumTipoRecebimento> tipo;
	public static volatile SingularAttribute<ParcelaEntrada, Integer> codigo;
	public static volatile SingularAttribute<ParcelaEntrada, String> observacao;
	public static volatile SingularAttribute<ParcelaEntrada, Calendar> data;
	public static volatile SingularAttribute<ParcelaEntrada, Double> valor;
	public static volatile SingularAttribute<ParcelaEntrada, Long> id;
	public static volatile SingularAttribute<ParcelaEntrada, TipoPagamento> tipoPagamento;
	public static volatile SingularAttribute<ParcelaEntrada, Boolean> flagAbaterFinanciamento;

}

