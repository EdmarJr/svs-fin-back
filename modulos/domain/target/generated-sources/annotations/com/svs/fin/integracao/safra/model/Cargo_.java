package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cargo.class)
public abstract class Cargo_ {

	public static volatile SingularAttribute<Cargo, String> ds_cargo;
	public static volatile SingularAttribute<Cargo, Integer> id_profissao;
	public static volatile SingularAttribute<Cargo, Integer> id_nat_ocupacao;
	public static volatile SingularAttribute<Cargo, Integer> id_cargo;

}

