package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Proposta.class)
public abstract class Proposta_ {

	public static volatile SingularAttribute<Proposta, VeiculoProposta> veiculo;
	public static volatile SingularAttribute<Proposta, Boolean> liberacaoExtra;
	public static volatile SingularAttribute<Proposta, Boolean> aprovacaoCobranca;
	public static volatile SingularAttribute<Proposta, Boolean> enviaEmail;
	public static volatile SingularAttribute<Proposta, UnidadeOrganizacional> unidade;
	public static volatile SingularAttribute<Proposta, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<Proposta, Boolean> aprovacaoEntrega;
	public static volatile SingularAttribute<Proposta, String> empresaCnpj;
	public static volatile SingularAttribute<Proposta, Long> atendimentoCodigo;
	public static volatile SingularAttribute<Proposta, Boolean> aprovacaoGerente;
	public static volatile SingularAttribute<Proposta, String> estoqueTipo;
	public static volatile SingularAttribute<Proposta, Long> id;
	public static volatile SingularAttribute<Proposta, Long> propostaCodigo;
	public static volatile SingularAttribute<Proposta, String> propostaObservacao;
	public static volatile SingularAttribute<Proposta, Double> propostaValor;
	public static volatile SingularAttribute<Proposta, Boolean> entregaPesquisaEmail;

}

