package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Emprego.class)
public abstract class Tipo_Emprego_ {

	public static volatile SingularAttribute<Tipo_Emprego, Integer> id_tipo_emprego;
	public static volatile SingularAttribute<Tipo_Emprego, String> ds_tipo_emprego;

}

