package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Faixas_Patrimonio.class)
public abstract class Faixas_Patrimonio_ {

	public static volatile SingularAttribute<Faixas_Patrimonio, String> ds_faixa_patrimonio;
	public static volatile SingularAttribute<Faixas_Patrimonio, String> id_faixa_patrimonio;

}

