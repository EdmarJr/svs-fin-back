package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tarifa_Avaliacao.class)
public abstract class Tarifa_Avaliacao_ {

	public static volatile SingularAttribute<Tarifa_Avaliacao, String> vl_maximo;
	public static volatile SingularAttribute<Tarifa_Avaliacao, String> tipo_tarifa;

}

