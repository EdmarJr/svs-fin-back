package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Combustivel.class)
public abstract class Tipo_Combustivel_ {

	public static volatile SingularAttribute<Tipo_Combustivel, String> id_combustivel;
	public static volatile SingularAttribute<Tipo_Combustivel, String> ds_combustivel;

}

