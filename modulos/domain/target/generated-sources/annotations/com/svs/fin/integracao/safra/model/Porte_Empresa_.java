package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Porte_Empresa.class)
public abstract class Porte_Empresa_ {

	public static volatile SingularAttribute<Porte_Empresa, String> ds_porte_empresa;
	public static volatile SingularAttribute<Porte_Empresa, Double> vl_final;
	public static volatile SingularAttribute<Porte_Empresa, Integer> id_porte_empresa;
	public static volatile SingularAttribute<Porte_Empresa, Double> vl_inicial;

}

