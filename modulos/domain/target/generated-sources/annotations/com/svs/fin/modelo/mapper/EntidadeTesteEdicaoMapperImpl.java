package com.svs.fin.modelo.mapper;

import com.svs.fin.model.dto.EntidadeTesteEdicaoDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import teste.EntidadeTeste;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
public class EntidadeTesteEdicaoMapperImpl implements EntidadeTesteEdicaoMapper {

    @Override
    public EntidadeTesteEdicaoDto paraDto(EntidadeTeste source) {
        if ( source == null ) {
            return null;
        }

        EntidadeTesteEdicaoDto entidadeTesteEdicaoDto = new EntidadeTesteEdicaoDto();

        entidadeTesteEdicaoDto.setId( source.getId() );
        entidadeTesteEdicaoDto.setDescricao( source.getDescricao() );

        return entidadeTesteEdicaoDto;
    }

    @Override
    public EntidadeTeste doDto(EntidadeTesteEdicaoDto source) {
        if ( source == null ) {
            return null;
        }

        EntidadeTeste entidadeTeste = new EntidadeTeste();

        entidadeTeste.setId( source.getId() );
        entidadeTeste.setDescricao( source.getDescricao() );

        return entidadeTeste;
    }

    @Override
    public List<EntidadeTesteEdicaoDto> paraDto(List<EntidadeTeste> source) {
        if ( source == null ) {
            return null;
        }

        List<EntidadeTesteEdicaoDto> list = new ArrayList<EntidadeTesteEdicaoDto>( source.size() );
        for ( EntidadeTeste entidadeTeste : source ) {
            list.add( paraDto( entidadeTeste ) );
        }

        return list;
    }

    @Override
    public void atualizar(EntidadeTesteEdicaoDto source, EntidadeTeste target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setDescricao( source.getDescricao() );
    }
}
