package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VeiculoProposta.class)
public abstract class VeiculoProposta_ {

	public static volatile SingularAttribute<VeiculoProposta, Integer> veiculoAnoModelo;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoCodigo;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoNrRenavam;
	public static volatile SingularAttribute<VeiculoProposta, Integer> veiculoAnoFabricacao;
	public static volatile SingularAttribute<VeiculoProposta, String> codigoModeloMolicar;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoMunicipioNomePlacaIbge;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoCorCodExterna;
	public static volatile SingularAttribute<VeiculoProposta, String> cor;
	public static volatile SingularAttribute<VeiculoProposta, String> descricaoModelo;
	public static volatile SingularAttribute<VeiculoProposta, Boolean> taxi;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoModeloVeiculoCod;
	public static volatile SingularAttribute<VeiculoProposta, String> descricaoMarca;
	public static volatile SingularAttribute<VeiculoProposta, Boolean> adaptado;
	public static volatile ListAttribute<VeiculoProposta, OpcionalVeiculo> opcionais;
	public static volatile SingularAttribute<VeiculoProposta, String> codigoModeloFipe;
	public static volatile SingularAttribute<VeiculoProposta, Integer> veiculoKm;
	public static volatile SingularAttribute<VeiculoProposta, String> codigoModeloMarca;
	public static volatile SingularAttribute<VeiculoProposta, String> chassi;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoStatus;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoEstadoNomePlaca;
	public static volatile SingularAttribute<VeiculoProposta, Long> id;
	public static volatile SingularAttribute<VeiculoProposta, String> veiculoEstadoCodPlaca;
	public static volatile SingularAttribute<VeiculoProposta, Boolean> anoInformadoManualmente;
	public static volatile SingularAttribute<VeiculoProposta, String> placa;

}

