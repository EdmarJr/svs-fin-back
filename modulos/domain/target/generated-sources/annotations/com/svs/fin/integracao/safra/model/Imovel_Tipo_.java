package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Imovel_Tipo.class)
public abstract class Imovel_Tipo_ {

	public static volatile SingularAttribute<Imovel_Tipo, Integer> id_imovel_tipo;
	public static volatile SingularAttribute<Imovel_Tipo, String> ds_imovel_tipo;
	public static volatile SingularAttribute<Imovel_Tipo, Integer> id_imovel_especie;

}

