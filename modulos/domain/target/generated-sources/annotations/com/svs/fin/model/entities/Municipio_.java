package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Municipio.class)
public abstract class Municipio_ {

	public static volatile SingularAttribute<Municipio, Estado> estado;
	public static volatile SingularAttribute<Municipio, String> codigoIbge;
	public static volatile SingularAttribute<Municipio, String> nome;
	public static volatile SingularAttribute<Municipio, Long> id;

}

