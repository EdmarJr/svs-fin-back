package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FornecedorClienteSvs.class)
public abstract class FornecedorClienteSvs_ {

	public static volatile SingularAttribute<FornecedorClienteSvs, String> telefone;
	public static volatile SingularAttribute<FornecedorClienteSvs, String> nome;
	public static volatile SingularAttribute<FornecedorClienteSvs, Long> id;
	public static volatile SingularAttribute<FornecedorClienteSvs, ClienteSvs> clienteSvs;

}

