package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Endereco.class)
public abstract class Tipo_Endereco_ {

	public static volatile SingularAttribute<Tipo_Endereco, String> id_tipo_pessoa;
	public static volatile SingularAttribute<Tipo_Endereco, String> ds_tipo_endereco;
	public static volatile SingularAttribute<Tipo_Endereco, Integer> id_tipo_endereco;

}

