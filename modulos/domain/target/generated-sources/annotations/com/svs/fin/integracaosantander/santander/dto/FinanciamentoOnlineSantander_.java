package com.svs.fin.integracaosantander.santander.dto;

import com.svs.fin.model.entities.CalculoRentabilidade;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FinanciamentoOnlineSantander.class)
public abstract class FinanciamentoOnlineSantander_ {

	public static volatile SingularAttribute<FinanciamentoOnlineSantander, JsonRespostaProposta> jsonRespostaProposta;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, CalculoRentabilidade> calculoRentabilidade;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, JsonRespostaPreAnaliseSantander> jsonRespostaPreAnaliseSantander;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, JsonRequisicaoIdentificationSantander> jsonRequisicaoIdentificationSantander;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, JsonRespostaSimulacaoSantander> jsonRespostaSimulacaoSantander;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, JsonRequisicaoProposta> jsonRequisicaoProposta;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, Long> id;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, JsonRequisicaoSimulacaoSantander> jsonRequisicaoSimulacaoSantander;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, String> status;
	public static volatile SingularAttribute<FinanciamentoOnlineSantander, byte[]> pdfCet;

}

