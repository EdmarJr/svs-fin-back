package com.svs.fin.integracaosantander.santander.dto;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonRespostaSimulacaoSantander.class)
public abstract class JsonRespostaSimulacaoSantander_ {

	public static volatile SingularAttribute<JsonRespostaSimulacaoSantander, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonRespostaSimulacaoSantander, Calendar> data;
	public static volatile SingularAttribute<JsonRespostaSimulacaoSantander, Long> id;

}

