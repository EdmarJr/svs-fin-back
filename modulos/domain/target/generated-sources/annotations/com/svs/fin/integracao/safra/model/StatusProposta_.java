package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StatusProposta.class)
public abstract class StatusProposta_ {

	public static volatile SingularAttribute<StatusProposta, String> ds_status;
	public static volatile SingularAttribute<StatusProposta, String> id_status;

}

