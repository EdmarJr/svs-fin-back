package com.svs.fin.integracaoItau;

import com.svs.fin.model.entities.OperacaoFinanciada;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FinanciamentoOnlineItau.class)
public abstract class FinanciamentoOnlineItau_ {

	public static volatile SingularAttribute<FinanciamentoOnlineItau, OperacaoFinanciada> operacaoFinanciada;
	public static volatile SingularAttribute<FinanciamentoOnlineItau, JsonTransactionRequestItau> jsonTransactionRequestItau;
	public static volatile SingularAttribute<FinanciamentoOnlineItau, String> idOnline;
	public static volatile SingularAttribute<FinanciamentoOnlineItau, JsonCreditAnalysisItau> jsonCreditAnalysisItau;
	public static volatile SingularAttribute<FinanciamentoOnlineItau, Long> id;
	public static volatile SingularAttribute<FinanciamentoOnlineItau, JsonSimulationOperationResponseItau> jsonSimulationOperationResponseItau;

}

