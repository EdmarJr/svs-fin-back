package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AcessUserRoleUnidadeOrganizacionalId.class)
public abstract class AcessUserRoleUnidadeOrganizacionalId_ {

	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacionalId, Long> unidadeOrganizacionalId;
	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacionalId, Long> roleId;
	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacionalId, Long> acessUserId;

}

