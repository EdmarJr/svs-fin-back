package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Isencao_Fiscal.class)
public abstract class Isencao_Fiscal_ {

	public static volatile SingularAttribute<Isencao_Fiscal, String> ds_isencao_fiscal;
	public static volatile SingularAttribute<Isencao_Fiscal, Integer> id_isencao_fiscal;

}

