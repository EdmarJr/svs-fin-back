package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Nacionalidade.class)
public abstract class Nacionalidade_ {

	public static volatile SingularAttribute<Nacionalidade, Integer> id_nacionalidade;
	public static volatile SingularAttribute<Nacionalidade, String> ds_nacionalidade;
	public static volatile SingularAttribute<Nacionalidade, String> fl_default;

}

