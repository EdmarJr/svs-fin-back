package com.svs.fin.model.entities;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MotivoAprovacao.class)
public abstract class MotivoAprovacao_ {

	public static volatile SingularAttribute<MotivoAprovacao, Boolean> ativo;
	public static volatile SingularAttribute<MotivoAprovacao, LocalDateTime> dataHoraDeAtualizacao;
	public static volatile SingularAttribute<MotivoAprovacao, Usuario> usuarioResponsavelUltimaAlteracao;
	public static volatile SingularAttribute<MotivoAprovacao, String> titulo;
	public static volatile SingularAttribute<MotivoAprovacao, Long> id;
	public static volatile SingularAttribute<MotivoAprovacao, LocalDateTime> dataHoraDeCriacao;

}

