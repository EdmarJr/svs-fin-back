package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemCBC.class)
public abstract class ItemCBC_ {

	public static volatile SingularAttribute<ItemCBC, OperacaoFinanciada> operacaoFinanciada;
	public static volatile SingularAttribute<ItemCBC, Integer> codigo;
	public static volatile SingularAttribute<ItemCBC, OpcaoItemCbc> opcaoItemCbc;
	public static volatile SingularAttribute<ItemCBC, Boolean> adicionarAoFinanciamento;
	public static volatile SingularAttribute<ItemCBC, Double> valor;
	public static volatile SingularAttribute<ItemCBC, Calendar> dataPrevisao;
	public static volatile SingularAttribute<ItemCBC, Long> id;

}

