package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Telefone.class)
public abstract class Telefone_ {

	public static volatile SingularAttribute<Telefone, Long> codigoDealerWorkflow;
	public static volatile SingularAttribute<Telefone, String> telefone;
	public static volatile SingularAttribute<Telefone, TipoTelefone> tipoTelefone;
	public static volatile SingularAttribute<Telefone, String> ddd;
	public static volatile SingularAttribute<Telefone, Long> id;
	public static volatile SingularAttribute<Telefone, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<Telefone, TipoEndereco> tipoEndereco;

}

