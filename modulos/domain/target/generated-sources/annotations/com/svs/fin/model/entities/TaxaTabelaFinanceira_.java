package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaxaTabelaFinanceira.class)
public abstract class TaxaTabelaFinanceira_ {

	public static volatile SingularAttribute<TaxaTabelaFinanceira, Double> percentualTaxaRetorno;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Integer> carencia;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Double> percentualEntrada;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Double> taxaMes;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Double> percentualRebate;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, TabelaFinanceira> tabelaFinanceira;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Double> coeficiente;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Integer> anoFim;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Integer> parcelas;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Integer> anoInicio;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Double> valorRebate;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Boolean> zeroKm;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, Long> id;
	public static volatile SingularAttribute<TaxaTabelaFinanceira, MascaraTaxaTabelaFinanceira> mascara;

}

