package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Sede_Social.class)
public abstract class Sede_Social_ {

	public static volatile SingularAttribute<Sede_Social, String> ds_sede_social;
	public static volatile SingularAttribute<Sede_Social, Integer> id_sede_social;

}

