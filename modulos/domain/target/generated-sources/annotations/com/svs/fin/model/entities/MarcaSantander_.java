package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumMarca;
import com.svs.fin.enums.EnumTipoVeiculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MarcaSantander.class)
public abstract class MarcaSantander_ {

	public static volatile SingularAttribute<MarcaSantander, EnumTipoVeiculo> tipoVeiculo;
	public static volatile SingularAttribute<MarcaSantander, EnumMarca> marcaMySaga;
	public static volatile SingularAttribute<MarcaSantander, String> integrationCode;
	public static volatile SingularAttribute<MarcaSantander, String> description;
	public static volatile SingularAttribute<MarcaSantander, Long> id;

}

