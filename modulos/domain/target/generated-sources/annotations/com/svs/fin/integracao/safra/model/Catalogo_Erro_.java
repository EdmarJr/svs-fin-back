package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Catalogo_Erro.class)
public abstract class Catalogo_Erro_ {

	public static volatile SingularAttribute<Catalogo_Erro, Integer> id_erro;
	public static volatile SingularAttribute<Catalogo_Erro, String> ds_erro;

}

