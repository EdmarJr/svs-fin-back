package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoRelacionamentoEmpresaSantander.class)
public abstract class TipoRelacionamentoEmpresaSantander_ {

	public static volatile SingularAttribute<TipoRelacionamentoEmpresaSantander, String> description;
	public static volatile SingularAttribute<TipoRelacionamentoEmpresaSantander, Long> id;

}

