package com.svs.fin.integracaosantander.santander.dto;

import com.svs.fin.model.entities.integracaosantander.AnoModeloCombustivelSantander;
import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonRequisicaoIdentificationSantander.class)
public abstract class JsonRequisicaoIdentificationSantander_ {

	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, EnumTipoVeiculo> tipoVeiculo;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, String> cpfCliente;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, Calendar> data;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, Long> id;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, String> nomeCliente;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, AnoModeloCombustivelSantander> anoModeloCombustivelSantander;
	public static volatile SingularAttribute<JsonRequisicaoIdentificationSantander, ModeloVeiculoSantander> modeloVeiculoSantander;

}

