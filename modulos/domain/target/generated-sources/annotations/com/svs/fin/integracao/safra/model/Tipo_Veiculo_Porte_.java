package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Veiculo_Porte.class)
public abstract class Tipo_Veiculo_Porte_ {

	public static volatile SingularAttribute<Tipo_Veiculo_Porte, String> ds_cd_porte;
	public static volatile SingularAttribute<Tipo_Veiculo_Porte, String> ds_veiculo_porte;
	public static volatile SingularAttribute<Tipo_Veiculo_Porte, String> id_veiculo_porte;

}

