package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Imovel_Especie.class)
public abstract class Imovel_Especie_ {

	public static volatile SingularAttribute<Imovel_Especie, String> id_pessoa;
	public static volatile SingularAttribute<Imovel_Especie, Integer> id_imovel_especie;
	public static volatile SingularAttribute<Imovel_Especie, String> ds_imovel_especie;

}

