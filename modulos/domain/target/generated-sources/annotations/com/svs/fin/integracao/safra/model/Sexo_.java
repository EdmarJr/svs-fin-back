package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Sexo.class)
public abstract class Sexo_ {

	public static volatile SingularAttribute<Sexo, String> ds_sexo;
	public static volatile SingularAttribute<Sexo, String> id_sexo;

}

