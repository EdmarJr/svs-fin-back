package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Escolaridade.class)
public abstract class Escolaridade_ {

	public static volatile SingularAttribute<Escolaridade, String> ds_escolaridade;
	public static volatile SingularAttribute<Escolaridade, Integer> id_escolaridade;

}

