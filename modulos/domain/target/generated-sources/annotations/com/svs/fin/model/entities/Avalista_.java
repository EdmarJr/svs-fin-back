package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.enums.EnumSexo;
import com.svs.fin.enums.EnumTipoAvalista;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Avalista.class)
public abstract class Avalista_ {

	public static volatile SingularAttribute<Avalista, EnumTipoAvalista> tipoAvalista;
	public static volatile SingularAttribute<Avalista, String> ufEmissorRg;
	public static volatile SingularAttribute<Avalista, EnumOrgaoEmissorDocumento> orgaoEmissorDocRg;
	public static volatile SingularAttribute<Avalista, String> telefoneCelular;
	public static volatile ListAttribute<Avalista, VeiculoPatrimonio> veiculosPatrimonio;
	public static volatile SingularAttribute<Avalista, Ocupacao> ocupacao;
	public static volatile SingularAttribute<Avalista, String> ufNascimento;
	public static volatile SingularAttribute<Avalista, String> cpf;
	public static volatile SingularAttribute<Avalista, Conjuge> conjuge;
	public static volatile SingularAttribute<Avalista, Long> id;
	public static volatile SingularAttribute<Avalista, EnumEscolaridade> escolaridade;
	public static volatile SingularAttribute<Avalista, Calendar> dataNascimento;
	public static volatile ListAttribute<Avalista, ImovelPatrimonio> imoveisPatrimonio;
	public static volatile SingularAttribute<Avalista, String> email;
	public static volatile SingularAttribute<Avalista, EnumEstados> ufOrgaoEmissorDocRg;
	public static volatile SingularAttribute<Avalista, String> telefoneResidencial;
	public static volatile SingularAttribute<Avalista, Endereco> endereco;
	public static volatile SingularAttribute<Avalista, String> orgaoEmissorRg;
	public static volatile SingularAttribute<Avalista, String> cidadeNascimento;
	public static volatile SingularAttribute<Avalista, Calendar> dataEmissaoRg;
	public static volatile SingularAttribute<Avalista, String> nome;
	public static volatile SingularAttribute<Avalista, EnumEstadoCivil> estadoCivil;
	public static volatile ListAttribute<Avalista, OutraRendaClienteSvs> outrasRendas;
	public static volatile SingularAttribute<Avalista, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<Avalista, String> rg;
	public static volatile SingularAttribute<Avalista, String> nomePai;
	public static volatile SingularAttribute<Avalista, EnumSexo> sexo;
	public static volatile SingularAttribute<Avalista, String> nacionalidade;
	public static volatile SingularAttribute<Avalista, String> nomeMae;

}

