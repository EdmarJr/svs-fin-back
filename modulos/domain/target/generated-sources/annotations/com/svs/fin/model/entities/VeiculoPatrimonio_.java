package com.svs.fin.model.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VeiculoPatrimonio.class)
public abstract class VeiculoPatrimonio_ {

	public static volatile SingularAttribute<VeiculoPatrimonio, String> marca;
	public static volatile SingularAttribute<VeiculoPatrimonio, BigDecimal> valor;
	public static volatile SingularAttribute<VeiculoPatrimonio, Integer> anoFabricacao;
	public static volatile SingularAttribute<VeiculoPatrimonio, Long> id;
	public static volatile SingularAttribute<VeiculoPatrimonio, Integer> anoModelo;
	public static volatile SingularAttribute<VeiculoPatrimonio, ClienteSvs> clienteSvs;
	public static volatile SingularAttribute<VeiculoPatrimonio, String> modelo;
	public static volatile SingularAttribute<VeiculoPatrimonio, Avalista> avalista;
	public static volatile SingularAttribute<VeiculoPatrimonio, String> placa;

}

