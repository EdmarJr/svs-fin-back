package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FaturamentoClienteSvs.class)
public abstract class FaturamentoClienteSvs_ {

	public static volatile SingularAttribute<FaturamentoClienteSvs, Calendar> dataReferencia;
	public static volatile SingularAttribute<FaturamentoClienteSvs, Double> valor;
	public static volatile SingularAttribute<FaturamentoClienteSvs, Long> id;
	public static volatile SingularAttribute<FaturamentoClienteSvs, ClienteSvs> clienteSvs;

}

