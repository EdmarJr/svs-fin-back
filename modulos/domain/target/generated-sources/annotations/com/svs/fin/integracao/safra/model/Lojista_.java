package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Lojista.class)
public abstract class Lojista_ {

	public static volatile SingularAttribute<Lojista, String> ds_cnpj;
	public static volatile SingularAttribute<Lojista, String> ds_abreviatura;
	public static volatile SingularAttribute<Lojista, String> ds_nome;
	public static volatile SingularAttribute<Lojista, Double> vl_tc_cdc;
	public static volatile SingularAttribute<Lojista, Double> vl_tc_leasing;
	public static volatile SingularAttribute<Lojista, String> id_codigo;

}

