package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Registro_Gravame.class)
public abstract class Registro_Gravame_ {

	public static volatile SingularAttribute<Registro_Gravame, String> id_uf;
	public static volatile SingularAttribute<Registro_Gravame, Double> vlr_registro;
	public static volatile SingularAttribute<Registro_Gravame, Double> vl_reg_ncobrado;

}

