package com.svs.fin.integracaosantander.santander;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoDocumentoSantander.class)
public abstract class TipoDocumentoSantander_ {

	public static volatile SingularAttribute<TipoDocumentoSantander, String> description;
	public static volatile SingularAttribute<TipoDocumentoSantander, Long> id;

}

