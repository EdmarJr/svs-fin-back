package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Atividade.class)
public abstract class Atividade_ {

	public static volatile SingularAttribute<Atividade, Integer> id_atividade;
	public static volatile SingularAttribute<Atividade, Integer> id_ramo_atividade;
	public static volatile SingularAttribute<Atividade, String> ds_atividade;

}

