package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MotivoAprovacaoSvs.class)
public abstract class MotivoAprovacaoSvs_ {

	public static volatile SingularAttribute<MotivoAprovacaoSvs, Calendar> alteradoEm;
	public static volatile SingularAttribute<MotivoAprovacaoSvs, Boolean> ativo;
	public static volatile SingularAttribute<MotivoAprovacaoSvs, Calendar> criadoEm;
	public static volatile SingularAttribute<MotivoAprovacaoSvs, String> titulo;
	public static volatile SingularAttribute<MotivoAprovacaoSvs, Long> id;
	public static volatile SingularAttribute<MotivoAprovacaoSvs, AcessUser> usuarioCriacaoAlteracao;

}

