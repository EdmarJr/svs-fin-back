package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EstadoCivilSantander.class)
public abstract class EstadoCivilSantander_ {

	public static volatile SingularAttribute<EstadoCivilSantander, String> description;
	public static volatile SingularAttribute<EstadoCivilSantander, Long> id;

}

