package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoOcupacao;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ocupacao.class)
public abstract class Ocupacao_ {

	public static volatile SingularAttribute<Ocupacao, String> telefone;
	public static volatile SingularAttribute<Ocupacao, Calendar> socioDesde;
	public static volatile SingularAttribute<Ocupacao, Endereco> endereco;
	public static volatile SingularAttribute<Ocupacao, String> numeroBeneficioAposentadoria;
	public static volatile SingularAttribute<Ocupacao, ProfissaoSantander> profissao;
	public static volatile SingularAttribute<Ocupacao, String> cnpj;
	public static volatile SingularAttribute<Ocupacao, EnumTipoOcupacao> tipoOcupacao;
	public static volatile SingularAttribute<Ocupacao, String> nomeEmpregador;
	public static volatile SingularAttribute<Ocupacao, Double> renda;
	public static volatile SingularAttribute<Ocupacao, AtividadeEconomicaSantander> atividade;
	public static volatile SingularAttribute<Ocupacao, String> telefoneContador;
	public static volatile SingularAttribute<Ocupacao, String> ocupacao;
	public static volatile SingularAttribute<Ocupacao, String> contador;
	public static volatile SingularAttribute<Ocupacao, BigDecimal> percParticipacao;
	public static volatile SingularAttribute<Ocupacao, Calendar> dataInicialCargo;
	public static volatile SingularAttribute<Ocupacao, TipoAtividadeEconomicaSantander> tipoAtividade;
	public static volatile SingularAttribute<Ocupacao, String> nomeContador;
	public static volatile SingularAttribute<Ocupacao, Long> id;
	public static volatile SingularAttribute<Ocupacao, String> cargo;
	public static volatile SingularAttribute<Ocupacao, BigDecimal> capitalSocial;

}

