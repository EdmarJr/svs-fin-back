package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoAtividadeEconomicaSantander.class)
public abstract class TipoAtividadeEconomicaSantander_ {

	public static volatile SingularAttribute<TipoAtividadeEconomicaSantander, String> description;
	public static volatile SingularAttribute<TipoAtividadeEconomicaSantander, Long> id;

}

