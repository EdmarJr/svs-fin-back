package com.svs.fin.integracaoItau;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonSimulationOperationResponseItau.class)
public abstract class JsonSimulationOperationResponseItau_ {

	public static volatile SingularAttribute<JsonSimulationOperationResponseItau, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonSimulationOperationResponseItau, Calendar> data;
	public static volatile SingularAttribute<JsonSimulationOperationResponseItau, Long> id;

}

