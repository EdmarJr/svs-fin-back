package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CnpjInstituicaoFinanceira.class)
public abstract class CnpjInstituicaoFinanceira_ {

	public static volatile SingularAttribute<CnpjInstituicaoFinanceira, Boolean> ativo;
	public static volatile SingularAttribute<CnpjInstituicaoFinanceira, InstituicaoFinanceira> instituicaoFinanceira;
	public static volatile ListAttribute<CnpjInstituicaoFinanceira, ProdutoFinanceiro> produtosFinanceiros;
	public static volatile SingularAttribute<CnpjInstituicaoFinanceira, Long> id;
	public static volatile SingularAttribute<CnpjInstituicaoFinanceira, String> cnpj;
	public static volatile SingularAttribute<CnpjInstituicaoFinanceira, String> razaoSocial;

}

