package com.svs.fin.model.entities.integracaosantander;

import com.svs.fin.enums.EnumDepartamento;
import com.svs.fin.model.entities.MarcaSantander;
import com.svs.fin.model.entities.UnidadeOrganizacional;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModeloVeiculoSantander.class)
public abstract class ModeloVeiculoSantander_ {

	public static volatile SingularAttribute<ModeloVeiculoSantander, MarcaSantander> marca;
	public static volatile SingularAttribute<ModeloVeiculoSantander, Boolean> ativo;
	public static volatile SingularAttribute<ModeloVeiculoSantander, String> integrationCode;
	public static volatile ListAttribute<ModeloVeiculoSantander, UnidadeOrganizacional> unidades;
	public static volatile ListAttribute<ModeloVeiculoSantander, EnumDepartamento> departamentos;
	public static volatile SingularAttribute<ModeloVeiculoSantander, String> description;
	public static volatile SingularAttribute<ModeloVeiculoSantander, Long> id;
	public static volatile ListAttribute<ModeloVeiculoSantander, AnoModeloCombustivelSantander> anosModelo;

}

