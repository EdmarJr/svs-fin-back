package com.svs.fin.model.entities;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstituicaoFinanceira.class)
public abstract class InstituicaoFinanceira_ {

	public static volatile SingularAttribute<InstituicaoFinanceira, Long> codigoDealerWorkflow;
	public static volatile SingularAttribute<InstituicaoFinanceira, String> codigo;
	public static volatile SingularAttribute<InstituicaoFinanceira, Boolean> ativo;
	public static volatile SingularAttribute<InstituicaoFinanceira, LocalDateTime> dataHoraDeAtualizacao;
	public static volatile SingularAttribute<InstituicaoFinanceira, Usuario> usuarioResponsavelUltimaAlteracao;
	public static volatile SingularAttribute<InstituicaoFinanceira, String> nome;
	public static volatile SingularAttribute<InstituicaoFinanceira, Long> id;
	public static volatile SingularAttribute<InstituicaoFinanceira, LocalDateTime> dataHoraDeCriacao;
	public static volatile SingularAttribute<InstituicaoFinanceira, String> razaoSocial;
	public static volatile ListAttribute<InstituicaoFinanceira, CnpjInstituicaoFinanceira> cnpjsAssociados;

}

