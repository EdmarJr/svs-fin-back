package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumFaseOperacaoFinanciada;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AndamentoOperacao.class)
public abstract class AndamentoOperacao_ {

	public static volatile SingularAttribute<AndamentoOperacao, OperacaoFinanciada> operacaoFinanciada;
	public static volatile SingularAttribute<AndamentoOperacao, EnumFaseOperacaoFinanciada> fase;
	public static volatile SingularAttribute<AndamentoOperacao, AcessUser> usuario;
	public static volatile SingularAttribute<AndamentoOperacao, Long> id;
	public static volatile SingularAttribute<AndamentoOperacao, String> comentario;
	public static volatile SingularAttribute<AndamentoOperacao, LocalDateTime> dataHoraDeCriacao;
	public static volatile SingularAttribute<AndamentoOperacao, EnumFaseOperacaoFinanciada> proximaFase;
	public static volatile SingularAttribute<AndamentoOperacao, MotivoAprovacaoSvs> motivoAprovacao;

}

