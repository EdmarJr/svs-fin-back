package com.svs.fin.modelo.mapper;

import com.svs.fin.model.dto.EntidadeTesteDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import teste.EntidadeTeste;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
public class EntidadeTesteMapperImpl implements EntidadeTesteMapper {

    @Override
    public EntidadeTesteDto paraDto(EntidadeTeste source) {
        if ( source == null ) {
            return null;
        }

        EntidadeTesteDto entidadeTesteDto = new EntidadeTesteDto();

        entidadeTesteDto.setId( source.getId() );
        entidadeTesteDto.setDescricao( source.getDescricao() );

        return entidadeTesteDto;
    }

    @Override
    public EntidadeTeste doDto(EntidadeTesteDto source) {
        if ( source == null ) {
            return null;
        }

        EntidadeTeste entidadeTeste = new EntidadeTeste();

        entidadeTeste.setId( source.getId() );
        entidadeTeste.setDescricao( source.getDescricao() );

        return entidadeTeste;
    }

    @Override
    public List<EntidadeTesteDto> paraDto(List<EntidadeTeste> source) {
        if ( source == null ) {
            return null;
        }

        List<EntidadeTesteDto> list = new ArrayList<EntidadeTesteDto>( source.size() );
        for ( EntidadeTeste entidadeTeste : source ) {
            list.add( paraDto( entidadeTeste ) );
        }

        return list;
    }

    @Override
    public void atualizar(EntidadeTesteDto source, EntidadeTeste target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setDescricao( source.getDescricao() );
    }
}
