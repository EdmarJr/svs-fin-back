package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoEndereco.class)
public abstract class TipoEndereco_ {

	public static volatile SingularAttribute<TipoEndereco, Long> codigoSISDIA;
	public static volatile SingularAttribute<TipoEndereco, Boolean> ativo;
	public static volatile SingularAttribute<TipoEndereco, Long> codigoDealerWorklow;
	public static volatile SingularAttribute<TipoEndereco, Long> id;
	public static volatile SingularAttribute<TipoEndereco, String> descricao;

}

