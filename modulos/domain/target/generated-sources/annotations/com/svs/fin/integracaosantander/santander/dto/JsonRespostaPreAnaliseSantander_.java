package com.svs.fin.integracaosantander.santander.dto;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JsonRespostaPreAnaliseSantander.class)
public abstract class JsonRespostaPreAnaliseSantander_ {

	public static volatile SingularAttribute<JsonRespostaPreAnaliseSantander, byte[]> jsonByte;
	public static volatile SingularAttribute<JsonRespostaPreAnaliseSantander, String> cpfCliente;
	public static volatile SingularAttribute<JsonRespostaPreAnaliseSantander, Calendar> data;
	public static volatile SingularAttribute<JsonRespostaPreAnaliseSantander, Long> id;
	public static volatile SingularAttribute<JsonRespostaPreAnaliseSantander, String> nomeCliente;

}

