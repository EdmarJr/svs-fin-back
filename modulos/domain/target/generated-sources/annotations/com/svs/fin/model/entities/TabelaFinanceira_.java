package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoTabelaFinanceira;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TabelaFinanceira.class)
public abstract class TabelaFinanceira_ {

	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorCartorio;
	public static volatile SingularAttribute<TabelaFinanceira, EnumTipoTabelaFinanceira> tipo;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorTacCobrada;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorPercentualPlus;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorBonus;
	public static volatile SingularAttribute<TabelaFinanceira, Boolean> ativo;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorTacDevolvida;
	public static volatile SingularAttribute<TabelaFinanceira, InstituicaoFinanceira> instituicaoFinanceira;
	public static volatile SingularAttribute<TabelaFinanceira, String> nome;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorPlus;
	public static volatile ListAttribute<TabelaFinanceira, TaxaTabelaFinanceira> taxas;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorOutros;
	public static volatile SingularAttribute<TabelaFinanceira, ProdutoFinanceiro> produtoFinanceiro;
	public static volatile ListAttribute<TabelaFinanceira, UnidadeOrganizacional> unidades;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorVistoria;
	public static volatile SingularAttribute<TabelaFinanceira, Long> id;
	public static volatile SingularAttribute<TabelaFinanceira, Calendar> dataInicial;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorGravame;
	public static volatile SingularAttribute<TabelaFinanceira, Calendar> dataFinal;
	public static volatile SingularAttribute<TabelaFinanceira, BigDecimal> valorPercentualBonus;

}

