package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoTabelaFinanceira.class)
public abstract class TipoTabelaFinanceira_ {

	public static volatile SingularAttribute<TipoTabelaFinanceira, Boolean> ativo;
	public static volatile SingularAttribute<TipoTabelaFinanceira, String> nome;
	public static volatile SingularAttribute<TipoTabelaFinanceira, Long> id;

}

