package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumTipoRecebimento;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoPagamento.class)
public abstract class TipoPagamento_ {

	public static volatile SingularAttribute<TipoPagamento, Long> codigoDealerWorkflow;
	public static volatile SingularAttribute<TipoPagamento, EnumTipoRecebimento> tipo;
	public static volatile SingularAttribute<TipoPagamento, Boolean> ativo;
	public static volatile SingularAttribute<TipoPagamento, Long> id;
	public static volatile SingularAttribute<TipoPagamento, String> descricao;

}

