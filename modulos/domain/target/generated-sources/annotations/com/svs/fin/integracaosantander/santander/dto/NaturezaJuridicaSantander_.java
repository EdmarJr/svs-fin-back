package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NaturezaJuridicaSantander.class)
public abstract class NaturezaJuridicaSantander_ {

	public static volatile SingularAttribute<NaturezaJuridicaSantander, String> description;
	public static volatile SingularAttribute<NaturezaJuridicaSantander, Long> id;

}

