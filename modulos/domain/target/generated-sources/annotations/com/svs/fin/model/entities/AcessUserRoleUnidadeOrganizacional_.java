package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AcessUserRoleUnidadeOrganizacional.class)
public abstract class AcessUserRoleUnidadeOrganizacional_ {

	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacional, UnidadeOrganizacional> unidadeOrganizacional;
	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacional, Role> role;
	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacional, AcessUserRoleUnidadeOrganizacionalId> id;
	public static volatile SingularAttribute<AcessUserRoleUnidadeOrganizacional, AcessUser> acessUser;

}

