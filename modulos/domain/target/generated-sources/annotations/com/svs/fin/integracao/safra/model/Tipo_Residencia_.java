package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Residencia.class)
public abstract class Tipo_Residencia_ {

	public static volatile SingularAttribute<Tipo_Residencia, Integer> id_tipo_residencia;
	public static volatile SingularAttribute<Tipo_Residencia, String> fl_residencia;
	public static volatile SingularAttribute<Tipo_Residencia, String> ds_tipo_residencia;

}

