package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BancoSantander.class)
public abstract class BancoSantander_ {

	public static volatile SingularAttribute<BancoSantander, String> description;
	public static volatile SingularAttribute<BancoSantander, Long> id;

}

