package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.enums.EnumSexo;
import com.svs.fin.enums.EnumTipoPessoa;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ClienteSvs.class)
public abstract class ClienteSvs_ {

	public static volatile SingularAttribute<ClienteSvs, EnumTipoPessoa> tipoPessoa;
	public static volatile ListAttribute<ClienteSvs, ReferenciaPessoal> referenciasPessoais;
	public static volatile SingularAttribute<ClienteSvs, EnumOrgaoEmissorDocumento> orgaoEmissorDocRg;
	public static volatile SingularAttribute<ClienteSvs, String> cnpj;
	public static volatile SingularAttribute<ClienteSvs, String> nomeFantasia;
	public static volatile ListAttribute<ClienteSvs, VeiculoPatrimonio> veiculosPatrimonio;
	public static volatile SingularAttribute<ClienteSvs, Ocupacao> ocupacao;
	public static volatile SingularAttribute<ClienteSvs, Boolean> menorIncapaz;
	public static volatile SingularAttribute<ClienteSvs, MunicipioSantander> municipioNascimento;
	public static volatile SingularAttribute<ClienteSvs, String> cpf;
	public static volatile SingularAttribute<ClienteSvs, Conjuge> conjuge;
	public static volatile SingularAttribute<ClienteSvs, Long> id;
	public static volatile SingularAttribute<ClienteSvs, EnumEscolaridade> escolaridade;
	public static volatile SingularAttribute<ClienteSvs, Date> dataNascimento;
	public static volatile SingularAttribute<ClienteSvs, ReferenciaBancaria> referenciaBancaria2;
	public static volatile SingularAttribute<ClienteSvs, ReferenciaBancaria> referenciaBancaria1;
	public static volatile ListAttribute<ClienteSvs, ImovelPatrimonio> imoveisPatrimonio;
	public static volatile SingularAttribute<ClienteSvs, EnumEstados> ufOrgaoEmissorDocRg;
	public static volatile SingularAttribute<ClienteSvs, String> email;
	public static volatile ListAttribute<ClienteSvs, Avalista> avalistas;
	public static volatile SingularAttribute<ClienteSvs, Endereco> endereco;
	public static volatile SingularAttribute<ClienteSvs, String> orgaoEmissorRg;
	public static volatile ListAttribute<ClienteSvs, FornecedorClienteSvs> fornecedores;
	public static volatile SingularAttribute<ClienteSvs, Calendar> dataEmissaoRg;
	public static volatile SingularAttribute<ClienteSvs, String> nome;
	public static volatile SingularAttribute<ClienteSvs, EnumEstadoCivil> estadoCivil;
	public static volatile ListAttribute<ClienteSvs, OutraRendaClienteSvs> outrasRendas;
	public static volatile ListAttribute<ClienteSvs, Telefone> telefones;
	public static volatile ListAttribute<ClienteSvs, FaturamentoClienteSvs> faturamentos;
	public static volatile SingularAttribute<ClienteSvs, String> codigoDealerWorkflow;
	public static volatile SingularAttribute<ClienteSvs, String> telefoneContador;
	public static volatile SingularAttribute<ClienteSvs, Boolean> possuiCnh;
	public static volatile SingularAttribute<ClienteSvs, String> rg;
	public static volatile ListAttribute<ClienteSvs, Endereco> enderecos;
	public static volatile SingularAttribute<ClienteSvs, String> ufOrgaoEmissorRg;
	public static volatile SingularAttribute<ClienteSvs, String> nomePai;
	public static volatile SingularAttribute<ClienteSvs, String> nomeContador;
	public static volatile SingularAttribute<ClienteSvs, EstadoSantander> estadoNascimento;
	public static volatile SingularAttribute<ClienteSvs, String> cpfCnpj;
	public static volatile SingularAttribute<ClienteSvs, EnumSexo> sexo;
	public static volatile SingularAttribute<ClienteSvs, NacionalidadeSantander> nacionalidade;
	public static volatile SingularAttribute<ClienteSvs, String> nomeMae;

}

