package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FormaPagamentoSantander.class)
public abstract class FormaPagamentoSantander_ {

	public static volatile SingularAttribute<FormaPagamentoSantander, String> codigoProduto;
	public static volatile SingularAttribute<FormaPagamentoSantander, String> description;
	public static volatile SingularAttribute<FormaPagamentoSantander, Long> id;

}

