package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Fluxo.class)
public abstract class Tipo_Fluxo_ {

	public static volatile SingularAttribute<Tipo_Fluxo, Integer> id_tipo_fluxo;
	public static volatile SingularAttribute<Tipo_Fluxo, String> ds_tipo_fluxo;

}

