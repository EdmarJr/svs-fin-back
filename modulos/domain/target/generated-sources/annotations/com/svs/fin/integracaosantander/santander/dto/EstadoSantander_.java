package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EstadoSantander.class)
public abstract class EstadoSantander_ {

	public static volatile SingularAttribute<EstadoSantander, String> description;
	public static volatile SingularAttribute<EstadoSantander, String> id;

}

