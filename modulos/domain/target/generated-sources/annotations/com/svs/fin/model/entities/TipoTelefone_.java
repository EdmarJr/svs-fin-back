package com.svs.fin.model.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoTelefone.class)
public abstract class TipoTelefone_ {

	public static volatile SingularAttribute<TipoTelefone, Long> codigoSISDIA;
	public static volatile SingularAttribute<TipoTelefone, Boolean> ativo;
	public static volatile SingularAttribute<TipoTelefone, Long> codigoDealerWorklow;
	public static volatile SingularAttribute<TipoTelefone, Long> id;
	public static volatile SingularAttribute<TipoTelefone, String> descricao;

}

