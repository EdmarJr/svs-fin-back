package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Afinidade.class)
public abstract class Afinidade_ {

	public static volatile SingularAttribute<Afinidade, Integer> id_afinidade;
	public static volatile SingularAttribute<Afinidade, String> ds_afinidade;

}

