package com.svs.fin.model.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaxaPerfilInstituicaoFinanceira.class)
public abstract class TaxaPerfilInstituicaoFinanceira_ {

	public static volatile SingularAttribute<TaxaPerfilInstituicaoFinanceira, BigDecimal> taxaFinal;
	public static volatile SingularAttribute<TaxaPerfilInstituicaoFinanceira, BigDecimal> taxaInicial;
	public static volatile SingularAttribute<TaxaPerfilInstituicaoFinanceira, InstituicaoFinanceira> instituicaoFinanceira;
	public static volatile SingularAttribute<TaxaPerfilInstituicaoFinanceira, Long> id;
	public static volatile SingularAttribute<TaxaPerfilInstituicaoFinanceira, Perfil> perfil;

}

