package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AprovacaoBancaria.class)
public abstract class AprovacaoBancaria_ {

	public static volatile ListAttribute<AprovacaoBancaria, MotivoAprovacaoSvs> motivosAprovacao;
	public static volatile SingularAttribute<AprovacaoBancaria, Calendar> data;
	public static volatile SingularAttribute<AprovacaoBancaria, Boolean> aprovado;
	public static volatile SingularAttribute<AprovacaoBancaria, CalculoRentabilidade> calculoRentabilidade;
	public static volatile SingularAttribute<AprovacaoBancaria, AcessUser> usuario;
	public static volatile SingularAttribute<AprovacaoBancaria, Long> id;
	public static volatile SingularAttribute<AprovacaoBancaria, String> comentario;

}

