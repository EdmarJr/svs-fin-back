package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UnidadeOrganizacional.class)
public abstract class UnidadeOrganizacional_ {

	public static volatile SingularAttribute<UnidadeOrganizacional, Boolean> ativo;
	public static volatile SingularAttribute<UnidadeOrganizacional, String> nomeFantasia;
	public static volatile SingularAttribute<UnidadeOrganizacional, Boolean> integra;
	public static volatile SingularAttribute<UnidadeOrganizacional, Calendar> dataInicioIntegracao;
	public static volatile SingularAttribute<UnidadeOrganizacional, String> emailIntegracao;
	public static volatile SingularAttribute<UnidadeOrganizacional, String> sistemaParaIntegracao;
	public static volatile SingularAttribute<UnidadeOrganizacional, String> idEmpExterno;
	public static volatile SingularAttribute<UnidadeOrganizacional, Long> id;
	public static volatile SingularAttribute<UnidadeOrganizacional, String> cnpj;

}

