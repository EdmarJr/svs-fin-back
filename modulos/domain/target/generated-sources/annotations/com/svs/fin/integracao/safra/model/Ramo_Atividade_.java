package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ramo_Atividade.class)
public abstract class Ramo_Atividade_ {

	public static volatile SingularAttribute<Ramo_Atividade, String> ds_ramo_atividade;
	public static volatile SingularAttribute<Ramo_Atividade, Integer> id_ramo_atividade;

}

