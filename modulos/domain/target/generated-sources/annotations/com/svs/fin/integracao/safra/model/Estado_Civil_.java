package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Estado_Civil.class)
public abstract class Estado_Civil_ {

	public static volatile SingularAttribute<Estado_Civil, String> ds_estado_civil;
	public static volatile SingularAttribute<Estado_Civil, Integer> id_estado_civil;
	public static volatile SingularAttribute<Estado_Civil, String> fl_casado;

}

