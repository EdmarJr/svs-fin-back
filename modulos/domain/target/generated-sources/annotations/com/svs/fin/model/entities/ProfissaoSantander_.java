package com.svs.fin.model.entities;

import br.com.gruposaga.sdk.zflow.model.gen.enums.ProfessionEnum;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProfissaoSantander.class)
public abstract class ProfissaoSantander_ {

	public static volatile SingularAttribute<ProfissaoSantander, ProfessionEnum> professionEquivalenteZFlow;
	public static volatile SingularAttribute<ProfissaoSantander, String> description;
	public static volatile SingularAttribute<ProfissaoSantander, Long> id;

}

