package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumBancoIntegracaoOnline;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MascaraTaxaTabelaFinanceira.class)
public abstract class MascaraTaxaTabelaFinanceira_ {

	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, Boolean> ativo;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, LocalDateTime> dataHoraDeAtualizacao;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, EnumBancoIntegracaoOnline> bancoIntegracaoOnline;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, Double> percentualRetorno;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, Integer> ordem;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, Usuario> usuarioResponsavelUltimaAlteracao;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, Long> id;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, String> mascara;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, String> codigoRetorno;
	public static volatile SingularAttribute<MascaraTaxaTabelaFinanceira, LocalDateTime> dataHoraDeCriacao;

}

