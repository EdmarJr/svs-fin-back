package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumEscolaridade;
import com.svs.fin.enums.EnumEstadoCivil;
import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.enums.EnumSexo;
import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Conjuge.class)
public abstract class Conjuge_ {

	public static volatile SingularAttribute<Conjuge, String> telefoneResidencial;
	public static volatile SingularAttribute<Conjuge, String> orgaoEmissorRg;
	public static volatile SingularAttribute<Conjuge, String> cidadeNascimento;
	public static volatile SingularAttribute<Conjuge, String> ufEmissorRg;
	public static volatile SingularAttribute<Conjuge, String> nome;
	public static volatile SingularAttribute<Conjuge, EnumEstadoCivil> estadoCivil;
	public static volatile SingularAttribute<Conjuge, EnumOrgaoEmissorDocumento> orgaoEmissorDocRg;
	public static volatile SingularAttribute<Conjuge, String> telefoneCelular;
	public static volatile SingularAttribute<Conjuge, String> rg;
	public static volatile SingularAttribute<Conjuge, Ocupacao> ocupacao;
	public static volatile SingularAttribute<Conjuge, String> ufNascimento;
	public static volatile SingularAttribute<Conjuge, String> cpf;
	public static volatile SingularAttribute<Conjuge, String> nomePai;
	public static volatile SingularAttribute<Conjuge, Long> id;
	public static volatile SingularAttribute<Conjuge, EnumEscolaridade> escolaridade;
	public static volatile SingularAttribute<Conjuge, EnumSexo> sexo;
	public static volatile SingularAttribute<Conjuge, Calendar> dataNascimento;
	public static volatile SingularAttribute<Conjuge, String> nacionalidade;
	public static volatile SingularAttribute<Conjuge, String> email;
	public static volatile SingularAttribute<Conjuge, EnumEstados> ufOrgaoEmissorDocRg;
	public static volatile SingularAttribute<Conjuge, String> nomeMae;

}

