package com.svs.fin.integracao.safra.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Seguro.class)
public abstract class Tipo_Seguro_ {

	public static volatile SingularAttribute<Tipo_Seguro, String> ds_tipo_seguro;
	public static volatile SingularAttribute<Tipo_Seguro, Integer> id_tipo_seguro;
	public static volatile SingularAttribute<Tipo_Seguro, BigDecimal> vl_percentual;
	public static volatile SingularAttribute<Tipo_Seguro, String> ds_empresa;

}

