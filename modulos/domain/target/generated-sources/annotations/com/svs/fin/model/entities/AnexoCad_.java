package com.svs.fin.model.entities;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AnexoCad.class)
public abstract class AnexoCad_ {

	public static volatile SingularAttribute<AnexoCad, String> extensao;
	public static volatile SingularAttribute<AnexoCad, OperacaoFinanciada> operacaoFinanciada;
	public static volatile SingularAttribute<AnexoCad, String> tipoDocumento;
	public static volatile SingularAttribute<AnexoCad, String> randomId;
	public static volatile SingularAttribute<AnexoCad, Integer> projeto;
	public static volatile SingularAttribute<AnexoCad, byte[]> anexo;
	public static volatile SingularAttribute<AnexoCad, CalculoRentabilidade> calculoRentabilidade;
	public static volatile SingularAttribute<AnexoCad, String> nomeArquivo;
	public static volatile SingularAttribute<AnexoCad, Calendar> dataUpload;
	public static volatile SingularAttribute<AnexoCad, AcessUser> usuario;
	public static volatile SingularAttribute<AnexoCad, Long> id;

}

