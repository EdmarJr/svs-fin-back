package com.svs.fin.integracaosantander.santander.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MunicipioSantander.class)
public abstract class MunicipioSantander_ {

	public static volatile SingularAttribute<MunicipioSantander, EstadoSantander> estado;
	public static volatile SingularAttribute<MunicipioSantander, String> description;
	public static volatile SingularAttribute<MunicipioSantander, Long> id;

}

