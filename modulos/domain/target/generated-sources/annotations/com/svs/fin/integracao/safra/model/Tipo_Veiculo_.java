package com.svs.fin.integracao.safra.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo_Veiculo.class)
public abstract class Tipo_Veiculo_ {

	public static volatile SingularAttribute<Tipo_Veiculo, String> id_tipo_veiculo;
	public static volatile SingularAttribute<Tipo_Veiculo, String> ds_tipo_veiculo;

}

