package com.svs.fin.model.entities;

import com.svs.fin.enums.EnumBancoIntegracaoOnline;
import com.svs.fin.enums.EnumStatusAprovacaoBancaria;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalculoRentabilidade.class)
public abstract class CalculoRentabilidade_ {

	public static volatile SingularAttribute<CalculoRentabilidade, OperacaoFinanciada> operacaoFinanciada;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorCartorio;
	public static volatile SingularAttribute<CalculoRentabilidade, InstituicaoFinanceira> instituicaoFinanceira;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorRetorno;
	public static volatile ListAttribute<CalculoRentabilidade, AnexoCad> anexos;
	public static volatile SingularAttribute<CalculoRentabilidade, Integer> parcelas;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorPmt;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorTac;
	public static volatile SingularAttribute<CalculoRentabilidade, EnumBancoIntegracaoOnline> bancoIntegracaoOnline;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorOutros;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> percPlus;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorVistoria;
	public static volatile SingularAttribute<CalculoRentabilidade, Long> id;
	public static volatile SingularAttribute<CalculoRentabilidade, Boolean> coeficienteAlterado;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorGravame;
	public static volatile SingularAttribute<CalculoRentabilidade, Boolean> enviadoAoBanco;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorEntrada;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> percEntrada;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valor;
	public static volatile SingularAttribute<CalculoRentabilidade, TabelaFinanceira> tabelaFinanceira;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorPlus;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> coeficiente;
	public static volatile SingularAttribute<CalculoRentabilidade, MascaraTaxaTabelaFinanceira> mascaraTaxa;
	public static volatile SingularAttribute<CalculoRentabilidade, String> idFinanciamentoOnline;
	public static volatile SingularAttribute<CalculoRentabilidade, Boolean> aprovacaoOnline;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> percRetorno;
	public static volatile SingularAttribute<CalculoRentabilidade, Double> valorFinanciado;
	public static volatile SingularAttribute<CalculoRentabilidade, EnumStatusAprovacaoBancaria> status;

}

