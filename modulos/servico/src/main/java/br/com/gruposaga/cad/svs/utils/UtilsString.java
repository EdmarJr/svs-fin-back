package br.com.gruposaga.cad.svs.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UtilsString {
	public static String toPrimeiraLetraMaiscula(String texto) {
		return texto.length() > 0 ? texto.substring(0, 1).toUpperCase() + texto.substring(1, texto.length()) : texto;
	}

	public static String toPrimeiraLetraMinuscula(String texto) {
		return texto.length() > 0 ? texto.substring(0, 1).toLowerCase() + texto.substring(1, texto.length()) : texto;
	}

	public static String toSemPrefixo(String texto, String prefixo) {
		return texto.length() > 0 && texto.indexOf(prefixo) == 0 ? texto.substring(prefixo.length(), texto.length())
				: texto;
	}

	public static String toComPrefixo(String texto, String prefixo) {
		return prefixo + texto;
	}

	public static String toAngularNameFormat(String texto) {
		String[] split = texto.split("(?<=[a-z])(?=[A-Z])");
		List<String> arraySoComMinusculas = new ArrayList<String>();
		Arrays.asList(split)
				.forEach((textoIterate) -> arraySoComMinusculas.add(toPrimeiraLetraMinuscula(textoIterate)));
		String retorno = String.join("-", arraySoComMinusculas);
		return retorno;
	}
}
