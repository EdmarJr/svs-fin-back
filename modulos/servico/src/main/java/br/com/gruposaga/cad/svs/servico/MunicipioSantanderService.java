package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.MunicipioSantander;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
public class MunicipioSantanderService extends Service<MunicipioSantander> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<MunicipioSantander> getClassOfT() {
		return MunicipioSantander.class;
	}

	public List<MunicipioSantander> obterPorSiglaEstado(String siglaEstado) {
		List<MunicipioSantander> retorno = crudService
				.findWithNamedQuery(MunicipioSantander.NQ_OBTER_POR_SIGLA_ESTADO.NAME,
						QueryParameter
								.with(MunicipioSantander.NQ_OBTER_POR_SIGLA_ESTADO.NM_PM_SIGLA_ESTADO, siglaEstado)
								.parameters());

		return retorno;
	}

}
