package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.Avalista;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AvalistaService extends Service<Avalista> {

	@Inject
	private CrudService crudService;

	@Inject
	private EnderecoService enderecoService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	public void beforeSave(Avalista avalista) {
		super.beforeSave(avalista);
		vincularOutrasRendas(avalista);
		vincularVeiculosPatrimonio(avalista);
		vincularImoveisPatrimonio(avalista);
		enderecoService.beforeSave(avalista.getEndereco());
	}

	public void vincularOutrasRendas(Avalista avalista) {
		avalista.getOutrasRendas().forEach(outraRenda -> outraRenda.setAvalista(avalista));
	}

	public void vincularVeiculosPatrimonio(Avalista avalista) {
		avalista.getVeiculosPatrimonio().forEach(veiculoPatrimonio -> veiculoPatrimonio.setAvalista(avalista));
	}

	public void vincularImoveisPatrimonio(Avalista avalista) {
		avalista.getImoveisPatrimonio().forEach(imovelPatrimonio -> imovelPatrimonio.setAvalista(avalista));
	}

	@Override
	protected Class<Avalista> getClassOfT() {
		return Avalista.class;
	}

	public List<Avalista> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(Avalista.NQ_OBTER_POR_CLIENTE_SVS.NAME,
				QueryParameter.with(Avalista.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
