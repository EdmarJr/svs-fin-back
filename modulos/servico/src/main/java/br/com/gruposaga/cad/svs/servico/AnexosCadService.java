package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.AnexoCad;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AnexosCadService extends Service<AnexoCad> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<AnexoCad> getClassOfT() {
		return AnexoCad.class;
	}

	public byte[] obterArrayDeBytesDoAnexoPorId(Long id) {
		AnexoCad anexo = obterPorId(id);
		return anexo.getAnexo();

	}

	public List<AnexoCad> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<AnexoCad> retorno = crudService.findWithNamedQuery(AnexoCad.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME,
				QueryParameter.with(AnexoCad.NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA,
						idOperacaoFinanciada).parameters());
		return retorno;
	}
	
	public List<AnexoCad> obterPorCalculoRentabilidade(Long idCalculoRentabilidade) {
		List<AnexoCad> retorno = crudService.findWithNamedQuery(AnexoCad.NQ_OBTER_POR_CALCULO_RENTABILIDADE.NAME,
				QueryParameter.with(AnexoCad.NQ_OBTER_POR_CALCULO_RENTABILIDADE.NM_PM_ID_CALCULO_RENTABILIDADE, 
						idCalculoRentabilidade).parameters());
		return retorno;
	}

}
