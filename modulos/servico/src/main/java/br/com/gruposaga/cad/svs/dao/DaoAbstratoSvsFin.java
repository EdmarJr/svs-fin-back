package br.com.gruposaga.cad.svs.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;

import com.svs.fin.model.dto.ListParamsDto;
import com.svs.fin.model.dto.ParamFilterListDto;
import com.svs.model.commom.HistoricoAlteracao;
import com.svs.model.commom.Usuario;
import com.svs.model.commom.Usuario_;

import br.com.gruposaga.cad.cpo.Cpo;

public abstract class DaoAbstratoSvsFin<T> {

	private final Class<T> type;

	@Cpo
	@Inject
	protected EntityManager emCpo;

	protected String cpf;

	public DaoAbstratoSvsFin() {
		this.type = null;
	}

	public DaoAbstratoSvsFin(Class<T> type) {
		this.type = type;
	}

	/**
	 * Retorna o Type (Class) do dao em runtime
	 * 
	 * @return
	 */
	public Class<T> getType() {
		return this.type;
	}

	/**
	 * Busca um registro no banco de dados de acordo com seu Id.
	 * 
	 * @param id
	 * @return
	 */
	public T buscarPorId(Long id) {
		T t = emCpo.find(getType(), id);
		emCpo.detach(t);
		return t;
	}

	/**
	 * Busca um registro no banco de dados de acordo com seu Id.
	 * 
	 * @param id
	 * @return
	 */
	public Object buscarPorId(Long id, Class<?> type) {
		Object t = emCpo.find(type, id);
		emCpo.detach(t);
		return t;
	}

	/**
	 * Busca um registro no banco de dados de acordo com seu Id e o mantem
	 * sincronizado com o bd.
	 * 
	 * @param id
	 * @return
	 */
	public T buscarPorIdSincronizado(Long id) {
		T t = emCpo.find(getType(), id);
		return t;
	}

	/**
	 * Busca um registro no banco de dados de acordo com seu Id e o mantem
	 * sincronizado com o bd.
	 * 
	 * @param id
	 * @return
	 */
	public Object buscarPorIdSincronizado(Long id, Class<?> type) {
		Object t = emCpo.find(type, id);
		return t;
	}

	public T verificarExistencia(String valorAtributo, String nomeAtributoComparar, Long idDiferenteDe) {

		CriteriaBuilder cb = emCpo.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(getType());
		Root<T> root = cq.from(getType());

		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(cb.and(cb.like(cb.upper(root.get(nomeAtributoComparar)), valorAtributo.toUpperCase())));

		if (idDiferenteDe != null) {
			predicates.add(cb.and(cb.notEqual(root.get("id"), idDiferenteDe)));
		}

		cq.where((Predicate[]) predicates.toArray(new Predicate[predicates.size()]));
		TypedQuery<T> query = emCpo.createQuery(cq);

		T t = null;

		try {
			t = query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

		return t;
	}

	public Usuario acessUserByCpf(String cpf) {

		Usuario usuario = null;
		try {
			CriteriaBuilder cb = emCpo.getCriteriaBuilder();
			CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
			Root<Usuario> root = cq.from(Usuario.class);
			cq.select(root).where(cb.equal(root.get(Usuario_.cpf), cpf));

			usuario = emCpo.createQuery(cq).getSingleResult();

		} catch (NoResultException nre) {
			return null;
		}
		return usuario;
	}

	/**
	 * Busca as versoes de um registro do banco de dados.
	 * 
	 * @param id
	 * @return
	 */
	public List<Object[]> buscarRevisoes(Long id, boolean incluirVersaoAtual, int maxResults) {

		AuditReader reader = AuditReaderFactory.get(emCpo);

		AuditQuery auditQuery = reader.createQuery().forRevisionsOfEntity(getType(), false, false);
		auditQuery.add(AuditEntity.id().eq(id));
		auditQuery.addOrder(AuditEntity.revisionNumber().desc());
		auditQuery.setMaxResults(maxResults);

		List<Number> revNumbers = reader.getRevisions(getType(), id);

		if (incluirVersaoAtual && revNumbers.size() == 0) {
			return new ArrayList<Object[]>();

		} else if (incluirVersaoAtual == false && revNumbers.size() < 1) {
			return new ArrayList<Object[]>();
		}

		if (incluirVersaoAtual == false) {
			auditQuery.add(AuditEntity.revisionNumber().lt(revNumbers.get(revNumbers.size() - 1)));
		}

		@SuppressWarnings("unchecked")
		List<Object[]> priorRevisions = auditQuery.getResultList();

		return priorRevisions;
	}

	/**
	 * Cria um registro no banco de dados
	 * 
	 * @param t
	 *            registro a ser criado
	 */
	public void criar(T t) {
		emCpo.persist(t);
	}

	/**
	 * Cria um registro no banco de dados
	 * 
	 * @param t
	 *            registro a ser criado
	 */
	public void criarHistorico(HistoricoAlteracao t) {
		emCpo.persist(t);
	}

	/**
	 * Altera um registro no banco de dados
	 * 
	 * @param t
	 *            registro a ser alterado
	 */
	public void alterar(T t) {
		emCpo.merge(t);
	}

	/**
	 * Exclui um registro no banco de dados
	 * 
	 * @param t
	 *            registro a ser alterado
	 */
	public void excluir(T t) throws Exception {
		emCpo.remove(t);
	}

	/**
	 * Lista os registros ordenados de acordo com o atributo informado
	 * 
	 * @param attribute
	 *            Atributo a ser utilizado na ordenação
	 * @return
	 */
	public List<T> listarAsc(SingularAttribute<T, String> attribute, boolean apenasAtivos) throws Exception {

		CriteriaBuilder cb = emCpo.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(getType());
		Root<T> root = cq.from(getType());
		cq.select(root);

		if (apenasAtivos) {
			List<Predicate> predicates = new ArrayList<Predicate>();
			Predicate predicate = cb.conjunction();
			predicates.add(cb.and(predicate, cb.equal(root.get("ativo"), Boolean.TRUE)));
			cq.where((Predicate[]) predicates.toArray(new Predicate[predicates.size()]));
		}

		cq.orderBy(cb.asc(root.get(attribute.getName())));

		return emCpo.createQuery(cq).getResultList();
	}

	/**
	 * Lista os registros de acordo com os parâmetros de consulta fornecidos
	 * 
	 * @param listParamsDto
	 *            Parâmetros da listagem a ser construída
	 * @return
	 */
	public List<T> listarFiltro(ListParamsDto listParamsDto) throws Exception {

		CriteriaBuilder cb = emCpo.getCriteriaBuilder();

		CriteriaQuery<T> criteria = cb.createQuery(getType());
		Root<T> rootOperacao = criteria.from(getType());
		List<Predicate> predicatesFilter = new ArrayList<Predicate>();

		configurarFiltroPredicates(listParamsDto.getFilters(), cb, rootOperacao, predicatesFilter);
		configurarFiltroAcesso(cb, rootOperacao, predicatesFilter);

		criteria.where((Predicate[]) predicatesFilter.toArray(new Predicate[predicatesFilter.size()]));

		configurarOrdenacao(listParamsDto, cb, criteria, rootOperacao);

		CriteriaQuery<T> select = criteria.select(rootOperacao);
		TypedQuery<T> typedQuery = emCpo.createQuery(select);

		configurarPagina(listParamsDto, typedQuery);

		List<T> lista = typedQuery.getResultList();
		return lista;
	}

	protected List<Predicate> configurarFiltroPredicates(List<ParamFilterListDto> filters, CriteriaBuilder cb,
			Root root, List<Predicate> predicates) throws Exception {
		return DaoUtil.configurarFiltroPredicates(filters, cb, root, predicates);
	}

	public int countListarFiltro(ListParamsDto listParamsDto) throws Exception {

		CriteriaBuilder cb = emCpo.getCriteriaBuilder();

		CriteriaQuery<Long> criteria = cb.createQuery(Long.class);

		Root<T> rootOperacao = criteria.from(getType());
		List<Predicate> predicatesFilter = new ArrayList<Predicate>();

		configurarFiltroPredicates(listParamsDto.getFilters(), cb, rootOperacao, predicatesFilter);
		configurarFiltroAcesso(cb, rootOperacao, predicatesFilter);

		criteria.where((Predicate[]) predicatesFilter.toArray(new Predicate[predicatesFilter.size()]));

		Expression<Long> count = cb.countDistinct(rootOperacao);
		criteria.select(count);

		Long quantidade = emCpo.createQuery(criteria).getSingleResult();

		return quantidade.intValue();
	}

	/**
	 * 
	 * @param listParamsDto
	 *            Parâmetros da listagem a ser construída
	 * @param typedQuery
	 *            Query que está sendo construída
	 */
	private void configurarPagina(ListParamsDto listParamsDto, TypedQuery<T> typedQuery) throws Exception {

		if (listParamsDto.getMaxResult() == 0) {
			typedQuery.setMaxResults(200);
		} else {
			typedQuery.setMaxResults(listParamsDto.getMaxResult());
		}

		if (listParamsDto.getFirstResult() >= 0) {
			typedQuery.setFirstResult(listParamsDto.getFirstResult());
		}
	}

	/**
	 * Configura a ordenação da listagem (método deverá ser sobrescrito em consultas
	 * mais complexas, onde for necessário realizar join's)
	 * 
	 * @param listParamsDto
	 *            Parâmetros da listagem a ser construída
	 * @param cb
	 *            CriteriaBuilder sendo construída
	 * @param criteria
	 *            Critéria onde a ordenação será configurada
	 * @param rootOperacao
	 *            Root da consulta sendo construída
	 */
	protected void configurarOrdenacao(ListParamsDto listParamsDto, CriteriaBuilder cb, CriteriaQuery<T> criteria,
			Root<T> rootOperacao) throws Exception {
		if (listParamsDto.getSortField() != null && !listParamsDto.getSortField().trim().equals("")) {
			if (listParamsDto.getSortOrder().toUpperCase().contains("DES")) {
				criteria.orderBy(cb.desc(rootOperacao.get(listParamsDto.getSortField())));
			} else {
				criteria.orderBy(cb.asc(rootOperacao.get(listParamsDto.getSortField())));
			}
		}
	}

	/**
	 * 
	 * Método deve construir os predicates de permissao de acesso (método deverá ser
	 * sobrescrito se necessário)
	 * 
	 * @param cb
	 *            CriteriaBuilder sendo construída
	 * @param root
	 *            Root da consulta sendo construída
	 * @return
	 */
	protected void configurarFiltroAcesso(CriteriaBuilder cb, Root<T> root, List<Predicate> predicates)
			throws Exception {

	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
