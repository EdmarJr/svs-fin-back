package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.Perfil;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class PerfilService extends Service<Perfil> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<Perfil> getClassOfT() {
		return Perfil.class;
	}

}
