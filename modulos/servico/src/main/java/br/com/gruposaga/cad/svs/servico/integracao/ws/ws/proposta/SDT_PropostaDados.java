package br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SDT_PropostaDados")
public class SDT_PropostaDados {

	private Long propostaCodigo;

	private Double propostaValor;

	private String empresaCnpj;

	private Long atendimentoCodigo;

	private Boolean enviaEmail;

	private Boolean entregaPesquisaEmail;

	private Boolean aprovacaoCobranca;

	private Boolean aprovacaoGerente;

	private Boolean aprovacaoEntrega;

	private Boolean liberacaoExtra;

	private String propostaObservacao;

	private String estoqueTipo;

	private DadosVeiculo dadosVeiculo;

	private DadosCliente dadosCliente;

	private DadosVendedor dadosVendedor;

	@XmlElement(name = "Proposta_Codigo")
	public Long getPropostaCodigo() {
		return propostaCodigo;
	}

	public void setPropostaCodigo(Long propostaCodigo) {
		this.propostaCodigo = propostaCodigo;
	}

	@XmlElement(name = "Proposta_Valor")
	public Double getPropostaValor() {
		return propostaValor;
	}

	public void setPropostaValor(Double propostaValor) {
		this.propostaValor = propostaValor;
	}

	@XmlElement(name = "Empresa_CNPJ")
	public String getEmpresaCnpj() {
		return empresaCnpj;
	}

	public void setEmpresaCnpj(String empresaCnpj) {
		this.empresaCnpj = empresaCnpj;
	}

	@XmlElement(name = "Atendimento_Codigo")
	public Long getAtendimentoCodigo() {
		return atendimentoCodigo;
	}

	public void setAtendimentoCodigo(Long atendimentoCodigo) {
		this.atendimentoCodigo = atendimentoCodigo;
	}

	@XmlElement(name = "Envia_Email")
	public Boolean getEnviaEmail() {
		return enviaEmail;
	}

	public void setEnviaEmail(Boolean enviaEmail) {
		this.enviaEmail = enviaEmail;
	}

	@XmlElement(name = "EntregaPesquisaEmail")
	public Boolean getEntregaPesquisaEmail() {
		return entregaPesquisaEmail;
	}

	public void setEntregaPesquisaEmail(Boolean entregaPesquisaEmail) {
		this.entregaPesquisaEmail = entregaPesquisaEmail;
	}

	@XmlElement(name = "AprovacaoCobranca")
	public Boolean getAprovacaoCobranca() {
		return aprovacaoCobranca;
	}

	public void setAprovacaoCobranca(Boolean aprovacaoCobranca) {
		this.aprovacaoCobranca = aprovacaoCobranca;
	}

	@XmlElement(name = "AprovacaoGerente")
	public Boolean getAprovacaoGerente() {
		return aprovacaoGerente;
	}

	public void setAprovacaoGerente(Boolean aprovacaoGerente) {
		this.aprovacaoGerente = aprovacaoGerente;
	}

	@XmlElement(name = "AprovacaoEntrega")
	public Boolean getAprovacaoEntrega() {
		return aprovacaoEntrega;
	}

	public void setAprovacaoEntrega(Boolean aprovacaoEntrega) {
		this.aprovacaoEntrega = aprovacaoEntrega;
	}

	@XmlElement(name = "LiberacaoExtra")
	public Boolean getLiberacaoExtra() {
		return liberacaoExtra;
	}

	public void setLiberacaoExtra(Boolean liberacaoExtra) {
		this.liberacaoExtra = liberacaoExtra;
	}

	@XmlElement(name = "Proposta_Observacao")
	public String getPropostaObservacao() {
		return propostaObservacao;
	}

	public void setPropostaObservacao(String propostaObservacao) {
		this.propostaObservacao = propostaObservacao;
	}

	@XmlElement(name = "Estoque_Tipo")
	public String getEstoqueTipo() {
		return estoqueTipo;
	}

	public void setEstoqueTipo(String estoqueTipo) {
		this.estoqueTipo = estoqueTipo;
	}

	@XmlElement(name = "DadosVeiculo")
	public DadosVeiculo getDadosVeiculo() {
		return dadosVeiculo;
	}

	public void setDadosVeiculo(DadosVeiculo dadosVeiculo) {
		this.dadosVeiculo = dadosVeiculo;
	}

	@XmlElement(name = "DadosCliente")
	public DadosCliente getDadosCliente() {
		return dadosCliente;
	}

	public void setDadosCliente(DadosCliente dadosCliente) {
		this.dadosCliente = dadosCliente;
	}

	@XmlElement(name = "DadosVendedor")
	public DadosVendedor getDadosVendedor() {
		return dadosVendedor;
	}

	public void setDadosVendedor(DadosVendedor dadosVendedor) {
		this.dadosVendedor = dadosVendedor;
	}
}
