package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.enums.EnumTipoRecebimento;
import com.svs.fin.model.entities.TipoPagamento;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;
import br.com.gruposaga.cad.svs.utils.UtilsEnum;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TipoPagamentoService extends Service<TipoPagamento> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	public List<TipoPagamento> obterPorTipoRecebimento(String tipoRecebimento) {
		List<TipoPagamento> retorno = crudService.findWithNamedQuery(TipoPagamento.NQ_OBTER_POR_TIPO.NAME,
				QueryParameter
						.with(TipoPagamento.NQ_OBTER_POR_TIPO.NM_PM_TIPO,
								UtilsEnum.obterEnumLabeledPorLabel(EnumTipoRecebimento.values(), tipoRecebimento))
						.parameters());
		
		return retorno;
	}

	@Override
	protected Class<TipoPagamento> getClassOfT() {
		return TipoPagamento.class;
	}

}
