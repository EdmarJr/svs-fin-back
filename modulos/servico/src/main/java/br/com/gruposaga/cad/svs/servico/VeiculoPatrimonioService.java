package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.VeiculoPatrimonio;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
public class VeiculoPatrimonioService extends Service<VeiculoPatrimonio> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<VeiculoPatrimonio> getClassOfT() {
		return VeiculoPatrimonio.class;
	}

	public List<VeiculoPatrimonio> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(VeiculoPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.NAME,
				QueryParameter.with(VeiculoPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
