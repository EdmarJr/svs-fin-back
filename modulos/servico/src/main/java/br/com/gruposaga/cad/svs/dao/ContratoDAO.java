package br.com.gruposaga.cad.svs.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.svs.fin.model.entities.OperacaoFinanciada;
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ContratoDAO {
	
	@PersistenceContext(unitName = "Dealer")
	private EntityManager manager;
	
	public String obterMensagemContrato(OperacaoFinanciada operacaoFinanciada) {
        StringBuilder sSql = new StringBuilder();
        
        String bancoDealerWrk = "Saga_DealernetWF_homologacao";
       
		sSql.append("select ConfiguraMensagem_Texto from " + bancoDealerWrk + ".dbo.ConfiguraMensagem where ConfiguraMensagem_Codigo ");
        sSql.append("in (select ParametroEmpresaGeralMsg_ConfiguraMensagemCod from " + bancoDealerWrk + ".dbo.ParametroEmpresaGeralMsg where ParametroEmpresa_Codigo = " + operacaoFinanciada.getUnidade().getIdEmpExterno() + ")");

        Session session = (Session) manager.getDelegate();

        SQLQuery q = session.createSQLQuery(sSql.toString());
        List<String> result = q.list();

        String resultadoConcatenado = "";

        for (String mensagem : result) {
            resultadoConcatenado += mensagem;
        }

        return resultadoConcatenado;
	}

}
