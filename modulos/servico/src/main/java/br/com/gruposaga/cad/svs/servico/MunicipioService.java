package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.Estado;
import com.svs.fin.model.entities.Municipio;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MunicipioService extends Service<Municipio> {

	@Inject
	private CrudService crudService;

	@Inject
	private EstadoService estadoService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<Municipio> getClassOfT() {
		return Municipio.class;
	}

	private List<Estado> listaEstadosGerenciados = new ArrayList<Estado>();

	@Override
	public void beforeSave(Municipio municipio) {
		super.beforeSave(municipio);
		if (municipio.getEstado() != null && municipio.getEstado().getId() != null) {

			if (seEstadoExisteNaListaDeGerenciados(municipio.getEstado())) {
				municipio
						.setEstado(listaEstadosGerenciados.get(listaEstadosGerenciados.indexOf(municipio.getEstado())));
			} else {
				Estado estado = estadoService.obterPorId(municipio.getEstado().getId());
				municipio.setEstado(estado);
				listaEstadosGerenciados.add(estado);
			}

			municipio.setEstado(estadoService.obterPorId(municipio.getEstado().getId()));
		}
	}

	private boolean seEstadoExisteNaListaDeGerenciados(Estado estado) {
		return listaEstadosGerenciados.indexOf(estado) != -1;
	}

}
