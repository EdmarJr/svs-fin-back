package br.com.gruposaga.cad.svs.dao.generic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.dto.listagemtabelalazy.ParametroBusca;
import com.svs.fin.model.dto.listagemtabelalazy.ParametroOrdenacao;

import br.com.gruposaga.cad.cpo.Cpo;
import br.com.gruposaga.cad.svs.utils.UtilsData;
import br.com.gruposaga.cad.svs.utils.UtilsIterable;
import br.com.gruposaga.cad.svs.utils.UtilsJpql;
import br.com.gruposaga.cad.svs.utils.UtilsReflection;

@SuppressWarnings("serial")
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class PaginadorService implements Serializable {

	private static final String IN = "IN";
	private static final String SEPARADOR_ARRAY_DE_OPCOES = ":";
	private static final String LIKE = "like";
	private static final String FIM = "Fim";
	private static final String INICIO = "Inicio";
	private static final String BETWEEN = "BETWEEN";
	private static final String SEPARADOR_RANGE = "-ATE-";
	@Cpo
	@Inject
	public EntityManager em;

	@PostConstruct
	public void init() {
	}

	public <T> List<T> obterPorParametros(Class<T> clazz,
			GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy) {
		return obterPorParametros(clazz, grupoDeParametrosParaListagemLazy, new String[] {});
	}

	public <T> List<T> obterPorParametros(Class<T> clazz,
			GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy,
			String... listaDeParametrosParaFazerFetch) {
		StringBuilder stringBuilder = new StringBuilder(
				UtilsJpql.criarSelectParaBuscarTodos(clazz, listaDeParametrosParaFazerFetch));

		// obter texto jpsql parametros busca
		String parteJpqlParametrosDeBusca = construirParteDeBuscaDoSqlPorListaDeParametrosBusca(clazz,
				grupoDeParametrosParaListagemLazy.getParametrosDeBusca());

		stringBuilder = new StringBuilder(UtilsJpql.removerJoinsConflitantesDoTextoJpqlObjeto(stringBuilder.toString(),
				parteJpqlParametrosDeBusca));
		stringBuilder.append(parteJpqlParametrosDeBusca);

		// adicionar parametros ordenacao
		stringBuilder.append(construirParteDeOrdenacaoDoSqlPorListaDeParametrosOrdenacao(
				grupoDeParametrosParaListagemLazy.getParametrosDeOrdenacao()));

		String namedQuery = stringBuilder.toString();

		TypedQuery<T> query = em.createQuery(namedQuery, clazz);
		// definir valores dos parametros - definindo aqui, evitamos sql injection
		adicionarParametrosDeBuscaNaQuery(clazz, grupoDeParametrosParaListagemLazy.getParametrosDeBusca(), query);

		if (grupoDeParametrosParaListagemLazy.getLimit() != null) {
			query.setMaxResults(grupoDeParametrosParaListagemLazy.getLimit());
		}

		if (grupoDeParametrosParaListagemLazy.getOffSet() != null) {
			query.setFirstResult(grupoDeParametrosParaListagemLazy.getOffSet());
		}

		/**
		 * o distinct resolveria, mas, quando utilizando order by e o distinct, o
		 * postgres retorna um erro bem complicado de se resolver
		 **/
		List<T> resultados = UtilsJpql.seQueryFazJoin(namedQuery)
				? UtilsIterable.removerItensDuplicadosNaLista(query.getResultList())
				: query.getResultList();

		// resultados.forEach(item -> {
		// for (String parametro : listaDeParametrosParaFazerFetch) {
		// inicializarListaEmObjeto(item, parametro);
		// }
		// });

		return resultados;
	}

	public <T> Long obterTotalDeLinhasPorParametros(Class<T> clazz,
			GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy) {
		StringBuilder stringBuilder = new StringBuilder(
				UtilsJpql.criarSelectCountParaObterNumeroDeLinhasDaTabela(clazz));
		// adicionar parametros busca
		stringBuilder.append(construirParteDeBuscaDoSqlPorListaDeParametrosBusca(clazz,
				grupoDeParametrosParaListagemLazy.getParametrosDeBusca()));
		String namedQuery = stringBuilder.toString();
		if (UtilsJpql.seQueryFazJoinENaoTemDistinct(namedQuery)) {
			namedQuery = namedQuery.replace("SELECT", "SELECT DISTINCT");
		}
		Query query = em.createQuery(namedQuery);
		// definir valores dos parametros - definindo aqui, evitamos sql injection
		adicionarParametrosDeBuscaNaQuery(clazz, grupoDeParametrosParaListagemLazy.getParametrosDeBusca(), query);
		return (Long) query.getSingleResult();
	}

	/**
	 * Todas as consultas são feitas em letras maiúsculas para que não ocorra
	 * diferenciação entre case na hora de fazer o search
	 * 
	 * @param clazz
	 * @param listaDeParametrosDeBusca
	 * @param query
	 */
	private <T> void adicionarParametrosDeBuscaNaQuery(Class<T> clazz, List<ParametroBusca> listaDeParametrosDeBusca,
			Query query) {
		listaDeParametrosDeBusca.forEach((p) -> {
			Object valorCampo = null;
			String textoBusca = p.getTextoBusca() != null ? p.getTextoBusca().trim().toUpperCase() : p.getTextoBusca();
			if (seCampoIsRangeDeValores(textoBusca)) {
				String[] valorParametros = textoBusca.split(SEPARADOR_RANGE);
				List<Object> parametrosRange = obterParametrosRangeComOTipoCorreto(clazz, p, valorParametros);
				query.setParameter(obterNomeParametro(p.getNomeCampo()) + INICIO, parametrosRange.get(0));
				query.setParameter(obterNomeParametro(p.getNomeCampo()) + FIM, parametrosRange.get(1));
				return;
			} else if (seParametroDeBuscaIsArrayDeOpcoes(textoBusca)) {
				String[] textosBuscas = obterListaDeValoresDeUmArrayDeOpcoes(p.getTextoBusca());
				List<Object> arrayDeOpcoes = new ArrayList<>();
				for (int i = 0; i < textosBuscas.length; i++) {
					Object valorCampoApropriado = obterValorCampoApropriadoParaQuery(clazz, p.getNomeCampo(),
							textosBuscas[i]);
					arrayDeOpcoes.add(valorCampoApropriado);
				}
				query.setParameter(obterNomeParametro(p.getNomeCampo()), arrayDeOpcoes);
				return;
			} else {
				valorCampo = obterValorCampoApropriadoParaQuery(clazz, p.getNomeCampo(), p.getTextoBusca());
			}
//			} else if (UtilsReflection.getTypeOfFieldOfClass(clazz, p.getNomeCampo()) == Long.class) {
//				valorCampo = Long.parseLong(textoBusca);
//			} else if (UtilsReflection.getTypeOfFieldOfClass(clazz, p.getNomeCampo()) == Integer.class) {
//				valorCampo = Integer.parseInt(textoBusca);
//			} else if (UtilsReflection.getTypeOfFieldOfClass(clazz, p.getNomeCampo()).getSuperclass() == Enum.class) {
//				valorCampo = obterEnumConstantReferenteAoTextoBuscado(
//						(Class<Enum<?>>) UtilsReflection.getTypeOfFieldOfClass(clazz, p.getNomeCampo()),
//						p.getTextoBusca());
//			} else {
//				valorCampo = UtilsJpql.toLikeParameter(textoBusca);
//			}
			query.setParameter(obterNomeParametro(p.getNomeCampo()), valorCampo);
		});
	}

	private String[] obterListaDeValoresDeUmArrayDeOpcoes(String textoBusca) {
		return textoBusca.split(SEPARADOR_ARRAY_DE_OPCOES);
	}

	private <T> Object obterValorCampoApropriadoParaQuery(Class<T> clazz, String nomeCampo, String textoBuscaOriginal) {
		String textoBusca = textoBuscaOriginal != null ? textoBuscaOriginal.trim().toUpperCase() : textoBuscaOriginal;
		Object valorCampo = null;
		if (UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampo) == Long.class) {
			valorCampo = Long.parseLong(textoBusca);
		} else if (UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampo) == Integer.class) {
			valorCampo = Integer.parseInt(textoBusca);
		} else if (UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampo).getSuperclass() == Enum.class) {
			valorCampo = obterEnumConstantReferenteAoTextoBuscado(
					(Class<Enum<?>>) UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampo), textoBuscaOriginal);
		} else {
			valorCampo = UtilsJpql.toLikeParameter(textoBusca);
		}

		return valorCampo;
	}

	private boolean seParametroDeBuscaIsArrayDeOpcoes(String textoBusca) {
		return textoBusca.indexOf(SEPARADOR_ARRAY_DE_OPCOES) != -1;
	}

	private <T> List<Object> obterParametrosRangeComOTipoCorreto(Class<T> clazz, ParametroBusca p,
			String[] valorParametros) {
		List<Object> parametrosRange = new ArrayList<Object>();
		for (String valorParametro : valorParametros) {
			if (UtilsData.seTextoTemFormatoDeDataPtBr(valorParametro)) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				LocalDate localDate = LocalDate.parse(valorParametro, formatter);
				if (UtilsReflection.getTypeOfFieldOfClass(clazz, p.getNomeCampo()) == Calendar.class) {
					parametrosRange.add(UtilsData.toCalendar(localDate));
				} else {
					parametrosRange.add(localDate);
				}
			} else {
				parametrosRange.add(valorParametros);
			}
		}
		return parametrosRange;
	}

	private Enum<?> obterEnumConstantReferenteAoTextoBuscado(Class<Enum<?>> clazzEnum, String textoBuscaOriginal) {
		String textoBuscaFormatoConstante = textoBuscaOriginal.trim().toUpperCase();
		Enum<?>[] enumConstants = clazzEnum.getEnumConstants();
		Enum<?> enumRetorno = obterEnumPorNomeConstante(textoBuscaFormatoConstante, clazzEnum);

		if (enumRetorno == null) {
			Field[] declaredFields = clazzEnum.getDeclaredFields();
			for (Field field : declaredFields) {
				JsonProperty annotation = field.getAnnotation(JsonProperty.class);
				if (annotation != null && annotation.value().equals(textoBuscaOriginal)) {
					enumRetorno = obterEnumPorNomeConstante(field.getName(), clazzEnum);
					break;
				}
			}
		}

		if (enumRetorno == null) {
			for (Enum<?> enum1 : enumConstants) {
				if (enum1.toString().trim().toUpperCase().indexOf(textoBuscaFormatoConstante) != -1) {
					enumRetorno = enum1;
					break;
				}
			}
		}
		return enumRetorno;
	}

	private Enum<?> obterEnumPorNomeConstante(String textoBusca, Class<Enum<?>> clazzEnum) {
		Enum<?> enumRetorno = null;
		Enum<?>[] enumConstants = clazzEnum.getEnumConstants();
		for (Enum<?> enum1 : enumConstants) {
			if (enum1.toString().trim().toUpperCase().indexOf(textoBusca) == 0) {
				enumRetorno = enum1;
				break;
			}
		}
		return enumRetorno;
	}

	private String construirParteDeOrdenacaoDoSqlPorListaDeParametrosOrdenacao(
			List<ParametroOrdenacao> parametrosDeOrdenacao) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < parametrosDeOrdenacao.size(); i++) {
			if (UtilsIterable.sePrimeiraIteracao(i)) {
				stringBuilder.append(" order by ");
			}

			ParametroOrdenacao parametroOrdenacao = parametrosDeOrdenacao.get(i);
			stringBuilder.append("t.");
			stringBuilder.append(parametroOrdenacao.getNomeCampo());
			stringBuilder.append(" ");
			// embora pleonástico, evita sql injection
			stringBuilder.append(parametroOrdenacao.getTipoOrdenacao().equalsIgnoreCase("asc") ? "ASC" : "DESC");

			if (UtilsIterable.seNaoIsUltimaIteracao(parametrosDeOrdenacao.size(), i)) {
				stringBuilder.append(", ");
			}
		}
		return stringBuilder.toString();
	}

	private <T> String construirParteDeBuscaDoSqlPorListaDeParametrosBusca(Class<T> clazz,
			List<ParametroBusca> parametrosDeBusca) {
		StringBuilder stringBuilder = new StringBuilder();

		// realiza joins necessários por causa das listas
		String joinsNecessarios = construirJoinsNecessarios(clazz, parametrosDeBusca);
		stringBuilder.append(joinsNecessarios);

		for (int i = 0; i < parametrosDeBusca.size(); i++) {
			String aliasInicial = "t.";
			if (UtilsIterable.sePrimeiraIteracao(i)) {
				stringBuilder.append(" where ");
			}
			ParametroBusca parametroBusca = parametrosDeBusca.get(i);
			String nomeCampoOriginal = parametroBusca.getNomeCampo();
			String[] pedacosNomeCampo = nomeCampoOriginal.split("[.]");
			String nomeCampo = "";
			for (int j = 0; j < pedacosNomeCampo.length; j++) {
				nomeCampo += pedacosNomeCampo[j];
				Class<?> typeCampo = UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampo);
				if (UtilsReflection.seTipoIsList(typeCampo)) {
					aliasInicial = nomeCampo.replace(".", "") + ".";
					nomeCampo = String.join(".",
							UtilsIterable.obterItensArrayAPartirDaPosicaoReferencia(pedacosNomeCampo, j + 1));
					break;
				}
				if (UtilsIterable.seNaoIsUltimaIteracao(pedacosNomeCampo.length, j)) {
					nomeCampo += ".";
				}
			}

			if (seCampoAceitaUpper(clazz, parametroBusca, nomeCampo)) {
				stringBuilder.append("upper(");
			}
			stringBuilder.append(aliasInicial);
			stringBuilder.append(nomeCampo);
			if (seCampoAceitaUpper(clazz, parametroBusca, nomeCampo)) {
				stringBuilder.append(")");
			}

			String operadorDeComparacao = obterOperadorDeComparacao(clazz, nomeCampoOriginal,
					parametroBusca.getTextoBusca());
			stringBuilder.append(" " + operadorDeComparacao + " :");
			if (operadorDeComparacao.equals(BETWEEN)) {
				stringBuilder.append(obterNomeParametro(nomeCampoOriginal) + INICIO);
				stringBuilder.append(" AND :");
				stringBuilder.append(obterNomeParametro(nomeCampoOriginal) + FIM);
			} else {
				stringBuilder.append(obterNomeParametro(nomeCampoOriginal));
			}
			if (UtilsIterable.seNaoIsUltimaIteracao(parametrosDeBusca.size(), i)) {
				stringBuilder.append(" AND ");
			}
		}

		return stringBuilder.toString();
	}

	private <T> boolean seCampoAceitaUpper(Class<T> clazz, ParametroBusca parametroBusca, String nomeCampo) {
		return !seParametrosSoTemNumeros(parametroBusca.getTextoBusca())
				&& !UtilsReflection.seTypeOfFieldOfClassIgualaoTipoReferencia(clazz, nomeCampo, Calendar.class)
				&& !UtilsReflection.seTypeOfFieldOfClassIgualaoTipoReferencia(clazz, nomeCampo, LocalDate.class);
	}

	private <T> String obterOperadorDeComparacao(Class<T> clazz, String nomeCampoOriginal, String textoBusca) {
		if (seCampoIsRangeDeValores(textoBusca)) {
			return BETWEEN;
		} else if (seParametroDeBuscaIsArrayDeOpcoes(textoBusca)) {
			return IN;
		} else if (UtilsReflection.seCampoIsString(clazz, nomeCampoOriginal)) {
			return LIKE;
		} else {
			return "=";
		}
	}

	private boolean seParametrosSoTemNumeros(String texto) {
		return texto.matches("^[0-9]*$");
	}

	private <T> String construirJoinsNecessarios(Class<T> clazz, List<ParametroBusca> parametrosDeBusca) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < parametrosDeBusca.size(); i++) {
			ParametroBusca parametroBusca = parametrosDeBusca.get(i);
			String[] pedacosNomeCampo = parametroBusca.getNomeCampo().split("[.]");
			String nomeCampoTemp = "";
			for (int j = 0; j < pedacosNomeCampo.length; j++) {
				nomeCampoTemp += pedacosNomeCampo[j];
				Class<?> typeCampo = UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampoTemp);
				if (UtilsReflection.seTipoIsList(typeCampo)) {
					stringBuilder.append(" INNER JOIN t." + nomeCampoTemp + " " + nomeCampoTemp.replace(".", ""));
				}
				nomeCampoTemp += ".";
			}
		}
		return stringBuilder.toString();
	}

	private String obterNomeParametro(String nomeCampo) {
		return nomeCampo.replaceAll("[.]", "");
	}

	private boolean seCampoIsRangeDeValores(String textoBusca) {
		if (textoBusca.split(SEPARADOR_RANGE).length == 2) {
			return true;
		}
		return false;
	}

}