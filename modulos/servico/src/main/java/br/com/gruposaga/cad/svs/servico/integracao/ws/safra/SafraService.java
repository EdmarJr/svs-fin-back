package br.com.gruposaga.cad.svs.servico.integracao.ws.safra;

import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.tempuri.ICapturaPropostas;
import org.tempuri.ICapturaPropostasProxy;

import com.svs.fin.integracao.safra.model.Cadastro_Comum;
import com.svs.fin.integracao.safra.model.DadosBasicosSafra;
import com.svs.fin.integracao.safra.model.Financiamento;
import com.svs.fin.integracao.safra.model.ParamCalculaParcelas;
import com.svs.fin.integracao.safra.model.Proposta_PF;
import com.svs.fin.integracao.safra.model.RetornoCalculoParcelas;
import com.svs.fin.integracao.safra.model.RetornoProposta;
import com.svs.fin.integracao.safra.model.ValidaHASH;
import com.svs.fin.integracao.safra.model_comum.Avalista;
import com.svs.fin.integracao.safra.pojos.PropostaCompletaPFSafra;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SafraService {

	private static final String HASH = "1DFFA600E6489D3700A39CE2E5BEFD01";
	private static final Integer ID_REVENDA = 111;

	public void init() {

//		Cadastro_Comum cadastroComum = new Cadastro_Comum();
//		Proposta_PF proposta_PF = new Proposta_PF();
//		Avalista avalista = new Avalista();
//		Financiamento financiamento = new Financiamento();
//		try {
//			DadosBasicosSafra dados_Safra = iCapturaPropostas.dados_Safra(HASH);
//
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		// iCapturaPropostas.enviar_Proposta_PF(p_ds_hash_simul, validahash,
		// cadastroComum, proposta_PF, avalista, financiamento);
		// iCapturarPropostas.
		// iCapturaPropostas.dados_Safra(dshashEmpFEI);
	}

	public RetornoCalculoParcelas obterSimulacoes(ParamCalculaParcelas parametros) throws RemoteException {
		ICapturaPropostas iCapturaPropostas = new ICapturaPropostasProxy();
		RetornoCalculoParcelas retorno = iCapturaPropostas.consultarFluxoParcelas(parametros);
		return retorno;
	}
	
	public RetornoProposta enviarPropostaPF(PropostaCompletaPFSafra proposta) throws RemoteException {
		ICapturaPropostas iCapturaPropostas = new ICapturaPropostasProxy();
		ValidaHASH validaHASH = new ValidaHASH(ID_REVENDA, HASH);
		String p_ds_hash_simul = "aaa";
		return iCapturaPropostas.enviar_Proposta_PF(p_ds_hash_simul, validaHASH, proposta.getPropostaComum(), proposta.getPropostaPf(), proposta.getAvalistas(), proposta.getPropostaFinanciamento());
	}

}
