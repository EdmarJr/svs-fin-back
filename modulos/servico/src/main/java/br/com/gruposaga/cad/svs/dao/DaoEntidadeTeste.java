package br.com.gruposaga.cad.svs.dao;

import com.svs.model.centralLeads.ProdutoInteresse;
import teste.EntidadeTeste;

public class DaoEntidadeTeste extends DaoAbstratoSvsFin<EntidadeTeste>{

	public DaoEntidadeTeste() {
		super(EntidadeTeste.class);
	}
}
