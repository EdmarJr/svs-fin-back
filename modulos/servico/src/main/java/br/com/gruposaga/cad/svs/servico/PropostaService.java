package br.com.gruposaga.cad.svs.servico;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.svs.fin.model.entities.ClienteSvs;
import com.svs.fin.model.entities.Proposta;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.exceptions.ClienteDealerNaoSalvoException;
import br.com.gruposaga.cad.svs.exceptions.PropostaNaoEncontradaException;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.DadosCliente;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.SDT_PropostaDados;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.WSPropostaDados;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.WSPropostaDadosCONSULTARPROPOSTA;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.WSPropostaDadosCONSULTARPROPOSTAResponse;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.WSPropostaDadosSoapPort;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PropostaService extends Service<Proposta> {

	@Inject
	private CrudService crudService;

	@Inject
	private VeiculoPropostaService veiculoPropostaService;
	
	@Inject
	private ClienteSvsService clienteSvsService;
	
	@Inject
	private UnidadeOrganizacionalService unidadeOrganizacionalService;
	@Inject
	private OpcionalVeiculoService opcionalVeiculoService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<Proposta> getClassOfT() {
		return Proposta.class;
	}
	
	public void alterarValorProposta(Proposta proposta) {
		proposta = obterPorId(proposta.getId());
		proposta.setPropostaValor(proposta.getPropostaValor());
		alterar(proposta);

	}

	public Proposta obterPorCodigo(Long codigoProposta)
			throws PropostaNaoEncontradaException, JAXBException, ClienteDealerNaoSalvoException {

		// if (buscarExistente) {
		// this.operacoesFinanciadasExistentes = getOperacaoFinanciadaHome()
		// .getOperacoesFinanciadas(getOperacaoFinanciadaSelected().getProposta().getPropostaCodigo());
		// if (getOperacoesFinanciadasExistentes().size() > 0) {
		// RequestContext.getCurrentInstance().update("operacaoExistenteDialog");
		// RequestContext.getCurrentInstance().execute("PF('operacaoExistenteDialog').show();");
		// return;
		// }
		// }
		WSPropostaDados ws = new WSPropostaDados();
		WSPropostaDadosSoapPort wsPort = ws.getWSPropostaDadosSoapPort();

		WSPropostaDadosCONSULTARPROPOSTA dadosConsulta = new WSPropostaDadosCONSULTARPROPOSTA();
		dadosConsulta.setUsuarioIdentificador("MYSAGA");
		dadosConsulta.setUsuariosenhaSenha("Mysaga@2017");
		dadosConsulta.setPropostaCodigo(codigoProposta.intValue());

		WSPropostaDadosCONSULTARPROPOSTAResponse consultaResponse = wsPort.consultarproposta(dadosConsulta);

		BufferedReader br = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(consultaResponse.getXml().getBytes())));

		JAXBContext jaxbContext = JAXBContext.newInstance(SDT_PropostaDados.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		SDT_PropostaDados sdt_proposta = (SDT_PropostaDados) jaxbUnmarshaller.unmarshal(br);

		if (sdt_proposta.getPropostaCodigo() == null || sdt_proposta.getPropostaCodigo() == 0) {
			throw new PropostaNaoEncontradaException();
		}

		Proposta proposta = montarObjetoProposta(sdt_proposta);

		DadosCliente dadosCliente = sdt_proposta.getDadosCliente();
		if (dadosCliente.getClienteCpfCnpj() != null && !dadosCliente.getClienteCpfCnpj().equals("")) {

			dadosCliente.setClienteCpfCnpj(dadosCliente.getClienteCpfCnpj());
			ClienteSvs cliente = clienteSvsService.obterPorCpfCnpj(dadosCliente.getClienteCpfCnpj());
			if (cliente == null) {
				// TODO BUSCAR CLIENTE NA BASE DO DEALER - é o que está sendo feito abaixo, e
				// depois salva
				// ele buscando os dados que faltam ainda na base do dealer
			}
			if (cliente == null) {
				cliente = clienteSvsService.obterPorCpfCnpjDoBancoDoDealer(dadosCliente.getClienteCpfCnpj());
				try {
					if (cliente != null) {
						clienteSvsService.salvarClienteVindoDoDealer(cliente);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new ClienteDealerNaoSalvoException("Houve um erro ao salvar o cliente proveniente do Dealer");
				}
			}
			proposta.setClienteSvs(cliente);

		}

		proposta.setCpfVendedor(sdt_proposta.getDadosVendedor().getVendedorCpf());

		if (sdt_proposta != null && sdt_proposta.getEmpresaCnpj() != null
				&& !sdt_proposta.getEmpresaCnpj().trim().equals("")) {
			// operacaoFinanciada.setUnidade(unidadeOrganizacionalService.obterPorCnpj(sdt_proposta.getEmpresaCnpj()));
			// -- JA SETTADO EM PROPOSTA
		}

		// if (!getPermissaoUsuario().getAcessoAdmin() &&
		// !(permissaoUsuario.getAcessoVenda() && permissaoUsuario
		// .getUnidadesPermissaoVenda().contains(getOperacaoFinanciadaSelected().getUnidade())))
		// {
		//
		// UnidadeOrganizacional unidade = getOperacaoFinanciadaSelected().getUnidade();
		// limparOperacaoFinanciada();
		// FacesContext.getCurrentInstance().addMessage(null, new
		// FacesMessage(FacesMessage.SEVERITY_INFO, "Erro!",
		// "Você não possui permissão para esta loja! (" + unidade.getNomeFantasia() +
		// ")"));
		//
		// }

		// this.parcelaEntradaSelected = new ParcelaEntrada();
		return proposta;
	}

	public Proposta montarObjetoProposta(SDT_PropostaDados sdtProposta) {

		Proposta proposta = new Proposta();

		proposta.setAprovacaoCobranca(sdtProposta.getAprovacaoCobranca());
		proposta.setAprovacaoEntrega(sdtProposta.getAprovacaoEntrega());
		proposta.setAprovacaoGerente(sdtProposta.getAprovacaoGerente());
		proposta.setAtendimentoCodigo(sdtProposta.getAtendimentoCodigo());
		proposta.setEmpresaCnpj(sdtProposta.getEmpresaCnpj());
		proposta.setEntregaPesquisaEmail(sdtProposta.getEntregaPesquisaEmail());
		proposta.setEnviaEmail(sdtProposta.getEnviaEmail());
		proposta.setEstoqueTipo(sdtProposta.getEstoqueTipo());
		proposta.setLiberacaoExtra(sdtProposta.getLiberacaoExtra());
		proposta.setPropostaCodigo(sdtProposta.getPropostaCodigo());
		proposta.setPropostaObservacao(sdtProposta.getPropostaObservacao());
		proposta.setPropostaValor(sdtProposta.getPropostaValor());
		proposta.setUnidade(unidadeOrganizacionalService.obterPorCnpj(sdtProposta.getEmpresaCnpj()));

		// if (sdtProposta.getEstoqueTipo().equals("VN")) { -JA CODIFIQUEI NO HTML
		// // operacao.setDepartamento(EnumDepartamentoSvs.NOVOS);
		// } else if (sdtProposta.getEstoqueTipo().equals("VU")) {
		// // operacao.setDepartamento(EnumDepartamentoSvs.SEMINOVOS);
		// } else if (sdtProposta.getEstoqueTipo().equals("VD")) {
		// // operacao.setDepartamento(EnumDepartamentoSvs.VENDA_DIRETA);
		// }

		proposta.setVeiculo(veiculoPropostaService.montarObjetoVeiculo(sdtProposta.getDadosVeiculo()));
		proposta.getVeiculo().getOpcionais().addAll(opcionalVeiculoService.obterOpcionaisDoDealerQueNaoEstaoNaProposta(
				proposta.getPropostaCodigo(), proposta.getVeiculo().getOpcionais()));

//		if (proposta.getVeiculo().getDescricaoMarca() != null
//				&& !proposta.getVeiculo().getDescricaoMarca().trim().equals("")) {
//			marcaSantanderService.buscarMarcaSantanderNoBancoCorporativo(proposta.getVeiculo().getDescricaoMarca());
//			 operacao.setMarcaSantander(buscarMarcaSantander(proposta.getVeiculo().getDescricaoMarca()));
//		} -- PASSEI PRO FRONT
		return proposta;
	}
	


	

}
