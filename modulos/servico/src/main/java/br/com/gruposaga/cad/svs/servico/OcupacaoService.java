package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.Ocupacao;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OcupacaoService extends Service<Ocupacao> {

	@Inject
	private CrudService crudService;

	@Inject
	private EnderecoService enderecoService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	public void beforeSave(Ocupacao ocupacao) {
		super.beforeSave(ocupacao);
		enderecoService.beforeSave(ocupacao.getEndereco());

	}

	@Override
	protected Class<Ocupacao> getClassOfT() {
		return Ocupacao.class;
	}

}
