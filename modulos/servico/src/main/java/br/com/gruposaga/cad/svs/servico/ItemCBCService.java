package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.ItemCBC;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ItemCBCService extends Service<ItemCBC> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<ItemCBC> getClassOfT() {
		return ItemCBC.class;
	}
	
	public List<ItemCBC> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<ItemCBC> retorno = crudService.findWithNamedQuery(ItemCBC.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME,
				QueryParameter.with(ItemCBC.NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA,
						idOperacaoFinanciada).parameters());
		return retorno;
	}

}
