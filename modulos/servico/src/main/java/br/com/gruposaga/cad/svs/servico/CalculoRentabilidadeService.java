
package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.enums.EnumStatusAprovacaoBancaria;
import com.svs.fin.model.entities.AnexoCad;
import com.svs.fin.model.entities.CalculoRentabilidade;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CalculoRentabilidadeService extends Service<CalculoRentabilidade> {

	@Inject
	private CrudService crudService;
	@Inject
	private AnexosCadService anexosCadService;
	@Inject
	private OperacaoFinanciadaService operacaoFinanciadaService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<CalculoRentabilidade> getClassOfT() {
		return CalculoRentabilidade.class;
	}
	
	public void alterarStatusCalculoRentabilidade(Long idCalculoRentabilidade, EnumStatusAprovacaoBancaria novoStatus) {
		CalculoRentabilidade calculoRentabilidade = obterPorId(idCalculoRentabilidade);
		calculoRentabilidade.setStatus(novoStatus);
		alterar(calculoRentabilidade);
	}
	
	@Override
	public void beforeSave(CalculoRentabilidade calculoRentabilidade) {
		if(calculoRentabilidade.getOperacaoFinanciada() != null && calculoRentabilidade.getOperacaoFinanciada().getId() != null) {
			calculoRentabilidade.setOperacaoFinanciada(operacaoFinanciadaService.obterPorId(calculoRentabilidade.getOperacaoFinanciada().getId()));
		}
		
		List<AnexoCad> novaListaAnexos = new ArrayList<AnexoCad>();
		calculoRentabilidade.getAnexos().forEach(t -> {
			AnexoCad anexo = anexosCadService.obterPorId(t.getId());
			if (anexo != null) {
				anexo.setCalculoRentabilidade(calculoRentabilidade);
				novaListaAnexos.add(anexo);
			}
		});
		
		calculoRentabilidade.setAnexos(novaListaAnexos);		
		
		super.beforeSave(calculoRentabilidade);
	}
	
	public List<CalculoRentabilidade> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<CalculoRentabilidade> retorno = crudService.findWithNamedQuery(CalculoRentabilidade.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME,
				QueryParameter.with(CalculoRentabilidade.NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA,
						idOperacaoFinanciada).parameters());
		return retorno;
	}

}
