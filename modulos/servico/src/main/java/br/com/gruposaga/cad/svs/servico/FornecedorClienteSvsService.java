package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.FornecedorClienteSvs;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class FornecedorClienteSvsService extends Service<FornecedorClienteSvs> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<FornecedorClienteSvs> getClassOfT() {
		return FornecedorClienteSvs.class;
	}

	public List<FornecedorClienteSvs> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(FornecedorClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NAME, QueryParameter
				.with(FornecedorClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
