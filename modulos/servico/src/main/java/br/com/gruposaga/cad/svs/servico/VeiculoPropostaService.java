package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.VeiculoProposta;

import br.com.gruposaga.cad.svs.dao.dealer.ExtracoesSysDealerWorkFlow;
import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta.DadosVeiculo;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class VeiculoPropostaService extends Service<VeiculoProposta> {

	@Inject
	private CrudService crudService;

	@Inject
	private ExtracoesSysDealerWorkFlow extracoesSysDealerWorkFlow;
	
	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<VeiculoProposta> getClassOfT() {
		return VeiculoProposta.class;
	}
	
	public VeiculoProposta montarObjetoVeiculo(DadosVeiculo dadosVeiculo) {
		VeiculoProposta veiculo = new VeiculoProposta();
		veiculo.setChassi(dadosVeiculo.getChassi());
		veiculo.setCodigoModeloFipe(dadosVeiculo.getCodigoModeloFipe());
		veiculo.setCodigoModeloMarca(dadosVeiculo.getCodigoModeloMarca());
		veiculo.setCodigoModeloMolicar(dadosVeiculo.getCodigoModeloMolicar());
		veiculo.setDescricaoModelo(dadosVeiculo.getDescricaoModelo());
		veiculo.setPlaca(dadosVeiculo.getPlaca());
		veiculo.setVeiculoAnoFabricacao(dadosVeiculo.getVeiculoAnoFabricacao());
		veiculo.setVeiculoAnoModelo(dadosVeiculo.getVeiculoAnoModelo());
		veiculo.setVeiculoCodigo(dadosVeiculo.getVeiculoCodigo());
		veiculo.setVeiculoCorCodExterna(dadosVeiculo.getVeiculoCorCodExterna());
		veiculo.setVeiculoEstadoCodPlaca(dadosVeiculo.getVeiculoEstadoCodPlaca());
		veiculo.setVeiculoEstadoNomePlaca(dadosVeiculo.getVeiculoEstadoNomePlaca());
		veiculo.setVeiculoKm(dadosVeiculo.getVeiculoKm());
		veiculo.setVeiculoModeloVeiculoCod(dadosVeiculo.getVeiculoModeloVeiculoCod());
		veiculo.setVeiculoMunicipioNomePlacaIbge(dadosVeiculo.getVeiculoMunicipioNomePlacaIbge());
		veiculo.setVeiculoNrRenavam(dadosVeiculo.getVeiculoNrRenavam());
		veiculo.setVeiculoStatus(dadosVeiculo.getVeiculoStatus());

		veiculo.setCor(extracoesSysDealerWorkFlow.getDescricaoCor(dadosVeiculo.getVeiculoCorCodExterna()));
		veiculo.setDescricaoMarca(extracoesSysDealerWorkFlow.getDescricaoMarcaVeiculo(dadosVeiculo.getVeiculoCodigo()));

		if (dadosVeiculo.getVeiculoAnoFabricacao() == 0 || dadosVeiculo.getVeiculoAnoModelo() == 0) {
			veiculo.setAnoInformadoManualmente(Boolean.TRUE);
		}
		
		return veiculo;
	}

}
