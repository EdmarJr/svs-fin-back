package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.AndamentoOperacao;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AndamentoOperacaoService extends Service<AndamentoOperacao> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<AndamentoOperacao> getClassOfT() {
		return AndamentoOperacao.class;
	}
	
	public List<AndamentoOperacao> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<AndamentoOperacao> retorno = crudService.findWithNamedQuery(AndamentoOperacao.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME,
				QueryParameter.with(AndamentoOperacao.NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA,
						idOperacaoFinanciada).parameters());
		return retorno;
	}

}
