package br.com.gruposaga.cad.svs.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UtilsData {

	public static LocalDateTime toLocalDateTime(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		TimeZone tz = calendar.getTimeZone();
		ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
		return LocalDateTime.ofInstant(calendar.toInstant(), zid);
	}

	public static Calendar toCalendar(LocalDateTime dateTime) {
		Calendar cal = Calendar.getInstance();
		Instant instant = dateTime.atZone(ZoneId.systemDefault()).toInstant();
		cal.setTime(Date.from(instant));
		return cal;
	}
	
	
	public static boolean seTextoTemFormatoDeDataPtBr(String texto) {
		return UtilsRegex.seStringMatchRegex("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]", texto);
	}

	public static Calendar toCalendar(LocalDate date) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		return cal;

	}
}
