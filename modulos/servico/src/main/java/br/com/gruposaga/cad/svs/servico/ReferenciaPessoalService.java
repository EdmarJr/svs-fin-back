package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.ReferenciaPessoal;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
public class ReferenciaPessoalService extends Service<ReferenciaPessoal> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<ReferenciaPessoal> getClassOfT() {
		return ReferenciaPessoal.class;
	}

	public List<ReferenciaPessoal> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(ReferenciaPessoal.NQ_OBTER_POR_CLIENTE_SVS.NAME,
				QueryParameter.with(ReferenciaPessoal.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
