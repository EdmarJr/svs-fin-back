package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.ImovelPatrimonio;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ImovelPatrimonioService extends Service<ImovelPatrimonio> {

	@Inject
	private CrudService crudService;

	@Inject
	private EnderecoService enderecoService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	public void beforeSave(ImovelPatrimonio t) {
		super.beforeSave(t);
		enderecoService.beforeSave(t.getEndereco());
	}

	@Override
	protected Class<ImovelPatrimonio> getClassOfT() {
		return ImovelPatrimonio.class;
	}

	public List<ImovelPatrimonio> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(ImovelPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.NAME, QueryParameter
				.with(ImovelPatrimonio.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
