package br.com.gruposaga.cad.svs.servico.integracaoitau;

import javax.inject.Inject;

import com.svs.fin.integracaoItau.JsonCreditAnalysisItau;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.servico.Service;

public class JsonCreditAnalysisItauService extends Service<JsonCreditAnalysisItau> {
	
	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<JsonCreditAnalysisItau> getClassOfT() {
		return JsonCreditAnalysisItau.class;
	}

}
