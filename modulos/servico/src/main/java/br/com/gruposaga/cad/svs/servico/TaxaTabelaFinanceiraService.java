package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.TaxaTabelaFinanceira;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
public class TaxaTabelaFinanceiraService extends Service<TaxaTabelaFinanceira> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<TaxaTabelaFinanceira> getClassOfT() {
		return TaxaTabelaFinanceira.class;
	}

	public List<TaxaTabelaFinanceira> obterPorIdTabelaFinaceira(Long id) {
		List<TaxaTabelaFinanceira> retorno = crudService
				.findWithNamedQuery(TaxaTabelaFinanceira.NQ_OBTER_POR_TABELA_FINANCEIRA.NAME,
						QueryParameter
								.with(TaxaTabelaFinanceira.NQ_OBTER_POR_TABELA_FINANCEIRA.NM_PM_TABELA_FINANCEIRA, id)
								.parameters());

		return retorno;
	}

}
