package br.com.gruposaga.cad.svs.servico.integracao.ws.safra;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.svs.fin.integracao.safra.model.DadosBasicosSafra;

import br.com.gruposaga.cad.cpo.Cpo;
import br.com.gruposaga.cad.svs.utils.UtilsReflection;

@Startup
@Singleton
public class BatchIntegracaoDadosSafra {
	@Resource
	private TimerService timerService;

	@Cpo
	@Inject
	public EntityManager em;

	@PostConstruct
	public void posConstrucao() {
		// this.agenda();
		// this.verificacaoPeriodicaSeHaNovasContas(null);
		integrarDados();
	}

	public void integrarDados() {
		try {
			DadosBasicosSafra dados_Safra = SafraUtils.getInstanceWsCapturaPropostas().dados_Safra(SafraUtils.DS_HASH);
			Class<? extends DadosBasicosSafra> classDadosBasicosSafra = dados_Safra.getClass();
			Field[] fields = classDadosBasicosSafra.getDeclaredFields();
			for (Field field : fields) {
				if (field.getName().equals("__equalsCalc") || field.getName().equals("__hashCodeCalc")
						|| field.getName().equals("typeDesc")) {
					continue;
				}
				Method metodo = UtilsReflection.obterMetodoGetCorrespondenteAoField(field, classDadosBasicosSafra);
				if (metodo == null) {
					continue;
				}
				Object[] objetos = (Object[]) metodo.invoke(dados_Safra);
				for (Object objeto : objetos) {
					em.merge(objeto);
				}

			}
			// em.merge(entity)
			// dados_Safra.
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
