package br.com.gruposaga.cad.svs.dao.dealer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.CalendarType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import com.svs.fin.enums.EnumEstados;
import com.svs.fin.enums.EnumOrgaoEmissorDocumento;
import com.svs.fin.model.entities.ClienteSvs;
import com.svs.fin.model.entities.Endereco;
import com.svs.fin.model.entities.OpcionalVeiculo;

import br.com.gruposaga.cad.svs.utils.UtilsMascara;
import br.com.gruposaga.cad.svs.utils.UtilsSO;

@Stateless(description = "Extra��es Dealernet Workflow")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ExtracoesSysDealerWorkFlow {

	// @Inject
	// @TenantDealerWfApplication
	// EntityManager sqlWKfApl;
	//
	// @Inject
	// @TenantDealerWfHomologacao
	// EntityManager sqlWKfHomologacao;
	//
	// @Inject
	// @TenantDealerWf
	// EntityManager sqlWKf;

	@PersistenceContext(unitName = "Dealer")
	public EntityManager em;

	String bancoDealerWrk = "Saga_DealernetWF_homologacao";

	private Session session;

	public String getBancoDealerWkf() {
		// try {
		// if(Uteis.BANCO_DADOS_EXECUCAO.equalsIgnoreCase("CPOTESTE")){ //TODO FAZER
		// DIFERENCIACAO DE BANCOS
		// return "Saga_DealernetWF_homologacao";
		// }else{
		// return "Saga_DealernetWF";
		// }
		// }catch(Exception e){
		// e.printStackTrace();
		// return "Saga_DealernetWF_homologacao";
		// }
		return "Saga_DealernetWF_homologacao";
	}

	public EntityManager obterEntityManagerCorreto(String bancoDealerWrk) {
		return getSqlWKfHomologacao();
		// TODO DIFERENCIAR BANCOS
		// if (bancoDealerWrk.toUpperCase().contains("HOMOLOGACAO")) {
		// session = (Session) getSqlWKfHomologacao().getDelegate();
		// } else {
		// session = (Session) getSqlWKfApl().getDelegate();
		// }
	}

	public Boolean verificarSeEnderecoJaEstaCadastrado(Endereco endereco, String codigoPessoa, String bancoDealerWrk) {

		StringBuilder sbSql = new StringBuilder();

		sbSql.append("select count(*) as qtde from ").append(bancoDealerWrk)
				.append(".dbo.PessoaEndereco where PessoaEndereco_TipoEnderecoCod = :tipoEnderecoCod ");
		sbSql.append(" and Pessoa_Codigo = :pessoaCodigo");

		Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

		SQLQuery q = session.createSQLQuery(sbSql.toString());
		q.setLong("tipoEnderecoCod", endereco.getTipoEndereco().getCodigoDealerWorklow());
		q.setLong("pessoaCodigo", new Long(codigoPessoa));

		q.addScalar("qtde", IntegerType.INSTANCE);
		q.setMaxResults(1);

		Integer result = (Integer) q.uniqueResult();
		session.clear();

		if (result > 0) {
			return true;
		}

		return false;
	}
	
	public String getDescricaoMarcaVeiculo(String codVeiculo) {

        StringBuilder sbSql = new StringBuilder();

        sbSql.append("select m.Marca_Descricao as marca_descricao from ").append(bancoDealerWrk)
                .append(".dbo.Veiculo v ")
                .append(" left join ").append(bancoDealerWrk).append(".dbo.ModeloVeiculo mv on mv.ModeloVeiculo_Codigo = v.Veiculo_ModeloVeiculoCod ")
                .append(" left join ").append(bancoDealerWrk).append(".dbo.Marca m on m.Marca_Codigo = mv.ModeloVeiculo_MarcaCod ")
                .append(" where v.veiculo_codigo = ").append(codVeiculo);

        Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

        SQLQuery q = session.createSQLQuery(sbSql.toString());
        q.addScalar("marca_descricao", StringType.INSTANCE);
        Object result = q.uniqueResult();
        session.clear();

        if (result != null) {
            return (String) result;
        }
        return null;
    }
	
	public String getDescricaoCor(String codigoCor) {

		Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

        StringBuilder sbSql = new StringBuilder();

        sbSql.append("select Cor_Descricao as cor");
        sbSql.append(" from ").append(bancoDealerWrk).append(".dbo.Cor");
        sbSql.append(" where Cor_Codigo = '").append(codigoCor).append("'");

        SQLQuery q = session.createSQLQuery(sbSql.toString());
        q.addScalar("cor", StringType.INSTANCE);
        Object result = q.uniqueResult();
        session.clear();

        if (result != null) {
            return (String) result;
        }
        return null;
    }

	public Long getCodigoMunicipio(String codigoIbge, String bancoDealerWrk) {

		StringBuilder sbSql = new StringBuilder();

		sbSql.append("select municipio_codigo codigo from ").append(bancoDealerWrk)
				.append(".dbo.Municipio where Municipio_IBGE like '");
		sbSql.append(codigoIbge).append("'");

		/**
		 * Devido a cadastro errado no Dealer, é necessário fazer o tratamento
		 */
		if (codigoIbge.trim().equals("5300108")) {
			sbSql.append(" and municipio_nome like 'BRASILIA'");
		}

		Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

		SQLQuery q = session.createSQLQuery(sbSql.toString());
		q.setMaxResults(1);

		q.addScalar("codigo", LongType.INSTANCE);

		Long result = (Long) q.uniqueResult();

		return result;
	}

	public void inserirEnderecosNaoExistentesNoCliente(ClienteSvs cliente) throws Exception {

		SimpleDateFormat sdf = null;
		if (!UtilsSO.getSistemaOperacional().contains("WIN")) {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} else {
			sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		}

		ClienteSvs clienteDealer = getClienteSvsByCpfCnpj(UtilsMascara.removeCaracteresCpfCnpj(cliente.getCpfCnpj()));

		if (clienteDealer != null && clienteDealer.getCodigoDealerWorkflow() != null
				&& !clienteDealer.getCodigoDealerWorkflow().trim().equals("")) {

			for (Endereco endereco : cliente.getEnderecos()) {
				if (!verificarSeEnderecoJaEstaCadastrado(endereco, clienteDealer.getCodigoDealerWorkflow(),
						bancoDealerWrk)) {

					StringBuilder sSql = new StringBuilder();

					sSql.append(" INSERT INTO " + bancoDealerWrk + ".dbo.PessoaEndereco ");
					sSql.append(" (Pessoa_Codigo,");
					sSql.append("  PessoaEndereco_DataCriacao, ");
					sSql.append("  PessoaEndereco_TipoEnderecoCod, ");
					sSql.append("  PessoaEndereco_Cep, ");
					sSql.append("  TipoLogradouro_Codigo, ");
					sSql.append("  PessoaEndereco_Logradouro, ");
					sSql.append("  PessoaEndereco_Numero, ");
					sSql.append("  PessoaEndereco_Complemento, ");
					sSql.append("  PessoaEndereco_EstadoCod, ");
					sSql.append("  PessoaEndereco_MunicipioCod, ");
					sSql.append("  PessoaEndereco_Bairro, ");
					sSql.append("  PessoaEndereco_PontoReferencia, ");
					sSql.append("  PessoaEndereco_UsuCodCriacao, ");
					sSql.append("  PessoaEndereco_Status) ");

					sSql.append(" values(");

					sSql.append(clienteDealer.getCodigoDealerWorkflow()).append(","); // Pessoa_Codigo
					sSql.append("'").append(sdf.format(Calendar.getInstance().getTime())).append("',"); // PessoaEndereco_DataCriacao
					sSql.append(endereco.getTipoEndereco().getCodigoDealerWorklow()).append(","); // PessoaEndereco_TipoEnderecoCod
					sSql.append("'").append(endereco.getCep()).append("',"); // PessoaEndereco_Cep
					sSql.append("'").append(endereco.getTipoLogradouro().getCodigoDealerWorkflow()).append("',"); // TipoLogradouro_Codigo
					sSql.append("'").append(endereco.getLogradouro()).append("',"); // PessoaEndereco_Logradouro
					sSql.append("'").append(endereco.getNumero()).append("',"); // PessoaEndereco_Numero
					sSql.append("'").append(endereco.getComplemento()).append("',"); // PessoaEndereco_Complemento
					sSql.append("'").append(endereco.getMunicipio().getEstado().getSigla()).append("',"); // PessoaEndereco_EstadoCod
					sSql.append("'").append(getCodigoMunicipio(endereco.getMunicipio().getCodigoIbge(), bancoDealerWrk))
							.append("',"); // PessoaEndereco_MunicipioCod
					sSql.append("'").append(endereco.getBairro()).append("',"); // PessoaEndereco_Bairro
					sSql.append("'',"); // PessoaEndereco_PontoReferencia
					sSql.append("1,"); // PessoaEndereco_UsuCodCriacao
					sSql.append("1)"); // PessoaEndereco_Status

					Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

					try {
						session.createSQLQuery(sSql.toString()).executeUpdate();
						session.flush();
						session.clear();

					} catch (Exception e) {
						throw e;
					}
				}
			}
		}
	}

	public Boolean verificarSeRGJaEstaCadastrado(String codigoPessoa, String bancoDealerWrk) {

		StringBuilder sbSql = new StringBuilder();

		sbSql.append("select count(*) as qtde from ").append(bancoDealerWrk)
				.append(".dbo.PessoaDoc where Pessoa_Codigo = :pessoaCodigo and PessoaDoc_Tipo like 'RG%'");

		Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

		SQLQuery q = session.createSQLQuery(sbSql.toString());
		q.setLong("pessoaCodigo", new Long(codigoPessoa));

		q.addScalar("qtde", IntegerType.INSTANCE);

		Integer result = (Integer) q.uniqueResult();
		session.clear();

		if (result > 0) {
			return true;
		}

		return false;
	}

	public void inserirRGNoCliente(ClienteSvs cliente) throws Exception {

		if (cliente.getRg() != null && !cliente.getRg().trim().equals("")) {

			ClienteSvs clienteDealer = getClienteSvsByCpfCnpj(
					UtilsMascara.removeCaracteresCpfCnpj(cliente.getCpfCnpj()));

			if (clienteDealer != null && clienteDealer.getCodigoDealerWorkflow() != null
					&& !clienteDealer.getCodigoDealerWorkflow().trim().equals("")
					&& !verificarSeRGJaEstaCadastrado(clienteDealer.getCodigoDealerWorkflow(), bancoDealerWrk)) {

				String ufOrgaoEmissorRg = null;

				if (cliente.getUfOrgaoEmissorDocRg() != null) {
					ufOrgaoEmissorRg = cliente.getUfOrgaoEmissorDocRg().toString();
				} else {
					ufOrgaoEmissorRg = cliente.getUfOrgaoEmissorRg() != null ? cliente.getUfOrgaoEmissorRg() : "";
				}

				String orgaoEmissorRg = null;
				if (cliente.getOrgaoEmissorDocRg() != null) {
					orgaoEmissorRg = cliente.getOrgaoEmissorDocRg().toString();
				} else {
					orgaoEmissorRg = cliente.getOrgaoEmissorRg() != null ? cliente.getOrgaoEmissorRg() : "";
				}

				StringBuilder sSql = new StringBuilder();

				sSql.append(" INSERT INTO " + bancoDealerWrk + ".dbo.PessoaDoc ");
				sSql.append(" (Pessoa_Codigo,");
				sSql.append("  PessoaDoc_Tipo, ");
				sSql.append("  PessoaDoc_Numero, ");
				sSql.append("  PessoaDoc_UF, ");
				sSql.append("  PessoaDoc_OrgaoEmissor) ");

				sSql.append(" values(");

				sSql.append(clienteDealer.getCodigoDealerWorkflow()).append(","); // Pessoa_Codigo
				sSql.append("'RG',"); // PessoaDoc_Tipo
				sSql.append("'" + cliente.getRg().trim()).append("',"); // PessoaDoc_Numero
				sSql.append("'" + ufOrgaoEmissorRg).append("',"); // PessoaDoc_UF
				sSql.append("'" + orgaoEmissorRg).append("'"); // PessoaDoc_OrgaoEmissor
				sSql.append(")");

				Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

				try {
					session.createSQLQuery(sSql.toString()).executeUpdate();
					session.flush();
					session.clear();

				} catch (Exception e) {
					throw e;
				}
			}
		}
	}
	
	public List<OpcionalVeiculo> listarOpcionaisVeiculo(Long idProposta) {

        StringBuilder sql = new StringBuilder();

        sql.append("select VeiculoOpcional_OpcionalCod opcionalCodigo, ");
        sql.append(" Opcional_Descricao as opcionalDescricao from ").append(bancoDealerWrk).append(".dbo.VeiculoOpcional vo ");
        sql.append(" left join ").append(bancoDealerWrk).append(".dbo.Opcional op on vo.VeiculoOpcional_OpcionalCod = op.Opcional_Codigo ");
        sql.append(" left join ").append(bancoDealerWrk).append(".dbo.Proposta prop on prop.Proposta_VeiculoCod = vo.Veiculo_Codigo ");
        sql.append(" where prop.Proposta_Codigo = ");
        sql.append(idProposta);

        Session session = (Session) obterEntityManagerCorreto(bancoDealerWrk).getDelegate();

        SQLQuery q = session.createSQLQuery(sql.toString());
        q.addScalar("opcionalCodigo", LongType.INSTANCE);
        q.addScalar("opcionalDescricao", StringType.INSTANCE);

        q.setResultTransformer(Transformers.aliasToBean(OpcionalVeiculo.class));

        List<OpcionalVeiculo> lista = new ArrayList<OpcionalVeiculo>(q.list());
        session.clear();

        return lista;
    }

	public ClienteSvs getClienteSvsByCpfCnpj(String cpfCnpj) {

		StringBuilder sSql = new StringBuilder();

		sSql.append("select p.Pessoa_Nome as nome, ");
		sSql.append("p.Pessoa_NomeFantasia as nomeFantasia, ");
		sSql.append("p.Pessoa_Sexo as sexoString, ");
		sSql.append("p.Pessoa_DataNascimentoFundacao as dataNascimento, ");
		sSql.append("p.Pessoa_TipoPessoa as tipoPessoaString, ");
		sSql.append("p.Pessoa_Codigo as codigoDealerWorkflow, ");
		sSql.append("p.Pessoa_Email as email, ");
		sSql.append("ec.EstadoCivil_Descricao as estadoCivilString, ");
		sSql.append("pd.PessoaDoc_Numero as rg, ");
		sSql.append("pd.PessoaDoc_OrgaoEmissor as orgaoEmissorRg, ");
		sSql.append("pd.PessoaDoc_UF as ufOrgaoEmissorRg ");
		sSql.append("from " + bancoDealerWrk + ".dbo.Pessoa p ");
		sSql.append(" left join " + bancoDealerWrk
				+ ".dbo.EstadoCivil ec on ec.EstadoCivil_Codigo = p.EstadoCivil_Codigo ");
		sSql.append(" left join " + bancoDealerWrk
				+ ".dbo.PessoaDoc pd on (pd.Pessoa_Codigo = p.Pessoa_Codigo and pd.PessoaDoc_Tipo like 'RG%') ");
		sSql.append("where p.Pessoa_DocIdentificador like '");
		sSql.append(cpfCnpj);
		sSql.append("'");

		Session session = null;

		session = (Session) obterEntityManagerCorreto(getBancoDealerWkf()).getDelegate();
		// if (bancoDealerWrk.toUpperCase().contains("HOMOLOGACAO")) { //TODO VALIDAR
		// BANCO PRODUCAO E HOMOLOGACAO
		// session = (Session) getSqlWKfHomologacao().getDelegate();
		// } else {
		// session = (Session) getSqlWKfApl().getDelegate();
		// }

		SQLQuery q = session.createSQLQuery(sSql.toString());
		q.addScalar("nome", StringType.INSTANCE);
		q.addScalar("dataNascimento", CalendarType.INSTANCE);
		q.addScalar("nomeFantasia", StringType.INSTANCE);
		q.addScalar("sexoString", StringType.INSTANCE);
		q.addScalar("tipoPessoaString", StringType.INSTANCE);
		q.addScalar("codigoDealerWorkflow", StringType.INSTANCE);
		q.addScalar("email", StringType.INSTANCE);
		q.addScalar("estadoCivilString", StringType.INSTANCE);
		q.addScalar("rg", StringType.INSTANCE);
		q.addScalar("orgaoEmissorRg", StringType.INSTANCE);
		q.addScalar("ufOrgaoEmissorRg", StringType.INSTANCE);

		q.setResultTransformer(Transformers.aliasToBean(ClienteSvs.class));

		List<ClienteSvs> result = q.list();

		if (result != null && result.size() > 0) {

			ClienteSvs cliente = result.get(0);

			if (cliente.getOrgaoEmissorRg() != null && cliente.getOrgaoEmissorRg().trim().equals("")) {
					String orgaoEmissorString = cliente.getOrgaoEmissorRg().toUpperCase();
					if (cliente.getOrgaoEmissorRg().contains(" ")) {
						orgaoEmissorString = cliente.getOrgaoEmissorRg().split(" ")[0].toUpperCase();
					} else if (cliente.getOrgaoEmissorRg().startsWith("SSP")) {
						orgaoEmissorString = "SSP";
					} else if (cliente.getOrgaoEmissorRg().startsWith("SPTC")) {
						orgaoEmissorString = "SPTC";
					} else if (cliente.getOrgaoEmissorRg().startsWith("DGPC")) {
						orgaoEmissorString = "DGPC";
					} else if (cliente.getOrgaoEmissorRg().startsWith("SESP")) {
						orgaoEmissorString = "SESP";
					} else if (cliente.getOrgaoEmissorRg().startsWith("GEJ")) {
						orgaoEmissorString = "GEJ";
					}
					EnumOrgaoEmissorDocumento orgaoEmissorDocumento = EnumOrgaoEmissorDocumento
							.valueOf(orgaoEmissorString);
					if (orgaoEmissorDocumento != null) {
						cliente.setOrgaoEmissorDocRg(orgaoEmissorDocumento);
					}
			}

			if (cliente.getUfOrgaoEmissorRg() != null && cliente.getUfOrgaoEmissorRg().trim().equals("")) {
				try {
					String orgaoEmissorString = cliente.getUfOrgaoEmissorRg().toUpperCase();
					EnumEstados uf = EnumEstados.valueOf(orgaoEmissorString);
					if (uf != null) {
						cliente.setUfOrgaoEmissorDocRg(uf);
					}
				} catch (Exception e) {
				}
			}

			return cliente;
		}
		return null;
	}

	private EntityManager getSqlWKfHomologacao() {
		return em;
	}
}
