package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.MotivoAprovacao;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class MotivoAprovacaoService extends Service<MotivoAprovacao> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<MotivoAprovacao> getClassOfT() {
		return MotivoAprovacao.class;
	}

}
