package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.CnpjInstituicaoFinanceira;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CnpjInstituicaoFinanceiraService extends Service<CnpjInstituicaoFinanceira> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	public List<CnpjInstituicaoFinanceira> obterPorIdInstituicaoFinaceira(Long id) {
		List<CnpjInstituicaoFinanceira> retorno = crudService.findWithNamedQuery(
				CnpjInstituicaoFinanceira.NQ_OBTER_POR_INSTITUICAO_FINANCEIRA.NAME,
				QueryParameter.with(
						CnpjInstituicaoFinanceira.NQ_OBTER_POR_INSTITUICAO_FINANCEIRA.NM_PM_INSTITUICAO_FINANCEIRA, id)
						.parameters());

		return retorno;
	}

	@Override
	protected Class<CnpjInstituicaoFinanceira> getClassOfT() {
		return CnpjInstituicaoFinanceira.class;
	}

}
