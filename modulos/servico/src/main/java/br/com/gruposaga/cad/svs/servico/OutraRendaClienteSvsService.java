package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.OutraRendaClienteSvs;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
public class OutraRendaClienteSvsService extends Service<OutraRendaClienteSvs> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<OutraRendaClienteSvs> getClassOfT() {
		return OutraRendaClienteSvs.class;
	}

	public List<OutraRendaClienteSvs> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(OutraRendaClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NAME,
				QueryParameter.with(OutraRendaClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
