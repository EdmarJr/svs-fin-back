package br.com.gruposaga.cad.svs.exceptions;

public class ClienteDealerNaoSalvoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClienteDealerNaoSalvoException() {
		// TODO Auto-generated constructor stub
	}

	public ClienteDealerNaoSalvoException(String message) {
		super(message);
	}
}
