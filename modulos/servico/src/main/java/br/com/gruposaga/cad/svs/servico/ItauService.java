package br.com.gruposaga.cad.svs.servico;

import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.google.gson.Gson;
import com.svs.fin.enums.EnumBancoIntegracaoOnline;
import com.svs.fin.enums.EnumStatusAprovacaoBancaria;
import com.svs.fin.integracaoItau.FinanciamentoOnlineItau;
import com.svs.fin.integracaoItau.JsonCreditAnalysisItau;
import com.svs.fin.integracaoItau.JsonSimulationOperationResponseItau;
import com.svs.fin.integracaoItau.JsonTransactionRequestItau;
import com.svs.fin.model.entities.Avalista;
import com.svs.fin.model.entities.Banco;
import com.svs.fin.model.entities.CalculoRentabilidade;
import com.svs.fin.model.entities.ClienteSvs;
import com.svs.fin.model.entities.DeParaBancoZFlow;
import com.svs.fin.model.entities.MascaraTaxaTabelaFinanceira;
import com.svs.fin.model.entities.OperacaoFinanciada;
import com.svs.fin.utils.IntegracaoZFlowUtil;

import br.com.gruposaga.cad.svs.servico.integracaoitau.JsonCreditAnalysisItauService;
import br.com.gruposaga.sdk.zflow.model.gen.Client;
import br.com.gruposaga.sdk.zflow.model.gen.ClientReferences;
import br.com.gruposaga.sdk.zflow.model.gen.CreditAnalysis;
import br.com.gruposaga.sdk.zflow.model.gen.FinanceOperationOption;
import br.com.gruposaga.sdk.zflow.model.gen.FinanceOperationRequest;
import br.com.gruposaga.sdk.zflow.model.gen.FinanceTransaction;
import br.com.gruposaga.sdk.zflow.model.gen.SimulationOperationResponse;
import br.com.gruposaga.sdk.zflow.model.gen.enums.FinanceOperationTypeEnum;
import br.com.gruposaga.sdk.zflow.resource.TransactionResource;
import br.com.gruposaga.sdk.zflow.service.ZFlowResource;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ItauService {

	@Inject
	private ClienteSvsService clienteSvsService;
	@Inject
	private MascaraTaxaTabelaFinanceiraService mascaraTaxaTabelaFinanceiraService;
	@Inject
	private FinanciamentoOnlineItauService financiamentoOnlineItauService;
	@Inject
	private InstituicaoFinanceiraService instuicaoFinanceiraService;
	@Inject
	private ZFlowResource zFlowResource;
	@Inject
	private DeParaBancoZFlowService deParaBancoZFlowService;

	@Inject
	private JsonCreditAnalysisItauService jsonCreditAnalysisItauService;

	@Inject
	private OperacaoFinanciadaService operacaoFinanciadaService;

	public void enviarPropostaItau(String cpfCnpjCliente, FinanceOperationOption opcaoFinanciamento,
			FinanciamentoOnlineItau financiamentoItau) {
		JsonSimulationOperationResponseItau jsonResposta = financiamentoItau.getJsonSimulationOperationResponseItau();
		SimulationOperationResponse simulationResponse = new Gson().fromJson(jsonResposta.getJsonTexto(),
				SimulationOperationResponse.class);

		String id = simulationResponse.getId();

		JsonTransactionRequestItau jsonRequisicao = financiamentoItau.getJsonTransactionRequestItau();

		FinanceTransaction financeTransaction = new Gson().fromJson(jsonRequisicao.getJsonTexto(),
				FinanceTransaction.class);

		ClienteSvs clienteSvs = clienteSvsService.obterPorCpfCnpj(cpfCnpjCliente);
		Client clientZflow = financeTransaction.getClient();

		clientZflow.setClientReferences(new ClientReferences());

		IntegracaoZFlowUtil.popularDadosPessoais(clienteSvs, clientZflow);
		IntegracaoZFlowUtil.popularDadosProfissionais(clienteSvs, clientZflow);
		IntegracaoZFlowUtil.popularTelefones(clienteSvs, clientZflow);
		IntegracaoZFlowUtil.popularReferenciasPessoais(clienteSvs, clientZflow);
		IntegracaoZFlowUtil.popularReferenciaBancaria1(clienteSvs, clientZflow,
				getDeParaBanco(clienteSvs.getReferenciaBancaria1().getBanco()));
		IntegracaoZFlowUtil.popularReferenciaBancaria2(clienteSvs, clientZflow,
				getDeParaBanco(clienteSvs.getReferenciaBancaria2().getBanco()));
		IntegracaoZFlowUtil.popularPatrimonios(clienteSvs, clientZflow);
		IntegracaoZFlowUtil.popularDadosConjuge(clienteSvs, clientZflow);

		financeTransaction.getOperationRequest().setInstallments(opcaoFinanciamento.getInstallments());

		if (clienteSvs.getAvalistas() != null && clienteSvs.getAvalistas().size() > 0) {
			Client avalistaZFlow = getAvalistaZFlow(clienteSvs.getAvalistas().get(0));
			financeTransaction.setAvalist(avalistaZFlow);
		}

		TransactionResource transactionResource = getzFlowResource().proxy(TransactionResource.class);
		SimulationOperationResponse simulationOperationResponse = transactionResource.update(id, financeTransaction);

		jsonRequisicao.setFinanceTransaction(financeTransaction);
		jsonResposta.setSimulationOperationResponse(simulationOperationResponse);

		salvarHistoricoTransactionRequestItau(jsonRequisicao, jsonResposta);

		CreditAnalysis creditAnalysis = transactionResource.analysis(id);

		JsonCreditAnalysisItau jsonCreditAnalysis = financiamentoItau.getJsonCreditAnalysisItau();
		jsonCreditAnalysis.setCreditAnalysis(creditAnalysis);

		salvarHistoricoCreditAnalysisItau(jsonCreditAnalysis);
		salvarFinanciamentoItau(financiamentoItau);
	}

	public void realizarAnaliseItau(OperacaoFinanciada operacaoFinanciada) {
		removerFinanciamentosItau(operacaoFinanciada);
		removerCalculosBancoOnline(operacaoFinanciada, EnumBancoIntegracaoOnline.ITAU);

		for (int retorno = 0; retorno <= 3; retorno++) {
			FinanciamentoOnlineItau financiamento = new FinanciamentoOnlineItau();
			financiamento.setOperacaoFinanciada(operacaoFinanciada);
			fazerPreAnaliseItau(operacaoFinanciada, financiamento, retorno);
			salvarFinanciamentoItau(financiamento);
		}

		operacaoFinanciadaService.alterar(operacaoFinanciada);
	}

	public void fazerPreAnaliseItau(OperacaoFinanciada operacaoFinanciada, FinanciamentoOnlineItau financiamentoItau,
			int i) {
		ClienteSvs clienteSvs = operacaoFinanciada.getCliente();

		TransactionResource transactionResource = getzFlowResource().proxy(TransactionResource.class);

		Client client = new Client();
		client.setClientReferences(new ClientReferences());

		FinanceTransaction financeTransaction = new FinanceTransaction();
		financeTransaction.setClient(client);

		IntegracaoZFlowUtil.popularDadosProfissionais(clienteSvs, client);
		IntegracaoZFlowUtil.popularDadosPessoais(clienteSvs, client);
		IntegracaoZFlowUtil.popularTelefones(clienteSvs, client);
		IntegracaoZFlowUtil.popularReferenciasPessoais(clienteSvs, client);
		IntegracaoZFlowUtil.popularReferenciaBancaria1(clienteSvs, client,
				getDeParaBanco(clienteSvs.getReferenciaBancaria1().getBanco()));
		IntegracaoZFlowUtil.popularReferenciaBancaria2(clienteSvs, client,
				getDeParaBanco(clienteSvs.getReferenciaBancaria2().getBanco()));
		IntegracaoZFlowUtil.popularPatrimonios(clienteSvs, client);
		IntegracaoZFlowUtil.popularDadosConjuge(clienteSvs, client);

		FinanceOperationRequest operationRequest = new FinanceOperationRequest();
		operationRequest.setFinanceOperationType(FinanceOperationTypeEnum.DEFINED);

		IntegracaoZFlowUtil.popularInformacoesDaProposta(operacaoFinanciada, operationRequest);

		operationRequest.financeProtectionInsurance(Boolean.TRUE);
		operationRequest.setVehicleSellerRebatePercent(new Float(i));
		financeTransaction.setOperationRequest(operationRequest);
		SimulationOperationResponse simulationOperationResponse = transactionResource.persist(financeTransaction);

		JsonTransactionRequestItau jsonRequisicao = financiamentoItau.getJsonTransactionRequestItau();
		jsonRequisicao.setFinanceTransaction(financeTransaction);

		JsonSimulationOperationResponseItau jsonResposta = financiamentoItau.getJsonSimulationOperationResponseItau();
		jsonResposta.setSimulationOperationResponse(simulationOperationResponse);

		financiamentoItau.setIdOnline(jsonResposta.getSimulationOperationResponse().getId());

		salvarHistoricoTransactionRequestItau(jsonRequisicao, jsonResposta);

		for (FinanceOperationOption financeOption : jsonResposta.getSimulationOperationResponse()
				.getFinanceOperationDetails().getFinanceOperationOptions()) {
			operacaoFinanciada.getTodasRentabilidades().add(montarCalculoRentabilidadeItau(operacaoFinanciada, i,
					financeOption, jsonResposta.getSimulationOperationResponse().getId()));
		}

	}

	public void removerFinanciamentosItau(OperacaoFinanciada operacaoFinanciada) {
		List<FinanciamentoOnlineItau> financiamentos = financiamentoOnlineItauService
				.obterPorOperacaoFinanciada(operacaoFinanciada.getId());

		financiamentos.forEach(financiamento -> {
			financiamentoOnlineItauService.deletar(financiamento.getId());
		});

		operacaoFinanciada.setFinanciamentosOnlineItau(null);
	}

	public void removerCalculosBancoOnline(OperacaoFinanciada operacaoFinanciada, EnumBancoIntegracaoOnline banco) {
		List<CalculoRentabilidade> todasRentabilidadesFiltrada = operacaoFinanciada.getTodasRentabilidades().stream()
				.filter(rentabilidade -> !rentabilidade.getAprovacaoOnline()
						|| !rentabilidade.getBancoIntegracaoOnline().equals(banco))
				.collect(Collectors.toList());

		operacaoFinanciada.setTodasRentabilidades(todasRentabilidadesFiltrada);
	}

	public void salvarFinanciamentoItau(FinanciamentoOnlineItau financiamento) {
		financiamento
				.setOperacaoFinanciada(operacaoFinanciadaService.obterPorId(financiamento.getIdOperacaoFinanciada()));

		if (financiamento.getId() != null && financiamento.getId() > 0) {
			financiamentoOnlineItauService.alterar(financiamento);
		} else {
			financiamentoOnlineItauService.incluir(financiamento);
		}
	}

	public void salvarHistoricoTransactionRequestItau(JsonTransactionRequestItau jsonRequisicao,
			JsonSimulationOperationResponseItau jsonResposta) {
		FinanceTransaction financeTransaction = jsonRequisicao.getFinanceTransaction();

		Gson gson = new Gson();
		String jsonEnviado = gson.toJson(financeTransaction);

		jsonRequisicao.setCpfCliente(financeTransaction.getClient().getCpf().toString());
		jsonRequisicao.setNomeCliente(financeTransaction.getClient().getName());
		jsonRequisicao.setData(Calendar.getInstance());
		jsonRequisicao.setJsonByte(jsonEnviado.getBytes(Charset.forName("UTF-8")));

		SimulationOperationResponse simulationResponse = jsonResposta.getSimulationOperationResponse();

		String jsonRecebido = gson.toJson(simulationResponse);

		jsonResposta.setData(Calendar.getInstance());
		jsonResposta.setJsonByte(jsonRecebido.getBytes(Charset.forName("UTF-8")));
	}

	public CalculoRentabilidade montarCalculoRentabilidadeItau(OperacaoFinanciada operacaoFinanciada, int i,
			FinanceOperationOption financeOption, String idOnline) {

		Double valorEntrada = operacaoFinanciada.getValorEntradaItau() != null
				? operacaoFinanciada.getValorEntradaItau()
				: operacaoFinanciada.getValorEntrada();

		Double percentualEntrada = valorEntrada * 100 / operacaoFinanciada.getProposta().getPropostaValor();

		CalculoRentabilidade calculoRentabilidade = new CalculoRentabilidade();

		calculoRentabilidade.setAprovacaoOnline(Boolean.TRUE);
		calculoRentabilidade.setBancoIntegracaoOnline(EnumBancoIntegracaoOnline.ITAU);
		calculoRentabilidade.setEnviadoAoBanco(Boolean.TRUE);
		calculoRentabilidade.setOperacaoFinanciada(operacaoFinanciada);
		calculoRentabilidade.setParcelas(financeOption.getInstallments());
		calculoRentabilidade.setValorEntrada(valorEntrada);
		calculoRentabilidade.setValorFinanciado(operacaoFinanciada.getValorFinanciadoItau());
		calculoRentabilidade.setStatus(EnumStatusAprovacaoBancaria.NAO_ENVIADO);
		calculoRentabilidade.setValorPmt(new Double(financeOption.getVehicleInstallmentValue()));
		calculoRentabilidade.setPercEntrada(percentualEntrada);
		calculoRentabilidade
				.setInstituicaoFinanceira(instuicaoFinanceiraService.obterPorNome("BANCO ITAU UNIBANCO S/A"));

		MascaraTaxaTabelaFinanceira mascara = mascaraTaxaTabelaFinanceiraService.obterPorMascaraRetornoBanco(
				"Retorno " + i + " Itau", new Integer(i).toString(), EnumBancoIntegracaoOnline.ITAU);
		mascara = mascara != null ? mascara
				: mascaraTaxaTabelaFinanceiraService.incluir("Retorno " + i + " Itau", new Integer(i).toString(),
						EnumBancoIntegracaoOnline.ITAU);
		calculoRentabilidade.setMascaraTaxa(mascara);
		calculoRentabilidade.setIdFinanciamentoOnline(idOnline);

		if (calculoRentabilidade.getMascaraTaxa().getPercentualRetorno() != null) {
			calculoRentabilidade.setValorRetorno(operacaoFinanciada.getValorFinanciado()
					* (calculoRentabilidade.getMascaraTaxa().getPercentualRetorno() / 100));
		}

		return calculoRentabilidade;
	}

	public DeParaBancoZFlow getDeParaBanco(Banco banco) {

		if (banco == null) {
			return null;
		}

		return deParaBancoZFlowService.obterPorId(banco.getId());
	}

	public ZFlowResource getzFlowResource() {
		return zFlowResource;
	}

	public void setzFlowResource(ZFlowResource zFlowResource) {
		this.zFlowResource = zFlowResource;
	}

	private Client getAvalistaZFlow(Avalista avalista) {
		Client avalistaZFlow = new Client();

		IntegracaoZFlowUtil.popularDadosPessoais(avalista, avalistaZFlow);
		IntegracaoZFlowUtil.popularDadosProfissionais(avalista, avalistaZFlow);
		IntegracaoZFlowUtil.popularPatrimonios(avalista, avalistaZFlow);
		IntegracaoZFlowUtil.popularDadosConjuge(avalista, avalistaZFlow);

		return avalistaZFlow;
	}

	private void salvarHistoricoCreditAnalysisItau(JsonCreditAnalysisItau jsonCreditAnalysisItau) {
		CreditAnalysis financeTransaction = jsonCreditAnalysisItau.getCreditAnalysis();

		Gson gson = new Gson();
		String jsonEnviado = gson.toJson(financeTransaction);

		jsonCreditAnalysisItau.setData(Calendar.getInstance());
		jsonCreditAnalysisItau.setJsonByte(jsonEnviado.getBytes(Charset.forName("UTF-8")));

		jsonCreditAnalysisItauService.alterar(jsonCreditAnalysisItau);
	}

}
