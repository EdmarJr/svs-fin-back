package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.integracaoItau.FinanciamentoOnlineItau;
import com.svs.fin.model.entities.MascaraTaxaTabelaFinanceira;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class FinanciamentoOnlineItauService extends Service<FinanciamentoOnlineItau> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<FinanciamentoOnlineItau> getClassOfT() {
		return FinanciamentoOnlineItau.class;
	}	
	
	public List<FinanciamentoOnlineItau> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<FinanciamentoOnlineItau> retorno = crudService.findWithNamedQuery(FinanciamentoOnlineItau.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME,
				QueryParameter.with(FinanciamentoOnlineItau.NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA,
						idOperacaoFinanciada).parameters());
		return retorno;
	}

}
