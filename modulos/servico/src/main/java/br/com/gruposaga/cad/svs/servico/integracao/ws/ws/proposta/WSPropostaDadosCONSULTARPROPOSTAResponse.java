
package  br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Xml" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Msgretorno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xml",
    "msgretorno"
})
@XmlRootElement(name = "WS_PropostaDados.CONSULTARPROPOSTAResponse")
public class WSPropostaDadosCONSULTARPROPOSTAResponse {

    @XmlElement(name = "Xml", required = true)
    protected String xml;
    @XmlElement(name = "Msgretorno", required = true)
    protected String msgretorno;

    /**
     * Obt�m o valor da propriedade xml.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXml() {
        return xml;
    }

    /**
     * Define o valor da propriedade xml.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXml(String value) {
        this.xml = value;
    }

    /**
     * Obt�m o valor da propriedade msgretorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgretorno() {
        return msgretorno;
    }

    /**
     * Define o valor da propriedade msgretorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgretorno(String value) {
        this.msgretorno = value;
    }

}
