package br.com.gruposaga.cad.svs.servico;


import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.TipoTelefone;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class TipoTelefoneService extends Service<TipoTelefone>{
	
	@Inject
	private CrudService crudService;
	
	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<TipoTelefone> getClassOfT() {
		return TipoTelefone.class;
	}
	
	
	
	
}
