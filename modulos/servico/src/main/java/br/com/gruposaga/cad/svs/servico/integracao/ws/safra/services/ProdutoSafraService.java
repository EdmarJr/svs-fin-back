package br.com.gruposaga.cad.svs.servico.integracao.ws.safra.services;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.integracao.safra.model.Produto;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.servico.Service;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProdutoSafraService extends Service<Produto> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<Produto> getClassOfT() {
		return Produto.class;
	}

}
