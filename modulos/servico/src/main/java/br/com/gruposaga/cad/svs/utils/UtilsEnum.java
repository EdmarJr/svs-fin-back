package br.com.gruposaga.cad.svs.utils;

import com.svs.fin.enums.EnumLabeled;

public class UtilsEnum {
	public static <T extends EnumLabeled> T obterEnumLabeledPorLabel(T[] listaDeEnums, String textoLabel) {
		for (T enum1 : listaDeEnums) {
			if (enum1.getLabel().equals(textoLabel)) {
				return enum1;
			}
		}
		return null;
	}
}
