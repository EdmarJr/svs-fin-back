package br.com.gruposaga.cad.svs.dao.generic;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

import br.com.gruposaga.cad.cpo.Cpo;

@SuppressWarnings("serial")
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CrudService implements Serializable {

	@Cpo
	@Inject
	public EntityManager em;

	@Inject
	private PaginadorService paginadorService;

	@PostConstruct
	public void init() {
	}

	public <T> T create(T t) {
		this.em.persist(t);
		this.em.flush();
		this.em.refresh(t);
		return t;
	}

	public <T> T find(Class<T> type, Object id) {
		return (T) this.em.find(type, id);
	}

	public <T> void delete(Class<T> type, Object id) {
		Object ref = this.em.getReference(type, id);
		this.em.remove(ref);
	}

	public <T> void delete(Object object) {
		this.em.remove(object);
	}

	public <T> T update(T t) {
		return (T) this.em.merge(t);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}

	public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		return findWithNamedQuery(namedQueryName, parameters, 0);
	}

	public void executeNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		Query query = this.em.createNamedQuery(namedQueryName);

		atribuirParametrosNaQuery(parameters, query);

		query.executeUpdate();
	}

	private void atribuirParametrosNaQuery(Map<String, Object> parameters, Query query) {
		Iterator<Entry<String, Object>> entries = parameters.entrySet().iterator();
		while (entries.hasNext()) {
			Entry<String, Object> thisEntry = (Entry<String, Object>) entries.next();
			if (thisEntry.getValue() instanceof Collection<?>) {
				query.setParameter(thisEntry.getKey().toString(), (Collection) thisEntry.getValue());
			} else if (thisEntry.getValue() instanceof Object[]) {
				query.setParameter(thisEntry.getKey().toString(), Arrays.asList((Object[]) thisEntry.getValue()));
			} else {
				query.setParameter(thisEntry.getKey().toString(), thisEntry.getValue());
			}
		}
	}

	public <T> T obterPorId(Class<T> clazz, Object id) {
		T resultado = this.em.find(clazz, id);
		return resultado;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String queryName, int resultLimit) {
		return this.em.createNamedQuery(queryName).setMaxResults(resultLimit).getResultList();
	}

	public <T> T findSingleResultWithNamedQuery(String queryName, Map<String, Object> parameters) {
		List<T> resultList = findWithNamedQuery(queryName, parameters);
		return resultList.size() > 0 ? resultList.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findByNativeQuery(String sql, Class<T> type) {
		return this.em.createNativeQuery(sql, type).getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);

		atribuirParametrosNaQuery(parameters, query);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obterTodos(Class<T> clazz) {
		return paginadorService.obterPorParametros(clazz,
				GrupoDeParametrosParaListagemLazy.Builder.novoGrupo().build());
	}

	public <T> Long obterTotalDeLinhasPorParametros(Class<T> clazz,
			GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy) {
		return paginadorService.obterTotalDeLinhasPorParametros(clazz, grupoDeParametrosParaListagemLazy);
	}

	public <T> List<T> obterPorParametros(Class<T> clazz,
			GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy) {
		return paginadorService.obterPorParametros(clazz, grupoDeParametrosParaListagemLazy);
	}

	public <T> List<T> obterPorParametros(Class<T> clazz,
			GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy,
			String... listaDeParametrosParaFazerFetchEager) {
		return paginadorService.obterPorParametros(clazz, grupoDeParametrosParaListagemLazy,
				listaDeParametrosParaFazerFetchEager);
	}

}