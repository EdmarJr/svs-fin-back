package br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DadosVendedor {

	private String vendedorId;

	private String vendedorNome;

	private String vendedorCpf;

	@XmlElement(name = "VendedorId")
	public String getVendedorId() {
		return vendedorId;
	}

	public void setVendedorId(String vendedorId) {
		this.vendedorId = vendedorId;
	}

	@XmlElement(name = "VendedorNome")
	public String getVendedorNome() {
		return vendedorNome;
	}

	public void setVendedorNome(String vendedorNome) {
		this.vendedorNome = vendedorNome;
	}

	@XmlElement(name = "VendedorCPF")
	public String getVendedorCpf() {
		return vendedorCpf;
	}

	public void setVendedorCpf(String vendedorCpf) {
		this.vendedorCpf = vendedorCpf;
	}
}
