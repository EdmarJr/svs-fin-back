package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import com.svs.fin.model.dto.EntidadeTesteDto;
import com.svs.fin.model.dto.EntidadeTesteEdicaoDto;
import com.svs.fin.model.dto.ListParamsDto;
import com.svs.fin.modelo.mapper.EntidadeTesteEdicaoMapper;
import com.svs.fin.modelo.mapper.EntidadeTesteMapper;

import br.com.gruposaga.cad.svs.dao.DaoEntidadeTeste;
import br.com.gruposaga.cad.svs.dao.consultas.ConsultaEntidadeTeste;
import br.com.gruposaga.cad.svs.fin.modelo.pojo.EntidadeTesteConsultaPojo;
import teste.EntidadeTeste;
import teste.EntidadeTeste_;

public class ServicoEntidadeTeste {

	private @Inject DaoEntidadeTeste dao;
	private @Inject ConsultaEntidadeTeste consulta;

	@Transactional
	public void atualizar(Long idEntidadeTeste, EntidadeTesteEdicaoDto entidadeTesteDto, String cpfUsuario)
			throws Exception {

		EntidadeTeste emailBlacklist = dao.buscarPorId(idEntidadeTeste);
		EntidadeTesteEdicaoMapper.INSTANCE.atualizar(entidadeTesteDto, emailBlacklist);

		verificarExistencia(emailBlacklist);

		dao.alterar(emailBlacklist);
	}

	@Transactional
	public EntidadeTeste persistir(EntidadeTesteEdicaoDto entidadeTesteDto, String cpfUsuario) throws Exception {

		EntidadeTeste entidadeTeste = EntidadeTesteEdicaoMapper.INSTANCE.doDto(entidadeTesteDto);

		verificarExistencia(entidadeTeste);

		dao.criar(entidadeTeste);
		return entidadeTeste;
	}

	private void verificarExistencia(EntidadeTeste entidadeTeste) throws Exception {
		if (dao.verificarExistencia(entidadeTeste.getDescricao(), "descricao", entidadeTeste.getId()) != null) {
			throw new Exception("Já existe outro produto com a mesma descrição cadastrado.");
		}
	}

	@Transactional
	public long contarRegistros(ListParamsDto listParamsDto) throws Exception {
		return consulta.getQtdeResultados(listParamsDto);
	}

	@Transactional
	public List<EntidadeTesteConsultaPojo> listarFiltro(ListParamsDto listParamsDto) {
		return consulta.executar(listParamsDto, listParamsDto.getFirstResult(), listParamsDto.getMaxResult());
	}

	@Transactional
	public List<EntidadeTesteDto> listar() throws Exception {
		return EntidadeTesteMapper.INSTANCE.paraDto(dao.listarAsc(EntidadeTeste_.descricao, true));
	}

	@Transactional
	public EntidadeTesteDto consultar(Long idEntidadeTeste) {
		return EntidadeTesteMapper.INSTANCE.paraDto(dao.buscarPorId(idEntidadeTeste));
	}

	@Transactional
	public void inativar(Long idEntidadeTeste, String cpfUsuario) throws Exception {
		EntidadeTeste entidadeTeste = dao.buscarPorIdSincronizado(idEntidadeTeste);
		dao.alterar(entidadeTeste);
	}
}
