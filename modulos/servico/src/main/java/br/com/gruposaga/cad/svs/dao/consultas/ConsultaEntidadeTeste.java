package br.com.gruposaga.cad.svs.dao.consultas;

import br.com.gruposaga.cad.svs.dao.DaoUtil;
import br.com.gruposaga.cad.svs.fin.modelo.pojo.EntidadeTesteConsultaPojo;
import com.svs.model.centralLeads.ModeloQuestionarioSvs_;
import com.svs.fin.model.dto.ListParamsDto;
import com.svs.fin.model.dto.ParamFilterListDto;
import teste.EntidadeTeste;
import teste.EntidadeTeste_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class ConsultaEntidadeTeste extends ConsultaPaginadaCpo<EntidadeTesteConsultaPojo, ListParamsDto, EntidadeTeste>
{
    private CriteriaQuery<?> cq;

    @Override
    protected void inicializar (CriteriaQuery<?> cq)
    {
        this.cq = cq;
    }

    @Override
    protected void configurarRetorno (CriteriaQuery<EntidadeTesteConsultaPojo> cq)
    {
        cq.select(cb.construct(EntidadeTesteConsultaPojo.class,
            root.get(EntidadeTeste_.id),
            root.get(EntidadeTeste_.descricao)));
    }

    @Override
    protected void configurarFiltro(CriteriaQuery<?> cq, ListParamsDto listParams) {

        List<Predicate> predicates = new ArrayList<Predicate>();
        if (!listParams.getFilters().isEmpty()) {
            try {
                DaoUtil.configurarFiltroPredicates(listParams.getFilters(), cb, root, predicates);
                cq.where((Predicate[]) predicates.toArray(new Predicate[predicates.size()]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(listParams.getSortField() != null && !listParams.getSortField().trim().equals("")) {
            if(listParams.getSortOrder().equalsIgnoreCase("desc")) {
                cq.orderBy(cb.desc(root.get(listParams.getSortField())));
            }else {
                cq.orderBy(cb.asc(root.get(listParams.getSortField())));
            }
        }
    }
}
