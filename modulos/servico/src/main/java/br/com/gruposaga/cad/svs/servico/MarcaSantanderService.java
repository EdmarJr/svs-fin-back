package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import com.svs.fin.enums.EnumTipoRecebimento;
import com.svs.fin.enums.EnumTipoVeiculo;
import com.svs.fin.model.entities.MarcaSantander;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.CrudServiceCorporativo;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;
import br.com.gruposaga.cad.svs.utils.UtilsEnum;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MarcaSantanderService extends Service<MarcaSantander> {

	@PersistenceContext(unitName = "corporativo", type = PersistenceContextType.TRANSACTION)
	private EntityManager corporativoFactory;

	@Inject
	private CrudService crudService;
	@Inject
	private CrudServiceCorporativo crudServiceCorporativo;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<MarcaSantander> getClassOfT() {
		return MarcaSantander.class;
	}

	public MarcaSantander buscarMarcaSantander(String descricaoMarca) {

		MarcaSantander marcaSantanderRetorno = crudService
				.findSingleResultWithNamedQuery(MarcaSantander.NQ_OBTER_POR_DESCRIPTION.NAME,
						QueryParameter
								.with(MarcaSantander.NQ_OBTER_POR_DESCRIPTION.NM_PM_DESCRIPTION, descricaoMarca.trim())
								.parameters());
		if (marcaSantanderRetorno == null) {
			marcaSantanderRetorno = obterDoBancoCoporativoAposSincronizarComOCpo(descricaoMarca, marcaSantanderRetorno);
		}
//this.corporativoFactory.
//		Session s = (Session) dao.getOracle().getDelegate();
//		Criteria criteria = s.createCriteria(MarcaSantander.class, "marcaSantander");
//		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
//		criteria.setMaxResults(1);
//
//		criteria.add(Restrictions.like("description", descricaoMarca.trim().toUpperCase()));

		return marcaSantanderRetorno;
	}

	private MarcaSantander obterDoBancoCoporativoAposSincronizarComOCpo(String descricaoMarca,
			MarcaSantander marcaSantanderRetorno) {
		MarcaSantander marcaSantanderCorporativo = crudServiceCorporativo
				.findSingleResultWithNamedQuery(MarcaSantander.NQ_OBTER_POR_DESCRIPTION.NAME,
						QueryParameter
								.with(MarcaSantander.NQ_OBTER_POR_DESCRIPTION.NM_PM_DESCRIPTION, descricaoMarca.trim())
								.parameters());
		if (marcaSantanderCorporativo != null) {
			marcaSantanderCorporativo.setId(null);
			this.incluir(marcaSantanderCorporativo);
			marcaSantanderRetorno = marcaSantanderCorporativo;
		}
		return marcaSantanderRetorno;
	}

	public List<MarcaSantander> obterPorTipoVeiculo(String tipoVeiculo) {
		List<MarcaSantander> listaMarcasSantander = crudService.findWithNamedQuery(
				MarcaSantander.NQ_OBTER_POR_TIPO_VEICULO.NAME,
				QueryParameter
						.with(MarcaSantander.NQ_OBTER_POR_TIPO_VEICULO.NM_PM_TIPO_VEICULO,
								UtilsEnum.obterEnumLabeledPorLabel(EnumTipoVeiculo.values(), tipoVeiculo.trim()))
						.parameters());
		return listaMarcasSantander;

	}

}
