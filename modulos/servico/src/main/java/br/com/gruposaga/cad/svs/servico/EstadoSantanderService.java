package br.com.gruposaga.cad.svs.servico;


import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.EstadoSantander;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class EstadoSantanderService extends Service<EstadoSantander>{
	
	@Inject
	private CrudService crudService;
	
	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<EstadoSantander> getClassOfT() {
		return EstadoSantander.class;
	}
	
	
	
	
}
