package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.TipoEndereco;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TipoEnderecoService extends Service<TipoEndereco> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<TipoEndereco> getClassOfT() {
		return TipoEndereco.class;
	}

}
