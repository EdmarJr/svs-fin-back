package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.UnidadeOrganizacional;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;
import br.com.gruposaga.cad.svs.utils.UtilsMascara;

@Stateless
public class UnidadeOrganizacionalService extends Service<UnidadeOrganizacional> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<UnidadeOrganizacional> getClassOfT() {
		return UnidadeOrganizacional.class;
	}

	public UnidadeOrganizacional obterPorCnpj(String cnpj) {
		if (UtilsMascara.seCnpjEstaSemMascara(cnpj)) {
			cnpj = UtilsMascara.obterTextoComMascaraCnpj(cnpj);
		}
		return crudService.findSingleResultWithNamedQuery(UnidadeOrganizacional.NQ_OBTER_POR_CNPJ.NAME,
				QueryParameter.with(UnidadeOrganizacional.NQ_OBTER_POR_CNPJ.NM_PM_CNPJ, cnpj).parameters());
	}

}
