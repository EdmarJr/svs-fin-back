package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.CnpjInstituicaoFinanceira;
import com.svs.fin.model.entities.InstituicaoFinanceira;
import com.svs.fin.model.entities.ProdutoFinanceiro;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InstituicaoFinanceiraService extends Service<InstituicaoFinanceira> {

	@Inject
	private CrudService crudService;

	@Inject
	private ProdutoFinanceiroService produtoFinanceiroService;
	
	public InstituicaoFinanceira obterPorNome(String nome) {
		List<InstituicaoFinanceira> instituicaoFinanceira = getCrudService().findWithNamedQuery(
				InstituicaoFinanceira.NQ_OBTER_POR_NOME_INSTITUICAO.NAME,
				QueryParameter.with(InstituicaoFinanceira.NQ_OBTER_POR_NOME_INSTITUICAO.NM_PM_NOME, nome).parameters());

		return instituicaoFinanceira.size() > 0 ? instituicaoFinanceira.get(0) : null;
	}	

	@Override
	public void alterar(InstituicaoFinanceira instituicaoFinanceira) {
		instituicaoFinanceira.getCnpjsAssociados()
				.forEach(cnpj -> cnpj.setInstituicaoFinanceira(instituicaoFinanceira));
		attachProdutosFinanceiros(instituicaoFinanceira);
		super.alterar(instituicaoFinanceira);
	}

	@Override
	public void incluir(InstituicaoFinanceira instituicaoFinanceira) {
		instituicaoFinanceira.getCnpjsAssociados()
				.forEach(cnpj -> cnpj.setInstituicaoFinanceira(instituicaoFinanceira));
		attachProdutosFinanceiros(instituicaoFinanceira);
		super.incluir(instituicaoFinanceira);
	}

	private void attachProdutosFinanceiros(InstituicaoFinanceira instituicaoFinanceira) {
		for (CnpjInstituicaoFinanceira cnpj : instituicaoFinanceira.getCnpjsAssociados()) {
			List<ProdutoFinanceiro> produtosFinanceiros = new ArrayList<>();
			cnpj.getProdutosFinanceiros()
					.forEach(produto -> produtosFinanceiros.add(produtoFinanceiroService.obterPorId(produto.getId())));
			cnpj.setProdutosFinanceiros(produtosFinanceiros);
		}
	}

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<InstituicaoFinanceira> getClassOfT() {
		return InstituicaoFinanceira.class;
	}

}
