package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.FaturamentoClienteSvs;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class FaturamentoClienteSvsService extends Service<FaturamentoClienteSvs> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<FaturamentoClienteSvs> getClassOfT() {
		return FaturamentoClienteSvs.class;
	}

	public List<FaturamentoClienteSvs> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(FaturamentoClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NAME, QueryParameter
				.with(FaturamentoClienteSvs.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
