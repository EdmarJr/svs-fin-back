package br.com.gruposaga.cad.svs.servico;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.ParcelaEntrada;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ParcelaEntradaService extends Service<ParcelaEntrada> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}
	
	@Override
	protected Class<ParcelaEntrada> getClassOfT() {
		return ParcelaEntrada.class;
	}

	public List<ParcelaEntrada> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<ParcelaEntrada> retorno = crudService.findWithNamedQuery(ParcelaEntrada.NQ_OBTER_POR_OPERACAO_FINANCIADA.NAME,
				QueryParameter.with(ParcelaEntrada.NQ_OBTER_POR_OPERACAO_FINANCIADA.NM_PM_ID_OPERACAO_FINANCIADA,
						idOperacaoFinanciada).parameters());
		return retorno;
	}
	
	public void tratarParcelasAAbater(List<ParcelaEntrada> parcelasEntrada) {
		parcelasEntrada.forEach(parcelaEntradaIterate -> {
			if(parcelaEntradaIterate.getCodigoParcelaAbater() != null) {
				List<ParcelaEntrada> parcelasComCodigoEncontrado = parcelasEntrada.stream().filter(parcelaEntradaInFilter -> parcelaEntradaInFilter.getCodigo().equals(parcelaEntradaIterate.getCodigoParcelaAbater())).collect(Collectors.toList());
				
				if(parcelasComCodigoEncontrado.size() > 0) {
					parcelaEntradaIterate.setParcelaAbater(parcelasComCodigoEncontrado.get(0));
				}
			}
		});
	}

}
