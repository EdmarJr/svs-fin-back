package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.AnexoCad;
import com.svs.fin.model.entities.OperacaoFinanciada;
import com.svs.fin.utils.RelatorioJasperUtil;

import br.com.gruposaga.cad.svs.dao.ContratoDAO;
import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OperacaoFinanciadaService extends Service<OperacaoFinanciada> {

	@Inject
	private CrudService crudService;
	@Inject
	private AnexosCadService anexosCadService;
	@Inject
	private ContratoDAO contratoDAO;

	@Inject
	private ParcelaEntradaService parcelaEntradaService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	public List<OperacaoFinanciada> obterPorVinculoComCodigoProposta(Long codigoProposta) {
		List<OperacaoFinanciada> retorno = crudService.findWithNamedQuery(
				OperacaoFinanciada.NQ_OBTER_OPERACOES_VINCULADAS_A_UM_CODIGO_PROPOSTA.NAME,
				QueryParameter.with(
						OperacaoFinanciada.NQ_OBTER_OPERACOES_VINCULADAS_A_UM_CODIGO_PROPOSTA.NM_PM_CODIGO_PROPOSTA,
						codigoProposta).parameters());
		return retorno;
	}

	@Override
	public void beforeSave(OperacaoFinanciada operacao) {
		// operacao.getCalculoTabelaInstitucional().setOperacaoFinanciada(operacao);
		if (operacao.getVendedor() != null) {
			operacao.getVendedor().setAcessos(null);
		}
		operacao.getItensCbc().forEach(t -> t.setOperacaoFinanciada(operacao));
		operacao.getAndamentos().forEach(t -> t.setOperacaoFinanciada(operacao));
		operacao.getCalculosEnviadosIF().forEach(t -> t.setOperacaoFinanciada(operacao));
		operacao.getParcelasEntrada().forEach(t -> t.setOperacaoFinanciada(operacao));
		List<AnexoCad> novaListaAnexos = new ArrayList<AnexoCad>();
		operacao.getAnexos().forEach(t -> {
			AnexoCad anexo = anexosCadService.obterPorId(t.getId());
			if (anexo != null) {
				anexo.setOperacaoFinanciada(operacao);
				novaListaAnexos.add(anexo);
			}
		});
		operacao.setAnexos(novaListaAnexos);
		parcelaEntradaService.tratarParcelasAAbater(operacao.getParcelasEntrada());
		super.beforeSave(operacao);
	}

	@Override
	protected Class<OperacaoFinanciada> getClassOfT() {
		return OperacaoFinanciada.class;
	}

	public byte[] gerarContrato(Long id, String filesPath) {
		OperacaoFinanciada operacaoFinanciada = this.obterPorId(id);

		return gerarRelatorioByte(filesPath, contratoDAO.obterMensagemContrato(operacaoFinanciada),
				new Object[] { operacaoFinanciada });
	}

	private byte[] gerarRelatorioByte(String filesPath, String mensagemContrato, Object[] arrayOperacaoFinanciada) {
		RelatorioJasperUtil relatorioUtil = new RelatorioJasperUtil();
		HashMap<String, Object> mapParametros = new HashMap<String, Object>();

		// String mensagemContrato =
		// contratoService.obterMensagemContrato(operacaoFinanciada);
		mapParametros.put("MSG_CONTRATO", mensagemContrato);

		// List<OperacaoFinanciada> listOperacaoFinanciada = new
		// ArrayList<OperacaoFinanciada>();
		// listOperacaoFinanciada.add(operacaoFinanciada);

		// Object[] arrayOperacaoFinanciada = listOperacaoFinanciada.toArray();
		byte[] resultByte = relatorioUtil.gerarRelatorioPdf(mapParametros, "Contrato", arrayOperacaoFinanciada, "svs",
				filesPath);

		return resultByte;
	}

}
