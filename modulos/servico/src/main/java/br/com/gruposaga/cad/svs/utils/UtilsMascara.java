package br.com.gruposaga.cad.svs.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsMascara {
	private static final String REGEX_CPF_COM_MASCARA = "[0-9][0-9][0-9][.][0-9][0-9][0-9][.][0-9][0-9][0-9][-][0-9][0-9]";
	private static final String REGEX_CNPJ_COM_MASCARA = "[0-9][0-9][.][0-9][0-9][0-9][.][0-9][0-9][0-9][/][0-9][0-9][0-9][0-9][-][0-9][0-9]";
	private static final String REGEX_CNPJ_SEM_MASCARA = "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
	private static final String REGEX_CPF_SEM_MASCARA = "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";

	public static String obterTextoComMascaraCpf(String texto) {
		String retorno = "";
		Pattern pattern = Pattern.compile("([0-9][0-9][0-9])([0-9][0-9][0-9])([0-9][0-9][0-9])([0-9][0-9])");
		Matcher matcher = pattern.matcher(texto);
		if (matcher.matches()) {
			retorno = matcher.group(1) + "." + matcher.group(2) + "." + matcher.group(3) + "-" + matcher.group(4);
		}
		return retorno;
	}

	public static String obterTextoComMascaraCnpj(String texto) {
		String retorno = "";
		Pattern pattern = Pattern
				.compile("([0-9][0-9])([0-9][0-9][0-9])([0-9][0-9][0-9])([0-9][0-9][0-9][0-9])([0-9][0-9])");
		Matcher matcher = pattern.matcher(texto);
		if (matcher.matches()) {
			retorno = matcher.group(1) + "." + matcher.group(2) + "." + matcher.group(3) + "/" + matcher.group(4) + "-"
					+ matcher.group(5);
		}
		return retorno;
	}

	public static boolean seCpfEstaSemMascara(String cpfCnpj) {
		return UtilsRegex.seStringMatchRegex(REGEX_CPF_SEM_MASCARA, cpfCnpj);
	}

	public static boolean seCnpjEstaSemMascara(String cpfCnpj) {
		return UtilsRegex.seStringMatchRegex(REGEX_CNPJ_SEM_MASCARA, cpfCnpj);
	}

	public static String removeCaracteresCpfCnpj(String cpf) {
		if (cpf != null) {
			cpf = cpf.replace(".", "").replace("-", "").replace("/", "");
			return cpf.trim();
		}
		return "";
	}

	public static boolean seIsCpf(String cpfCnpj) {
		// TODO
		return UtilsRegex.seStringMatchRegex(REGEX_CPF_SEM_MASCARA, cpfCnpj)
				|| UtilsRegex.seStringMatchRegex(REGEX_CPF_COM_MASCARA, cpfCnpj);
	}

	public static boolean seIsCnpj(String cpfCnpj) {
		// TODO
		return UtilsRegex.seStringMatchRegex(REGEX_CNPJ_SEM_MASCARA, cpfCnpj)
				|| UtilsRegex.seStringMatchRegex(REGEX_CNPJ_COM_MASCARA, cpfCnpj);
	}

	public static String obterCpfOuCnpjComMascara(String cpfCnpj) {
		String retorno = "";
		if (UtilsMascara.seCpfEstaSemMascara(cpfCnpj)) {
			retorno = UtilsMascara.obterTextoComMascaraCpf(cpfCnpj);
		} else if (UtilsMascara.seCnpjEstaSemMascara(cpfCnpj)) {
			retorno = UtilsMascara.obterTextoComMascaraCnpj(cpfCnpj);
		}
		return retorno;
	}
}
