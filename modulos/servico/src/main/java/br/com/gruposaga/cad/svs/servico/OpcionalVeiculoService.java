package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.OpcionalVeiculo;
import com.svs.fin.model.entities.Proposta;

import br.com.gruposaga.cad.svs.dao.dealer.ExtracoesSysDealerWorkFlow;
import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OpcionalVeiculoService extends Service<OpcionalVeiculo> {

	@Inject
	private CrudService crudService;
	
	@Inject
	private ExtracoesSysDealerWorkFlow extracoesSysDealerWorkFlow;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<OpcionalVeiculo> getClassOfT() {
		return OpcionalVeiculo.class;
	}
	
	public List<OpcionalVeiculo> obterOpcionaisDoDealerQueNaoEstaoNaProposta(Long propostaCodigo, List<OpcionalVeiculo> opcionaisJaNaProposta) {
		List<OpcionalVeiculo> opcionaisRetorno = new ArrayList<OpcionalVeiculo>();
		List<OpcionalVeiculo> opcionais = extracoesSysDealerWorkFlow
				.listarOpcionaisVeiculo(propostaCodigo);
		opcionais.forEach((o) -> {
			if(opcionaisJaNaProposta.indexOf(o) == -1) {
				opcionaisRetorno.add(o);
			}
		});
		return opcionaisRetorno;
//		proposta.getVeiculo().getOpcionais().indexOf(arg0)
//		for (OpcionalVeiculo opcional : opcionais) {
//			boolean jaNaLista = false;
//			for (OpcionalVeiculo opcionalAux : proposta.getVeiculo().getOpcionais()) {
//				if (opcionalAux.getOpcionalCodigo().equals(opcional.getOpcionalCodigo())) {
//					jaNaLista = true;
//					break;
//				}
//			}
//			if (!jaNaLista) {
//				proposta.getVeiculo().getOpcionais().add(opcional);
//			}
//		}
	}

}
