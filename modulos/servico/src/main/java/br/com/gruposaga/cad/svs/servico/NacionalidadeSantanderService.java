package br.com.gruposaga.cad.svs.servico;


import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.NacionalidadeSantander;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class NacionalidadeSantanderService extends Service<NacionalidadeSantander>{
	
	@Inject
	private CrudService crudService;
	
	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<NacionalidadeSantander> getClassOfT() {
		return NacionalidadeSantander.class;
	}
	
	
	
	
}
