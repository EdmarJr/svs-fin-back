package br.com.gruposaga.cad.svs.servico.integracao.ws.safra.services;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.integracao.safra.model.Segmento_Veiculo;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.servico.Service;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class Segmento_VeiculoSafraService extends Service<Segmento_Veiculo> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<Segmento_Veiculo> getClassOfT() {
		return Segmento_Veiculo.class;
	}

}
