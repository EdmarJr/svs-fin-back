
package  br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the dealernet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dealernet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSPropostaDadosLIBERARPROPOSTAResponse }
     * 
     */
    public WSPropostaDadosLIBERARPROPOSTAResponse createWSPropostaDadosLIBERARPROPOSTAResponse() {
        return new WSPropostaDadosLIBERARPROPOSTAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCANCELARPROPOSTAResponse }
     * 
     */
    public WSPropostaDadosCANCELARPROPOSTAResponse createWSPropostaDadosCANCELARPROPOSTAResponse() {
        return new WSPropostaDadosCANCELARPROPOSTAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosATUALIZARPARCELAFINANCEIRA }
     * 
     */
    public WSPropostaDadosATUALIZARPARCELAFINANCEIRA createWSPropostaDadosATUALIZARPARCELAFINANCEIRA() {
        return new WSPropostaDadosATUALIZARPARCELAFINANCEIRA();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCONSULTARPROPOSTAResponse }
     * 
     */
    public WSPropostaDadosCONSULTARPROPOSTAResponse createWSPropostaDadosCONSULTARPROPOSTAResponse() {
        return new WSPropostaDadosCONSULTARPROPOSTAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCANCELARPROPOSTA }
     * 
     */
    public WSPropostaDadosCANCELARPROPOSTA createWSPropostaDadosCANCELARPROPOSTA() {
        return new WSPropostaDadosCANCELARPROPOSTA();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCONSULTARPROPOSTA }
     * 
     */
    public WSPropostaDadosCONSULTARPROPOSTA createWSPropostaDadosCONSULTARPROPOSTA() {
        return new WSPropostaDadosCONSULTARPROPOSTA();
    }

    /**
     * Create an instance of {@link WSPropostaDadosATUALIZARPROPOSTA }
     * 
     */
    public WSPropostaDadosATUALIZARPROPOSTA createWSPropostaDadosATUALIZARPROPOSTA() {
        return new WSPropostaDadosATUALIZARPROPOSTA();
    }

    /**
     * Create an instance of {@link WSPropostaDadosBLOQUEIOPROPOSTA }
     * 
     */
    public WSPropostaDadosBLOQUEIOPROPOSTA createWSPropostaDadosBLOQUEIOPROPOSTA() {
        return new WSPropostaDadosBLOQUEIOPROPOSTA();
    }

    /**
     * Create an instance of {@link WSPropostaDadosATUALIZARPROPOSTAResponse }
     * 
     */
    public WSPropostaDadosATUALIZARPROPOSTAResponse createWSPropostaDadosATUALIZARPROPOSTAResponse() {
        return new WSPropostaDadosATUALIZARPROPOSTAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCONSULTAFATVECResponse }
     * 
     */
    public WSPropostaDadosCONSULTAFATVECResponse createWSPropostaDadosCONSULTAFATVECResponse() {
        return new WSPropostaDadosCONSULTAFATVECResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosATUALIZARPARCELAFINANCEIRAResponse }
     * 
     */
    public WSPropostaDadosATUALIZARPARCELAFINANCEIRAResponse createWSPropostaDadosATUALIZARPARCELAFINANCEIRAResponse() {
        return new WSPropostaDadosATUALIZARPARCELAFINANCEIRAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCONSULTAFATVEC }
     * 
     */
    public WSPropostaDadosCONSULTAFATVEC createWSPropostaDadosCONSULTAFATVEC() {
        return new WSPropostaDadosCONSULTAFATVEC();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCADASTRARPROPOSTA }
     * 
     */
    public WSPropostaDadosCADASTRARPROPOSTA createWSPropostaDadosCADASTRARPROPOSTA() {
        return new WSPropostaDadosCADASTRARPROPOSTA();
    }

    /**
     * Create an instance of {@link WSPropostaDadosBLOQUEIOPROPOSTAResponse }
     * 
     */
    public WSPropostaDadosBLOQUEIOPROPOSTAResponse createWSPropostaDadosBLOQUEIOPROPOSTAResponse() {
        return new WSPropostaDadosBLOQUEIOPROPOSTAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosCADASTRARPROPOSTAResponse }
     * 
     */
    public WSPropostaDadosCADASTRARPROPOSTAResponse createWSPropostaDadosCADASTRARPROPOSTAResponse() {
        return new WSPropostaDadosCADASTRARPROPOSTAResponse();
    }

    /**
     * Create an instance of {@link WSPropostaDadosLIBERARPROPOSTA }
     * 
     */
    public WSPropostaDadosLIBERARPROPOSTA createWSPropostaDadosLIBERARPROPOSTA() {
        return new WSPropostaDadosLIBERARPROPOSTA();
    }

}
