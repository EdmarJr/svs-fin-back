package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ModeloVeiculoSantanderService extends Service<ModeloVeiculoSantander> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<ModeloVeiculoSantander> getClassOfT() {
		return ModeloVeiculoSantander.class;
	}

	public List<ModeloVeiculoSantander> obterPorIdMarca(Long idMarca) {
		List<ModeloVeiculoSantander> retorno = crudService.findWithNamedQuery(
				ModeloVeiculoSantander.NQ_OBTER_POR_ID_MARCA.NAME,
				QueryParameter.with(ModeloVeiculoSantander.NQ_OBTER_POR_ID_MARCA.NM_PM_ID_MARCA, idMarca).parameters());
		return retorno;

	}

}
