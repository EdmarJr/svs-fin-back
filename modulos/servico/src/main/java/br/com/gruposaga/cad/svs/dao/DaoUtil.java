package br.com.gruposaga.cad.svs.dao;

import com.svs.fin.model.dto.ParamFilterListDto;
import com.svs.model.enums.EnumDepartamentoSvs;
import com.svs.model.enums.EnumStatusLeadSvs;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DaoUtil {

    public static boolean isAStringField(String tipoFiltro, String campoFiltro) {
        return !campoFiltro.endsWith(".id")
            && (tipoFiltro == null
            || tipoFiltro.trim().equals("")
            || tipoFiltro.trim().equalsIgnoreCase("string"));
    }

    public static boolean isANumberField(ParamFilterListDto param) {
        return param.getField().endsWith(".id")
            || (param.getType() != null &&
                    param.getType().equalsIgnoreCase("number")
                    || param.getType().toUpperCase().contains("INT")
                    || param.getType().toUpperCase().contains("LONG")
                );
    }

    public static boolean isDateField(ParamFilterListDto param) {
        return param.getType() != null &&
            (param.getType().toUpperCase().contains("DATA")
            || param.getType().toUpperCase().contains("DATE")
            || param.getType().toUpperCase().contains("CALENDAR"));
    }

    public static boolean isABooleanField(ParamFilterListDto param) {
        return param.getType() != null && param.getType().equalsIgnoreCase("boolean");
    }

    public static Calendar convertToCalendar(String dateString, String dateFormat) throws Exception{
        SimpleDateFormat sdf = null;
        if (dateFormat == null || dateFormat.trim().equals("")) {
            sdf = new SimpleDateFormat("dd/MM/yyyy");
        }else{
            sdf = new SimpleDateFormat(dateFormat);
        }
        Date date = sdf.parse(dateString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static  List<EnumStatusLeadSvs> getListaEnumStatus(List<String> values){
        List<EnumStatusLeadSvs> listEnumStatus = new ArrayList<>();
        for (String value : values) {
            listEnumStatus.add(EnumStatusLeadSvs.valueOf(value));
        }
        return listEnumStatus;
    }


    public static  List<EnumDepartamentoSvs> getListaEnumDepartamento(List<String> values){
        List<EnumDepartamentoSvs> listEnumStatus = new ArrayList<>();
        for (String value : values) {
            listEnumStatus.add(EnumDepartamentoSvs.valueOf(value));
        }
        return listEnumStatus;
    }

    public static  List<Long> getListaIds(List<String> values){
        List<Long> listIds = new ArrayList<>();
        for (String value : values) {
            listIds.add(new Long(value));
        }
        return listIds;
    }


    /**
     *
     * Método deve construir os predicates de filtro da listagem
     * (método deverá ser sobrescrito em consultas mais complexas, onde for necessário realizar join's)
     *
     * @param filters Parametros de filtro aplicados
     * @param cb CriteriaBuilder sendo construída
     * @param root Root da consulta sendo construída
     * @return
     */
    public static List<Predicate> configurarFiltroPredicates(List<ParamFilterListDto> filters, CriteriaBuilder cb, Root root, List<Predicate> predicates) throws Exception{

        try {
            if (filters != null && !filters.isEmpty()) {
                Predicate predicate = cb.conjunction();
                for (ParamFilterListDto param : filters) {

                    String valueFilter = null;
                    Path<String> field = root.get(param.getField());

                    if(param.getValues().size() > 0){
                        predicates.add(cb.and(predicate, field.in(param.getValues())));
                    }else {
                        criarPredicateCampoFiltro(cb, predicates, predicate, param, field);
                    }

                    if(param.getMinValue() != null && !param.getMinValue().trim().equals("")){
                        if(DaoUtil.isDateField(param)){
                            predicates.add(cb.and(predicate, cb.greaterThanOrEqualTo(root.<Calendar>get(param.getField()), DaoUtil.convertToCalendar(param.getMinValue(), param.getDateFormat()))));
                        }
                    }

                    if(param.getMaxValue() != null && !param.getMaxValue().trim().equals("")){
                        if(DaoUtil.isDateField(param)){
                            predicates.add(cb.and(predicate, cb.lessThanOrEqualTo(root.<Calendar>get(param.getField()), DaoUtil.convertToCalendar(param.getMaxValue(), param.getDateFormat()))));
                        }
                    }
                }
                return predicates;
            }else{
                return null;
            }


        }catch(IllegalArgumentException i) {
            return null;
        }
    }



    public static void criarPredicateCampoFiltro(CriteriaBuilder cb, List<Predicate> predicates, Predicate predicate, ParamFilterListDto param, Path<String> field) {

        String valueFilter;

        String valorFiltro = param.getValue();
        String tipoFiltro = param.getType();

        if (valorFiltro != null && !valorFiltro.trim().equals("")){

            if (isAStringField(tipoFiltro, param.getField())) {
                if (param.getExact()) {
                    valueFilter = valorFiltro.toUpperCase();
                } else {
                    valueFilter = "%" + valorFiltro.toUpperCase() + "%";
                }
                predicates.add(cb.and(predicate, cb.like(cb.upper(field), valueFilter)));

            } else if (DaoUtil.isANumberField(param)) {
                predicates.add(cb.and(predicate, cb.equal(field, valorFiltro)));

            } else if (DaoUtil.isABooleanField(param)) {
                predicates.add(cb.and(predicate, cb.equal(field, Boolean.valueOf(valorFiltro))));
            }
        }
    }
}
