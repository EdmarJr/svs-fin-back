package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.enums.EnumTipoPessoa;
import com.svs.fin.model.entities.ClienteSvs;
import com.svs.fin.model.entities.OperacaoFinanciada;
import com.svs.fin.model.entities.interfaces.IClienteCentralCreditoDto;
import com.svs.fin.utils.RelatorioJasperUtil;

import br.com.gruposaga.cad.credito.model.dto.ClienteCentralCreditoDto;
import br.com.gruposaga.cad.credito.ws.api.WsClienteCredito;
import br.com.gruposaga.cad.svs.dao.ContratoDAO;
import br.com.gruposaga.cad.svs.dao.dealer.ExtracoesSysDealerWorkFlow;
import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;
import br.com.gruposaga.cad.svs.utils.UtilsMascara;
import br.com.gruposaga.cad.svs.utils.UtilsReflection;
import br.com.gruposaga.cad.ws.Erro400;
import br.com.gruposaga.cad.ws.Erro404;
import br.com.gruposaga.cad.ws.Erro500U;
import br.com.gruposaga.cad.wsc.WsClient;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ClienteSvsService extends Service<ClienteSvs> {

	@Inject
	private CrudService crudService;

	private WsClienteCredito wsClienteCredito;

	private @Inject WsClient wsClient;

	@Inject
	private AvalistaService avalistaService;

	@Inject
	private ImovelPatrimonioService ImovelPatrimonioService;

	@Inject
	private EnderecoService enderecoService;

	@Inject
	private OcupacaoService ocupacaoService;

	@Inject
	private ContratoDAO contratoDAO;

	@Inject
	private ExtracoesSysDealerWorkFlow extracoesSysDealerWorkFlow;

	@Inject
	private OperacaoFinanciadaService operacaoFinanciadaService;

	@PostConstruct
	public void init() {
		wsClienteCredito = wsClient.proxy(WsClienteCredito.class);
	}

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<ClienteSvs> getClassOfT() {
		return ClienteSvs.class;
	}

	@Override
	public void beforeSave(ClienteSvs clienteSvs) {
		clienteSvs.setCpfCnpj(UtilsMascara.removeCaracteresCpfCnpj(clienteSvs.getCpfCnpj()));
		super.beforeSave(clienteSvs);
		vincularEnderecos(clienteSvs);
		vincularOutrasRendas(clienteSvs);
		vincularFaturamentos(clienteSvs);
		vincularAvalistas(clienteSvs);
		vincularFornecedores(clienteSvs);
		vincularTelefones(clienteSvs);
		vincularImoveisPatrimonio(clienteSvs);
		vincularVeiculosPatrimonio(clienteSvs);
		vincularReferenciasPessoais(clienteSvs);
		tratarAvalistas(clienteSvs);
		tratarEnderecos(clienteSvs);
		tratarImoveisPatrimonio(clienteSvs);
		ocupacaoService.beforeSave(clienteSvs.getOcupacao());
	}

	private void tratarAvalistas(ClienteSvs clienteSvs) {
		clienteSvs.getAvalistas().forEach(avalista -> avalistaService.beforeSave(avalista));
	}

	private void tratarImoveisPatrimonio(ClienteSvs clienteSvs) {
		clienteSvs.getImoveisPatrimonio()
				.forEach(imovelPatrimonio -> ImovelPatrimonioService.beforeSave(imovelPatrimonio));
	}

	public void tratarEnderecos(ClienteSvs clienteSvs) {
		clienteSvs.getEnderecos().forEach(endereco -> enderecoService.beforeSave(endereco));
	}

	public void vincularEnderecos(ClienteSvs clienteSvs) {
		clienteSvs.getEnderecos().forEach(endereco -> endereco.setClienteSvs(clienteSvs));
	}

	public void vincularOutrasRendas(ClienteSvs clienteSvs) {
		clienteSvs.getOutrasRendas().forEach(outraRenda -> outraRenda.setClienteSvs(clienteSvs));
	}

	public void vincularFaturamentos(ClienteSvs clienteSvs) {
		clienteSvs.getFaturamentos().forEach(faturamento -> faturamento.setClienteSvs(clienteSvs));
	}

	public void vincularAvalistas(ClienteSvs clienteSvs) {
		clienteSvs.getAvalistas().forEach(avalista -> avalista.setClienteSvs(clienteSvs));
	}

	public void vincularFornecedores(ClienteSvs clienteSvs) {
		clienteSvs.getFornecedores().forEach(fornecedor -> fornecedor.setClienteSvs(clienteSvs));
	}

	public void vincularTelefones(ClienteSvs clienteSvs) {
		clienteSvs.getTelefones().forEach(telefone -> telefone.setClienteSvs(clienteSvs));
	}

	public void vincularImoveisPatrimonio(ClienteSvs clienteSvs) {
		clienteSvs.getImoveisPatrimonio().forEach(ip -> ip.setClienteSvs(clienteSvs));
	}

	public void vincularVeiculosPatrimonio(ClienteSvs clienteSvs) {
		clienteSvs.getVeiculosPatrimonio().forEach(vp -> vp.setClienteSvs(clienteSvs));
	}

	public void vincularReferenciasPessoais(ClienteSvs clienteSvs) {
		clienteSvs.getReferenciasPessoais().forEach(rp -> rp.setClienteSvs(clienteSvs));
	}

	public ClienteSvs obterPorCpfCnpj(String cpfCnpj) {
		return obterPorCpfCnpj(cpfCnpj, false);
	}

	public ClienteSvs obterPorCpfCnpj(String cpfCnpj, Boolean flagClienteCarregado) {
		ClienteSvs clienteSvsRetorno = null;
		if (flagClienteCarregado != null && flagClienteCarregado) {
			clienteSvsRetorno = crudService.findSingleResultWithNamedQuery(ClienteSvs.NQ_OBTER_POR_CPF_CNPJ.NAME,
					QueryParameter.with(ClienteSvs.NQ_OBTER_POR_CPF_CNPJ.NM_PM_CPF_CNPJ,
							UtilsMascara.removeCaracteresCpfCnpj(cpfCnpj)).parameters());
			initList(clienteSvsRetorno.getAvalistas());
			initList(clienteSvsRetorno.getEnderecos());
			initList(clienteSvsRetorno.getFaturamentos());
			initList(clienteSvsRetorno.getFornecedores());
			initList(clienteSvsRetorno.getTelefones());
			initList(clienteSvsRetorno.getOutrasRendas());
			initList(clienteSvsRetorno.getEnderecos());
		} else {
			clienteSvsRetorno = crudService.findSingleResultWithNamedQuery(ClienteSvs.NQ_OBTER_POR_CPF_CNPJ.NAME,
					QueryParameter.with(ClienteSvs.NQ_OBTER_POR_CPF_CNPJ.NM_PM_CPF_CNPJ,
							UtilsMascara.removeCaracteresCpfCnpj(cpfCnpj)).parameters());
		}

		if (clienteSvsRetorno == null) {
			try {
				clienteSvsRetorno = new ClienteSvs();
				ClienteCentralCreditoDto clienteCentralCredito = wsClienteCredito.getByCpfCnpj(cpfCnpj);
				UtilsReflection.copiarPropriedadesDoObjetoReferenciaParaOObjetoDestino(clienteSvsRetorno,
						clienteCentralCredito, IClienteCentralCreditoDto.class);
			} catch (Erro404 e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Erro400 e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			} catch (Erro500U e) {
				e.printStackTrace();
				return null;
			}
		}

		return clienteSvsRetorno;
	}

	public void salvarClienteVindoDoDealer(ClienteSvs clienteSvs) throws Exception {
		clienteSvs.setCpfCnpj(UtilsMascara.removeCaracteresCpfCnpj(clienteSvs.getCpfCnpj()));

		if (clienteSvs.getId() != null && clienteSvs.getId() > 0) {
			alterar(clienteSvs);
		} else {
			incluir(clienteSvs);
		}

		extracoesSysDealerWorkFlow.inserirEnderecosNaoExistentesNoCliente(clienteSvs);
		extracoesSysDealerWorkFlow.inserirRGNoCliente(clienteSvs);
	}

	public ClienteSvs obterPorCpfCnpjDoBancoDoDealer(String cpfCnpj) {
		ClienteSvs clienteSvsRetorno = null;

		cpfCnpj = UtilsMascara.obterCpfOuCnpjComMascara(cpfCnpj);

		clienteSvsRetorno = extracoesSysDealerWorkFlow.getClienteSvsByCpfCnpj(cpfCnpj);

		if (clienteSvsRetorno == null) {
			return clienteSvsRetorno;
		}

		if (UtilsMascara.seIsCnpj(cpfCnpj)) {
			clienteSvsRetorno.setCnpj(cpfCnpj);
		} else {
			clienteSvsRetorno.setCnpj(cpfCnpj);
		}

		return clienteSvsRetorno;
	}

	public byte[] gerarFichaCliente(Long id, String filesPath) {
		OperacaoFinanciada operacaoFinanciada = operacaoFinanciadaService.obterPorId(id);

		operacaoFinanciada.getCliente().setVeiculoProposta(operacaoFinanciada.getProposta().getVeiculo());
		operacaoFinanciada.getCliente().setCalculoContrato(operacaoFinanciada.getCalculoContrato());

		return gerarRelatorioFichaClienteByte(operacaoFinanciada.getCliente(), filesPath);
	}

	private byte[] gerarRelatorioFichaClienteByte(ClienteSvs cliente, String filesPath) {
		RelatorioJasperUtil relatorioUtil = new RelatorioJasperUtil();
		HashMap<String, Object> mapParametros = new HashMap<String, Object>();

		List<ClienteSvs> listCliente = new ArrayList<ClienteSvs>();
		listCliente.add(cliente);

		if (cliente.getTipoPessoa().equals(EnumTipoPessoa.PESSOA_FISICA)) {
			return relatorioUtil.gerarRelatorioPdf(mapParametros, "FichaClientePessoaFisica", listCliente.toArray(),
					"svs", filesPath);
		} else {
			return relatorioUtil.gerarRelatorioPdf(mapParametros, "FichaClientePessoaJuridica", listCliente.toArray(),
					"svs", filesPath);
		}
	}

	public <T> void initList(List<T> lista) {
		lista.size();
	}

}
