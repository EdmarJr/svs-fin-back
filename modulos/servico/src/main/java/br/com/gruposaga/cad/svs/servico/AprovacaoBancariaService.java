package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.AprovacaoBancaria;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AprovacaoBancariaService extends Service<AprovacaoBancaria> {
	
	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<AprovacaoBancaria> getClassOfT() {
		return AprovacaoBancaria.class;
	}
	
	public List<AprovacaoBancaria> obterPorCalculoRentabilidade(Long idCalculoRentabilidade) {
		List<AprovacaoBancaria> retorno = crudService.findWithNamedQuery(AprovacaoBancaria.NQ_OBTER_POR_CALCULO_RENTABILIDADE.NAME,
				QueryParameter.with(AprovacaoBancaria.NQ_OBTER_POR_CALCULO_RENTABILIDADE.NM_PM_ID_CALCULO_RENTABILIDADE, 
						idCalculoRentabilidade).parameters());
		return retorno;
	}

}
