package br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DadosCliente {

	private String clienteNome;

	private String clienteCpfCnpj;

	@XmlElement(name = "ClienteNome")
	public String getClienteNome() {
		return clienteNome;
	}

	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}

	@XmlElement(name = "ClienteCPFCNPJ")
	public String getClienteCpfCnpj() {
		return clienteCpfCnpj;
	}

	public void setClienteCpfCnpj(String clienteCpfCnpj) {
		this.clienteCpfCnpj = clienteCpfCnpj;
	}
}
