package br.com.gruposaga.cad.svs.dao.consultas;

import br.com.gruposaga.cad.cpo.Cpo;
import br.com.gruposaga.jpp.core.utils.UtilsJava;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Design Pattern Template: class utilitária para execução de uma consulta paginada, que acontece em duas etapas:
 * primeiro, a contagem de resultados; depois, a consulta conforme a página atual.
 *
 * @param <T> o tipo de resultado retornado como Lista.
 * @param <Y> o tipo de filtro necessário para a consulta.
 * @param <V> a classe que origina a raiz da query (Root).
 * @author Amsterdam Luís
 */
public abstract class ConsultaPaginadaCpo<T, Y, V>
{
	@Cpo
	@Inject protected EntityManager emCpo;

    protected CriteriaBuilder cb;
    protected Root<V>         root;

    public List<T> executar (Y filtro, int primResul, int qtdeMaxResul)
    {
        cb = emCpo.getCriteriaBuilder();
        Class<T> classeResultado = UtilsJava.obterArgumentoTipo(this, 0);
        CriteriaQuery<T> cq = cb.createQuery(classeResultado);
        Class<V> classeRoot = UtilsJava.obterArgumentoTipo(this, 2);
        root = cq.from(classeRoot);
        inicializar(cq);
        configurarRetorno(cq);
        configurarFiltro(cq, filtro);
        TypedQuery<T> query = emCpo.createQuery(cq);
        query.setFirstResult(primResul);
        
        if(qtdeMaxResul == 0) {
        	query.setMaxResults(200);
        }else {
        	query.setMaxResults(qtdeMaxResul);
        }
        return query.getResultList();
    }

    public long getQtdeResultados (Y filtro)
    {
        cb = emCpo.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Class<V> classeRoot = UtilsJava.obterArgumentoTipo(this, 2);
        root = cq.from(classeRoot);
        inicializar(cq);
        cq.select(cb.count(root));
        configurarFiltro(cq, filtro);
        return emCpo.createQuery(cq).getSingleResult();
    }

    /**
     * Método chamado após a inicialização do campos protegidos. Pode ser utilizado pela subclasse para criação de
     * outros objetos usados na query em ambos os métodos abstratos.
     *
     * @param cq a cq;
     */
    protected void inicializar (CriteriaQuery<?> cq)
    {

    }

    /**
     * Nesse método a classe filha deve configurar o retorno da query (select, orderBy...).
     *
     * @param cq a criteria query.
     */
    protected abstract void configurarRetorno (CriteriaQuery<T> cq);

    /**
     * Nesse método a classe filha deve configurar o filtro da query (where etc.).
     *
     * @param cq     a criteria query.
     * @param filtro o filtro da consulta.
     */
    protected abstract void configurarFiltro (CriteriaQuery<?> cq, Y filtro);



}
