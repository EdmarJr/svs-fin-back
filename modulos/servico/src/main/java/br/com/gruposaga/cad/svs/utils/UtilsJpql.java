package br.com.gruposaga.cad.svs.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsJpql {

	public static String toLikeParameter(String parametro) {
		return "%" + parametro + "%";
	}

	public static <T> String criarSelectParaBuscarTodos(Class<T> clazz) {
		return UtilsJpql.criarSelectParaBuscarTodos(clazz, new String[] {});
	}
	
	public static <T> String criarSelectParaBuscarTodos(Class<T> clazz, String... nomeCamposFetch) {
		if (nomeCamposFetch == null) {
			nomeCamposFetch = new String[] {};
		}

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT ");
		// Dá erro ao utiliza-lo com o order by
//		if (nomeCamposFetch.length > 0) {
//			stringBuilder.append("DISTINCT ");
//		}
		stringBuilder.append("t FROM ");
		stringBuilder.append(clazz.getName());
		stringBuilder.append(" t");

		for (int i = 0; i < nomeCamposFetch.length; i++) {
			stringBuilder.append(" LEFT JOIN FETCH t." + nomeCamposFetch[i]);
		}

		return stringBuilder.toString();
	}

	public static <T> String criarSelectCountParaObterNumeroDeLinhasDaTabela(Class<T> clazz) {
		StringBuilder stringBuilder = new StringBuilder();
		// criar select
		stringBuilder.append("SELECT COUNT(t) FROM ");
		stringBuilder.append(clazz.getName());
		stringBuilder.append(" t");
		return stringBuilder.toString();
	}
	
	public static boolean seQueryFazJoin(String namedQuery) {
		return namedQuery.indexOf("JOIN") != -1 || namedQuery.indexOf("join") != -1;
	}
	
	public static <T> void inicializarListaEmObjeto(T objeto, String nomeAtributoLista) {
		List<?> lista;
		try {
			lista = (List<?>) objeto.getClass()
					.getMethod("get" + UtilsString.toPrimeiraLetraMaiscula(nomeAtributoLista)).invoke(objeto);
			lista.size();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String removerJoinsConflitantesDoTextoJpqlObjeto(String textoJpqlObjeto,
			String textoJpqlReferencia) {
		StringBuilder stringBuilder = new StringBuilder(textoJpqlObjeto);
		HashMap<String, String> mapJoinsParteJpqlParametrosDeBusca = obterMapDeJoins(textoJpqlReferencia);
		
		HashMap<String, String> mapJoinsSqlInicial = obterMapDeJoins(textoJpqlObjeto);
		
		for(Entry<String,String> entry: mapJoinsSqlInicial.entrySet()) {
			if(mapJoinsParteJpqlParametrosDeBusca.containsKey(entry.getKey())) {
				stringBuilder = new StringBuilder(stringBuilder.toString().replace(entry.getValue(), ""));
			}
		}
		
		return stringBuilder.toString();
	}

	private static HashMap<String, String> obterMapDeJoins(String texto) {
		Pattern pattern = Pattern.compile("([LEFT]*[INNER]*)[ ]?[Jj][Oo][Ii][Nn] [FETCH]*[ ]?([A-z.]+)");
		Matcher matcher = pattern.matcher(texto);
		HashMap<String, String> map = new HashMap<String, String>();
		while (matcher.find()) {
			map.put(matcher.group(2), matcher.group(0));
		}
		return map;
	}
	
	public static boolean seQueryFazJoinENaoTemDistinct(String namedQuery) {
		return UtilsJpql.seQueryFazJoin(namedQuery) && namedQuery.indexOf("SELECT DISTINCT") == -1;
	}

}
