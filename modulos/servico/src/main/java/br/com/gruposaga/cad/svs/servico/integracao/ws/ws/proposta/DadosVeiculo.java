package br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DadosVeiculo {

	private String chassi;

	private String placa;

	private String descricaoModelo;

	private String codigoModeloMarca;

	private String codigoModeloMolicar;

	private String codigoModeloFipe;

	private String veiculoStatus;

	private Integer veiculoKm;

	private Integer veiculoAnoModelo;

	private Integer veiculoAnoFabricacao;

	private String veiculoMunicipioNomePlacaIbge;

	private String veiculoEstadoNomePlaca;

	private String veiculoEstadoCodPlaca;

	private String veiculoCorCodExterna;

	private String veiculoNrRenavam;

	private String veiculoCodigo;

	private String veiculoModeloVeiculoCod;

	@XmlElement(name = "Chassi")
	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	@XmlElement(name = "Placa")
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	@XmlElement(name = "DescricaoModelo")
	public String getDescricaoModelo() {
		return descricaoModelo;
	}

	public void setDescricaoModelo(String descricaoModelo) {
		this.descricaoModelo = descricaoModelo;
	}

	@XmlElement(name = "CodigoModeloMarca")
	public String getCodigoModeloMarca() {
		return codigoModeloMarca;
	}

	public void setCodigoModeloMarca(String codigoModeloMarca) {
		this.codigoModeloMarca = codigoModeloMarca;
	}

	@XmlElement(name = "CodigoModeloMolicar")
	public String getCodigoModeloMolicar() {
		return codigoModeloMolicar;
	}

	public void setCodigoModeloMolicar(String codigoModeloMolicar) {
		this.codigoModeloMolicar = codigoModeloMolicar;
	}

	@XmlElement(name = "CodigoModeloFIPE")
	public String getCodigoModeloFipe() {
		return codigoModeloFipe;
	}

	public void setCodigoModeloFipe(String codigoModeloFipe) {
		this.codigoModeloFipe = codigoModeloFipe;
	}

	@XmlElement(name = "Veiculo_Status")
	public String getVeiculoStatus() {
		return veiculoStatus;
	}

	public void setVeiculoStatus(String veiculoStatus) {
		this.veiculoStatus = veiculoStatus;
	}

	@XmlElement(name = "Veiculo_Km")
	public Integer getVeiculoKm() {
		return veiculoKm;
	}

	public void setVeiculoKm(Integer veiculoKm) {
		this.veiculoKm = veiculoKm;
	}

	@XmlElement(name = "VeiculoAno_Modelo")
	public Integer getVeiculoAnoModelo() {
		return veiculoAnoModelo;
	}

	public void setVeiculoAnoModelo(Integer veiculoAnoModelo) {
		this.veiculoAnoModelo = veiculoAnoModelo;
	}

	@XmlElement(name = "VeiculoAno_Fabricacao")
	public Integer getVeiculoAnoFabricacao() {
		return veiculoAnoFabricacao;
	}

	public void setVeiculoAnoFabricacao(Integer veiculoAnoFabricacao) {
		this.veiculoAnoFabricacao = veiculoAnoFabricacao;
	}

	@XmlElement(name = "Veiculo_MunicipioNom_PlacaIBGE")
	public String getVeiculoMunicipioNomePlacaIbge() {
		return veiculoMunicipioNomePlacaIbge;
	}

	public void setVeiculoMunicipioNomePlacaIbge(String veiculoMunicipioNomePlacaIbge) {
		this.veiculoMunicipioNomePlacaIbge = veiculoMunicipioNomePlacaIbge;
	}

	@XmlElement(name = "Veiculo_EstadoNom_Placa")
	public String getVeiculoEstadoNomePlaca() {
		return veiculoEstadoNomePlaca;
	}

	public void setVeiculoEstadoNomePlaca(String veiculoEstadoNomePlaca) {
		this.veiculoEstadoNomePlaca = veiculoEstadoNomePlaca;
	}

	@XmlElement(name = "Veiculo_EstadoCod_Placa")
	public String getVeiculoEstadoCodPlaca() {
		return veiculoEstadoCodPlaca;
	}

	public void setVeiculoEstadoCodPlaca(String veiculoEstadoCodPlaca) {
		this.veiculoEstadoCodPlaca = veiculoEstadoCodPlaca;
	}

	@XmlElement(name = "Veiculo_CorCodExterna")
	public String getVeiculoCorCodExterna() {
		return veiculoCorCodExterna;
	}

	public void setVeiculoCorCodExterna(String veiculoCorCodExterna) {
		this.veiculoCorCodExterna = veiculoCorCodExterna;
	}

	@XmlElement(name = "Veiculo_NroRenavam")
	public String getVeiculoNrRenavam() {
		return veiculoNrRenavam;
	}

	public void setVeiculoNrRenavam(String veiculoNrRenavam) {
		this.veiculoNrRenavam = veiculoNrRenavam;
	}

	@XmlElement(name = "Veiculo_Codigo")
	public String getVeiculoCodigo() {
		return veiculoCodigo;
	}

	public void setVeiculoCodigo(String veiculoCodigo) {
		this.veiculoCodigo = veiculoCodigo;
	}

	@XmlElement(name = "Veiculo_ModeloVeiculoCod")
	public String getVeiculoModeloVeiculoCod() {
		return veiculoModeloVeiculoCod;
	}

	public void setVeiculoModeloVeiculoCod(String veiculoModeloVeiculoCod) {
		this.veiculoModeloVeiculoCod = veiculoModeloVeiculoCod;
	}
}