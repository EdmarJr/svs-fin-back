package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.Endereco;
import com.svs.fin.model.entities.Municipio;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class EnderecoService extends Service<Endereco> {

	@Inject
	private CrudService crudService;

	@Inject
	private MunicipioService municipioService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	private List<Municipio> listaMunicipiosGerenciados = new ArrayList<Municipio>();

	@Override
	public void beforeSave(Endereco endereco) {
		super.beforeSave(endereco);
		if (endereco.getMunicipio() != null && endereco.getMunicipio().getId() != null) {
			if (seMunicipioExisteNaListaDeGerenciados(endereco.getMunicipio())) {
				endereco.setMunicipio(
						listaMunicipiosGerenciados.get(listaMunicipiosGerenciados.indexOf(endereco.getMunicipio())));
			} else {
				Municipio municipio = municipioService.obterPorId(endereco.getMunicipio().getId());
				endereco.setMunicipio(municipio);
				listaMunicipiosGerenciados.add(municipio);
			}

		}
		municipioService.beforeSave(endereco.getMunicipio());

	}

	private boolean seMunicipioExisteNaListaDeGerenciados(Municipio municipio) {
		return listaMunicipiosGerenciados.indexOf(municipio) != -1;
	}

	@Override
	protected Class<Endereco> getClassOfT() {
		return Endereco.class;
	}

	public List<Endereco> obterPorIdClienteSvs(Long idClienteSvs) {
		return crudService.findWithNamedQuery(Endereco.NQ_OBTER_POR_CLIENTE_SVS.NAME,
				QueryParameter.with(Endereco.NQ_OBTER_POR_CLIENTE_SVS.NM_PM_ID_CLIENTE_SVS, idClienteSvs).parameters());
	}

}
