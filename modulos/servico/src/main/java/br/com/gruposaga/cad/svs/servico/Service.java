package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@TransactionAttribute(TransactionAttributeType.REQUIRED)
public abstract class Service<T> {

	public void beforeSave(T t) {
	}

	public void incluir(T t) {
		beforeSave(t);
		getCrudService().create(t);
	}

	public void alterar(T t) {
		beforeSave(t);
		getCrudService().update(t);
	}

	public void deletar(T t) {
		getCrudService().delete(t);
	}

	public void deletar(Long id) {
		getCrudService().delete(getClassOfT(), id);
	}

	public List<T> obterTodos() {
		return getCrudService().obterTodos(getClassOfT());
	}

	public T obterPorId(Long id) {
		return getCrudService().obterPorId(getClassOfT(), id);
	}

	public PaginaDeObjetos<T> obterTodos(Integer offSet, Integer limit) {
		return obterTodos(
				GrupoDeParametrosParaListagemLazy.Builder.novoGrupo().setLimit(limit).setOffSet(offSet).build());
	}

	public PaginaDeObjetos<T> obterTodos(Integer offSet, Integer limit, List<String> parametrosDeOrdenacao,
			List<String> parametrosDeBusca) {
		return obterTodos(GrupoDeParametrosParaListagemLazy.Builder.novoGrupo().setLimit(limit).setOffSet(offSet)
				.adicionarParametrosBusca(parametrosDeBusca).adicionarParametrosOrdenacao(parametrosDeOrdenacao)
				.build());
	}

	public PaginaDeObjetos<T> obterTodos(GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy) {

		return obterTodos(grupoDeParametrosParaListagemLazy, new String[] {});
	}

	public PaginaDeObjetos<T> obterTodos(GrupoDeParametrosParaListagemLazy grupoDeParametrosParaListagemLazy,
			String... listaDeParametrosParaFazerFetchEager) {
		List<T> objetos = getCrudService().obterPorParametros(getClassOfT(), grupoDeParametrosParaListagemLazy,
				listaDeParametrosParaFazerFetchEager);
		Long totalObjetosNaTabela = getCrudService().obterTotalDeLinhasPorParametros(getClassOfT(),
				grupoDeParametrosParaListagemLazy);
		PaginaDeObjetos<T> paginaDeObjetos = PaginaDeObjetos.Builder
				.novaPagina(totalObjetosNaTabela, objetos, grupoDeParametrosParaListagemLazy).build();
		return paginaDeObjetos;
	}

	protected abstract CrudService getCrudService();

	protected abstract Class<T> getClassOfT();

}
