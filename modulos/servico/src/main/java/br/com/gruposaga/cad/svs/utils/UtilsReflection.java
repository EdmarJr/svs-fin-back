package br.com.gruposaga.cad.svs.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.mapping.Set;

public class UtilsReflection {
	public static Class<?> getTypeOfFieldOfClass(Class<?> clazz, String nomeCampo) {
		try {
			String[] array = nomeCampo.split("[.]");
			if (array.length == 0) {
				return null;
			}
			for (int i = 0; i < array.length; i++) {
				Class<?> type = clazz.getDeclaredField(array[i]).getType();
				if (seTipoIsList(type) && UtilsIterable.seNaoIsUltimaIteracao(array.length, i)) {
					ParameterizedType listType = (ParameterizedType) clazz.getDeclaredField(array[i]).getGenericType();
					type = (Class<?>) listType.getActualTypeArguments()[0];
				}
				clazz = type;
			}

			return clazz;

		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static boolean seTypeOfFieldOfClassIgualaoTipoReferencia(Class<?> clazz, String nomeCampo,
			Class<?> tipoReferencia) {
		return getTypeOfFieldOfClass(clazz, nomeCampo) == tipoReferencia;
	}

	public static <T> Boolean seCampoIsString(Class<T> clazz, String nomeCampo) {
		return UtilsReflection.getTypeOfFieldOfClass(clazz, nomeCampo) == String.class;
	}

	public static boolean seTipoIsList(Class<?> typeCampo) {
		return typeCampo == List.class || typeCampo == ArrayList.class || typeCampo == Collection.class
				|| typeCampo == Set.class;
	}

	public static Field[] obterFieldsOfClass(Class<?> clazz) {
		return clazz.getFields();
	}

	public static Method obterMetodoGetCorrespondenteAoField(Field field, Class<?> clazz) {
		String nomeMetodo = obterNomeMetodoCorrespondenteAoField(field, "get");
		try {
			Method metodo = clazz.getDeclaredMethod(nomeMetodo);
			return metodo;
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return null;
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public static Method obterMetodoSetCorrespondenteAoField(Field field, Class<?> clazz) {
		String nomeMetodo = obterNomeMetodoCorrespondenteAoField(field, "set");

		try {
			Method metodo = obterMetodoPorNomeSemSaberParametrosQueEleRecebe(clazz, nomeMetodo);
			return metodo;
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return null;
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * No caso de sobrecarga, pode haver confusão
	 * 
	 * @param field
	 * @param clazz
	 * @param nomeMetodo
	 * @return
	 * @throws NoSuchMethodException
	 */
	private static Method obterMetodoPorNomeSemSaberParametrosQueEleRecebe(Class<?> clazz, String nomeMetodo)
			throws NoSuchMethodException {
		Method[] metodos = obterMetodosDeclaradosEmUmaClasse(clazz);

		for (Method method : metodos) {
			if (method.getName().equals(nomeMetodo)) {
				return method;
			}
		}
		return null;
	}

	public static List<String> obterNomesDeMetodosDeclaradosEmUmaClasseComOPrefixoReferencia(String prefixoReferencia,
			Class<?> clazz) {
		List<String> nomesMetodos = new ArrayList<String>();
		Method[] metodos = obterMetodosDeclaradosEmUmaClasse(clazz);
		for (Method method : metodos) {
			if (method.getName().indexOf(prefixoReferencia) == 0) {
				nomesMetodos.add(method.getName());
			}
		}
		return nomesMetodos;
	}

	public static Method[] obterMetodosDeclaradosEmUmaClasse(Class<?> clazz) {
		return clazz.getDeclaredMethods();
	}

	public static String obterNomeMetodoCorrespondenteAoField(Field field, String prefixo) {
		String nomeField = field.getName();
		String nomeMetodo = obterNomeMetodoComPrefixo(prefixo, nomeField);
		return nomeMetodo;
	}

	private static String obterNomeMetodoComPrefixo(String prefixo, String nomeField) {
		String nomeMetodo = prefixo + (prefixo != null && !prefixo.equals("")
				? nomeField.substring(0, 1).toUpperCase() + nomeField.substring(1, nomeField.length())
				: nomeField);
		return nomeMetodo;
	}

	/**
	 * Por enquanto, só funciona passando interface como classe referencia
	 * 
	 * @param objetoDestino
	 * @param objetoReferencia
	 * @param classeReferenciaParaObterMetodosQueSeraoChamados
	 */
	public static void copiarPropriedadesDoObjetoReferenciaParaOObjetoDestino(Object objetoDestino,
			Object objetoReferencia, Class<?> classeReferenciaParaObterMetodosQueSeraoChamados) {
		String prefixoGet = "get";
		Class<? extends Object> clazzObjetoDestino = objetoDestino.getClass();
		List<String> nomesDosMetodosSemPrefixoEComPrimeiraLetraMinuscula = new ArrayList<String>();
		if (classeReferenciaParaObterMetodosQueSeraoChamados.isInterface()) {
			List<String> nomesDosMetodos = obterNomesDeMetodosDeclaradosEmUmaClasseComOPrefixoReferencia(prefixoGet,
					classeReferenciaParaObterMetodosQueSeraoChamados);
			nomesDosMetodos.forEach(nome -> {
				nomesDosMetodosSemPrefixoEComPrimeiraLetraMinuscula
						.add(UtilsString.toPrimeiraLetraMinuscula(UtilsString.toSemPrefixo(nome, prefixoGet)));
			});
		}

		for (String nomeDoMetodoSemPrefixoEComPrimeiraLetraMinuscula : nomesDosMetodosSemPrefixoEComPrimeiraLetraMinuscula) {
			try {
				Method metodoGetObjetoReferencia = obterMetodoPorNomeSemSaberParametrosQueEleRecebe(
						objetoReferencia.getClass(),
						UtilsString.toComPrefixo(
								UtilsString.toPrimeiraLetraMaiscula(nomeDoMetodoSemPrefixoEComPrimeiraLetraMinuscula),
								prefixoGet));

				Method metodoGetInteface = obterMetodoPorNomeSemSaberParametrosQueEleRecebe(
						classeReferenciaParaObterMetodosQueSeraoChamados.getClass(),
						UtilsString.toComPrefixo(
								UtilsString.toPrimeiraLetraMaiscula(nomeDoMetodoSemPrefixoEComPrimeiraLetraMinuscula),
								prefixoGet));

				Object retornoInvocacaoMetodoGet = metodoGetObjetoReferencia.invoke(objetoReferencia);

				Method metodoSetObjetoDestino = obterMetodoPorNomeSemSaberParametrosQueEleRecebe(clazzObjetoDestino,
						UtilsString.toComPrefixo(
								UtilsString.toPrimeiraLetraMaiscula(nomeDoMetodoSemPrefixoEComPrimeiraLetraMinuscula),
								"set"));

				if (seTipoIsList(metodoGetObjetoReferencia.getReturnType())) {
					metodoGetObjetoReferencia.getReturnType().getTypeParameters();
					Collection novaCollection = (Collection<?>) metodoGetObjetoReferencia.getReturnType().newInstance();
					for (Object o : ((Collection<?>) retornoInvocacaoMetodoGet)) {
						Class classeTipoGenericoDoMetodoSetDoObjetoDestino = ((Class<?>) obterTipoDaCollectionPassadaParaOMetodoSet(
								metodoSetObjetoDestino));
						Object novoObjeto = classeTipoGenericoDoMetodoSetDoObjetoDestino.newInstance();
						novaCollection.add(novoObjeto);
						copiarPropriedadesDoObjetoReferenciaParaOObjetoDestino(novoObjeto, o,
								((Class<?>) obterTipoDaCollectionDoRetornoDoMetodoGet(metodoGetInteface)));
					}
					retornoInvocacaoMetodoGet = novaCollection;
				}
				metodoSetObjetoDestino.invoke(objetoDestino, retornoInvocacaoMetodoGet);

			} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private static Type obterTipoDaCollectionPassadaParaOMetodoSet(Method metodoSetObjetoDestino) {
		Type type = ((ParameterizedType) metodoSetObjetoDestino.getGenericParameterTypes()[0])
				.getActualTypeArguments()[0];
		return type;
	}

	private static Type obterTipoDaCollectionDoRetornoDoMetodoGet(Method metodoGet) {
		Type type = ((ParameterizedType) metodoGet.getGenericReturnType()).getActualTypeArguments()[0];
		return type;
	}

	public static Class<?> obterGenericTypeDoFieldComNomeReferencia(Class<?> clazz, String nomeField)
			throws NoSuchFieldException, SecurityException {
		Field field = clazz.getDeclaredField(nomeField);
		ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
		Class<?> genericType = (Class<?>) stringListType.getActualTypeArguments()[0];
		return genericType;
	}

}
