package br.com.gruposaga.cad.svs.servico;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.OpcaoItemCbc;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OpcaoItemCbcService extends Service<OpcaoItemCbc> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<OpcaoItemCbc> getClassOfT() {
		return OpcaoItemCbc.class;
	}

	public List<OpcaoItemCbc> obterPorIdUnidadeOrganizacional(Long idUnidadeOrganizacional) {
		List<OpcaoItemCbc> listaDeOpcoes = crudService.findWithNamedQuery(
				OpcaoItemCbc.NQ_OBTER_POR_ID_UNIDADE_ORGANIZACIONAL.NAME,
				QueryParameter.with(OpcaoItemCbc.NQ_OBTER_POR_ID_UNIDADE_ORGANIZACIONAL.NM_PM_ID_UNIDADE_ORGANIZACIONAL,
						idUnidadeOrganizacional).parameters());

		return listaDeOpcoes;
	}

}
