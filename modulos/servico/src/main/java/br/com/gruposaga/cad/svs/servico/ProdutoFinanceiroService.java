package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.ProdutoFinanceiro;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class ProdutoFinanceiroService extends Service<ProdutoFinanceiro> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<ProdutoFinanceiro> getClassOfT() {
		return ProdutoFinanceiro.class;
	}

}
