package br.com.gruposaga.cad.svs.servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.enums.EnumTipoTabelaFinanceira;
import com.svs.fin.model.entities.TabelaFinanceira;
import com.svs.fin.model.entities.UnidadeOrganizacional;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;
import br.com.gruposaga.cad.svs.utils.UtilsEnum;

@Stateless
public class TabelaFinanceiraService extends Service<TabelaFinanceira> {

	@Inject
	private CrudService crudService;

	@Inject
	private UnidadeOrganizacionalService unidadeOrganizacionalService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<TabelaFinanceira> getClassOfT() {
		return TabelaFinanceira.class;
	}

	@Override
	public void alterar(TabelaFinanceira tabelaFinanceira) {
		tabelaFinanceira.getTaxas().forEach(taxa -> taxa.setTabelaFinanceira(tabelaFinanceira));
		attachUnidadesOrganizacionais(tabelaFinanceira);
		super.alterar(tabelaFinanceira);
	}

	public List<TabelaFinanceira> obterPorEstadoUnidadeOrganizacionalTipoEQntdParcelas(Boolean ativo,
			Long idUnidadeOrganizacional, String tipo, Integer qtdParcelas) {
		List<TabelaFinanceira> tabelasFinanceiras = getCrudService().findWithNamedQuery(
				TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NAME,
				QueryParameter.with(
						TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_ESTADO_ATIVO,
						ativo)
						.and(TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_ID_UNIDADE_ORGANIZACIONAL,
								idUnidadeOrganizacional)
						.and(TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_TIPO,
								UtilsEnum.obterEnumLabeledPorLabel(EnumTipoTabelaFinanceira.values(), tipo))
						.and(TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO_COM_E_TAXAS_POR_PARCELA.NM_PM_QTD_PARCELAS,
								qtdParcelas)
						.parameters());
		return tabelasFinanceiras;
	}

	public List<TabelaFinanceira> obterPorEstadoUnidadeOrganizacionalETipo(Boolean ativo, Long idUnidadeOrganizacional,
			String tipo) {
		List<TabelaFinanceira> tabelasFinanceiras = getCrudService().findWithNamedQuery(
				TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NAME,
				QueryParameter
						.with(TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NM_PM_ESTADO_ATIVO,
								ativo)
						.and(TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NM_PM_ID_UNIDADE_ORGANIZACIONAL,
								idUnidadeOrganizacional)
						.and(TabelaFinanceira.NQ_OBTER_POR_ESTADO_UNIDADE_ORGANIZACIONAL_E_TIPO.NM_PM_TIPO,
								UtilsEnum.obterEnumLabeledPorLabel(EnumTipoTabelaFinanceira.values(), tipo))
						.parameters());
		return tabelasFinanceiras;
	}

	@Override
	public void incluir(TabelaFinanceira tabelaFinanceira) {
		tabelaFinanceira.getTaxas().forEach(taxa -> taxa.setTabelaFinanceira(tabelaFinanceira));
		attachUnidadesOrganizacionais(tabelaFinanceira);
		super.incluir(tabelaFinanceira);
	}

	private void attachUnidadesOrganizacionais(TabelaFinanceira tabelaFinanceira) {
		// for (UnidadeOrganizacional unidadeOrganizacional :
		// tabelaFinanceira.getUnidades()) {
		List<UnidadeOrganizacional> unidadesOrganizacionais = new ArrayList<>();
		tabelaFinanceira.getUnidades().forEach(
				unidade -> unidadesOrganizacionais.add(unidadeOrganizacionalService.obterPorId(unidade.getId())));
		tabelaFinanceira.setUnidades(unidadesOrganizacionais);
		// }
	}

}
