package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.TaxaPerfilInstituicaoFinanceira;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class TaxaPerfilInstituicaoFinanceiraService extends Service<TaxaPerfilInstituicaoFinanceira> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<TaxaPerfilInstituicaoFinanceira> getClassOfT() {
		return TaxaPerfilInstituicaoFinanceira.class;
	}

}
