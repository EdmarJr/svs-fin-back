package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.AcessUser;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AcessUserService extends Service<AcessUser> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<AcessUser> getClassOfT() {
		return AcessUser.class;
	}

	public AcessUser obterPorCpf(String cpf) {
		return crudService.findSingleResultWithNamedQuery(AcessUser.NQ_OBTER_POR_CPF.NAME,
				QueryParameter.with(AcessUser.NQ_OBTER_POR_CPF.NM_PM_CPF, cpf).parameters());
	}

}
