
package  br.com.gruposaga.cad.svs.servico.integracao.ws.ws.proposta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Usuario_identificador" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usuariosenha_senha" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Xml" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "usuarioIdentificador",
    "usuariosenhaSenha",
    "xml"
})
@XmlRootElement(name = "WS_PropostaDados.CONSULTAFATVEC")
public class WSPropostaDadosCONSULTAFATVEC {

    @XmlElement(name = "Usuario_identificador", required = true)
    protected String usuarioIdentificador;
    @XmlElement(name = "Usuariosenha_senha", required = true)
    protected String usuariosenhaSenha;
    @XmlElement(name = "Xml", required = true)
    protected String xml;

    /**
     * Obt�m o valor da propriedade usuarioIdentificador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioIdentificador() {
        return usuarioIdentificador;
    }

    /**
     * Define o valor da propriedade usuarioIdentificador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioIdentificador(String value) {
        this.usuarioIdentificador = value;
    }

    /**
     * Obt�m o valor da propriedade usuariosenhaSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuariosenhaSenha() {
        return usuariosenhaSenha;
    }

    /**
     * Define o valor da propriedade usuariosenhaSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuariosenhaSenha(String value) {
        this.usuariosenhaSenha = value;
    }

    /**
     * Obt�m o valor da propriedade xml.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXml() {
        return xml;
    }

    /**
     * Define o valor da propriedade xml.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXml(String value) {
        this.xml = value;
    }

}
