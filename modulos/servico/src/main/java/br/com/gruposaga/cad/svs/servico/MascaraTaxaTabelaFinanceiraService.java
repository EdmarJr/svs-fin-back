package br.com.gruposaga.cad.svs.servico;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;

import com.svs.fin.enums.EnumBancoIntegracaoOnline;
import com.svs.fin.model.entities.MascaraTaxaTabelaFinanceira;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;
import br.com.gruposaga.cad.svs.dao.generic.QueryParameter;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MascaraTaxaTabelaFinanceiraService extends Service<MascaraTaxaTabelaFinanceira> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<MascaraTaxaTabelaFinanceira> getClassOfT() {
		return MascaraTaxaTabelaFinanceira.class;
	}
	
	public MascaraTaxaTabelaFinanceira obterPorMascaraRetornoBanco(String descricaoMascara, String codigoRetorno, EnumBancoIntegracaoOnline bancoIntegracao) {
		if(descricaoMascara == null || codigoRetorno == null || descricaoMascara.trim().equals("") || codigoRetorno.trim().equals("")) {
			return null;
		}
		List<MascaraTaxaTabelaFinanceira> mascaraTaxaTabelaFinanceira = getCrudService().findWithNamedQuery(
				MascaraTaxaTabelaFinanceira.NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NAME,
				QueryParameter
						.with(MascaraTaxaTabelaFinanceira.NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NM_PM_MASCARA, descricaoMascara)
						.and(MascaraTaxaTabelaFinanceira.NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NM_PM_CODIGO_RETORNO, codigoRetorno)
						.and(MascaraTaxaTabelaFinanceira.NQ_OBTER_POR_MASCARA_RETORNO_BANCO.NM_PM_BANCO, bancoIntegracao)
						.parameters());
		
		return mascaraTaxaTabelaFinanceira.size() > 0 ? mascaraTaxaTabelaFinanceira.get(0) : null;
	}
	
	public MascaraTaxaTabelaFinanceira incluir(String descricaoMascara, String codigoRetorno, EnumBancoIntegracaoOnline bancoIntegracao) {

		MascaraTaxaTabelaFinanceira mascara = new MascaraTaxaTabelaFinanceira();
		mascara.setAtivo(Boolean.TRUE);
		mascara.setCodigoRetorno(codigoRetorno);
		mascara.setMascara(descricaoMascara);
		mascara.setOrdem(new Integer (codigoRetorno));
		mascara.setBancoIntegracaoOnline(bancoIntegracao);

		incluir(mascara);

		return mascara;
	}

}
