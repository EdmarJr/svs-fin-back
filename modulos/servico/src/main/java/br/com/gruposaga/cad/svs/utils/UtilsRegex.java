package br.com.gruposaga.cad.svs.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsRegex {
	public static boolean seStringMatchRegex(String regex, String textoToMatch) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(textoToMatch);
		return matcher.matches();
	}

}
