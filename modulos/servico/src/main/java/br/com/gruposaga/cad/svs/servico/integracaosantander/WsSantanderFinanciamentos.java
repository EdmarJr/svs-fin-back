package br.com.gruposaga.cad.svs.servico.integracaosantander;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.svs.fin.integracaosantander.santander.IdentificationRequest;
import com.svs.fin.integracaosantander.santander.PreAnaliseResponse;
import com.svs.fin.integracaosantander.santander.PropostaCadastroRequest;
import com.svs.fin.integracaosantander.santander.PropostaCadastroResponse;
import com.svs.fin.integracaosantander.santander.SimulationRequest;
import com.svs.fin.integracaosantander.santander.SimulationResponse;
import com.svs.fin.integracaosantander.santander.TipoDocumentoSantander;
import com.svs.fin.integracaosantander.santander.TokenSantander;
import com.svs.fin.integracaosantander.santander.dto.AgenteCertificadoDTO;
import com.svs.fin.integracaosantander.santander.dto.AnoModeloCombustivelDTO;
import com.svs.fin.integracaosantander.santander.dto.BancoSantander;
import com.svs.fin.integracaosantander.santander.dto.EnderecoDTO;
import com.svs.fin.integracaosantander.santander.dto.FormaPagamentoSantander;
import com.svs.fin.integracaosantander.santander.dto.GeneroDTO;
import com.svs.fin.integracaosantander.santander.dto.GrauParentescoDTO;
import com.svs.fin.integracaosantander.santander.dto.MarcaDTO;
import com.svs.fin.integracaosantander.santander.dto.ModeloDTO;
import com.svs.fin.integracaosantander.santander.dto.NaturalidadeDTO;
import com.svs.fin.integracaosantander.santander.dto.NaturezaJuridicaSantander;
import com.svs.fin.integracaosantander.santander.dto.PorteEmpresaDTO;
import com.svs.fin.integracaosantander.santander.dto.StatusPropostaRequestDTO;
import com.svs.fin.integracaosantander.santander.dto.StatusPropostaResponseDTO;
import com.svs.fin.integracaosantander.santander.dto.TabIdDTO;
import com.svs.fin.integracaosantander.santander.dto.TipoRelacionamentoEmpresaSantander;
import com.svs.fin.model.entities.AtividadeEconomicaSantander;
import com.svs.fin.model.entities.EstadoCivilSantander;
import com.svs.fin.model.entities.EstadoSantander;
import com.svs.fin.model.entities.MarcaSantander;
import com.svs.fin.model.entities.MunicipioSantander;
import com.svs.fin.model.entities.NacionalidadeSantander;
import com.svs.fin.model.entities.ProfissaoSantander;
import com.svs.fin.model.entities.TipoAtividadeEconomicaSantander;
import com.svs.fin.utils.Uteis;

public class WsSantanderFinanciamentos {

	private TokenSantander tokenIntegracao;
	private Gson gson;

	public WsSantanderFinanciamentos() {
		tokenIntegracao = gerarToken();
		gson = new Gson();
	}

	public TokenSantander gerarToken() {

		try {

			HttpPost httpPost = new HttpPost("https://viverebrasil.com.br/apigwpro/uaa/pro/auth/token");
			httpPost.setHeader("Content-Type", "application/json");
			httpPost.setHeader("Authorization", "Basic aW50ZWdyYV9jbGk6MTIzNDU2");

			httpPost.setEntity(
					new StringEntity("{ \"username\": \"INTEGRA_GPOSAGA\", \"password\": \"Financeir@246\" }"));

			try {
				CloseableHttpClient httpclient = HttpClients.createDefault();
				CloseableHttpResponse response2 = httpclient.execute(httpPost);

				String token = EntityUtils.toString(response2.getEntity(), "UTF-8");

				JsonObject jobj = new Gson().fromJson(token, JsonObject.class);
				String tokenTratado = jobj.get("access_token").toString().replaceAll("\"", "");
				String tokenType = jobj.get("token_type").toString().replaceAll("\"", "");
				Long expiresIn = new Long(jobj.get("expires_in").toString().replaceAll("\"", ""));

				Date dataExpiracao = new Date(expiresIn);

				TokenSantander tokenSantander = new TokenSantander();
				tokenSantander.setAcessToken(tokenTratado);
				tokenSantander.setTokenType(tokenType);
				tokenSantander.setExpiresIn(dataExpiracao);

				return tokenSantander;

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			return null;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public TabIdDTO getTabId(String integrationCode) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/tab/code/" + integrationCode);

		Invocation.Builder invocationBuilder = target.request();
		invocationBuilder.header("Authorization",
				getTokenIntegracao().getTokenType() + " " + getTokenIntegracao().getAcessToken());

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			TabIdDTO jobj = new Gson().fromJson(resultado, TabIdDTO.class);

			return jobj;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getCET(String uuid) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/simulation/vehicle/" + uuid + "/cet");

		Invocation.Builder invocationBuilder = target.request();
		invocationBuilder.header("Authorization",
				getTokenIntegracao().getTokenType() + " " + getTokenIntegracao().getAcessToken());

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			JsonObject jobj = new Gson().fromJson(resultado, JsonObject.class);
			String bytesPdfCET = jobj.get("base64").toString().replaceAll("\"", "");

			return bytesPdfCET;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param tipoVeiculo:
	 *            Código id do tipo de veículo, utilizar os domínios: C = Carros e
	 *            Utilitários M = Motos
	 * @return
	 */
	public List<MarcaDTO> getMarcasDisponiveis(String tipoVeiculo) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/vehicle-makes/" + tipoVeiculo);
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<MarcaDTO>>() {
			}.getType();
			List<MarcaDTO> marcas = (List<MarcaDTO>) getGson().fromJson(resultado, listType);

			return marcas;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<ModeloDTO> getModelosDisponiveis(MarcaSantander marca) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target(
				"https://viverebrasil.com.br/apigwpro/api/pro/domains/vehicle-make/" + marca.getId().toString());
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<ModeloDTO>>() {
			}.getType();

			System.out.println("Buscando modelos.. marca " + marca.getDescription());
			List<ModeloDTO> modelos = (List<ModeloDTO>) gson.fromJson(resultado, listType);
			System.out.println(modelos.size() + " modelos encontrados.. marca " + marca.getDescription());

			return modelos;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<AnoModeloCombustivelDTO> getAnosModeloCombustivelDoModelo(ModeloDTO modeloDto) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target(
				"https://viverebrasil.com.br/apigwpro/api/pro/domains/vehicle-model/" + modeloDto.getId().toString());
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();
		Response response = invocationBuilder.get();
		String resultado = response.readEntity(String.class);

		try {

			if (resultado.contains("Vehicle model not found")) {
				System.out.println("Anos/modelos n�o encontrados - MODELO: " + modeloDto.getDescription() + " - ID: "
						+ modeloDto.getId());
				return null;
			}

			Type listType = new TypeToken<List<AnoModeloCombustivelDTO>>() {
			}.getType();
			List<AnoModeloCombustivelDTO> anosModeloCombustivel = (List<AnoModeloCombustivelDTO>) gson
					.fromJson(resultado, listType);

			return anosModeloCombustivel;

		} catch (Exception e) {

			System.out.println("Erro ao converter JSON: " + resultado);
			e.printStackTrace();
		}

		return null;
	}

	public List<FormaPagamentoSantander> getFormasPagamento(String productCode) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/forms-payment/" + productCode);
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<FormaPagamentoSantander>>() {
			}.getType();
			List<FormaPagamentoSantander> formasPagamento = (List<FormaPagamentoSantander>) gson.fromJson(resultado,
					listType);

			for (FormaPagamentoSantander formaPagamento : formasPagamento) {
				formaPagamento.setCodigoProduto(productCode);
			}

			return formasPagamento;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<TipoAtividadeEconomicaSantander> getOpcoesTipoAtividade() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/economicActivityGroup");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<TipoAtividadeEconomicaSantander>>() {
			}.getType();
			List<TipoAtividadeEconomicaSantander> tipos = (List<TipoAtividadeEconomicaSantander>) gson
					.fromJson(resultado, listType);

			return tipos;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<EstadoSantander> getOpcoesEstado() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/states");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<EstadoSantander>>() {
			}.getType();
			List<EstadoSantander> estados = (List<EstadoSantander>) gson.fromJson(resultado, listType);

			return estados;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<MunicipioSantander> getOpcoesMunicipio(String estado) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/naturalness/" + estado);
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<MunicipioSantander>>() {
			}.getType();
			List<MunicipioSantander> municipios = (List<MunicipioSantander>) gson.fromJson(resultado, listType);

			return municipios;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<AtividadeEconomicaSantander> getOpcoesAtividadeEconomica(String tipoAtividadeId) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/economicActivity/" + tipoAtividadeId);
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<AtividadeEconomicaSantander>>() {
			}.getType();
			List<AtividadeEconomicaSantander> atividadesEconomicas = (List<AtividadeEconomicaSantander>) gson
					.fromJson(resultado, listType);

			return atividadesEconomicas;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<PorteEmpresaDTO> getOpcoesPorteEmpresa() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/companySize/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<PorteEmpresaDTO>>() {
			}.getType();
			List<PorteEmpresaDTO> opcoesPorteEmpresa = (List<PorteEmpresaDTO>) gson.fromJson(resultado, listType);

			for (PorteEmpresaDTO forma : opcoesPorteEmpresa) {
				System.out.println(forma.getDescription());
			}

			return opcoesPorteEmpresa;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<NaturezaJuridicaSantander> getOpcoesNaturezaJuridica() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/legalNature/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<NaturezaJuridicaSantander>>() {
			}.getType();
			List<NaturezaJuridicaSantander> opcoesNaturezaJuridica = (List<NaturezaJuridicaSantander>) gson
					.fromJson(resultado, listType);

			return opcoesNaturezaJuridica;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<TipoRelacionamentoEmpresaSantander> getOpcoesTipoRelacionamento() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/relationshipTypeCompany/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<TipoRelacionamentoEmpresaSantander>>() {
			}.getType();
			List<TipoRelacionamentoEmpresaSantander> opcoesTipoRelacionamento = (List<TipoRelacionamentoEmpresaSantander>) gson
					.fromJson(resultado, listType);

			return opcoesTipoRelacionamento;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<BancoSantander> getOpcoesBanco() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/bankName/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<BancoSantander>>() {
			}.getType();
			List<BancoSantander> opcoesBanco = (List<BancoSantander>) gson.fromJson(resultado, listType);

			return opcoesBanco;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<ProfissaoSantander> getOpcoesProfissao() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/role/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<ProfissaoSantander>>() {
			}.getType();
			List<ProfissaoSantander> opcoesProfissao = (List<ProfissaoSantander>) gson.fromJson(resultado, listType);

			return opcoesProfissao;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<EstadoCivilSantander> getOpcoesEstadoCivil() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/maritalStatus/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<EstadoCivilSantander>>() {
			}.getType();
			List<EstadoCivilSantander> opcoesEstadoCivil = (List<EstadoCivilSantander>) gson.fromJson(resultado,
					listType);

			return opcoesEstadoCivil;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<GrauParentescoDTO> getOpcoesGrauParentesco() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/kinshipDegree/");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<GrauParentescoDTO>>() {
			}.getType();
			List<GrauParentescoDTO> opcoesGrauParentesco = (List<GrauParentescoDTO>) gson.fromJson(resultado, listType);

			for (GrauParentescoDTO grauParentesco : opcoesGrauParentesco) {
				System.out.println(grauParentesco.getDescription());
			}
			return opcoesGrauParentesco;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<NaturalidadeDTO> getOpcoesNaturalidade(String codEstado) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/naturalness/" + codEstado);
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<NaturalidadeDTO>>() {
			}.getType();
			List<NaturalidadeDTO> naturalidades = (List<NaturalidadeDTO>) gson.fromJson(resultado, listType);

			return naturalidades;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<NacionalidadeSantander> getOpcoesNacionalidade() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/nationality");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<NacionalidadeSantander>>() {
			}.getType();
			List<NacionalidadeSantander> nacionalidades = (List<NacionalidadeSantander>) gson.fromJson(resultado,
					listType);

			return nacionalidades;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<GeneroDTO> getOpcoesGenero() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/gender");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<GeneroDTO>>() {
			}.getType();
			List<GeneroDTO> generos = (List<GeneroDTO>) gson.fromJson(resultado, listType);

			return generos;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<TipoDocumentoSantander> getOpcoesTipoDocumento() {

		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/documentType");
		target.request().header("Authorization", getTokenIntegracao().getAcessToken());
		target.request().header("Content_type", "application/json");

		Invocation.Builder invocationBuilder = target.request();

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<TipoDocumentoSantander>>() {
			}.getType();
			List<TipoDocumentoSantander> tiposDocumento = (List<TipoDocumentoSantander>) gson.fromJson(resultado,
					listType);

			return tiposDocumento;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<AgenteCertificadoDTO> getOpcoesAgenteCertificado(String idFornecCanal) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/domains/certifiedAgent/" + idFornecCanal);

		Invocation.Builder invocationBuilder = target.request();
		invocationBuilder.header("Authorization",
				getTokenIntegracao().getTokenType() + " " + getTokenIntegracao().getAcessToken());
		invocationBuilder.header("Content_type", "application/json");

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			Type listType = new TypeToken<List<AgenteCertificadoDTO>>() {
			}.getType();
			List<AgenteCertificadoDTO> agentes = (List<AgenteCertificadoDTO>) gson.fromJson(resultado, listType);

			return agentes;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public EnderecoDTO buscarEndereco(String cep) {

		Client client = ClientBuilder.newClient();

		WebTarget target = client
				.target("https://viverebrasil.com.br/apigwpro/api/pro/address/zipcode/" + cep + "/address/");

		Invocation.Builder invocationBuilder = target.request();
		invocationBuilder.header("Authorization",
				getTokenIntegracao().getTokenType() + " " + getTokenIntegracao().getAcessToken());
		invocationBuilder.header("Content_type", "application/json");

		try {
			Response response = invocationBuilder.get();
			String resultado = response.readEntity(String.class);

			EnderecoDTO endereco = (EnderecoDTO) gson.fromJson(resultado, EnderecoDTO.class);

			return endereco;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public PreAnaliseResponse fazerIdentificacaoPreAnalise(IdentificationRequest identification) throws Exception {

		HttpPost httpPost = new HttpPost("https://viverebrasil.com.br/apigwpro/api/pro/identification/vehicle/");
		setHeaderToken(httpPost);

		String cpfNaoTratado = identification.getCustomer().getDocument();
		String cpfTratado = Uteis.removeCaracteresCpfCnpj(identification.getCustomer().getDocument());
		identification.getCustomer().setDocument(cpfTratado);

		Gson gson = new Gson();
		String jsonIdentification = gson.toJson(identification);

		byte ptext[] = jsonIdentification.getBytes("ISO-8859-1");
		jsonIdentification = new String(ptext, "UTF-8");

		System.out.println("JSON json preanalise: " + jsonIdentification);

		httpPost.setEntity(new StringEntity(jsonIdentification));

		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = httpclient.execute(httpPost);

		String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");

		// if(!responseString.contains("atxResponse")) {
		// if(responseString.toUpperCase().contains("MESSAGE")) {
		// JsonObject jobj = new Gson().fromJson(responseString, JsonObject.class);
		// String message = jobj.get("message") != null ?
		// jobj.get("message").toString().replaceAll("\"", "") :
		// jobj.get("errors") != null ? jobj.get("errors").toString() : jobj.toString();
		// }
		// }
		//
		System.out.println("JSON json preanalise response: " + responseString);

		PreAnaliseResponse simulation = new Gson().fromJson(responseString, PreAnaliseResponse.class);
		simulation.setJson(responseString);

		return simulation;
	}

	public SimulationResponse fazerSimulacao(SimulationRequest simulationReq) throws Exception {

		Calendar dataNascimento = null;
		String responseString = null;
		SimulationResponse simulationResponse = null;
		String cpfTratado = Uteis.removeCaracteresCpfCnpj(simulationReq.getCustomer().getDocument());
		simulationReq.getCustomer().setDocument(cpfTratado);

		try {

			dataNascimento = simulationReq.getCustomer().getDataNascimento();
			simulationReq.getCustomer().setDataNascimento(null);

			HttpPost httpPost = new HttpPost("https://viverebrasil.com.br/apigwpro/api/pro/simulation/vehicle/");
			setHeaderToken(httpPost);

			String jsonSimulation = gson.toJson(simulationReq);

			byte ptext[] = jsonSimulation.getBytes("ISO-8859-1");
			jsonSimulation = new String(ptext, "UTF-8");

			System.out.println("JSON simulacao request: " + jsonSimulation);

			httpPost.setEntity(new StringEntity(jsonSimulation));

			CloseableHttpClient httpclient = HttpClients.createDefault();
			CloseableHttpResponse response = httpclient.execute(httpPost);

			responseString = EntityUtils.toString(response.getEntity(), "UTF-8");

			System.out.println("JSON simulacao response: " + responseString);

			responseString = responseString.substring(22, responseString.length() - 1);
			simulationResponse = new Gson().fromJson(responseString, SimulationResponse.class);

		} catch (Exception e) {

			simulationReq.getCustomer().setDataNascimento(dataNascimento);
			throw new Exception(responseString);

		} finally {

			simulationReq.getCustomer().setDataNascimento(dataNascimento);
		}
		return simulationResponse;
	}

	public PropostaCadastroResponse enviarProposta(PropostaCadastroRequest proposta) throws Exception {

		HttpPost httpPost = new HttpPost("https://viverebrasil.com.br/apigwpro/api/pro/capture/vehicle/proposal");
		setHeaderToken(httpPost);

		proposta.getCustomer().setUuid(null);

		String jsonProposta = gson.toJson(proposta);

		byte ptext[] = jsonProposta.getBytes("ISO-8859-1");
		jsonProposta = new String(ptext, "UTF-8");

		System.out.println("JSON envio proposta request utf8: " + jsonProposta);

		httpPost.setEntity(new StringEntity(jsonProposta));

		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = httpclient.execute(httpPost);

		String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");

		System.out.println("JSON envio proposta response: " + responseString);

		PropostaCadastroResponse propostaCadastroResponse = null;

		if (responseString.length() > 1) {
			propostaCadastroResponse = gson.fromJson(responseString, PropostaCadastroResponse.class);
		} else {
			propostaCadastroResponse = new PropostaCadastroResponse();
		}

		return propostaCadastroResponse;
	}

	public StatusPropostaResponseDTO consultarStatusProposta(StatusPropostaRequestDTO statusRequest, String idProposta)
			throws Exception {

		HttpPost httpPost = new HttpPost("https://viverebrasil.com.br/apigwpro/api/pro/status/proposal/" + idProposta);
		setHeaderToken(httpPost);

		Gson gson = new Gson();
		String jsonProposta = gson.toJson(statusRequest);

		httpPost.setEntity(new StringEntity(jsonProposta));

		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = httpclient.execute(httpPost);

		String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");

		StatusPropostaResponseDTO statusPropostaResponse = null;

		if (responseString.length() > 1) {
			statusPropostaResponse = gson.fromJson(responseString, StatusPropostaResponseDTO.class);
		} else {
			statusPropostaResponse = new StatusPropostaResponseDTO();
		}

		return statusPropostaResponse;
	}

	private void setHeaderToken(HttpPost httpPost) {
		httpPost.setHeader("Content-Type", "application/json");
		httpPost.setHeader("Authorization",
				getTokenIntegracao().getTokenType() + " " + getTokenIntegracao().getAcessToken());
	}

	public TokenSantander getTokenIntegracao() {
		Date dataAtual = new Date();
		if (tokenIntegracao == null || dataAtual.getTime() >= tokenIntegracao.getExpiresIn().getTime()) {
			tokenIntegracao = gerarToken();
		}
		return tokenIntegracao;
	}

	public void setTokenIntegracao(TokenSantander tokenIntegracao) {
		this.tokenIntegracao = tokenIntegracao;
	}

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}
}
