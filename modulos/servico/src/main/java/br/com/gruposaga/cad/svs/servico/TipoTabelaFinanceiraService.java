package br.com.gruposaga.cad.svs.servico;


import javax.ejb.Stateless;
import javax.inject.Inject;

import com.svs.fin.model.entities.TipoTabelaFinanceira;

import br.com.gruposaga.cad.svs.dao.generic.CrudService;

@Stateless
public class TipoTabelaFinanceiraService extends Service<TipoTabelaFinanceira>{
	
	@Inject
	private CrudService crudService;
	
	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<TipoTabelaFinanceira> getClassOfT() {
		return TipoTabelaFinanceira.class;
	}
	
	
	
	
}
