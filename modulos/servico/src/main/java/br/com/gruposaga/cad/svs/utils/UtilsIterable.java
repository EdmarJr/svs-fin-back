package br.com.gruposaga.cad.svs.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UtilsIterable {
	public static Boolean sePrimeiraIteracao(int indiceDaIteracao) {
		return indiceDaIteracao == 0;
	}

	public static Boolean seNaoIsUltimaIteracao(int tamanhoLista, int indiceDaIteracao) {
		return indiceDaIteracao != (tamanhoLista - 1);
	}

	public static <T> T[] obterItensArrayAPartirDaPosicaoReferencia(T[] array, int posicaoReferencia) {
		List<T> retorno = new ArrayList<T>();
		for (int k = posicaoReferencia; k < array.length; k++) {
			retorno.add(array[k]);
		}
		T[] copyOf = Arrays.copyOf(array, array.length - posicaoReferencia);
		return (T[]) retorno.toArray(copyOf);
	}

	/**
	 * utiliza o indexOf como método de comparação. Assim, é necessário implementar
	 * equals and haschode
	 **/
	public static <T> ArrayList<T> removerItensDuplicadosNaLista(List<T> resultados) {
		ArrayList<T> retorno = new ArrayList<T>();
		resultados.forEach(r -> {
			if (retorno.indexOf(r) == -1) {
				retorno.add(r);
			}
		});
		return retorno;
	}
}
