/**
 * CapturaPropostas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface CapturaPropostas extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_ICapturaPropostasAddress();

    public org.tempuri.ICapturaPropostas getBasicHttpBinding_ICapturaPropostas() throws javax.xml.rpc.ServiceException;

    public org.tempuri.ICapturaPropostas getBasicHttpBinding_ICapturaPropostas(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
