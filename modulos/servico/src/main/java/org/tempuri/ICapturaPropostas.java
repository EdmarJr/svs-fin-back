/**
 * ICapturaPropostas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface ICapturaPropostas extends java.rmi.Remote {
    public com.svs.fin.integracao.safra.model.RetornoProposta enviar_Proposta_PF(java.lang.String p_ds_hash_simul, com.svs.fin.integracao.safra.model.ValidaHASH validahash, com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum, com.svs.fin.integracao.safra.model.Proposta_PF propostaPf, com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas, com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.RetornoProposta enviar_Proposta_PJ(java.lang.String p_ds_hash_simul, com.svs.fin.integracao.safra.model.ValidaHASH validahash, com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum, com.svs.fin.integracao.safra.model.Proposta_PJ propostaPj, com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas, com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.RetornoConsultaProposta proposta_Decisao(com.svs.fin.integracao.safra.model.ValidaHASH validahash, java.lang.String id_proposta) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.ClienteValores clientes_Valores(java.lang.String dsHASH, java.lang.String id_cliente, java.lang.String id_produto, java.lang.String id_lojista, java.lang.String id_uf) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.DadosBasicosSafra dados_Safra(java.lang.String dshashEmpFEI) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.RetornoTabelasRevenda consultarTabelas(com.svs.fin.integracao.safra.model.ValidaHASH object) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.RetornoCalculoParcelas consultarFluxoParcelas(com.svs.fin.integracao.safra.model.ParamCalculaParcelas object) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.RetornoOperadores consultarOperadores(com.svs.fin.integracao.safra.model.ValidaHASH object) throws java.rmi.RemoteException;
    public com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[] consultarDocumentosPendentes(java.lang.String dsHASH, java.lang.String id_proposta) throws java.rmi.RemoteException;
}
