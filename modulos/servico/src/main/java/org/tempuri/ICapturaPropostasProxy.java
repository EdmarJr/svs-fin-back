package org.tempuri;

public class ICapturaPropostasProxy implements org.tempuri.ICapturaPropostas {
  private String _endpoint = null;
  private org.tempuri.ICapturaPropostas iCapturaPropostas = null;
  
  public ICapturaPropostasProxy() {
    _initICapturaPropostasProxy();
  }
  
  public ICapturaPropostasProxy(String endpoint) {
    _endpoint = endpoint;
    _initICapturaPropostasProxy();
  }
  
  private void _initICapturaPropostasProxy() {
    try {
      iCapturaPropostas = (new org.tempuri.CapturaPropostasLocator()).getBasicHttpBinding_ICapturaPropostas();
      if (iCapturaPropostas != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iCapturaPropostas)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iCapturaPropostas)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iCapturaPropostas != null)
      ((javax.xml.rpc.Stub)iCapturaPropostas)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.ICapturaPropostas getICapturaPropostas() {
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas;
  }
  
  public com.svs.fin.integracao.safra.model.RetornoProposta enviar_Proposta_PF(java.lang.String p_ds_hash_simul, com.svs.fin.integracao.safra.model.ValidaHASH validahash, com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum, com.svs.fin.integracao.safra.model.Proposta_PF propostaPf, com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas, com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.enviar_Proposta_PF(p_ds_hash_simul, validahash, propostaComum, propostaPf, avalistas, propostaFinanciamento);
  }
  
  public com.svs.fin.integracao.safra.model.RetornoProposta enviar_Proposta_PJ(java.lang.String p_ds_hash_simul, com.svs.fin.integracao.safra.model.ValidaHASH validahash, com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum, com.svs.fin.integracao.safra.model.Proposta_PJ propostaPj, com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas, com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.enviar_Proposta_PJ(p_ds_hash_simul, validahash, propostaComum, propostaPj, avalistas, propostaFinanciamento);
  }
  
  public com.svs.fin.integracao.safra.model.RetornoConsultaProposta proposta_Decisao(com.svs.fin.integracao.safra.model.ValidaHASH validahash, java.lang.String id_proposta) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.proposta_Decisao(validahash, id_proposta);
  }
  
  public com.svs.fin.integracao.safra.model.ClienteValores clientes_Valores(java.lang.String dsHASH, java.lang.String id_cliente, java.lang.String id_produto, java.lang.String id_lojista, java.lang.String id_uf) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.clientes_Valores(dsHASH, id_cliente, id_produto, id_lojista, id_uf);
  }
  
  public com.svs.fin.integracao.safra.model.DadosBasicosSafra dados_Safra(java.lang.String dshashEmpFEI) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.dados_Safra(dshashEmpFEI);
  }
  
  public com.svs.fin.integracao.safra.model.RetornoTabelasRevenda consultarTabelas(com.svs.fin.integracao.safra.model.ValidaHASH object) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.consultarTabelas(object);
  }
  
  public com.svs.fin.integracao.safra.model.RetornoCalculoParcelas consultarFluxoParcelas(com.svs.fin.integracao.safra.model.ParamCalculaParcelas object) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.consultarFluxoParcelas(object);
  }
  
  public com.svs.fin.integracao.safra.model.RetornoOperadores consultarOperadores(com.svs.fin.integracao.safra.model.ValidaHASH object) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.consultarOperadores(object);
  }
  
  public com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[] consultarDocumentosPendentes(java.lang.String dsHASH, java.lang.String id_proposta) throws java.rmi.RemoteException{
    if (iCapturaPropostas == null)
      _initICapturaPropostasProxy();
    return iCapturaPropostas.consultarDocumentosPendentes(dsHASH, id_proposta);
  }
  
  
}