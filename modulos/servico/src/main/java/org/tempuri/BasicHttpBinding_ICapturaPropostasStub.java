/**
 * BasicHttpBinding_ICapturaPropostasStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class BasicHttpBinding_ICapturaPropostasStub extends org.apache.axis.client.Stub implements org.tempuri.ICapturaPropostas {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[9];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Enviar_Proposta_PF");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "p_ds_hash_simul"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "validahash"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH"), com.svs.fin.integracao.safra.model.ValidaHASH.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "propostaComum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cadastro_Comum"), com.svs.fin.integracao.safra.model.Cadastro_Comum.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "propostaPf"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_PF"), com.svs.fin.integracao.safra.model.Proposta_PF.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "avalistas"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_Avalista"), com.svs.fin.integracao.safra.model.Proposta_Avalista.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "propostaFinanciamento"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Financiamento"), com.svs.fin.integracao.safra.model.Financiamento.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoProposta"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoProposta.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "Enviar_Proposta_PFResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Enviar_Proposta_PJ");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "p_ds_hash_simul"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "validahash"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH"), com.svs.fin.integracao.safra.model.ValidaHASH.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "propostaComum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cadastro_Comum"), com.svs.fin.integracao.safra.model.Cadastro_Comum.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "propostaPj"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_PJ"), com.svs.fin.integracao.safra.model.Proposta_PJ.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "avalistas"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_Avalista"), com.svs.fin.integracao.safra.model.Proposta_Avalista.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "propostaFinanciamento"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Financiamento"), com.svs.fin.integracao.safra.model.Financiamento.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoProposta"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoProposta.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "Enviar_Proposta_PJResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Proposta_Decisao");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "validahash"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH"), com.svs.fin.integracao.safra.model.ValidaHASH.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "id_proposta"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoConsultaProposta"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoConsultaProposta.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "Proposta_DecisaoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Clientes_Valores");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "dsHASH"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "id_cliente"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "id_produto"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "id_lojista"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "id_uf"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ClienteValores"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.ClienteValores.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "Clientes_ValoresResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Dados_Safra");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "dshashEmpFEI"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DadosBasicosSafra"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.DadosBasicosSafra.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "Dados_SafraResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConsultarTabelas");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "Object"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH"), com.svs.fin.integracao.safra.model.ValidaHASH.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoTabelasRevenda"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoTabelasRevenda.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarTabelasResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConsultarFluxoParcelas");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "Object"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ParamCalculaParcelas"), com.svs.fin.integracao.safra.model.ParamCalculaParcelas.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoCalculoParcelas"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoCalculoParcelas.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarFluxoParcelasResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConsultarOperadores");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "Object"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH"), com.svs.fin.integracao.safra.model.ValidaHASH.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoOperadores"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoOperadores.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarOperadoresResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConsultarDocumentosPendentes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "DsHASH"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://tempuri.org/", "id_proposta"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfRetornoDocumentosPendentes"));
        oper.setReturnClass(com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarDocumentosPendentesResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoDocumentosPendentes"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

    }

    public BasicHttpBinding_ICapturaPropostasStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public BasicHttpBinding_ICapturaPropostasStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public BasicHttpBinding_ICapturaPropostasStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Acionista_Socio");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Acionista_Socio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfAcionista_Socio");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Acionista_Socio[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Acionista_Socio");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Acionista_Socio");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfAvalista");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Avalista[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Avalista");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Avalista");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfBem_Imovel");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Bem_Imovel[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfBem_Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Bem_Veiculo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Veiculo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Veiculo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfEndividamento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Endividamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endividamento");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endividamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfProduto_Vendido");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Produto_Vendido[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Produto_Vendido");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Produto_Vendido");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfReferencia");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Referencia[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfReferencia_Bancaria");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfReferencia_Pessoal");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Pessoal");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Pessoal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "ArrayOfSocio_Participacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Socio_Participacao[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Socio_Participacao");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Socio_Participacao");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Avalista");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Avalista.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Imovel");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Bem_Imovel.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Bem_Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Bem_Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Cnh");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Cnh.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Conjuge_Dados_Basicos");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Conjuge_Dados_Basicos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Contato_Cadastro");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Contato_Cadastro.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Dados_Profissionais");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Dados_Profissionais.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Documento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Empresa_Anterior");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Empresa_Anterior.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endereco");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Endereco.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Endividamento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Endividamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Filiacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Filiacao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Financiado");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Financiado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Local_Nascimento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Local_Nascimento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Outros_Dados.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Complemento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Outros_Dados_Complemento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Diversos");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Outros_Dados_Diversos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Faturamento_Mensal");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Outros_Dados_Faturamento_Mensal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Outros_Dados_Residencia");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Outros_Dados_Residencia.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Ppe");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Ppe.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Produto_Vendido");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Produto_Vendido.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Referencia.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Bancaria");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Referencia_Bancaria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Referencia_Pessoal");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Referencia_Pessoal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Socio_Participacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Socio_Participacao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.Comum", "Telefone");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_comum.Telefone.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Fluxo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_dados_financiamento.Fluxo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Isencoes");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_dados_financiamento.Isencoes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model.DadosFinaciamento", "Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model_dados_financiamento.Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Afinidade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Afinidade.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfAfinidade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Afinidade[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Afinidade");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Afinidade");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfAtividade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Atividade[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Atividade");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Atividade");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfBem_Opcional");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Bem_Opcional[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Bem_Opcional");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Bem_Opcional");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfCargo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Cargo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cargo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cargo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfCatalogo_Erro");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Catalogo_Erro[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Catalogo_Erro");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Catalogo_Erro");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfCoeficiente");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Coeficiente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficiente");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficiente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfDirigentes_Cargo_Sub");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Dirigentes_Cargo_Sub[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Dirigentes_Cargo_Sub");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Dirigentes_Cargo_Sub");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfEmpresa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Empresa[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Empresa");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Empresa");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfEscolaridade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Escolaridade[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Escolaridade");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Escolaridade");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfEstado_Civil");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Estado_Civil[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Estado_Civil");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Estado_Civil");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfFaixa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Faixa[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixa");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixa");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfFaixaAno");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.FaixaAno[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaAno");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaAno");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfFaixaBQ");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.FaixaBQ[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaBQ");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaBQ");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfFaixas_Patrimonio");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Faixas_Patrimonio[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixas_Patrimonio");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixas_Patrimonio");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfImovel_Especie");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Imovel_Especie[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Especie");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Especie");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfImovel_Tipo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Imovel_Tipo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Tipo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Tipo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfIsencao_Fiscal");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Isencao_Fiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Isencao_Fiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Isencao_Fiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfLojista");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Lojista[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Lojista");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Lojista");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfMarcas");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Marcas[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfMoeda");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Moeda[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Moeda");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Moeda");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfNacionalidade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Nacionalidade[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Nacionalidade");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Nacionalidade");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfNatureza_Ocupacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Natureza_Ocupacao[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Natureza_Ocupacao");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Natureza_Ocupacao");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfPessoa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Pessoa[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Pessoa");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Pessoa");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfPlanos");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Planos[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Planos");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Planos");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfPorte_Empresa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Porte_Empresa[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Porte_Empresa");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Porte_Empresa");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfPrazos");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Prazos[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Prazos");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Prazos");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfProduto");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Produto[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Produto");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Produto");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfProfissao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Profissao[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Profissao");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Profissao");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfRamo_Atividade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Ramo_Atividade[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Ramo_Atividade");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Ramo_Atividade");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfRegistro_Gravame");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Registro_Gravame[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Registro_Gravame");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Registro_Gravame");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfRetornoDocumentosPendentes");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoDocumentosPendentes");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoDocumentosPendentes");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfRetornoOperador");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoOperador[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoOperador");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoOperador");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfSede_Social");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Sede_Social[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sede_Social");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sede_Social");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfSegmento_Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Segmento_Veiculo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Segmento_Veiculo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Segmento_Veiculo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfServico");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Servico[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Servico");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Servico");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfSexo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Sexo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sexo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sexo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfStatusProposta");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.StatusProposta[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "StatusProposta");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "StatusProposta");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTabelaJuros");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.TabelaJuros[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelaJuros");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelaJuros");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTarifa_Avaliacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tarifa_Avaliacao[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tarifa_Avaliacao");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tarifa_Avaliacao");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Categoria");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Categoria[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Categoria");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Categoria");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Combustivel");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Combustivel[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Combustivel");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Combustivel");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Compromisso");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Compromisso[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Compromisso");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Compromisso");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Documento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Documento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Documento");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Documento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Emprego");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Emprego[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Emprego");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Emprego");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Endereco");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Endereco[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endereco");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endereco");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Endividamento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Endividamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endividamento");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endividamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Fluxo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Fluxo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Fluxo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Fluxo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Residencia");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Residencia[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Residencia");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Residencia");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Seguro");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Seguro[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Seguro");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Seguro");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Sub_Telefone");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Sub_Telefone[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Sub_Telefone");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Sub_Telefone");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Telefone");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Telefone[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Telefone");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Telefone");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Veiculo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ArrayOfTipo_Veiculo_Porte");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Veiculo_Porte[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo_Porte");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo_Porte");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Atividade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Atividade.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Bem_Opcional");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Bem_Opcional.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cadastro_Comum");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Cadastro_Comum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "CalculoParcelas");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.CalculoParcelas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Cargo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Cargo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Catalogo_Erro");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Catalogo_Erro.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ClienteValores");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.ClienteValores.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Coeficiente");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Coeficiente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "DadosBasicosSafra");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.DadosBasicosSafra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Dirigentes_Cargo_Sub");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Dirigentes_Cargo_Sub.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Empresa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Empresa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Escolaridade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Escolaridade.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Estado_Civil");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Estado_Civil.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Faixa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaAno");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.FaixaAno.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "FaixaBQ");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.FaixaBQ.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Faixas_Patrimonio");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Faixas_Patrimonio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Financiamento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Financiamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Especie");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Imovel_Especie.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Imovel_Tipo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Imovel_Tipo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Isencao_Fiscal");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Isencao_Fiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Lojista");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Lojista.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Marcas");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Marcas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Moeda");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Moeda.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Nacionalidade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Nacionalidade.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Natureza_Ocupacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Natureza_Ocupacao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Padrao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Padrao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ParamCalculaParcelas");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.ParamCalculaParcelas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Pessoa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Pessoa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Planos");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Planos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Porte_Empresa");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Porte_Empresa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Prazos");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Prazos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Produto");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Produto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Profissao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Profissao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_Avalista");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Proposta_Avalista.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_PF");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Proposta_PF.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Proposta_PJ");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Proposta_PJ.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Ramo_Atividade");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Ramo_Atividade.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Registro_Gravame");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Registro_Gravame.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoCalculoParcelas");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoCalculoParcelas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoConsultaProposta");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoConsultaProposta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoDocumentosPendentes");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoOperador");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoOperador.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoOperadores");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoOperadores.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoProposta");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoProposta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "RetornoTabelasRevenda");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.RetornoTabelasRevenda.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sede_Social");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Sede_Social.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Segmento_Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Segmento_Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Servico");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Servico.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Sexo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Sexo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "StatusProposta");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.StatusProposta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "TabelaJuros");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.TabelaJuros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tarifa_Avaliacao");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tarifa_Avaliacao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Categoria");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Categoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Combustivel");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Combustivel.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Compromisso");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Compromisso.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Documento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Emprego");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Emprego.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endereco");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Endereco.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Endividamento");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Endividamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Fluxo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Fluxo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Residencia");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Residencia.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Seguro");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Seguro.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Sub_Telefone");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Sub_Telefone.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Telefone");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Telefone.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "Tipo_Veiculo_Porte");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.Tipo_Veiculo_Porte.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/FEI.Model", "ValidaHASH");
            cachedSerQNames.add(qName);
            cls = com.svs.fin.integracao.safra.model.ValidaHASH.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.svs.fin.integracao.safra.model.RetornoProposta enviar_Proposta_PF(java.lang.String p_ds_hash_simul, com.svs.fin.integracao.safra.model.ValidaHASH validahash, com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum, com.svs.fin.integracao.safra.model.Proposta_PF propostaPf, com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas, com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/Enviar_Proposta_PF");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "Enviar_Proposta_PF"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {p_ds_hash_simul, validahash, propostaComum, propostaPf, avalistas, propostaFinanciamento});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoProposta) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoProposta) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoProposta.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.RetornoProposta enviar_Proposta_PJ(java.lang.String p_ds_hash_simul, com.svs.fin.integracao.safra.model.ValidaHASH validahash, com.svs.fin.integracao.safra.model.Cadastro_Comum propostaComum, com.svs.fin.integracao.safra.model.Proposta_PJ propostaPj, com.svs.fin.integracao.safra.model.Proposta_Avalista avalistas, com.svs.fin.integracao.safra.model.Financiamento propostaFinanciamento) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/Enviar_Proposta_PJ");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "Enviar_Proposta_PJ"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {p_ds_hash_simul, validahash, propostaComum, propostaPj, avalistas, propostaFinanciamento});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoProposta) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoProposta) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoProposta.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.RetornoConsultaProposta proposta_Decisao(com.svs.fin.integracao.safra.model.ValidaHASH validahash, java.lang.String id_proposta) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/Proposta_Decisao");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "Proposta_Decisao"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {validahash, id_proposta});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoConsultaProposta) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoConsultaProposta) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoConsultaProposta.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.ClienteValores clientes_Valores(java.lang.String dsHASH, java.lang.String id_cliente, java.lang.String id_produto, java.lang.String id_lojista, java.lang.String id_uf) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/Clientes_Valores");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "Clientes_Valores"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dsHASH, id_cliente, id_produto, id_lojista, id_uf});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.ClienteValores) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.ClienteValores) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.ClienteValores.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.DadosBasicosSafra dados_Safra(java.lang.String dshashEmpFEI) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/Dados_Safra");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "Dados_Safra"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dshashEmpFEI});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.DadosBasicosSafra) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.DadosBasicosSafra) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.DadosBasicosSafra.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.RetornoTabelasRevenda consultarTabelas(com.svs.fin.integracao.safra.model.ValidaHASH object) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/ConsultarTabelas");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarTabelas"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {object});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoTabelasRevenda) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoTabelasRevenda) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoTabelasRevenda.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.RetornoCalculoParcelas consultarFluxoParcelas(com.svs.fin.integracao.safra.model.ParamCalculaParcelas object) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/ConsultarFluxoParcelas");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarFluxoParcelas"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {object});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoCalculoParcelas) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoCalculoParcelas) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoCalculoParcelas.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.RetornoOperadores consultarOperadores(com.svs.fin.integracao.safra.model.ValidaHASH object) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/ConsultarOperadores");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarOperadores"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {object});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoOperadores) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoOperadores) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoOperadores.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[] consultarDocumentosPendentes(java.lang.String dsHASH, java.lang.String id_proposta) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://tempuri.org/ICapturaPropostas/ConsultarDocumentosPendentes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarDocumentosPendentes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dsHASH, id_proposta});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.svs.fin.integracao.safra.model.RetornoDocumentosPendentes[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
