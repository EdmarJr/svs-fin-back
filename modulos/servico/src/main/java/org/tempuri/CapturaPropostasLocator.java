/**
 * CapturaPropostasLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class CapturaPropostasLocator extends org.apache.axis.client.Service implements org.tempuri.CapturaPropostas {

	public CapturaPropostasLocator() {
	}

	public CapturaPropostasLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public CapturaPropostasLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for BasicHttpBinding_ICapturaPropostas
	// producao abaixo
	 private java.lang.String BasicHttpBinding_ICapturaPropostas_address =
	 "https://fei.safrafinanceira.com.br/CapturaPropostas.svc";
	// homologacao abaixo
//	private java.lang.String BasicHttpBinding_ICapturaPropostas_address = "https://fei-h.safrafinanceira.com.br/CapturaPropostas.svc";

	public java.lang.String getBasicHttpBinding_ICapturaPropostasAddress() {
		return BasicHttpBinding_ICapturaPropostas_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String BasicHttpBinding_ICapturaPropostasWSDDServiceName = "BasicHttpBinding_ICapturaPropostas";

	public java.lang.String getBasicHttpBinding_ICapturaPropostasWSDDServiceName() {
		return BasicHttpBinding_ICapturaPropostasWSDDServiceName;
	}

	public void setBasicHttpBinding_ICapturaPropostasWSDDServiceName(java.lang.String name) {
		BasicHttpBinding_ICapturaPropostasWSDDServiceName = name;
	}

	public org.tempuri.ICapturaPropostas getBasicHttpBinding_ICapturaPropostas() throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(BasicHttpBinding_ICapturaPropostas_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getBasicHttpBinding_ICapturaPropostas(endpoint);
	}

	public org.tempuri.ICapturaPropostas getBasicHttpBinding_ICapturaPropostas(java.net.URL portAddress)
			throws javax.xml.rpc.ServiceException {
		try {
			org.tempuri.BasicHttpBinding_ICapturaPropostasStub _stub = new org.tempuri.BasicHttpBinding_ICapturaPropostasStub(
					portAddress, this);
			_stub.setPortName(getBasicHttpBinding_ICapturaPropostasWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setBasicHttpBinding_ICapturaPropostasEndpointAddress(java.lang.String address) {
		BasicHttpBinding_ICapturaPropostas_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has no
	 * port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (org.tempuri.ICapturaPropostas.class.isAssignableFrom(serviceEndpointInterface)) {
				org.tempuri.BasicHttpBinding_ICapturaPropostasStub _stub = new org.tempuri.BasicHttpBinding_ICapturaPropostasStub(
						new java.net.URL(BasicHttpBinding_ICapturaPropostas_address), this);
				_stub.setPortName(getBasicHttpBinding_ICapturaPropostasWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has no
	 * port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("BasicHttpBinding_ICapturaPropostas".equals(inputPortName)) {
			return getBasicHttpBinding_ICapturaPropostas();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://tempuri.org/", "CapturaPropostas");
	}

	private java.util.HashSet ports = null;

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "BasicHttpBinding_ICapturaPropostas"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {

		if ("BasicHttpBinding_ICapturaPropostas".equals(portName)) {
			setBasicHttpBinding_ICapturaPropostasEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
