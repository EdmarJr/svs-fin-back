package br.com.gruposaga.cad.svs.ws.config.interceptors;
import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import br.com.gruposaga.cad.svs.ws.WsClientTokenProvider;
 
@Provider
public class LoggingInterceptor implements ContainerRequestFilter {
	private @Inject
    WsClientTokenProvider tokenProvider;
	
	@Context
	private HttpHeaders httpHeaders;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		tokenProvider.setToken(this.httpHeaders.getHeaderString("Authorization"));
		
	}
	
//	@Override
//	public ServerResponse preProcess(HttpRequest request, ResourceMethodInvoker method)
//			throws Failure, WebApplicationException {
//		tokenProvider.setToken(this.httpHeaders.getHeaderString("Authorization"));
//		return null;
//	}

}