package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.ProdutoFinanceiro;

import br.com.gruposaga.cad.svs.servico.ProdutoFinanceiroService;

@Path("/produtosFinanceiros")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutosFinanceirosResource {

	@Inject
	private ProdutoFinanceiroService produtoFinanceiroService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<ProdutoFinanceiro> paginaDeObjetos = produtoFinanceiroService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(produtoFinanceiroService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(ProdutoFinanceiro produtoFinanceiro) {
		produtoFinanceiroService.alterar(produtoFinanceiro);
		return Response.ok().build();
	}

	@POST
	public Response incluir(ProdutoFinanceiro produtoFinanceiro) {
		produtoFinanceiroService.incluir(produtoFinanceiro);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		produtoFinanceiroService.deletar(id);
		return Response.ok().build();
	}

}
