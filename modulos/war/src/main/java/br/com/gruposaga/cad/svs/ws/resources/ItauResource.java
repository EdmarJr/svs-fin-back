package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.OperacaoFinanciamentoDto;

import br.com.gruposaga.cad.svs.servico.ItauService;
import br.com.gruposaga.cad.svs.servico.OperacaoFinanciadaService;

@Path("/itau")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ItauResource {

	@Inject
	private ItauService itauService;

	@Inject
	private OperacaoFinanciadaService operacaoFinanciadaService;

	@Context
	ServletContext servletContext;

	@POST
	@Path("/proposta")
	public Response enviarProspotaItau(OperacaoFinanciamentoDto operacaoFinanciamento) {
		itauService.enviarPropostaItau(operacaoFinanciamento.getCpfCnpjCliente(),
				operacaoFinanciamento.getOpcaoFinanciamento(), operacaoFinanciamento.getFinanciamentoItau());

		return Response.ok().build();
	}

	@GET
	@Path("/analise")
	public Response analisarItau(@QueryParam("idOperacaoFinanciada") Long idOperacaoFinanciada) {
		itauService.realizarAnaliseItau(operacaoFinanciadaService.obterPorId(idOperacaoFinanciada));
		return Response.ok().build();
	}

}
