package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.TipoEndereco;

import br.com.gruposaga.cad.svs.servico.TipoEnderecoService;

@Path("/tiposEndereco")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TiposEnderecoResource {

	@Inject
	private TipoEnderecoService tipoEnderecoService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<TipoEndereco> paginaDeObjetos = tipoEnderecoService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(tipoEnderecoService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(TipoEndereco tipoEndereco) {
		tipoEnderecoService.alterar(tipoEndereco);
		return Response.ok().build();
	}

	@POST
	public Response incluir(TipoEndereco tipoEndereco) {
		tipoEnderecoService.incluir(tipoEndereco);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		tipoEnderecoService.deletar(id);
		return Response.ok().build();
	}

}
