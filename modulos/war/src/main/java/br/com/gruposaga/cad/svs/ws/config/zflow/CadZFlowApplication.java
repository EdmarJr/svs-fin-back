package br.com.gruposaga.cad.svs.ws.config.zflow;

import java.util.Optional;

import javax.inject.Inject;

import br.com.gruposaga.jpp.prefs.DoXml;
import br.com.gruposaga.sdk.zflow.ZFlowApplication;
import br.com.gruposaga.sdk.zflow.model.Token;
import br.com.gruposaga.sdk.zflow.model.ZFlowClient;

/**
 * @author Amsterdam Luís
 */
public class CadZFlowApplication implements ZFlowApplication {
	@DoXml
	private @Inject ZFlowPreferences preferences;
	private @Inject ZFlowManager manager;

	@Override
	public int getCurrentUserId() {
		return 1;
	}

	@Override
	public ZFlowClient getClient(int userId) {
		return manager.getClient(userId);
	}

	@Override
	public Optional<Token> getToken(int userId) {
		return manager.getToken(userId);
	}

	@Override
	public void setToken(int userId, Token token) {
		manager.setToken(userId, token);
	}

	@Override
	public String getApplicationUrl() {
		return preferences.getApplicationUrl();
	}

	@Override
	public String getAuthorizationUrl() {
		return preferences.getAuthorizationUrl();
	}
}
