package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.TabelaFinanceira;

import br.com.gruposaga.cad.svs.servico.TabelaFinanceiraService;

@Path("/tabelasFinanceiras")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TabelasFinanceirasResource {

	@Inject
	private TabelaFinanceiraService tabelaFinanceiraService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("ativo") Boolean ativo,
			@QueryParam("idUnidadeOrganizacional") Long idUnidadeOrganizacional, @QueryParam("tipo") String tipo,
			@QueryParam("qtdParcelas") Integer qtdParcelas) {

		Object jsonObjectRetorno = null;
		if (tipo != null && !tipo.equals("") && idUnidadeOrganizacional != null && ativo != null
				&& qtdParcelas != null) {
			jsonObjectRetorno = obterPorEstadoUnidadeOrganizacionalETipoParcelas(ativo, idUnidadeOrganizacional, tipo,
					qtdParcelas);
		} else if (tipo != null && !tipo.equals("") && idUnidadeOrganizacional != null && ativo != null) {
			jsonObjectRetorno = obterPorEstadoUnidadeOrganizacionalETipo(ativo, idUnidadeOrganizacional, tipo);
		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			jsonObjectRetorno = tabelaFinanceiraService.obterTodos(parametros, getListaDeFetchsEager());
		}

		return Response.ok(jsonObjectRetorno).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(tabelaFinanceiraService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	public List<TabelaFinanceira> obterPorEstadoUnidadeOrganizacionalETipo(Boolean ativo, Long idUnidadeOrganizacional,
			String tipo) {
		return tabelaFinanceiraService.obterPorEstadoUnidadeOrganizacionalETipo(ativo, idUnidadeOrganizacional, tipo);
	}

	public List<TabelaFinanceira> obterPorEstadoUnidadeOrganizacionalETipoParcelas(Boolean ativo,
			Long idUnidadeOrganizacional, String tipo, Integer qtdParcelas) {
		return tabelaFinanceiraService.obterPorEstadoUnidadeOrganizacionalTipoEQntdParcelas(ativo,
				idUnidadeOrganizacional, tipo, qtdParcelas);
	}

	@PUT
	public Response alterar(TabelaFinanceira tabelaFinanceira) {
		tabelaFinanceiraService.alterar(tabelaFinanceira);
		return Response.ok().build();
	}

	@POST
	public Response incluir(TabelaFinanceira tabelaFinanceira) {
		tabelaFinanceiraService.incluir(tabelaFinanceira);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		tabelaFinanceiraService.deletar(id);
		return Response.ok().build();
	}

	private String[] getListaDeFetchsEager() {
		return new String[] { "unidades" };
	}

}
