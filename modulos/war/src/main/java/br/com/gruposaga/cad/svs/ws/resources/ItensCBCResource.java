package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.ItemCBC;

import br.com.gruposaga.cad.svs.servico.ItemCBCService;

@Path("/itensCBC")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ItensCBCResource {

	@Inject
	private ItemCBCService itemCBCService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idOperacaoFinanciada") Long idOperacaoFinanciada) {
		
		if (idOperacaoFinanciada != null) {
			List<ItemCBC> listaPorIdOperacaoFinanciada = obterPorOperacaoFinanciada(idOperacaoFinanciada);
			return Response.ok(listaPorIdOperacaoFinanciada).build();
		}
		
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<ItemCBC> paginaDeObjetos = itemCBCService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	public List<ItemCBC> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<ItemCBC> retorno = itemCBCService.obterPorOperacaoFinanciada(idOperacaoFinanciada);
		return retorno;
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(itemCBCService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(ItemCBC itemCBC) {
		itemCBCService.alterar(itemCBC);
		return Response.ok().build();
	}

	@POST
	public Response incluir(ItemCBC itemCBC) {
		itemCBCService.incluir(itemCBC);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		itemCBCService.deletar(id);
		return Response.ok().build();
	}

}
