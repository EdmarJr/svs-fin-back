package br.com.gruposaga.cad.svs.ws.config.zflow;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.gruposaga.cad.wsc.WsClient;
import br.com.gruposaga.cad.zflow.modelo.pojo.NovoToken;
import br.com.gruposaga.cad.zflow.ws.api.WsTokens;
import br.com.gruposaga.cad.zflow.ws.api.WsUsuarios;
import br.com.gruposaga.sdk.zflow.model.Token;
import br.com.gruposaga.sdk.zflow.model.ZFlowClient;

/**
 * @author Amsterdam Luís
 */
@Singleton
public class ZFlowManager {
	@Inject
	private WsClient wsClient;
	private WsTokens wsTokens;
	private WsUsuarios wsUsuarios;

	private final Map<Integer, Token> tokens = new ConcurrentHashMap<>();

	@PostConstruct
	private void init() {
		wsTokens = wsClient.proxy(WsTokens.class);
		wsUsuarios = wsClient.proxy(WsUsuarios.class);
	}

	public void setToken(int userId, Token token) {
		tokens.put(userId, token);
		NovoToken novoToken = new NovoToken();
		novoToken.setAccessToken(token.getAccessToken());
		novoToken.setCreationDate(token.getCreationDate());
		novoToken.setExpiresIn(token.getExpiresIn());
		novoToken.setIdToken(token.getIdToken());
		novoToken.setNotBeforePolicy(token.getNotBeforePolicy());
		novoToken.setRefreshToken(token.getRefreshToken());
		novoToken.setRefreshExpiresIn(token.getRefreshExpiresIn());
		novoToken.setSessionState(token.getSessionState());
		novoToken.setTokenType(token.getTokenType());
		wsTokens.persist(userId, novoToken);
	}

	public ZFlowClient getClient(int userId) {
		return wsUsuarios.find(userId);
	}

	public Optional<Token> getToken(int userId) {
		Token token = tokens.get(userId);
		return Optional.ofNullable(token == null ? wsTokens.find(userId) : token);
	}
}
