package br.com.gruposaga.cad.svs.ws.resources;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/santander")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SantanderResource {

	@GET
	@Path(value = "/pre-analise")
	public void fazerPreAnalise() throws UnsupportedEncodingException {

		// this.simulationResponse = null;
		// this.simulationRequest = null;
		// this.inputStream = null;
	}

}
