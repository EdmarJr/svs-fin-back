package br.com.gruposaga.cad.svs.ws.resources;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.OperacaoFinanciada;

import br.com.gruposaga.cad.svs.servico.ClienteSvsService;
import br.com.gruposaga.cad.svs.servico.OperacaoFinanciadaService;
import br.com.gruposaga.cad.svs.utils.UtilsFile;

@Path("/operacoesFinanciadas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OperacoesFinanciadasResource {

	@Inject
	private OperacaoFinanciadaService operacaoFinanciadaService;
	@Inject
	private ClienteSvsService clienteSvsService;
	@Context
	ServletContext servletContext;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<OperacaoFinanciada> paginaDeObjetos = operacaoFinanciadaService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		Response response = Response.ok(operacaoFinanciadaService.obterPorId(Long.parseLong(id.toString()))).build();
		return response;
	}

	@GET
	@Path("/proposta/{codigoProposta}")
	public Response obterPorVinculoComCodigoProposta(@PathParam("codigoProposta") Long codigoProposta) {
		return Response.ok(operacaoFinanciadaService.obterPorVinculoComCodigoProposta(codigoProposta)).build();
	}

	@PUT
	public Response alterar(OperacaoFinanciada operacaoFinanciada) {
		operacaoFinanciadaService.alterar(operacaoFinanciada);
		return Response.ok().build();
	}

	@POST
	public Response incluir(OperacaoFinanciada operacaoFinanciada) {
		operacaoFinanciadaService.incluir(operacaoFinanciada);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		operacaoFinanciadaService.deletar(id);
		return Response.ok().build();
	}

	@GET
	@Path("/contrato/download")
	@Produces("application/pdf")
	public Response gerarContrato(@QueryParam("idOperacaoFinanciada") Long id) {
		String filesPath = servletContext.getRealPath("/") + "/WEB-INF";
		System.out.println("ENDEREÇO CONTRATO:::::::" + filesPath);
		String nomeArquivo = "contrato.pdf";
		byte[] byteArray = operacaoFinanciadaService.gerarContrato(id, filesPath);
		File file = UtilsFile.novaInstanciaDeFileComEnderecoSemUtilizacao(nomeArquivo);
		// ResponseBuilder responseBuilder = Response.ok(byteArray);
		try {
			FileUtils.writeByteArrayToFile(file, byteArray);
			ResponseBuilder response = Response.ok((Object) file);
			response.header("Content-Disposition", "attachment; filename=\"" + nomeArquivo + "\"");
			return response.build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(500).build();
		}
		// responseBuilder.type("application/pdf");
		// responseBuilder.header("Content-Disposition", "filename=contrato.pdf");
		// return responseBuilder.build();
	}

}
