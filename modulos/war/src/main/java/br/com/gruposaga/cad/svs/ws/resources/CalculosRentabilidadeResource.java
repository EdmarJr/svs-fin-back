package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.enums.EnumStatusAprovacaoBancaria;
import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.CalculoRentabilidade;

import br.com.gruposaga.cad.svs.servico.CalculoRentabilidadeService;

@Path("/calculosRentabilidade")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CalculosRentabilidadeResource {

	@Inject
	private CalculoRentabilidadeService calculoRentabilidadeService;
	
	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idOperacaoFinanciada") Long idOperacaoFinanciada) {
		
		if (idOperacaoFinanciada != null) {
			List<CalculoRentabilidade> listaPorIdOperacaoFinanciada = obterPorOperacaoFinanciada(idOperacaoFinanciada);
			return Response.ok(listaPorIdOperacaoFinanciada).build();
		}
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<CalculoRentabilidade> paginaDeObjetos = calculoRentabilidadeService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	public List<CalculoRentabilidade> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<CalculoRentabilidade> retorno = calculoRentabilidadeService
				.obterPorOperacaoFinanciada(idOperacaoFinanciada);
		return retorno;
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Long id) {
		return Response.ok(calculoRentabilidadeService.obterPorId(id)).build();
	}

	@PUT
	public Response alterar(CalculoRentabilidade calculoRentabilidade) {
		calculoRentabilidadeService.alterar(calculoRentabilidade);
		return Response.ok().build();
	}

	@POST
	public Response incluir(CalculoRentabilidade calculoRentabilidade) {
		calculoRentabilidadeService.incluir(calculoRentabilidade);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		calculoRentabilidadeService.deletar(id);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/aprovar/{id}")
	public Response aprovar(@PathParam("id") Long id) {
		calculoRentabilidadeService.alterarStatusCalculoRentabilidade(id, EnumStatusAprovacaoBancaria.APROVADO);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/reprovar/{id}")
	public Response reprovar(@PathParam("id") Long id) {
		calculoRentabilidadeService.alterarStatusCalculoRentabilidade(id, EnumStatusAprovacaoBancaria.REPROVADO);
		return Response.ok().build();
	}	

}
