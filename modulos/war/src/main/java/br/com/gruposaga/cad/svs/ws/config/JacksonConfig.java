package br.com.gruposaga.cad.svs.ws.config;

import java.time.LocalDate;

import javax.ws.rs.ext.ContextResolver;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.svs.fin.modelo.mapper.jackson.LocalDateFromJsonMilis;
import com.svs.fin.modelo.mapper.jackson.LocalDateToJsonMilis;

//@Provider
public class JacksonConfig implements ContextResolver<ObjectMapper> {

	private ObjectMapper objectMapper;

	public JacksonConfig() {

		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		// objectMapper.disable(MapperFeature.USE_GETTERS_AS_SETTERS);
		@SuppressWarnings("deprecation")
		SimpleModule module = new SimpleModule("MyModule", new Version(1, 0, 0, null));
		module.addSerializer(LocalDate.class,new LocalDateToJsonMilis());
		module.addDeserializer(LocalDate.class, new LocalDateFromJsonMilis());
		// module.addSerializer(LocalDate.class, new LocalDateSerializer());
		// module.addDeserializer(LocalDate.class, new LocalDateDeserializer());
		// module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
		// module.addDeserializer(LocalDateTime.class, new
		// LocalDateTimeDeserializer());
		// module.addSerializer(LocalTime.class, new LocalTimeSerializer());
		// module.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
		// module.addSerializer(StatusCampanhaEnum.class, new
		// StatusCampanhaEnumSerializer());
		// module.addDeserializer(StatusCampanhaEnum.class, new
		// StatusCampanhaEnumDeserializer());
		// module.addSerializer(DiasDaSemanaEnum.class, new
		// DiasDaSemanaEnumSerializer());
		// module.addDeserializer(DiasDaSemanaEnum.class, new
		// DiasDaSemanaEnumDeserializer());
		// module.addDeserializer(SexoEnum.class, new SexoEnumDeserializer());

		Class<?>[] definitions = BidirectionalDefinition.class.getDeclaredClasses();
		for (Class<?> definition : definitions) {
			objectMapper.addMixIn(definition.getAnnotation(JsonIdentityInfo.class).scope(), definition);
		}

		objectMapper.registerModule(module);

	}

	public ObjectMapper getContext(Class<?> objectType) {
		return objectMapper;
	}

}