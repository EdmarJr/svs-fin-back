package br.com.gruposaga.cad.svs.ws.impl;


import br.com.gruposaga.cad.svs.fin.modelo.pojo.EntidadeTesteConsultaPojo;
import br.com.gruposaga.cad.svs.servico.ServicoEntidadeTeste;
import br.com.gruposaga.cad.svs.ws.WsAbstratoSvsFin;
import br.com.gruposaga.cad.svs.fin.ws.api.WebServicesSvsFin;
import br.com.gruposaga.cad.svs.fin.ws.api.WsEntidadeTeste;
import br.com.gruposaga.cad.ws.Erro400;
import com.svs.fin.model.dto.EntidadeTesteDto;
import com.svs.fin.model.dto.EntidadeTesteEdicaoDto;
import com.svs.fin.model.dto.ListParamsDto;

import javax.inject.Inject;
import javax.ws.rs.Path;
import java.util.List;

@Path(WebServicesSvsFin.ENTIDADE_TESTE)
public class ImplWsEntidadeTeste extends WsAbstratoSvsFin implements WsEntidadeTeste {
	
	private @Inject
    ServicoEntidadeTeste servico;

	@Override
	public void put(Long idEntidadeTeste, EntidadeTesteEdicaoDto entidadeTesteDto) throws Erro400 {
		try {
			servico.atualizar(idEntidadeTeste, entidadeTesteDto, getCpfUsuario());
		}catch (Exception e) {
            logger.error("Erro", e);
			if(e.getMessage() != null) {
				throw new Erro400(e.getMessage());
			}else {
				throw new Erro400("Erro ao realizar a inclusão, verifique os dados informados.");
			}
		}
	}

	@Override
	public Long post(EntidadeTesteEdicaoDto entidadeTesteDto) throws Erro400{
		
		try {
			return servico.persistir(entidadeTesteDto, getCpfUsuario()).getId();
			
		} catch (Exception e) {
            logger.error("Erro", e);
			if(e.getMessage() != null) {
				throw new Erro400(e.getMessage());
			}else {
				throw new Erro400("Erro ao realizar a inclusão, verifique os dados informados.");
			}
		}
	}

	@Override
	public EntidadeTesteDto get(Long idEntidadeTeste) {
		return servico.consultar(idEntidadeTeste);
	}

	@Override
	public List<EntidadeTesteConsultaPojo> listarFiltro(ListParamsDto pageFilterDto) throws Erro400{
		try {
			return servico.listarFiltro(pageFilterDto);
		}catch (Exception e) {
            logger.error("Erro", e);
			throw new Erro400("Erro ao realizar consulta, verifique os parâmetros informados.");
		}
	}
	
	@Override
	public long contarRegistros(ListParamsDto listCountParamsDto) throws Erro400{
		try {
			return servico.contarRegistros(listCountParamsDto);
		}catch (Exception e) {
            logger.error("Erro", e);
			throw new Erro400("Erro ao realizar consulta, verifique os parâmetros informados.");
		}
	}
}
