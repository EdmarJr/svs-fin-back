package br.com.gruposaga.cad.svs.ws.config.zflow;

/**
 * @author Amsterdam Luís
 */
public class ZFlowPreferences {
	private String applicationUrl;
	private String authorizationUrl;

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public String getAuthorizationUrl() {
		return authorizationUrl;
	}

	public void setAuthorizationUrl(String authorizationUrl) {
		this.authorizationUrl = authorizationUrl;
	}
}
