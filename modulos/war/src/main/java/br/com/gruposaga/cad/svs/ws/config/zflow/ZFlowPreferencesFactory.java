package br.com.gruposaga.cad.svs.ws.config.zflow;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import br.com.gruposaga.jpp.prefs.DoXml;
import br.com.gruposaga.jpp.prefs.FabricaPreferencias;

/**
 * @author Amsterdam Luís
 */
@Singleton
public class ZFlowPreferencesFactory extends FabricaPreferencias<ZFlowPreferences> {

	private ZFlowPreferences zFlowPreferences;

	@PostConstruct
	private void init() {
		zFlowPreferences = lerXml();
	}

	@DoXml
	@Produces
	private ZFlowPreferences produce() {
		return zFlowPreferences;
	}

	@Override
	protected ZFlowPreferences criarPreferenciaPadrao(String nomeArquivo) {
		ZFlowPreferences response = new ZFlowPreferences();
		response.setApplicationUrl("https://financeapi.zflow.com.br");
		response.setAuthorizationUrl("https://accounts.zflow.com.br");
		return response;
	}
}
