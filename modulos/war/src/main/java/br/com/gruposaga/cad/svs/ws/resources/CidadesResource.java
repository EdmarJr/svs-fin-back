package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.Cidade;

import br.com.gruposaga.cad.svs.servico.CidadeService;

@Path("/cidades")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CidadesResource {

	@Inject
	private CidadeService cidadeService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<Cidade> paginaDeObjetos = cidadeService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(cidadeService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(Cidade cidade) {
		cidadeService.alterar(cidade);
		return Response.ok().build();
	}

	@POST
	public Response incluir(Cidade cidade) {
		cidadeService.incluir(cidade);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		cidadeService.deletar(id);
		return Response.ok().build();
	}

}
