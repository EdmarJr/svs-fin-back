package br.com.gruposaga.cad.svs.ws.config;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.svs.fin.model.entities.CnpjInstituicaoFinanceira;
import com.svs.fin.model.entities.InstituicaoFinanceira;

public interface BidirectionalDefinition {

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = InstituicaoFinanceira.class)
	public interface InstituicaoFinanceiraDef {
	};

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CnpjInstituicaoFinanceira.class)
	public interface CnpjInstituicaoFinanceiraDef {
	};

	// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
	// property = "id", scope = ProdutoFinanceiro.class)
	// public interface ProdutoFinanceiroDef {
	// };

	// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
	// property = "id", scope = ProdutoFinanceiro.class)
	// public interface ProdutoFinanceiroDef {
	// };

}