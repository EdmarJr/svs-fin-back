package br.com.gruposaga.cad.svs.ws.resources;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.ClienteSvs;

import br.com.gruposaga.cad.svs.fin.ws.api.WsClienteSvs;
import br.com.gruposaga.cad.svs.servico.ClienteSvsService;
import br.com.gruposaga.cad.svs.utils.UtilsFile;

@Path("/clientes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClientesResource implements WsClienteSvs {

	@Inject
	private ClienteSvsService clienteSvsService;

	@Context
	ServletContext servletContext;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("cpfCnpj") String cpfCnpj,
			@QueryParam("flagLoadLists") Boolean flagLoadLists) {
		Object retorno = null;
		if (cpfCnpj != null) {
			retorno = obterPorCpfCnpjComFlagDeListasCarregadas(cpfCnpj, flagLoadLists);
			if (retorno == null) {
				return Response.status(404).build();
			}

		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			retorno = clienteSvsService.obterTodos(parametros);

		}

		return Response.ok(retorno).build();
	}

	@GET
	@Path("/ficha/download")
	@Produces("application/pdf")
	public Response gerarFichaCliente(@QueryParam("idOperacaoFinanciada") Long id) {
		String filesPath = servletContext.getRealPath("/") + "/WEB-INF";
		String nomeArquivo = "ficha.pdf";
		byte[] byteArray = clienteSvsService.gerarFichaCliente(id, filesPath);
		File file = UtilsFile.novaInstanciaDeFileComEnderecoSemUtilizacao(nomeArquivo);

		try {
			FileUtils.writeByteArrayToFile(file, byteArray);
			ResponseBuilder response = Response.ok((Object) file);
			response.header("Content-Disposition", "attachment; filename=\"" + nomeArquivo + "\"");
			return response.build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(500).build();
		}

	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(clienteSvsService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@GET
	@Path("/cpfCnpj/{cpfCnpj}")
	public Response obterPorCpfCnpj(@PathParam("cpfCnpj") String cpfCnpj) {
		ClienteSvs cliente = clienteSvsService.obterPorCpfCnpj(cpfCnpj);
		if (cliente == null) {
			return Response.status(404).build();
		}
		return Response.ok(cliente).build();
	}

	public ClienteSvs obterPorCpfCnpjComFlagDeListasCarregadas(String cpfCnpj, Boolean flagLoadLists) {
		return clienteSvsService.obterPorCpfCnpj(cpfCnpj, flagLoadLists);
	}

	@PUT
	public Response alterar(ClienteSvs clienteSvs) {
		clienteSvsService.alterar(clienteSvs);
		return Response.ok().build();
	}

	@POST
	public Response incluir(ClienteSvs clienteSvs) {
		clienteSvsService.incluir(clienteSvs);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		clienteSvsService.deletar(id);
		return Response.ok().build();
	}

}
