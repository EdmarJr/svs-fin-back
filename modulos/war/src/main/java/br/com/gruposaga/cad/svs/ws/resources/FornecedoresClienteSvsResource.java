package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.FornecedorClienteSvs;

import br.com.gruposaga.cad.svs.servico.FornecedorClienteSvsService;

@Path("/fornecedoresClienteSvs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FornecedoresClienteSvsResource {

	@Inject
	private FornecedorClienteSvsService fornecedorClienteSvsService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<FornecedorClienteSvs> paginaDeObjetos = fornecedorClienteSvsService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(fornecedorClienteSvsService.obterPorId(Long.parseLong(id.toString()))).build();
	}
	@GET
	@Path("/clienteSvs/{id}")
	public Response obterPorIdClienteSvs(@PathParam("id") Integer id) {
		return Response.ok(fornecedorClienteSvsService.obterPorIdClienteSvs(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(FornecedorClienteSvs fornecedorClienteSvs) {
		fornecedorClienteSvsService.alterar(fornecedorClienteSvs);
		return Response.ok().build();
	}

	@POST
	public Response incluir(FornecedorClienteSvs fornecedorClienteSvs) {
		fornecedorClienteSvsService.incluir(fornecedorClienteSvs);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		fornecedorClienteSvsService.deletar(id);
		return Response.ok().build();
	}

}
