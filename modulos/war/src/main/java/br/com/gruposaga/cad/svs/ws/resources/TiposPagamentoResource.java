package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.TipoPagamento;

import br.com.gruposaga.cad.svs.servico.TipoPagamentoService;

@Path("/tiposPagamento")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TiposPagamentoResource {

	@Inject
	private TipoPagamentoService tipoPagamentoService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("tipo") String tipoRecebimento) {
		
		Object objetoRetorno = null;
		
		if(tipoRecebimento != null && !tipoRecebimento.equals("")) {
			objetoRetorno = obterPorTipoRecebimento(tipoRecebimento);
		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			objetoRetorno = tipoPagamentoService.obterTodos(parametros);
		}
		
		return Response.ok(objetoRetorno).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(tipoPagamentoService.obterPorId(Long.parseLong(id.toString()))).build();
	}
	
	public List<TipoPagamento> obterPorTipoRecebimento(String tipoRecebimento) {
		List<TipoPagamento> retorno = tipoPagamentoService.obterPorTipoRecebimento(tipoRecebimento);
		return retorno;
	}

	@PUT
	public Response alterar(TipoPagamento tipoPagamento) {
		tipoPagamentoService.alterar(tipoPagamento);
		return Response.ok().build();
	}

	@POST
	public Response incluir(TipoPagamento tipoPagamento) {
		tipoPagamentoService.incluir(tipoPagamento);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		tipoPagamentoService.deletar(id);
		return Response.ok().build();
	}

}
