package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.integracaosantander.ModeloVeiculoSantander;

import br.com.gruposaga.cad.svs.servico.ModeloVeiculoSantanderService;

@Path("/modelosVeiculoSantander")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ModelosVeiculoSantanderResource {

	@Inject
	private ModeloVeiculoSantanderService modeloVeiculoSantanderService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idMarca") Long idMarca) {
		Object objectToJson = null;
		if (idMarca != null) {
			objectToJson = obterPorIdMarca(idMarca);
		} else {

			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			objectToJson = modeloVeiculoSantanderService.obterTodos(parametros);
		}
		return Response.ok(objectToJson).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(modeloVeiculoSantanderService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	public List<ModeloVeiculoSantander> obterPorIdMarca(Long idMarca) {
		List<ModeloVeiculoSantander> retorno = modeloVeiculoSantanderService.obterPorIdMarca(idMarca);
		return retorno;
	}

	@PUT
	public Response alterar(ModeloVeiculoSantander modeloVeiculoSantander) {
		modeloVeiculoSantanderService.alterar(modeloVeiculoSantander);
		return Response.ok().build();
	}

	@POST
	public Response incluir(ModeloVeiculoSantander modeloVeiculoSantander) {
		modeloVeiculoSantanderService.incluir(modeloVeiculoSantander);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		modeloVeiculoSantanderService.deletar(id);
		return Response.ok().build();
	}

}
