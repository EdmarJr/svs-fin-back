package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.MarcaSantander;

import br.com.gruposaga.cad.svs.servico.MarcaSantanderService;

@Path("/marcasSantander")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MarcasSantanderResource {

	@Inject
	private MarcaSantanderService marcaSantanderService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("tipoVeiculo") String tipoVeiculo) {
		
		Object jsonRetorno = null;
		if(tipoVeiculo != null && !tipoVeiculo.trim().equals("")) {
			jsonRetorno = obterPorTipoVeiculo(tipoVeiculo);
		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			PaginaDeObjetos<MarcaSantander> paginaDeObjetos = marcaSantanderService.obterTodos(parametros);
			jsonRetorno = paginaDeObjetos;
		}
		
		return Response.ok(jsonRetorno).build();
	}
	
	private List<MarcaSantander> obterPorTipoVeiculo(String tipoVeiculo) {
		List<MarcaSantander> retorno = marcaSantanderService.obterPorTipoVeiculo(tipoVeiculo);
		return retorno;
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(marcaSantanderService.obterPorId(Long.parseLong(id.toString()))).build();
	}
	
	@PUT
	public Response alterar(MarcaSantander marcaSantander) {
		marcaSantanderService.alterar(marcaSantander);
		return Response.ok().build();
	}

	@POST
	public Response incluir(MarcaSantander marcaSantander) {
		marcaSantanderService.incluir(marcaSantander);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		marcaSantanderService.deletar(id);
		return Response.ok().build();
	}

}
