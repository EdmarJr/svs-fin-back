package br.com.gruposaga.cad.svs.ws.resources.safra;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.integracao.safra.model.Segmento_Veiculo;
import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

import br.com.gruposaga.cad.svs.servico.integracao.ws.safra.services.Segmento_VeiculoSafraService;

@Path("/segmento_VeiculosSafra")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Segmento_VeiculosSafraResource {

	@Inject
	private Segmento_VeiculoSafraService segmento_VeiculoSafraService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<Segmento_Veiculo> paginaDeObjetos = segmento_VeiculoSafraService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(segmento_VeiculoSafraService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(Segmento_Veiculo segmento_Veiculo) {
		segmento_VeiculoSafraService.alterar(segmento_Veiculo);
		return Response.ok().build();
	}

	@POST
	public Response incluir(Segmento_Veiculo segmento_Veiculo) {
		segmento_VeiculoSafraService.incluir(segmento_Veiculo);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		segmento_VeiculoSafraService.deletar(id);
		return Response.ok().build();
	}

}
