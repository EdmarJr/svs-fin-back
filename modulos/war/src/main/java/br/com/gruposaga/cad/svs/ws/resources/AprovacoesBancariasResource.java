package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.AprovacaoBancaria;

import br.com.gruposaga.cad.svs.servico.AprovacaoBancariaService;

@Path("/aprovacoes-bancarias")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AprovacoesBancariasResource {
	
	@Inject
	private AprovacaoBancariaService aprovacaoBancariaService;
	
	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idCalculoRentabilidade") Long idCalculoRentabilidade) {
		if (idCalculoRentabilidade != null) {
			List<AprovacaoBancaria> listaPorIdCalculoRentabilidade = obterPorCalculoRentabilidade(idCalculoRentabilidade);
			return Response.ok(listaPorIdCalculoRentabilidade).build();
		}		
		
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<AprovacaoBancaria> paginaDeObjetos = aprovacaoBancariaService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(aprovacaoBancariaService.obterPorId(Long.parseLong(id.toString()))).build();
	}
	
	@PUT
	public Response alterar(AprovacaoBancaria aprovacaoBancaria) {
		aprovacaoBancariaService.alterar(aprovacaoBancaria);
		return Response.ok().build();
	}

	@POST
	public Response incluir(AprovacaoBancaria aprovacaoBancaria) {
		aprovacaoBancariaService.incluir(aprovacaoBancaria);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		aprovacaoBancariaService.deletar(id);
		return Response.ok().build();
	}	
	
	public List<AprovacaoBancaria> obterPorCalculoRentabilidade(Long idCalculoRentabilidade) {
		List<AprovacaoBancaria> retorno = aprovacaoBancariaService.obterPorCalculoRentabilidade(idCalculoRentabilidade);
		return retorno;
	}
}
