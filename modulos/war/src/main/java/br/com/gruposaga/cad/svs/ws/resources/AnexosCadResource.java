package br.com.gruposaga.cad.svs.ws.resources;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.AnexoCad;

import br.com.gruposaga.cad.svs.servico.AnexosCadService;
import br.com.gruposaga.cad.svs.utils.UtilsFile;

@Path("/anexosCad")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AnexosCadResource {
	@Inject
	private AnexosCadService anexosCadService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idOperacaoFinanciada") Long idOperacaoFinanciada,
			@QueryParam("idCalculoRentabilidade") Long idCalculoRentabilidade) {
		if (idOperacaoFinanciada != null) {
			List<AnexoCad> listaPorIdOperacaoFinanciada = obterPorOperacaoFinanciada(idOperacaoFinanciada);
			return Response.ok(listaPorIdOperacaoFinanciada).build();
		}
		
		if (idCalculoRentabilidade != null) {
			List<AnexoCad> listaPorIdCalculoRentabilidade= obterPorCalculoRentabilidade(idCalculoRentabilidade);
			return Response.ok(listaPorIdCalculoRentabilidade).build();
		}
		
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<AnexoCad> paginaDeObjetos = anexosCadService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(anexosCadService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(AnexoCad anexoCad) {
		anexosCadService.alterar(anexoCad);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		anexosCadService.deletar(id);
		return Response.ok().build();
	}

	@GET
	@Path("/{id}/pdf")
	@Produces("application/pdf")
	public Response getFile(@PathParam("id") Long id) {
		AnexoCad anexoCad = anexosCadService.obterPorId(id);
		byte[] anexoCadArrayBytes = anexosCadService.obterArrayDeBytesDoAnexoPorId(id);
		String nomeArquivo = anexoCad.getNomeArquivo() + ".pdf";
		File file = UtilsFile.novaInstanciaDeFileComEnderecoSemUtilizacao(nomeArquivo);
		try {
			FileUtils.writeByteArrayToFile(file, anexoCadArrayBytes);
			ResponseBuilder response = Response.ok((Object) file);
			response.header("Content-Disposition", "attachment; filename=\"" + nomeArquivo + "\"");
			return response.build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return Response.status(500).build();
		}

	}

	public List<AnexoCad> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<AnexoCad> retorno = anexosCadService.obterPorOperacaoFinanciada(idOperacaoFinanciada);
		return retorno;
	}
	
	public List<AnexoCad> obterPorCalculoRentabilidade(Long idCalculoRentabilidade) {
		List<AnexoCad> retorno = anexosCadService.obterPorCalculoRentabilidade(idCalculoRentabilidade);
		return retorno;
	}

	@POST
	@Consumes("multipart/form-data")
	public Response incluir(MultipartFormDataInput input) {
		String fileName = "";

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("Anexos");
		List<AnexoCad> anexosIdInclusos = new ArrayList<AnexoCad>();
		for (InputPart inputPart : inputParts) {
			try {
				AnexoCad anexoCad = new AnexoCad();
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);
				anexoCad.setNomeArquivo(fileName.substring(0, fileName.lastIndexOf(".")));
				InputStream inputStream = inputPart.getBody(InputStream.class, null);
				byte[] bytes = IOUtils.toByteArray(inputStream);
				anexoCad.setAnexo(bytes);
				anexoCad.setExtensao(fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()));
				anexosCadService.incluir(anexoCad);
				anexosIdInclusos.add(anexoCad);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(200).entity(anexosIdInclusos).build();
	}

	/**
	 * header sample { Content-Type=[image/png], Content-Disposition=[form-data;
	 * name="file"; filename="filename.extension"] }
	 **/
	// get uploaded filename, is there a easy way in RESTEasy?
	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

}
