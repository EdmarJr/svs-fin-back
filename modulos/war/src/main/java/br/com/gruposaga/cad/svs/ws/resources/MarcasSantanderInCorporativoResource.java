package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.entities.MarcaSantander;

import br.com.gruposaga.cad.svs.servico.MarcaSantanderService;

@Path("/marcasSantanderInCorporativo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MarcasSantanderInCorporativoResource {

	@Inject
	private MarcaSantanderService marcaSantanderService;

//	@GET
//	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
//			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
//			@QueryParam("limit") Integer limit) {
//		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
//				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
//		PaginaDeObjetos<MarcaSantander> paginaDeObjetos = marcaSantanderService.obterTodos(parametros);
//		return Response.ok(paginaDeObjetos).build();
//	}

	@GET
	public Response obterPorId(@QueryParam("description") String description) {
		MarcaSantander marcaSantander = marcaSantanderService.buscarMarcaSantander(description);
		return Response.ok(marcaSantander).build();
	}

//	@PUT
//	public Response alterar(MarcaSantander marcaSantander) {
//		marcaSantanderService.alterar(marcaSantander);
//		return Response.ok().build();
//	}
//
//	@POST
//	public Response incluir(MarcaSantander marcaSantander) {
//		marcaSantanderService.incluir(marcaSantander);
//		return Response.ok().build();
//	}
//
//	@DELETE
//	@Path("/{id}")
//	public Response remover(@PathParam("id") Long id) {
//		marcaSantanderService.deletar(id);
//		return Response.ok().build();
//	}

}
