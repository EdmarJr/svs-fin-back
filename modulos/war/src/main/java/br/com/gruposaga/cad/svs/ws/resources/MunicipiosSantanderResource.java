package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.MunicipioSantander;

import br.com.gruposaga.cad.svs.servico.MunicipioSantanderService;

@Path("/municipiosSantander")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MunicipiosSantanderResource {

	@Inject
	private MunicipioSantanderService municipioSantanderService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("siglaEstado") String siglaEstado) {

		Object responseBody = null;
		if (siglaEstado != null) {
			responseBody = obterPorSiglaEstado(siglaEstado);
		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			responseBody = municipioSantanderService.obterTodos(parametros);
		}
		return Response.ok(responseBody).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(municipioSantanderService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	public List<MunicipioSantander> obterPorSiglaEstado(String siglaEstado) {
		List<MunicipioSantander> retorno = municipioSantanderService.obterPorSiglaEstado(siglaEstado);
		return retorno;
	}

	@PUT
	public Response alterar(MunicipioSantander municipioSantander) {
		municipioSantanderService.alterar(municipioSantander);
		return Response.ok().build();
	}

	@POST
	public Response incluir(MunicipioSantander municipioSantander) {
		municipioSantanderService.incluir(municipioSantander);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		municipioSantanderService.deletar(id);
		return Response.ok().build();
	}

}
