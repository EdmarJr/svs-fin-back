package br.com.gruposaga.cad.svs.ws.resources;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.integracao.safra.model.ParamCalculaParcelas;
import com.svs.fin.integracao.safra.model.RetornoCalculoParcelas;
import com.svs.fin.integracao.safra.model.RetornoProposta;
import com.svs.fin.integracao.safra.pojos.PropostaCompletaPFSafra;

import br.com.gruposaga.cad.svs.servico.integracao.ws.safra.SafraService;


@Path("/safra")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SafraResource {

	@Inject
	private SafraService safraService;


	@Context
	ServletContext servletContext;
	
	
	@POST
	@Path("/getSimulacoesFinanciamento")
	public Response obterSimulacoes(ParamCalculaParcelas parametros) {
		try {
			List<RetornoCalculoParcelas> retornosCalculoParcelas = new ArrayList<RetornoCalculoParcelas>();
			retornosCalculoParcelas.add(safraService.obterSimulacoes(parametros));
			return Response.ok(retornosCalculoParcelas).build();
		} catch (RemoteException e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@POST
	@Path("/proposta/pf")
	public Response enviarPropostaPF(PropostaCompletaPFSafra propostaCompletaPFSafra) {
		try {
			RetornoProposta retornoEnvioProposta = safraService.enviarPropostaPF(propostaCompletaPFSafra);
			return Response.ok(retornoEnvioProposta).build();
		} catch (RemoteException e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	

}
