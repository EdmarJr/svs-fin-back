package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.integracaoItau.FinanciamentoOnlineItau;
import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

import br.com.gruposaga.cad.svs.servico.FinanciamentoOnlineItauService;

@Path("/financiamentosOnlineItau")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FinanciamentoOnlineItauResource {
	
	@Inject
	private FinanciamentoOnlineItauService financiamentoOnlineItauService;
	
	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idOperacaoFinanciada") Long idOperacaoFinanciada) {
		if (idOperacaoFinanciada != null) {
			List<FinanciamentoOnlineItau> listaPorIdOperacaoFinanciada = obterPorOperacaoFinanciada(idOperacaoFinanciada);
			return Response.ok(listaPorIdOperacaoFinanciada).build();
		}
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<FinanciamentoOnlineItau> paginaDeObjetos = financiamentoOnlineItauService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}
	
	public List<FinanciamentoOnlineItau> obterPorOperacaoFinanciada(Long idOperacaoFinanciada) {
		List<FinanciamentoOnlineItau> retorno = financiamentoOnlineItauService.obterPorOperacaoFinanciada(idOperacaoFinanciada);
		return retorno;
	}

}
