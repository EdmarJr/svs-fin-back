package br.com.gruposaga.cad.svs.ws.resources.safra;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.integracao.safra.model.Tipo_Fluxo;
import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

import br.com.gruposaga.cad.svs.servico.integracao.ws.safra.services.Tipo_FluxoSafraService;

@Path("/tipo_FluxosSafra")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Tipo_FluxosSafraResource {

	@Inject
	private Tipo_FluxoSafraService tipo_FluxoSafraService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<Tipo_Fluxo> paginaDeObjetos = tipo_FluxoSafraService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(tipo_FluxoSafraService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(Tipo_Fluxo tipo_Fluxo) {
		tipo_FluxoSafraService.alterar(tipo_Fluxo);
		return Response.ok().build();
	}

	@POST
	public Response incluir(Tipo_Fluxo tipo_Fluxo) {
		tipo_FluxoSafraService.incluir(tipo_Fluxo);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		tipo_FluxoSafraService.deletar(id);
		return Response.ok().build();
	}

}
