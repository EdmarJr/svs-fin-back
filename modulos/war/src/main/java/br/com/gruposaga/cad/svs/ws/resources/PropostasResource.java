package br.com.gruposaga.cad.svs.ws.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import com.svs.fin.model.dto.PaginaDeObjetos;
import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.Proposta;

import br.com.gruposaga.cad.svs.exceptions.ClienteDealerNaoSalvoException;
import br.com.gruposaga.cad.svs.exceptions.PropostaNaoEncontradaException;
import br.com.gruposaga.cad.svs.servico.PropostaService;

@Path("/propostas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PropostasResource {

	@Inject
	private PropostaService propostaService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit) {
		GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
				.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
		PaginaDeObjetos<Proposta> paginaDeObjetos = propostaService.obterTodos(parametros);
		return Response.ok(paginaDeObjetos).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(propostaService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(Proposta proposta) {
		propostaService.alterar(proposta);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/propostaValor")
	public Response alteraValorProposta(Proposta proposta) {
		propostaService.alterarValorProposta(proposta);
		return Response.ok().build();
	}

	@GET
	@Path("/codigo/{codigoProposta}")
	public Response obterPorCodigo(@PathParam("codigoProposta") Long codigoProposta) {
		try {
			return Response.ok(propostaService.obterPorCodigo(codigoProposta)).build();
		} catch (JAXBException e) {
			e.printStackTrace();
			return Response.status(500).build();
		} catch (PropostaNaoEncontradaException e) {
			e.printStackTrace();
			return Response.status(404).build();
		} catch (ClienteDealerNaoSalvoException e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
	}

	@POST
	public Response incluir(Proposta proposta) {
		propostaService.incluir(proposta);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		propostaService.deletar(id);
		return Response.ok().build();
	}

}
