package br.com.gruposaga.cad.svs.ws.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;
import com.svs.fin.model.entities.OpcaoItemCbc;

import br.com.gruposaga.cad.svs.servico.OpcaoItemCbcService;

@Path("/opcoesItemCbc")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OpcoesItemCbcResource {

	@Inject
	private OpcaoItemCbcService opcaoItemCbcService;

	@GET
	public Response listar(@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("idUnidadeOrganizacional") Long idUnidadeOrganizacional) {

		Object jsonObjectRetorno = null;
		if (idUnidadeOrganizacional != null) {
			jsonObjectRetorno = obterPorIdUnidadeOrganizacional(idUnidadeOrganizacional);
		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			jsonObjectRetorno = opcaoItemCbcService.obterTodos(parametros);
		}
		return Response.ok(jsonObjectRetorno).build();
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(opcaoItemCbcService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(OpcaoItemCbc opcaoItemCbc) {
		opcaoItemCbcService.alterar(opcaoItemCbc);
		return Response.ok().build();
	}

	@POST
	public Response incluir(OpcaoItemCbc opcaoItemCbc) {
		opcaoItemCbcService.incluir(opcaoItemCbc);
		return Response.ok().build();
	}

	public List<OpcaoItemCbc> obterPorIdUnidadeOrganizacional(Long idUnidadeOrganizacional) {
		return opcaoItemCbcService.obterPorIdUnidadeOrganizacional(idUnidadeOrganizacional);

	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		opcaoItemCbcService.deletar(id);
		return Response.ok().build();
	}

}
